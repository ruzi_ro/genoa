Partial Public Class OptimiserClass
  ' **********************************************************************
  '
  '
  ' **********************************************************************

	Private z(-1) As Double														'objective function coefficients
  Private Price(-1) As Double                       'price vector
  Private Profit(-1) As Double                      'profitability vector
	Private AdjRate(-1) As Double											'rate of adjustment of IN variables
	Private NumberOfInABVs As Integer									'Number of IN ABVs (artificial basis variables)

  Private Function RunSimplex() As SimplexErrorCode
		' **********************************************************************
		'
		'
		' **********************************************************************

    '<M5> run simplex algorithm

    Dim i As Integer
    Dim j As Integer
    Dim temp As Double
    Dim ReturnCode As SimplexErrorCode

		ReDim z(VariableCount + NumberOfConstraints)
    ReDim Price(NumberOfConstraints)
    ReDim Profit(VariableCount + NumberOfConstraints)
    ReDim AdjRate(NumberOfConstraints)

    '<S1> Initialise all variables other than ABVs to be OUT at their lower limits
    ' (artificial basis variables)

    For j = 1 To VariableCount

      OutVars.Add(j)
      State(j) = VariableStress.vsLo
      PortfolioWeight(j) = LowerLimit(j)
      z(j) = 0                                'objective function "expected return"

    Next j

    '<S2> Set up ABVs (artificial basis variables)
    ' Coefficients stored to the 'right' of the existing Coefficient data, and in the
    ' Ai() array, origin 1,1.

    Try
      Dim ABV_Index As Integer

      For i = 1 To NumberOfConstraints

        ABV_Index = VariableCount + i

        temp = ConstraintLimit(i)

        For j = 1 To VariableCount
					temp -= (ConstraintCoefficient(i, j) * LowerLimit(j))	' NPP - At this point LowerLimit(j) = PortfolioWeight(j).
        Next j

        If temp >= 0 Then
          ConstraintCoefficient(i, ABV_Index) = 1
        Else
          ConstraintCoefficient(i, ABV_Index) = -1
        End If

				ConstraintCoefficientInverse(i, i) = ConstraintCoefficient(i, ABV_Index)

        InVars.Add(ABV_Index)
        State(ABV_Index) = VariableStress.vsIn
        PortfolioWeight(ABV_Index) = Math.Abs(temp)

        z(ABV_Index) = -1 ' objective function coefficients

      Next i                                      'objective function "expected return"

    Catch ex As Exception
    End Try


    NumberOfInABVs = NumberOfConstraints     'starting number of IN ABVs (artificial basis variables)

    ' NPP 12 Sep 07 - Appears obsolete
    '
    'MatrixRows = VariableCount + NumberOfConstraints
    'MatrixCols = 2
    'For i = 1 To (VariableCount + NumberOfConstraints)
    '  Matrix(i, 1) = State(i)
    '  Matrix(i, 2) = z(i)
    'Next

    '<S3> Run simplex phase 1

    ReturnCode = SimplexPhase(1)

    If ReturnCode = SimplexErrorCode.SimplexOK Then

      '<S4> No ABVs are in (not degenerate)
      'Reallocate arrays to delete elements for ABVs

      'ReDim Preserve LowerLimit(VariableCount)
      'ReDim Preserve UpperLimit(VariableCount)
      'ReDim Preserve ConstraintCoefficient(NumberOfConstraints, VariableCount)
      'ReDim Preserve PortfolioWeight(VariableCount)
      'ReDim Preserve State(VariableCount)

    ElseIf ReturnCode = SimplexErrorCode.SimplexDegenerate Then

      '<S5> Degenerate problem -- One or more ABV's still IN

      If Not SkipDegenerate Then
        Call MsgBox("Degenerate Problem, Program will continue", vbExclamation + vbOKCancel)
      End If

      ReturnCode = SimplexErrorCode.SimplexOK           'Allow program to continue

      ' VariableCount = VariableCount + NumberOfConstraints   'Add in ABVs to variable count
      _SimplexWasDegenerate = True ' Alters VariableCount as above...

      ' ReDim Preserve ExpectedReturns(VariableCount)
      ' ReDim Profit(VariableCount) ' Now initially allocated as a larger array

      'Set upper limits on ABVs to zero

      For i = 1 To NumberOfConstraints
        UpperLimit(VariableCount - NumberOfConstraints + i) = EPSILON
      Next i


      'Increase size of MMat() while preserving contents
      'X-X Covariance matrix is now initially sized to be larger. X-X

      'Dim Mtemp(1, 1)
      'ReDim Mtemp(NumberOfRealSecurities, NumberOfRealSecurities)

      'For i = 1 To NumberOfRealSecurities
      '  For j = 1 To NumberOfRealSecurities
      '    Mtemp(i, j) = CovarianceMatrix(i, j)
      '  Next j
      'Next i

      'ReDim CovarianceMatrix(VariableCount + NumberOfConstraints, VariableCount + NumberOfConstraints)
      'For i = 1 To NumberOfRealSecurities
      '  For j = 1 To NumberOfRealSecurities
      '    CovarianceMatrix(i, j) = Mtemp(i, j)
      '  Next j
      'Next i

      'InVars.Resize(VariableCount + NumberOfConstraints)
      'OutVars.Resize(VariableCount + NumberOfConstraints)

    Else
      ' Exit..

      Return ReturnCode
    End If

    '<S6> Run simplex phase 2
    'Objective is now to maximise expected return

    For j = 1 To VariableCount
      z(j) = ExpectedReturns(j)
    Next j

    ReturnCode = SimplexPhase(2)

    If ReturnCode = SimplexErrorCode.SimplexOK Then
      AlterMu() '<S7> Ensure unique solution
    End If

    Return ReturnCode




  End Function


	Private Function SimplexPhase(ByVal Phase As Integer) As SimplexErrorCode
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim i0 As Integer, i As Integer, j0 As Integer, j As Integer
		Dim k As Integer, jMax As Integer
		Dim InDirection As Direction
		Dim ProfitMax As Double, sum As Double

		SimplexPhase = SimplexErrorCode.SimplexOK

		Do While True

			'<S10> Compute price for each constraint

			For i = 1 To NumberOfConstraints

				sum = 0
				For j = 1 To InVars.Count
					sum = sum - ConstraintCoefficientInverse(j, i) * z(InVars.Member(j))
				Next j
				Price(i) = sum

			Next i

			'<S11> Compute profit for each OUT variable coming IN

			ProfitMax = 0.0#
			For j0 = 1 To OutVars.Count

				j = OutVars.Member(j0)
				sum = z(j)
				For i = 1 To NumberOfConstraints
					sum = sum + (ConstraintCoefficient(i, j) * Price(i))
				Next i
				If (IsUp(j)) Then sum = -sum
				Profit(j) = sum
				If Profit(j) >= ProfitMax Then
					jMax = j
					ProfitMax = Profit(j)
				End If

			Next j0

			If ProfitMax < EPSILON Then

				'<S12> No profit from any OUT variable coming IN

				If Phase = 1 Then

					'degenerate or infeasible problem

					For j0 = 1 To NumberOfInABVs

						j = InVars.Member(InVars.Count + 1 - j0)

						If PortfolioWeight(j) > EPSILON Then

							'An IN ABV is not zero -- infeasible problem

							Return SimplexErrorCode.SimplexInfeasible
							Exit Function

						End If

					Next j0

					'All IN ABVs are zero -- degenerate problem

					SimplexPhase = SimplexErrorCode.SimplexDegenerate	'(NumberOfInABVs>0)

				End If

				Exit Do

			End If

			If IsUp(jMax) Then
				InDirection = Direction.Lower
			Else
				InDirection = Direction.Higher
			End If

			'<S13> Compute rate of adjustment for each IN variable as
			'variable jMax comes IN (AdjRate=-Ai*ConstraintCoefficient(ALL,jMax))

			For i = 1 To NumberOfConstraints

				sum = 0

				For k = 1 To NumberOfConstraints
					sum = sum - ConstraintCoefficientInverse(i, k) * ConstraintCoefficient(k, jMax)
				Next k

				If InDirection = Direction.Lower Then
					sum = -sum
				End If
				AdjRate(i) = sum

			Next i

			'<S14> Compute theta, the maximum amount that variable jMax
			'can change before another IN variable hits a limit and is
			'forced OUT.  Also determine which variable is forced OUT.
			'Here we compute theta such that it will always be positive.

			Dim theta As Double, TempTheta As Double
			Dim iOut As Integer
			Dim OutDirection As Direction

			iOut = 0																		'0 indicates variable coming in also goes out
			OutDirection = InDirection

			If UpperLimit(jMax) >= INFINITY Then
				theta = INFINITY
			Else
				theta = UpperLimit(jMax) - LowerLimit(jMax)
			End If

			For i = 1 To NumberOfConstraints

				j = InVars.Member(i)

				If (AdjRate(i) < -EPSILON) Then

					'check for variable hitting lower limit
					TempTheta = (LowerLimit(j) - PortfolioWeight(j)) / AdjRate(i)
					If (TempTheta < theta) Then
						theta = TempTheta
						iOut = i
						OutDirection = Direction.Lower
					End If

				ElseIf (AdjRate(i) > EPSILON) And (UpperLimit(j) <> INFINITY) Then

					'Check for variable hitting upper limit
					TempTheta = (UpperLimit(j) - PortfolioWeight(j)) / AdjRate(i)
					If (TempTheta < theta) Then
						theta = TempTheta
						iOut = i
						OutDirection = Direction.Higher
					End If

				End If

			Next i

			'<S15> Check for failure to find a variable to go OUT

			If theta = INFINITY Then
				SimplexPhase = SimplexErrorCode.SimplexUnboundedE
				Exit Function
			End If

			'Get "j" index of variable going OUT.
			Dim jOut As Integer

			If iOut >= 1 Then
				jOut = InVars.Member(iOut)
			Else
				jOut = jMax															'variable coming in is also going out
			End If

			'<S16> Update the IN variables (X's)

			For i0 = 1 To NumberOfConstraints
				j = InVars.Member(i0)
				PortfolioWeight(j) = PortfolioWeight(j) + theta * AdjRate(i0)
			Next i0

			If InDirection = Direction.Higher Then
				PortfolioWeight(jMax) = PortfolioWeight(jMax) + theta
			Else
				PortfolioWeight(jMax) = PortfolioWeight(jMax) - theta
			End If

			GoIn(jMax)																	'<S17> variable jMax goes IN
			GoOut(jOut, OutDirection)										'<S18> variable jOut goes OUT

			'<S19> Update AInverse If var going OUT is not var coming IN.
			If jMax <> jOut Then
				UpdateAInverse(iOut, jMax, InDirection)
			End If

			If Phase = 1 And jOut > VariableCount Then

				'Artificial basis variable went out
				NumberOfInABVs -= 1

				If NumberOfInABVs <= 0 Then
					Exit Do	'all ABVs OUT -- End of phase 1
				End If

			End If

			'MsgBox("Phase " & Phase & " In " & jMax & " Out " & jOut & " ABVs " & NumberOfInABVs)
		Loop

	End Function


  '<S20> Update Ai (inverse if ConstraintCoefficient(ALL, IN)) for new IN set.
	Private Sub UpdateAInverse(ByVal iOut As Integer, ByVal jMax As Integer, ByVal InDirection As Direction)
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim temp As Double, i As Integer, k As Integer
		For i = 1 To NumberOfConstraints
			If i <> iOut Then
				temp = AdjRate(i) / AdjRate(iOut)
				For k = 1 To NumberOfConstraints
					ConstraintCoefficientInverse(i, k) = ConstraintCoefficientInverse(i, k) - ConstraintCoefficientInverse(iOut, k) * temp
				Next k
			End If
		Next i

		If InDirection = Direction.Higher Then
			temp = -AdjRate(iOut)
		Else
			temp = AdjRate(iOut)
		End If

		For k = 1 To NumberOfConstraints
			ConstraintCoefficientInverse(iOut, k) = ConstraintCoefficientInverse(iOut, k) / temp
		Next k

		'<S21> Reorder rows of Ai to stay consistent with Invars

		ReorderAiRows(iOut, InVars.Position(jMax))

	End Sub

  'Reorder the rows of Ai to stay consistent with InVars (ascending order).
  'In C or C++ this is more efficiently handled by manipulating pointers to rows

	Private Sub ReorderAiRows(ByVal DelRow As Integer, ByVal AddRow As Integer)
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim i As Integer, j As Integer, temp As Double

		If AddRow > DelRow Then

			For j = 1 To NumberOfConstraints
				temp = ConstraintCoefficientInverse(DelRow, j)
				For i = DelRow To AddRow - 1
					ConstraintCoefficientInverse(i, j) = ConstraintCoefficientInverse(i + 1, j)
				Next i
				ConstraintCoefficientInverse(AddRow, j) = temp
			Next j

		ElseIf AddRow < DelRow Then

			For j = 1 To NumberOfConstraints
				temp = ConstraintCoefficientInverse(DelRow, j)
				For i = DelRow To AddRow + 1 Step -1
					ConstraintCoefficientInverse(i, j) = ConstraintCoefficientInverse(i - 1, j)
				Next i
				ConstraintCoefficientInverse(AddRow, j) = temp
			Next j

		End If
	End Sub

  '<S30> Alter mu's as required to ensure unique solution

  Private Sub AlterMu()
		' **********************************************************************
		'
		'
		' **********************************************************************

    Dim j0 As Integer, j As Integer
    For j0 = 1 To OutVars.Count
      j = OutVars.Member(j0)
      If Profit(j) > -0.000001 Then
        If IsLo(j) Then
          ExpectedReturns(j) = ExpectedReturns(j) - 0.000001
        Else
          ExpectedReturns(j) = ExpectedReturns(j) + 0.000001
        End If
      End If
    Next j0

  End Sub

End Class
