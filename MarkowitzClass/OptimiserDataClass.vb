Public Class OptimiserDataClass
	' *****************************************************************************************
	' This Class is designed to return a copy of the Optimiser parameters and results set.
	'
	' May also be used to set the Optimisation Parameters.
	'
	' *****************************************************************************************

	Public ParametersSet As Boolean
	Public ResultsSet As Boolean


	Public OptimiserInitialised As Boolean
	Public OptimiserCompleted As Boolean

	Public InstrumentNumerator As Dictionary(Of Integer, Integer)	' Key is PertracId, Integer is Array Offset - Copied from the ConstraintWorkerClass
	Public SkipDegenerate As Boolean
	Public NumberOfRealSecurities As Integer	' Number of securities
	Public NumberOfSlackVariables As Integer	' Number of Slack variables, used to convert constraints to Equality constraints.
	Public NumberOfConstraints As Integer			' Number of Original Constraints
	Public ELambdaE As Double									' Stop after this lambdaE (min 1E-5)
	Public MaxCPs As Integer									' Maximum number of Corner Portfolios to calculate
	Public VariableCount As Integer						' Number of Real and imagined Securities
	Public SimplexWasDegenerate As Boolean		' Result for Simplex Pass One, If Degenerate then ABVs included in pass Two.
	Public CornerPortfolioCount As Integer		' Actual number of corner portfolios generated

	Public ExpectedReturns() As Double				' (Mu) Expected returns vector
	Public StdErrors() As Double							' 
	Public LowerLimit() As Double							' Lower Limits Vector
	Public UpperLimit() As Double							' Upper Limits Vector

	Public ConstraintCoefficient(,) As Double	' Constraint coefficient matrix
	Public ConstraintLimit() As Double				 ' Constraint right-hand sides
	Public MarginalsConstraintArray(,) As Double

	Public CovarianceMatrix(,) As Double			 'M (Correlation) matrix

	Public StatusString As String

	Public CP_Number(-1) As Integer
	Public CP_ExpectedReturn(-1) As Double
	Public CP_StandardDeviation(-1) As Double
	Public CP_LambdaE(-1) As Double
	Public CP_WeightingArray(-1) As Array

	Public SaveDebugInfo As Boolean
	Public Debug_PortfolioWeight() As Array
	Public Debug_AlphaV() As Array
	Public Debug_BetaV() As Array
	Friend Debug_StaticVars() As CLAIterationVariables
	Public Debug_InVars() As Array
	Public Debug_OutVars() As Array
	Public Debug_BBar() As Array
	Public Debug_Mi() As Array
	Public Debug_ExpectedReturns() As Array

	Public Flag_NeverAllowZeroWeightedStocksBackIn As Boolean
	Public StockBannedList As New ArrayList

	Public Sub New()

	End Sub

	Public Sub ResetDataObject()
		' ***********************************************************************
		' Does what it says....
		'
		' ***********************************************************************

		Try

			ParametersSet = False
			ResultsSet = False

			OptimiserInitialised = False
			OptimiserCompleted = False
			CornerPortfolioCount = 0

			If (InstrumentNumerator IsNot Nothing) Then
				Try
					If (InstrumentNumerator.Count > 0) Then
						InstrumentNumerator.Clear()
					End If
				Catch ex As Exception
				End Try
			End If
			InstrumentNumerator = New Dictionary(Of Integer, Integer)

			ELambdaE = 0
			MaxCPs = 0
			SimplexWasDegenerate = False
			NumberOfRealSecurities = 0
			NumberOfSlackVariables = 0
			NumberOfConstraints = 0

			VariableCount = 0

			ExpectedReturns = Nothing
			StdErrors = Nothing
			LowerLimit = Nothing
			UpperLimit = Nothing
			ConstraintCoefficient = Nothing
			ConstraintLimit = Nothing
			CovarianceMatrix = Nothing
			MarginalsConstraintArray = Nothing

			If (CP_Number IsNot Nothing) AndAlso (CP_Number.Length > 0) Then
				ReDim CP_Number(-1)
				ReDim CP_ExpectedReturn(-1)
				ReDim CP_StandardDeviation(-1)
				ReDim CP_LambdaE(-1)
				ReDim CP_WeightingArray(-1)
			End If

			SaveDebugInfo = False
			Debug_PortfolioWeight = Nothing
			Debug_AlphaV = Nothing
			Debug_BetaV = Nothing
			Debug_StaticVars = Nothing
			Debug_InVars = Nothing
			Debug_OutVars = Nothing
			Debug_BBar = Nothing
			Debug_Mi = Nothing
			ExpectedReturns = Nothing

			Flag_NeverAllowZeroWeightedStocksBackIn = False
			If (StockBannedList IsNot Nothing) Then
				StockBannedList.Clear()
			End If

			StatusString = ""

		Catch ex As Exception
		End Try

	End Sub

	Public ReadOnly Property InstrumentIDs() As Integer()
		' ***********************************************************************
		' Returns Integer array of Pertrac IDs in the correct order.
		' Note the array is Zero based, where as most of the arrays in the Optimiser
		' are Base One (1) - That being a legacy of its origins as an Excel Macro !!
		'
		' ***********************************************************************

		Get
			Dim RVal(-1) As Integer

			Try

				If (InstrumentNumerator IsNot Nothing) AndAlso (InstrumentNumerator.Count > 0) Then
					ReDim RVal(InstrumentNumerator.Count)

					Dim ID_Array(InstrumentNumerator.Keys.Count - 1) As Integer
					Dim Counter As Integer

					InstrumentNumerator.Keys.CopyTo(ID_Array, 0)

					For Counter = 0 To (ID_Array.Length - 1)
						RVal(InstrumentNumerator(ID_Array(Counter))) = ID_Array(Counter)
					Next

				End If

			Catch ex As Exception
			End Try

			Return RVal

		End Get
	End Property




End Class
