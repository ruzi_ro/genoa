Partial Public Class OptimiserClass
	' **********************************************************************
	' This Interface allows access to the Constraint Worker class by the 
	' Optimiser class. I tried moving the Constraint worker class to the 
	' Markowitz class, but it is slosely associated with the frmOptimiser
	' class so this proved more trouble than it was worth.
	'
	' This Interface only defines those functions used by the Markowitz class.
	'
	' **********************************************************************

	Public Interface IConstraintWorkerInterface

		Sub RefreshAllData(Optional ByVal pForceReload As Boolean = False)

		Sub OptimiserConstraintArray(ByRef pConstraintsArray(,) As Double, ByRef pConstraintValues() As Double, ByRef pInstrumentCount As Integer, ByRef pOptimiserInstrumentNumerator As Dictionary(Of Integer, Integer), ByRef pSlackVariableCount As Integer, ByRef pOriginalConstraintCount As Integer, ByRef pRunUnconstrained As Boolean)

		Function OptimiserExpectedReturns(ByRef pExpectedReturns() As Double, ByRef pStdErrors() As Double, ByVal NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer) As Integer

		Function OptimiserUpperAndLowerLimits(ByRef pLowerLimit() As Double, ByRef pUpperLimit() As Double, ByRef NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer, ByVal pRunUnconstrained As Boolean) As Integer

		Function OptimiserCovarianceMatrix(ByRef pCovarianceMatrix(,) As Double, ByRef NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer) As Integer

		Function CopyMarginalsConstraintArray() As Double(,)

	End Interface

End Class

