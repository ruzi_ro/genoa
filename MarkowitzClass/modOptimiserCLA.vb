Partial Public Class OptimiserClass
  ' **********************************************************************
  '
  '
  ' **********************************************************************

	Private alphav(-1) As Double
	Private betav(-1) As Double
	Private xi(-1) As Double
	Private bbar(-1) As Double
	Private Mi(-1, -1) As Double
	'Private E1 As Double
	'Private V1 As Double

	Private OutCPNum(-1) As Integer
	Private OutE(-1) As Double
	Private OutSD(-1) As Double
	Private OutLambdaE(-1) As Double
	Private OutX(-1) As Array

	'Variables to keep track of securities coming in or out of the solution in the CLA.  Used to spot loops - in which case
	'mu is peturbed
	'Now encapsulated in the CLAIterationVariables class, NPP 23/Oct/2007.

	'Dim Problemj As Integer					 'The security causing the problem :- Appears to do nothing !!
	'Dim jChange As Integer					 'Security coming in or out on any iteration
	'Dim jLast As Integer						 'The last one to come in or out
	'Dim jLastButOne As Integer			 'The one to come in or out before the last one

	Private Sub CLASetup()
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim j0 As Integer
		Dim j As Integer
		Dim k0 As Integer
		Dim k As Integer
		Dim i As Integer
		Dim sum As Double

		' Initialise Output Arrays.

		ReDim OutCPNum(MaxCPs)
		ReDim OutE(MaxCPs)
		ReDim OutSD(MaxCPs)
		ReDim OutLambdaE(MaxCPs)
		ReDim OutX(MaxCPs)

		Try
			If (StockBannedList IsNot Nothing) Then
				StockBannedList.Clear()
			Else
				StockBannedList = New ArrayList
			End If
			If (InStockWatchQueue IsNot Nothing) Then
				InStockWatchQueue.Clear()
			Else
				InStockWatchQueue = New Queue(Of Integer)
			End If
			If (InStockWeights IsNot Nothing) Then
				InStockWeights.Clear()
			Else
				InStockWeights = New Dictionary(Of Integer, Double)
			End If

		Catch ex As Exception
		End Try

		'<C1> Allocate arrays to CLA

		ReDim alphav(VariableCount + NumberOfConstraints)
		ReDim betav(VariableCount + NumberOfConstraints)
		ReDim xi(VariableCount + NumberOfConstraints)
		ReDim bbar(VariableCount + NumberOfConstraints)
		ReDim Mi(VariableCount + NumberOfConstraints, VariableCount + NumberOfConstraints)

		'<C2> Initialise OUT elements of alpha and beta

		For j0 = 1 To OutVars.Count
			j = OutVars.Member(j0)
			alphav(j) = PortfolioWeight(j)
			betav(j) = 0
		Next j0

		'<C3> Add the Lambda variables to the IN set

		For j = VariableCount + 1 To VariableCount + NumberOfConstraints
			InVars.Add(j)
		Next j

		'<C4> Add A and A' to MMat (already contains C)
		For i = 1 To NumberOfConstraints
			For j = 1 To VariableCount
				CovarianceMatrix(VariableCount + i, j) = ConstraintCoefficient(i, j)
				CovarianceMatrix(j, VariableCount + i) = ConstraintCoefficient(i, j)
			Next j
		Next i

		'<C5> Compute bbar vector
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)
			If j <= VariableCount Then
				sum = 0
			Else
				sum = ConstraintLimit(j - VariableCount)
			End If
			For k0 = 1 To OutVars.Count
				k = OutVars.Member(k0)
				sum = sum - CovarianceMatrix(j, k) * PortfolioWeight(k)
			Next k0
			bbar(j) = sum
		Next j0

		'<C6> Set up initial Mi (M-bar-inverse)
		'   Mi = |  0          Ai           |
		'        | Ai'   -Ai'*C(IN,IN)*Ai   |
		'First copy Ai and Ai'
		For j0 = 1 To NumberOfConstraints	' Got to be wrong (NPP ??)
			j = InVars.Member(j0)
			For i = 1 To NumberOfConstraints
				Mi(VariableCount + i, j) = ConstraintCoefficientInverse(j0, i)
				Mi(j, VariableCount + i) = Mi(VariableCount + i, j)
			Next i
		Next j0

		'T=A'*C(IN,IN)

		Dim T(NumberOfConstraints, NumberOfConstraints) As Double
		'ReDim T(NumberOfConstraints, NumberOfConstraints)

		For i = 1 To NumberOfConstraints
			For j0 = 1 To NumberOfConstraints
				j = InVars.Member(j0)
				sum = 0
				For k = 1 To NumberOfConstraints
					sum = sum - ConstraintCoefficientInverse(k, i) * CovarianceMatrix(InVars.Member(k), j)
				Next k
				T(i, j0) = sum
			Next j0
		Next i

		'Lower right portion of Mi is then T * Ai

		For i = 1 To NumberOfConstraints
			For j = 1 To NumberOfConstraints
				sum = 0
				For k = 1 To NumberOfConstraints
					sum = sum + T(i, k) * ConstraintCoefficientInverse(k, j)
				Next k
				Mi(VariableCount + i, VariableCount + j) = sum
			Next j
		Next i

		ReDim ConstraintCoefficientInverse(-1, -1)

		' Debug
		If (SaveDebugInfo) Then
			Try
				Dim Counter As Integer
				Dim Index As Integer

				Debug_PortfolioWeight(0) = Array.CreateInstance(GetType(Double), PortfolioWeight.Length)
				Array.Copy(PortfolioWeight, Debug_PortfolioWeight(0), PortfolioWeight.Length)

				Debug_ExpectedReturns(0) = Array.CreateInstance(GetType(Double), ExpectedReturns.Length)
				Array.Copy(ExpectedReturns, Debug_ExpectedReturns(0), ExpectedReturns.Length)

				Debug_BBar(0) = Array.CreateInstance(GetType(Double), bbar.Length)
				Array.Copy(bbar, Debug_BBar(0), bbar.Length)

				Debug_AlphaV(0) = Array.CreateInstance(GetType(Double), alphav.Length)
				Array.Copy(alphav, Debug_AlphaV(0), alphav.Length)

				Debug_BetaV(0) = Array.CreateInstance(GetType(Double), betav.Length)
				Array.Copy(betav, Debug_BetaV(0), betav.Length)

				Debug_StaticVars(0) = New CLAIterationVariables()

				Debug_InVars(0) = Array.CreateInstance(GetType(Integer), VariableCount + NumberOfConstraints + 1)
				For Counter = 1 To InVars.Count
					Index = InVars.Member(Counter)

					If (Index < Debug_InVars(0).Length) Then
						Debug_InVars(0)(Index) = 1
					End If
				Next Counter

				Debug_OutVars(0) = Array.CreateInstance(GetType(Integer), VariableCount + NumberOfConstraints + 1)
				For Counter = 1 To OutVars.Count
					Index = OutVars.Member(Counter)

					If (Index < Debug_OutVars(0).Length) Then
						Debug_OutVars(0)(Index) = 1
					End If
				Next Counter

				Debug_Mi(0) = Array.CreateInstance(GetType(Double), Mi.GetLength(0), Mi.GetLength(1))
				Array.Copy(Mi, Debug_Mi(0), Mi.Length)

			Catch ex As Exception
			End Try
		End If


	End Sub

	Private Function Iteration(ByVal pCLA_Count As Integer, ByRef StaticVars As CLAIterationVariables) As Double
		' **********************************************************************
		'
		'
		' **********************************************************************
		' Returns LambdaE

		Dim LambdaE As Double = 0	' Return Value.

		'Static jMaxA As Integer
		'Static OutDirection As Direction
		'Static lambdaA As Double
		'Static jMaxB As Integer
		'Static InDirection As Direction
		'Static lambdaB As Double

		Dim j0 As Integer, j As Integer, k0 As Integer, k As Integer
		Dim tempLambdaA As Double, tempLambdaB As Double
		Dim alpha As Double, beta As Double
		Dim gamma As Double, delta As Double

		'<C10> If this is not the first iteration then add or delete the
		'variable determined in the previous iteration

		If pCLA_Count > 1 Then
			If (StaticVars.lambdaA > StaticVars.lambdaB) Then	' 
				'MsgBox(lambdaA)
				'MsgBox("Out on " & CLAcount - 1 & " iteration " & jMaxA)
				StaticVars.jChange = StaticVars.jMaxA
				DeleteVariable(StaticVars.jMaxA, StaticVars.OutDirection)
			Else
				'MsgBox(lambdaB)
				'MsgBox("In on " & CLAcount - 1 & " iteration " & jMaxB)
				StaticVars.jChange = StaticVars.jMaxB
				AddVariable(StaticVars.jMaxB)

				' This section of code seeks to avoid the problems that we have encountered whereby an instrument
				' goes IN with a weight of Zero, goes Out in the next iteration and then Goes IN again on the subsequent
				' iteration - this repeats indefinately causing the frontier to come to an abrupt halt.
				' 
				' The solution we have currently persued is to Block certain instruments from being selected by the IN selection
				' routine. Currently an instrument is being blocked if it goes IN with a the same weight as it last went IN with 
				' and it was also in the last 'Flag_TightInOutLoopWatchSize' stocks to go IN.
				' 
				' WHAT ABOUT STOCKS THAT WENT OUT WITH A WEIGHT ?????
				'
				'
				If (Flag_NeverAllowZeroWeightedStocksBackIn) Then
					If (InStockWatchQueue.Contains(StaticVars.jMaxB)) Then
						Try
							If (Math.Abs(PortfolioWeight(StaticVars.jMaxB) - InStockWeights(StaticVars.jMaxB)) < EPSILON) AndAlso (Not StockBannedList.Contains(StaticVars.jMaxB)) Then
								StockBannedList.Add(StaticVars.jMaxB)
							End If
						Catch ex As Exception
						End Try
					End If
				End If

				InStockWatchQueue.Enqueue(StaticVars.jMaxB)
				While (InStockWatchQueue.Count > Flag_TightInOutLoopWatchSize)
					InStockWatchQueue.Dequeue()
				End While

				If (InStockWeights.ContainsKey(StaticVars.jMaxB)) Then
					InStockWeights.Remove(StaticVars.jMaxB)
				End If
				InStockWeights.Add(StaticVars.jMaxB, PortfolioWeight(StaticVars.jMaxB))

			End If
		End If

		'******** Addition to original ********** Check to see if stuck in a loop 

		'' NPP - What does this achieve ?
		'' NPP - Commented out, 23 Oct 2007.

		'If (StaticVars.jChange = StaticVars.jLast) And (StaticVars.jChange = StaticVars.jLastButOne) Then
		'	'MsgBox("Loop on iteration " & CLAcount & " security " & jChange & " mu is " & ExpectedReturns(jChange) * 100 & "%")
		'	If StaticVars.jChange <= NumberOfRealSecurities Then
		'		StaticVars.Problemj = StaticVars.jChange
		'	End If
		'End If

		StaticVars.jLastButOne = StaticVars.jLast
		StaticVars.jLast = StaticVars.jChange

		'<C11> Determine which IN variable wants to go OUT first
		StaticVars.jMaxA = -1
		StaticVars.lambdaA = 0
		StaticVars.OutDirection = 0

		For j0 = 1 To InVars.Count
			'Compute alpha and beta for variable

			j = InVars.Member(j0)
			alpha = 0
			beta = 0

			For k0 = 1 To InVars.Count

				k = InVars.Member(k0)
				alpha += Mi(j, k) * bbar(k)

				If k <= VariableCount Then
					beta += Mi(j, k) * ExpectedReturns(k)
				End If

			Next k0

			alphav(j) = alpha
			betav(j) = beta

			If (j <= VariableCount) Then	' If (j < VariableCount) Then *** NPP 25 Mar 2008 ***

				'For non-lambda variable check for going OUT

				If beta > EPSILON Then

					'check for hitting lower limit.

					tempLambdaA = (LowerLimit(j) - alpha) / beta

					If tempLambdaA >= StaticVars.lambdaA Then
						StaticVars.jMaxA = j
						StaticVars.lambdaA = tempLambdaA
						StaticVars.OutDirection = Direction.Lower
					End If

				ElseIf UpperLimit(j) < INFINITY And beta < -EPSILON Then

					'Check for hitting upper limit

					tempLambdaA = (UpperLimit(j) - alpha) / beta
					If tempLambdaA >= StaticVars.lambdaA Then
						StaticVars.jMaxA = j
						StaticVars.lambdaA = tempLambdaA
						StaticVars.OutDirection = Direction.Higher
					End If

				End If

			End If
		Next j0

		'<C12> Determine which OUT variable wants to come IN first

		StaticVars.jMaxB = -1
		StaticVars.lambdaB = 0
		StaticVars.InDirection = 0

		For j0 = 1 To OutVars.Count
			'Compute gamma and delta for variable

			j = OutVars.Member(j0)
			gamma = 0
			delta = -ExpectedReturns(j)

			For k = 1 To VariableCount + NumberOfConstraints

				gamma += CovarianceMatrix(j, k) * alphav(k)
				delta += CovarianceMatrix(j, k) * betav(k)

			Next k

			If (Not Flag_NeverAllowZeroWeightedStocksBackIn) OrElse (Not StockBannedList.Contains(j)) Then

				If IsLo(j) Then

					If delta > EPSILON Then
						'check for variable coming off lower limit
						tempLambdaB = -gamma / delta
						If (tempLambdaB >= StaticVars.lambdaB) Then
							StaticVars.jMaxB = j
							StaticVars.lambdaB = tempLambdaB
							StaticVars.InDirection = Direction.Higher
						End If
					End If

				Else		'at upper limit

					If delta < -EPSILON Then
						'Check for variable coming off upper limit
						tempLambdaB = -gamma / delta
						If tempLambdaB >= StaticVars.lambdaB Then
							StaticVars.jMaxB = j
							StaticVars.lambdaB = tempLambdaB
							StaticVars.InDirection = Direction.Lower
						End If
					End If

				End If

			End If

		Next j0

		'<C13> The new lambda-E is the greater of lambda-A and lambda-B.
		'If lambda-A is greater, then the variable first goes OUT as
		'lambda-E is decreased.  If lambda-B is greater, then a
		'variable first comes IN is as lambda-E is decreased.
		'LambdaE = Math.Max(lambdaA, lambdaB)
		'LambdaE = Math.Max(LambdaE, 0.0#)

		LambdaE = Math.Max(Math.Max(StaticVars.lambdaA, StaticVars.lambdaB), 0.0#)

		'<C14> Calculate the new corner portfolio, the E and V for
		'new corner portfolio, a0, a1 and a2 between this and
		'previous corner portfolio.

		Dim ExpectedReturn As Double
		Dim Variance As Double

		Try
			' CalcCornerPortfolio(LambdaE, ExpectedReturn, Variance) ' ExpectedReturn, Variance are return values
			' Move inline - NPP 02 Oct 2007 - 

			' Calculate New Portfolio Weights.

			For j0 = 1 To InVars.Count - NumberOfConstraints
				j = InVars.Member(j0)
				PortfolioWeight(j) = alphav(j) + betav(j) * LambdaE
			Next j0
			'End If

			' Calculate Expected Return and Variance on the new portfolio.

			ExpectedReturn = 0
			Variance = 0

			For j = 1 To NumberOfRealSecurities
				ExpectedReturn += (ExpectedReturns(j) * PortfolioWeight(j))
				Variance += (CovarianceMatrix(j, j) * PortfolioWeight(j) * PortfolioWeight(j))

				For k = 1 To j - 1
					Variance += (2.0 * CovarianceMatrix(j, k) * PortfolioWeight(j) * PortfolioWeight(k))
				Next k
			Next j

		Catch ex As Exception
		End Try

		Try
			' Save Corner portfolio details to Output arrays.

			' OutputCornerPortfolio(pCLA_Count, ExpectedReturn, Variance, LambdaE)
			' Move inline - NPP 02 Oct 2007 - 

			Dim OutX_Array() As Double

			OutCPNum(pCLA_Count) = pCLA_Count
			OutE(pCLA_Count) = ExpectedReturn

			If (Variance < 0.0#) Then
				' Should not happen
				OutSD(pCLA_Count) = 0
			Else
				OutSD(pCLA_Count) = Variance ^ (0.5)
			End If

			' Found some portfolios returning NaN for Std Dev ' NPP 13 Feb 2013
			If Double.IsNaN(OutSD(pCLA_Count)) Then OutSD(pCLA_Count) = 0

			OutLambdaE(pCLA_Count) = LambdaE

			OutX_Array = System.Array.CreateInstance(GetType(Double), NumberOfRealSecurities + 1)
			Array.Copy(PortfolioWeight, OutX_Array, OutX_Array.Length)
			OutX(pCLA_Count) = OutX_Array

		Catch ex As Exception
		End Try

		' Debug
		If (SaveDebugInfo) Then
			Try
				Dim Counter As Integer
				Dim Index As Integer

				Debug_PortfolioWeight(pCLA_Count) = Array.CreateInstance(GetType(Double), PortfolioWeight.Length)
				Array.Copy(PortfolioWeight, Debug_PortfolioWeight(pCLA_Count), PortfolioWeight.Length)

				Debug_ExpectedReturns(pCLA_Count) = Array.CreateInstance(GetType(Double), ExpectedReturns.Length)
				Array.Copy(ExpectedReturns, Debug_ExpectedReturns(pCLA_Count), ExpectedReturns.Length)

				Debug_BBar(pCLA_Count) = Array.CreateInstance(GetType(Double), bbar.Length)
				Array.Copy(bbar, Debug_BBar(pCLA_Count), bbar.Length)

				Debug_AlphaV(pCLA_Count) = Array.CreateInstance(GetType(Double), alphav.Length)
				Array.Copy(alphav, Debug_AlphaV(pCLA_Count), alphav.Length)

				Debug_BetaV(pCLA_Count) = Array.CreateInstance(GetType(Double), betav.Length)
				Array.Copy(betav, Debug_BetaV(pCLA_Count), betav.Length)

				Debug_StaticVars(pCLA_Count) = New CLAIterationVariables(StaticVars)

				Debug_InVars(pCLA_Count) = Array.CreateInstance(GetType(Integer), VariableCount + NumberOfConstraints + 1)
				For Counter = 1 To InVars.Count
					Index = InVars.Member(Counter)

					If (Index < Debug_InVars(pCLA_Count).Length) Then
						Debug_InVars(pCLA_Count)(Index) = 1
					End If
				Next Counter

				Debug_OutVars(pCLA_Count) = Array.CreateInstance(GetType(Integer), VariableCount + NumberOfConstraints + 1)
				For Counter = 1 To OutVars.Count
					Index = OutVars.Member(Counter)

					If (Index < Debug_OutVars(pCLA_Count).Length) Then
						Debug_OutVars(pCLA_Count)(Index) = 1
					End If
				Next Counter

				Debug_Mi(pCLA_Count) = Array.CreateInstance(GetType(Double), Mi.GetLength(0), Mi.GetLength(1))
				Array.Copy(Mi, Debug_Mi(pCLA_Count), Mi.Length)

			Catch ex As Exception
			End Try
		End If

		Return LambdaE

	End Function

	

	'Do Updates required for variable jAdd to come IN.
	Private Sub AddVariable(ByVal jAdd As Integer)
		' **********************************************************************
		'
		'
		' **********************************************************************

		If (jAdd <= 0) Then

			Exit Sub
		End If

		Dim j0 As Integer
		Dim j As Integer
		Dim k0 As Integer
		Dim k As Integer
		Dim sum As Double
		Dim xij As Double

		'MsgBox("Add " & jAdd)

		'C20> update Mi for variable coming in
		'xi = Mi(IN, IN) * M(IN, jAdd)
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)
			sum = 0
			For k0 = 1 To InVars.Count
				k = InVars.Member(k0)
				sum = sum + Mi(j, k) * CovarianceMatrix(k, jAdd)
			Next k0
			xi(j) = sum
		Next j0

		'xij=M(jAdd,jAdd) - M(jAdd,IN) * xi
		xij = CovarianceMatrix(jAdd, jAdd)
		For k0 = 1 To InVars.Count
			k = InVars.Member(k0)
			xij = xij - CovarianceMatrix(jAdd, k) * xi(k)
		Next k0

		'Mi(IN,IN)+=xi*xi.T/xij
		'Mi(jAdd,IN) = Mi(IN,jAdd)=-xi/xij
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)

			For k0 = 1 To j0 - 1
				k = InVars.Member(k0)

				Mi(j, k) = Mi(j, k) + (xi(j) * xi(k) / xij)
				Mi(k, j) = Mi(j, k)
			Next k0

			Mi(j, j) = Mi(j, j) + (xi(j) * xi(j) / xij)

			Mi(j, jAdd) = -xi(j) / xij
			Mi(jAdd, j) = Mi(j, jAdd)
		Next j0
		Mi(jAdd, jAdd) = 1.0# / xij

		'<C21> Update bbar for the current IN variables
		'  bbar(IN)=bbar(IN)+M(IN,jAdd)*PortfolioWeight(jAdd)
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)
			bbar(j) = bbar(j) + CovarianceMatrix(j, jAdd) * PortfolioWeight(jAdd)
		Next j0

		GoIn(jAdd)			'Variable jAdd goes IN

		'<C22> Compute bbar for new IN variable.
		'  bbar(jAdd) =-M(jAdd,OUT)*PortfolioWeight(OUT)
		sum = 0
		For j0 = 1 To OutVars.Count
			j = OutVars.Member(j0)
			sum = sum - CovarianceMatrix(jAdd, j) * PortfolioWeight(j)
		Next j0
		bbar(jAdd) = sum

	End Sub

	'Do Updates required for variable jDel to go OUT.
	Private Sub DeleteVariable(ByVal jDel As Integer, ByVal Direction As Integer)
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim j0 As Integer, j As Integer, k0 As Integer, k As Integer

		'MsgBox("Delete " & jDel)

		'<c30> update alpha and beta vectors for variable going out
		alphav(jDel) = PortfolioWeight(jDel)
		betav(jDel) = 0

		GoOut(jDel, Direction)											'<c31> variable jDel goes OUT

		'<C32> Update Mi and bbar for variable going OUT
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)
			For k0 = 1 To InVars.Count
				k = InVars.Member(k0)
				Mi(j, k) = Mi(j, k) - Mi(j, jDel) * Mi(jDel, k) / Mi(jDel, jDel)
			Next k0
		Next j0

		'<c33> update bbar(IN)
		For j0 = 1 To InVars.Count
			j = InVars.Member(j0)
			bbar(j) = bbar(j) - CovarianceMatrix(j, jDel) * PortfolioWeight(jDel)
		Next j0

		'' _NPP 
		'bbar(jDel) = 0
		'For j = 1 To (VariableCount + NumberOfConstraints)
		'	Mi(j, jDel) = 0
		'	Mi(jDel, j) = 0
		'Next

	End Sub

	
	''Calculate the new cornerportfolio and statistics
	'Private Sub CalcCornerPortfolio(ByVal pLambdaE As Double, ByRef rExpectedReturn As Double, ByRef rVariance As Double)
	'	' **********************************************************************
	'	'
	'	'
	'	' **********************************************************************

	'	' Static OldLambdaE As Double
	'	'Dim dEdLambdaE As Double

	'	Dim j As Integer, j0 As Integer, k As Integer

	'	'<c40> Calculate the new corner portfolio

	'	For j0 = 1 To InVars.Count - NumberOfConstraints
	'		j = InVars.Member(j0)
	'		PortfolioWeight(j) = alphav(j) + betav(j) * pLambdaE
	'	Next j0

	'	'<c41> Calculate dE_dLamdba

	'	'dEdLambdaE = 0

	'	'For j0 = 1 To InVars.Count - NumberOfConstraints
	'	'	j = InVars.Member(j0)
	'	'	dEdLambdaE = dEdLambdaE + betav(j) * ExpectedReturns(j)
	'	'Next j0

	'	'If dEdLambdaE < 0.000000001 The

	'	'If True Then 'force recalc of E and V from scratch

	'	'<c42> "kink" in curve, compute E and V from scratch
	'	'a0 = INVALID : a1 = INVALID : a2 = INVALID

	'	'a0 = -1
	'	'a1 = -1
	'	'a2 = -1 'Replaces above line so that results can be displayed

	'	rExpectedReturn = 0
	'	rVariance = 0

	'	For j = 1 To NumberOfRealSecurities
	'		rExpectedReturn += (ExpectedReturns(j) * PortfolioWeight(j))
	'		rVariance += (CovarianceMatrix(j, j) * PortfolioWeight(j) * PortfolioWeight(j))

	'		For k = 1 To j - 1
	'			rVariance += (2.0 * CovarianceMatrix(j, k) * PortfolioWeight(j) * PortfolioWeight(k))
	'		Next k
	'	Next j

	'	'Else
	'	'  '<c43> comput a0, a1, a2, E, and V
	'	'  a2 = 1.0# / dEdLambdaE
	'	'  a1 = 2.0# * (OldLambdaE - a2 * ExpectedReturn)
	'	'  a0 = Variance - a1 * ExpectedReturn - a2 * ExpectedReturn * ExpectedReturn
	'	'  ExpectedReturn = ExpectedReturn + (LambdaE - OldLambdaE) * dEdLambdaE
	'	'  Variance = a0 + a1 * ExpectedReturn + a2 * ExpectedReturn * ExpectedReturn
	'	'  If Variance < 0 Then Variance = 0

	'	'  'compare V and E to V1 and E1 computed from scratch

	'	'End If

	'	' OldLambdaE = LambdaE

	'End Sub

	'Private Sub OutputCornerPortfolio(ByVal pCLA_Count As Integer, ByVal pExpectedReturn As Double, ByVal pVariance As Double, ByVal pLambdaE As Double)
	'	' **********************************************************************
	'	'
	'	'
	'	' **********************************************************************
	'	Dim OutX_Array() As Double

	'	OutCPNum(pCLA_Count) = pCLA_Count
	'	OutE(pCLA_Count) = pExpectedReturn
	'	OutSD(pCLA_Count) = pVariance ^ (0.5)
	'	OutLambdaE(pCLA_Count) = pLambdaE

	'	'For i = 1 To NumberOfRealSecurities
	'	'	OutX_Array(i) = PortfolioWeight(i)
	'	'Next

	'	OutX_Array = System.Array.CreateInstance(GetType(Double), NumberOfRealSecurities + 1)
	'	Array.Copy(PortfolioWeight, OutX_Array, OutX_Array.Length)
	'	' 		PortfolioWeight.CopyTo(OutX_Array, 0)
	'	OutX(pCLA_Count) = OutX_Array

	'End Sub


End Class
