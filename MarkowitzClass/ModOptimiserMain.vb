Imports System.IO

Partial Public Class OptimiserClass


  ' **********************************************************************
  '
  '
  ' **********************************************************************

#Region " Local and Global Variable declarations"

	Private _OptimiserInitialised As Boolean
	Private _OptimiserCompleted As Boolean

	Private _InstrumentNumerator As Dictionary(Of Integer, Integer)	' Key is PertracId, Integer is Array Offset - Copied from the ConstraintWorkerClass
	Private _SkipDegenerate As Boolean
	Private _NumberOfRealSecurities As Integer	' Number of securities
	Private _NumberOfSlackVariables As Integer	' Number of Slack variables, used to convert constraints to Equality constraints.
	Private _NumberOfConstraints As Integer			' Number of Original Constraints
	Private _ELambdaE As Double									' Stop after this lambdaE (min 1E-5)
	Private _MaxCPs As Integer									' Maximum number of Corner Portfolios to calculate
	Private _VariableCount As Integer						' Number of Real and imagined Securities
	Private _SimplexWasDegenerate As Boolean		' Result for Simplex Pass One, If Degenerate then ABVs included in pass Two.
	Private _CornerPortfolioCount As Integer		' Actual number of corner portfolios generated
	'Private _IncludeLimitsAsConstraints As Boolean
	Private _UseMarginalsIteration As Boolean
	Private _MAX_MarginalsMoveCount As Integer

	Private ExpectedReturns() As Double					' (Mu) Expected returns vector
	Private StdErrors() As Double								' Std Errors, Used to perturb expected returns for analysis purposes.
	Private LowerLimit() As Double							' Lower Limits Vector
	Private UpperLimit() As Double							' Upper Limits Vector

	Private ConstraintCoefficient(,) As Double	' Constraint coefficient matrix
	Private ConstraintLimit() As Double					' Constraint right-hand sides
	Private ConstraintCoefficientInverse(,) As Double	'Inverse of In columns of A
	Private MarginalsConstraintArray(,) As Double

	Private CovarianceMatrix(,) As Double				'M (Correlation) matrix

	Private InVars As Cset											'Set of In variables
	Private OutVars As Cset											'Set of Out variables
	Private PortfolioWeight() As Double					'Portfolio weights
	Private State() As VariableStress						'Variable states

	Private _StatusString As String
	Private _SaveDebugInfo As Boolean

	Private Debug_PortfolioWeight() As Array
	Private Debug_AlphaV() As Array
	Private Debug_BetaV() As Array
	Private Debug_StaticVars() As CLAIterationVariables
	Private Debug_InVars() As Array
	Private Debug_OutVars() As Array
	Private Debug_BBar() As Array
	Private Debug_Mi() As Array
	Private Debug_ExpectedReturns() As Array

	Private Flag_NeverAllowZeroWeightedStocksBackIn As Boolean = False
	Private StockBannedList As New ArrayList
	Private Flag_TightInOutLoopWatchSize As Integer = 1	' Used for
	Private InStockWatchQueue As New Queue(Of Integer)
	Private InStockWeights As New Dictionary(Of Integer, Double)

#End Region

#Region " Property declarations"

  Public ReadOnly Property OptimiserInitialised() As Boolean
    Get
      Return _OptimiserInitialised
    End Get
  End Property

  Public ReadOnly Property OptimiserCompleted() As Boolean
    Get
      Return _OptimiserCompleted
    End Get
  End Property

  Public ReadOnly Property SkipDegenerate() As Boolean
    Get
      Return _SkipDegenerate
    End Get
  End Property

  Public ReadOnly Property NumberOfRealSecurities() As Integer
    ' Number of Given Securities.
    ' Set when the Securities are loaded.

    Get
      Return _NumberOfRealSecurities
    End Get
  End Property

  Public ReadOnly Property NumberOfSlackVariables() As Integer
    ' Number of Slack Variables.
    ' Set when the Constraints are loaded.
    ' One New Slack variable (together with associated 'LowerLimit =0') is created
    ' for each Non-Equality constraint.

    Get
      Return _NumberOfSlackVariables
    End Get
  End Property

  Public ReadOnly Property NumberOfConstraints() As Integer
    ' Number of Constraints.
    ' Set when the Constraints are loaded.

    Get
      Return _NumberOfConstraints
    End Get
  End Property

	'Public Property IncludeLimitsAsConstraints() As Boolean
	'	Get
	'		Return _IncludeLimitsAsConstraints
	'	End Get
	'	Set(ByVal value As Boolean)
	'		_IncludeLimitsAsConstraints = value
	'	End Set
	'End Property

	Public Property UseZeroWeightFix() As Boolean
		Get
			Return Flag_NeverAllowZeroWeightedStocksBackIn
		End Get
		Set(ByVal value As Boolean)
			Flag_NeverAllowZeroWeightedStocksBackIn = value
		End Set
	End Property

	Public Property UseMarginalsIteration() As Boolean
		Get
			Return _UseMarginalsIteration
		End Get
		Set(ByVal value As Boolean)
			_UseMarginalsIteration = value
		End Set
	End Property

	Public Property MAX_MarginalsMoveCount() As Integer
		Get
			Return _MAX_MarginalsMoveCount
		End Get
		Set(ByVal value As Integer)
			_MAX_MarginalsMoveCount = value
		End Set
	End Property

  Public Property ELambdaE() As Double
    Get
      Return _ELambdaE
    End Get
    Set(ByVal value As Double)
      _ELambdaE = value
    End Set
  End Property

  Public Property MaxCPs() As Integer
    Get
      Return _MaxCPs
    End Get
    Set(ByVal value As Integer)
      _MaxCPs = value
    End Set
  End Property

	Public Property SaveDebugInfo() As Boolean
		Get
			Return _SaveDebugInfo
		End Get
		Set(ByVal value As Boolean)
			_SaveDebugInfo = value
		End Set
	End Property

  Public ReadOnly Property VariableCount() As Integer
    '

    Get
      If _SimplexWasDegenerate Then
        Return (_NumberOfRealSecurities + _NumberOfSlackVariables + _NumberOfConstraints)
      Else
        Return (_NumberOfRealSecurities + _NumberOfSlackVariables)
      End If
    End Get
  End Property

	Public ReadOnly Property StatusString() As String
		Get
			Return _StatusString
		End Get
	End Property

	Public ReadOnly Property CornerPortfolioCount() As Integer
		Get
			Return _CornerPortfolioCount
		End Get
	End Property

	Public ReadOnly Property InstrumentIDs(Optional ByVal pOptimiserSnapshot As OptimiserDataClass = Nothing) As Integer()
		' *****************************************************************************************
		' Return Array of Pertrac IDs used in the Optimiser, in the order that they are represented 
		' internally.
		'
		' Note : Values returned are Base 1, element Zero is unused.
		' *****************************************************************************************

		Get
			Dim RVal(-1) As Integer

			Try
				Dim ThisNumerator As Dictionary(Of Integer, Integer)

				' Get reference to Instrument Numerator.

				If (pOptimiserSnapshot Is Nothing) Then
					ThisNumerator = _InstrumentNumerator
				Else
					ThisNumerator = pOptimiserSnapshot.InstrumentNumerator
				End If

				' Create return array of Pertrac IDs in the correct positions.

				If (ThisNumerator IsNot Nothing) AndAlso (ThisNumerator.Count > 0) Then
					ReDim RVal(ThisNumerator.Count)

					Dim ID_Array(ThisNumerator.Keys.Count - 1) As Integer
					Dim Counter As Integer

					ThisNumerator.Keys.CopyTo(ID_Array, 0)

					For Counter = 0 To (ID_Array.Length - 1)
						RVal(ThisNumerator(ID_Array(Counter)) + 1) = ID_Array(Counter)
					Next

				End If

			Catch ex As Exception
			End Try

			Return RVal

		End Get
	End Property

#End Region

#Region " Object Constructor."

  Public Sub New()
    ' **********************************************************************
    '
    '
    ' **********************************************************************

    ' Initialise Variables.

    _SkipDegenerate = False

    ResetOptimisation()

  End Sub

  Public Sub ResetOptimisation()

    _OptimiserInitialised = False
    _OptimiserCompleted = False
		_CornerPortfolioCount = 0
		_SaveDebugInfo = False
		_UseMarginalsIteration = False

		If (_InstrumentNumerator IsNot Nothing) Then
			Try
				If (_InstrumentNumerator.Count > 0) Then
					_InstrumentNumerator.Clear()
				End If
			Catch ex As Exception
				_InstrumentNumerator = New Dictionary(Of Integer, Integer)
			End Try
		Else
			_InstrumentNumerator = New Dictionary(Of Integer, Integer)
		End If

		_ELambdaE = 0.000001 ' Default Value.
    _MaxCPs = 100
    _SimplexWasDegenerate = False
    _NumberOfRealSecurities = 0
    _NumberOfSlackVariables = 0
    _NumberOfConstraints = 0

    _VariableCount = 0

		ExpectedReturns = Nothing
		StdErrors = Nothing
    LowerLimit = Nothing
    UpperLimit = Nothing
    ConstraintCoefficient = Nothing
    ConstraintLimit = Nothing
    CovarianceMatrix = Nothing
		MarginalsConstraintArray = Nothing

    InVars = Nothing
    OutVars = Nothing
    PortfolioWeight = Nothing
    State = Nothing
		ConstraintCoefficientInverse = Nothing

		If (OutCPNum.Length > 0) Then
			ReDim OutCPNum(-1)
			ReDim OutE(-1)
			ReDim OutSD(-1)
			ReDim OutLambdaE(-1)
			ReDim OutX(-1)
		End If

		_StatusString = ""

		Try
			Debug_PortfolioWeight = Nothing
			Debug_AlphaV = Nothing
			Debug_BetaV = Nothing
			Debug_StaticVars = Nothing
			Debug_InVars = Nothing
			Debug_OutVars = Nothing
			Debug_BBar = Nothing
			Debug_Mi = Nothing
			Debug_ExpectedReturns = Nothing

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Set Inputs Routines"

	Public Function InitialiseOptimiser(ByVal OptimiserSnapshot As OptimiserDataClass) As Boolean
		' **********************************************************************
		' Initialise the optimiser with the parameters contained in the given 
		' OptimiserDataClass object.
		'
		' **********************************************************************

		Try
			ResetOptimisation()

			If (OptimiserSnapshot Is Nothing) OrElse (OptimiserSnapshot.OptimiserInitialised = False) Then
				Exit Function
			End If

			' Instrument Numerator

			If (_InstrumentNumerator Is Nothing) Then
				_InstrumentNumerator = New Dictionary(Of Integer, Integer)
			ElseIf (_InstrumentNumerator.Count > 0) Then
				_InstrumentNumerator.Clear()
			End If

			If (OptimiserSnapshot.InstrumentNumerator.Keys.Count > 0) Then
				Dim InstrumentCounter As Integer
				Dim ThisInstrumentID As Integer
				Dim InstrumentIDs(OptimiserSnapshot.InstrumentNumerator.Keys.Count - 1) As Integer

				OptimiserSnapshot.InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)

				For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)
					ThisInstrumentID = InstrumentIDs(InstrumentCounter)

					_InstrumentNumerator.Add(ThisInstrumentID, OptimiserSnapshot.InstrumentNumerator(ThisInstrumentID))
				Next
			End If

			_NumberOfRealSecurities = OptimiserSnapshot.NumberOfRealSecurities
			_NumberOfSlackVariables = OptimiserSnapshot.NumberOfSlackVariables
			_NumberOfConstraints = OptimiserSnapshot.NumberOfConstraints
			_ELambdaE = OptimiserSnapshot.ELambdaE
			_MaxCPs = OptimiserSnapshot.MaxCPs
			_VariableCount = OptimiserSnapshot.VariableCount
			Flag_NeverAllowZeroWeightedStocksBackIn = OptimiserSnapshot.Flag_NeverAllowZeroWeightedStocksBackIn

			' ExpectedReturns
			If (OptimiserSnapshot.ExpectedReturns IsNot Nothing) AndAlso (OptimiserSnapshot.ExpectedReturns.Length > 0) Then
				ReDim ExpectedReturns(OptimiserSnapshot.ExpectedReturns.Length - 1)

				Array.Copy(OptimiserSnapshot.ExpectedReturns, ExpectedReturns, OptimiserSnapshot.ExpectedReturns.Length)
			Else
				ReDim ExpectedReturns(-1)
			End If

			' StdErrors
			If (OptimiserSnapshot.StdErrors IsNot Nothing) AndAlso (OptimiserSnapshot.StdErrors.Length > 0) Then
				ReDim StdErrors(OptimiserSnapshot.StdErrors.Length - 1)

				Array.Copy(OptimiserSnapshot.StdErrors, StdErrors, OptimiserSnapshot.StdErrors.Length)
			Else
				ReDim StdErrors(-1)
			End If

			' LowerLimit
			If (OptimiserSnapshot.LowerLimit IsNot Nothing) AndAlso (OptimiserSnapshot.LowerLimit.Length > 0) Then
				ReDim LowerLimit(OptimiserSnapshot.LowerLimit.Length - 1)

				Array.Copy(OptimiserSnapshot.LowerLimit, LowerLimit, OptimiserSnapshot.LowerLimit.Length)
			Else
				ReDim LowerLimit(-1)
			End If

			' UpperLimit
			If (OptimiserSnapshot.UpperLimit IsNot Nothing) AndAlso (OptimiserSnapshot.UpperLimit.Length > 0) Then
				ReDim UpperLimit(OptimiserSnapshot.UpperLimit.Length - 1)

				Array.Copy(OptimiserSnapshot.UpperLimit, UpperLimit, OptimiserSnapshot.UpperLimit.Length)
			Else
				ReDim UpperLimit(-1)
			End If

			' ConstraintCoefficient
			If (OptimiserSnapshot.ConstraintCoefficient IsNot Nothing) AndAlso (OptimiserSnapshot.ConstraintCoefficient.Rank = 2) Then
				If (OptimiserSnapshot.ConstraintCoefficient.GetLength(0) > 0) AndAlso (OptimiserSnapshot.ConstraintCoefficient.GetLength(1) > 0) Then
					ReDim ConstraintCoefficient(OptimiserSnapshot.ConstraintCoefficient.GetLength(0) - 1, OptimiserSnapshot.ConstraintCoefficient.GetLength(1) - 1)

					Array.Copy(OptimiserSnapshot.ConstraintCoefficient, ConstraintCoefficient, OptimiserSnapshot.ConstraintCoefficient.Length)
				End If
			End If

			' ConstraintLimit
			If (OptimiserSnapshot.ConstraintLimit IsNot Nothing) AndAlso (OptimiserSnapshot.ConstraintLimit.Length > 0) Then
				ReDim ConstraintLimit(OptimiserSnapshot.ConstraintLimit.Length - 1)

				Array.Copy(OptimiserSnapshot.ConstraintLimit, ConstraintLimit, OptimiserSnapshot.ConstraintLimit.Length)
			Else
				ReDim ConstraintLimit(-1)
			End If

			' MarginalsConstraintArray
			If (OptimiserSnapshot.MarginalsConstraintArray IsNot Nothing) AndAlso (OptimiserSnapshot.MarginalsConstraintArray.Rank = 2) Then
				If (OptimiserSnapshot.MarginalsConstraintArray.GetLength(0) > 0) AndAlso (OptimiserSnapshot.MarginalsConstraintArray.GetLength(1) > 0) Then
					ReDim MarginalsConstraintArray(OptimiserSnapshot.MarginalsConstraintArray.GetLength(0) - 1, OptimiserSnapshot.MarginalsConstraintArray.GetLength(1) - 1)

					Array.Copy(OptimiserSnapshot.MarginalsConstraintArray, MarginalsConstraintArray, OptimiserSnapshot.MarginalsConstraintArray.Length)
				End If
			End If

			' CovarianceMatrix
			If (OptimiserSnapshot.CovarianceMatrix IsNot Nothing) AndAlso (OptimiserSnapshot.CovarianceMatrix.Rank = 2) Then
				If (OptimiserSnapshot.CovarianceMatrix.GetLength(0) > 0) AndAlso (OptimiserSnapshot.CovarianceMatrix.GetLength(1) > 0) Then
					ReDim CovarianceMatrix(OptimiserSnapshot.CovarianceMatrix.GetLength(0) - 1, OptimiserSnapshot.CovarianceMatrix.GetLength(1) - 1)

					Array.Copy(OptimiserSnapshot.CovarianceMatrix, CovarianceMatrix, OptimiserSnapshot.CovarianceMatrix.Length)
				End If
			End If

			_OptimiserInitialised = True

			' Debug stuff.
			If (Debug_PortfolioWeight IsNot Nothing) Then
				Debug_PortfolioWeight = Nothing
			End If

			If (Debug_AlphaV IsNot Nothing) Then
				Debug_AlphaV = Nothing
			End If

			If (Debug_BetaV IsNot Nothing) Then
				Debug_BetaV = Nothing
			End If

			If (Debug_StaticVars IsNot Nothing) Then
				Debug_StaticVars = Nothing
			End If

			If (Debug_InVars IsNot Nothing) Then
				Debug_InVars = Nothing
			End If

			If (Debug_OutVars IsNot Nothing) Then
				Debug_OutVars = Nothing
			End If

			If (Debug_BBar IsNot Nothing) Then
				Debug_BBar = Nothing
			End If

			If (Debug_Mi IsNot Nothing) Then
				Debug_Mi = Nothing
			End If

			If (Debug_ExpectedReturns IsNot Nothing) Then
				Debug_ExpectedReturns = Nothing
			End If

		Catch ex As Exception

		End Try

	End Function

	Public Function InitialiseOptimiser(ByVal pConstraintWorker As IConstraintWorkerInterface, ByVal pRunUnconstrained As Boolean) As Boolean
		' **********************************************************************
		' Initialise the optimiser with the data contained in the given 
		' Constraint Worker.
		'
		' **********************************************************************

		Try
			ResetOptimisation()

			If (pConstraintWorker Is Nothing) Then
				Return False
			End If

			SyncLock pConstraintWorker

				' Check Constraint Class is upto date

				pConstraintWorker.RefreshAllData(False)

				' Get Constraints Array and Constraint Values

				Call pConstraintWorker.OptimiserConstraintArray(ConstraintCoefficient, ConstraintLimit, _NumberOfRealSecurities, _InstrumentNumerator, _NumberOfSlackVariables, _NumberOfConstraints, pRunUnconstrained)

				MarginalsConstraintArray = pConstraintWorker.CopyMarginalsConstraintArray()

				' Get Expected Returns (inc Slack Variables)

				If (pConstraintWorker.OptimiserExpectedReturns(ExpectedReturns, StdErrors, NumberOfRealSecurities, NumberOfSlackVariables, NumberOfConstraints) < 0) Then
					' Error
					_StatusString = "Error getting Expected Returns."

					Return False
				End If

				' Get Lower and Upper Limits

				If (pConstraintWorker.OptimiserUpperAndLowerLimits(LowerLimit, UpperLimit, NumberOfRealSecurities, NumberOfSlackVariables, NumberOfConstraints, pRunUnconstrained) < 0) Then
					' Error
					_StatusString = "Error getting Upper And Lower Limits."

					Return False
				End If

				' Get Correlation Matrix

				If (pConstraintWorker.OptimiserCovarianceMatrix(CovarianceMatrix, NumberOfRealSecurities, NumberOfSlackVariables, NumberOfConstraints) < 0) Then
					' Error
					_StatusString = "Error getting Covariance Matrix."

					Return False
				End If

			End SyncLock

			' Dimension Other arrays.

			InVars = New Cset(NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1)
			OutVars = New Cset(NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1)

			PortfolioWeight = Array.CreateInstance(GetType(Double), (NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1))
			State = Array.CreateInstance(GetType(VariableStress), (NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1))
			ConstraintCoefficientInverse = Array.CreateInstance(GetType(Double), (NumberOfConstraints + 1), (NumberOfConstraints + 1))

			_OptimiserInitialised = True

		Catch ex As Exception
			_OptimiserInitialised = False

			_StatusString = ex.Message

			Return False
		End Try

		Return _OptimiserInitialised

	End Function

#End Region

#Region " Run Optimiser"

	Public Function Optimise() As Integer
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim CLA_Count As Integer = 0
		Dim rc As SimplexErrorCode

		Try

			' Validate

			If (NumberOfRealSecurities <= 0) Then
				Return (-1)
			End If

			If (Not OptimiserInitialised) Then
				Return (-1)
			End If

			' OK, Start the optimisation process....

			rc = RunSimplex()

			If rc <> SimplexErrorCode.SimplexOK Then

				'<M6> fatal error in Simplex
				If rc = SimplexErrorCode.SimplexInfeasible Then
					MsgBox("Infeasible problem. Check constraints and limits.", vbExclamation)
				ElseIf rc = SimplexErrorCode.SimplexUnboundedE Then
					MsgBox("Unbounded E. Make sure you have a valid budget constraint.", vbExclamation)
				ElseIf rc = SimplexErrorCode.SimplexDegenerate Then
					'Message box was already displayed by Simplex.Run
				End If

				Return 0 ' CLA_Count = 0

				Exit Function

			End If

			' Initialise Debug Arrays

			If (SaveDebugInfo) Then
				ReDim Debug_PortfolioWeight(MaxCPs)
				ReDim Debug_AlphaV(MaxCPs)
				ReDim Debug_BetaV(MaxCPs)
				ReDim Debug_StaticVars(MaxCPs)
				ReDim Debug_InVars(MaxCPs)
				ReDim Debug_OutVars(MaxCPs)
				ReDim Debug_BBar(MaxCPs)
				ReDim Debug_Mi(MaxCPs)
				ReDim Debug_ExpectedReturns(MaxCPs)

			End If

			' VariableCount may now include Simplex created ABVs (artificial basis variables)

			CLASetup()														 '<M7> set up critical line algorithm

			'<M8> Trace out the efficient frontier

			Try
				Dim LambdaE As Double
				Dim StaticVars As New CLAIterationVariables
				Dim MarginalIterationBetweenCPs As Integer = 50
				Dim IterationCounter As Integer = 0
				Dim SaveCP As Boolean
				Dim MarginalsMoveCount As Integer

				MarginalsMoveCount = MAX_MarginalsMoveCount

				For CLA_Count = 1 To MaxCPs

					If (_UseMarginalsIteration) Then
						' ****************************************************
						'
						' ****************************************************

						LambdaE = 1
						IterationCounter = 0

						If (CLA_Count = 1) Then
							LambdaE = MarginalsIteration(CLA_Count, False, 1, False)
						Else
							SaveCP = False
							While (IterationCounter < MarginalIterationBetweenCPs)
								IterationCounter += 1
								If (IterationCounter >= MarginalIterationBetweenCPs) Then
									SaveCP = True
								End If

								LambdaE = MarginalsIteration(CLA_Count, True, MarginalsMoveCount, False, SaveCP)

							End While
						End If

						If (LambdaE < ELambdaE) OrElse (LambdaE <= 0) Then
							Exit For
						End If

						If CLA_Count >= MaxCPs Then
							Exit For
						End If

					Else
						' ****************************************************
						'
						' ****************************************************

						LambdaE = Iteration(CLA_Count, StaticVars)

						Try	' See if the corner portfolios are recognised as OK by 'CheckMarginalsLimits'
							Dim RVal As Boolean
							RVal = CheckMarginalsLimits(PortfolioWeight)
							If RVal = False Then
								RVal = RVal
							End If
						Catch ex As Exception
						End Try

						If (LambdaE < ELambdaE) OrElse (LambdaE <= 0) Then
							Exit For
						End If

						If CLA_Count >= MaxCPs Then
							Exit For
						End If

					End If

				Next CLA_Count

			Catch ex As Exception

			End Try

			_CornerPortfolioCount = CLA_Count
			_OptimiserCompleted = True

		Catch ex As Exception
			_StatusString &= (vbCrLf & ex.Message)
			_OptimiserCompleted = False
			CLA_Count = (-1)
		End Try

		Return CLA_Count

	End Function

#End Region

#Region " Return Data"

	Public Function GetOptimisationData(Optional ByVal ReturnParameters As Boolean = True, Optional ByVal ReturnResults As Boolean = True) As OptimiserDataClass
		' *****************************************************************************************
		' Save Current Optimiser Data to an OptimiserDataClass Class object.
		'
		' *****************************************************************************************

		Dim RVal As New OptimiserDataClass

		Try
			RVal.OptimiserInitialised = OptimiserInitialised
			RVal.OptimiserCompleted = OptimiserCompleted
			RVal.Flag_NeverAllowZeroWeightedStocksBackIn = Flag_NeverAllowZeroWeightedStocksBackIn

			If (RVal.InstrumentNumerator Is Nothing) Then
				RVal.InstrumentNumerator = New Dictionary(Of Integer, Integer)
			ElseIf (RVal.InstrumentNumerator.Count > 0) Then
				RVal.InstrumentNumerator.Clear()
			End If

			If (_InstrumentNumerator.Keys.Count > 0) Then
				Dim InstrumentCounter As Integer
				Dim ThisInstrumentID As Integer
				Dim InstrumentIDs(_InstrumentNumerator.Keys.Count - 1) As Integer

				_InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)

				For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)
					ThisInstrumentID = InstrumentIDs(InstrumentCounter)

					RVal.InstrumentNumerator.Add(ThisInstrumentID, _InstrumentNumerator(ThisInstrumentID))
				Next
			End If

			RVal.NumberOfRealSecurities = NumberOfRealSecurities
			RVal.NumberOfSlackVariables = NumberOfSlackVariables
			RVal.NumberOfConstraints = NumberOfConstraints
			RVal.ELambdaE = ELambdaE
			RVal.MaxCPs = MaxCPs
			RVal.VariableCount = VariableCount

			RVal.StatusString = StatusString
			RVal.SaveDebugInfo = SaveDebugInfo
			RVal.Flag_NeverAllowZeroWeightedStocksBackIn = Flag_NeverAllowZeroWeightedStocksBackIn

		Catch ex As Exception
		End Try

		Try

			' **********************************************************************************************
			'
			' **********************************************************************************************

			If (ReturnParameters) Then

				' ExpectedReturns
				If (ExpectedReturns IsNot Nothing) AndAlso (ExpectedReturns.Length > 0) Then
					ReDim RVal.ExpectedReturns(ExpectedReturns.Length - 1)

					Array.Copy(ExpectedReturns, RVal.ExpectedReturns, ExpectedReturns.Length)
				Else
					ReDim RVal.ExpectedReturns(-1)
				End If

				' StdErrors
				If (StdErrors IsNot Nothing) AndAlso (StdErrors.Length > 0) Then
					ReDim RVal.StdErrors(StdErrors.Length - 1)

					Array.Copy(StdErrors, RVal.StdErrors, StdErrors.Length)
				Else
					ReDim RVal.StdErrors(-1)
				End If

				' LowerLimit
				If (LowerLimit IsNot Nothing) AndAlso (LowerLimit.Length > 0) Then
					ReDim RVal.LowerLimit(LowerLimit.Length - 1)

					Array.Copy(LowerLimit, RVal.LowerLimit, LowerLimit.Length)
				Else
					ReDim RVal.LowerLimit(-1)
				End If

				' UpperLimit
				If (UpperLimit IsNot Nothing) AndAlso (UpperLimit.Length > 0) Then
					ReDim RVal.UpperLimit(UpperLimit.Length - 1)

					Array.Copy(UpperLimit, RVal.UpperLimit, UpperLimit.Length)
				Else
					ReDim RVal.UpperLimit(-1)
				End If

				' ConstraintCoefficient
				If (ConstraintCoefficient IsNot Nothing) AndAlso (ConstraintCoefficient.Rank = 2) Then
					If (ConstraintCoefficient.GetLength(0) > 0) AndAlso (ConstraintCoefficient.GetLength(1) > 0) Then
						ReDim RVal.ConstraintCoefficient(ConstraintCoefficient.GetLength(0) - 1, ConstraintCoefficient.GetLength(1) - 1)

						Array.Copy(ConstraintCoefficient, RVal.ConstraintCoefficient, ConstraintCoefficient.Length)
					End If
				End If

				' ConstraintLimit
				If (ConstraintLimit IsNot Nothing) AndAlso (ConstraintLimit.Length > 0) Then
					ReDim RVal.ConstraintLimit(ConstraintLimit.Length - 1)

					Array.Copy(ConstraintLimit, RVal.ConstraintLimit, ConstraintLimit.Length)
				Else
					ReDim RVal.ConstraintLimit(-1)
				End If

				' MarginalsConstraintArray
				If (MarginalsConstraintArray IsNot Nothing) AndAlso (MarginalsConstraintArray.Rank = 2) Then
					If (MarginalsConstraintArray.GetLength(0) > 0) AndAlso (MarginalsConstraintArray.GetLength(1) > 0) Then
						ReDim RVal.MarginalsConstraintArray(MarginalsConstraintArray.GetLength(0) - 1, MarginalsConstraintArray.GetLength(1) - 1)

						Array.Copy(MarginalsConstraintArray, RVal.MarginalsConstraintArray, MarginalsConstraintArray.Length)
					End If
				End If

				' CovarianceMatrix
				If (CovarianceMatrix IsNot Nothing) AndAlso (CovarianceMatrix.Rank = 2) Then
					If (CovarianceMatrix.GetLength(0) > 0) AndAlso (CovarianceMatrix.GetLength(1) > 0) Then
						ReDim RVal.CovarianceMatrix(CovarianceMatrix.GetLength(0) - 1, CovarianceMatrix.GetLength(1) - 1)

						Array.Copy(CovarianceMatrix, RVal.CovarianceMatrix, CovarianceMatrix.Length)
					End If
				End If

				RVal.ParametersSet = True
			End If

			' **********************************************************************************************
			'
			' **********************************************************************************************

			If (ReturnResults) Then

				RVal.SkipDegenerate = SkipDegenerate
				RVal.SimplexWasDegenerate = _SimplexWasDegenerate
				RVal.CornerPortfolioCount = CornerPortfolioCount

				' CP_Number(-1) As Integer
				If (OutCPNum IsNot Nothing) AndAlso (OutCPNum.Length > 0) Then
					ReDim RVal.CP_Number(OutCPNum.Length - 1)

					Array.Copy(OutCPNum, RVal.CP_Number, OutCPNum.Length)
				Else
					ReDim RVal.CP_Number(-1)
				End If

				' CP_ExpectedReturn(-1) As Double
				If (OutE IsNot Nothing) AndAlso (OutE.Length > 0) Then
					ReDim RVal.CP_ExpectedReturn(OutE.Length - 1)

					Array.Copy(OutE, RVal.CP_ExpectedReturn, OutE.Length)
				Else
					ReDim RVal.CP_ExpectedReturn(-1)
				End If

				' CP_StandardDeviation(-1) As Double
				If (OutSD IsNot Nothing) AndAlso (OutSD.Length > 0) Then
					ReDim RVal.CP_StandardDeviation(OutSD.Length - 1)

					Array.Copy(OutSD, RVal.CP_StandardDeviation, OutSD.Length)
				Else
					ReDim RVal.CP_StandardDeviation(-1)
				End If

				' CP_LambdaE(-1) As Double
				If (OutLambdaE IsNot Nothing) AndAlso (OutLambdaE.Length > 0) Then
					ReDim RVal.CP_LambdaE(OutLambdaE.Length - 1)

					Array.Copy(OutLambdaE, RVal.CP_LambdaE, OutLambdaE.Length)
				Else
					ReDim RVal.CP_LambdaE(-1)
				End If

				' CP_WeightingArray(-1) As Array
				If (OutX IsNot Nothing) AndAlso (OutX.Length > 0) Then
					ReDim RVal.CP_WeightingArray(OutX.Length - 1)

					Dim CPCounter As Integer

					For CPCounter = 0 To (OutX.Length - 1)
						If (OutX(CPCounter) IsNot Nothing) Then
							RVal.CP_WeightingArray(CPCounter) = Array.CreateInstance(GetType(Double), OutX(CPCounter).Length)

							Array.Copy(OutX(CPCounter), RVal.CP_WeightingArray(CPCounter), OutX(CPCounter).Length)
						Else
							RVal.CP_WeightingArray(CPCounter) = Nothing
						End If

					Next

				Else
					ReDim RVal.CP_WeightingArray(-1)
				End If

				Try
					If (StockBannedList IsNot Nothing) AndAlso (StockBannedList.Count > 0) Then
						Dim ArrayCounter As Integer

						If (RVal.StockBannedList IsNot Nothing) Then
							RVal.StockBannedList.Clear()
						Else
							RVal.StockBannedList = New ArrayList
						End If

						For ArrayCounter = 0 To (StockBannedList.Count - 1)
							RVal.StockBannedList.Add(StockBannedList(ArrayCounter))
						Next

					Else
						If (RVal.StockBannedList IsNot Nothing) Then
							RVal.StockBannedList.Clear()
						End If
					End If
				Catch ex As Exception
				End Try

				RVal.ResultsSet = True
			End If

			' **********************************************************************************************
			'
			' **********************************************************************************************

			If (SaveDebugInfo) Then
				Dim Counter As Integer

				'Public SaveDebugInfo As Boolean
				'Public Debug_PortfolioWeight() As Array
				If (Debug_PortfolioWeight IsNot Nothing) AndAlso (Debug_PortfolioWeight.Length > 0) Then
					ReDim RVal.Debug_PortfolioWeight(Debug_PortfolioWeight.Length - 1)

					For Counter = 0 To (Debug_PortfolioWeight.Length - 1)
						If (Debug_PortfolioWeight(Counter) IsNot Nothing) Then
							RVal.Debug_PortfolioWeight(Counter) = Array.CreateInstance(GetType(Double), Debug_PortfolioWeight(Counter).Length)

							Array.Copy(Debug_PortfolioWeight(Counter), RVal.Debug_PortfolioWeight(Counter), Debug_PortfolioWeight(Counter).Length)
						Else
							RVal.Debug_PortfolioWeight(Counter) = Nothing
						End If

					Next
				Else
					ReDim RVal.Debug_PortfolioWeight(-1)
				End If

				'Public Debug_ExpectedReturns() As Array
				If (Debug_ExpectedReturns IsNot Nothing) AndAlso (Debug_ExpectedReturns.Length > 0) Then
					ReDim RVal.Debug_ExpectedReturns(Debug_ExpectedReturns.Length - 1)

					For Counter = 0 To (Debug_ExpectedReturns.Length - 1)
						If (Debug_ExpectedReturns(Counter) IsNot Nothing) Then
							RVal.Debug_ExpectedReturns(Counter) = Array.CreateInstance(GetType(Double), Debug_ExpectedReturns(Counter).Length)

							Array.Copy(Debug_ExpectedReturns(Counter), RVal.Debug_ExpectedReturns(Counter), Debug_ExpectedReturns(Counter).Length)
						Else
							RVal.Debug_ExpectedReturns(Counter) = Nothing
						End If

					Next
				Else
					ReDim RVal.Debug_ExpectedReturns(-1)
				End If

				'Public SaveDebugInfo As Boolean
				'Public Debug_BBar() As Array
				If (Debug_BBar IsNot Nothing) AndAlso (Debug_BBar.Length > 0) Then
					ReDim RVal.Debug_BBar(Debug_BBar.Length - 1)

					For Counter = 0 To (Debug_BBar.Length - 1)
						If (Debug_BBar(Counter) IsNot Nothing) Then
							RVal.Debug_BBar(Counter) = Array.CreateInstance(GetType(Double), Debug_BBar(Counter).Length)

							Array.Copy(Debug_BBar(Counter), RVal.Debug_BBar(Counter), Debug_BBar(Counter).Length)
						Else
							RVal.Debug_BBar(Counter) = Nothing
						End If

					Next

				Else
					ReDim RVal.Debug_BBar(-1)
				End If

				'Public Debug_AlphaV() As Array
				If (Debug_AlphaV IsNot Nothing) AndAlso (Debug_AlphaV.Length > 0) Then
					ReDim RVal.Debug_AlphaV(Debug_AlphaV.Length - 1)

					For Counter = 0 To (Debug_AlphaV.Length - 1)
						If (Debug_AlphaV(Counter) IsNot Nothing) Then
							RVal.Debug_AlphaV(Counter) = Array.CreateInstance(GetType(Double), Debug_AlphaV(Counter).Length)

							Array.Copy(Debug_AlphaV(Counter), RVal.Debug_AlphaV(Counter), Debug_AlphaV(Counter).Length)
						Else
							RVal.Debug_AlphaV(Counter) = Nothing
						End If

					Next

				Else
					ReDim RVal.Debug_AlphaV(-1)
				End If

				'Public Debug_BetaV() As Array
				If (Debug_BetaV IsNot Nothing) AndAlso (Debug_BetaV.Length > 0) Then
					ReDim RVal.Debug_BetaV(Debug_BetaV.Length - 1)

					For Counter = 0 To (Debug_BetaV.Length - 1)
						If (Debug_BetaV(Counter) IsNot Nothing) Then
							RVal.Debug_BetaV(Counter) = Array.CreateInstance(GetType(Double), Debug_BetaV(Counter).Length)

							Array.Copy(Debug_BetaV(Counter), RVal.Debug_BetaV(Counter), Debug_BetaV(Counter).Length)
						Else
							RVal.Debug_BetaV(Counter) = Nothing
						End If

					Next

				Else
					ReDim RVal.Debug_BetaV(-1)
				End If

				' ReDim Debug_InVars(MaxCPs)
				' ReDim Debug_OutVars(MaxCPs)
				If (Debug_InVars IsNot Nothing) AndAlso (Debug_InVars.Length > 0) Then
					ReDim RVal.Debug_InVars(Debug_InVars.Length - 1)

					For Counter = 0 To (Debug_InVars.Length - 1)
						If (Debug_InVars(Counter) IsNot Nothing) Then
							RVal.Debug_InVars(Counter) = Array.CreateInstance(GetType(Double), Debug_InVars(Counter).Length)

							Array.Copy(Debug_InVars(Counter), RVal.Debug_InVars(Counter), Debug_InVars(Counter).Length)
						Else
							RVal.Debug_InVars(Counter) = Nothing
						End If

					Next

				End If

				If (Debug_OutVars IsNot Nothing) AndAlso (Debug_OutVars.Length > 0) Then
					ReDim RVal.Debug_OutVars(Debug_OutVars.Length - 1)

					For Counter = 0 To (Debug_OutVars.Length - 1)
						If (Debug_OutVars(Counter) IsNot Nothing) Then
							RVal.Debug_OutVars(Counter) = Array.CreateInstance(GetType(Double), Debug_OutVars(Counter).Length)

							Array.Copy(Debug_OutVars(Counter), RVal.Debug_OutVars(Counter), Debug_OutVars(Counter).Length)
						Else
							RVal.Debug_OutVars(Counter) = Nothing
						End If

					Next

				End If

				'Private Debug_StaticVars() As CLAIterationVariables
				If (Debug_StaticVars IsNot Nothing) AndAlso (Debug_StaticVars.Length > 0) Then
					ReDim RVal.Debug_StaticVars(Debug_StaticVars.Length - 1)

					For Counter = 0 To (Debug_StaticVars.Length - 1)
						If (Debug_StaticVars(Counter) IsNot Nothing) Then
							RVal.Debug_StaticVars(Counter) = New CLAIterationVariables(Debug_StaticVars(Counter))
						Else
							RVal.Debug_StaticVars(Counter) = Nothing
						End If
					Next

				Else
					ReDim RVal.Debug_StaticVars(-1)
				End If

				'Private Debug_StaticVars() As CLAIterationVariables
				If (Debug_Mi IsNot Nothing) AndAlso (Debug_Mi.Length > 0) Then
					ReDim RVal.Debug_Mi(Debug_Mi.Length - 1)

					For Counter = 0 To (Debug_Mi.Length - 1)
						If (Debug_Mi(Counter) IsNot Nothing) Then

							RVal.Debug_Mi(Counter) = Array.CreateInstance(GetType(Double), Debug_Mi(Counter).GetLength(0), Debug_Mi(Counter).GetLength(1))
							Array.Copy(CovarianceMatrix, RVal.Debug_Mi(Counter), Debug_Mi(Counter).Length)

						End If
					Next
				End If


			End If

		Catch ex As Exception

		End Try

		Return RVal

	End Function

#End Region

End Class
