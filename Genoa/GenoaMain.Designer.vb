<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GenoaMain
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GenoaMain))
    Me.GenoaMenu = New System.Windows.Forms.MenuStrip
    Me.Menu_File = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_File_ToggleMDI = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_EnableReportFadeIn = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Small = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Medium = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_Large = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Font_XLarge = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_Close = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_StaticData = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Edit_Groups = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Edit_GroupMembers = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Edit_CustomFields = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_CustomFieldData = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_FundBrowser = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_FundSearch = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MarketData = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MarketData_Definitions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MarketData_Information = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MarketData_Performance = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_UpdatePertracDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Optimisation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Optimise_ExpectedReturns = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Optimise_SetCovarianceMatrix = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Optimise_Optimise = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_About = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Reports = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_InformationReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_ComparisonReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_CompetitorGroupsReport = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Rpt_TradeHistory = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KnowledgeDate = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KD_SetKD = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_KD_Refresh = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_KD_Display = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Windows = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_UserPermissions = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_System_ReviewCC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SelectCC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SetMAC = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_SetExpiryDate = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_System_UpdateDatabase = New System.Windows.Forms.ToolStripMenuItem
    Me.GenoaStatusStrip = New System.Windows.Forms.StatusStrip
    Me.MainProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.GenoaStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.Button_Debug = New System.Windows.Forms.Button
    Me.DsCompetitorGroupReport1 = New Genoa.dsCompetitorGroupReport
    Me.Panel_Debug = New System.Windows.Forms.Panel
    Me.Menu_Rpt_GroupAttributions = New System.Windows.Forms.ToolStripMenuItem
    Me.GenoaMenu.SuspendLayout()
    Me.GenoaStatusStrip.SuspendLayout()
    CType(Me.DsCompetitorGroupReport1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel_Debug.SuspendLayout()
    Me.SuspendLayout()
    '
    'GenoaMenu
    '
    Me.GenoaMenu.AllowMerge = False
    Me.GenoaMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File, Me.Menu_StaticData, Me.Menu_MarketData, Me.Menu_Optimisation, Me.Menu_About, Me.Menu_Reports, Me.Menu_KnowledgeDate, Me.Menu_Windows, Me.Menu_System})
    Me.GenoaMenu.Location = New System.Drawing.Point(0, 0)
    Me.GenoaMenu.Name = "GenoaMenu"
    Me.GenoaMenu.Size = New System.Drawing.Size(811, 24)
    Me.GenoaMenu.TabIndex = 0
    Me.GenoaMenu.Text = "GenoaMenu"
    '
    'Menu_File
    '
    Me.Menu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File_ToggleMDI, Me.ToolStripSeparator1, Me.Menu_File_EnableReportFadeIn, Me.Menu_Font, Me.ToolStripSeparator7, Me.Menu_File_Close})
    Me.Menu_File.Name = "Menu_File"
    Me.Menu_File.Size = New System.Drawing.Size(35, 20)
    Me.Menu_File.Text = "&File"
    '
    'Menu_File_ToggleMDI
    '
    Me.Menu_File_ToggleMDI.Name = "Menu_File_ToggleMDI"
    Me.Menu_File_ToggleMDI.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_ToggleMDI.Text = "Toggle &MDI"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(191, 6)
    '
    'Menu_File_EnableReportFadeIn
    '
    Me.Menu_File_EnableReportFadeIn.Name = "Menu_File_EnableReportFadeIn"
    Me.Menu_File_EnableReportFadeIn.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_EnableReportFadeIn.Text = "Enable Report Fade-In"
    '
    'Menu_Font
    '
    Me.Menu_Font.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Font_Small, Me.Menu_Font_Medium, Me.Menu_Font_Large, Me.Menu_Font_XLarge})
    Me.Menu_Font.Enabled = False
    Me.Menu_Font.Name = "Menu_Font"
    Me.Menu_Font.Size = New System.Drawing.Size(194, 22)
    Me.Menu_Font.Text = "Font Size"
    '
    'Menu_Font_Small
    '
    Me.Menu_Font_Small.Name = "Menu_Font_Small"
    Me.Menu_Font_Small.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Small.Text = "Small (6)"
    '
    'Menu_Font_Medium
    '
    Me.Menu_Font_Medium.Checked = True
    Me.Menu_Font_Medium.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_Font_Medium.Name = "Menu_Font_Medium"
    Me.Menu_Font_Medium.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Medium.Text = "Medium (8)"
    '
    'Menu_Font_Large
    '
    Me.Menu_Font_Large.Name = "Menu_Font_Large"
    Me.Menu_Font_Large.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_Large.Text = "Large (10)"
    '
    'Menu_Font_XLarge
    '
    Me.Menu_Font_XLarge.Name = "Menu_Font_XLarge"
    Me.Menu_Font_XLarge.Size = New System.Drawing.Size(144, 22)
    Me.Menu_Font_XLarge.Text = "X Large (12)"
    '
    'ToolStripSeparator7
    '
    Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
    Me.ToolStripSeparator7.Size = New System.Drawing.Size(191, 6)
    '
    'Menu_File_Close
    '
    Me.Menu_File_Close.Name = "Menu_File_Close"
    Me.Menu_File_Close.Size = New System.Drawing.Size(194, 22)
    Me.Menu_File_Close.Text = "&Close"
    '
    'Menu_StaticData
    '
    Me.Menu_StaticData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Edit_Groups, Me.Menu_Edit_GroupMembers, Me.ToolStripSeparator5, Me.Menu_Edit_CustomFields, Me.Menu_Static_CustomFieldData, Me.ToolStripSeparator4, Me.Menu_FundBrowser, Me.Menu_FundSearch})
    Me.Menu_StaticData.Name = "Menu_StaticData"
    Me.Menu_StaticData.Size = New System.Drawing.Size(72, 20)
    Me.Menu_StaticData.Text = "&Static Data"
    '
    'Menu_Edit_Groups
    '
    Me.Menu_Edit_Groups.Name = "Menu_Edit_Groups"
    Me.Menu_Edit_Groups.Size = New System.Drawing.Size(210, 22)
    Me.Menu_Edit_Groups.Tag = "frmGroupList"
    Me.Menu_Edit_Groups.Text = "&Groups"
    '
    'Menu_Edit_GroupMembers
    '
    Me.Menu_Edit_GroupMembers.Name = "Menu_Edit_GroupMembers"
    Me.Menu_Edit_GroupMembers.Size = New System.Drawing.Size(210, 22)
    Me.Menu_Edit_GroupMembers.Tag = "frmGroupMembers"
    Me.Menu_Edit_GroupMembers.Text = "Group &Members"
    '
    'ToolStripSeparator5
    '
    Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
    Me.ToolStripSeparator5.Size = New System.Drawing.Size(207, 6)
    '
    'Menu_Edit_CustomFields
    '
    Me.Menu_Edit_CustomFields.Name = "Menu_Edit_CustomFields"
    Me.Menu_Edit_CustomFields.Size = New System.Drawing.Size(210, 22)
    Me.Menu_Edit_CustomFields.Tag = "frmCustomFields"
    Me.Menu_Edit_CustomFields.Text = "&Custom Fields"
    '
    'Menu_Static_CustomFieldData
    '
    Me.Menu_Static_CustomFieldData.Name = "Menu_Static_CustomFieldData"
    Me.Menu_Static_CustomFieldData.Size = New System.Drawing.Size(210, 22)
    Me.Menu_Static_CustomFieldData.Tag = "frmCustomDataMaintenance"
    Me.Menu_Static_CustomFieldData.Text = "Update Custom Field Data"
    '
    'ToolStripSeparator4
    '
    Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
    Me.ToolStripSeparator4.Size = New System.Drawing.Size(207, 6)
    '
    'Menu_FundBrowser
    '
    Me.Menu_FundBrowser.Name = "Menu_FundBrowser"
    Me.Menu_FundBrowser.Size = New System.Drawing.Size(210, 22)
    Me.Menu_FundBrowser.Tag = "frmFundBrowser"
    Me.Menu_FundBrowser.Text = "Fund Browser"
    '
    'Menu_FundSearch
    '
    Me.Menu_FundSearch.Name = "Menu_FundSearch"
    Me.Menu_FundSearch.Size = New System.Drawing.Size(210, 22)
    Me.Menu_FundSearch.Tag = "frmFundSearch"
    Me.Menu_FundSearch.Text = "Fund Search"
    '
    'Menu_MarketData
    '
    Me.Menu_MarketData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_MarketData_Definitions, Me.Menu_MarketData_Information, Me.Menu_MarketData_Performance, Me.ToolStripMenuItem2, Me.Menu_UpdatePertracDatabaseToolStripMenuItem})
    Me.Menu_MarketData.Name = "Menu_MarketData"
    Me.Menu_MarketData.Size = New System.Drawing.Size(78, 20)
    Me.Menu_MarketData.Text = "Market Data"
    '
    'Menu_MarketData_Definitions
    '
    Me.Menu_MarketData_Definitions.Name = "Menu_MarketData_Definitions"
    Me.Menu_MarketData_Definitions.Size = New System.Drawing.Size(207, 22)
    Me.Menu_MarketData_Definitions.Tag = "frmMasterName"
    Me.Menu_MarketData_Definitions.Text = "Instrument Definitions"
    '
    'Menu_MarketData_Information
    '
    Me.Menu_MarketData_Information.Name = "Menu_MarketData_Information"
    Me.Menu_MarketData_Information.Size = New System.Drawing.Size(207, 22)
    Me.Menu_MarketData_Information.Tag = "frmInformation"
    Me.Menu_MarketData_Information.Text = "Instrument Details"
    '
    'Menu_MarketData_Performance
    '
    Me.Menu_MarketData_Performance.Name = "Menu_MarketData_Performance"
    Me.Menu_MarketData_Performance.Size = New System.Drawing.Size(207, 22)
    Me.Menu_MarketData_Performance.Tag = "frmPerformance"
    Me.Menu_MarketData_Performance.Text = "Instrument Return Series"
    '
    'ToolStripMenuItem2
    '
    Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
    Me.ToolStripMenuItem2.Size = New System.Drawing.Size(204, 6)
    '
    'Menu_UpdatePertracDatabaseToolStripMenuItem
    '
    Me.Menu_UpdatePertracDatabaseToolStripMenuItem.Name = "Menu_UpdatePertracDatabaseToolStripMenuItem"
    Me.Menu_UpdatePertracDatabaseToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
    Me.Menu_UpdatePertracDatabaseToolStripMenuItem.Tag = "frmUpdatePertrac"
    Me.Menu_UpdatePertracDatabaseToolStripMenuItem.Text = "Update &Pertrac Database"
    '
    'Menu_Optimisation
    '
    Me.Menu_Optimisation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Optimise_ExpectedReturns, Me.Menu_Optimise_SetCovarianceMatrix, Me.Menu_Optimise_Optimise})
    Me.Menu_Optimisation.Name = "Menu_Optimisation"
    Me.Menu_Optimisation.Size = New System.Drawing.Size(78, 20)
    Me.Menu_Optimisation.Text = "&Optimisation"
    '
    'Menu_Optimise_ExpectedReturns
    '
    Me.Menu_Optimise_ExpectedReturns.Name = "Menu_Optimise_ExpectedReturns"
    Me.Menu_Optimise_ExpectedReturns.Size = New System.Drawing.Size(221, 22)
    Me.Menu_Optimise_ExpectedReturns.Tag = "frmGroupReturns"
    Me.Menu_Optimise_ExpectedReturns.Text = "&Set Group Returns and Data"
    '
    'Menu_Optimise_SetCovarianceMatrix
    '
    Me.Menu_Optimise_SetCovarianceMatrix.Name = "Menu_Optimise_SetCovarianceMatrix"
    Me.Menu_Optimise_SetCovarianceMatrix.Size = New System.Drawing.Size(221, 22)
    Me.Menu_Optimise_SetCovarianceMatrix.Tag = "frmGroupCovariance"
    Me.Menu_Optimise_SetCovarianceMatrix.Text = "Set &Covariance Matrix"
    '
    'Menu_Optimise_Optimise
    '
    Me.Menu_Optimise_Optimise.Name = "Menu_Optimise_Optimise"
    Me.Menu_Optimise_Optimise.Size = New System.Drawing.Size(221, 22)
    Me.Menu_Optimise_Optimise.Tag = "frmOptimise"
    Me.Menu_Optimise_Optimise.Text = "Optimise"
    '
    'Menu_About
    '
    Me.Menu_About.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.Menu_About.Name = "Menu_About"
    Me.Menu_About.Size = New System.Drawing.Size(48, 20)
    Me.Menu_About.Tag = "frmAbout"
    Me.Menu_About.Text = "&About"
    '
    'Menu_Reports
    '
    Me.Menu_Reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_InformationReport, Me.Menu_Rpt_ComparisonReport, Me.Menu_Rpt_CompetitorGroupsReport, Me.ToolStripSeparator9, Me.Menu_Rpt_TradeHistory, Me.Menu_Rpt_GroupAttributions})
    Me.Menu_Reports.Name = "Menu_Reports"
    Me.Menu_Reports.Size = New System.Drawing.Size(52, 20)
    Me.Menu_Reports.Text = "&Report"
    '
    'Menu_Rpt_InformationReport
    '
    Me.Menu_Rpt_InformationReport.Name = "Menu_Rpt_InformationReport"
    Me.Menu_Rpt_InformationReport.Size = New System.Drawing.Size(211, 22)
    Me.Menu_Rpt_InformationReport.Tag = "frmInformationReport"
    Me.Menu_Rpt_InformationReport.Text = "&Information Report"
    '
    'Menu_Rpt_ComparisonReport
    '
    Me.Menu_Rpt_ComparisonReport.Name = "Menu_Rpt_ComparisonReport"
    Me.Menu_Rpt_ComparisonReport.Size = New System.Drawing.Size(211, 22)
    Me.Menu_Rpt_ComparisonReport.Tag = "frmGroupReport"
    Me.Menu_Rpt_ComparisonReport.Text = "&Comparison Report"
    '
    'Menu_Rpt_CompetitorGroupsReport
    '
    Me.Menu_Rpt_CompetitorGroupsReport.Name = "Menu_Rpt_CompetitorGroupsReport"
    Me.Menu_Rpt_CompetitorGroupsReport.Size = New System.Drawing.Size(211, 22)
    Me.Menu_Rpt_CompetitorGroupsReport.Tag = "frmCompetitorGroupsReport"
    Me.Menu_Rpt_CompetitorGroupsReport.Text = "Competitor Groups Report"
    '
    'ToolStripSeparator9
    '
    Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
    Me.ToolStripSeparator9.Size = New System.Drawing.Size(208, 6)
    '
    'Menu_Rpt_TradeHistory
    '
    Me.Menu_Rpt_TradeHistory.Name = "Menu_Rpt_TradeHistory"
    Me.Menu_Rpt_TradeHistory.Size = New System.Drawing.Size(211, 22)
    Me.Menu_Rpt_TradeHistory.Tag = "frmTradeHistory"
    Me.Menu_Rpt_TradeHistory.Text = "Trade History"
    '
    'Menu_KnowledgeDate
    '
    Me.Menu_KnowledgeDate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_KD_SetKD, Me.ToolStripSeparator2, Me.Menu_KD_Refresh, Me.ToolStripSeparator3, Me.Menu_KD_Display})
    Me.Menu_KnowledgeDate.Name = "Menu_KnowledgeDate"
    Me.Menu_KnowledgeDate.Size = New System.Drawing.Size(94, 20)
    Me.Menu_KnowledgeDate.Text = "&KnowledgeDate"
    '
    'Menu_KD_SetKD
    '
    Me.Menu_KD_SetKD.Name = "Menu_KD_SetKD"
    Me.Menu_KD_SetKD.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_SetKD.Tag = "frmSetKnowledgeDate"
    Me.Menu_KD_SetKD.Text = "&Set KnowledgeDate"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(176, 6)
    '
    'Menu_KD_Refresh
    '
    Me.Menu_KD_Refresh.Name = "Menu_KD_Refresh"
    Me.Menu_KD_Refresh.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_Refresh.Text = "&Refresh All Tables"
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(176, 6)
    '
    'Menu_KD_Display
    '
    Me.Menu_KD_Display.Name = "Menu_KD_Display"
    Me.Menu_KD_Display.Size = New System.Drawing.Size(179, 22)
    Me.Menu_KD_Display.Text = " "
    '
    'Menu_Windows
    '
    Me.Menu_Windows.Name = "Menu_Windows"
    Me.Menu_Windows.Size = New System.Drawing.Size(62, 20)
    Me.Menu_Windows.Text = "&Windows"
    '
    'Menu_System
    '
    Me.Menu_System.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_System_UserPermissions, Me.ToolStripSeparator6, Me.Menu_System_ReviewCC, Me.Menu_System_SelectCC, Me.Menu_System_SetMAC, Me.Menu_System_SetExpiryDate, Me.ToolStripSeparator8, Me.Menu_System_UpdateDatabase})
    Me.Menu_System.Name = "Menu_System"
    Me.Menu_System.Size = New System.Drawing.Size(54, 20)
    Me.Menu_System.Text = "&System"
    '
    'Menu_System_UserPermissions
    '
    Me.Menu_System_UserPermissions.Name = "Menu_System_UserPermissions"
    Me.Menu_System_UserPermissions.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_UserPermissions.Tag = "frmUserPermissions"
    Me.Menu_System_UserPermissions.Text = "User &Permissions"
    '
    'ToolStripSeparator6
    '
    Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
    Me.ToolStripSeparator6.Size = New System.Drawing.Size(229, 6)
    '
    'Menu_System_ReviewCC
    '
    Me.Menu_System_ReviewCC.Name = "Menu_System_ReviewCC"
    Me.Menu_System_ReviewCC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_ReviewCC.Tag = "frmChangeControlReview"
    Me.Menu_System_ReviewCC.Text = "Add / Review &Change Controls"
    '
    'Menu_System_SelectCC
    '
    Me.Menu_System_SelectCC.Name = "Menu_System_SelectCC"
    Me.Menu_System_SelectCC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SelectCC.Tag = "frmSelectChangeControl"
    Me.Menu_System_SelectCC.Text = "&Select Change Controls"
    '
    'Menu_System_SetMAC
    '
    Me.Menu_System_SetMAC.Name = "Menu_System_SetMAC"
    Me.Menu_System_SetMAC.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SetMAC.Text = "Set MAC"
    '
    'Menu_System_SetExpiryDate
    '
    Me.Menu_System_SetExpiryDate.Name = "Menu_System_SetExpiryDate"
    Me.Menu_System_SetExpiryDate.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_SetExpiryDate.Text = "Set Expiry Date"
    '
    'ToolStripSeparator8
    '
    Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
    Me.ToolStripSeparator8.Size = New System.Drawing.Size(229, 6)
    '
    'Menu_System_UpdateDatabase
    '
    Me.Menu_System_UpdateDatabase.Name = "Menu_System_UpdateDatabase"
    Me.Menu_System_UpdateDatabase.Size = New System.Drawing.Size(232, 22)
    Me.Menu_System_UpdateDatabase.Text = "Update Database"
    '
    'GenoaStatusStrip
    '
    Me.GenoaStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MainProgressBar, Me.GenoaStatusLabel})
    Me.GenoaStatusStrip.Location = New System.Drawing.Point(0, 124)
    Me.GenoaStatusStrip.Name = "GenoaStatusStrip"
    Me.GenoaStatusStrip.Size = New System.Drawing.Size(811, 22)
    Me.GenoaStatusStrip.TabIndex = 1
    Me.GenoaStatusStrip.Text = "GenoaStatusStrip"
    '
    'MainProgressBar
    '
    Me.MainProgressBar.Maximum = 20
    Me.MainProgressBar.Name = "MainProgressBar"
    Me.MainProgressBar.Size = New System.Drawing.Size(150, 16)
    Me.MainProgressBar.Step = 1
    Me.MainProgressBar.Visible = False
    '
    'GenoaStatusLabel
    '
    Me.GenoaStatusLabel.Name = "GenoaStatusLabel"
    Me.GenoaStatusLabel.Size = New System.Drawing.Size(0, 17)
    '
    'Button_Debug
    '
    Me.Button_Debug.Location = New System.Drawing.Point(43, 35)
    Me.Button_Debug.Name = "Button_Debug"
    Me.Button_Debug.Size = New System.Drawing.Size(50, 23)
    Me.Button_Debug.TabIndex = 2
    Me.Button_Debug.Text = "Debug"
    Me.Button_Debug.UseVisualStyleBackColor = True
    '
    'DsCompetitorGroupReport1
    '
    Me.DsCompetitorGroupReport1.DataSetName = "dsCompetitorGroupReport"
    Me.DsCompetitorGroupReport1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
    '
    'Panel_Debug
    '
    Me.Panel_Debug.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Debug.Controls.Add(Me.Button_Debug)
    Me.Panel_Debug.Enabled = False
    Me.Panel_Debug.Location = New System.Drawing.Point(12, 36)
    Me.Panel_Debug.Name = "Panel_Debug"
    Me.Panel_Debug.Size = New System.Drawing.Size(787, 85)
    Me.Panel_Debug.TabIndex = 23
    Me.Panel_Debug.Visible = False
    '
    'Menu_Rpt_GroupAttributions
    '
    Me.Menu_Rpt_GroupAttributions.Name = "Menu_Rpt_GroupAttributions"
    Me.Menu_Rpt_GroupAttributions.Size = New System.Drawing.Size(211, 22)
    Me.Menu_Rpt_GroupAttributions.Tag = "frmAttributionsReports"
    Me.Menu_Rpt_GroupAttributions.Text = "Group Attributions"
    '
    'GenoaMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(811, 146)
    Me.Controls.Add(Me.Panel_Debug)
    Me.Controls.Add(Me.GenoaStatusStrip)
    Me.Controls.Add(Me.GenoaMenu)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "GenoaMain"
    Me.Text = "Genoa"
    Me.GenoaMenu.ResumeLayout(False)
    Me.GenoaMenu.PerformLayout()
    Me.GenoaStatusStrip.ResumeLayout(False)
    Me.GenoaStatusStrip.PerformLayout()
    CType(Me.DsCompetitorGroupReport1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel_Debug.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents GenoaMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Menu_File As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_File_ToggleMDI As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_File_Close As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents GenoaStatusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents GenoaStatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Menu_KnowledgeDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_KD_SetKD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Refresh As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_About As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Display As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Reports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_StaticData As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_UserPermissions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Optimisation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MainProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Menu_Windows As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Edit_Groups As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Edit_GroupMembers As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_FundBrowser As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_InformationReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_ComparisonReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Optimise_ExpectedReturns As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Optimise_SetCovarianceMatrix As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_FundSearch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Edit_CustomFields As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_System_ReviewCC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_System_SelectCC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Static_CustomFieldData As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_Small As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Font_Medium As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_Large As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Font_XLarge As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Optimise_Optimise As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_CompetitorGroupsReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Button_Debug As System.Windows.Forms.Button
  Friend WithEvents Menu_MarketData As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_MarketData_Definitions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_MarketData_Information As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_MarketData_Performance As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents DsCompetitorGroupReport1 As Genoa.dsCompetitorGroupReport
  Friend WithEvents Menu_System_SetMAC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_SetExpiryDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Panel_Debug As System.Windows.Forms.Panel
  Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_System_UpdateDatabase As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_UpdatePertracDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_File_EnableReportFadeIn As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Rpt_TradeHistory As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_GroupAttributions As System.Windows.Forms.ToolStripMenuItem

End Class
