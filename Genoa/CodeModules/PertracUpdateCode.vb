Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceGlobals

Module PertracUpdateCode

	Public Function UpdatePertracPerformance(ByRef Mainform As GenoaMain, ByVal pSourceAccessDB As String, ByVal PertracID As Integer, ByVal AccessHasPriority As Boolean, ByVal DeleteSpareRenaissanceReturns As Boolean, ByVal DontUpdateJustCount As Boolean, ByVal StatusLabel As ToolStripStatusLabel) As Integer
		' ************************************************************************************************
		'
		'
		' ************************************************************************************************

		Dim RVal As Integer = 0
		Dim AccessConnection As OleDb.OleDbConnection = Nothing
		Dim AccessCommand As OleDb.OleDbCommand = Nothing
		Dim AccessReader As OleDb.OleDbDataReader = Nothing

		Dim RenaissanceConnection As SqlConnection = Nothing
		Dim RenaissanceCommand As SqlCommand = Nothing
		Dim RenaissanceReader As SqlDataReader = Nothing
		Dim RenaissanceAdaptor As New SqlDataAdapter

		Dim SqlOrdinalID As Integer
		Dim SqlOrdinalDate As Integer
		Dim SqlOrdinalReturn As Integer
		Dim SqlOrdinalFundsManaged As Integer
		Dim SqlOrdinalNAV As Integer
		Dim SqlOrdinalEstimate As Integer
		Dim SqlReaderValues(6) As Object

		Dim SqlID As Integer
		Dim SqlDate As Date
		Dim SqlReturn As Double
		Dim SqlFundsManaged As Double
		Dim SqlNAV As Double
		Dim SqlEstimate As Boolean

		Dim PerformanceDataset As RenaissanceDataClass.DSPerformance
		Dim PerformanceTable As RenaissanceDataClass.DSPerformance.tblPerformanceDataTable
		Dim PerformanceAdaptor As SqlDataAdapter
		Dim ThisPerformanceRow As RenaissanceDataClass.DSPerformance.tblPerformanceRow

		Dim AccessOrdinalID As Integer
		Dim AccessOrdinalDate As Integer
		Dim AccessOrdinalReturn As Integer
		Dim AccessOrdinalFundsManaged As Integer
		Dim AccessOrdinalNAV As Integer
		Dim AccessOrdinalEstimate As Integer

		Dim AccessID As Integer
		Dim AccessDate As Date
		Dim AccessReturn As Double
		Dim AccessFundsManaged As Double
		Dim AccessNAV As Double
		Dim AccessEstimate As Boolean

		Dim AccessReaderValues(6) As Object

		Try

			If (StatusLabel IsNot Nothing) Then
				StatusLabel.Text = "Opening Databases"
				StatusLabel.Owner.Refresh()
				Application.DoEvents()
			End If

			' Open Renaissance Connection 

			If (PertracID > 0) Then
				RenaissanceCommand = New SqlCommand("Select [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate] FROM [MASTERSQL].[dbo].[Performance] WHERE [ID] = " & PertracID.ToString & " ORDER BY [ID], [Date]", Nothing)
			Else
				RenaissanceCommand = New SqlCommand("Select [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate] FROM [MASTERSQL].[dbo].[Performance] ORDER BY [ID], [Date]", Nothing)
			End If
      RenaissanceCommand.Connection = Mainform.GetGenoaConnection
      RenaissanceCommand.CommandTimeout = 600
			RenaissanceReader = RenaissanceCommand.ExecuteReader

			RenaissanceConnection = Mainform.GetGenoaConnection
			Mainform.MainAdaptorHandler.Set_AdaptorCommands(RenaissanceConnection, RenaissanceAdaptor, RenaissanceStandardDatasets.Performance.TableName)

			' Get Renaissance Ordinals 

			SqlOrdinalID = RenaissanceReader.GetOrdinal("ID")
			SqlOrdinalDate = RenaissanceReader.GetOrdinal("Date")
			SqlOrdinalReturn = RenaissanceReader.GetOrdinal("Return")
			SqlOrdinalFundsManaged = RenaissanceReader.GetOrdinal("FundsManaged")
			SqlOrdinalNAV = RenaissanceReader.GetOrdinal("NAV")
			SqlOrdinalEstimate = RenaissanceReader.GetOrdinal("Estimate")

			' Open Access DB

			AccessConnection = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; " + "Data Source=" + pSourceAccessDB)
      AccessConnection.Open()

			If (PertracID > 0) Then
				AccessCommand = New OleDbCommand("Select [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate] FROM Performance WHERE ID = " & PertracID.ToString & " ORDER BY [ID], [Date]", AccessConnection)
			Else
				AccessCommand = New OleDbCommand("Select [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate] FROM Performance WHERE ID IN (SELECT ID FROM Information WHERE (DataVendorName <> 'User')) ORDER BY [ID], [Date]", AccessConnection)
			End If

      AccessCommand.CommandTimeout = 600
			AccessReader = AccessCommand.ExecuteReader

			' Get Access Ordinals 

			AccessOrdinalID = AccessReader.GetOrdinal("ID")
			AccessOrdinalDate = AccessReader.GetOrdinal("Date")
			AccessOrdinalReturn = AccessReader.GetOrdinal("Return")
			AccessOrdinalFundsManaged = AccessReader.GetOrdinal("FundsManaged")
			AccessOrdinalNAV = AccessReader.GetOrdinal("NAV")
			AccessOrdinalEstimate = AccessReader.GetOrdinal("Estimate")

			PerformanceAdaptor = New SqlDataAdapter
			Mainform.MainAdaptorHandler.Set_AdaptorCommands(RenaissanceConnection, PerformanceAdaptor, RenaissanceStandardDatasets.Performance.TableName)
			PerformanceDataset = New DSPerformance
			PerformanceTable = PerformanceDataset.tblPerformance

			Dim MoreAccessRows As Boolean = AccessReader.HasRows
			Dim MoreSQLRows As Boolean = RenaissanceReader.HasRows
			Dim ReadAccess As Boolean = AccessReader.HasRows
			Dim ReadSQL As Boolean = RenaissanceReader.HasRows

			' Process Returns

			If (StatusLabel IsNot Nothing) Then
				StatusLabel.Text = "Processing Returns..."
				StatusLabel.Owner.Refresh()
				Application.DoEvents()
			End If

			While True


				If (Not MoreAccessRows) Then

					If (AccessID >= 0) Then
						AccessID = (-1)
					End If

				ElseIf (ReadAccess) Then

					MoreAccessRows = AccessReader.Read

					If (MoreAccessRows) Then

						AccessReader.GetValues(AccessReaderValues)

						AccessID = CInt(AccessReaderValues(AccessOrdinalID))
						AccessDate = CDate(AccessReaderValues(AccessOrdinalDate))
						AccessReturn = CDbl(AccessReaderValues(AccessOrdinalReturn))
						AccessFundsManaged = CDbl(AccessReaderValues(AccessOrdinalFundsManaged))
						AccessNAV = CDbl(AccessReaderValues(AccessOrdinalNAV))
						AccessEstimate = CBool(AccessReaderValues(AccessOrdinalEstimate))

					End If
				End If

				ReadAccess = False


				If (Not MoreSQLRows) Then

					If (SqlID >= 0) Then
						SqlID = (-1)
					End If

				ElseIf (ReadSQL) Then

					MoreSQLRows = RenaissanceReader.Read

					If (MoreSQLRows) Then

						RenaissanceReader.GetValues(SqlReaderValues)

						SqlID = CInt(SqlReaderValues(SqlOrdinalID))
						SqlDate = CDate(SqlReaderValues(SqlOrdinalDate))
						SqlReturn = CDbl(SqlReaderValues(SqlOrdinalReturn))
						SqlFundsManaged = CDbl(SqlReaderValues(SqlOrdinalFundsManaged))
						SqlNAV = CDbl(SqlReaderValues(SqlOrdinalNAV))
						SqlEstimate = CBool(SqlReaderValues(SqlOrdinalEstimate))

					End If

				End If

				ReadSQL = False

				' Test and process returns

				If (SqlID < 0) AndAlso (AccessID < 0) Then
					' Reached the end of both Readers.

					Exit While

				ElseIf (AccessID < 0) OrElse ((SqlID >= 0) AndAlso (SqlID < AccessID)) Then
					' Reached the end of the Access Reader OR 
					' Appear to have a renaissance return not present in the Access DB

					ReadSQL = True

					If (DeleteSpareRenaissanceReturns) Then

						Try
							RVal += 1

							If (Not DontUpdateJustCount) Then
								PerformanceAdaptor.DeleteCommand.Parameters("@ID").Value = SqlID
								PerformanceAdaptor.DeleteCommand.Parameters("@Date").Value = SqlDate

								PerformanceAdaptor.DeleteCommand.ExecuteNonQuery()
							End If

						Catch ex As Exception
							Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)
						End Try

					End If

				ElseIf (SqlID < 0) OrElse ((AccessID >= 0) AndAlso (SqlID > AccessID)) Then
					' Reached the end of the RenaissanceReader OR 
					' Appear to have an Access return not in Renaissance - Add it

					ReadAccess = True

					Try
						RVal += 1

						If (Not DontUpdateJustCount) Then
							ThisPerformanceRow = PerformanceTable.NewtblPerformanceRow

							ThisPerformanceRow.ID = AccessID
							ThisPerformanceRow.PerformanceDate = AccessDate
							ThisPerformanceRow.PerformanceReturn = AccessReturn
							ThisPerformanceRow.NAV = AccessNAV
							ThisPerformanceRow.FundsManaged = AccessFundsManaged
							ThisPerformanceRow.Estimate = AccessEstimate

							PerformanceTable.Rows.Add(ThisPerformanceRow)
						End If

					Catch ex As Exception
						Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)
					End Try

				Else ' IDs are the same

					If (SqlDate = AccessDate) Then

						ReadAccess = True
						ReadSQL = True

						' Has Access got priority and has the record changed ?

						If (AccessHasPriority) AndAlso ((Math.Abs(AccessReturn - SqlReturn) > 0.000001) OrElse (Math.Abs(AccessNAV - SqlNAV) > 0.1) OrElse (Math.Abs(AccessFundsManaged - SqlFundsManaged) > 0.1) OrElse (AccessEstimate <> SqlEstimate)) Then

							Try
								RVal += 1

								If (Not DontUpdateJustCount) Then
									ThisPerformanceRow = PerformanceTable.NewtblPerformanceRow

									ThisPerformanceRow.ID = AccessID
									ThisPerformanceRow.PerformanceDate = AccessDate
									ThisPerformanceRow.PerformanceReturn = AccessReturn
									ThisPerformanceRow.NAV = AccessNAV
									ThisPerformanceRow.FundsManaged = AccessFundsManaged
									ThisPerformanceRow.Estimate = AccessEstimate

									PerformanceTable.Rows.Add(ThisPerformanceRow)
								End If

							Catch ex As Exception
								Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)
							End Try

						End If

					ElseIf (SqlDate < AccessDate) Then

						' Appear to have a renaissance return not present in the Access DB

						ReadSQL = True

						If (DeleteSpareRenaissanceReturns) Then

							Try
								RVal += 1

								If (Not DontUpdateJustCount) Then
									PerformanceAdaptor.DeleteCommand.Parameters("@ID").Value = SqlID
									PerformanceAdaptor.DeleteCommand.Parameters("@Date").Value = SqlDate

									PerformanceAdaptor.DeleteCommand.ExecuteNonQuery()
								End If

							Catch ex As Exception
								Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)
							End Try

						End If

					Else ' (SqlDate > AccessDate) Then

						' Appear to have an Access return not in Renaissance - Add it

						ReadAccess = True

						Try
							RVal += 1

							If (Not DontUpdateJustCount) Then
								ThisPerformanceRow = PerformanceTable.NewtblPerformanceRow

								ThisPerformanceRow.ID = AccessID
								ThisPerformanceRow.PerformanceDate = AccessDate
								ThisPerformanceRow.PerformanceReturn = AccessReturn
								ThisPerformanceRow.NAV = AccessNAV
								ThisPerformanceRow.FundsManaged = AccessFundsManaged
								ThisPerformanceRow.Estimate = AccessEstimate

								PerformanceTable.Rows.Add(ThisPerformanceRow)
							End If

						Catch ex As Exception
							Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)
						End Try

					End If
				End If


				If (PerformanceTable.Rows.Count >= 2000) Then

					If (StatusLabel IsNot Nothing) Then
						StatusLabel.Text = "[" & RVal.ToString & "] Processing Returns..."
						StatusLabel.Owner.Refresh()
						Application.DoEvents()
					End If

					If (Not DontUpdateJustCount) Then
						RenaissanceAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
						RenaissanceAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

						Mainform.MainDataHandler.AdaptorUpdate(RenaissanceAdaptor, PerformanceTable)
						' Mainform.AdaptorUpdate("UpdatePertracPerformance()", RenaissanceAdaptor, PerformanceTable)
						PerformanceTable.Rows.Clear()

						PerformanceDataset = New DSPerformance
						PerformanceTable = PerformanceDataset.tblPerformance
					End If
				End If

			End While

			If (Not DontUpdateJustCount) AndAlso (PerformanceTable.Rows.Count > 0) Then
				RenaissanceAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
				RenaissanceAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

				Mainform.MainDataHandler.AdaptorUpdate(RenaissanceAdaptor, PerformanceTable)
				PerformanceTable.Rows.Clear()

			End If

		Catch ex As Exception
			Mainform.LogError("UpdatePertracPerformance()", LOG_LEVELS.Error, ex.Message, "Error in UpdatePertracPerformance()", ex.StackTrace, True)

		Finally

			Try

				If (StatusLabel IsNot Nothing) Then
					StatusLabel.Text = "Tidying up...."
					StatusLabel.Owner.Refresh()
					Application.DoEvents()
				End If

			Catch ex As Exception
			End Try

			' Tidy Renaissance Connection

			Try
				If (RenaissanceReader IsNot Nothing) Then
					RenaissanceReader.Close()
				End If
			Catch ex As Exception
			Finally
				RenaissanceReader = Nothing
			End Try

			Try
				If (RenaissanceCommand IsNot Nothing) Then
					If (RenaissanceCommand.Connection IsNot Nothing) Then
						RenaissanceCommand.Connection.Close()
					End If
				End If
			Catch ex As Exception
			Finally
				RenaissanceCommand.Connection = Nothing
				RenaissanceCommand = Nothing
			End Try

			Try
				If (RenaissanceConnection IsNot Nothing) Then
					RenaissanceConnection.Close()
				End If
			Catch ex As Exception
			Finally
				RenaissanceConnection = Nothing
			End Try

			Try
				If (RenaissanceAdaptor IsNot Nothing) Then
					If (RenaissanceAdaptor.SelectCommand IsNot Nothing) Then
						RenaissanceAdaptor.SelectCommand.Connection = Nothing
					End If
					If (RenaissanceAdaptor.DeleteCommand IsNot Nothing) Then
						RenaissanceAdaptor.DeleteCommand.Connection = Nothing
					End If
					If (RenaissanceAdaptor.InsertCommand IsNot Nothing) Then
						RenaissanceAdaptor.InsertCommand.Connection = Nothing
					End If
					If (RenaissanceAdaptor.UpdateCommand IsNot Nothing) Then
						RenaissanceAdaptor.UpdateCommand.Connection = Nothing
					End If
				End If

			Catch ex As Exception
			Finally
				RenaissanceAdaptor = Nothing
			End Try


			' Tidy Access Connection
			Try
				If (AccessConnection IsNot Nothing) Then
					If (AccessReader IsNot Nothing) Then
						AccessReader.Close()
					End If

					AccessConnection.Close()
				End If
			Catch ex As Exception
			Finally
				AccessReader = Nothing
				AccessCommand = Nothing
				AccessConnection = Nothing
			End Try
		End Try


		Try
			' Post Update Message

			If (RVal > 0) AndAlso (Not DontUpdateJustCount) Then

				If (StatusLabel IsNot Nothing) Then
					StatusLabel.Text = "Posting Update message"
					StatusLabel.Owner.Refresh()
					Application.DoEvents()
				End If

				Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs
				UpdateMessage.TableChanged(RenaissanceChangeID.Performance) = True
				If (PertracID > 0) Then
					UpdateMessage.UpdateDetail(RenaissanceChangeID.Performance) = PertracID.ToString
				End If

				Call Mainform.Main_RaiseEvent(UpdateMessage)
				Application.DoEvents()

			End If

		Catch ex As Exception
		End Try

		Try
			If (StatusLabel IsNot Nothing) Then
				If (DontUpdateJustCount) Then
					StatusLabel.Text = RVal.ToString & " returns to update."
				Else
					StatusLabel.Text = RVal.ToString & " returns updated."
				End If
				StatusLabel.Owner.Refresh()
				Application.DoEvents()
			End If
		Catch ex As Exception
		End Try

		Return RVal

	End Function


End Module
