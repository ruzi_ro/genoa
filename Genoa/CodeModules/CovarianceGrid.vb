Imports RenaissanceStatFunctions
Imports Genoa.MaxFunctions
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

' Imports Genoa.DatePeriodFunctions

Module CovarianceGrid

  Public Function GetFullCovarianceMatrix(ByRef pMainForm As GenoaMain, ByRef StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByRef ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef pListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable, ByRef ScalingFactorHash As Dictionary(Of Integer, Double), ByVal BackfillPriceData As Boolean, ByVal MatchBackfillVolatility As Boolean, ByVal pLamda As Double, ByVal DFrom As Date, ByVal DTo As Date, ByVal pShowCovariance As Boolean, ByVal pLoadFromTable As Boolean, ByVal pCovListID As Integer, ByRef pStatusLabel As ToolStripLabel) As Integer
    ' ***************************************************************************************************
    '
    '
    '
    ' ***************************************************************************************************

    If (pListTable Is Nothing) OrElse (pListTable.Rows.Count <= 0) Then
      ThisGrid.Rows.Count = 1
      ThisGrid.Cols.Count = 1
    End If

    Dim RVal_MissingInstruments As Integer = 0

    Dim SelectedPertracIDs(pListTable.Rows.Count - 1) As Integer
    Dim SelectedIndexPertracIDs(pListTable.Rows.Count - 1) As Integer
    Dim PertracCount As Integer
    Dim RowIndex As Integer
    Dim ColIndex As Integer
    Dim Lamda As Double
    Dim Pertrac1 As Integer
    Dim Pertrac1_BackFill As Integer
    Dim Pertrac2 As Integer
    Dim Pertrac2_BackFill As Integer

    Dim PertracHash As Dictionary(Of Integer, Integer) = Nothing

    Try
      ThisGrid.Redraw = False

      PertracCount = pListTable.Rows.Count

      Lamda = pLamda

      ThisGrid.Rows.Count = (PertracCount + 1)
      ThisGrid.Cols.Count = (PertracCount + 2)

      Try
        If (ThisGrid.Rows.Count > 1) Then
          ThisGrid.Clear(C1.Win.C1FlexGrid.ClearFlags.Content, 1, 1, ThisGrid.Rows.Count - 1, ThisGrid.Cols.Count - 2)
        End If
      Catch ex As Exception
      End Try

      ThisGrid.Cols(PertracCount + 1).Visible = False
      ThisGrid.Cols(PertracCount + 1).DataType = GetType(Integer)

      Dim ThisListRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim ThisComparison As StatFunctions.ComparisonStatsClass = Nothing
      Dim MissingInstrumentsCheck(PertracCount) As Boolean

      'If (pLoadFromTable) Then
      ' Pertrac Hash is used when loading a Grid from the tblCovariance.
      ' It allows Genoa to quickly map PertracIDs to Grid coordinates.

      PertracHash = New Dictionary(Of Integer, Integer)
      If (ScalingFactorHash Is Nothing) Then
        ScalingFactorHash = New Dictionary(Of Integer, Double)
      End If

      ThisGrid.Tag = PertracHash
      'End If

      ' Initialise Grid and Pertrac Hash

      Try

        For RowIndex = 1 To PertracCount
          Try
            ThisListRow = pListTable.Rows(RowIndex - 1)
            MissingInstrumentsCheck(RowIndex) = True

            SelectedPertracIDs(RowIndex - 1) = ThisListRow.GroupPertracCode
            SelectedIndexPertracIDs(RowIndex - 1) = ThisListRow.GroupIndexCode

            If (Not ScalingFactorHash.ContainsKey(ThisListRow.GroupPertracCode)) Then
              ScalingFactorHash.Add(ThisListRow.GroupPertracCode, 1.0#) ' Sets default values
            End If

            'If (pLoadFromTable) Then
            PertracHash.Add(ThisListRow.GroupPertracCode, RowIndex)
            'End If

            ThisGrid.Item(RowIndex, 0) = pListTable.Rows(RowIndex - 1)("PertracName")
            ThisGrid.Item(0, RowIndex) = pListTable.Rows(RowIndex - 1)("PertracName")

            ThisGrid.Item(RowIndex, PertracCount + 1) = ThisListRow.GroupPertracCode

            ThisGrid.Cols(RowIndex).AllowEditing = False
            ThisGrid.Cols(RowIndex).AllowMerging = False

            If (pShowCovariance) Then
              ThisGrid.Cols(RowIndex).Width = 65
            Else
              ThisGrid.Cols(RowIndex).Width = 50
            End If

            ThisGrid.Cols(RowIndex).Visible = True
            ThisGrid.Cols(RowIndex).DataType = GetType(Double)

          Catch ex As Exception
          End Try
        Next

      Catch ex As Exception

      End Try

      ' Not Loading From tblCovariance

      If (Not pLoadFromTable) Then
        Try
          pStatusLabel.Text = "Loading Pertrac Returns Series (x" & SelectedPertracIDs.Length.ToString & ")"
          Application.DoEvents()

          pMainForm.PertracData.LoadPertracTables(SelectedPertracIDs)
        Catch ex As Exception
        End Try
      End If

      pStatusLabel.Text = ""

      Dim PositiveStyle As C1.Win.C1FlexGrid.CellStyle = ThisGrid.Styles("Positive")
      Dim NegativeStyle As C1.Win.C1FlexGrid.CellStyle = ThisGrid.Styles("Negative")
      Dim BlackStyle As C1.Win.C1FlexGrid.CellStyle = ThisGrid.Styles("Black")

      Try

        ' Set Styles if they do not exist

        If (PositiveStyle Is Nothing) Then
          PositiveStyle = ThisGrid.Styles.Add("Positive")
          PositiveStyle.ForeColor = Color.Blue
          PositiveStyle.Format = "#,##0.0000"
        End If

        If (NegativeStyle Is Nothing) Then
          NegativeStyle = ThisGrid.Styles.Add("Negative")
          NegativeStyle.ForeColor = Color.Red
          NegativeStyle.Format = "#,##0.0000"
        End If

        If (BlackStyle Is Nothing) Then
          BlackStyle = ThisGrid.Styles.Add("Black")
          BlackStyle.ForeColor = Color.Blue
          BlackStyle.Format = "#,##0.0000"
          BlackStyle.Font = New Font(BlackStyle.Font, FontStyle.Bold)
        End If

        ' Set numeric precision appropriate to Covariance or Correlation.

        If (pShowCovariance) Then
          PositiveStyle.Format = "#,##0.0000000"
          NegativeStyle.Format = "#,##0.0000000"
        Else
          PositiveStyle.Format = "#,##0.0000"
          NegativeStyle.Format = "#,##0.0000"
        End If

      Catch ex As Exception
      End Try

      Try
        ' Load From tblCovariance

        If (pLoadFromTable) Then
          Dim ThisCovListID As Integer = pCovListID

          ' Check DataPeriod

          Dim CovListRow As DSCovarianceList.tblCovarianceListRow = Nothing

          CovListRow = CType(LookupTableRow(pMainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCovarianceList, ThisCovListID), DSCovarianceList.tblCovarianceListRow)

          If (CovListRow IsNot Nothing) AndAlso (CovListRow.DataPeriod <> CInt(StatsDatePeriod)) Then
            MessageBox.Show("The DataPeriod of the saved Covariance Grid does not match the Native DataPeriod of the Current Group", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
          End If

          ' Get CovTable
          Dim CovDS As DSCovariance
          Dim CovTbl As DSCovariance.tblCovarianceDataTable = Nothing
          Dim SelectedCovRows(-1) As DSCovariance.tblCovarianceRow
          Dim CovRow As DSCovariance.tblCovarianceRow

          CovDS = pMainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovariance, False)
          If (CovDS IsNot Nothing) Then
            CovTbl = CovDS.tblCovariance

            If (CovTbl IsNot Nothing) Then
              SelectedCovRows = CovTbl.Select("CovListID=" & ThisCovListID.ToString, "PertracID1,PertracID2")
            End If
          End If

          If (SelectedCovRows.Length > 0) AndAlso (PertracHash IsNot Nothing) Then
            Try
              pStatusLabel.Text = "Loading Correlations."
              Application.DoEvents()

            Catch ex As Exception
            End Try

            Dim CorrCounter As Integer

            For CorrCounter = 0 To (SelectedCovRows.Length - 1)
              CovRow = SelectedCovRows(CorrCounter)

              If (PertracHash.ContainsKey(CovRow.PertracID1)) AndAlso (PertracHash.ContainsKey(CovRow.PertracID2)) Then
                Pertrac1 = CovRow.PertracID1
                Pertrac2 = CovRow.PertracID2

                RowIndex = CInt(PertracHash(Pertrac1))
                ColIndex = CInt(PertracHash(Pertrac2))

                MissingInstrumentsCheck(RowIndex) = False

                Try
                  If (RowIndex = ColIndex) AndAlso (pShowCovariance = False) Then
                    ThisGrid.Item(RowIndex, ColIndex) = 1

                    ' Format Cell
                    ThisGrid.SetCellStyle(RowIndex, ColIndex, BlackStyle)

                  ElseIf (Pertrac1 = Pertrac2) AndAlso (pShowCovariance = False) Then
                    ThisGrid.Item(RowIndex, ColIndex) = 1
                    ThisGrid.Item(ColIndex, RowIndex) = 1

                    ' Format Cells
                    ThisGrid.SetCellStyle(RowIndex, ColIndex, PositiveStyle)
                    ThisGrid.SetCellStyle(ColIndex, RowIndex, PositiveStyle)

                  Else
                    ThisGrid.SetCellStyle(RowIndex, ColIndex, PositiveStyle)
                    ThisGrid.SetCellStyle(ColIndex, RowIndex, PositiveStyle)

                    If pShowCovariance Then
                      ThisGrid.Item(RowIndex, ColIndex) = CovRow.Covariance
                      ThisGrid.Item(ColIndex, RowIndex) = CovRow.Covariance

                      If (CovRow.Covariance < 0) Then
                        ThisGrid.SetCellStyle(RowIndex, ColIndex, NegativeStyle)
                        ThisGrid.SetCellStyle(ColIndex, RowIndex, NegativeStyle)
                      End If
                    Else
                      ThisGrid.Item(RowIndex, ColIndex) = CovRow.Correlation
                      ThisGrid.Item(ColIndex, RowIndex) = CovRow.Correlation

                      If (CovRow.Correlation < 0) Then
                        ThisGrid.SetCellStyle(RowIndex, ColIndex, NegativeStyle)
                        ThisGrid.SetCellStyle(ColIndex, RowIndex, NegativeStyle)
                      End If
                    End If

                  End If
                Catch ex As Exception

                  ThisGrid.Item(RowIndex, ColIndex) = 0
                  ThisGrid.Item(ColIndex, RowIndex) = 0
                End Try

              End If

            Next

            pStatusLabel.Text = ""

            ' Count missing Instruments : Instruments in the ListTable vs Instruments loaded from the Covariance Table.

            For RowIndex = 1 To PertracCount
              If MissingInstrumentsCheck(RowIndex) Then
                RVal_MissingInstruments += 1
              End If
            Next
          End If

        Else

          ' Load Normally - Calculate covariances.

          Dim PeriodCount As Integer = 12
          Try
            PeriodCount = GetPeriodCount(StatsDatePeriod, DFrom, DTo)
          Catch ex As Exception
          End Try

          For RowIndex = 1 To PertracCount

            Try
              pStatusLabel.Text = "Calculating Correlations for " & ThisGrid.Item(RowIndex, 0)
              Application.DoEvents()

            Catch ex As Exception
            End Try

            If (BackfillPriceData) Then
              Pertrac1 = SelectedPertracIDs(RowIndex - 1)
              Pertrac1_BackFill = SelectedIndexPertracIDs(RowIndex - 1)
            Else
              Pertrac1 = CInt(ThisGrid.Item(RowIndex, ThisGrid.Cols.Count - 1))
            End If

            For ColIndex = 1 To RowIndex

              If (BackfillPriceData) Then
                Pertrac2 = SelectedPertracIDs(ColIndex - 1)
                Pertrac2_BackFill = SelectedIndexPertracIDs(ColIndex - 1)
              Else
                Pertrac2 = CInt(ThisGrid.Item(ColIndex, ThisGrid.Cols.Count - 1))
              End If

              Try
                If (RowIndex = ColIndex) AndAlso (pShowCovariance = False) Then
                  ThisGrid.Item(RowIndex, ColIndex) = 1

                  ' Format Cell
                  ThisGrid.SetCellStyle(RowIndex, ColIndex, BlackStyle)

                ElseIf (Pertrac1 = Pertrac2) AndAlso (pShowCovariance = False) Then
                  ThisGrid.Item(RowIndex, ColIndex) = 1
                  ThisGrid.Item(ColIndex, RowIndex) = 1

                  ' Format Cells

                  ThisGrid.SetCellStyle(RowIndex, ColIndex, PositiveStyle)
                  ThisGrid.SetCellStyle(ColIndex, RowIndex, PositiveStyle)

                Else

                  If (BackfillPriceData) AndAlso ((Pertrac1_BackFill > 0) Or (Pertrac2_BackFill > 0)) Then

                    Dim CombinedPertrac1 As ULong = pMainForm.StatFunctions.CombinePertracIDs(Pertrac1, Pertrac1_BackFill)
                    Dim CombinedPertrac2 As ULong = pMainForm.StatFunctions.CombinePertracIDs(Pertrac2, Pertrac2_BackFill)

                    If (Pertrac1 <= Pertrac2) Then
                      ThisComparison = pMainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CombinedPertrac1, CombinedPertrac2, MatchBackfillVolatility, CombinedPertrac1, DFrom, DTo, Lamda, PeriodCount, -1, ScalingFactorHash(Pertrac1), ScalingFactorHash(Pertrac2))
                    Else
                      ThisComparison = pMainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CombinedPertrac2, CombinedPertrac1, MatchBackfillVolatility, CombinedPertrac2, DFrom, DTo, Lamda, PeriodCount, -1, ScalingFactorHash(Pertrac2), ScalingFactorHash(Pertrac1))
                    End If

                  Else
                    ThisComparison = pMainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CULng(MIN(Pertrac1, Pertrac2)), CULng(MAX(Pertrac1, Pertrac2)), False, CULng(MIN(Pertrac1, Pertrac2)), DFrom, DTo, Lamda, PeriodCount, -1, ScalingFactorHash(CInt(MIN(Pertrac1, Pertrac2))), ScalingFactorHash(CInt(MAX(Pertrac1, Pertrac2))))
                  End If

                  ThisGrid.SetCellStyle(RowIndex, ColIndex, PositiveStyle)
                  ThisGrid.SetCellStyle(ColIndex, RowIndex, PositiveStyle)

                  If (ThisComparison Is Nothing) Then
                    ThisGrid.Item(RowIndex, ColIndex) = 0
                    ThisGrid.Item(ColIndex, RowIndex) = 0

                  Else
                    If pShowCovariance Then
                      ThisGrid.Item(RowIndex, ColIndex) = ThisComparison.Covariance
                      ThisGrid.Item(ColIndex, RowIndex) = ThisComparison.Covariance

                      If (ThisComparison.Covariance < 0) Then
                        ThisGrid.SetCellStyle(RowIndex, ColIndex, NegativeStyle)
                        ThisGrid.SetCellStyle(ColIndex, RowIndex, NegativeStyle)
                      End If
                    Else
                      ThisGrid.Item(RowIndex, ColIndex) = ThisComparison.Correlation
                      ThisGrid.Item(ColIndex, RowIndex) = ThisComparison.Correlation

                      If (ThisComparison.Correlation < 0) Then
                        ThisGrid.SetCellStyle(RowIndex, ColIndex, NegativeStyle)
                        ThisGrid.SetCellStyle(ColIndex, RowIndex, NegativeStyle)
                      End If
                    End If
                  End If

                End If
              Catch ex As Exception
                ThisGrid.Item(RowIndex, ColIndex) = 0
                ThisGrid.Item(ColIndex, RowIndex) = 0
              End Try

            Next
          Next

        End If

      Catch ex As Exception
      End Try

    Catch ex As Exception
    Finally
      ThisGrid.Redraw = True
      ThisGrid.Refresh()
    End Try

    pStatusLabel.Text = ""
    Return RVal_MissingInstruments

  End Function


End Module
