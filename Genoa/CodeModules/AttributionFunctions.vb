﻿Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissancePertracDataClass.PertracDataClass
Imports RenaissanceDataClass
Imports RenaissancePertracAndStatsGlobals
Imports RenaissanceUtilities.DatePeriodFunctions

Module AttributionFunctions

  Friend Class classBriefInstrumentDetails

    Public InstrumentID As Integer
    Public InstrumentDescription As String
    Public InstrumentClass As String
    Public InstrumentType As Integer
    Public InstrumentTypeDescription As String
    Public FundTypeID As Integer
    Public FundTypeDescription As String
    Public FundTypeAttributionGrouping As String
    Public FundTypeIsAdmin As Integer
    Public FundTypeSortOrder As Integer

    Public Sub New()
      InstrumentID = 0
      InstrumentDescription = ""
      InstrumentClass = ""
      InstrumentType = 0
      InstrumentTypeDescription = ""
      FundTypeID = 0
      FundTypeDescription = ""
      FundTypeAttributionGrouping = ""
      FundTypeIsAdmin = 0
      FundTypeSortOrder = 0
    End Sub

  End Class

  Friend Function GetBriefDetails(ByRef MainForm As GenoaMain, ByVal InstrumentID As ULong) As classBriefInstrumentDetails
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim RVal As New classBriefInstrumentDetails
    Dim BasePertracID As Integer = CInt(InstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask)
    Dim thisFlags As RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags
    Dim ThisUnderlyingID As Integer

    Try

      thisFlags = RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(BasePertracID)
      ThisUnderlyingID = RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BasePertracID)

      RVal.InstrumentID = BasePertracID

      Select Case thisFlags

        Case PertracInstrumentFlags.VeniceInstrument

          Dim InstrumentsRow As DSInstrument.tblInstrumentRow
          Dim FundTypeRow As DSFundType.tblFundTypeRow

          Try
            InstrumentsRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisUnderlyingID)

            If (InstrumentsRow IsNot Nothing) Then

              RVal.InstrumentDescription = InstrumentsRow.InstrumentDescription
              RVal.InstrumentClass = InstrumentsRow.InstrumentClass
              RVal.InstrumentType = InstrumentsRow.InstrumentType
              RVal.FundTypeID = InstrumentsRow.InstrumentFundType

              RVal.InstrumentTypeDescription = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrumentType, InstrumentsRow.InstrumentType, "InstrumentTypeDescription"), ""))

              If (RVal.FundTypeID > 0) Then

                FundTypeRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFundType, RVal.FundTypeID)

                If (FundTypeRow IsNot Nothing) Then

                  RVal.FundTypeDescription = FundTypeRow.FundTypeDescription
                  RVal.FundTypeAttributionGrouping = FundTypeRow.FundTypeAttributionGrouping
                  RVal.FundTypeIsAdmin = FundTypeRow.FundTypeIsAdministrativeOnly
                  RVal.FundTypeSortOrder = FundTypeRow.FundTypeSortOrder

                End If

              End If

            End If

          Catch ex As Exception
          Finally
            InstrumentsRow = Nothing
            FundTypeRow = Nothing
          End Try

        Case PertracInstrumentFlags.PertracInstrument

          RVal.InstrumentDescription = CStr(Nz(MainForm.PertracData.GetInformationValue(BasePertracID, PertracInformationFields.FundName), "Pertrac Instrument : " & BasePertracID.ToString))

        Case PertracInstrumentFlags.CTA_Simulation

          Dim SimulationRow As DSCTA_Simulation.tblCTA_SimulationRow

          Try
            SimulationRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, ThisUnderlyingID)

            If (SimulationRow IsNot Nothing) Then

              RVal.InstrumentDescription = SimulationRow.SimulationName

              RVal.InstrumentTypeDescription = "Simulation"

            End If

          Catch ex As Exception
          Finally
            SimulationRow = Nothing
          End Try

        Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted

          Dim GroupRow As DSGroupList.tblGroupListRow

          Try
            GroupRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblGroupList, ThisUnderlyingID)

            If (GroupRow IsNot Nothing) Then

              RVal.InstrumentDescription = GroupRow.GroupListName

              RVal.InstrumentTypeDescription = "Group"

            End If

          Catch ex As Exception
          Finally
            GroupRow = Nothing
          End Try

        Case PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted

          RVal.InstrumentDescription = "Dynamic Group : & " & ThisUnderlyingID.ToString
          RVal.InstrumentTypeDescription = "Group"

        Case PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean

          RVal.InstrumentDescription = "DrillDown Group : & " & ThisUnderlyingID.ToString
          RVal.InstrumentTypeDescription = "Group"

        Case Else

      End Select

    Catch ex As Exception

    End Try

    Return RVal

  End Function

  Friend Function GetAttributionData(ByRef MainForm As GenoaMain, ByVal BySector As Boolean, ByVal InstrumentID As ULong, ByVal ReportDataPeriod As RenaissanceGlobals.DealingPeriod, ByVal StartYear As Integer, ByVal EndYear As Integer, ByVal LastDate As Date, ByVal LookThrough As LookthroughEnum, ByVal pAggregateToParent As Boolean) As DataTable
    ' *************************************************************************************
    ' Create an Attributions array for the given Instrument (expected to be a group) and then return 
    ' a DataTable containing Attributions data presented in the requested DataPeriod.
    '
    ' *************************************************************************************

    Dim RVal As New DataTable
    Dim thisRow As DataRow

    If (BySector) Then
      'spu_ViewAttributionsBySectorReturns() 

      RVal.Columns.Add(New DataColumn("AttribFund", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FundCode", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundName", GetType(String)))
      RVal.Columns.Add(New DataColumn("AttribDate", GetType(Date)))
      RVal.Columns.Add(New DataColumn("AttribPeriod", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FundTypeID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FundTypeDescription", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundTypeAttributionGrouping", GetType(String)))
      RVal.Columns.Add(New DataColumn("SumOfAttribWeight", GetType(Double)))
      RVal.Columns.Add(New DataColumn("FundTypeIsAdmin", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("SectorReturn", GetType(Double)))
      RVal.Columns.Add(New DataColumn("SectorBips", GetType(Double)))
    Else
      'spu_ViewAttributions 

      RVal.Columns.Add(New DataColumn("AttribFund", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("AttribInstrument", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("AttribDate", GetType(Date)))
      RVal.Columns.Add(New DataColumn("AttribPeriod", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("AttribWeight", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribEndWeight", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribProfit", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribPercentReturn", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribBips", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribExpectedBips", GetType(Double)))
      RVal.Columns.Add(New DataColumn("AttribIsLookthrough", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("AttribKnowledgedate", GetType(Date)))
      RVal.Columns.Add(New DataColumn("AttribDateEntered", GetType(Date)))
      RVal.Columns.Add(New DataColumn("FundCode", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundName", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundLegalEntity", GetType(String)))
      RVal.Columns.Add(New DataColumn("InstrumentDescription", GetType(String)))
      RVal.Columns.Add(New DataColumn("InstrumentClass", GetType(String)))
      RVal.Columns.Add(New DataColumn("InstrumentType", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("InstrumentTypeDescription", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundTypeID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FundTypeDescription", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundTypeAttributionGrouping", GetType(String)))
      RVal.Columns.Add(New DataColumn("FundTypeIsAdministrativeOnly", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FundTypeSortOrder", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("YTDAttribBips", GetType(Double)))
      RVal.Columns.Add(New DataColumn("SortableInstrumentDescription", GetType(String)))

    End If

    ' Hmmm, how do we do this ?
    ' ...

    Dim NativeDataStartDate As Date
    Dim NativeDataEndDate As Date

    ' First Get the Net Delta and Instrument Returns in the Native Data period

    Dim NativeInstrumentReturnSeries(-1) As Array
    Dim NativeInstrumentDateSeries(-1) As Array
    Dim NativeInstrumentNAVSeries() As Double
    Dim ReportInstrumentNAVSeries() As Double
    Dim NativeInstrumentDateOffset(-1) As Integer
    Dim ReportInstrumentReturnSeries(-1, -1) As Double

    Dim ReportAnnualPeriod As Integer = MainForm.PertracData.AnnualPeriodCount(ReportDataPeriod)
    Dim ReportInstrumentDateSeries() As Date
    'Dim InstrumentDateOffset(-1) As Integer

    Dim NativeDataPeriod As DealingPeriod = MainForm.PertracData.GetPertracDataPeriod(CInt(InstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask))
    Dim NativeInstrumentDeltasSeries() As RenaissancePertracAndStatsGlobals.NetDeltaItemClass
    Dim BriefInstrumentDetails(-1) As classBriefInstrumentDetails
    Dim Counter As Integer
    Dim UnderlyingInstrumentID As ULong
    Dim NativeAnnualPeriod As Integer = MainForm.PertracData.AnnualPeriodCount(NativeDataPeriod)
    Dim AttributionsArray(-1, -1) As Double
    Dim InstrumentCounter As Integer
    Dim DateCounter As Integer
    Dim NativeAdjustedDateIndex As Integer
    Dim ReportDateIndex As Integer

    NativeDataStartDate = FitDateToPeriod(NativeDataPeriod, New Date(StartYear, 1, 1), False)
    NativeDataStartDate = AddPeriodToDate(NativeDataPeriod, NativeDataStartDate, -1)
    NativeDataStartDate = FitDateToPeriod(NativeDataPeriod, NativeDataStartDate, False)
    NativeDataEndDate = FitDateToPeriod(NativeDataPeriod, New Date(EndYear, 12, 31), True)
    NativeDataEndDate = AddPeriodToDate(NativeDataPeriod, NativeDataEndDate, 1)
    NativeDataEndDate = FitDateToPeriod(NativeDataPeriod, NativeDataEndDate, False)

    NativeInstrumentDeltasSeries = MainForm.StatFunctions.NetDeltas(NativeDataPeriod, InstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate, LookThrough)

    If (NativeInstrumentDeltasSeries IsNot Nothing) AndAlso (NativeInstrumentDeltasSeries.Length > 0) Then

      '  ' Consolidate to Underlying Instruments.

      '  Dim UnderlyingIDDictionary As New Dictionary(Of ULong, Integer)
      '  Dim InstrumentLookup As New Dictionary(Of ULong, ULong)
      Dim BasePertracID As Integer
      Dim SimBaseID As Object

      '  If (LookThrough = LookthroughEnum.LookthroughToSimulations) Then

      '    Try

      '      ' Create Dictionary linking Pertrac IDs to Underlying IDs
      '      ' Create Dictionary linking UnderlyingIDs to an Array Index.

      '      For Counter = 0 To (NativeInstrumentDeltasSeries.Length - 1)

      '        UnderlyingInstrumentID = NativeInstrumentDeltasSeries(Counter).PertracId

      '        BasePertracID = CInt(UnderlyingInstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask)

      '        If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(CUInt(BasePertracID)) = PertracInstrumentFlags.CTA_Simulation Then

      '          SimBaseID = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BasePertracID), "InstrumentID")

      '          If (SimBaseID IsNot Nothing) AndAlso (IsNumeric(SimBaseID)) Then
      '            UnderlyingInstrumentID = CULng(SimBaseID)
      '          End If

      '        End If

      '        If (Not UnderlyingIDDictionary.ContainsKey(UnderlyingInstrumentID)) Then
      '          UnderlyingIDDictionary.Add(UnderlyingInstrumentID, UnderlyingIDDictionary.Count)
      '        End If

      '        If (Not InstrumentLookup.ContainsKey(NativeInstrumentDeltasSeries(Counter).PertracId)) Then
      '          InstrumentLookup.Add(NativeInstrumentDeltasSeries(Counter).PertracId, UnderlyingInstrumentID)
      '        End If

      '      Next Counter

      '      ' Consolidation required ?

      '      If (UnderlyingIDDictionary.Count > 0) AndAlso (NativeInstrumentDeltasSeries.Length > UnderlyingIDDictionary.Count) Then

      '        Dim TempDeltasSeries(UnderlyingIDDictionary.Count - 1) As RenaissancePertracAndStatsGlobals.NetDeltaItemClass
      '        Dim UnderlyingKeys() As ULong = UnderlyingIDDictionary.Keys.ToArray

      '        ' Dimension Replacement Array

      '        For Counter = 0 To (UnderlyingKeys.Length - 1)
      '          TempDeltasSeries(UnderlyingIDDictionary(UnderlyingKeys(Counter))) = New RenaissancePertracAndStatsGlobals.NetDeltaItemClass(UnderlyingKeys(Counter), CType(Array.CreateInstance(GetType(Double), NativeInstrumentDeltasSeries(0).DeltaArray.Length), Double()))
      '        Next

      '        ' Copy in aggregated Delta values

      '        Dim UnderlyingIndex As Integer

      '        For Counter = 0 To (NativeInstrumentDeltasSeries.Length - 1)

      '          UnderlyingIndex = UnderlyingIDDictionary(InstrumentLookup(NativeInstrumentDeltasSeries(Counter).PertracId))

      '          For DateCounter = 0 To (NativeInstrumentDeltasSeries(0).DeltaArray.Length - 1)

      '            TempDeltasSeries(UnderlyingIndex).DeltaArray(DateCounter) += NativeInstrumentDeltasSeries(Counter).DeltaArray(DateCounter)

      '          Next

      '        Next

      '        ' Replace NativeInstrumentDeltasSeries

      '        NativeInstrumentDeltasSeries = Nothing

      '        NativeInstrumentDeltasSeries = TempDeltasSeries

      '      End If

      '    Catch ex As Exception
      '    End Try

      '  End If

      ' Redimension native Data arrays

      ReDim NativeInstrumentReturnSeries(NativeInstrumentDeltasSeries.Length)
      ReDim NativeInstrumentDateSeries(NativeInstrumentDeltasSeries.Length)
      ReDim NativeInstrumentDateOffset(NativeInstrumentDeltasSeries.Length)
      ReDim BriefInstrumentDetails(NativeInstrumentDeltasSeries.Length)

      ' Get Instrument Data

      NativeInstrumentDateSeries(0) = MainForm.StatFunctions.DateSeries(NativeDataPeriod, InstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)
      NativeInstrumentReturnSeries(0) = MainForm.StatFunctions.ReturnSeries(NativeDataPeriod, InstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)
      NativeInstrumentNAVSeries = MainForm.StatFunctions.NAVSeries(NativeDataPeriod, InstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)

      ReportInstrumentDateSeries = MainForm.StatFunctions.DateSeries(ReportDataPeriod, InstrumentID, False, ReportAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)
      ReportInstrumentNAVSeries = MainForm.StatFunctions.NAVSeries(ReportDataPeriod, InstrumentID, False, ReportAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)

      ReDim ReportInstrumentReturnSeries(NativeInstrumentDeltasSeries.Length - 1, ReportInstrumentDateSeries.Length - 1)

      ' Get Native Underlying Information. 

      For Counter = 0 To (NativeInstrumentDeltasSeries.Length - 1)
        UnderlyingInstrumentID = NativeInstrumentDeltasSeries(Counter).PertracId

        NativeInstrumentDateSeries(Counter + 1) = MainForm.StatFunctions.DateSeries(NativeDataPeriod, UnderlyingInstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)
        NativeInstrumentReturnSeries(Counter + 1) = MainForm.StatFunctions.ReturnSeries(NativeDataPeriod, UnderlyingInstrumentID, False, NativeAnnualPeriod, 1.0#, True, NativeDataStartDate, NativeDataEndDate)

        If (LookThrough = LookthroughEnum.LookthroughToSimulations) Then
          ' Get Instrument Details for Simulation Underlying.

          BasePertracID = CInt(UnderlyingInstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask)

          If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(CUInt(BasePertracID)) = PertracInstrumentFlags.CTA_Simulation Then

            SimBaseID = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BasePertracID), "InstrumentID")

            If (SimBaseID IsNot Nothing) AndAlso (IsNumeric(SimBaseID)) Then
              UnderlyingInstrumentID = CULng(SimBaseID)
            End If

          End If

        End If

        ' If 'AggregateToParentID' andalso Instrument Type is 'Venice instrument', then check for the Parent ID.

        If (pAggregateToParent) Then
          BasePertracID = CInt(UnderlyingInstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask)

          If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(CUInt(BasePertracID)) = PertracInstrumentFlags.VeniceInstrument Then

            SimBaseID = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BasePertracID), "InstrumentParentID")

            If (SimBaseID IsNot Nothing) AndAlso (IsNumeric(SimBaseID)) AndAlso (CInt(SimBaseID) > 0) Then

              UnderlyingInstrumentID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(SimBaseID), PertracInstrumentFlags.VeniceInstrument)

            End If

          End If

        End If

        ' Get Instrument Details for this Instrument ID.

        BriefInstrumentDetails(Counter + 1) = GetBriefDetails(MainForm, UnderlyingInstrumentID)

        If (NativeInstrumentDateSeries(Counter + 1) IsNot Nothing) AndAlso (NativeInstrumentDateSeries(Counter + 1).Length > 0) Then
          NativeInstrumentDateOffset(Counter + 1) = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(NativeDataPeriod, NativeInstrumentDateSeries(Counter + 1)(0), NativeInstrumentDateSeries(0)(0))
        Else
          NativeInstrumentDateOffset(Counter + 1) = 0
        End If

      Next Counter

      ' OK, Now we have Native Delta and Instrument data.

      ' Dimension Final Array in Requested Date Period.

      ReDim AttributionsArray(NativeInstrumentDeltasSeries.Length - 1, ReportInstrumentDateSeries.Length - 1)

      ' Quick pause for Brain Ache .....
      ' OK, here we go ...

      ' Aggregate Data from 'Native' data arrays to the 'Report Periodicity' result array.

      ' I conclude that an instrument's Attribution over a period has to be calculated using a pseudo cash calculation.
      ' When converting from, for example, a daily series to a monthly series the monthly attribution is the sum of 
      ' (Daily Return * Previous day's NAV) / Month Start NAV.
      ' This is the conclusion drawn from the ..\Development\Genoa_2008\GroupProof.xls spreadsheet.
      '

      Dim YesterdaysNAV As Double
      Dim PeriodStartNAV As Double

      For InstrumentCounter = 1 To (NativeInstrumentDeltasSeries.Length - 1)

        For DateCounter = 0 To (NativeInstrumentDateSeries(0).Length - 1)

          NativeAdjustedDateIndex = DateCounter + NativeInstrumentDateOffset(InstrumentCounter)
          ReportDateIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(ReportDataPeriod, ReportInstrumentDateSeries(0), NativeInstrumentDateSeries(0)(DateCounter))

          If (ReportDateIndex <= 0) Then
            PeriodStartNAV = NativeInstrumentNAVSeries(0)
          Else
            PeriodStartNAV = ReportInstrumentNAVSeries(ReportDateIndex - 1)
          End If

          YesterdaysNAV = NativeInstrumentNAVSeries(Math.Max(0, DateCounter - 1))

          If (NativeAdjustedDateIndex >= 0) AndAlso (NativeAdjustedDateIndex < NativeInstrumentReturnSeries(InstrumentCounter).Length) Then
            If (ReportDateIndex >= 0) AndAlso (ReportDateIndex < ReportInstrumentDateSeries.Length) Then
              AttributionsArray(InstrumentCounter, ReportDateIndex) += 100.0# * NativeInstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(DateCounter) * NativeInstrumentReturnSeries(InstrumentCounter)(NativeAdjustedDateIndex) * YesterdaysNAV / PeriodStartNAV

              ' Build Report-Period Instrument Return Series. Used later...
              ReportInstrumentReturnSeries(InstrumentCounter, ReportDateIndex) = ((1.0# + ReportInstrumentReturnSeries(InstrumentCounter, ReportDateIndex)) * (1.0# + NativeInstrumentReturnSeries(InstrumentCounter)(NativeAdjustedDateIndex))) - 1.0#
            End If
          End If

        Next

      Next

      ' Add Data Rows

      Dim FundCode As Integer = InstrumentID
      Dim FundName As String = MainForm.PertracData.GetInformationValue(InstrumentID, RenaissanceGlobals.PertracInformationFields.FundName)

      If (BySector) Then

        ' More_Code_Here()
        '
        ' Not sure that Sector Aggregation is appropriate for Attributions which (mostly) do not relate
        ' to Venice Instruments.
        '

      Else
        Dim YTD_Bips As Double

        For InstrumentCounter = 1 To (NativeInstrumentDeltasSeries.Length - 1)

          For DateCounter = 0 To (ReportInstrumentDateSeries.Length - 1)

            ' Within Data Range ?

            If (StartYear <= 0) OrElse ((CDate(ReportInstrumentDateSeries(DateCounter)).Year >= StartYear) AndAlso (CDate(ReportInstrumentDateSeries(DateCounter)).Year <= EndYear)) Then

              ' On or Before the given 'Latest Date' ?

              If (LastDate <= Renaissance_BaseDate) OrElse (LastDate >= CDate(ReportInstrumentDateSeries(DateCounter))) Then

                ' Reset YTD Bips ?

                If (DateCounter = 0) Then
                  YTD_Bips = 0.0#
                ElseIf (Year(ReportInstrumentDateSeries(DateCounter - 1)) <> Year(ReportInstrumentDateSeries(DateCounter))) Then
                  YTD_Bips = 0.0#
                End If

                ' 

                NativeAdjustedDateIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(NativeDataPeriod, NativeInstrumentDateSeries(InstrumentCounter)(0), ReportInstrumentDateSeries(DateCounter))

                ' Add Report Row.

                Try
                  thisRow = RVal.NewRow

                  thisRow("AttribFund") = 0
                  thisRow("AttribInstrument") = BriefInstrumentDetails(InstrumentCounter).InstrumentID
                  thisRow("AttribDate") = ReportInstrumentDateSeries(DateCounter)
                  thisRow("AttribPeriod") = ReportDataPeriod

                  If (NativeAdjustedDateIndex > 0) AndAlso (NativeAdjustedDateIndex < NativeInstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray.Length) Then

                    thisRow("AttribWeight") = NativeInstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(NativeAdjustedDateIndex)
                    thisRow("AttribEndWeight") = NativeInstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(NativeAdjustedDateIndex)
                    thisRow("AttribBips") = AttributionsArray(InstrumentCounter, DateCounter)
                    thisRow("AttribPercentReturn") = ReportInstrumentReturnSeries(InstrumentCounter, DateCounter)
                    YTD_Bips += AttributionsArray(InstrumentCounter, DateCounter)
                  Else
                    thisRow("AttribWeight") = 0
                    thisRow("AttribEndWeight") = 0
                    thisRow("AttribBips") = 0
                    thisRow("AttribPercentReturn") = 0
                  End If

                  thisRow("YTDAttribBips") = YTD_Bips
                  thisRow("AttribProfit") = 0
                  thisRow("AttribExpectedBips") = 0
                  thisRow("AttribIsLookthrough") = CBool(CInt(LookThrough))
                  thisRow("AttribKnowledgedate") = Renaissance_BaseDate
                  thisRow("AttribDateEntered") = Now.Date
                  thisRow("FundCode") = FundCode
                  thisRow("FundName") = FundName
                  thisRow("FundLegalEntity") = ""
                  thisRow("InstrumentDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentDescription
                  thisRow("InstrumentClass") = BriefInstrumentDetails(InstrumentCounter).InstrumentClass
                  thisRow("InstrumentType") = BriefInstrumentDetails(InstrumentCounter).InstrumentType
                  thisRow("InstrumentTypeDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentTypeDescription
                  thisRow("FundTypeID") = BriefInstrumentDetails(InstrumentCounter).FundTypeID
                  thisRow("FundTypeDescription") = BriefInstrumentDetails(InstrumentCounter).FundTypeDescription
                  thisRow("FundTypeAttributionGrouping") = BriefInstrumentDetails(InstrumentCounter).FundTypeAttributionGrouping
                  thisRow("FundTypeIsAdministrativeOnly") = BriefInstrumentDetails(InstrumentCounter).FundTypeIsAdmin
                  thisRow("FundTypeSortOrder") = BriefInstrumentDetails(InstrumentCounter).FundTypeSortOrder
                  thisRow("SortableInstrumentDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentDescription

                  RVal.Rows.Add(thisRow)
                Catch ex As Exception

                End Try

              End If



            End If

          Next

        Next

      End If

    End If


    '' OK, First, Lets get the Net Delta Data and the Instrument Returns.

    'Dim InstrumentReturnSeries(-1) As Array


    'Dim FundCode As Integer = InstrumentID
    'Dim FundName As String = MainForm.PertracData.GetInformationValue(InstrumentID, RenaissanceGlobals.PertracInformationFields.FundName)


    'Dim InstrumentDeltasSeries() As RenaissancePertracAndStatsGlobals.NetDeltaItemClass

    '' Get Delta Series

    'InstrumentDeltasSeries = MainForm.StatFunctions.NetDeltas(ReportDataPeriod, InstrumentID, False, ReportAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data, LookThrough)

    '' If there is Delta data, then get underlying Instrument data

    'If (InstrumentDeltasSeries IsNot Nothing) AndAlso (InstrumentDeltasSeries.Length > 0) Then

    '  ReDim InstrumentReturnSeries(InstrumentDeltasSeries.Length)
    '  ReDim InstrumentDateSeries(InstrumentDeltasSeries.Length)
    '  ReDim InstrumentDateOffset(InstrumentDeltasSeries.Length)
    '  ReDim BriefInstrumentDetails(InstrumentDeltasSeries.Length)

    '  ' Get Instrument Data / Return Series - Index Zero.

    '  InstrumentDateSeries(0) = MainForm.StatFunctions.DateSeries(ReportDataPeriod, InstrumentID, False, ReportAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
    '  InstrumentReturnSeries(0) = MainForm.StatFunctions.ReturnSeries(ReportDataPeriod, InstrumentID, False, ReportAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)

    '  ' Get Underlying Data / Return Series - Index n + 1.

    '  For Counter = 0 To (InstrumentDeltasSeries.Length - 1)
    '    UnderlyingInstrumentID = InstrumentDeltasSeries(Counter).PertracId

    '    InstrumentDateSeries(Counter + 1) = MainForm.StatFunctions.DateSeries(ReportDataPeriod, UnderlyingInstrumentID, False, ReportAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
    '    InstrumentReturnSeries(Counter + 1) = MainForm.StatFunctions.ReturnSeries(ReportDataPeriod, UnderlyingInstrumentID, False, ReportAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)

    '    If (LookThrough = LookthroughEnum.LookthroughToSimulations) Then
    '      ' Get Instrument Details for Simulation Underlying.

    '      Dim BasePertracID As Integer = CInt(UnderlyingInstrumentID And RenaissancePertracAndStatsGlobals.Globals.Ulong_31BitMask)
    '      Dim SimBaseID As Object

    '      SimBaseID = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BasePertracID), "InstrumentID")

    '      If (SimBaseID IsNot Nothing) AndAlso (IsNumeric(SimBaseID)) Then
    '        UnderlyingInstrumentID = CInt(SimBaseID)
    '      End If

    '    End If

    '    BriefInstrumentDetails(Counter + 1) = GetBriefDetails(UnderlyingInstrumentID)

    '    If (InstrumentDateSeries(Counter + 1) IsNot Nothing) AndAlso (InstrumentDateSeries(Counter + 1).Length > 0) Then
    '      InstrumentDateOffset(Counter + 1) = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(ReportDataPeriod, InstrumentDateSeries(Counter + 1)(0), InstrumentDateSeries(0)(0))
    '    Else
    '      InstrumentDateOffset(Counter + 1) = 0
    '    End If
    '  Next

    '  ' OK, Now we have Delta and Instrument data.

    '  ' AttributionsArray Contains Attributions in Bips.

    '  ReDim AttributionsArray(InstrumentDeltasSeries.Length, InstrumentDateSeries(0).Length - 1)


    '  For InstrumentCounter = 0 To (InstrumentDeltasSeries.Length)

    '    For DateCounter = 0 To (InstrumentDateSeries(0).Length - 1)

    '      If (InstrumentCounter = 0) Then
    '        AttributionsArray(InstrumentCounter, DateCounter) = InstrumentReturnSeries(InstrumentCounter)(DateCounter) ' Index Zero = Group / Instrument Return. i.e. Gross Attribution
    '      Else
    '        AdjustedDateIndex = DateCounter + InstrumentDateOffset(InstrumentCounter)

    '        If (AdjustedDateIndex >= 0) AndAlso (AdjustedDateIndex < InstrumentReturnSeries(InstrumentCounter).Length) Then
    '          AttributionsArray(InstrumentCounter, DateCounter) = InstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(DateCounter) * InstrumentReturnSeries(InstrumentCounter)(AdjustedDateIndex) * 100.0#
    '        Else
    '          AttributionsArray(InstrumentCounter, DateCounter) = 0.0#
    '        End If
    '      End If

    '    Next

    '  Next

    '' Add Data Rows

    'If (BySector) Then

    '  More_Code_Here()

    'Else
    '  Dim YTD_Bips As Double

    '  For InstrumentCounter = 1 To (InstrumentDeltasSeries.Length)

    '    For DateCounter = 0 To (InstrumentDateSeries(0).Length - 1)

    '      If (AttributionYear <= 0) OrElse (CDate(InstrumentDateSeries(0)(DateCounter)).Year = AttributionYear) Then

    '        If (DateCounter = 0) Then
    '          YTD_Bips = 0.0#
    '        ElseIf (Year(InstrumentDateSeries(0)(DateCounter - 1)) <> Year(InstrumentDateSeries(0)(DateCounter))) Then
    '          YTD_Bips = 0.0#
    '        End If

    '        AdjustedDateIndex = DateCounter + InstrumentDateOffset(InstrumentCounter)

    '        Try
    '          thisRow = RVal.NewRow

    '          thisRow("AttribFund") = 0
    '          thisRow("AttribInstrument") = BriefInstrumentDetails(InstrumentCounter).InstrumentID
    '          thisRow("AttribDate") = InstrumentDateSeries(0)(DateCounter)
    '          thisRow("AttribPeriod") = ReportDataPeriod

    '          If (AdjustedDateIndex >= 0) AndAlso (AdjustedDateIndex < InstrumentReturnSeries(InstrumentCounter).Length) Then

    '            thisRow("AttribWeight") = InstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(DateCounter)
    '            thisRow("AttribEndWeight") = InstrumentDeltasSeries(InstrumentCounter - 1).DeltaArray(DateCounter)
    '            thisRow("AttribBips") = AttributionsArray(InstrumentCounter, DateCounter)
    '            thisRow("AttribPercentReturn") = InstrumentReturnSeries(InstrumentCounter)(AdjustedDateIndex)
    '            YTD_Bips += AttributionsArray(InstrumentCounter, DateCounter)
    '          Else
    '            thisRow("AttribWeight") = 0
    '            thisRow("AttribEndWeight") = 0
    '            thisRow("AttribBips") = 0
    '            thisRow("AttribPercentReturn") = 0
    '          End If

    '          thisRow("YTDAttribBips") = YTD_Bips
    '          thisRow("AttribProfit") = 0
    '          thisRow("AttribExpectedBips") = 0
    '          thisRow("AttribIsLookthrough") = CBool(CInt(LookThrough))
    '          thisRow("AttribKnowledgedate") = Renaissance_BaseDate
    '          thisRow("AttribDateEntered") = Now.Date
    '          thisRow("FundCode") = FundCode
    '          thisRow("FundName") = FundName
    '          thisRow("FundLegalEntity") = ""
    '          thisRow("InstrumentDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentDescription
    '          thisRow("InstrumentClass") = BriefInstrumentDetails(InstrumentCounter).InstrumentClass
    '          thisRow("InstrumentType") = BriefInstrumentDetails(InstrumentCounter).InstrumentType
    '          thisRow("InstrumentTypeDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentTypeDescription
    '          thisRow("FundTypeID") = BriefInstrumentDetails(InstrumentCounter).FundTypeID
    '          thisRow("FundTypeDescription") = BriefInstrumentDetails(InstrumentCounter).FundTypeDescription
    '          thisRow("FundTypeAttributionGrouping") = BriefInstrumentDetails(InstrumentCounter).FundTypeAttributionGrouping
    '          thisRow("FundTypeIsAdministrativeOnly") = BriefInstrumentDetails(InstrumentCounter).FundTypeIsAdmin
    '          thisRow("FundTypeSortOrder") = BriefInstrumentDetails(InstrumentCounter).FundTypeSortOrder
    '          thisRow("SortableInstrumentDescription") = BriefInstrumentDetails(InstrumentCounter).InstrumentDescription

    '          RVal.Rows.Add(thisRow)
    '        Catch ex As Exception

    '        End Try

    '      End If

    '    Next

    '  Next

    'End If

    ' End If

    Return RVal

  End Function

End Module
