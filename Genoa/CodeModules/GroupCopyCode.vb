Imports System.Data.SqlClient
Imports RenaissanceGlobals

Module GroupCopyCode

	Friend Function CopyToNewGroup(ByRef Mainform As GenoaMain, ByVal pSourceGroupID As Integer, ByVal pTargetGroupName As String, ByVal pCopyMembers As Boolean, ByVal pCopyItemData As Boolean, ByVal pDuplicateConstraints As Boolean) As Integer
		' *****************************************************************************
		' 
		' *****************************************************************************
		Dim RVal As Integer = 0

		Try
			Dim NewGroupID As Integer = 0
			Dim NewConstraintSetID As Integer = 0
			Dim ExistingConstraintSetID As Integer = 0
			Dim ExistingDSGroupList As RenaissanceDataClass.DSGroupList = Nothing
			Dim ExistingGroupRows() As RenaissanceDataClass.DSGroupList.tblGroupListRow = Nothing
			Dim NewGroupRow As RenaissanceDataClass.DSGroupList.tblGroupListRow = Nothing
			Dim ColumnCount As Integer
			Dim GroupListAdaptor As SqlDataAdapter = Nothing
			Dim ConstraintItemsAdaptor As SqlDataAdapter = Nothing

			' Retrieve Axisting Group details and correct Table Adaptor.

			If (pSourceGroupID > 0) Then

				ExistingDSGroupList = Mainform.Load_Table(RenaissanceStandardDatasets.tblGroupList)
				If (ExistingDSGroupList IsNot Nothing) AndAlso (ExistingDSGroupList.tblGroupList IsNot Nothing) Then
					ExistingGroupRows = ExistingDSGroupList.tblGroupList.Select("GroupListID=" & pSourceGroupID.ToString)
				End If
				GroupListAdaptor = Mainform.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupList.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupList.TableName)

			End If

			' OK, All being well, Copy Group.

			If (ExistingGroupRows IsNot Nothing) AndAlso (ExistingGroupRows.Length > 0) AndAlso (GroupListAdaptor IsNot Nothing) Then

				' Build New Group Row :

				NewGroupRow = ExistingDSGroupList.tblGroupList.NewtblGroupListRow

				' Copy Existing Row Data

				For ColumnCount = 0 To (ExistingDSGroupList.tblGroupList.Columns.Count - 1)
					If (Not ExistingDSGroupList.tblGroupList.Columns(ColumnCount).AutoIncrement) AndAlso (ExistingDSGroupList.tblGroupList.Columns(ColumnCount).Expression.Length <= 0) Then
						NewGroupRow(ColumnCount) = ExistingGroupRows(0)(ColumnCount)
					End If
				Next

				' Allow for Copying the Constraints Set

				ExistingConstraintSetID = ExistingGroupRows(0).DefaultConstraintGroup
				If (pDuplicateConstraints) AndAlso (ExistingGroupRows(0).DefaultConstraintGroup > 0) Then
					NewConstraintSetID = Mainform.CreateNewConstraintSet("Copy of " & Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblGenoaConstraintList, ExistingConstraintSetID, "ConstraintGroupName"), ExistingGroupRows(0).GroupListName & " Constraints").ToString)
				Else
					NewConstraintSetID = ExistingGroupRows(0).DefaultConstraintGroup
				End If

				NewGroupRow.DefaultConstraintGroup = NewConstraintSetID
				NewGroupRow.GroupListID = 0

				' Set New Group Name 

				NewGroupRow.GroupListName = pTargetGroupName

				' Save New Group Details

				ExistingDSGroupList.tblGroupList.Rows.Add(NewGroupRow)

				Try
					GroupListAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
					GroupListAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

					Call Mainform.AdaptorUpdate("CopyToNewGroup()", GroupListAdaptor, New DataRow() {NewGroupRow})
					NewGroupID = NewGroupRow.GroupListID
				Catch ex As Exception
					NewGroupID = 0
				End Try

				' OK, Copy Details

				If (pCopyMembers) AndAlso (NewGroupID > 0) Then
					CopyGroupDetails(Mainform, pSourceGroupID, NewGroupID, pCopyItemData)
				End If

				' Copy Constraint Details

				If (NewConstraintSetID > 0) AndAlso (NewConstraintSetID <> ExistingGroupRows(0).DefaultConstraintGroup) Then
					Dim ExistingDSConstraintItems As RenaissanceDataClass.DSGenoaConstraintItems
					Dim ExistingConstraintRows() As RenaissanceDataClass.DSGenoaConstraintItems.tblGenoaConstraintItemsRow = Nothing
					Dim NewConstraintRow As RenaissanceDataClass.DSGenoaConstraintItems.tblGenoaConstraintItemsRow = Nothing
					Dim OriginalConstraintRow As RenaissanceDataClass.DSGenoaConstraintItems.tblGenoaConstraintItemsRow = Nothing

					ExistingDSConstraintItems = Mainform.Load_Table(RenaissanceStandardDatasets.tblGenoaConstraintItems)
					If (ExistingDSConstraintItems IsNot Nothing) AndAlso (ExistingDSConstraintItems.tblGenoaConstraintItems IsNot Nothing) Then
						ExistingConstraintRows = ExistingDSConstraintItems.tblGenoaConstraintItems.Select("ConstraintGroupID=" & ExistingConstraintSetID.ToString)
					End If
					ConstraintItemsAdaptor = Mainform.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGenoaConstraintItems.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGenoaConstraintItems.TableName)

					If (ExistingConstraintRows IsNot Nothing) AndAlso (ExistingConstraintRows.Length > 0) Then

						For Each OriginalConstraintRow In ExistingConstraintRows

							NewConstraintRow = ExistingDSConstraintItems.tblGenoaConstraintItems.NewtblGenoaConstraintItemsRow

							' Copy Existing Row Data

							For ColumnCount = 0 To (ExistingDSConstraintItems.tblGenoaConstraintItems.Columns.Count - 1)
								If (Not ExistingDSConstraintItems.tblGenoaConstraintItems.Columns(ColumnCount).AutoIncrement) AndAlso (ExistingDSConstraintItems.tblGenoaConstraintItems.Columns(ColumnCount).Expression.Length <= 0) Then
									NewConstraintRow(ColumnCount) = OriginalConstraintRow(ColumnCount)
								End If
							Next

							NewConstraintRow.ConstraintGroupID = NewConstraintSetID
							NewConstraintRow.ConstraintID = 0

							ExistingDSConstraintItems.tblGenoaConstraintItems.Rows.Add(NewConstraintRow)

						Next

						Try
							ConstraintItemsAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
							ConstraintItemsAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

							Call Mainform.AdaptorUpdate("CopyToNewGroup()", ConstraintItemsAdaptor, ExistingDSConstraintItems.tblGenoaConstraintItems)
						Catch ex As Exception
							Mainform.LogError("CopyToNewGroup()", LOG_LEVELS.Error, ex.Message, "Error Copying Constraint Items", ex.StackTrace, True)
						End Try

					End If ' ExistingConstraintRows IsNot Nothing

				End If ' Copy Constraint Details

				' Copy Custom Field Data 






				RVal = NewGroupID

			End If

		Catch ex As Exception
			Mainform.LogError("CopyToNewGroup()", LOG_LEVELS.Error, ex.Message, "Error Copying Group Details", ex.StackTrace, True)
		End Try

		Return RVal

	End Function

	Friend Sub CopyGroupDetails(ByRef Mainform As GenoaMain, ByVal pSourceGroupID As Integer, ByVal pTargetGroupID As Integer, ByVal pCopyItemData As Boolean)
		' *****************************************************************************
		' 
		' *****************************************************************************
		Dim ExistingDSGroupItems As RenaissanceDataClass.DSGroupItems = Nothing
		Dim ExistingItemRows() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow = Nothing
		Dim NewItemRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow = Nothing
		Dim OriginalItemRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow = Nothing
		Dim GroupItemsAdaptor As SqlDataAdapter = Nothing

		Dim ExistingDSGroupItemData As RenaissanceDataClass.DSGroupItemData = Nothing
		Dim ExistingItemDataRows() As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow = Nothing
		Dim NewItemDataRow As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow = Nothing
		Dim OriginalItemDataRow As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow = Nothing
		Dim GroupItemDataAdaptor As SqlDataAdapter = Nothing

		Try

			' First, Copy Members

			Dim ColumnCount As Integer
			Dim ItemID As Integer

			ExistingDSGroupItems = Mainform.Load_Table(RenaissanceStandardDatasets.tblGroupItems)
			If (ExistingDSGroupItems IsNot Nothing) AndAlso (ExistingDSGroupItems.tblGroupItems IsNot Nothing) Then
				ExistingItemRows = ExistingDSGroupItems.tblGroupItems.Select("GroupID=" & pSourceGroupID.ToString)
			End If
			GroupItemsAdaptor = Mainform.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItems.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItems.TableName)

			If (pCopyItemData) Then
				ExistingDSGroupItemData = Mainform.Load_Table(RenaissanceStandardDatasets.tblGroupItemData)
				GroupItemDataAdaptor = Mainform.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItemData.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItemData.TableName)
			End If

			If (ExistingItemRows IsNot Nothing) AndAlso (ExistingItemRows.Length > 0) Then

				For Each OriginalItemRow In ExistingItemRows

					NewItemRow = ExistingDSGroupItems.tblGroupItems.NewtblGroupItemsRow

					' Copy Existing Row Data

					For ColumnCount = 0 To (ExistingDSGroupItems.tblGroupItems.Columns.Count - 1)
						If (Not ExistingDSGroupItems.tblGroupItems.Columns(ColumnCount).AutoIncrement) AndAlso (ExistingDSGroupItems.tblGroupItems.Columns(ColumnCount).Expression.Length <= 0) Then
							NewItemRow(ColumnCount) = OriginalItemRow(ColumnCount)
						End If
					Next

					NewItemRow.GroupID = pTargetGroupID
					NewItemRow.GroupItemID = 0

					ExistingDSGroupItems.tblGroupItems.Rows.Add(NewItemRow)

					Try
						ItemID = 0
						GroupItemsAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
						GroupItemsAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

						Call Mainform.AdaptorUpdate("CopyGroupDetails()", GroupItemsAdaptor, New DataRow() {NewItemRow})
						ItemID = NewItemRow.GroupItemID

					Catch ex As Exception
						Mainform.LogError("CopyGroupDetails()", LOG_LEVELS.Error, ex.Message, "Error Copying Group Items", ex.StackTrace, True)
					End Try

					' Copy Item Data 

					If (pCopyItemData) AndAlso (ItemID > 0) AndAlso (ExistingDSGroupItemData IsNot Nothing) Then

						ExistingItemDataRows = ExistingDSGroupItemData.tblGroupItemData.Select("GroupItemID=" & OriginalItemRow.GroupItemID.ToString)

						If (ExistingItemDataRows IsNot Nothing) AndAlso (ExistingItemDataRows.Length > 0) Then

							For Each OriginalItemDataRow In ExistingItemDataRows
								NewItemDataRow = ExistingDSGroupItemData.tblGroupItemData.NewtblGroupItemDataRow

								For ColumnCount = 0 To (ExistingDSGroupItemData.tblGroupItemData.Columns.Count - 1)
									If (Not ExistingDSGroupItemData.tblGroupItemData.Columns(ColumnCount).AutoIncrement) AndAlso (ExistingDSGroupItemData.tblGroupItemData.Columns(ColumnCount).Expression.Length <= 0) Then
										NewItemDataRow(ColumnCount) = OriginalItemDataRow(ColumnCount)
									End If
								Next

								NewItemDataRow.GroupItemID = ItemID

								ExistingDSGroupItemData.tblGroupItemData.Rows.Add(NewItemDataRow)
							Next

							Try
								GroupItemDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
								GroupItemDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

								Call Mainform.AdaptorUpdate("CopyGroupDetails()", GroupItemDataAdaptor, ExistingDSGroupItemData.tblGroupItemData)

							Catch ex As Exception
								Mainform.LogError("CopyGroupDetails()", LOG_LEVELS.Error, ex.Message, "Error Copying Group Item Data.", ex.StackTrace, True)
							End Try

						End If ' Item Data exists...

					End If ' Copy Item Data...

				Next ' Item 

			End If ' Existing Items Exist.

		Catch ex As Exception
			Mainform.LogError("", LOG_LEVELS.Error, ex.Message, "Error Copying Group", ex.StackTrace, True)
		End Try

	End Sub

End Module
