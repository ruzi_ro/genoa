﻿Imports System
Imports System.Collections
Imports System.Windows.Forms
Imports RenaissanceGlobals

Public Class CustomC1Combo
	Inherits ComboBox
	Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

	Protected Overrides Sub RefreshItem(ByVal index As Integer)
		MyBase.RefreshItem(index)
	End Sub

	Protected Overrides Sub SetItemsCore(ByVal items As System.Collections.IList)
		MyBase.SetItemsCore(items)
	End Sub

	Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
		Return value.ToString
	End Function

	Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
		Return System.Drawing.Design.UITypeEditorEditStyle.DropDown
	End Function

	Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
		Return Me.SelectedValue
	End Function

	Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
		Try
			If (value IsNot Nothing) Then
				Me.SelectedValue = value
			End If
		Catch ex As Exception
		End Try
	End Sub

	Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit

		Select Case e.KeyCode
			Case Keys.Enter, Keys.Tab
				Return True
		End Select

		Return False
	End Function

	Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
		MyBase.FontHeight = rc.Height

		Bounds = rc
	End Sub

	Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
		Return True
	End Function
End Class

Public Class RadC1GridCombo
	Inherits FCP_TelerikControls.FCP_RadComboBox
	Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

	Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
		Return value.ToString
	End Function

	Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
		Return System.Drawing.Design.UITypeEditorEditStyle.DropDown
	End Function

	Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
		Return Me.SelectedValue
	End Function

	Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
		Try
			If (value IsNot Nothing) Then
				Me.SelectedValue = value
			End If
		Catch ex As Exception
		End Try
	End Sub

	Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit

		Select Case e.KeyCode
			Case Keys.Enter, Keys.Tab
				Return True
		End Select

		Return False
	End Function

	Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
		MyBase.FontHeight = rc.Height

		Bounds = rc
	End Sub

	Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
		Return True
	End Function
End Class

