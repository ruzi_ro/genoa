Module TableLookupFunctions

	Friend Function LookupTableRow(ByRef Mainform As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, Optional ByVal ReturnField As String = "", Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As DataRow
		' *************************************************************
		' Return the DataRow for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************

		Dim ThisDS As DataSet
		Dim ThisTable As DataTable
		Dim SelectedRows() As DataRow

		Try
			ThisDS = Mainform.Load_Table(StdTableDef)
			If (ThisDS Is Nothing) OrElse (ThisDS.Tables.Count < 1) Then
				Return Nothing
			End If

			ThisTable = ThisDS.Tables(0)
			If (ReturnField.Length > 0) AndAlso (ThisTable.Columns.Contains(ReturnField) = False) Then
				Return Nothing
			End If

			If (ThisAuditID > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select("AuditID=" & ThisAuditID.ToString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			ElseIf (SearchString.Length > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			Else
				Return Nothing
			End If

		Catch ex As Exception
			Return Nothing
		End Try

	End Function

	Friend Function LookupTableValue(ByRef Mainform As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, ByVal ReturnField As String, Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As Object
		' *************************************************************
		' Return the given Data Value for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************
		Dim SelectedRow As DataRow

		Try
			SelectedRow = LookupTableRow(Mainform, StdTableDef, ThisAuditID, ReturnField, SortString, SearchString)

			If (SelectedRow IsNot Nothing) Then
				If (SelectedRow.Table.Columns.Contains(ReturnField)) Then
					Return SelectedRow(ReturnField)
				End If
			End If

		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

End Module
