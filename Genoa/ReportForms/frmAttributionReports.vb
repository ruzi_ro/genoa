Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissancePertracAndStatsGlobals

Public Class frmAttributionsReports

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
  Friend WithEvents btnClose As System.Windows.Forms.Button
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Combo_AttributionYear As System.Windows.Forms.ComboBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Check_LookthroughAttributions As System.Windows.Forms.CheckBox
  Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
  Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Group_Selection As System.Windows.Forms.GroupBox
  Friend WithEvents Group_ReportGrouping As System.Windows.Forms.GroupBox
  Friend WithEvents Combo_ReportGroupingOption As System.Windows.Forms.ComboBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectedReportGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Date_DateTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Check_DontShowOthers As System.Windows.Forms.CheckBox
  Friend WithEvents Label4 As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_AttributionYear = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Check_LookthroughAttributions = New System.Windows.Forms.CheckBox
    Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
    Me.Combo_Instrument = New System.Windows.Forms.ComboBox
    Me.Combo_ReportName = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Group_Selection = New System.Windows.Forms.GroupBox
    Me.Group_ReportGrouping = New System.Windows.Forms.GroupBox
    Me.Check_DontShowOthers = New System.Windows.Forms.CheckBox
    Me.Combo_SelectedReportGroup = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Combo_ReportGroupingOption = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Label6 = New System.Windows.Forms.Label
    Me.Form_StatusStrip.SuspendLayout()
    Me.Group_Selection.SuspendLayout()
    Me.Group_ReportGrouping.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(50, 328)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 6
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(266, 328)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 7
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(9, 42)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 17)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Group / Sim Name"
    '
    'Combo_AttributionYear
    '
    Me.Combo_AttributionYear.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionYear.Location = New System.Drawing.Point(133, 66)
    Me.Combo_AttributionYear.Name = "Combo_AttributionYear"
    Me.Combo_AttributionYear.Size = New System.Drawing.Size(252, 21)
    Me.Combo_AttributionYear.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(9, 70)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(120, 17)
    Me.Label1.TabIndex = 55
    Me.Label1.Text = "Attribution Year"
    '
    'Check_LookthroughAttributions
    '
    Me.Check_LookthroughAttributions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_LookthroughAttributions.Location = New System.Drawing.Point(6, 17)
    Me.Check_LookthroughAttributions.Name = "Check_LookthroughAttributions"
    Me.Check_LookthroughAttributions.Size = New System.Drawing.Size(285, 20)
    Me.Check_LookthroughAttributions.TabIndex = 0
    Me.Check_LookthroughAttributions.Text = "Report on Lookthrough Attributions"
    '
    'Check_AggregateToParent
    '
    Me.Check_AggregateToParent.Enabled = False
    Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateToParent.Location = New System.Drawing.Point(6, 39)
    Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
    Me.Check_AggregateToParent.Size = New System.Drawing.Size(285, 20)
    Me.Check_AggregateToParent.TabIndex = 1
    Me.Check_AggregateToParent.Text = "Aggregate positions to Parent Instrument"
    '
    'Combo_Instrument
    '
    Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Instrument.Location = New System.Drawing.Point(133, 38)
    Me.Combo_Instrument.Name = "Combo_Instrument"
    Me.Combo_Instrument.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Instrument.TabIndex = 1
    '
    'Combo_ReportName
    '
    Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportName.Items.AddRange(New Object() {"Attribution Report", "Attribution Sector Bips", "Attribution Sector Returns", "Attribution Sector Weights", "Attribution Best / Worst"})
    Me.Combo_ReportName.Location = New System.Drawing.Point(133, 10)
    Me.Combo_ReportName.Name = "Combo_ReportName"
    Me.Combo_ReportName.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportName.TabIndex = 0
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(9, 14)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(120, 17)
    Me.Label4.TabIndex = 68
    Me.Label4.Text = "Report Name"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 368)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(396, 22)
    Me.Form_StatusStrip.TabIndex = 69
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Group_Selection
    '
    Me.Group_Selection.Controls.Add(Me.Check_AggregateToParent)
    Me.Group_Selection.Controls.Add(Me.Check_LookthroughAttributions)
    Me.Group_Selection.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_Selection.Location = New System.Drawing.Point(12, 126)
    Me.Group_Selection.Name = "Group_Selection"
    Me.Group_Selection.Size = New System.Drawing.Size(373, 72)
    Me.Group_Selection.TabIndex = 4
    Me.Group_Selection.TabStop = False
    Me.Group_Selection.Text = "Attribution Selection"
    '
    'Group_ReportGrouping
    '
    Me.Group_ReportGrouping.Controls.Add(Me.Check_DontShowOthers)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_SelectedReportGroup)
    Me.Group_ReportGrouping.Controls.Add(Me.Label5)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_ReportGroupingOption)
    Me.Group_ReportGrouping.Controls.Add(Me.Label3)
    Me.Group_ReportGrouping.Enabled = False
    Me.Group_ReportGrouping.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_ReportGrouping.Location = New System.Drawing.Point(12, 204)
    Me.Group_ReportGrouping.Name = "Group_ReportGrouping"
    Me.Group_ReportGrouping.Size = New System.Drawing.Size(373, 104)
    Me.Group_ReportGrouping.TabIndex = 5
    Me.Group_ReportGrouping.TabStop = False
    Me.Group_ReportGrouping.Text = "Report Grouping"
    '
    'Check_DontShowOthers
    '
    Me.Check_DontShowOthers.Enabled = False
    Me.Check_DontShowOthers.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DontShowOthers.Location = New System.Drawing.Point(121, 73)
    Me.Check_DontShowOthers.Name = "Check_DontShowOthers"
    Me.Check_DontShowOthers.Size = New System.Drawing.Size(245, 20)
    Me.Check_DontShowOthers.TabIndex = 2
    Me.Check_DontShowOthers.Text = "Don't show 'Others'"
    '
    'Combo_SelectedReportGroup
    '
    Me.Combo_SelectedReportGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectedReportGroup.Location = New System.Drawing.Point(121, 46)
    Me.Combo_SelectedReportGroup.Name = "Combo_SelectedReportGroup"
    Me.Combo_SelectedReportGroup.Size = New System.Drawing.Size(245, 21)
    Me.Combo_SelectedReportGroup.TabIndex = 1
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(5, 50)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(100, 17)
    Me.Label5.TabIndex = 76
    Me.Label5.Text = "Selected Group"
    '
    'Combo_ReportGroupingOption
    '
    Me.Combo_ReportGroupingOption.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportGroupingOption.Items.AddRange(New Object() {"No additional Grouping", "Group by Fund Type Attribution Grouping", "Group by Selected fund Type Group", "Group by Instrument Type", "Group by Selected Instrument Type"})
    Me.Combo_ReportGroupingOption.Location = New System.Drawing.Point(121, 19)
    Me.Combo_ReportGroupingOption.Name = "Combo_ReportGroupingOption"
    Me.Combo_ReportGroupingOption.Size = New System.Drawing.Size(245, 21)
    Me.Combo_ReportGroupingOption.TabIndex = 0
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(5, 23)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 17)
    Me.Label3.TabIndex = 74
    Me.Label3.Text = "Report Grouping"
    '
    'Date_DateTo
    '
    Me.Date_DateTo.Location = New System.Drawing.Point(133, 94)
    Me.Date_DateTo.Name = "Date_DateTo"
    Me.Date_DateTo.Size = New System.Drawing.Size(252, 20)
    Me.Date_DateTo.TabIndex = 3
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(9, 98)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(120, 17)
    Me.Label6.TabIndex = 132
    Me.Label6.Text = "Latest Attribution Date"
    '
    'frmAttributionsReports
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(396, 390)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Date_DateTo)
    Me.Controls.Add(Me.Group_ReportGrouping)
    Me.Controls.Add(Me.Group_Selection)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Combo_ReportName)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_Instrument)
    Me.Controls.Add(Me.Combo_AttributionYear)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmAttributionsReports"
    Me.Text = "Group Attribution Reports"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Group_Selection.ResumeLayout(False)
    Me.Group_ReportGrouping.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Genoa form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  Private FormControls As ArrayList = Nothing

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
  Private THIS_FORM_ChangeID As RenaissanceChangeID

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean


  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
    ' Public property to return handle to the 'Main' Genoa form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
    Get
      Return False
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
    Get
      Return True
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = GenoaFormID.frmAttributionsReports

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    _FormOpenFailed = False

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
    End If

    ' Build Combos

    Me.Combo_ReportName.SelectedIndex = 0

    Call Me.SetFundCombo()
    Call Me.SetAttributionYearCombo(Now.Year)

    Me.Date_DateTo.Value = New Date(Now.Year + 1, 1, 1).AddDays(-1)
    Check_DontShowOthers.Checked = False
    Check_LookthroughAttributions.Checked = True

    MainForm.SetComboSelectionLengths(Me)

    Combo_Instrument.Focus()

  End Sub

  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceUpdateEventArgs)
    ' Routine to handle changes / updates to tables by this and other windows.
    ' If this, or any other, form posts a change to a table, then it will invoke an update event 
    ' detailing what tables have been altered.
    ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
    ' the 'GenoaAutoUpdate' event of the main Genoa form.
    ' Each form may them react as appropriate to changes in any table that might impact it.
    '
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If

    ' Changes to the tblAttribution table :-
    If (e.TableChanged(RenaissanceChangeID.tblAttribution) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetAttributionYearCombo(Now.Year)
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetFundCombo()

    Dim ThisCommand As New SqlCommand
    Dim tmpTable As New DataTable

    Try

      ThisCommand.CommandType = CommandType.StoredProcedure
      ThisCommand.CommandText = "adp_tblMastername_SelectDeltaInstruments"
      ThisCommand.Connection = MainForm.GetGenoaConnection()

      If (ThisCommand.Connection IsNot Nothing) Then
        SyncLock ThisCommand.Connection
          tmpTable.Load(ThisCommand.ExecuteReader)
        End SyncLock
      End If

    Catch ex As Exception
    Finally
      Try
        If (ThisCommand IsNot Nothing) AndAlso (ThisCommand.Connection IsNot Nothing) Then
          ThisCommand.Connection.Close()
          ThisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Try
      If tmpTable.Rows.Count > 0 Then

        Call MainForm.SetTblGenericCombo( _
          Me.Combo_Instrument, _
          tmpTable.Select(), _
          "MASTERNAME", _
          "ID", _
          "", False, True, False)     ' 

      Else

        If (Me.Combo_Instrument.Items.Count > 0) Then

          Me.Combo_Instrument.DataSource = Nothing
          Me.Combo_Instrument.Items.Clear()

        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

  Sub SetAttributionYearCombo(Optional ByVal pInitialvalue As Integer = 0)
    ' ********************************************************************************
    ' Set the Attribution Year Combo to reflect those years for which Attribution
    ' records exist for the selected fund.
    '
    ' ********************************************************************************

    Dim thisRow As DataRow

    Dim InitialValue As Integer
    Dim Counter As Integer

    InitialValue = pInitialvalue
    Try
      If Me.Combo_AttributionYear.SelectedIndex >= 0 Then
        InitialValue = CInt(Me.Combo_AttributionYear.SelectedValue)
      End If
      Me.Combo_AttributionYear.Items.Clear()
    Catch ex As Exception
      InitialValue = 0
    End Try

    Try
      If Me.Combo_Instrument.SelectedIndex <= 0 Then ' Note Leading Blank.
        Exit Sub
      End If
      If Not IsNumeric(Combo_Instrument.SelectedValue) Then
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Dim thisCommand As SqlCommand = Nothing
    Dim tmpTable As New DataTable

    Try
      Dim Query As String
      Query = "SELECT DATEPART(yyyy, Attribdate) FROM fn_tblAttribution_SelectKD(Null) WHERE (AttribFund = " & Combo_Instrument.SelectedValue.ToString & ") GROUP BY DATEPART(yyyy, Attribdate) ORDER BY DATEPART(yyyy, Attribdate) DESC"
      thisCommand = New SqlCommand(Query, MainForm.GetGenoaConnection())

      If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
        SyncLock thisCommand.Connection
          tmpTable.Load(thisCommand.ExecuteReader)
        End SyncLock
      End If
    Catch ex As Exception
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try
    thisCommand = Nothing

    Try
      If tmpTable.Rows.Count > 0 Then
        For Each thisRow In tmpTable.Rows
          Me.Combo_AttributionYear.Items.Add(thisRow.Item(0).ToString)
        Next

        tmpTable.Rows.Clear()
      End If
    Catch ex As Exception
    End Try

    ' Initial value
    Try
      If (InitialValue > 0) Then
        For Counter = 0 To (Me.Combo_AttributionYear.Items.Count - 1)
          If CInt(Me.Combo_AttributionYear.Items(Counter)) = InitialValue Then
            Me.Combo_AttributionYear.SelectedIndex = Counter
            Exit Sub
          End If
        Next
      End If
      If (Me.Combo_AttributionYear.Items.Count > 0) AndAlso (Me.Combo_AttributionYear.SelectedIndex < 0) Then
        Me.Combo_AttributionYear.SelectedIndex = 0
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " SetButton / Control Events (Form Specific Code) "

  Private Sub Combo_ReportName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportName.SelectedIndexChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try

      If Me.Combo_ReportName.SelectedIndex >= 0 Then
        Me.Combo_AttributionYear.Enabled = True
        Me.Date_DateTo.Enabled = True

        Select Case Me.Combo_ReportName.Text
          Case "Attribution Report"
            Me.Group_ReportGrouping.Enabled = True
            Me.Check_AggregateToParent.Enabled = True

          Case "Attribution Sector Bips"
            Me.Group_ReportGrouping.Enabled = True
            Me.Check_AggregateToParent.Enabled = False
            Me.Check_AggregateToParent.Checked = False

          Case "Attribution Sector Returns"
            Me.Group_ReportGrouping.Enabled = True
            'If Me.Combo_ReportGroupingOption.Items.Count > 0 Then
            '  Combo_ReportGroupingOption.SelectedIndex = 0
            'End If
            'If Me.Combo_SelectedReportGroup.Items.Count > 0 Then
            '  Combo_SelectedReportGroup.SelectedIndex = 0
            'End If

            Me.Check_AggregateToParent.Enabled = False
            Me.Check_AggregateToParent.Checked = False

          Case "Attribution Sector Weights"
            Me.Group_ReportGrouping.Enabled = True

            Me.Check_AggregateToParent.Enabled = False
            Me.Check_AggregateToParent.Checked = False

          Case "Attribution Best / Worst"
            Me.Group_ReportGrouping.Enabled = False
            If Me.Combo_ReportGroupingOption.Items.Count > 0 Then
              Combo_ReportGroupingOption.SelectedIndex = 0
            End If
            If Me.Combo_SelectedReportGroup.Items.Count > 0 Then
              Combo_SelectedReportGroup.SelectedIndex = 0
            End If

            Me.Check_AggregateToParent.Enabled = True

        End Select
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_ReportGroupingOption_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportGroupingOption.SelectedIndexChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try
      Try
        Combo_SelectedReportGroup.DataSource = Nothing
        Me.Combo_SelectedReportGroup.Items.Clear()
      Catch ex As Exception
      End Try

      Select Case Me.Combo_ReportGroupingOption.SelectedIndex

        Case 0
          'No additional Grouping

          Combo_SelectedReportGroup.Enabled = False
          Check_DontShowOthers.Enabled = False
          Check_DontShowOthers.Checked = False

        Case 1
          'Group by Fund Type Attribution Grouping

          Combo_SelectedReportGroup.Enabled = False
          Check_DontShowOthers.Enabled = False
          Check_DontShowOthers.Checked = False

        Case 2
          'Group by Selected fund Type Group

          Combo_SelectedReportGroup.Enabled = True
          Check_DontShowOthers.Enabled = True
          Check_DontShowOthers.Checked = False

          Call MainForm.SetTblGenericCombo( _
           Me.Combo_SelectedReportGroup, _
           RenaissanceStandardDatasets.tblFundType, _
           "FundTypeAttributionGrouping", _
           "FundTypeAttributionGrouping", _
           "", True, True, False)     ' 

        Case 3
          'Group by Instrument Type

          Combo_SelectedReportGroup.Enabled = False
          Check_DontShowOthers.Enabled = False
          Check_DontShowOthers.Checked = False

        Case 4
          'Group by Selected Instrument Type

          Combo_SelectedReportGroup.Enabled = True
          Check_DontShowOthers.Enabled = True
          Check_DontShowOthers.Checked = False

          Call MainForm.SetTblGenericCombo( _
           Me.Combo_SelectedReportGroup, _
           RenaissanceStandardDatasets.tblInstrumentType, _
           "InstrumentTypeDescription", _
           "InstrumentTypeID", _
           "")     ' 

        Case Else
          Combo_SelectedReportGroup.Enabled = False
          Check_DontShowOthers.Enabled = False
          Check_DontShowOthers.Checked = False

      End Select
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Instrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Instrument.SelectedIndexChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try

      Call SetAttributionYearCombo(Now.Year)

      If (Combo_AttributionYear.SelectedIndex < 0) AndAlso (Combo_AttributionYear.Items.Count > 0) Then
        Combo_AttributionYear.SelectedIndex = 0
      End If

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Combo_AttributionYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_AttributionYear.SelectedIndexChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    If (Combo_AttributionYear.SelectedIndex >= 0) Then
      Me.Date_DateTo.Value = New Date(CInt(Me.Combo_AttributionYear.Items(Combo_AttributionYear.SelectedIndex)) + 1, 1, 1).AddDays(-1)
    End If
  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      RunReport()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    Finally
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      Label_Status.Text = ""
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
    End Try
  End Sub

  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim InstrumentID As Integer
    Dim AttributionYear As Integer
    Dim MaxAttributionDate As Date
    Dim ReportName As String
    Dim DontShowOthers As Boolean = False
    Dim BySector As Boolean = False

    Dim AttribPeriod As DealingPeriod = DealingPeriod.Monthly
    Dim Lookthrough As LookthroughEnum = LookthroughEnum.None

    ' Validate

    If (Combo_Instrument.SelectedValue Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If

    If (Combo_Instrument.SelectedIndex <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If
    InstrumentID = CInt(Combo_Instrument.SelectedValue)

    If IsNumeric(Combo_AttributionYear.Text) = False Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Year must be selected.", "", True)
      Exit Sub
    End If
    AttributionYear = CInt(Combo_AttributionYear.Text)

    ' Get Max Attribution Date

    MaxAttributionDate = Renaissance_BaseDate
    If IsDate(Date_DateTo.Value) Then
      MaxAttributionDate = Date_DateTo.Value
    End If

    ' Don't Show Others ?

    DontShowOthers = Check_DontShowOthers.Checked

    ' Display Report

    ' Attribution(Report)
    ' Attribution Sector Bips
    ' Attribution Sector Returns
    ' Attribution Sector Weights
    ' Attribution(Money)
    ' Attribution(Best / Worst)

    Select Case Combo_ReportName.SelectedIndex
      Case 0
        ReportName = "rptAttribution"

      Case 1
        ReportName = "rptSectorBips"
        BySector = True

      Case 2
        ReportName = "rptSectorReturns"
        BySector = True

      Case 3
        ReportName = "rptSectorWeights"
        BySector = True

      Case 4
        ReportName = "rptAttributionBestWorst"

      Case Else
        ReportName = ""
    End Select

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "Run Attribution Report : " & ReportName, "", "", False)

    Try
      Me.Cursor = Cursors.WaitCursor

      If (ReportName.Length > 0) Then
        MainForm.SetToolStripText(Label_Status, "Loading Data and Rendering Report.")
        If Not (MainForm.InvokeRequired) Then
          Application.DoEvents()
        End If

        Dim ReportGroupingOption As Integer = Combo_ReportGroupingOption.SelectedIndex
        Dim ReportGroupingChoice As String = ""
        Dim ReportData As DataTable

        If (Combo_SelectedReportGroup.SelectedValue IsNot Nothing) Then
          ReportGroupingChoice = Combo_SelectedReportGroup.SelectedValue.ToString
        End If

        If (Check_LookthroughAttributions.Checked) Then
          Lookthrough = LookthroughEnum.LookthroughToSimulations
        End If

        ReportData = GetAttributionData(MainForm, BySector, InstrumentID, DealingPeriod.Monthly, AttributionYear, AttributionYear, MaxAttributionDate, Lookthrough, Check_AggregateToParent.Checked)

        Call MainForm.MainReportHandler.DisplayReport(ReportName, ReportName, ReportData, 0, Now.Date, Renaissance_BaseDate, Nothing)

      Else
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Attribution report not found.", "", True)
      End If

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

 

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region




End Class
