Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceUtilities.DatePeriodFunctions

Public Class frmCompetitorGroupsReport

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents btnRunReport As System.Windows.Forms.Button
	Friend WithEvents btnClose As System.Windows.Forms.Button
	Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
	Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
	Friend WithEvents Combo_SelectFrom As System.Windows.Forms.ComboBox
	Friend WithEvents Radio_Pertrac As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_ExistingGroups As System.Windows.Forms.RadioButton
	Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
	Friend WithEvents Panel_SelectFrom As System.Windows.Forms.Panel
	Friend WithEvents Combo_SelectGroup1 As System.Windows.Forms.ComboBox
	Friend WithEvents Check_Section1 As System.Windows.Forms.CheckBox
	Friend WithEvents Text_SectionLabel1 As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents Text_SectionLabel2 As System.Windows.Forms.TextBox
	Friend WithEvents Combo_SelectGroup2 As System.Windows.Forms.ComboBox
	Friend WithEvents Check_Section2 As System.Windows.Forms.CheckBox
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents Text_SectionLabel3 As System.Windows.Forms.TextBox
	Friend WithEvents Combo_SelectGroup3 As System.Windows.Forms.ComboBox
	Friend WithEvents Check_Section3 As System.Windows.Forms.CheckBox
	Friend WithEvents Panel4 As System.Windows.Forms.Panel
	Friend WithEvents Check_Section4 As System.Windows.Forms.CheckBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Text_SectionLabel4 As System.Windows.Forms.TextBox
	Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents Date_ReportEndDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents Panel5 As System.Windows.Forms.Panel
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Combo_Column2DateRange As System.Windows.Forms.ComboBox
	Friend WithEvents Label_Column3StartDate As System.Windows.Forms.Label
	Friend WithEvents Date_Column3StartDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents Label7 As System.Windows.Forms.Label
	Friend WithEvents Combo_Column3DateRange As System.Windows.Forms.ComboBox
	Friend WithEvents Radio_ExistingFunds As System.Windows.Forms.RadioButton
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.Combo_SelectFrom = New System.Windows.Forms.ComboBox
		Me.Radio_Pertrac = New System.Windows.Forms.RadioButton
		Me.Radio_ExistingGroups = New System.Windows.Forms.RadioButton
		Me.Radio_ExistingFunds = New System.Windows.Forms.RadioButton
		Me.List_SelectItems = New System.Windows.Forms.ListBox
		Me.Panel_SelectFrom = New System.Windows.Forms.Panel
		Me.Combo_SelectGroup1 = New System.Windows.Forms.ComboBox
		Me.Check_Section1 = New System.Windows.Forms.CheckBox
		Me.Text_SectionLabel1 = New System.Windows.Forms.TextBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.Panel2 = New System.Windows.Forms.Panel
		Me.Label2 = New System.Windows.Forms.Label
		Me.Text_SectionLabel2 = New System.Windows.Forms.TextBox
		Me.Combo_SelectGroup2 = New System.Windows.Forms.ComboBox
		Me.Check_Section2 = New System.Windows.Forms.CheckBox
		Me.Panel3 = New System.Windows.Forms.Panel
		Me.Label3 = New System.Windows.Forms.Label
		Me.Text_SectionLabel3 = New System.Windows.Forms.TextBox
		Me.Combo_SelectGroup3 = New System.Windows.Forms.ComboBox
		Me.Check_Section3 = New System.Windows.Forms.CheckBox
		Me.Panel4 = New System.Windows.Forms.Panel
		Me.Label4 = New System.Windows.Forms.Label
		Me.Text_SectionLabel4 = New System.Windows.Forms.TextBox
		Me.Check_Section4 = New System.Windows.Forms.CheckBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.Date_ReportEndDate = New System.Windows.Forms.DateTimePicker
		Me.Panel5 = New System.Windows.Forms.Panel
		Me.Label_Column3StartDate = New System.Windows.Forms.Label
		Me.Date_Column3StartDate = New System.Windows.Forms.DateTimePicker
		Me.Label7 = New System.Windows.Forms.Label
		Me.Combo_Column3DateRange = New System.Windows.Forms.ComboBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_Column2DateRange = New System.Windows.Forms.ComboBox
		Me.Status1.SuspendLayout()
		Me.Panel_SelectFrom.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.Panel2.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.Panel4.SuspendLayout()
		Me.Panel5.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnRunReport.Location = New System.Drawing.Point(104, 546)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 6
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.Location = New System.Drawing.Point(320, 546)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 7
		Me.btnClose.Text = "&Close"
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 584)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(515, 22)
		Me.Status1.TabIndex = 69
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(0, 17)
		'
		'Combo_SelectFrom
		'
		Me.Combo_SelectFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectFrom.Enabled = False
		Me.Combo_SelectFrom.Location = New System.Drawing.Point(188, 31)
		Me.Combo_SelectFrom.Name = "Combo_SelectFrom"
		Me.Combo_SelectFrom.Size = New System.Drawing.Size(308, 21)
		Me.Combo_SelectFrom.TabIndex = 2
		'
		'Radio_Pertrac
		'
		Me.Radio_Pertrac.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_Pertrac.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_Pertrac.Location = New System.Drawing.Point(3, 3)
		Me.Radio_Pertrac.Name = "Radio_Pertrac"
		Me.Radio_Pertrac.Size = New System.Drawing.Size(124, 21)
		Me.Radio_Pertrac.TabIndex = 0
		Me.Radio_Pertrac.TabStop = True
		Me.Radio_Pertrac.Text = "Pertrac Instrument"
		Me.Radio_Pertrac.UseVisualStyleBackColor = True
		'
		'Radio_ExistingGroups
		'
		Me.Radio_ExistingGroups.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ExistingGroups.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ExistingGroups.Location = New System.Drawing.Point(3, 30)
		Me.Radio_ExistingGroups.Name = "Radio_ExistingGroups"
		Me.Radio_ExistingGroups.Size = New System.Drawing.Size(124, 21)
		Me.Radio_ExistingGroups.TabIndex = 1
		Me.Radio_ExistingGroups.TabStop = True
		Me.Radio_ExistingGroups.Text = "Genoa Group"
		Me.Radio_ExistingGroups.UseVisualStyleBackColor = True
		'
		'Radio_ExistingFunds
		'
		Me.Radio_ExistingFunds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ExistingFunds.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ExistingFunds.Location = New System.Drawing.Point(3, 57)
		Me.Radio_ExistingFunds.Name = "Radio_ExistingFunds"
		Me.Radio_ExistingFunds.Size = New System.Drawing.Size(124, 21)
		Me.Radio_ExistingFunds.TabIndex = 2
		Me.Radio_ExistingFunds.TabStop = True
		Me.Radio_ExistingFunds.Text = "Existing Position"
		Me.Radio_ExistingFunds.UseVisualStyleBackColor = True
		'
		'List_SelectItems
		'
		Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_SelectItems.Enabled = False
		Me.List_SelectItems.FormattingEnabled = True
		Me.List_SelectItems.Location = New System.Drawing.Point(188, 58)
		Me.List_SelectItems.Name = "List_SelectItems"
		Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_SelectItems.Size = New System.Drawing.Size(308, 121)
		Me.List_SelectItems.Sorted = True
		Me.List_SelectItems.TabIndex = 4
		'
		'Panel_SelectFrom
		'
		Me.Panel_SelectFrom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_ExistingFunds)
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_ExistingGroups)
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_Pertrac)
		Me.Panel_SelectFrom.Enabled = False
		Me.Panel_SelectFrom.Location = New System.Drawing.Point(47, 58)
		Me.Panel_SelectFrom.Name = "Panel_SelectFrom"
		Me.Panel_SelectFrom.Size = New System.Drawing.Size(135, 89)
		Me.Panel_SelectFrom.TabIndex = 3
		'
		'Combo_SelectGroup1
		'
		Me.Combo_SelectGroup1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroup1.Enabled = False
		Me.Combo_SelectGroup1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup1.Location = New System.Drawing.Point(188, 5)
		Me.Combo_SelectGroup1.Name = "Combo_SelectGroup1"
		Me.Combo_SelectGroup1.Size = New System.Drawing.Size(308, 21)
		Me.Combo_SelectGroup1.TabIndex = 1
		'
		'Check_Section1
		'
		Me.Check_Section1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_Section1.Location = New System.Drawing.Point(5, 5)
		Me.Check_Section1.Name = "Check_Section1"
		Me.Check_Section1.Size = New System.Drawing.Size(177, 21)
		Me.Check_Section1.TabIndex = 0
		Me.Check_Section1.Text = "Show Section One"
		Me.Check_Section1.UseVisualStyleBackColor = True
		'
		'Text_SectionLabel1
		'
		Me.Text_SectionLabel1.Enabled = False
		Me.Text_SectionLabel1.Location = New System.Drawing.Point(188, 32)
		Me.Text_SectionLabel1.Name = "Text_SectionLabel1"
		Me.Text_SectionLabel1.Size = New System.Drawing.Size(308, 20)
		Me.Text_SectionLabel1.TabIndex = 2
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel1.Controls.Add(Me.Label1)
		Me.Panel1.Controls.Add(Me.Text_SectionLabel1)
		Me.Panel1.Controls.Add(Me.Combo_SelectGroup1)
		Me.Panel1.Controls.Add(Me.Check_Section1)
		Me.Panel1.Location = New System.Drawing.Point(5, 131)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(504, 64)
		Me.Panel1.TabIndex = 2
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(82, 35)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 17)
		Me.Label1.TabIndex = 83
		Me.Label1.Text = "Section Label"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Panel2
		'
		Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel2.Controls.Add(Me.Label2)
		Me.Panel2.Controls.Add(Me.Text_SectionLabel2)
		Me.Panel2.Controls.Add(Me.Combo_SelectGroup2)
		Me.Panel2.Controls.Add(Me.Check_Section2)
		Me.Panel2.Location = New System.Drawing.Point(5, 201)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(504, 64)
		Me.Panel2.TabIndex = 3
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(82, 35)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(100, 17)
		Me.Label2.TabIndex = 84
		Me.Label2.Text = "Section Label"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Text_SectionLabel2
		'
		Me.Text_SectionLabel2.Enabled = False
		Me.Text_SectionLabel2.Location = New System.Drawing.Point(188, 32)
		Me.Text_SectionLabel2.Name = "Text_SectionLabel2"
		Me.Text_SectionLabel2.Size = New System.Drawing.Size(308, 20)
		Me.Text_SectionLabel2.TabIndex = 2
		'
		'Combo_SelectGroup2
		'
		Me.Combo_SelectGroup2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroup2.Enabled = False
		Me.Combo_SelectGroup2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup2.Location = New System.Drawing.Point(188, 5)
		Me.Combo_SelectGroup2.Name = "Combo_SelectGroup2"
		Me.Combo_SelectGroup2.Size = New System.Drawing.Size(308, 21)
		Me.Combo_SelectGroup2.TabIndex = 1
		'
		'Check_Section2
		'
		Me.Check_Section2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_Section2.Location = New System.Drawing.Point(5, 5)
		Me.Check_Section2.Name = "Check_Section2"
		Me.Check_Section2.Size = New System.Drawing.Size(177, 21)
		Me.Check_Section2.TabIndex = 0
		Me.Check_Section2.Text = "Show Section Two"
		Me.Check_Section2.UseVisualStyleBackColor = True
		'
		'Panel3
		'
		Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel3.Controls.Add(Me.Label3)
		Me.Panel3.Controls.Add(Me.Text_SectionLabel3)
		Me.Panel3.Controls.Add(Me.Combo_SelectGroup3)
		Me.Panel3.Controls.Add(Me.Check_Section3)
		Me.Panel3.Location = New System.Drawing.Point(5, 271)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(504, 64)
		Me.Panel3.TabIndex = 4
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(82, 35)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(100, 17)
		Me.Label3.TabIndex = 84
		Me.Label3.Text = "Section Label"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Text_SectionLabel3
		'
		Me.Text_SectionLabel3.Enabled = False
		Me.Text_SectionLabel3.Location = New System.Drawing.Point(188, 32)
		Me.Text_SectionLabel3.Name = "Text_SectionLabel3"
		Me.Text_SectionLabel3.Size = New System.Drawing.Size(308, 20)
		Me.Text_SectionLabel3.TabIndex = 2
		'
		'Combo_SelectGroup3
		'
		Me.Combo_SelectGroup3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroup3.Enabled = False
		Me.Combo_SelectGroup3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup3.Location = New System.Drawing.Point(188, 5)
		Me.Combo_SelectGroup3.Name = "Combo_SelectGroup3"
		Me.Combo_SelectGroup3.Size = New System.Drawing.Size(308, 21)
		Me.Combo_SelectGroup3.TabIndex = 1
		'
		'Check_Section3
		'
		Me.Check_Section3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_Section3.Location = New System.Drawing.Point(5, 5)
		Me.Check_Section3.Name = "Check_Section3"
		Me.Check_Section3.Size = New System.Drawing.Size(177, 21)
		Me.Check_Section3.TabIndex = 0
		Me.Check_Section3.Text = "Show Section Three"
		Me.Check_Section3.UseVisualStyleBackColor = True
		'
		'Panel4
		'
		Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel4.Controls.Add(Me.Label4)
		Me.Panel4.Controls.Add(Me.Text_SectionLabel4)
		Me.Panel4.Controls.Add(Me.Check_Section4)
		Me.Panel4.Controls.Add(Me.Panel_SelectFrom)
		Me.Panel4.Controls.Add(Me.Combo_SelectFrom)
		Me.Panel4.Controls.Add(Me.List_SelectItems)
		Me.Panel4.Location = New System.Drawing.Point(5, 341)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(504, 195)
		Me.Panel4.TabIndex = 5
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(82, 8)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(100, 17)
		Me.Label4.TabIndex = 86
		Me.Label4.Text = "Section Label"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Text_SectionLabel4
		'
		Me.Text_SectionLabel4.Enabled = False
		Me.Text_SectionLabel4.Location = New System.Drawing.Point(188, 5)
		Me.Text_SectionLabel4.Name = "Text_SectionLabel4"
		Me.Text_SectionLabel4.Size = New System.Drawing.Size(308, 20)
		Me.Text_SectionLabel4.TabIndex = 1
		'
		'Check_Section4
		'
		Me.Check_Section4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_Section4.Location = New System.Drawing.Point(5, 31)
		Me.Check_Section4.Name = "Check_Section4"
		Me.Check_Section4.Size = New System.Drawing.Size(177, 21)
		Me.Check_Section4.TabIndex = 0
		Me.Check_Section4.Text = "Show Section Four"
		Me.Check_Section4.UseVisualStyleBackColor = True
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(12, 12)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(177, 17)
		Me.Label5.TabIndex = 87
		Me.Label5.Text = "Report End Date"
		'
		'Date_ReportEndDate
		'
		Me.Date_ReportEndDate.CustomFormat = "dd MMM yyyy"
		Me.Date_ReportEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.Date_ReportEndDate.Location = New System.Drawing.Point(195, 8)
		Me.Date_ReportEndDate.Name = "Date_ReportEndDate"
		Me.Date_ReportEndDate.Size = New System.Drawing.Size(150, 20)
		Me.Date_ReportEndDate.TabIndex = 0
		'
		'Panel5
		'
		Me.Panel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel5.Controls.Add(Me.Label_Column3StartDate)
		Me.Panel5.Controls.Add(Me.Date_Column3StartDate)
		Me.Panel5.Controls.Add(Me.Label7)
		Me.Panel5.Controls.Add(Me.Combo_Column3DateRange)
		Me.Panel5.Controls.Add(Me.Label6)
		Me.Panel5.Controls.Add(Me.Combo_Column2DateRange)
		Me.Panel5.Location = New System.Drawing.Point(5, 34)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(504, 91)
		Me.Panel5.TabIndex = 1
		'
		'Label_Column3StartDate
		'
		Me.Label_Column3StartDate.Enabled = False
		Me.Label_Column3StartDate.Location = New System.Drawing.Point(5, 63)
		Me.Label_Column3StartDate.Name = "Label_Column3StartDate"
		Me.Label_Column3StartDate.Size = New System.Drawing.Size(177, 17)
		Me.Label_Column3StartDate.TabIndex = 87
		Me.Label_Column3StartDate.Text = "Column 3 Start Date"
		'
		'Date_Column3StartDate
		'
		Me.Date_Column3StartDate.CustomFormat = "dd MMM yyyy"
		Me.Date_Column3StartDate.Enabled = False
		Me.Date_Column3StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.Date_Column3StartDate.Location = New System.Drawing.Point(188, 60)
		Me.Date_Column3StartDate.Name = "Date_Column3StartDate"
		Me.Date_Column3StartDate.Size = New System.Drawing.Size(150, 20)
		Me.Date_Column3StartDate.TabIndex = 2
		'
		'Label7
		'
		Me.Label7.Location = New System.Drawing.Point(5, 33)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(177, 17)
		Me.Label7.TabIndex = 85
		Me.Label7.Text = "Column 3 Date Range"
		'
		'Combo_Column3DateRange
		'
		Me.Combo_Column3DateRange.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Column3DateRange.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Column3DateRange.Items.AddRange(New Object() {"1 Year", "2 Year", "3 Year", "5 Year", "Inception To Date", "Custom Start Date"})
		Me.Combo_Column3DateRange.Location = New System.Drawing.Point(188, 33)
		Me.Combo_Column3DateRange.Name = "Combo_Column3DateRange"
		Me.Combo_Column3DateRange.Size = New System.Drawing.Size(308, 21)
		Me.Combo_Column3DateRange.TabIndex = 1
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(5, 5)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(177, 17)
		Me.Label6.TabIndex = 83
		Me.Label6.Text = "Column 2 Date Range"
		'
		'Combo_Column2DateRange
		'
		Me.Combo_Column2DateRange.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Column2DateRange.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Column2DateRange.Items.AddRange(New Object() {"1 Year", "2 Year", "3 Year", "5 Year", "Inception To Date"})
		Me.Combo_Column2DateRange.Location = New System.Drawing.Point(188, 5)
		Me.Combo_Column2DateRange.Name = "Combo_Column2DateRange"
		Me.Combo_Column2DateRange.Size = New System.Drawing.Size(308, 21)
		Me.Combo_Column2DateRange.TabIndex = 0
		'
		'frmCompetitorGroupsReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(515, 606)
		Me.Controls.Add(Me.Panel5)
		Me.Controls.Add(Me.Date_ReportEndDate)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Panel4)
		Me.Controls.Add(Me.Panel3)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Status1)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.MinimumSize = New System.Drawing.Size(450, 250)
		Me.Name = "frmCompetitorGroupsReport"
		Me.Text = "Genoa Competitor Groups Report"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.Panel_SelectFrom.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.Panel3.ResumeLayout(False)
		Me.Panel3.PerformLayout()
		Me.Panel4.ResumeLayout(False)
		Me.Panel4.PerformLayout()
		Me.Panel5.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' Data Structures
	Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

#End Region

#Region " Form Properties"

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value
    End Set
  End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
    _DefaultStatsDatePeriod = pMainForm.DefaultStatsDatePeriod

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmCompetitorGroupsReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_SelectGroup1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup1.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectGroup2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup2.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectGroup3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup3.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try
			Set_ComboGroups()

			Me.Check_Section1.Checked = False
			Me.Check_Section2.Checked = False
			Me.Check_Section3.Checked = False
			Me.Check_Section4.Checked = False

			MainForm.SetComboSelectionLengths(Me)

      Date_ReportEndDate.Value = FitDateToPeriod(DefaultStatsDatePeriod, Now.Date.AddMonths(-1), True)

			Me.Radio_ExistingGroups.Checked = True
			Me.Combo_Column2DateRange.SelectedIndex = 1
			Me.Combo_Column3DateRange.SelectedIndex = 4

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_SelectGroup1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup1.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroup2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup2.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroup3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup3.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'GenoaAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblGroupList table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call Set_ComboGroups()

			If Radio_ExistingGroups.Checked Then
				Call Radio_ExistingGroups_CheckedChanged(Radio_ExistingGroups, New EventArgs)
			End If
		End If


		' Changes to the tblGroupList table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then

			If Radio_ExistingGroups.Checked Then
				Call Radio_ExistingGroups_CheckedChanged(Radio_ExistingGroups, New EventArgs)
			End If
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub Radio_ExistingGroups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingGroups.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingGroups radio.
		'
		' The Select Combo becomes a Combo to select an existing Group,
		' The SelectList contains all Instruments in the selected Group(s).
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_ExistingGroups.Checked) Then

			Try
				Me.Cursor = Cursors.WaitCursor

				' Re-Configure Select Combo events.

				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				Catch ex As Exception
				End Try
				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				Catch ex As Exception
				End Try

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				' Populate Select Combo.

				Call MainForm.SetTblGenericCombo( _
				Me.Combo_SelectFrom, _
				RenaissanceStandardDatasets.tblGroupList, _
				"GroupListName", _
				"GroupListID", _
				"", False, True, True, 0, "All Groups")		' 

				' Trigger  SelectList Rebuild.

				If Combo_SelectFrom.Items.Count > 0 Then
					Combo_SelectFrom.SelectedIndex = 0
				End If
				Call Combo_SelectFrom_SelectedIndexChanged(Combo_SelectFrom, New EventArgs)

			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try

		End If
	End Sub

	Private Sub Radio_Pertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Pertrac.CheckedChanged
		' ***********************************************************************************
		' React to the Pertrac Radio button being selected.
		'
		' The Select combo becomes a Select-As-You-Type edit box for the Select List control.
		' The SelectList control contains All or Selected Pertrac Instruments.
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_Pertrac.Checked) Then

			' Re-Configure Select Combo events.

			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
			Catch ex As Exception
			End Try
			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
			Catch ex As Exception
			End Try

			Try
				Me.Cursor = Cursors.WaitCursor

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				Combo_SelectFrom.DataSource = Nothing
				Combo_SelectFrom.Items.Clear()
				Combo_SelectFrom.Text = ""

				' Trigger Select List re-Build.

				Call Combo_SelectFrom_TextChanged(Combo_SelectFrom, New EventArgs)
			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try
		End If

	End Sub

	Private Sub Radio_ExistingFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingFunds.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingFunds radio.
		'
		' The Select Combo becomes a Combo to select an existing Venice Fund,
		' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_ExistingFunds.Checked) Then

			Try
				Me.Cursor = Cursors.WaitCursor

				' Re-Configure Select Combo events.

				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				Catch ex As Exception
				End Try
				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				Catch ex As Exception
				End Try

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				Call MainForm.PertracData.GetActivePertracInstruments(True, 0) '  GetActivePertracInstruments(MainForm, True, 0)

				' Populate Select Combo.

				Call MainForm.SetTblGenericCombo( _
				Me.Combo_SelectFrom, _
				RenaissanceStandardDatasets.tblFund, _
				"FundCode", _
				"FundID", _
				"", False, True, True, 0, "All Funds")	 ' 

				' Trigger  SelectList Rebuild.

				If Combo_SelectFrom.Items.Count > 0 Then
					Combo_SelectFrom.SelectedIndex = 0
				Else
					Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
				End If

			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try
		End If
	End Sub

	Private Sub Set_ComboGroups()

		MainForm.SetTblGenericCombo( _
		 Me.Combo_SelectGroup1, _
		 RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, _
		 "GroupListName", _
		 "GroupListID", _
		 "", _
		 True, True, True, 0, "No Group")

		MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup2, _
		RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, _
		"GroupListName", _
		"GroupListID", _
		"", _
		True, True, True, 0, "No Group")

		MainForm.SetTblGenericCombo( _
		 Me.Combo_SelectGroup3, _
		 RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, _
		 "GroupListName", _
		 "GroupListID", _
		 "", _
		 True, True, True, 0, "No Group")


	End Sub

	Private Sub Combo_SelectFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.SelectedIndexChanged
		' ***********************************************************************************
		'
		'
		'
		' ***********************************************************************************

		If (Me.Created) Then

			If (Combo_SelectFrom.SelectedIndex >= 0) Then

				If Me.Radio_Pertrac.Checked Then
					' 

					List_SelectItems.SuspendLayout()

					' Ensure the DataView is populated.
					If (PertracInstruments Is Nothing) Then
						PertracInstruments = MainForm.PertracData.GetPertracInstruments()
					End If

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.SelectedText & "%'"

					If (List_SelectItems.DataSource IsNot PertracInstruments) Then
						List_SelectItems.DataSource = PertracInstruments
					End If

					List_SelectItems.ResumeLayout()

				ElseIf (Me.Radio_ExistingGroups.Checked) AndAlso (IsNumeric(Combo_SelectFrom.SelectedValue)) Then
					' Populate Form with given data.

					Dim tmpCommand As New SqlCommand
					Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

					Try

						tmpCommand.CommandType = CommandType.StoredProcedure
						tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
						tmpCommand.Connection = MainForm.GetGenoaConnection
						tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectFrom.SelectedValue)
						tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
						tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

						List_SelectItems.SuspendLayout()

						If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
							ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
							ListTable.Columns.Add("ListDescription", GetType(String))
							ListTable.Columns.Add("PertracName", GetType(String))
							ListTable.Columns.Add("PertracProvider", GetType(String))
							ListTable.Columns.Add("IndexName", GetType(String))
							ListTable.Columns.Add("IndexProvider", GetType(String))

							List_SelectItems.DataSource = ListTable
						Else
							ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
						End If

						ListTable.Rows.Clear()
						ListTable.Load(tmpCommand.ExecuteReader)

						Try
							List_SelectItems.DisplayMember = "ListDescription"
							List_SelectItems.ValueMember = "GroupPertracCode"
						Catch ex As Exception
						End Try

						List_SelectItems.DataSource = ListTable
						List_SelectItems.ResumeLayout()

					Catch ex As Exception

						Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)

					Finally
						Try
							If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
								tmpCommand.Connection.Close()
								tmpCommand.Connection = Nothing
							End If
						Catch ex As Exception
						End Try
					End Try

				ElseIf Me.Radio_ExistingFunds.Checked Then

					List_SelectItems.SuspendLayout()

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					' List_SelectItems.DataSource = GetActivePertracInstruments(MainForm, False, CInt(Combo_SelectFrom.SelectedValue))
					List_SelectItems.DataSource = MainForm.PertracData.GetActivePertracInstruments(False, CInt(Combo_SelectFrom.SelectedValue))

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					List_SelectItems.ResumeLayout()

				End If

			End If
		End If
	End Sub



	Private Sub Combo_SelectFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.TextChanged
		' ***********************************************************************************
		' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
		'
		' The List is managed by applying a select critera to the PertracInstruments DataView.
		' ***********************************************************************************

		If (Me.Created) Then
			If (Combo_SelectFrom.SelectedIndex < 0) Then
				If Me.Radio_Pertrac.Checked Then

					List_SelectItems.SuspendLayout()

					' Ensure the DataView is populated.
					If (PertracInstruments Is Nothing) Then
						PertracInstruments = MainForm.PertracData.GetPertracInstruments()
					End If

					' Set Display and View Members

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					' Set Selection criteria.

					If (PertracInstruments.RowFilter <> "Mastername LIKE '" & Combo_SelectFrom.Text & "%'") Then
						PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.Text & "%'"
					End If

					' Set DataSource

					If (List_SelectItems.DataSource IsNot PertracInstruments) Then
						List_SelectItems.DataSource = PertracInstruments
					End If

					' Set Display and View Members (Double check).

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					List_SelectItems.ResumeLayout()

				End If
			End If
		End If
	End Sub

	Private Sub Button_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Dim IndexCounter As Integer

		Try
			List_SelectItems.SuspendLayout()

			For IndexCounter = (List_SelectItems.Items.Count - 1) To 0 Step -1
				If (List_SelectItems.SelectedIndices.Contains(IndexCounter) = False) Then
					List_SelectItems.SelectedIndices.Add(IndexCounter)
				End If
			Next
		Catch ex As Exception
		Finally
			List_SelectItems.ResumeLayout()
		End Try

	End Sub

	Private Sub Button_SelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

		Try
			List_SelectItems.ClearSelected()
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim IndexCounter As Integer
		Dim FormControls As ArrayList = Nothing

		Try
			FormControls = MainForm.DisableFormControls(Me)

			Dim Group1_Label As String = ""
			Dim Group2_Label As String = ""
			Dim Group3_Label As String = ""
			Dim Group4_Label As String = ""
			Dim Group1_ID As Integer = 0
			Dim Group2_ID As Integer = 0
			Dim Group3_ID As Integer = 0
			Dim Group4_SelectedIDs(-1) As Integer

			If (Me.Check_Section1.Checked) Then
				If (Combo_SelectGroup1.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroup1.SelectedValue)) Then
					Group1_ID = CInt(Combo_SelectGroup1.SelectedValue)
					Group1_Label = Text_SectionLabel1.Text.Trim
				End If
			End If
			If (Me.Check_Section2.Checked) Then
				If (Combo_SelectGroup2.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroup2.SelectedValue)) Then
					Group2_ID = CInt(Combo_SelectGroup2.SelectedValue)
					Group2_Label = Text_SectionLabel2.Text.Trim
				End If
			End If
			If (Me.Check_Section3.Checked) Then
				If (Combo_SelectGroup3.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroup3.SelectedValue)) Then
					Group3_ID = CInt(Combo_SelectGroup3.SelectedValue)
					Group3_Label = Text_SectionLabel3.Text.Trim
				End If
			End If
			If (Me.Check_Section4.Checked) Then
				Dim GroupName As String

				If (Me.List_SelectItems.SelectedItems.Count > 0) Then
					ReDim Group4_SelectedIDs(List_SelectItems.SelectedItems.Count - 1)

					For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
						Group4_SelectedIDs(IndexCounter) = List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember)
					Next

					GroupName = "Custom Selection"
					If (Me.Radio_ExistingGroups.Checked) AndAlso (Me.Combo_SelectFrom.SelectedIndex >= 0) Then
						If (List_SelectItems.SelectedItems.Count = List_SelectItems.Items.Count) Then
							GroupName = Combo_SelectFrom.SelectedItem(Me.Combo_SelectFrom.DisplayMember).ToString
						Else
							GroupName = "SubSet of " & Combo_SelectFrom.SelectedItem(Combo_SelectFrom.DisplayMember).ToString
						End If
					End If
				End If

				Group4_Label = Text_SectionLabel4.Text.Trim
			End If

			Me.StatusLabel1.Text = "Processing Report"
			Me.Refresh()
			Application.DoEvents()

      MainForm.MainReportHandler.CompetitorGroupsReport(DefaultStatsDatePeriod, Group1_Label, Group1_ID, Group2_Label, Group2_ID, Group3_Label, Group3_ID, Group4_Label, Group4_SelectedIDs, Date_ReportEndDate.Value, Combo_Column2DateRange.SelectedIndex, Combo_Column3DateRange.SelectedIndex, Date_Column3StartDate.Value, StatusLabel1)

		Catch ex As Exception

		Finally
			MainForm.EnableFormControls(FormControls)
		End Try

		Me.StatusLabel1.Text = ""
		Me.btnRunReport.Enabled = True

	End Sub


	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

	Private Sub Combo_Column3DateRange_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Column3DateRange.SelectedIndexChanged
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try

			If (Me.Created) AndAlso (Not Me.IsDisposed) Then
				If Me.Combo_Column3DateRange.SelectedIndex = 5 Then
					Me.Date_Column3StartDate.Enabled = True
					Label_Column3StartDate.Enabled = True
				Else
					Me.Date_Column3StartDate.Enabled = False
					Label_Column3StartDate.Enabled = False
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Date_ReportEndDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_ReportEndDate.ValueChanged
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			Dim NewEndDate As Date

			If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        NewEndDate = FitDateToPeriod(DefaultStatsDatePeriod, Date_ReportEndDate.Value, True)

				If (Date_ReportEndDate.Value <> NewEndDate) Then
					Date_ReportEndDate.Value = NewEndDate
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Date_Column3StartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_Column3StartDate.ValueChanged
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			Dim NewEndDate As Date

			If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        NewEndDate = FitDateToPeriod(DefaultStatsDatePeriod, Date_Column3StartDate.Value, False)

				If (Date_Column3StartDate.Value <> NewEndDate) Then
					Date_Column3StartDate.Value = NewEndDate
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Check_Section1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Section1.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Check_Section1.Checked) Then
				Combo_SelectGroup1.Enabled = True
				Text_SectionLabel1.Enabled = True
			Else
				Combo_SelectGroup1.Enabled = False
				Text_SectionLabel1.Enabled = False
			End If
		End If

	End Sub

	Private Sub Check_Section2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Section2.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Check_Section2.Checked) Then
				Combo_SelectGroup2.Enabled = True
				Text_SectionLabel2.Enabled = True
			Else
				Combo_SelectGroup2.Enabled = False
				Text_SectionLabel2.Enabled = False
			End If
		End If

	End Sub

	Private Sub Check_Section3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Section3.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Check_Section3.Checked) Then
				Combo_SelectGroup3.Enabled = True
				Text_SectionLabel3.Enabled = True
			Else
				Combo_SelectGroup3.Enabled = False
				Text_SectionLabel3.Enabled = False
			End If
		End If

	End Sub

	Private Sub Check_Section4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Section4.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Check_Section4.Checked) Then
				Panel_SelectFrom.Enabled = True
				Combo_SelectFrom.Enabled = True
				List_SelectItems.Enabled = True
				Text_SectionLabel4.Enabled = True
			Else
				Panel_SelectFrom.Enabled = False
				Combo_SelectFrom.Enabled = False
				List_SelectItems.Enabled = False
				Text_SectionLabel4.Enabled = False
			End If
		End If

	End Sub



#End Region







End Class
