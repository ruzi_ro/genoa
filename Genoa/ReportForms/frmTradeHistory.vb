Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

Public Class frmTradeHistory

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
  Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectInstrument As System.Windows.Forms.ComboBox
  Friend WithEvents Date_StartDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Date_EndDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Numeric_NAV As RenaissanceControls.NumericTextBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Radio_NAV_AtStart As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_NAV_Now As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_NAV_OnDate As System.Windows.Forms.RadioButton
  Friend WithEvents Date_NAV_Date As System.Windows.Forms.DateTimePicker
  Friend WithEvents Radio_SortByDate As System.Windows.Forms.RadioButton
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Radio_SortByInstrument As System.Windows.Forms.RadioButton
  Friend WithEvents Grid_Results As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Butto_Refresh As System.Windows.Forms.Button
  Friend WithEvents Tab_TradeHistory As System.Windows.Forms.TabControl
  Friend WithEvents TabPage_TradeGrid As System.Windows.Forms.TabPage
  Friend WithEvents TabPage_Exposure As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Exposure As Dundas.Charting.WinControl.Chart
  Friend WithEvents TabPage_Turnover As System.Windows.Forms.TabPage
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents CheckedList_Exposure As System.Windows.Forms.CheckedListBox
  Friend WithEvents Chart_Turnover As Dundas.Charting.WinControl.Chart
  Friend WithEvents Check_ShowGrossTurnover As System.Windows.Forms.CheckBox
  Friend WithEvents Check_ShowSellTurnover As System.Windows.Forms.CheckBox
  Friend WithEvents Check_ShowBuyTurnover As System.Windows.Forms.CheckBox
  Friend WithEvents Check_ShowTurnoverAsPercentage As System.Windows.Forms.CheckBox
  Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTradeHistory))
    Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Me.Status1 = New System.Windows.Forms.StatusStrip
    Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
    Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_SelectInstrument = New System.Windows.Forms.ComboBox
    Me.Date_StartDate = New System.Windows.Forms.DateTimePicker
    Me.Date_EndDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.Numeric_NAV = New RenaissanceControls.NumericTextBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Radio_NAV_AtStart = New System.Windows.Forms.RadioButton
    Me.Radio_NAV_Now = New System.Windows.Forms.RadioButton
    Me.Radio_NAV_OnDate = New System.Windows.Forms.RadioButton
    Me.Date_NAV_Date = New System.Windows.Forms.DateTimePicker
    Me.Radio_SortByDate = New System.Windows.Forms.RadioButton
    Me.Label4 = New System.Windows.Forms.Label
    Me.Radio_SortByInstrument = New System.Windows.Forms.RadioButton
    Me.Grid_Results = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Butto_Refresh = New System.Windows.Forms.Button
    Me.Tab_TradeHistory = New System.Windows.Forms.TabControl
    Me.TabPage_TradeGrid = New System.Windows.Forms.TabPage
    Me.TabPage_Exposure = New System.Windows.Forms.TabPage
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.CheckedList_Exposure = New System.Windows.Forms.CheckedListBox
    Me.Chart_Exposure = New Dundas.Charting.WinControl.Chart
    Me.TabPage_Turnover = New System.Windows.Forms.TabPage
    Me.Check_ShowSellTurnover = New System.Windows.Forms.CheckBox
    Me.Check_ShowBuyTurnover = New System.Windows.Forms.CheckBox
    Me.Check_ShowGrossTurnover = New System.Windows.Forms.CheckBox
    Me.Chart_Turnover = New Dundas.Charting.WinControl.Chart
    Me.Check_ShowTurnoverAsPercentage = New System.Windows.Forms.CheckBox
    Me.Status1.SuspendLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_TradeHistory.SuspendLayout()
    Me.TabPage_TradeGrid.SuspendLayout()
    Me.TabPage_Exposure.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    CType(Me.Chart_Exposure, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TabPage_Turnover.SuspendLayout()
    CType(Me.Chart_Turnover, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Status1
    '
    Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
    Me.Status1.Location = New System.Drawing.Point(0, 609)
    Me.Status1.Name = "Status1"
    Me.Status1.Size = New System.Drawing.Size(989, 22)
    Me.Status1.TabIndex = 69
    Me.Status1.Text = "StatusStrip1"
    '
    'ToolStripProgressBar1
    '
    Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.ToolStripProgressBar1.Maximum = 20
    Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
    Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
    Me.ToolStripProgressBar1.Step = 1
    Me.ToolStripProgressBar1.Visible = False
    '
    'StatusLabel1
    '
    Me.StatusLabel1.Name = "StatusLabel1"
    Me.StatusLabel1.Size = New System.Drawing.Size(111, 17)
    Me.StatusLabel1.Text = "ToolStripStatusLabel1"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(14, 15)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(95, 13)
    Me.Label6.TabIndex = 102
    Me.Label6.Text = "Group / Simulation"
    '
    'Combo_SelectInstrument
    '
    Me.Combo_SelectInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectInstrument.Location = New System.Drawing.Point(128, 12)
    Me.Combo_SelectInstrument.Name = "Combo_SelectInstrument"
    Me.Combo_SelectInstrument.Size = New System.Drawing.Size(411, 21)
    Me.Combo_SelectInstrument.TabIndex = 101
    '
    'Date_StartDate
    '
    Me.Date_StartDate.Location = New System.Drawing.Point(128, 39)
    Me.Date_StartDate.Name = "Date_StartDate"
    Me.Date_StartDate.Size = New System.Drawing.Size(148, 20)
    Me.Date_StartDate.TabIndex = 103
    '
    'Date_EndDate
    '
    Me.Date_EndDate.Location = New System.Drawing.Point(391, 39)
    Me.Date_EndDate.Name = "Date_EndDate"
    Me.Date_EndDate.Size = New System.Drawing.Size(148, 20)
    Me.Date_EndDate.TabIndex = 104
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(14, 43)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(56, 13)
    Me.Label1.TabIndex = 105
    Me.Label1.Text = "Date From"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(326, 43)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(46, 13)
    Me.Label2.TabIndex = 106
    Me.Label2.Text = "Date To"
    '
    'Numeric_NAV
    '
    Me.Numeric_NAV.Location = New System.Drawing.Point(128, 68)
    Me.Numeric_NAV.Name = "Numeric_NAV"
    Me.Numeric_NAV.RenaissanceTag = Nothing
    Me.Numeric_NAV.Size = New System.Drawing.Size(148, 20)
    Me.Numeric_NAV.TabIndex = 107
    Me.Numeric_NAV.Text = "1"
    Me.Numeric_NAV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Numeric_NAV.TextFormat = "#,##0"
    Me.Numeric_NAV.Value = 1
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(14, 75)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(29, 13)
    Me.Label3.TabIndex = 108
    Me.Label3.Text = "NAV"
    '
    'Radio_NAV_AtStart
    '
    Me.Radio_NAV_AtStart.AutoCheck = False
    Me.Radio_NAV_AtStart.AutoSize = True
    Me.Radio_NAV_AtStart.Location = New System.Drawing.Point(293, 69)
    Me.Radio_NAV_AtStart.Name = "Radio_NAV_AtStart"
    Me.Radio_NAV_AtStart.Size = New System.Drawing.Size(85, 17)
    Me.Radio_NAV_AtStart.TabIndex = 109
    Me.Radio_NAV_AtStart.TabStop = True
    Me.Radio_NAV_AtStart.Text = "NAV At Start"
    Me.Radio_NAV_AtStart.UseVisualStyleBackColor = True
    '
    'Radio_NAV_Now
    '
    Me.Radio_NAV_Now.AutoCheck = False
    Me.Radio_NAV_Now.AutoSize = True
    Me.Radio_NAV_Now.Location = New System.Drawing.Point(406, 69)
    Me.Radio_NAV_Now.Name = "Radio_NAV_Now"
    Me.Radio_NAV_Now.Size = New System.Drawing.Size(72, 17)
    Me.Radio_NAV_Now.TabIndex = 110
    Me.Radio_NAV_Now.TabStop = True
    Me.Radio_NAV_Now.Text = "NAV Now"
    Me.Radio_NAV_Now.UseVisualStyleBackColor = True
    '
    'Radio_NAV_OnDate
    '
    Me.Radio_NAV_OnDate.AutoCheck = False
    Me.Radio_NAV_OnDate.AutoSize = True
    Me.Radio_NAV_OnDate.Location = New System.Drawing.Point(508, 69)
    Me.Radio_NAV_OnDate.Name = "Radio_NAV_OnDate"
    Me.Radio_NAV_OnDate.Size = New System.Drawing.Size(68, 17)
    Me.Radio_NAV_OnDate.TabIndex = 111
    Me.Radio_NAV_OnDate.TabStop = True
    Me.Radio_NAV_OnDate.Text = "NAV on :"
    Me.Radio_NAV_OnDate.UseVisualStyleBackColor = True
    '
    'Date_NAV_Date
    '
    Me.Date_NAV_Date.Enabled = False
    Me.Date_NAV_Date.Location = New System.Drawing.Point(582, 67)
    Me.Date_NAV_Date.Name = "Date_NAV_Date"
    Me.Date_NAV_Date.Size = New System.Drawing.Size(148, 20)
    Me.Date_NAV_Date.TabIndex = 112
    '
    'Radio_SortByDate
    '
    Me.Radio_SortByDate.AutoCheck = False
    Me.Radio_SortByDate.AutoSize = True
    Me.Radio_SortByDate.Location = New System.Drawing.Point(128, 97)
    Me.Radio_SortByDate.Name = "Radio_SortByDate"
    Me.Radio_SortByDate.Size = New System.Drawing.Size(48, 17)
    Me.Radio_SortByDate.TabIndex = 113
    Me.Radio_SortByDate.TabStop = True
    Me.Radio_SortByDate.Text = "Date"
    Me.Radio_SortByDate.UseVisualStyleBackColor = True
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(14, 99)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(41, 13)
    Me.Label4.TabIndex = 114
    Me.Label4.Text = "Sort By"
    '
    'Radio_SortByInstrument
    '
    Me.Radio_SortByInstrument.AutoCheck = False
    Me.Radio_SortByInstrument.AutoSize = True
    Me.Radio_SortByInstrument.Location = New System.Drawing.Point(201, 97)
    Me.Radio_SortByInstrument.Name = "Radio_SortByInstrument"
    Me.Radio_SortByInstrument.Size = New System.Drawing.Size(74, 17)
    Me.Radio_SortByInstrument.TabIndex = 115
    Me.Radio_SortByInstrument.TabStop = True
    Me.Radio_SortByInstrument.Text = "Instrument"
    Me.Radio_SortByInstrument.UseVisualStyleBackColor = True
    '
    'Grid_Results
    '
    Me.Grid_Results.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Results.ColumnInfo = resources.GetString("Grid_Results.ColumnInfo")
    Me.Grid_Results.Location = New System.Drawing.Point(1, 1)
    Me.Grid_Results.Name = "Grid_Results"
    Me.Grid_Results.Rows.DefaultSize = 17
    Me.Grid_Results.Size = New System.Drawing.Size(970, 456)
    Me.Grid_Results.StyleInfo = resources.GetString("Grid_Results.StyleInfo")
    Me.Grid_Results.TabIndex = 116
    Me.Grid_Results.Tree.Column = 0
    '
    'Butto_Refresh
    '
    Me.Butto_Refresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Butto_Refresh.Location = New System.Drawing.Point(841, 15)
    Me.Butto_Refresh.Name = "Butto_Refresh"
    Me.Butto_Refresh.Size = New System.Drawing.Size(136, 78)
    Me.Butto_Refresh.TabIndex = 117
    Me.Butto_Refresh.Text = "Refresh"
    Me.Butto_Refresh.UseVisualStyleBackColor = True
    '
    'Tab_TradeHistory
    '
    Me.Tab_TradeHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Tab_TradeHistory.Controls.Add(Me.TabPage_TradeGrid)
    Me.Tab_TradeHistory.Controls.Add(Me.TabPage_Exposure)
    Me.Tab_TradeHistory.Controls.Add(Me.TabPage_Turnover)
    Me.Tab_TradeHistory.Location = New System.Drawing.Point(4, 120)
    Me.Tab_TradeHistory.Name = "Tab_TradeHistory"
    Me.Tab_TradeHistory.SelectedIndex = 0
    Me.Tab_TradeHistory.Size = New System.Drawing.Size(982, 486)
    Me.Tab_TradeHistory.TabIndex = 118
    '
    'TabPage_TradeGrid
    '
    Me.TabPage_TradeGrid.Controls.Add(Me.Grid_Results)
    Me.TabPage_TradeGrid.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_TradeGrid.Name = "TabPage_TradeGrid"
    Me.TabPage_TradeGrid.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_TradeGrid.Size = New System.Drawing.Size(974, 460)
    Me.TabPage_TradeGrid.TabIndex = 0
    Me.TabPage_TradeGrid.Text = "Grid"
    Me.TabPage_TradeGrid.UseVisualStyleBackColor = True
    '
    'TabPage_Exposure
    '
    Me.TabPage_Exposure.Controls.Add(Me.SplitContainer1)
    Me.TabPage_Exposure.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Exposure.Name = "TabPage_Exposure"
    Me.TabPage_Exposure.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Exposure.Size = New System.Drawing.Size(974, 460)
    Me.TabPage_Exposure.TabIndex = 1
    Me.TabPage_Exposure.Text = "Exposure"
    Me.TabPage_Exposure.UseVisualStyleBackColor = True
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.SplitContainer1.Location = New System.Drawing.Point(1, 1)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.CheckedList_Exposure)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.Chart_Exposure)
    Me.SplitContainer1.Size = New System.Drawing.Size(973, 459)
    Me.SplitContainer1.SplitterDistance = 252
    Me.SplitContainer1.TabIndex = 2
    '
    'CheckedList_Exposure
    '
    Me.CheckedList_Exposure.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.CheckedList_Exposure.CheckOnClick = True
    Me.CheckedList_Exposure.FormattingEnabled = True
    Me.CheckedList_Exposure.Location = New System.Drawing.Point(6, 1)
    Me.CheckedList_Exposure.Name = "CheckedList_Exposure"
    Me.CheckedList_Exposure.Size = New System.Drawing.Size(239, 454)
    Me.CheckedList_Exposure.TabIndex = 0
    '
    'Chart_Exposure
    '
    Me.Chart_Exposure.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Exposure.BackColor = System.Drawing.Color.Azure
    Me.Chart_Exposure.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Exposure.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Exposure.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Exposure.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Exposure.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea1.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisY.LabelStyle.Format = "P0"
    ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.StartFromZero = False
    ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.BackColor = System.Drawing.Color.Transparent
    ChartArea1.BorderColor = System.Drawing.Color.DimGray
    ChartArea1.Name = "Default"
    Me.Chart_Exposure.ChartAreas.Add(ChartArea1)
    Legend1.BackColor = System.Drawing.Color.Transparent
    Legend1.BorderColor = System.Drawing.Color.Transparent
    Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend1.DockToChartArea = "Default"
    Legend1.Enabled = False
    Legend1.Name = "Default"
    Me.Chart_Exposure.Legends.Add(Legend1)
    Me.Chart_Exposure.Location = New System.Drawing.Point(1, 1)
    Me.Chart_Exposure.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Exposure.Name = "Chart_Exposure"
    Me.Chart_Exposure.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series1.BorderWidth = 2
    Series1.ChartType = "Line"
    Series1.CustomAttributes = "LabelStyle=Top"
    Series1.Name = "Series1"
    Series1.ShadowOffset = 1
    Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series2.BorderWidth = 2
    Series2.ChartType = "Line"
    Series2.CustomAttributes = "LabelStyle=Top"
    Series2.Name = "Series2"
    Series2.ShadowOffset = 1
    Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Exposure.Series.Add(Series1)
    Me.Chart_Exposure.Series.Add(Series2)
    Me.Chart_Exposure.Size = New System.Drawing.Size(711, 453)
    Me.Chart_Exposure.TabIndex = 1
    Me.Chart_Exposure.Text = "Chart2"
    '
    'TabPage_Turnover
    '
    Me.TabPage_Turnover.Controls.Add(Me.Check_ShowTurnoverAsPercentage)
    Me.TabPage_Turnover.Controls.Add(Me.Check_ShowSellTurnover)
    Me.TabPage_Turnover.Controls.Add(Me.Check_ShowBuyTurnover)
    Me.TabPage_Turnover.Controls.Add(Me.Check_ShowGrossTurnover)
    Me.TabPage_Turnover.Controls.Add(Me.Chart_Turnover)
    Me.TabPage_Turnover.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Turnover.Name = "TabPage_Turnover"
    Me.TabPage_Turnover.Size = New System.Drawing.Size(974, 460)
    Me.TabPage_Turnover.TabIndex = 2
    Me.TabPage_Turnover.Text = "Turnover"
    Me.TabPage_Turnover.UseVisualStyleBackColor = True
    '
    'Check_ShowSellTurnover
    '
    Me.Check_ShowSellTurnover.AutoSize = True
    Me.Check_ShowSellTurnover.Location = New System.Drawing.Point(239, 3)
    Me.Check_ShowSellTurnover.Name = "Check_ShowSellTurnover"
    Me.Check_ShowSellTurnover.Size = New System.Drawing.Size(98, 17)
    Me.Check_ShowSellTurnover.TabIndex = 5
    Me.Check_ShowSellTurnover.Text = "SELL Turnover"
    Me.Check_ShowSellTurnover.UseVisualStyleBackColor = True
    '
    'Check_ShowBuyTurnover
    '
    Me.Check_ShowBuyTurnover.AutoSize = True
    Me.Check_ShowBuyTurnover.Location = New System.Drawing.Point(122, 3)
    Me.Check_ShowBuyTurnover.Name = "Check_ShowBuyTurnover"
    Me.Check_ShowBuyTurnover.Size = New System.Drawing.Size(94, 17)
    Me.Check_ShowBuyTurnover.TabIndex = 4
    Me.Check_ShowBuyTurnover.Text = "BUY Turnover"
    Me.Check_ShowBuyTurnover.UseVisualStyleBackColor = True
    '
    'Check_ShowGrossTurnover
    '
    Me.Check_ShowGrossTurnover.AutoSize = True
    Me.Check_ShowGrossTurnover.Location = New System.Drawing.Point(4, 3)
    Me.Check_ShowGrossTurnover.Name = "Check_ShowGrossTurnover"
    Me.Check_ShowGrossTurnover.Size = New System.Drawing.Size(99, 17)
    Me.Check_ShowGrossTurnover.TabIndex = 3
    Me.Check_ShowGrossTurnover.Text = "Gross Turnover"
    Me.Check_ShowGrossTurnover.UseVisualStyleBackColor = True
    '
    'Chart_Turnover
    '
    Me.Chart_Turnover.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Turnover.BackColor = System.Drawing.Color.Azure
    Me.Chart_Turnover.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Turnover.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Turnover.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Turnover.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Turnover.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea2.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisY.LabelStyle.Format = "N0"
    ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.StartFromZero = False
    ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.BackColor = System.Drawing.Color.Transparent
    ChartArea2.BorderColor = System.Drawing.Color.DimGray
    ChartArea2.Name = "Default"
    Me.Chart_Turnover.ChartAreas.Add(ChartArea2)
    Legend2.BackColor = System.Drawing.Color.Transparent
    Legend2.BorderColor = System.Drawing.Color.Transparent
    Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend2.DockToChartArea = "Default"
    Legend2.Enabled = False
    Legend2.Name = "Default"
    Me.Chart_Turnover.Legends.Add(Legend2)
    Me.Chart_Turnover.Location = New System.Drawing.Point(3, 21)
    Me.Chart_Turnover.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Turnover.Name = "Chart_Turnover"
    Me.Chart_Turnover.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series3.CustomAttributes = "LabelStyle=Top"
    Series3.Name = "Series1"
    Series3.ShadowOffset = 1
    Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series4.CustomAttributes = "LabelStyle=Top"
    Series4.Name = "Series2"
    Series4.ShadowOffset = 1
    Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Turnover.Series.Add(Series3)
    Me.Chart_Turnover.Series.Add(Series4)
    Me.Chart_Turnover.Size = New System.Drawing.Size(968, 436)
    Me.Chart_Turnover.TabIndex = 2
    Me.Chart_Turnover.Text = "Chart2"
    '
    'Check_ShowTurnoverAsPercentage
    '
    Me.Check_ShowTurnoverAsPercentage.AutoSize = True
    Me.Check_ShowTurnoverAsPercentage.Location = New System.Drawing.Point(383, 3)
    Me.Check_ShowTurnoverAsPercentage.Name = "Check_ShowTurnoverAsPercentage"
    Me.Check_ShowTurnoverAsPercentage.Size = New System.Drawing.Size(115, 17)
    Me.Check_ShowTurnoverAsPercentage.TabIndex = 6
    Me.Check_ShowTurnoverAsPercentage.Text = "Show as % of NAV"
    Me.Check_ShowTurnoverAsPercentage.UseVisualStyleBackColor = True
    '
    'frmTradeHistory
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(989, 631)
    Me.Controls.Add(Me.Tab_TradeHistory)
    Me.Controls.Add(Me.Butto_Refresh)
    Me.Controls.Add(Me.Radio_SortByInstrument)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Radio_SortByDate)
    Me.Controls.Add(Me.Date_NAV_Date)
    Me.Controls.Add(Me.Radio_NAV_OnDate)
    Me.Controls.Add(Me.Radio_NAV_Now)
    Me.Controls.Add(Me.Radio_NAV_AtStart)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Numeric_NAV)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_EndDate)
    Me.Controls.Add(Me.Date_StartDate)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_SelectInstrument)
    Me.Controls.Add(Me.Status1)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(900, 250)
    Me.Name = "frmTradeHistory"
    Me.Text = "Trade History"
    Me.Status1.ResumeLayout(False)
    Me.Status1.PerformLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_TradeHistory.ResumeLayout(False)
    Me.TabPage_TradeGrid.ResumeLayout(False)
    Me.TabPage_Exposure.ResumeLayout(False)
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    CType(Me.Chart_Exposure, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TabPage_Turnover.ResumeLayout(False)
    Me.TabPage_Turnover.PerformLayout()
    CType(Me.Chart_Turnover, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

  'Private Sub More_Code_Here()
  'End Sub


#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  Private FormToUpdate_Object As RenaissanceGlobals.RenaissanceTimerUpdateClass
  Private _FormToUpdate As Boolean = False
  Private InFormUpdate_Tick As Boolean = False

  ' The standard ChangeID for this form. e.g. tblPerson
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  ' Data Structures
  Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

  Private ExposureChartArrayList As New ArrayList
  Private TurnoverChartArrayList As New ArrayList


  Private Class GridColumns
    Public Shared FirstCol As Integer = 0
    Public Shared Delta As Integer = 1
    Public Shared LastDelta As Integer = 2
    Public Shared ClosingPrice As Integer = 3
    Public Shared OpeningPosition As Integer = 4
    Public Shared Trade As Integer = 5
    Public Shared ClosingPosition As Integer = 6
    Public Shared NAV As Integer = 7
    Public Shared ParentID As Integer = 8
    Public Shared Counter As Integer = 9
  End Class

  Private Sub Init_GridColumnsClass()

    GridColumns.FirstCol = Me.Grid_Results.Cols("FirstCol").SafeIndex
    GridColumns.Delta = Me.Grid_Results.Cols("Delta").SafeIndex
    GridColumns.LastDelta = Me.Grid_Results.Cols("LastDelta").SafeIndex
    GridColumns.ClosingPrice = Me.Grid_Results.Cols("ClosingPrice").SafeIndex
    GridColumns.OpeningPosition = Me.Grid_Results.Cols("OpeningPosition").SafeIndex
    GridColumns.Trade = Me.Grid_Results.Cols("Trade").SafeIndex
    GridColumns.ClosingPosition = Me.Grid_Results.Cols("ClosingPosition").SafeIndex
    GridColumns.NAV = Me.Grid_Results.Cols("NAV").SafeIndex
    GridColumns.ParentID = Me.Grid_Results.Cols("ParentID").SafeIndex
    GridColumns.Counter = Me.Grid_Results.Cols("Counter").SafeIndex

  End Sub

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
    Get
      Return False
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
    Get
      Return True
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value
    End Set
  End Property

  Private Property FormToUpdate() As Boolean
    Get
      Return _FormToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        If (FormToUpdate_Object IsNot Nothing) Then
          FormToUpdate_Object.FormToUpdate = True
        End If
      End If

      _FormToUpdate = value
    End Set
  End Property

#End Region

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _DefaultStatsDatePeriod = pMainForm.DefaultStatsDatePeriod

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = GenoaFormID.frmTradeHistory

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_SelectInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SelectInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SelectInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SelectInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_SelectInstrument.SelectedIndexChanged, AddressOf FormParameterChanged
    'AddHandler Date_StartDate.ValueChanged, AddressOf FormParameterChanged
    'AddHandler Date_EndDate.ValueChanged, AddressOf FormParameterChanged
    AddHandler Numeric_NAV.ValueChanged, AddressOf FormParameterChanged
    AddHandler Date_NAV_Date.ValueChanged, AddressOf FormParameterChanged

    Call Init_GridColumnsClass()

    Call SetPertracCombo()

    If (Combo_SelectInstrument.Items.Count > 0) Then
      Combo_SelectInstrument.SelectedIndex = 0
    End If

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' Initialise Chart Arraylists

    ExposureChartArrayList.Add(Me.Chart_Exposure)
    TurnoverChartArrayList.Add(Me.Chart_Turnover)

    '
    Grid_Results.Styles.Add("Positive").ForeColor = Color.Black
    Grid_Results.Styles.Add("Negative").ForeColor = Color.Red


    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
    End Try

    ' Initialise Controls

    Try

      Date_StartDate.Value = RenaissanceGlobals.Globals.Renaissance_BaseDate
      Date_EndDate.Value = Now.Date

      Radio_NAV_AtStart.Checked = True
      Radio_SortByDate.Checked = True

      Numeric_NAV.Value = 1000000

      FormToUpdate_Object = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

      Check_ShowBuyTurnover.Checked = True
      Check_ShowSellTurnover.Checked = True
      Check_ShowGrossTurnover.Checked = True
      Check_ShowTurnoverAsPercentage.Checked = False

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_SelectInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub

  Private Function FormUpdate_Tick() As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InFormUpdate_Tick) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try

    Try
      InFormUpdate_Tick = True

      PaintExposureCharts()
      PaintTurnoverCharts()
      SetExposureGridRows()

      FormToUpdate = False

    Catch ex As Exception
    Finally
      InFormUpdate_Tick = False
    End Try

    Return True

  End Function


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'GenoaAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    '' Changes to the tblGenoaItems table :-
    'If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaItems) = True) Or KnowledgeDateChanged Then

    '	' Re-Set combo.
    '	Call SetEntityCombo()
    'End If

    ' Changes to the tblGenoaItems table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetPertracCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetPertracCombo()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim SelectCommand As New SqlCommand
    Dim SelectData As New DataTable

    Try

      SelectCommand.CommandText = "adp_tblMastername_SelectDeltaInstruments"
      SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
      SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
      SelectCommand.Connection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)

      SelectData.Load(SelectCommand.ExecuteReader)

      MainForm.SetTblGenericCombo(Combo_SelectInstrument, SelectData.Select("True", "Mastername"), "Mastername", "ID", "")

    Catch ex As Exception

    Finally
      SelectCommand.Connection = Nothing
    End Try

  End Sub

  Private Sub FormParameterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      Grid_Results.Rows.Count = 1
      CheckedList_Exposure.Items.Clear()
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Date_StartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_StartDate.ValueChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormToUpdate = True
        'PaintExposureCharts()
        'PaintTurnoverCharts()
        'SetExposureGridRows()
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Date_EndDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_EndDate.ValueChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormToUpdate = True
        'PaintExposureCharts()
        'PaintTurnoverCharts()
        'SetExposureGridRows()
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

  Private Sub Butto_Refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Butto_Refresh.Click
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    GatherFormData()
    PaintExposureCharts()
    PaintTurnoverCharts()

  End Sub

  Private Sub Radio_NAV_AtStart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_NAV_AtStart.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormParameterChanged(Nothing, Nothing)

        If (Radio_NAV_AtStart.Checked) Then
          Radio_NAV_Now.Checked = False
          Radio_NAV_OnDate.Checked = False
          Date_NAV_Date.Enabled = False
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_NAV_Now_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_NAV_Now.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormParameterChanged(Nothing, Nothing)

        If (Radio_NAV_Now.Checked) Then
          Radio_NAV_AtStart.Checked = False
          Radio_NAV_OnDate.Checked = False
          Date_NAV_Date.Enabled = False
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_NAV_OnDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_NAV_OnDate.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormParameterChanged(Nothing, Nothing)

        If (Radio_NAV_OnDate.Checked) Then
          Radio_NAV_Now.Checked = False
          Radio_NAV_AtStart.Checked = False
          Date_NAV_Date.Enabled = True
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_SortByDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SortByDate.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        ' FormParameterChanged(Nothing, Nothing)
        FormToUpdate = True

        If (Radio_SortByDate.Checked) Then
          Radio_SortByInstrument.Checked = False
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_SortByInstrument_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SortByInstrument.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        ' FormParameterChanged(Nothing, Nothing)
        FormToUpdate = True

        If (Radio_SortByInstrument.Checked) Then
          Radio_SortByDate.Checked = False
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_NAV_OnDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Radio_NAV_OnDate.Click, Radio_NAV_Now.Click, Radio_NAV_AtStart.Click, Radio_SortByDate.Click, Radio_SortByInstrument.Click

    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      Dim ThisRadio As RadioButton

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (TypeOf sender Is RadioButton) Then
          ThisRadio = CType(sender, RadioButton)

          If (Not ThisRadio.Checked) Then
            ThisRadio.Checked = True
          End If
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_ShowGrossTurnover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowGrossTurnover.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        PaintTurnoverCharts()
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_ShowBuyTurnover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowBuyTurnover.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        PaintTurnoverCharts()
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_ShowSellTurnover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowSellTurnover.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        PaintTurnoverCharts()
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_ShowTurnoverAsPercentage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowTurnoverAsPercentage.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        PaintTurnoverCharts()
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Results_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Results.AfterDragColumn
    ' **********************************************************************************
    ' Handle the Moving of Grid columns
    '
    ' Simply re-set the values in the GridColumns class
    '
    ' **********************************************************************************
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Call Init_GridColumnsClass()
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Results_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Results.VisibleChanged
    ' **********************************************************************************
    '
    ' **********************************************************************************
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (Grid_Results.Visible) AndAlso (Grid_Results.Rows.Count <= 1) Then
          SetExposureGridRows()
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub CheckedList_Exposure_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedList_Exposure.ItemCheck
    ' **********************************************************************************
    '
    ' **********************************************************************************
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormToUpdate = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub CheckedList_Exposure_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CheckedList_Exposure.KeyPress
    ' **********************************************************************************
    '
    ' **********************************************************************************
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (e.KeyChar = " ") Then
          FormToUpdate = True
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub CheckedList_Exposure_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckedList_Exposure.SelectedIndexChanged
    ' **********************************************************************************
    '
    ' **********************************************************************************
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        FormToUpdate = True
      End If
    Catch ex As Exception
    End Try
  End Sub

#End Region

  Dim InstrumentID As Integer
  Dim InstrumentDescription As String
  Dim InstrumentDateSeries() As Date
  Dim InstrumentNAVSeries() As Double
  ' Dim InstrumentNAVSeries_Normalised() As Double
  Dim InstrumentReturnSeries() As Double
  Dim InstrumentDeltasSeries() As RenaissancePertracAndStatsGlobals.NetDeltaItemClass
  Dim InstrumentGrossDelta() As Double
  Dim InstrumentNetDelta() As Double
  Dim InstrumentTurnover() As Double
  Dim InstrumentBUYTurnover() As Double
  Dim InstrumentSELLTurnover() As Double
  Dim InstrumentTurnover_Percent() As Double
  Dim InstrumentBUYTurnover_Percent() As Double
  Dim InstrumentSELLTurnover_Percent() As Double
  Dim InstrumentNativeDataPeriod As DealingPeriod
  Dim InstrumentNativeAnnualPeriod As Integer
  Dim UnderlyingNAVSeries() As Array
  Dim UnderlyingDateSeries() As Array
  Dim UnderlyingInstrumentNames(-1) As String
  Dim UnderlyingInstrumentStartDateIndex(-1) As Integer

#Region " Charts Code"

  Private Sub Chart_Exposure_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Exposure.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintExposureCharts
    newChartForm.FormChart = Chart_Exposure
    newChartForm.ParentChartList = ExposureChartArrayList
    newChartForm.Text = "Exposure Chart"

    SyncLock ExposureChartArrayList
      ExposureChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    PaintExposureCharts(newChartForm.DisplayedChart)

  End Sub

  Private Sub Chart_Exposure_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Exposure.VisibleChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
          thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

          If thisChart.Visible Then
            PaintExposureCharts(thisChart)
          End If

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Chart_Turnover_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Turnover.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintTurnoverCharts
    newChartForm.FormChart = Chart_Turnover
    newChartForm.ParentChartList = TurnoverChartArrayList
    newChartForm.Text = "Turnover Chart"

    SyncLock TurnoverChartArrayList
      TurnoverChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    PaintTurnoverCharts(newChartForm.DisplayedChart)

  End Sub

  Private Sub Chart_Turnover_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Turnover.VisibleChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
          thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

          If thisChart.Visible Then
            PaintTurnoverCharts(thisChart)
          End If

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub PaintExposureCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    ' MainForm.DebugPrint("PaintExposureCharts : " & Now().ToString)

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines

        If (InstrumentID <= 0) Then
          For Each thisSeries As Dundas.Charting.WinControl.Series In ThisChartOnly.Series
            thisSeries.ChartArea = ""
          Next
        Else

          For Each thisSeries As Dundas.Charting.WinControl.Series In ThisChartOnly.Series
            thisSeries.ChartArea = ""
          Next

          For ThisIndex As Integer = 0 To (CheckedList_Exposure.CheckedIndices.Count - 1)

            Select Case CheckedList_Exposure.CheckedIndices(ThisIndex)

              Case 0 ' NET

                Set_LineChart(MainForm, InstrumentDateSeries, InstrumentNetDelta, ThisChartOnly, ThisIndex, "NET Delta", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")

              Case 1 ' GROSS

                Set_LineChart(MainForm, InstrumentDateSeries, InstrumentGrossDelta, ThisChartOnly, ThisIndex, "GROSS Delta", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")

              Case Else

                If (InstrumentDeltasSeries IsNot Nothing) AndAlso (InstrumentDeltasSeries.Length > CheckedList_Exposure.CheckedIndices(ThisIndex) - 2) Then
                  Set_LineChart(MainForm, InstrumentDateSeries, InstrumentDeltasSeries(CheckedList_Exposure.CheckedIndices(ThisIndex) - 2).DeltaArray, ThisChartOnly, ThisIndex, UnderlyingInstrumentNames(CheckedList_Exposure.CheckedIndices(ThisIndex) - 2), Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")
                End If

            End Select

          Next

        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock ExposureChartArrayList
        For Each thisChart In Me.ExposureChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines

            Try

              If (InstrumentID <= 0) Then
                For Each thisSeries As Dundas.Charting.WinControl.Series In thisChart.Series
                  thisSeries.ChartArea = ""
                Next
              Else

                For Each thisSeries As Dundas.Charting.WinControl.Series In thisChart.Series
                  thisSeries.ChartArea = ""
                Next

                For ThisIndex As Integer = 0 To (CheckedList_Exposure.CheckedIndices.Count - 1)

                  Select Case CheckedList_Exposure.CheckedIndices(ThisIndex)

                    Case 0 ' NET

                      Set_LineChart(MainForm, InstrumentDateSeries, InstrumentNetDelta, thisChart, ThisIndex, "NET Delta", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")

                    Case 1 ' GROSS

                      Set_LineChart(MainForm, InstrumentDateSeries, InstrumentGrossDelta, thisChart, ThisIndex, "GROSS Delta", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")

                    Case Else

                      If (InstrumentDeltasSeries IsNot Nothing) AndAlso (InstrumentDeltasSeries.Length > CheckedList_Exposure.CheckedIndices(ThisIndex) - 2) Then
                        Set_LineChart(MainForm, InstrumentDateSeries, InstrumentDeltasSeries(CheckedList_Exposure.CheckedIndices(ThisIndex) - 2).DeltaArray, thisChart, ThisIndex, UnderlyingInstrumentNames(CheckedList_Exposure.CheckedIndices(ThisIndex) - 2), Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft")
                      End If

                  End Select

                Next

              End If

            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintTurnoverCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines

        If (InstrumentID <= 0) Then
          For Each thisSeries As Dundas.Charting.WinControl.Series In ThisChartOnly.Series
            thisSeries.ChartArea = ""
          Next
        Else

          For Each thisSeries As Dundas.Charting.WinControl.Series In ThisChartOnly.Series
            thisSeries.ChartArea = ""
          Next

          Dim SeriesCount As Integer = 0

          If (Check_ShowTurnoverAsPercentage.Checked) Then

            ThisChartOnly.ChartAreas(0).AxisY.LabelStyle.Format = "#,##0%"

            If (Check_ShowGrossTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentTurnover_Percent, ThisChartOnly, SeriesCount, "GROSS", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
              SeriesCount += 1
            End If
            If (Check_ShowBuyTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentBUYTurnover_Percent, ThisChartOnly, SeriesCount, "BUY", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
              SeriesCount += 1
            End If
            If (Check_ShowSellTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentSELLTurnover_Percent, ThisChartOnly, SeriesCount, "SELL", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
              SeriesCount += 1
            End If
          Else

            ThisChartOnly.ChartAreas(0).AxisY.LabelStyle.Format = "#,##0"

            If (Check_ShowGrossTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentTurnover, ThisChartOnly, SeriesCount, "GROSS", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
              SeriesCount += 1
            End If
            If (Check_ShowBuyTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentBUYTurnover, ThisChartOnly, SeriesCount, "BUY", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
              SeriesCount += 1
            End If
            If (Check_ShowSellTurnover.Checked) Then
              Set_BarChart(MainForm, InstrumentDateSeries, InstrumentSELLTurnover, ThisChartOnly, SeriesCount, "SELL", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
              SeriesCount += 1
            End If
          End If

        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock TurnoverChartArrayList
        For Each thisChart In Me.TurnoverChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines

            Try

              If (InstrumentID <= 0) Then
                For Each thisSeries As Dundas.Charting.WinControl.Series In thisChart.Series
                  thisSeries.ChartArea = ""
                Next
              Else

                For Each thisSeries As Dundas.Charting.WinControl.Series In thisChart.Series
                  thisSeries.ChartArea = ""
                Next

                Dim SeriesCount As Integer = 0

                If (Check_ShowTurnoverAsPercentage.Checked) Then
                  thisChart.ChartAreas(0).AxisY.LabelStyle.Format = "#,##0%"

                  If (Check_ShowGrossTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentTurnover_Percent, thisChart, SeriesCount, "GROSS", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                  If (Check_ShowBuyTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentBUYTurnover_Percent, thisChart, SeriesCount, "BUY", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                  If (Check_ShowSellTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentSELLTurnover_Percent, thisChart, SeriesCount, "SELL", Date_StartDate.Value, Date_EndDate.Value, 100.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                Else
                  thisChart.ChartAreas(0).AxisY.LabelStyle.Format = "#,##0"

                  If (Check_ShowGrossTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentTurnover, thisChart, SeriesCount, "GROSS", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                  If (Check_ShowBuyTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentBUYTurnover, thisChart, SeriesCount, "BUY", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                  If (Check_ShowSellTurnover.Checked) Then
                    Set_BarChart(MainForm, InstrumentDateSeries, InstrumentSELLTurnover, thisChart, SeriesCount, "SELL", Date_StartDate.Value, Date_EndDate.Value, 1.0#, "TopLeft", False)
                    SeriesCount += 1
                  End If
                End If
              End If

            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

#End Region

  Private Sub GatherFormData()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      Dim DisplayStartDate As Date
      Dim DisplayEndDate As Date
      Dim NAVDate As Date
      Dim NAVDateIndex As Integer
      Dim NAV_Reference As Double

      Dim Counter As Integer

      If (Combo_SelectInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_SelectInstrument.SelectedValue)) Then
        InstrumentID = CInt(Combo_SelectInstrument.SelectedValue)
      Else
        InstrumentID = 0
      End If

      DisplayStartDate = MaxFunctions.MAX(Date_StartDate.Value, RenaissanceGlobals.Globals.Renaissance_BaseDate)
      DisplayEndDate = MaxFunctions.MIN(Date_EndDate.Value, Now.Date)
      NAV_Reference = Numeric_NAV.Value

      If (Radio_NAV_Now.Checked) Then
        NAVDate = MaxFunctions.MAX(MaxFunctions.MIN(DisplayEndDate, Now.Date), DisplayStartDate)
      ElseIf (Radio_NAV_OnDate.Checked) Then
        NAVDate = MaxFunctions.MAX(MaxFunctions.MIN(DisplayEndDate, Date_NAV_Date.Value), DisplayStartDate)
      Else
        NAVDate = DisplayStartDate
      End If

      ' 

      Grid_Results.Redraw = False

      Try
        ' Clear Grid & Charts , to start

        Grid_Results.Rows.Count = 1
        CheckedList_Exposure.Items.Clear()

        If (InstrumentID <= 0) Then
          InstrumentGrossDelta = Nothing
          InstrumentNetDelta = Nothing
          InstrumentTurnover = Nothing
          InstrumentBUYTurnover = Nothing
          InstrumentSELLTurnover = Nothing
          InstrumentTurnover_Percent = Nothing
          InstrumentBUYTurnover_Percent = Nothing
          InstrumentSELLTurnover_Percent = Nothing

          Exit Sub
        End If

        InstrumentDescription = MainForm.PertracData.GetInformationValue(InstrumentID, PertracInformationFields.Mastername).ToString

        ' Add Header Line.

        Grid_Results.Rows.Add()
        Grid_Results(Grid_Results.Rows.Count - 1, GridColumns.FirstCol) = InstrumentDescription
        Grid_Results.Rows(Grid_Results.Rows.Count - 1).IsNode = True
        Grid_Results.Rows(Grid_Results.Rows.Count - 1).Node.Level = 0

        ' Get Data

        InstrumentNativeDataPeriod = MainForm.PertracData.GetPertracDataPeriod(InstrumentID)
        InstrumentNativeAnnualPeriod = MainForm.PertracData.AnnualPeriodCount(InstrumentNativeDataPeriod)
        InstrumentDateSeries = MainForm.StatFunctions.DateSeries(InstrumentNativeDataPeriod, InstrumentID, False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
        InstrumentNAVSeries = MainForm.StatFunctions.NAVSeries(InstrumentNativeDataPeriod, InstrumentID, False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
        InstrumentReturnSeries = MainForm.StatFunctions.ReturnSeries(InstrumentNativeDataPeriod, InstrumentID, False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
        InstrumentDeltasSeries = MainForm.StatFunctions.NetDeltas(InstrumentNativeDataPeriod, CULng(InstrumentID), False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data, True)

        ' Exit if no data

        If (InstrumentDateSeries Is Nothing) OrElse (InstrumentDateSeries.Length <= 0) Then
          Exit Sub
        End If

        ' Generate Normalised NAV

        Dim NAV_Multiplier As Double

        InstrumentGrossDelta = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentNetDelta = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentTurnover = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentBUYTurnover = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentSELLTurnover = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentTurnover_Percent = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentBUYTurnover_Percent = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())
        InstrumentSELLTurnover_Percent = CType(Array.CreateInstance(GetType(Double), InstrumentNAVSeries.Length), Double())

        If (Radio_NAV_Now.Checked) OrElse (Radio_NAV_OnDate.Checked) Then

          NAVDateIndex = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), NAVDate)), (InstrumentNAVSeries.Length - 1))
          NAV_Multiplier = NAV_Reference / InstrumentNAVSeries(NAVDateIndex)

        Else ' Default - At Start

          NAV_Multiplier = NAV_Reference / InstrumentNAVSeries(0)

        End If

        For Counter = 0 To (InstrumentNAVSeries.Length - 1)
          InstrumentNAVSeries(Counter) *= NAV_Multiplier
        Next

        ' 

        'Dim LevelOneGridRow As C1.Win.C1FlexGrid.Row
        'Dim LevelTwoGridRow As C1.Win.C1FlexGrid.Row

        ReDim UnderlyingInstrumentNames(InstrumentDeltasSeries.Length - 1)
        ReDim UnderlyingInstrumentStartDateIndex(InstrumentDeltasSeries.Length - 1)
        Dim ThisUnderlyingDelta As Double
        Dim LastUnderlyingDelta As Double

        Dim FirstIndexStart As Integer
        Dim FirstIndexTarget As Integer
        Dim SecondIndexStart As Integer
        Dim SecondIndexTarget As Integer
        Dim FirstIndex As Integer
        Dim SecondIndex As Integer

        ' Set Charts Grid.

        CheckedList_Exposure.Items.Add("NET Exposure", False)
        CheckedList_Exposure.Items.Add("GROSS Exposure", False)

        CheckedList_Exposure.SetItemChecked(0, True)
        CheckedList_Exposure.SetItemChecked(1, True)

        '

        UnderlyingNAVSeries = CType(Array.CreateInstance(GetType(Array), InstrumentDeltasSeries.Length), Array)
        UnderlyingDateSeries = CType(Array.CreateInstance(GetType(Array), InstrumentDeltasSeries.Length), Array)

        For Counter = 0 To (InstrumentDeltasSeries.Length - 1)
          UnderlyingInstrumentNames(Counter) = MainForm.PertracData.GetInformationValue(CInt(InstrumentDeltasSeries(Counter).PertracId), PertracInformationFields.Mastername).ToString
          UnderlyingDateSeries(Counter) = MainForm.StatFunctions.DateSeries(InstrumentNativeDataPeriod, CInt(InstrumentDeltasSeries(Counter).PertracId), False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
          UnderlyingNAVSeries(Counter) = MainForm.StatFunctions.NAVSeries(InstrumentNativeDataPeriod, CInt(InstrumentDeltasSeries(Counter).PertracId), False, InstrumentNativeAnnualPeriod, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)

          If (UnderlyingDateSeries(Counter) IsNot Nothing) AndAlso (UnderlyingDateSeries(Counter).Length > 0) Then
            UnderlyingInstrumentStartDateIndex(Counter) = GetPriceIndex(InstrumentNativeDataPeriod, CDate(UnderlyingDateSeries(Counter)(0)), InstrumentDateSeries(0))
          Else
            UnderlyingDateSeries(Counter) = New Date() {}
            UnderlyingInstrumentStartDateIndex(Counter) = 1
          End If

          CheckedList_Exposure.Items.Add(UnderlyingInstrumentNames(Counter), False)

        Next

        If (Radio_SortByInstrument.Checked) Then
          ' FirstIndex is Instrument, Second is Date

          FirstIndexStart = 0
          FirstIndexTarget = InstrumentDeltasSeries.Length - 1
          'SecondIndexTarget = InstrumentDateSeries.Length - 1
          SecondIndexStart = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayStartDate)), InstrumentDateSeries.Length - 1)
          SecondIndexTarget = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayEndDate)), InstrumentDateSeries.Length - 1)

        Else
          ' FirstIndex is Date, Second is Instrument

          'FirstIndexTarget = InstrumentDateSeries.Length - 1
          FirstIndexStart = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayStartDate)), InstrumentDateSeries.Length - 1)
          FirstIndexTarget = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayEndDate)), InstrumentDateSeries.Length - 1)
          SecondIndexStart = 0
          SecondIndexTarget = InstrumentDeltasSeries.Length - 1

        End If

        '  Set Reporting Deltas.

        For FirstIndex = 0 To (InstrumentDateSeries.Length - 1)

          InstrumentGrossDelta(FirstIndex) = 0.0#
          InstrumentNetDelta(FirstIndex) = 0.0#
          InstrumentTurnover(FirstIndex) = 0.0#
          InstrumentBUYTurnover(FirstIndex) = 0.0#
          InstrumentSELLTurnover(FirstIndex) = 0.0#
          InstrumentTurnover_Percent(FirstIndex) = 0.0#
          InstrumentBUYTurnover_Percent(FirstIndex) = 0.0#
          InstrumentSELLTurnover_Percent(FirstIndex) = 0.0#

          For SecondIndex = 0 To (InstrumentDeltasSeries.Length - 1)

            ThisUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(FirstIndex)
            If (FirstIndex > 0) Then
              LastUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(FirstIndex - 1)
            Else
              LastUnderlyingDelta = 0.0#
            End If

            InstrumentGrossDelta(FirstIndex) += Math.Abs(ThisUnderlyingDelta)
            InstrumentNetDelta(FirstIndex) += ThisUnderlyingDelta
            InstrumentTurnover(FirstIndex) += Math.Abs((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex))

            If (ThisUnderlyingDelta - LastUnderlyingDelta) > 0.0# Then
              InstrumentBUYTurnover(FirstIndex) += ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex))
            Else
              InstrumentSELLTurnover(FirstIndex) += ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex))
            End If
          Next

          InstrumentTurnover_Percent(FirstIndex) = InstrumentTurnover(FirstIndex) / InstrumentNAVSeries(FirstIndex)
          InstrumentBUYTurnover_Percent(FirstIndex) = InstrumentBUYTurnover(FirstIndex) / InstrumentNAVSeries(FirstIndex)
          InstrumentSELLTurnover_Percent(FirstIndex) = InstrumentSELLTurnover(FirstIndex) / InstrumentNAVSeries(FirstIndex)

        Next

        ' Set Grid.

        SetExposureGridRows()

        ''Dim SumGrossExposure As Double
        ''Dim SumNetExposure As Double
        'Dim SumGrossTrades As Double
        'Dim UnderlyingPriceIndex As Integer

        'For FirstIndex = FirstIndexStart To FirstIndexTarget

        '  'SumGrossExposure = 0.0#
        '  'SumNetExposure = 0.0#
        '  SumGrossTrades = 0.0#

        '  ' Add Instrument / Date Row

        '  If (Radio_SortByInstrument.Checked) Then

        '    LevelOneGridRow = Grid_Results.Rows.Add()
        '    LevelOneGridRow(GridColumns.FirstCol) = UnderlyingInstrumentNames(FirstIndex)
        '    LevelOneGridRow(GridColumns.NAV) = ""
        '    LevelOneGridRow.IsNode = True
        '    LevelOneGridRow.Node.Level = 1

        '    LevelOneGridRow(GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

        '  Else ' By Date

        '    LevelOneGridRow = Grid_Results.Rows.Add()
        '    LevelOneGridRow(GridColumns.FirstCol) = InstrumentDateSeries(FirstIndex).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
        '    LevelOneGridRow(GridColumns.NAV) = InstrumentNAVSeries(FirstIndex).ToString("#,##0")
        '    LevelOneGridRow.IsNode = True
        '    LevelOneGridRow.Node.Level = 1

        '    ' LevelOneGridRow(GridColumns.FundID) = XXX
        '    LevelOneGridRow(GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

        '  End If

        '  For SecondIndex = SecondIndexStart To SecondIndexTarget

        '    ' Add Date / Instrument Row

        '    If (Radio_SortByInstrument.Checked) Then

        '      ThisUnderlyingDelta = InstrumentDeltasSeries(FirstIndex).DeltaArray(SecondIndex)
        '      LastUnderlyingDelta = InstrumentDeltasSeries(FirstIndex).DeltaArray(Math.Max(0, SecondIndex - 1))
        '      UnderlyingPriceIndex = Math.Max(Math.Min(SecondIndex + UnderlyingInstrumentStartDateIndex(FirstIndex), UnderlyingNAVSeries(FirstIndex).Length - 1), 0)
        '      'InstrumentGrossDelta(SecondIndex) += Math.Abs(ThisUnderlyingDelta)
        '      'InstrumentNetDelta(SecondIndex) += ThisUnderlyingDelta

        '      LevelTwoGridRow = Grid_Results.Rows.Add()
        '      LevelTwoGridRow(GridColumns.FirstCol) = InstrumentDateSeries(SecondIndex).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
        '      LevelTwoGridRow(GridColumns.Delta) = (ThisUnderlyingDelta).ToString("#,##0.00%")
        '      LevelTwoGridRow(GridColumns.LastDelta) = (LastUnderlyingDelta).ToString("#,##0.00%")
        '      LevelTwoGridRow(GridColumns.Trade) = ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(SecondIndex)).ToString("#,##0")
        '      LevelTwoGridRow(GridColumns.ClosingPrice) = CDbl(UnderlyingNAVSeries(FirstIndex)(UnderlyingPriceIndex)).ToString("#,##0.00##")
        '      LevelTwoGridRow.IsNode = False
        '      LevelTwoGridRow.Node.Level = 2

        '    Else ' First by Date, Then Instrument

        '      ThisUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(FirstIndex)
        '      LastUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(Math.Max(0, FirstIndex - 1))
        '      UnderlyingPriceIndex = Math.Max(Math.Min(FirstIndex + UnderlyingInstrumentStartDateIndex(SecondIndex), UnderlyingNAVSeries(SecondIndex).Length - 1), 0)
        '      'InstrumentGrossDelta(FirstIndex) += Math.Abs(ThisUnderlyingDelta)
        '      'InstrumentNetDelta(FirstIndex) += ThisUnderlyingDelta

        '      'SumGrossExposure += Math.Abs(ThisUnderlyingDelta)
        '      'SumNetExposure += ThisUnderlyingDelta
        '      SumGrossTrades += Math.Abs(((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex)))

        '      LevelTwoGridRow = Grid_Results.Rows.Add()
        '      LevelTwoGridRow(GridColumns.FirstCol) = UnderlyingInstrumentNames(SecondIndex)
        '      LevelTwoGridRow(GridColumns.Delta) = (ThisUnderlyingDelta).ToString("#,##0.00%")
        '      LevelTwoGridRow(GridColumns.LastDelta) = (LastUnderlyingDelta).ToString("#,##0.00%")
        '      LevelTwoGridRow(GridColumns.Trade) = ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex)).ToString("#,##0")
        '      LevelTwoGridRow(GridColumns.ClosingPrice) = CDbl(UnderlyingNAVSeries(SecondIndex)(UnderlyingPriceIndex)).ToString("#,##0.00##")

        '      LevelTwoGridRow.IsNode = False
        '      LevelTwoGridRow.Node.Level = 2

        '    End If

        '  Next

        '  If (Not Radio_SortByInstrument.Checked) Then

        '    'LevelOneGridRow(GridColumns.LastDelta) = (SumGrossExposure).ToString("#,##0.00%")
        '    'LevelOneGridRow(GridColumns.Delta) = (SumNetExposure).ToString("#,##0.00%")
        '    'LevelOneGridRow(GridColumns.Trade) = (SumGrossTrades).ToString("#,##0")

        '    LevelOneGridRow(GridColumns.LastDelta) = InstrumentGrossDelta(FirstIndex).ToString("#,##0.00%")
        '    LevelOneGridRow(GridColumns.Delta) = InstrumentNetDelta(FirstIndex).ToString("#,##0.00%")
        '    LevelOneGridRow(GridColumns.Trade) = (SumGrossTrades).ToString("#,##0")

        '  End If

        '  'LevelTwoGridRow = Grid_Results.Rows.Add()
        '  'LevelTwoGridRow(GridColumns.FirstCol) = ""
        '  'LevelTwoGridRow.IsNode = False
        '  'LevelTwoGridRow.Node.Level = 2

        '  LevelOneGridRow.Node.Expanded = False

        'Next


      Catch ex As Exception
      End Try

    Catch ex As Exception
    Finally
      Grid_Results.Redraw = True
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub SetExposureGridRows()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim InitialCursor As Cursor = Me.Cursor

    If (Not Me.Grid_Results.Visible) Then
      Grid_Results.Rows.Count = 1
      Exit Sub
    End If

    Try
      Me.Cursor = Cursors.WaitCursor
      Grid_Results.Redraw = False

      Grid_Results.Rows.Count = 1

      If (InstrumentDateSeries Is Nothing) OrElse (InstrumentDateSeries.Length <= 0) Then
        Exit Sub
      End If

      Dim DisplayStartDate As Date
      Dim DisplayEndDate As Date

      Dim LevelOneGridRow As C1.Win.C1FlexGrid.Row
      Dim LevelTwoGridRow As C1.Win.C1FlexGrid.Row
      Dim ThisUnderlyingDelta As Double
      Dim LastUnderlyingDelta As Double

      Dim FirstIndexStart As Integer
      Dim FirstIndexTarget As Integer
      Dim SecondIndexStart As Integer
      Dim SecondIndexTarget As Integer
      Dim FirstIndex As Integer
      Dim SecondIndex As Integer

      DisplayStartDate = MaxFunctions.MAX(Date_StartDate.Value, RenaissanceGlobals.Globals.Renaissance_BaseDate)
      DisplayEndDate = MaxFunctions.MIN(Date_EndDate.Value, Now.Date)

      If (Radio_SortByInstrument.Checked) Then
        ' FirstIndex is Instrument, Second is Date

        FirstIndexStart = 0
        FirstIndexTarget = InstrumentDeltasSeries.Length - 1
        'SecondIndexTarget = InstrumentDateSeries.Length - 1
        SecondIndexStart = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayStartDate)), InstrumentDateSeries.Length - 1)
        SecondIndexTarget = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayEndDate)), InstrumentDateSeries.Length - 1)

      Else
        ' FirstIndex is Date, Second is Instrument

        'FirstIndexTarget = InstrumentDateSeries.Length - 1
        FirstIndexStart = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayStartDate)), InstrumentDateSeries.Length - 1)
        FirstIndexTarget = Math.Min(Math.Max(0, GetPriceIndex(InstrumentNativeDataPeriod, InstrumentDateSeries(0), DisplayEndDate)), InstrumentDateSeries.Length - 1)
        SecondIndexStart = 0
        SecondIndexTarget = InstrumentDeltasSeries.Length - 1

      End If

      ' Set Grid.

      'Dim SumGrossExposure As Double
      'Dim SumNetExposure As Double
      Dim SumGrossTrades As Double
      Dim UnderlyingPriceIndex As Integer

      For FirstIndex = FirstIndexStart To FirstIndexTarget

        'SumGrossExposure = 0.0#
        'SumNetExposure = 0.0#
        SumGrossTrades = 0.0#

        ' Add Instrument / Date Row

        If (Radio_SortByInstrument.Checked) Then

          LevelOneGridRow = Grid_Results.Rows.Add()
          LevelOneGridRow(GridColumns.FirstCol) = UnderlyingInstrumentNames(FirstIndex)
          LevelOneGridRow(GridColumns.NAV) = ""
          LevelOneGridRow.IsNode = True
          LevelOneGridRow.Node.Level = 1

          LevelOneGridRow(GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

        Else ' By Date

          LevelOneGridRow = Grid_Results.Rows.Add()
          LevelOneGridRow(GridColumns.FirstCol) = InstrumentDateSeries(FirstIndex).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
          LevelOneGridRow(GridColumns.NAV) = InstrumentNAVSeries(FirstIndex).ToString("#,##0")
          LevelOneGridRow.IsNode = True
          LevelOneGridRow.Node.Level = 1

          ' LevelOneGridRow(GridColumns.FundID) = XXX
          LevelOneGridRow(GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

        End If

        For SecondIndex = SecondIndexStart To SecondIndexTarget

          ' Add Date / Instrument Row

          If (Radio_SortByInstrument.Checked) Then

            ThisUnderlyingDelta = InstrumentDeltasSeries(FirstIndex).DeltaArray(SecondIndex)
            LastUnderlyingDelta = InstrumentDeltasSeries(FirstIndex).DeltaArray(Math.Max(0, SecondIndex - 1))
            UnderlyingPriceIndex = Math.Max(Math.Min(SecondIndex + UnderlyingInstrumentStartDateIndex(FirstIndex), UnderlyingNAVSeries(FirstIndex).Length - 1), 0)
            'InstrumentGrossDelta(SecondIndex) += Math.Abs(ThisUnderlyingDelta)
            'InstrumentNetDelta(SecondIndex) += ThisUnderlyingDelta

            LevelTwoGridRow = Grid_Results.Rows.Add()
            LevelTwoGridRow(GridColumns.FirstCol) = InstrumentDateSeries(SecondIndex).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
            LevelTwoGridRow(GridColumns.Delta) = (ThisUnderlyingDelta).ToString("#,##0.00%")
            LevelTwoGridRow(GridColumns.LastDelta) = (LastUnderlyingDelta).ToString("#,##0.00%")
            LevelTwoGridRow(GridColumns.Trade) = ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(SecondIndex)).ToString("#,##0")
            LevelTwoGridRow(GridColumns.ClosingPrice) = CDbl(UnderlyingNAVSeries(FirstIndex)(UnderlyingPriceIndex)).ToString("#,##0.00##")

            If (SecondIndex > 0) Then
              LevelTwoGridRow(GridColumns.OpeningPosition) = (LastUnderlyingDelta * InstrumentNAVSeries(SecondIndex - 1)).ToString("#,##0")
            Else
              LevelTwoGridRow(GridColumns.OpeningPosition) = "0"
            End If
            LevelTwoGridRow(GridColumns.ClosingPosition) = (ThisUnderlyingDelta * InstrumentNAVSeries(SecondIndex)).ToString("#,##0")

            LevelTwoGridRow.IsNode = False
            LevelTwoGridRow.Node.Level = 2

            If (ThisUnderlyingDelta < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Delta, "Negative")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.ClosingPosition, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Delta, "Positive")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.ClosingPosition, "Positive")
            End If

            If (LastUnderlyingDelta < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.LastDelta, "Negative")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.OpeningPosition, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.LastDelta, "Positive")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.OpeningPosition, "Positive")
            End If

            If ((ThisUnderlyingDelta - LastUnderlyingDelta) < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Trade, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Trade, "Positive")
            End If

          Else ' First by Date, Then Instrument

            ThisUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(FirstIndex)
            LastUnderlyingDelta = InstrumentDeltasSeries(SecondIndex).DeltaArray(Math.Max(0, FirstIndex - 1))
            UnderlyingPriceIndex = Math.Max(Math.Min(FirstIndex + UnderlyingInstrumentStartDateIndex(SecondIndex), UnderlyingNAVSeries(SecondIndex).Length - 1), 0)
            'InstrumentGrossDelta(FirstIndex) += Math.Abs(ThisUnderlyingDelta)
            'InstrumentNetDelta(FirstIndex) += ThisUnderlyingDelta

            'SumGrossExposure += Math.Abs(ThisUnderlyingDelta)
            'SumNetExposure += ThisUnderlyingDelta
            SumGrossTrades += Math.Abs(((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex)))

            LevelTwoGridRow = Grid_Results.Rows.Add()
            LevelTwoGridRow(GridColumns.FirstCol) = UnderlyingInstrumentNames(SecondIndex)
            LevelTwoGridRow(GridColumns.Delta) = (ThisUnderlyingDelta).ToString("#,##0.00%")
            LevelTwoGridRow(GridColumns.LastDelta) = (LastUnderlyingDelta).ToString("#,##0.00%")
            LevelTwoGridRow(GridColumns.Trade) = ((ThisUnderlyingDelta - LastUnderlyingDelta) * InstrumentNAVSeries(FirstIndex)).ToString("#,##0")
            LevelTwoGridRow(GridColumns.ClosingPrice) = CDbl(UnderlyingNAVSeries(SecondIndex)(UnderlyingPriceIndex)).ToString("#,##0.00##")

            If (FirstIndex > 0) Then
              LevelTwoGridRow(GridColumns.OpeningPosition) = (LastUnderlyingDelta * InstrumentNAVSeries(FirstIndex - 1)).ToString("#,##0")
            Else
              LevelTwoGridRow(GridColumns.OpeningPosition) = "0"
            End If
            LevelTwoGridRow(GridColumns.ClosingPosition) = (ThisUnderlyingDelta * InstrumentNAVSeries(FirstIndex)).ToString("#,##0")

            If (ThisUnderlyingDelta < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Delta, "Negative")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.ClosingPosition, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Delta, "Positive")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.ClosingPosition, "Positive")
            End If

            If (LastUnderlyingDelta < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.LastDelta, "Negative")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.OpeningPosition, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.LastDelta, "Positive")
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.OpeningPosition, "Positive")
            End If

            If ((ThisUnderlyingDelta - LastUnderlyingDelta) < 0.0#) Then
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Trade, "Negative")
            Else
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, GridColumns.Trade, "Positive")
            End If

            LevelTwoGridRow.IsNode = False
            LevelTwoGridRow.Node.Level = 2

          End If

        Next

        If (Not Radio_SortByInstrument.Checked) Then

          'LevelOneGridRow(GridColumns.LastDelta) = (SumGrossExposure).ToString("#,##0.00%")
          'LevelOneGridRow(GridColumns.Delta) = (SumNetExposure).ToString("#,##0.00%")
          'LevelOneGridRow(GridColumns.Trade) = (SumGrossTrades).ToString("#,##0")

          LevelOneGridRow(GridColumns.LastDelta) = InstrumentGrossDelta(FirstIndex).ToString("#,##0.00%")
          LevelOneGridRow(GridColumns.Delta) = InstrumentNetDelta(FirstIndex).ToString("#,##0.00%")
          ' LevelOneGridRow(GridColumns.Trade) = (SumGrossTrades).ToString("#,##0")

          LevelOneGridRow(GridColumns.Trade) = (InstrumentTurnover(FirstIndex)).ToString("#,##0")

        End If

        'LevelTwoGridRow = Grid_Results.Rows.Add()
        'LevelTwoGridRow(GridColumns.FirstCol) = ""
        'LevelTwoGridRow.IsNode = False
        'LevelTwoGridRow.Node.Level = 2

        LevelOneGridRow.Node.Expanded = False

      Next

    Catch ex As Exception
    Finally
      Grid_Results.Redraw = True
      Me.Cursor = InitialCursor
    End Try

  End Sub












End Class
