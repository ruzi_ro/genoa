
Imports System.IO
Imports System.Data.SqlClient
Imports System.Windows.Forms.Clipboard
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceControls
Imports FCP_TelerikControls
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions
Imports RenaissanceUtilities.DatePeriodFunctions
Imports C1.Win.C1FlexGrid

Public Class frmFundBrowser

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Friend WithEvents Radio_ExistingFunds As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ExistingGroups As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Pertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Combo_SelectFrom As System.Windows.Forms.ComboBox
  Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Split_Form As System.Windows.Forms.SplitContainer
  Friend WithEvents Tab_InstrumentDetails As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Descriptions As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Contacts As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Performance As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comparisons As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Charts As System.Windows.Forms.TabPage
  Friend WithEvents TabControl_Charts As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Vami As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Return As System.Windows.Forms.TabPage
  Friend WithEvents Tab_StdDev As System.Windows.Forms.TabPage
  Friend WithEvents Tab_DrawDown As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Monthly As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Correlation As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Quartile As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Statistics As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Prices As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Returns As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label_Strategy As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label_DataSource As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Label_FundName As System.Windows.Forms.Label
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Label_ManagementCo As System.Windows.Forms.Label
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents Text_Description As System.Windows.Forms.TextBox
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents Label_Redemption As System.Windows.Forms.Label
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Label_Subscription As System.Windows.Forms.Label
  Friend WithEvents Label16 As System.Windows.Forms.Label
  Friend WithEvents Label_MinInvestment As System.Windows.Forms.Label
  Friend WithEvents Label18 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents Label_Hurdle As System.Windows.Forms.Label
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Label_PerformanceFee As System.Windows.Forms.Label
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents Label_MgmtFee As System.Windows.Forms.Label
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
  Friend WithEvents Label_Currency As System.Windows.Forms.Label
  Friend WithEvents Label42 As System.Windows.Forms.Label
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents Label_InvestorType As System.Windows.Forms.Label
  Friend WithEvents Label26 As System.Windows.Forms.Label
  Friend WithEvents Label_Domicile As System.Windows.Forms.Label
  Friend WithEvents Label28 As System.Windows.Forms.Label
  Friend WithEvents Label_FirmAssets As System.Windows.Forms.Label
  Friend WithEvents Label30 As System.Windows.Forms.Label
  Friend WithEvents Label_FundAssets As System.Windows.Forms.Label
  Friend WithEvents Label32 As System.Windows.Forms.Label
  Friend WithEvents Label_Notice As System.Windows.Forms.Label
  Friend WithEvents Label20 As System.Windows.Forms.Label
  Friend WithEvents Label_Lockup As System.Windows.Forms.Label
  Friend WithEvents Label22 As System.Windows.Forms.Label
  Friend WithEvents Label_Administrator As System.Windows.Forms.Label
  Friend WithEvents Label34 As System.Windows.Forms.Label
  Friend WithEvents Label_Open As System.Windows.Forms.Label
  Friend WithEvents Label24 As System.Windows.Forms.Label
  Friend WithEvents Label_Address1 As System.Windows.Forms.Label
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Label_ManagementCo2 As System.Windows.Forms.Label
  Friend WithEvents Label15 As System.Windows.Forms.Label
  Friend WithEvents Label_FundName2 As System.Windows.Forms.Label
  Friend WithEvents Label19 As System.Windows.Forms.Label
  Friend WithEvents Label33 As System.Windows.Forms.Label
  Friend WithEvents Label31 As System.Windows.Forms.Label
  Friend WithEvents Label_Country As System.Windows.Forms.Label
  Friend WithEvents Label_State As System.Windows.Forms.Label
  Friend WithEvents Label_Zip As System.Windows.Forms.Label
  Friend WithEvents Label_City As System.Windows.Forms.Label
  Friend WithEvents Label_Address2 As System.Windows.Forms.Label
  Friend WithEvents Label23 As System.Windows.Forms.Label
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents Label_ContactFax As System.Windows.Forms.Label
  Friend WithEvents Label_ContactPhone As System.Windows.Forms.Label
  Friend WithEvents Label_Contact As System.Windows.Forms.Label
  Friend WithEvents Label25 As System.Windows.Forms.Label
  Friend WithEvents Label_ContactEMail As System.Windows.Forms.Label
  Friend WithEvents Label_MarketingFax As System.Windows.Forms.Label
  Friend WithEvents Label_MarketingContact As System.Windows.Forms.Label
  Friend WithEvents Label17 As System.Windows.Forms.Label
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Label_MarketingPhone As System.Windows.Forms.Label
  Friend WithEvents LinkLabel_WebSite As System.Windows.Forms.LinkLabel
  Friend WithEvents Label27 As System.Windows.Forms.Label
  Friend WithEvents Label_Administrator2 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label_Managers As System.Windows.Forms.Label
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Grid_Performance_0 As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Label29 As System.Windows.Forms.Label
  Friend WithEvents Combo_Charts_CompareSeriesPertrac As FCP_RadComboBox
  Friend WithEvents Label37 As System.Windows.Forms.Label
  Friend WithEvents Label38 As System.Windows.Forms.Label
  Friend WithEvents Label36 As System.Windows.Forms.Label
  Friend WithEvents Label35 As System.Windows.Forms.Label
  Friend WithEvents Text_Chart_RollingPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents Text_Chart_Lamda As RenaissanceControls.NumericTextBox
  Friend WithEvents Date_Charts_DateTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents Date_Charts_DateFrom As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label39 As System.Windows.Forms.Label
  Friend WithEvents Chart_RollingReturn As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_StdDev As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_DrawDown As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Correlation As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Quartile As Dundas.Charting.WinControl.Chart
  Friend WithEvents MenuPeriods As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Button_DefaultDate As System.Windows.Forms.Button
  Friend WithEvents TabControl_PerformanceTables As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents Label_PT_0 As System.Windows.Forms.Label
  Friend WithEvents Button_Compare_DefaultDates As System.Windows.Forms.Button
  Friend WithEvents Edit_Compare_Lamda As RenaissanceControls.NumericTextBox
  Friend WithEvents Date_Compare_DateTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents Date_Compare_DateFrom As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label43 As System.Windows.Forms.Label
  Friend WithEvents Label44 As System.Windows.Forms.Label
  Friend WithEvents Label45 As System.Windows.Forms.Label
  Friend WithEvents Combo_Compare_Group As System.Windows.Forms.ComboBox
  Friend WithEvents Label46 As System.Windows.Forms.Label
  Friend WithEvents Tab_Beta As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Beta As Dundas.Charting.WinControl.Chart
  Friend WithEvents Tab_Alpha As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Alpha As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label47 As System.Windows.Forms.Label
  Friend WithEvents Combo_Chart_Conditional As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Compare_Series As FCP_RadComboBox
  Friend WithEvents Radio_ComparisonIsReference As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ListIsReference As System.Windows.Forms.RadioButton
  Friend WithEvents TabControl_Comparisons As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Comp_Correlation As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Statistics As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Correlation_UpDown As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Beta As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Beta_UpDown As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Alpha As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_APR As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comp_Volatility As System.Windows.Forms.TabPage
  Friend WithEvents Grid_Comp_Statistics As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Chart_Comparison_Correlation As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_CorrelationUpDown As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_Beta As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_BetaUpDown As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_Alpha As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_APR As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Comparison_Volatility As Dundas.Charting.WinControl.Chart
  Friend WithEvents Tab_Ranking As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Ranking As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label49 As System.Windows.Forms.Label
  Friend WithEvents Combo_Quartile_HighlightSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Label50 As System.Windows.Forms.Label
  Friend WithEvents Combo_Ranking_HighlightSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Label51 As System.Windows.Forms.Label
  Friend WithEvents Combo_Quartile_ChartData As System.Windows.Forms.ComboBox
  Friend WithEvents Label52 As System.Windows.Forms.Label
  Friend WithEvents Combo_Ranking_ChartData As System.Windows.Forms.ComboBox
  Friend WithEvents Grid_SimpleStatistics As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Label55 As System.Windows.Forms.Label
  Friend WithEvents Text_Statistics_RiskFree As RenaissanceControls.NumericTextBox
  Friend WithEvents Label54 As System.Windows.Forms.Label
  Friend WithEvents Label53 As System.Windows.Forms.Label
  Friend WithEvents Combo_Statistics_HighlightSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Grid_Stats_Drawdowns As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Tab_ReturnScatter As System.Windows.Forms.TabPage
  Friend WithEvents Chart_ReturnScatter As Dundas.Charting.WinControl.Chart
  Friend WithEvents ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_InformationReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_Basic As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_Quartile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_Ranking As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_All As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_ChartsReport_HeaderPage As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_MultiChart As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ChartsReport_CompleteReportPack As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents FundBrowser_StatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Tab_VAR As System.Windows.Forms.TabPage
  Friend WithEvents Label58 As System.Windows.Forms.Label
  Friend WithEvents Text_Chart_VARPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents Label57 As System.Windows.Forms.Label
  Friend WithEvents Label56 As System.Windows.Forms.Label
  Friend WithEvents Combo_Charts_VARSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Text_Chart_VARConfidence As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label59 As System.Windows.Forms.Label
  Friend WithEvents Chart_VAR As Dundas.Charting.WinControl.Chart
  Friend WithEvents Text_ScalingFactor As RenaissanceControls.NumericTextBox
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Radio_DynamicScalingFactor As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_SingleScalingFactor As System.Windows.Forms.RadioButton
  Friend WithEvents Tab_Omega As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Omega As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label60 As System.Windows.Forms.Label
  Friend WithEvents Edit_OmegaReturn As RenaissanceControls.NumericTextBox
  Friend WithEvents Label61 As System.Windows.Forms.Label
  Friend WithEvents Check_OmegaRatio As System.Windows.Forms.CheckBox
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Charts_CompareGroup As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Charts_ComparePertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Combo_Charts_CompareSeriesGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Check_AutoStart As System.Windows.Forms.CheckBox
  Friend WithEvents Radio_CustomGroup As System.Windows.Forms.RadioButton
  Friend WithEvents Numeric_QuantileOutlier As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label62 As System.Windows.Forms.Label
  Friend WithEvents Label63 As System.Windows.Forms.Label
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents RadioComparison_IsGroup As System.Windows.Forms.RadioButton
  Friend WithEvents RadioComparison_IsPertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Combo_Comparisons_Group As System.Windows.Forms.ComboBox
  Friend WithEvents LimitInstrumentDatePeriodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DatePeriod_Daily As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DatePeriod_Weekly As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DatePeriod_Fortnightly As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DatePeriod_Monthly As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DatePeriod_Quarterly As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Debug As System.Windows.Forms.ToolStripMenuItem

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundBrowser))
    Dim ChartArea21 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend21 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series36 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea22 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend22 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series37 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series38 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea23 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend23 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series39 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea24 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend24 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series40 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series41 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea25 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend25 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series42 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea26 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend26 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series43 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea27 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend27 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series44 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea28 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend28 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series45 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series46 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea29 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend29 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series47 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series48 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea30 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend30 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series49 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series50 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea31 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend31 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series51 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series52 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea32 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend32 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series53 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series54 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea33 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend33 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series55 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series56 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series57 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea34 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend34 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series58 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea35 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend35 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series59 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series60 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea36 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend36 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series61 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series62 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea37 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend37 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series63 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series64 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea38 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend38 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series65 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series66 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea39 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend39 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series67 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series68 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea40 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend40 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series69 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series70 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.LimitInstrumentDatePeriodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DatePeriod_Daily = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DatePeriod_Weekly = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DatePeriod_Fortnightly = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DatePeriod_Monthly = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DatePeriod_Quarterly = New System.Windows.Forms.ToolStripMenuItem
    Me.MenuPeriods = New System.Windows.Forms.ToolStripMenuItem
    Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_InformationReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_Basic = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_Quartile = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_Ranking = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_All = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_CompleteReportPack = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_ChartsReport_HeaderPage = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ChartsReport_MultiChart = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Debug = New System.Windows.Forms.ToolStripMenuItem
    Me.Radio_ExistingFunds = New System.Windows.Forms.RadioButton
    Me.Radio_ExistingGroups = New System.Windows.Forms.RadioButton
    Me.Radio_Pertrac = New System.Windows.Forms.RadioButton
    Me.Combo_SelectFrom = New System.Windows.Forms.ComboBox
    Me.List_SelectItems = New System.Windows.Forms.ListBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Split_Form = New System.Windows.Forms.SplitContainer
    Me.Radio_CustomGroup = New System.Windows.Forms.RadioButton
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Tab_InstrumentDetails = New System.Windows.Forms.TabControl
    Me.Tab_Descriptions = New System.Windows.Forms.TabPage
    Me.GroupBox4 = New System.Windows.Forms.GroupBox
    Me.Label_Currency = New System.Windows.Forms.Label
    Me.Label42 = New System.Windows.Forms.Label
    Me.GroupBox3 = New System.Windows.Forms.GroupBox
    Me.Label_Administrator = New System.Windows.Forms.Label
    Me.Label34 = New System.Windows.Forms.Label
    Me.Label_Open = New System.Windows.Forms.Label
    Me.Label24 = New System.Windows.Forms.Label
    Me.Label_InvestorType = New System.Windows.Forms.Label
    Me.Label26 = New System.Windows.Forms.Label
    Me.Label_Domicile = New System.Windows.Forms.Label
    Me.Label28 = New System.Windows.Forms.Label
    Me.Label_FirmAssets = New System.Windows.Forms.Label
    Me.Label30 = New System.Windows.Forms.Label
    Me.Label_FundAssets = New System.Windows.Forms.Label
    Me.Label32 = New System.Windows.Forms.Label
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Label_Notice = New System.Windows.Forms.Label
    Me.Label20 = New System.Windows.Forms.Label
    Me.Label_Lockup = New System.Windows.Forms.Label
    Me.Label22 = New System.Windows.Forms.Label
    Me.Label_Redemption = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label_Subscription = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Label_MinInvestment = New System.Windows.Forms.Label
    Me.Label18 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Label_Hurdle = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.Label_PerformanceFee = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.Label_MgmtFee = New System.Windows.Forms.Label
    Me.Label8 = New System.Windows.Forms.Label
    Me.Text_Description = New System.Windows.Forms.TextBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label_ManagementCo = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label_FundName = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Label_DataSource = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label_Strategy = New System.Windows.Forms.Label
    Me.Label1 = New System.Windows.Forms.Label
    Me.Tab_Contacts = New System.Windows.Forms.TabPage
    Me.Label13 = New System.Windows.Forms.Label
    Me.LinkLabel_WebSite = New System.Windows.Forms.LinkLabel
    Me.Label27 = New System.Windows.Forms.Label
    Me.Label_Administrator2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label_Managers = New System.Windows.Forms.Label
    Me.Label_MarketingFax = New System.Windows.Forms.Label
    Me.Label_MarketingContact = New System.Windows.Forms.Label
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label_MarketingPhone = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    Me.Label_ContactEMail = New System.Windows.Forms.Label
    Me.Label23 = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label_ContactFax = New System.Windows.Forms.Label
    Me.Label_ContactPhone = New System.Windows.Forms.Label
    Me.Label_Contact = New System.Windows.Forms.Label
    Me.Label33 = New System.Windows.Forms.Label
    Me.Label31 = New System.Windows.Forms.Label
    Me.Label_Country = New System.Windows.Forms.Label
    Me.Label_State = New System.Windows.Forms.Label
    Me.Label_Zip = New System.Windows.Forms.Label
    Me.Label_City = New System.Windows.Forms.Label
    Me.Label_Address2 = New System.Windows.Forms.Label
    Me.Label_Address1 = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Label_ManagementCo2 = New System.Windows.Forms.Label
    Me.Label15 = New System.Windows.Forms.Label
    Me.Label_FundName2 = New System.Windows.Forms.Label
    Me.Label19 = New System.Windows.Forms.Label
    Me.Tab_Performance = New System.Windows.Forms.TabPage
    Me.TabControl_PerformanceTables = New System.Windows.Forms.TabControl
    Me.TabPage1 = New System.Windows.Forms.TabPage
    Me.Label_PT_0 = New System.Windows.Forms.Label
    Me.Grid_Performance_0 = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Comparisons = New System.Windows.Forms.TabPage
    Me.Combo_Comparisons_Group = New System.Windows.Forms.ComboBox
    Me.Label63 = New System.Windows.Forms.Label
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.RadioComparison_IsGroup = New System.Windows.Forms.RadioButton
    Me.RadioComparison_IsPertrac = New System.Windows.Forms.RadioButton
    Me.TabControl_Comparisons = New System.Windows.Forms.TabControl
    Me.Tab_Comp_Statistics = New System.Windows.Forms.TabPage
    Me.Grid_Comp_Statistics = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Comp_Correlation = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_Correlation = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_Correlation_UpDown = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_CorrelationUpDown = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_Beta = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_Beta = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_Beta_UpDown = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_BetaUpDown = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_Alpha = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_Alpha = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_APR = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_APR = New Dundas.Charting.WinControl.Chart
    Me.Tab_Comp_Volatility = New System.Windows.Forms.TabPage
    Me.Chart_Comparison_Volatility = New Dundas.Charting.WinControl.Chart
    Me.Radio_ComparisonIsReference = New System.Windows.Forms.RadioButton
    Me.Radio_ListIsReference = New System.Windows.Forms.RadioButton
    Me.Combo_Compare_Series = New FCP_TelerikControls.FCP_RadComboBox
    Me.Button_Compare_DefaultDates = New System.Windows.Forms.Button
    Me.Edit_Compare_Lamda = New RenaissanceControls.NumericTextBox
    Me.Date_Compare_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Date_Compare_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label43 = New System.Windows.Forms.Label
    Me.Label44 = New System.Windows.Forms.Label
    Me.Label45 = New System.Windows.Forms.Label
    Me.Combo_Compare_Group = New System.Windows.Forms.ComboBox
    Me.Label46 = New System.Windows.Forms.Label
    Me.Tab_Charts = New System.Windows.Forms.TabPage
    Me.Check_AutoStart = New System.Windows.Forms.CheckBox
    Me.Combo_Charts_CompareSeriesGroup = New System.Windows.Forms.ComboBox
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Radio_Charts_CompareGroup = New System.Windows.Forms.RadioButton
    Me.Radio_Charts_ComparePertrac = New System.Windows.Forms.RadioButton
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Text_ScalingFactor = New RenaissanceControls.NumericTextBox
    Me.Radio_DynamicScalingFactor = New System.Windows.Forms.RadioButton
    Me.Radio_SingleScalingFactor = New System.Windows.Forms.RadioButton
    Me.Label47 = New System.Windows.Forms.Label
    Me.Combo_Chart_Conditional = New System.Windows.Forms.ComboBox
    Me.Button_DefaultDate = New System.Windows.Forms.Button
    Me.Label39 = New System.Windows.Forms.Label
    Me.Text_Chart_RollingPeriod = New RenaissanceControls.NumericTextBox
    Me.Text_Chart_Lamda = New RenaissanceControls.NumericTextBox
    Me.Date_Charts_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Date_Charts_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label37 = New System.Windows.Forms.Label
    Me.Label38 = New System.Windows.Forms.Label
    Me.Label36 = New System.Windows.Forms.Label
    Me.Label35 = New System.Windows.Forms.Label
    Me.Combo_Charts_CompareSeriesPertrac = New FCP_TelerikControls.FCP_RadComboBox
    Me.Label29 = New System.Windows.Forms.Label
    Me.TabControl_Charts = New System.Windows.Forms.TabControl
    Me.Tab_Vami = New System.Windows.Forms.TabPage
    Me.Chart_Prices = New Dundas.Charting.WinControl.Chart
    Me.Tab_Monthly = New System.Windows.Forms.TabPage
    Me.Chart_Returns = New Dundas.Charting.WinControl.Chart
    Me.Tab_Return = New System.Windows.Forms.TabPage
    Me.Chart_RollingReturn = New Dundas.Charting.WinControl.Chart
    Me.Tab_ReturnScatter = New System.Windows.Forms.TabPage
    Me.Chart_ReturnScatter = New Dundas.Charting.WinControl.Chart
    Me.Tab_StdDev = New System.Windows.Forms.TabPage
    Me.Chart_StdDev = New Dundas.Charting.WinControl.Chart
    Me.Tab_VAR = New System.Windows.Forms.TabPage
    Me.Chart_VAR = New Dundas.Charting.WinControl.Chart
    Me.Text_Chart_VARConfidence = New RenaissanceControls.PercentageTextBox
    Me.Label59 = New System.Windows.Forms.Label
    Me.Label58 = New System.Windows.Forms.Label
    Me.Text_Chart_VARPeriod = New RenaissanceControls.NumericTextBox
    Me.Label57 = New System.Windows.Forms.Label
    Me.Label56 = New System.Windows.Forms.Label
    Me.Combo_Charts_VARSeries = New System.Windows.Forms.ComboBox
    Me.Tab_Omega = New System.Windows.Forms.TabPage
    Me.Check_OmegaRatio = New System.Windows.Forms.CheckBox
    Me.Label60 = New System.Windows.Forms.Label
    Me.Edit_OmegaReturn = New RenaissanceControls.NumericTextBox
    Me.Label61 = New System.Windows.Forms.Label
    Me.Chart_Omega = New Dundas.Charting.WinControl.Chart
    Me.Tab_DrawDown = New System.Windows.Forms.TabPage
    Me.Chart_DrawDown = New Dundas.Charting.WinControl.Chart
    Me.Tab_Correlation = New System.Windows.Forms.TabPage
    Me.Chart_Correlation = New Dundas.Charting.WinControl.Chart
    Me.Tab_Beta = New System.Windows.Forms.TabPage
    Me.Chart_Beta = New Dundas.Charting.WinControl.Chart
    Me.Tab_Alpha = New System.Windows.Forms.TabPage
    Me.Chart_Alpha = New Dundas.Charting.WinControl.Chart
    Me.Tab_Quartile = New System.Windows.Forms.TabPage
    Me.Numeric_QuantileOutlier = New System.Windows.Forms.NumericUpDown
    Me.Label62 = New System.Windows.Forms.Label
    Me.Label51 = New System.Windows.Forms.Label
    Me.Combo_Quartile_ChartData = New System.Windows.Forms.ComboBox
    Me.Label49 = New System.Windows.Forms.Label
    Me.Combo_Quartile_HighlightSeries = New System.Windows.Forms.ComboBox
    Me.Chart_Quartile = New Dundas.Charting.WinControl.Chart
    Me.Tab_Ranking = New System.Windows.Forms.TabPage
    Me.Label52 = New System.Windows.Forms.Label
    Me.Combo_Ranking_ChartData = New System.Windows.Forms.ComboBox
    Me.Label50 = New System.Windows.Forms.Label
    Me.Combo_Ranking_HighlightSeries = New System.Windows.Forms.ComboBox
    Me.Chart_Ranking = New Dundas.Charting.WinControl.Chart
    Me.Tab_Statistics = New System.Windows.Forms.TabPage
    Me.Grid_Stats_Drawdowns = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Grid_SimpleStatistics = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Label55 = New System.Windows.Forms.Label
    Me.Text_Statistics_RiskFree = New RenaissanceControls.NumericTextBox
    Me.Label54 = New System.Windows.Forms.Label
    Me.Label53 = New System.Windows.Forms.Label
    Me.Combo_Statistics_HighlightSeries = New System.Windows.Forms.ComboBox
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
    Me.FundBrowser_StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.RootMenu.SuspendLayout()
    Me.Split_Form.Panel1.SuspendLayout()
    Me.Split_Form.Panel2.SuspendLayout()
    Me.Split_Form.SuspendLayout()
    Me.Tab_InstrumentDetails.SuspendLayout()
    Me.Tab_Descriptions.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.Tab_Contacts.SuspendLayout()
    Me.Tab_Performance.SuspendLayout()
    Me.TabControl_PerformanceTables.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    CType(Me.Grid_Performance_0, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comparisons.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.TabControl_Comparisons.SuspendLayout()
    Me.Tab_Comp_Statistics.SuspendLayout()
    CType(Me.Grid_Comp_Statistics, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Correlation.SuspendLayout()
    CType(Me.Chart_Comparison_Correlation, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Correlation_UpDown.SuspendLayout()
    CType(Me.Chart_Comparison_CorrelationUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Beta.SuspendLayout()
    CType(Me.Chart_Comparison_Beta, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Beta_UpDown.SuspendLayout()
    CType(Me.Chart_Comparison_BetaUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Alpha.SuspendLayout()
    CType(Me.Chart_Comparison_Alpha, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_APR.SuspendLayout()
    CType(Me.Chart_Comparison_APR, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Comp_Volatility.SuspendLayout()
    CType(Me.Chart_Comparison_Volatility, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Combo_Compare_Series, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Charts.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.Panel1.SuspendLayout()
    CType(Me.Combo_Charts_CompareSeriesPertrac, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TabControl_Charts.SuspendLayout()
    Me.Tab_Vami.SuspendLayout()
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Monthly.SuspendLayout()
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Return.SuspendLayout()
    CType(Me.Chart_RollingReturn, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_ReturnScatter.SuspendLayout()
    CType(Me.Chart_ReturnScatter, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_StdDev.SuspendLayout()
    CType(Me.Chart_StdDev, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_VAR.SuspendLayout()
    CType(Me.Chart_VAR, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Omega.SuspendLayout()
    CType(Me.Chart_Omega, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_DrawDown.SuspendLayout()
    CType(Me.Chart_DrawDown, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Correlation.SuspendLayout()
    CType(Me.Chart_Correlation, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Beta.SuspendLayout()
    CType(Me.Chart_Beta, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Alpha.SuspendLayout()
    CType(Me.Chart_Alpha, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Quartile.SuspendLayout()
    CType(Me.Numeric_QuantileOutlier, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_Quartile, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Ranking.SuspendLayout()
    CType(Me.Chart_Ranking, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Statistics.SuspendLayout()
    CType(Me.Grid_Stats_Drawdowns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Grid_SimpleStatistics, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.StatusStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LimitInstrumentDatePeriodToolStripMenuItem, Me.MenuPeriods, Me.ReportsToolStripMenuItem, Me.Menu_Debug})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1118, 24)
    Me.RootMenu.TabIndex = 12
    Me.RootMenu.Text = "MenuStrip1"
    '
    'LimitInstrumentDatePeriodToolStripMenuItem
    '
    Me.LimitInstrumentDatePeriodToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_DatePeriod_Daily, Me.Menu_DatePeriod_Weekly, Me.Menu_DatePeriod_Fortnightly, Me.Menu_DatePeriod_Monthly, Me.Menu_DatePeriod_Quarterly})
    Me.LimitInstrumentDatePeriodToolStripMenuItem.Name = "LimitInstrumentDatePeriodToolStripMenuItem"
    Me.LimitInstrumentDatePeriodToolStripMenuItem.Size = New System.Drawing.Size(171, 20)
    Me.LimitInstrumentDatePeriodToolStripMenuItem.Text = "Limit Instrument Date Period"
    '
    'Menu_DatePeriod_Daily
    '
    Me.Menu_DatePeriod_Daily.Name = "Menu_DatePeriod_Daily"
    Me.Menu_DatePeriod_Daily.Size = New System.Drawing.Size(132, 22)
    Me.Menu_DatePeriod_Daily.Text = "Daily"
    '
    'Menu_DatePeriod_Weekly
    '
    Me.Menu_DatePeriod_Weekly.Name = "Menu_DatePeriod_Weekly"
    Me.Menu_DatePeriod_Weekly.Size = New System.Drawing.Size(132, 22)
    Me.Menu_DatePeriod_Weekly.Text = "Weekly"
    '
    'Menu_DatePeriod_Fortnightly
    '
    Me.Menu_DatePeriod_Fortnightly.Name = "Menu_DatePeriod_Fortnightly"
    Me.Menu_DatePeriod_Fortnightly.Size = New System.Drawing.Size(132, 22)
    Me.Menu_DatePeriod_Fortnightly.Text = "Fortnightly"
    '
    'Menu_DatePeriod_Monthly
    '
    Me.Menu_DatePeriod_Monthly.Name = "Menu_DatePeriod_Monthly"
    Me.Menu_DatePeriod_Monthly.Size = New System.Drawing.Size(132, 22)
    Me.Menu_DatePeriod_Monthly.Text = "Monthly"
    '
    'Menu_DatePeriod_Quarterly
    '
    Me.Menu_DatePeriod_Quarterly.Name = "Menu_DatePeriod_Quarterly"
    Me.Menu_DatePeriod_Quarterly.Size = New System.Drawing.Size(132, 22)
    Me.Menu_DatePeriod_Quarterly.Text = "Quarterly"
    '
    'MenuPeriods
    '
    Me.MenuPeriods.Name = "MenuPeriods"
    Me.MenuPeriods.Size = New System.Drawing.Size(107, 20)
    Me.MenuPeriods.Text = "Charting &Periods"
    '
    'ReportsToolStripMenuItem
    '
    Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_InformationReport, Me.Menu_ChartsReport})
    Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
    Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
    Me.ReportsToolStripMenuItem.Text = "&Reports"
    '
    'Menu_InformationReport
    '
    Me.Menu_InformationReport.Name = "Menu_InformationReport"
    Me.Menu_InformationReport.Size = New System.Drawing.Size(175, 22)
    Me.Menu_InformationReport.Text = "Information Report"
    '
    'Menu_ChartsReport
    '
    Me.Menu_ChartsReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_ChartsReport_Basic, Me.Menu_ChartsReport_Quartile, Me.Menu_ChartsReport_Ranking, Me.Menu_ChartsReport_All, Me.Menu_ChartsReport_CompleteReportPack, Me.ToolStripMenuItem1, Me.Menu_ChartsReport_HeaderPage, Me.Menu_ChartsReport_MultiChart})
    Me.Menu_ChartsReport.Name = "Menu_ChartsReport"
    Me.Menu_ChartsReport.Size = New System.Drawing.Size(175, 22)
    Me.Menu_ChartsReport.Text = "Charts Report"
    '
    'Menu_ChartsReport_Basic
    '
    Me.Menu_ChartsReport_Basic.Name = "Menu_ChartsReport_Basic"
    Me.Menu_ChartsReport_Basic.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_Basic.Text = "Basic Charts"
    '
    'Menu_ChartsReport_Quartile
    '
    Me.Menu_ChartsReport_Quartile.Name = "Menu_ChartsReport_Quartile"
    Me.Menu_ChartsReport_Quartile.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_Quartile.Text = "Quartile Reports"
    '
    'Menu_ChartsReport_Ranking
    '
    Me.Menu_ChartsReport_Ranking.Name = "Menu_ChartsReport_Ranking"
    Me.Menu_ChartsReport_Ranking.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_Ranking.Text = "Ranking Reports"
    '
    'Menu_ChartsReport_All
    '
    Me.Menu_ChartsReport_All.Name = "Menu_ChartsReport_All"
    Me.Menu_ChartsReport_All.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_All.Text = "All of the Above"
    '
    'Menu_ChartsReport_CompleteReportPack
    '
    Me.Menu_ChartsReport_CompleteReportPack.Name = "Menu_ChartsReport_CompleteReportPack"
    Me.Menu_ChartsReport_CompleteReportPack.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_CompleteReportPack.Text = "Complete Report Pack"
    '
    'ToolStripMenuItem1
    '
    Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
    Me.ToolStripMenuItem1.Size = New System.Drawing.Size(203, 6)
    '
    'Menu_ChartsReport_HeaderPage
    '
    Me.Menu_ChartsReport_HeaderPage.Checked = True
    Me.Menu_ChartsReport_HeaderPage.CheckOnClick = True
    Me.Menu_ChartsReport_HeaderPage.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_ChartsReport_HeaderPage.Name = "Menu_ChartsReport_HeaderPage"
    Me.Menu_ChartsReport_HeaderPage.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_HeaderPage.Text = "Show Header Page"
    '
    'Menu_ChartsReport_MultiChart
    '
    Me.Menu_ChartsReport_MultiChart.Checked = True
    Me.Menu_ChartsReport_MultiChart.CheckOnClick = True
    Me.Menu_ChartsReport_MultiChart.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_ChartsReport_MultiChart.Name = "Menu_ChartsReport_MultiChart"
    Me.Menu_ChartsReport_MultiChart.Size = New System.Drawing.Size(206, 22)
    Me.Menu_ChartsReport_MultiChart.Text = "Show Six Charts per page"
    '
    'Menu_Debug
    '
    Me.Menu_Debug.Name = "Menu_Debug"
    Me.Menu_Debug.Size = New System.Drawing.Size(54, 20)
    Me.Menu_Debug.Text = "Debug"
    '
    'Radio_ExistingFunds
    '
    Me.Radio_ExistingFunds.AutoSize = True
    Me.Radio_ExistingFunds.Location = New System.Drawing.Point(50, 48)
    Me.Radio_ExistingFunds.Name = "Radio_ExistingFunds"
    Me.Radio_ExistingFunds.Size = New System.Drawing.Size(106, 17)
    Me.Radio_ExistingFunds.TabIndex = 3
    Me.Radio_ExistingFunds.Text = "Existing Positions"
    Me.Radio_ExistingFunds.UseVisualStyleBackColor = True
    '
    'Radio_ExistingGroups
    '
    Me.Radio_ExistingGroups.AutoSize = True
    Me.Radio_ExistingGroups.Location = New System.Drawing.Point(50, 2)
    Me.Radio_ExistingGroups.Name = "Radio_ExistingGroups"
    Me.Radio_ExistingGroups.Size = New System.Drawing.Size(98, 17)
    Me.Radio_ExistingGroups.TabIndex = 1
    Me.Radio_ExistingGroups.Text = "Existing Groups"
    Me.Radio_ExistingGroups.UseVisualStyleBackColor = True
    '
    'Radio_Pertrac
    '
    Me.Radio_Pertrac.AutoSize = True
    Me.Radio_Pertrac.Location = New System.Drawing.Point(50, 25)
    Me.Radio_Pertrac.Name = "Radio_Pertrac"
    Me.Radio_Pertrac.Size = New System.Drawing.Size(59, 17)
    Me.Radio_Pertrac.TabIndex = 2
    Me.Radio_Pertrac.Text = "Pertrac"
    Me.Radio_Pertrac.UseVisualStyleBackColor = True
    '
    'Combo_SelectFrom
    '
    Me.Combo_SelectFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectFrom.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectFrom.Location = New System.Drawing.Point(3, 71)
    Me.Combo_SelectFrom.Name = "Combo_SelectFrom"
    Me.Combo_SelectFrom.Size = New System.Drawing.Size(331, 21)
    Me.Combo_SelectFrom.TabIndex = 6
    '
    'List_SelectItems
    '
    Me.List_SelectItems.AllowDrop = True
    Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_SelectItems.FormattingEnabled = True
    Me.List_SelectItems.Location = New System.Drawing.Point(2, 98)
    Me.List_SelectItems.Name = "List_SelectItems"
    Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_SelectItems.Size = New System.Drawing.Size(335, 563)
    Me.List_SelectItems.Sorted = True
    Me.List_SelectItems.TabIndex = 7
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(2, 4)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(42, 64)
    Me.Label2.TabIndex = 0
    Me.Label2.Text = "Select from"
    '
    'Split_Form
    '
    Me.Split_Form.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_Form.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_Form.Location = New System.Drawing.Point(2, 26)
    Me.Split_Form.Name = "Split_Form"
    '
    'Split_Form.Panel1
    '
    Me.Split_Form.Panel1.BackColor = System.Drawing.SystemColors.Control
    Me.Split_Form.Panel1.Controls.Add(Me.Radio_CustomGroup)
    Me.Split_Form.Panel1.Controls.Add(Me.Date_ValueDate)
    Me.Split_Form.Panel1.Controls.Add(Me.Radio_ExistingGroups)
    Me.Split_Form.Panel1.Controls.Add(Me.Radio_ExistingFunds)
    Me.Split_Form.Panel1.Controls.Add(Me.Label2)
    Me.Split_Form.Panel1.Controls.Add(Me.List_SelectItems)
    Me.Split_Form.Panel1.Controls.Add(Me.Radio_Pertrac)
    Me.Split_Form.Panel1.Controls.Add(Me.Combo_SelectFrom)
    '
    'Split_Form.Panel2
    '
    Me.Split_Form.Panel2.Controls.Add(Me.Tab_InstrumentDetails)
    Me.Split_Form.Size = New System.Drawing.Size(1114, 679)
    Me.Split_Form.SplitterDistance = 344
    Me.Split_Form.SplitterWidth = 3
    Me.Split_Form.TabIndex = 0
    '
    'Radio_CustomGroup
    '
    Me.Radio_CustomGroup.AutoSize = True
    Me.Radio_CustomGroup.Location = New System.Drawing.Point(174, 4)
    Me.Radio_CustomGroup.Name = "Radio_CustomGroup"
    Me.Radio_CustomGroup.Size = New System.Drawing.Size(107, 17)
    Me.Radio_CustomGroup.TabIndex = 4
    Me.Radio_CustomGroup.Text = "Custom Selection"
    Me.Radio_CustomGroup.UseVisualStyleBackColor = True
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate.Enabled = False
    Me.Date_ValueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate.Location = New System.Drawing.Point(174, 48)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(148, 20)
    Me.Date_ValueDate.TabIndex = 5
    '
    'Tab_InstrumentDetails
    '
    Me.Tab_InstrumentDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Tab_InstrumentDetails.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Descriptions)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Contacts)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Performance)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Comparisons)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Charts)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Statistics)
    Me.Tab_InstrumentDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Tab_InstrumentDetails.Location = New System.Drawing.Point(1, 1)
    Me.Tab_InstrumentDetails.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_InstrumentDetails.Multiline = True
    Me.Tab_InstrumentDetails.Name = "Tab_InstrumentDetails"
    Me.Tab_InstrumentDetails.SelectedIndex = 0
    Me.Tab_InstrumentDetails.Size = New System.Drawing.Size(763, 675)
    Me.Tab_InstrumentDetails.TabIndex = 0
    '
    'Tab_Descriptions
    '
    Me.Tab_Descriptions.AutoScroll = True
    Me.Tab_Descriptions.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Descriptions.Controls.Add(Me.GroupBox4)
    Me.Tab_Descriptions.Controls.Add(Me.GroupBox3)
    Me.Tab_Descriptions.Controls.Add(Me.GroupBox2)
    Me.Tab_Descriptions.Controls.Add(Me.GroupBox1)
    Me.Tab_Descriptions.Controls.Add(Me.Text_Description)
    Me.Tab_Descriptions.Controls.Add(Me.Label7)
    Me.Tab_Descriptions.Controls.Add(Me.Label_ManagementCo)
    Me.Tab_Descriptions.Controls.Add(Me.Label6)
    Me.Tab_Descriptions.Controls.Add(Me.Label_FundName)
    Me.Tab_Descriptions.Controls.Add(Me.Label5)
    Me.Tab_Descriptions.Controls.Add(Me.Label_DataSource)
    Me.Tab_Descriptions.Controls.Add(Me.Label4)
    Me.Tab_Descriptions.Controls.Add(Me.Label_Strategy)
    Me.Tab_Descriptions.Controls.Add(Me.Label1)
    Me.Tab_Descriptions.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Descriptions.Name = "Tab_Descriptions"
    Me.Tab_Descriptions.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Descriptions.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Descriptions.TabIndex = 0
    Me.Tab_Descriptions.Text = "Descriptions"
    Me.Tab_Descriptions.UseVisualStyleBackColor = True
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.Label_Currency)
    Me.GroupBox4.Controls.Add(Me.Label42)
    Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox4.Location = New System.Drawing.Point(337, 185)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(315, 102)
    Me.GroupBox4.TabIndex = 14
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Pricing"
    '
    'Label_Currency
    '
    Me.Label_Currency.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Currency.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Currency.Location = New System.Drawing.Point(87, 16)
    Me.Label_Currency.Name = "Label_Currency"
    Me.Label_Currency.Size = New System.Drawing.Size(221, 19)
    Me.Label_Currency.TabIndex = 9
    Me.Label_Currency.Text = "Currency"
    '
    'Label42
    '
    Me.Label42.AutoSize = True
    Me.Label42.Location = New System.Drawing.Point(6, 17)
    Me.Label42.Name = "Label42"
    Me.Label42.Size = New System.Drawing.Size(49, 13)
    Me.Label42.TabIndex = 8
    Me.Label42.Text = "Currency"
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Label_Administrator)
    Me.GroupBox3.Controls.Add(Me.Label34)
    Me.GroupBox3.Controls.Add(Me.Label_Open)
    Me.GroupBox3.Controls.Add(Me.Label24)
    Me.GroupBox3.Controls.Add(Me.Label_InvestorType)
    Me.GroupBox3.Controls.Add(Me.Label26)
    Me.GroupBox3.Controls.Add(Me.Label_Domicile)
    Me.GroupBox3.Controls.Add(Me.Label28)
    Me.GroupBox3.Controls.Add(Me.Label_FirmAssets)
    Me.GroupBox3.Controls.Add(Me.Label30)
    Me.GroupBox3.Controls.Add(Me.Label_FundAssets)
    Me.GroupBox3.Controls.Add(Me.Label32)
    Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox3.Location = New System.Drawing.Point(11, 293)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(315, 164)
    Me.GroupBox3.TabIndex = 13
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "Fund Details"
    '
    'Label_Administrator
    '
    Me.Label_Administrator.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Administrator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Administrator.Location = New System.Drawing.Point(87, 136)
    Me.Label_Administrator.Name = "Label_Administrator"
    Me.Label_Administrator.Size = New System.Drawing.Size(221, 19)
    Me.Label_Administrator.TabIndex = 19
    Me.Label_Administrator.Text = "Administrator"
    '
    'Label34
    '
    Me.Label34.AutoSize = True
    Me.Label34.Location = New System.Drawing.Point(6, 137)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(67, 13)
    Me.Label34.TabIndex = 18
    Me.Label34.Text = "Administrator"
    '
    'Label_Open
    '
    Me.Label_Open.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Open.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Open.Location = New System.Drawing.Point(87, 112)
    Me.Label_Open.Name = "Label_Open"
    Me.Label_Open.Size = New System.Drawing.Size(221, 19)
    Me.Label_Open.TabIndex = 17
    Me.Label_Open.Text = "Open"
    '
    'Label24
    '
    Me.Label24.AutoSize = True
    Me.Label24.Location = New System.Drawing.Point(6, 113)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(33, 13)
    Me.Label24.TabIndex = 16
    Me.Label24.Text = "Open"
    '
    'Label_InvestorType
    '
    Me.Label_InvestorType.BackColor = System.Drawing.SystemColors.Window
    Me.Label_InvestorType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_InvestorType.Location = New System.Drawing.Point(87, 88)
    Me.Label_InvestorType.Name = "Label_InvestorType"
    Me.Label_InvestorType.Size = New System.Drawing.Size(221, 19)
    Me.Label_InvestorType.TabIndex = 15
    Me.Label_InvestorType.Text = "InvestorType"
    '
    'Label26
    '
    Me.Label26.AutoSize = True
    Me.Label26.Location = New System.Drawing.Point(6, 89)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(72, 13)
    Me.Label26.TabIndex = 14
    Me.Label26.Text = "Investor Type"
    '
    'Label_Domicile
    '
    Me.Label_Domicile.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Domicile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Domicile.Location = New System.Drawing.Point(87, 64)
    Me.Label_Domicile.Name = "Label_Domicile"
    Me.Label_Domicile.Size = New System.Drawing.Size(221, 19)
    Me.Label_Domicile.TabIndex = 13
    Me.Label_Domicile.Text = "Domicile"
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.Location = New System.Drawing.Point(6, 65)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(47, 13)
    Me.Label28.TabIndex = 12
    Me.Label28.Text = "Domicile"
    '
    'Label_FirmAssets
    '
    Me.Label_FirmAssets.BackColor = System.Drawing.SystemColors.Window
    Me.Label_FirmAssets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_FirmAssets.Location = New System.Drawing.Point(87, 40)
    Me.Label_FirmAssets.Name = "Label_FirmAssets"
    Me.Label_FirmAssets.Size = New System.Drawing.Size(221, 19)
    Me.Label_FirmAssets.TabIndex = 11
    Me.Label_FirmAssets.Text = "FirmAssets"
    '
    'Label30
    '
    Me.Label30.AutoSize = True
    Me.Label30.Location = New System.Drawing.Point(6, 41)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(60, 13)
    Me.Label30.TabIndex = 10
    Me.Label30.Text = "Firm Assets"
    '
    'Label_FundAssets
    '
    Me.Label_FundAssets.BackColor = System.Drawing.SystemColors.Window
    Me.Label_FundAssets.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_FundAssets.Location = New System.Drawing.Point(87, 16)
    Me.Label_FundAssets.Name = "Label_FundAssets"
    Me.Label_FundAssets.Size = New System.Drawing.Size(221, 19)
    Me.Label_FundAssets.TabIndex = 9
    Me.Label_FundAssets.Text = "FundAssets"
    '
    'Label32
    '
    Me.Label32.AutoSize = True
    Me.Label32.Location = New System.Drawing.Point(6, 17)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(65, 13)
    Me.Label32.TabIndex = 8
    Me.Label32.Text = "Fund Assets"
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Label_Notice)
    Me.GroupBox2.Controls.Add(Me.Label20)
    Me.GroupBox2.Controls.Add(Me.Label_Lockup)
    Me.GroupBox2.Controls.Add(Me.Label22)
    Me.GroupBox2.Controls.Add(Me.Label_Redemption)
    Me.GroupBox2.Controls.Add(Me.Label14)
    Me.GroupBox2.Controls.Add(Me.Label_Subscription)
    Me.GroupBox2.Controls.Add(Me.Label16)
    Me.GroupBox2.Controls.Add(Me.Label_MinInvestment)
    Me.GroupBox2.Controls.Add(Me.Label18)
    Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox2.Location = New System.Drawing.Point(337, 293)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(315, 164)
    Me.GroupBox2.TabIndex = 12
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Liquidity Terms"
    '
    'Label_Notice
    '
    Me.Label_Notice.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Notice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Notice.Location = New System.Drawing.Point(87, 112)
    Me.Label_Notice.Name = "Label_Notice"
    Me.Label_Notice.Size = New System.Drawing.Size(221, 19)
    Me.Label_Notice.TabIndex = 17
    Me.Label_Notice.Text = "Notice"
    '
    'Label20
    '
    Me.Label20.AutoSize = True
    Me.Label20.Location = New System.Drawing.Point(6, 113)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(38, 13)
    Me.Label20.TabIndex = 16
    Me.Label20.Text = "Notice"
    '
    'Label_Lockup
    '
    Me.Label_Lockup.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Lockup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Lockup.Location = New System.Drawing.Point(87, 88)
    Me.Label_Lockup.Name = "Label_Lockup"
    Me.Label_Lockup.Size = New System.Drawing.Size(221, 19)
    Me.Label_Lockup.TabIndex = 15
    Me.Label_Lockup.Text = "Lockup"
    '
    'Label22
    '
    Me.Label22.AutoSize = True
    Me.Label22.Location = New System.Drawing.Point(6, 89)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(43, 13)
    Me.Label22.TabIndex = 14
    Me.Label22.Text = "Lockup"
    '
    'Label_Redemption
    '
    Me.Label_Redemption.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Redemption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Redemption.Location = New System.Drawing.Point(87, 64)
    Me.Label_Redemption.Name = "Label_Redemption"
    Me.Label_Redemption.Size = New System.Drawing.Size(221, 19)
    Me.Label_Redemption.TabIndex = 13
    Me.Label_Redemption.Text = "Redemption"
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Location = New System.Drawing.Point(6, 65)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(64, 13)
    Me.Label14.TabIndex = 12
    Me.Label14.Text = "Redemption"
    '
    'Label_Subscription
    '
    Me.Label_Subscription.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Subscription.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Subscription.Location = New System.Drawing.Point(87, 40)
    Me.Label_Subscription.Name = "Label_Subscription"
    Me.Label_Subscription.Size = New System.Drawing.Size(221, 19)
    Me.Label_Subscription.TabIndex = 11
    Me.Label_Subscription.Text = "Strategy"
    '
    'Label16
    '
    Me.Label16.AutoSize = True
    Me.Label16.Location = New System.Drawing.Point(6, 41)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(65, 13)
    Me.Label16.TabIndex = 10
    Me.Label16.Text = "Subscription"
    '
    'Label_MinInvestment
    '
    Me.Label_MinInvestment.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MinInvestment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MinInvestment.Location = New System.Drawing.Point(87, 16)
    Me.Label_MinInvestment.Name = "Label_MinInvestment"
    Me.Label_MinInvestment.Size = New System.Drawing.Size(221, 19)
    Me.Label_MinInvestment.TabIndex = 9
    Me.Label_MinInvestment.Text = "MinInvestment"
    '
    'Label18
    '
    Me.Label18.AutoSize = True
    Me.Label18.Location = New System.Drawing.Point(6, 17)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(79, 13)
    Me.Label18.TabIndex = 8
    Me.Label18.Text = "Min Investment"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Label_Hurdle)
    Me.GroupBox1.Controls.Add(Me.Label12)
    Me.GroupBox1.Controls.Add(Me.Label_PerformanceFee)
    Me.GroupBox1.Controls.Add(Me.Label10)
    Me.GroupBox1.Controls.Add(Me.Label_MgmtFee)
    Me.GroupBox1.Controls.Add(Me.Label8)
    Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox1.Location = New System.Drawing.Point(11, 185)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(315, 102)
    Me.GroupBox1.TabIndex = 11
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Fees"
    '
    'Label_Hurdle
    '
    Me.Label_Hurdle.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Hurdle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Hurdle.Location = New System.Drawing.Point(88, 64)
    Me.Label_Hurdle.Name = "Label_Hurdle"
    Me.Label_Hurdle.Size = New System.Drawing.Size(221, 19)
    Me.Label_Hurdle.TabIndex = 7
    Me.Label_Hurdle.Text = "Hurdle"
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.Location = New System.Drawing.Point(7, 65)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(38, 13)
    Me.Label12.TabIndex = 6
    Me.Label12.Text = "Hurdle"
    '
    'Label_PerformanceFee
    '
    Me.Label_PerformanceFee.BackColor = System.Drawing.SystemColors.Window
    Me.Label_PerformanceFee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_PerformanceFee.Location = New System.Drawing.Point(88, 40)
    Me.Label_PerformanceFee.Name = "Label_PerformanceFee"
    Me.Label_PerformanceFee.Size = New System.Drawing.Size(221, 19)
    Me.Label_PerformanceFee.TabIndex = 5
    Me.Label_PerformanceFee.Text = "PerfFee"
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Location = New System.Drawing.Point(7, 41)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(50, 13)
    Me.Label10.TabIndex = 4
    Me.Label10.Text = "Perf. Fee"
    '
    'Label_MgmtFee
    '
    Me.Label_MgmtFee.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MgmtFee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MgmtFee.Location = New System.Drawing.Point(88, 16)
    Me.Label_MgmtFee.Name = "Label_MgmtFee"
    Me.Label_MgmtFee.Size = New System.Drawing.Size(221, 19)
    Me.Label_MgmtFee.TabIndex = 3
    Me.Label_MgmtFee.Text = "MgmtFee"
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Location = New System.Drawing.Point(7, 17)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(54, 13)
    Me.Label8.TabIndex = 2
    Me.Label8.Text = "Mgmt Fee"
    '
    'Text_Description
    '
    Me.Text_Description.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_Description.BackColor = System.Drawing.SystemColors.Window
    Me.Text_Description.Location = New System.Drawing.Point(99, 86)
    Me.Text_Description.MinimumSize = New System.Drawing.Size(200, 93)
    Me.Text_Description.Multiline = True
    Me.Text_Description.Name = "Text_Description"
    Me.Text_Description.ReadOnly = True
    Me.Text_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_Description.Size = New System.Drawing.Size(647, 93)
    Me.Text_Description.TabIndex = 10
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Location = New System.Drawing.Point(8, 87)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(60, 13)
    Me.Label7.TabIndex = 8
    Me.Label7.Text = "Description"
    '
    'Label_ManagementCo
    '
    Me.Label_ManagementCo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ManagementCo.BackColor = System.Drawing.SystemColors.Window
    Me.Label_ManagementCo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ManagementCo.Location = New System.Drawing.Point(99, 60)
    Me.Label_ManagementCo.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_ManagementCo.Name = "Label_ManagementCo"
    Me.Label_ManagementCo.Size = New System.Drawing.Size(647, 19)
    Me.Label_ManagementCo.TabIndex = 7
    Me.Label_ManagementCo.Text = "ManagementCo"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(8, 61)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(49, 13)
    Me.Label6.TabIndex = 6
    Me.Label6.Text = "Manager"
    '
    'Label_FundName
    '
    Me.Label_FundName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_FundName.BackColor = System.Drawing.SystemColors.Window
    Me.Label_FundName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_FundName.Location = New System.Drawing.Point(99, 34)
    Me.Label_FundName.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_FundName.Name = "Label_FundName"
    Me.Label_FundName.Size = New System.Drawing.Size(647, 19)
    Me.Label_FundName.TabIndex = 5
    Me.Label_FundName.Text = "Fund Name"
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(7, 35)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(62, 13)
    Me.Label5.TabIndex = 4
    Me.Label5.Text = "Fund Name"
    '
    'Label_DataSource
    '
    Me.Label_DataSource.BackColor = System.Drawing.SystemColors.Window
    Me.Label_DataSource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_DataSource.Location = New System.Drawing.Point(422, 8)
    Me.Label_DataSource.Name = "Label_DataSource"
    Me.Label_DataSource.Size = New System.Drawing.Size(230, 19)
    Me.Label_DataSource.TabIndex = 3
    Me.Label_DataSource.Text = "Data Source"
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(335, 9)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(84, 13)
    Me.Label4.TabIndex = 2
    Me.Label4.Text = "Data Source"
    '
    'Label_Strategy
    '
    Me.Label_Strategy.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Strategy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Strategy.Location = New System.Drawing.Point(99, 8)
    Me.Label_Strategy.Name = "Label_Strategy"
    Me.Label_Strategy.Size = New System.Drawing.Size(230, 19)
    Me.Label_Strategy.TabIndex = 1
    Me.Label_Strategy.Text = "Strategy"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(7, 9)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(46, 13)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "Strategy"
    '
    'Tab_Contacts
    '
    Me.Tab_Contacts.AutoScroll = True
    Me.Tab_Contacts.Controls.Add(Me.Label13)
    Me.Tab_Contacts.Controls.Add(Me.LinkLabel_WebSite)
    Me.Tab_Contacts.Controls.Add(Me.Label27)
    Me.Tab_Contacts.Controls.Add(Me.Label_Administrator2)
    Me.Tab_Contacts.Controls.Add(Me.Label3)
    Me.Tab_Contacts.Controls.Add(Me.Label_Managers)
    Me.Tab_Contacts.Controls.Add(Me.Label_MarketingFax)
    Me.Tab_Contacts.Controls.Add(Me.Label_MarketingContact)
    Me.Tab_Contacts.Controls.Add(Me.Label17)
    Me.Tab_Contacts.Controls.Add(Me.Label21)
    Me.Tab_Contacts.Controls.Add(Me.Label_MarketingPhone)
    Me.Tab_Contacts.Controls.Add(Me.Label25)
    Me.Tab_Contacts.Controls.Add(Me.Label_ContactEMail)
    Me.Tab_Contacts.Controls.Add(Me.Label23)
    Me.Tab_Contacts.Controls.Add(Me.Label9)
    Me.Tab_Contacts.Controls.Add(Me.Label_ContactFax)
    Me.Tab_Contacts.Controls.Add(Me.Label_ContactPhone)
    Me.Tab_Contacts.Controls.Add(Me.Label_Contact)
    Me.Tab_Contacts.Controls.Add(Me.Label33)
    Me.Tab_Contacts.Controls.Add(Me.Label31)
    Me.Tab_Contacts.Controls.Add(Me.Label_Country)
    Me.Tab_Contacts.Controls.Add(Me.Label_State)
    Me.Tab_Contacts.Controls.Add(Me.Label_Zip)
    Me.Tab_Contacts.Controls.Add(Me.Label_City)
    Me.Tab_Contacts.Controls.Add(Me.Label_Address2)
    Me.Tab_Contacts.Controls.Add(Me.Label_Address1)
    Me.Tab_Contacts.Controls.Add(Me.Label11)
    Me.Tab_Contacts.Controls.Add(Me.Label_ManagementCo2)
    Me.Tab_Contacts.Controls.Add(Me.Label15)
    Me.Tab_Contacts.Controls.Add(Me.Label_FundName2)
    Me.Tab_Contacts.Controls.Add(Me.Label19)
    Me.Tab_Contacts.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Contacts.Name = "Tab_Contacts"
    Me.Tab_Contacts.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Contacts.TabIndex = 2
    Me.Tab_Contacts.Text = "Contacts"
    Me.Tab_Contacts.UseVisualStyleBackColor = True
    '
    'Label13
    '
    Me.Label13.AutoSize = True
    Me.Label13.Location = New System.Drawing.Point(6, 354)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(51, 13)
    Me.Label13.TabIndex = 30
    Me.Label13.Text = "Web Site"
    '
    'LinkLabel_WebSite
    '
    Me.LinkLabel_WebSite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.LinkLabel_WebSite.BackColor = System.Drawing.SystemColors.Window
    Me.LinkLabel_WebSite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.LinkLabel_WebSite.Location = New System.Drawing.Point(98, 353)
    Me.LinkLabel_WebSite.MinimumSize = New System.Drawing.Size(200, 19)
    Me.LinkLabel_WebSite.Name = "LinkLabel_WebSite"
    Me.LinkLabel_WebSite.Size = New System.Drawing.Size(647, 19)
    Me.LinkLabel_WebSite.TabIndex = 17
    Me.LinkLabel_WebSite.TabStop = True
    Me.LinkLabel_WebSite.Text = "WebSite"
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.Location = New System.Drawing.Point(6, 324)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(67, 13)
    Me.Label27.TabIndex = 29
    Me.Label27.Text = "Administrator"
    '
    'Label_Administrator2
    '
    Me.Label_Administrator2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Administrator2.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Administrator2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Administrator2.Location = New System.Drawing.Point(98, 323)
    Me.Label_Administrator2.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_Administrator2.Name = "Label_Administrator2"
    Me.Label_Administrator2.Size = New System.Drawing.Size(647, 19)
    Me.Label_Administrator2.TabIndex = 16
    Me.Label_Administrator2.Text = "Administrator"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(6, 299)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(54, 13)
    Me.Label3.TabIndex = 28
    Me.Label3.Text = "Managers"
    '
    'Label_Managers
    '
    Me.Label_Managers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Managers.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Managers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Managers.Location = New System.Drawing.Point(98, 298)
    Me.Label_Managers.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_Managers.Name = "Label_Managers"
    Me.Label_Managers.Size = New System.Drawing.Size(647, 19)
    Me.Label_Managers.TabIndex = 15
    Me.Label_Managers.Text = "Managers"
    '
    'Label_MarketingFax
    '
    Me.Label_MarketingFax.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MarketingFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MarketingFax.Location = New System.Drawing.Point(254, 267)
    Me.Label_MarketingFax.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_MarketingFax.Name = "Label_MarketingFax"
    Me.Label_MarketingFax.Size = New System.Drawing.Size(150, 19)
    Me.Label_MarketingFax.TabIndex = 14
    Me.Label_MarketingFax.Text = "ContactFax"
    '
    'Label_MarketingContact
    '
    Me.Label_MarketingContact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_MarketingContact.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MarketingContact.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MarketingContact.Location = New System.Drawing.Point(98, 242)
    Me.Label_MarketingContact.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_MarketingContact.Name = "Label_MarketingContact"
    Me.Label_MarketingContact.Size = New System.Drawing.Size(647, 19)
    Me.Label_MarketingContact.TabIndex = 12
    Me.Label_MarketingContact.Text = "Contact"
    '
    'Label17
    '
    Me.Label17.AutoSize = True
    Me.Label17.Location = New System.Drawing.Point(6, 268)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(61, 13)
    Me.Label17.TabIndex = 27
    Me.Label17.Text = "Phone, Fax"
    '
    'Label21
    '
    Me.Label21.AutoSize = True
    Me.Label21.Location = New System.Drawing.Point(6, 243)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(94, 13)
    Me.Label21.TabIndex = 26
    Me.Label21.Text = "Marketing Contact"
    '
    'Label_MarketingPhone
    '
    Me.Label_MarketingPhone.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MarketingPhone.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MarketingPhone.Location = New System.Drawing.Point(98, 267)
    Me.Label_MarketingPhone.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_MarketingPhone.Name = "Label_MarketingPhone"
    Me.Label_MarketingPhone.Size = New System.Drawing.Size(150, 19)
    Me.Label_MarketingPhone.TabIndex = 13
    Me.Label_MarketingPhone.Text = "ContactPhone"
    '
    'Label25
    '
    Me.Label25.AutoSize = True
    Me.Label25.Location = New System.Drawing.Point(6, 213)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(33, 13)
    Me.Label25.TabIndex = 25
    Me.Label25.Text = "EMail"
    '
    'Label_ContactEMail
    '
    Me.Label_ContactEMail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ContactEMail.BackColor = System.Drawing.SystemColors.Window
    Me.Label_ContactEMail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ContactEMail.Location = New System.Drawing.Point(98, 212)
    Me.Label_ContactEMail.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_ContactEMail.Name = "Label_ContactEMail"
    Me.Label_ContactEMail.Size = New System.Drawing.Size(647, 19)
    Me.Label_ContactEMail.TabIndex = 11
    Me.Label_ContactEMail.Text = "ContactEMail"
    '
    'Label23
    '
    Me.Label23.AutoSize = True
    Me.Label23.Location = New System.Drawing.Point(6, 188)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(61, 13)
    Me.Label23.TabIndex = 24
    Me.Label23.Text = "Phone, Fax"
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Location = New System.Drawing.Point(6, 163)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(44, 13)
    Me.Label9.TabIndex = 23
    Me.Label9.Text = "Contact"
    '
    'Label_ContactFax
    '
    Me.Label_ContactFax.BackColor = System.Drawing.SystemColors.Window
    Me.Label_ContactFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ContactFax.Location = New System.Drawing.Point(254, 187)
    Me.Label_ContactFax.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_ContactFax.Name = "Label_ContactFax"
    Me.Label_ContactFax.Size = New System.Drawing.Size(150, 19)
    Me.Label_ContactFax.TabIndex = 10
    Me.Label_ContactFax.Text = "ContactFax"
    '
    'Label_ContactPhone
    '
    Me.Label_ContactPhone.BackColor = System.Drawing.SystemColors.Window
    Me.Label_ContactPhone.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ContactPhone.Location = New System.Drawing.Point(98, 187)
    Me.Label_ContactPhone.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_ContactPhone.Name = "Label_ContactPhone"
    Me.Label_ContactPhone.Size = New System.Drawing.Size(150, 19)
    Me.Label_ContactPhone.TabIndex = 9
    Me.Label_ContactPhone.Text = "ContactPhone"
    '
    'Label_Contact
    '
    Me.Label_Contact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Contact.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Contact.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Contact.Location = New System.Drawing.Point(98, 162)
    Me.Label_Contact.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_Contact.Name = "Label_Contact"
    Me.Label_Contact.Size = New System.Drawing.Size(647, 19)
    Me.Label_Contact.TabIndex = 8
    Me.Label_Contact.Text = "Contact"
    '
    'Label33
    '
    Me.Label33.AutoSize = True
    Me.Label33.Location = New System.Drawing.Point(6, 133)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(64, 13)
    Me.Label33.TabIndex = 22
    Me.Label33.Text = "Zip, Country"
    '
    'Label31
    '
    Me.Label31.AutoSize = True
    Me.Label31.Location = New System.Drawing.Point(6, 108)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(55, 13)
    Me.Label31.TabIndex = 21
    Me.Label31.Text = "City, State"
    '
    'Label_Country
    '
    Me.Label_Country.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Country.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Country.Location = New System.Drawing.Point(254, 132)
    Me.Label_Country.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_Country.Name = "Label_Country"
    Me.Label_Country.Size = New System.Drawing.Size(150, 19)
    Me.Label_Country.TabIndex = 7
    Me.Label_Country.Text = "Country"
    '
    'Label_State
    '
    Me.Label_State.BackColor = System.Drawing.SystemColors.Window
    Me.Label_State.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_State.Location = New System.Drawing.Point(254, 107)
    Me.Label_State.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_State.Name = "Label_State"
    Me.Label_State.Size = New System.Drawing.Size(150, 19)
    Me.Label_State.TabIndex = 5
    Me.Label_State.Text = "State"
    '
    'Label_Zip
    '
    Me.Label_Zip.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Zip.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Zip.Location = New System.Drawing.Point(98, 132)
    Me.Label_Zip.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_Zip.Name = "Label_Zip"
    Me.Label_Zip.Size = New System.Drawing.Size(150, 19)
    Me.Label_Zip.TabIndex = 6
    Me.Label_Zip.Text = "Zip"
    '
    'Label_City
    '
    Me.Label_City.BackColor = System.Drawing.SystemColors.Window
    Me.Label_City.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_City.Location = New System.Drawing.Point(98, 107)
    Me.Label_City.MinimumSize = New System.Drawing.Size(150, 19)
    Me.Label_City.Name = "Label_City"
    Me.Label_City.Size = New System.Drawing.Size(150, 19)
    Me.Label_City.TabIndex = 4
    Me.Label_City.Text = "City"
    '
    'Label_Address2
    '
    Me.Label_Address2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Address2.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Address2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Address2.Location = New System.Drawing.Point(98, 82)
    Me.Label_Address2.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_Address2.Name = "Label_Address2"
    Me.Label_Address2.Size = New System.Drawing.Size(647, 19)
    Me.Label_Address2.TabIndex = 3
    Me.Label_Address2.Text = "Address2"
    '
    'Label_Address1
    '
    Me.Label_Address1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Address1.BackColor = System.Drawing.SystemColors.Window
    Me.Label_Address1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Address1.Location = New System.Drawing.Point(98, 57)
    Me.Label_Address1.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_Address1.Name = "Label_Address1"
    Me.Label_Address1.Size = New System.Drawing.Size(647, 19)
    Me.Label_Address1.TabIndex = 2
    Me.Label_Address1.Text = "Address1"
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Location = New System.Drawing.Point(6, 59)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(45, 13)
    Me.Label11.TabIndex = 20
    Me.Label11.Text = "Address"
    '
    'Label_ManagementCo2
    '
    Me.Label_ManagementCo2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ManagementCo2.BackColor = System.Drawing.SystemColors.Window
    Me.Label_ManagementCo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ManagementCo2.Location = New System.Drawing.Point(98, 32)
    Me.Label_ManagementCo2.MinimumSize = New System.Drawing.Size(200, 19)
    Me.Label_ManagementCo2.Name = "Label_ManagementCo2"
    Me.Label_ManagementCo2.Size = New System.Drawing.Size(647, 19)
    Me.Label_ManagementCo2.TabIndex = 1
    Me.Label_ManagementCo2.Text = "ManagementCo2"
    '
    'Label15
    '
    Me.Label15.AutoSize = True
    Me.Label15.Location = New System.Drawing.Point(6, 33)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(85, 13)
    Me.Label15.TabIndex = 19
    Me.Label15.Text = "Management Co"
    '
    'Label_FundName2
    '
    Me.Label_FundName2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_FundName2.BackColor = System.Drawing.SystemColors.Window
    Me.Label_FundName2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_FundName2.Location = New System.Drawing.Point(98, 7)
    Me.Label_FundName2.Name = "Label_FundName2"
    Me.Label_FundName2.Size = New System.Drawing.Size(647, 19)
    Me.Label_FundName2.TabIndex = 0
    Me.Label_FundName2.Text = "FundName"
    '
    'Label19
    '
    Me.Label19.AutoSize = True
    Me.Label19.Location = New System.Drawing.Point(6, 8)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(62, 13)
    Me.Label19.TabIndex = 18
    Me.Label19.Text = "Fund Name"
    '
    'Tab_Performance
    '
    Me.Tab_Performance.AutoScroll = True
    Me.Tab_Performance.Controls.Add(Me.TabControl_PerformanceTables)
    Me.Tab_Performance.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Performance.Name = "Tab_Performance"
    Me.Tab_Performance.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Performance.TabIndex = 3
    Me.Tab_Performance.Text = "Performance"
    Me.Tab_Performance.UseVisualStyleBackColor = True
    '
    'TabControl_PerformanceTables
    '
    Me.TabControl_PerformanceTables.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_PerformanceTables.Controls.Add(Me.TabPage1)
    Me.TabControl_PerformanceTables.Location = New System.Drawing.Point(3, 3)
    Me.TabControl_PerformanceTables.Multiline = True
    Me.TabControl_PerformanceTables.Name = "TabControl_PerformanceTables"
    Me.TabControl_PerformanceTables.SelectedIndex = 0
    Me.TabControl_PerformanceTables.Size = New System.Drawing.Size(749, 643)
    Me.TabControl_PerformanceTables.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.Label_PT_0)
    Me.TabPage1.Controls.Add(Me.Grid_Performance_0)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(741, 617)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "Stock1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'Label_PT_0
    '
    Me.Label_PT_0.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_PT_0.BackColor = System.Drawing.SystemColors.Window
    Me.Label_PT_0.Location = New System.Drawing.Point(3, 3)
    Me.Label_PT_0.Name = "Label_PT_0"
    Me.Label_PT_0.Size = New System.Drawing.Size(735, 19)
    Me.Label_PT_0.TabIndex = 0
    Me.Label_PT_0.Text = "FundName"
    '
    'Grid_Performance_0
    '
    Me.Grid_Performance_0.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Performance_0.ColumnInfo = resources.GetString("Grid_Performance_0.ColumnInfo")
    Me.Grid_Performance_0.Location = New System.Drawing.Point(3, 21)
    Me.Grid_Performance_0.Name = "Grid_Performance_0"
    Me.Grid_Performance_0.Rows.DefaultSize = 17
    Me.Grid_Performance_0.Size = New System.Drawing.Size(738, 593)
    Me.Grid_Performance_0.StyleInfo = resources.GetString("Grid_Performance_0.StyleInfo")
    Me.Grid_Performance_0.TabIndex = 1
    '
    'Tab_Comparisons
    '
    Me.Tab_Comparisons.Controls.Add(Me.Combo_Comparisons_Group)
    Me.Tab_Comparisons.Controls.Add(Me.Label63)
    Me.Tab_Comparisons.Controls.Add(Me.Panel3)
    Me.Tab_Comparisons.Controls.Add(Me.TabControl_Comparisons)
    Me.Tab_Comparisons.Controls.Add(Me.Radio_ComparisonIsReference)
    Me.Tab_Comparisons.Controls.Add(Me.Radio_ListIsReference)
    Me.Tab_Comparisons.Controls.Add(Me.Combo_Compare_Series)
    Me.Tab_Comparisons.Controls.Add(Me.Button_Compare_DefaultDates)
    Me.Tab_Comparisons.Controls.Add(Me.Edit_Compare_Lamda)
    Me.Tab_Comparisons.Controls.Add(Me.Date_Compare_DateTo)
    Me.Tab_Comparisons.Controls.Add(Me.Date_Compare_DateFrom)
    Me.Tab_Comparisons.Controls.Add(Me.Label43)
    Me.Tab_Comparisons.Controls.Add(Me.Label44)
    Me.Tab_Comparisons.Controls.Add(Me.Label45)
    Me.Tab_Comparisons.Controls.Add(Me.Combo_Compare_Group)
    Me.Tab_Comparisons.Controls.Add(Me.Label46)
    Me.Tab_Comparisons.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Comparisons.Name = "Tab_Comparisons"
    Me.Tab_Comparisons.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Comparisons.TabIndex = 4
    Me.Tab_Comparisons.Text = "Comparisons"
    Me.Tab_Comparisons.UseVisualStyleBackColor = True
    '
    'Combo_Comparisons_Group
    '
    Me.Combo_Comparisons_Group.FormattingEnabled = True
    Me.Combo_Comparisons_Group.Location = New System.Drawing.Point(249, 29)
    Me.Combo_Comparisons_Group.Name = "Combo_Comparisons_Group"
    Me.Combo_Comparisons_Group.Size = New System.Drawing.Size(472, 21)
    Me.Combo_Comparisons_Group.TabIndex = 30
    '
    'Label63
    '
    Me.Label63.AutoSize = True
    Me.Label63.Location = New System.Drawing.Point(6, 32)
    Me.Label63.Name = "Label63"
    Me.Label63.Size = New System.Drawing.Size(94, 13)
    Me.Label63.TabIndex = 27
    Me.Label63.Text = "Comparison Series"
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.RadioComparison_IsGroup)
    Me.Panel3.Controls.Add(Me.RadioComparison_IsPertrac)
    Me.Panel3.Location = New System.Drawing.Point(109, 28)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(136, 24)
    Me.Panel3.TabIndex = 28
    '
    'RadioComparison_IsGroup
    '
    Me.RadioComparison_IsGroup.AutoSize = True
    Me.RadioComparison_IsGroup.Location = New System.Drawing.Point(68, 2)
    Me.RadioComparison_IsGroup.Name = "RadioComparison_IsGroup"
    Me.RadioComparison_IsGroup.Size = New System.Drawing.Size(54, 17)
    Me.RadioComparison_IsGroup.TabIndex = 16
    Me.RadioComparison_IsGroup.TabStop = True
    Me.RadioComparison_IsGroup.Text = "Group"
    Me.RadioComparison_IsGroup.UseVisualStyleBackColor = True
    '
    'RadioComparison_IsPertrac
    '
    Me.RadioComparison_IsPertrac.AutoSize = True
    Me.RadioComparison_IsPertrac.Location = New System.Drawing.Point(3, 2)
    Me.RadioComparison_IsPertrac.Name = "RadioComparison_IsPertrac"
    Me.RadioComparison_IsPertrac.Size = New System.Drawing.Size(59, 17)
    Me.RadioComparison_IsPertrac.TabIndex = 15
    Me.RadioComparison_IsPertrac.TabStop = True
    Me.RadioComparison_IsPertrac.Text = "Pertrac"
    Me.RadioComparison_IsPertrac.UseVisualStyleBackColor = True
    '
    'TabControl_Comparisons
    '
    Me.TabControl_Comparisons.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Statistics)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Correlation)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Correlation_UpDown)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Beta)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Beta_UpDown)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Alpha)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_APR)
    Me.TabControl_Comparisons.Controls.Add(Me.Tab_Comp_Volatility)
    Me.TabControl_Comparisons.Location = New System.Drawing.Point(0, 102)
    Me.TabControl_Comparisons.Margin = New System.Windows.Forms.Padding(1)
    Me.TabControl_Comparisons.Name = "TabControl_Comparisons"
    Me.TabControl_Comparisons.SelectedIndex = 0
    Me.TabControl_Comparisons.Size = New System.Drawing.Size(755, 542)
    Me.TabControl_Comparisons.TabIndex = 11
    '
    'Tab_Comp_Statistics
    '
    Me.Tab_Comp_Statistics.Controls.Add(Me.Grid_Comp_Statistics)
    Me.Tab_Comp_Statistics.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Statistics.Name = "Tab_Comp_Statistics"
    Me.Tab_Comp_Statistics.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Comp_Statistics.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Statistics.TabIndex = 1
    Me.Tab_Comp_Statistics.Text = "Statistics"
    Me.Tab_Comp_Statistics.UseVisualStyleBackColor = True
    '
    'Grid_Comp_Statistics
    '
    Me.Grid_Comp_Statistics.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Comp_Statistics.ColumnInfo = resources.GetString("Grid_Comp_Statistics.ColumnInfo")
    Me.Grid_Comp_Statistics.Location = New System.Drawing.Point(0, 0)
    Me.Grid_Comp_Statistics.Name = "Grid_Comp_Statistics"
    Me.Grid_Comp_Statistics.Rows.Count = 2
    Me.Grid_Comp_Statistics.Rows.DefaultSize = 17
    Me.Grid_Comp_Statistics.Size = New System.Drawing.Size(746, 516)
    Me.Grid_Comp_Statistics.StyleInfo = resources.GetString("Grid_Comp_Statistics.StyleInfo")
    Me.Grid_Comp_Statistics.TabIndex = 0
    '
    'Tab_Comp_Correlation
    '
    Me.Tab_Comp_Correlation.Controls.Add(Me.Chart_Comparison_Correlation)
    Me.Tab_Comp_Correlation.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Correlation.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_Comp_Correlation.Name = "Tab_Comp_Correlation"
    Me.Tab_Comp_Correlation.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Comp_Correlation.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Correlation.TabIndex = 0
    Me.Tab_Comp_Correlation.Text = "Correlation"
    Me.Tab_Comp_Correlation.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_Correlation
    '
    Me.Chart_Comparison_Correlation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_Correlation.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_Correlation.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_Correlation.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_Correlation.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_Correlation.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_Correlation.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea21.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea21.AxisX.Interval = 1
    ChartArea21.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea21.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea21.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea21.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea21.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea21.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea21.AxisX.MajorTickMark.Enabled = False
    ChartArea21.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisY.Crossing = 0
    ChartArea21.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea21.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea21.AxisY.LabelStyle.Format = "N2"
    ChartArea21.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea21.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea21.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea21.BackColor = System.Drawing.Color.Transparent
    ChartArea21.BorderColor = System.Drawing.Color.DimGray
    ChartArea21.Name = "Default"
    Me.Chart_Comparison_Correlation.ChartAreas.Add(ChartArea21)
    Legend21.BackColor = System.Drawing.Color.Transparent
    Legend21.BorderColor = System.Drawing.Color.Transparent
    Legend21.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend21.DockToChartArea = "Default"
    Legend21.Enabled = False
    Legend21.Name = "Default"
    Me.Chart_Comparison_Correlation.Legends.Add(Legend21)
    Me.Chart_Comparison_Correlation.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_Correlation.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_Correlation.Name = "Chart_Comparison_Correlation"
    Me.Chart_Comparison_Correlation.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series36.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series36.ChartType = "Bar"
    Series36.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series36.Name = "Series1"
    Series36.ShadowOffset = 1
    Series36.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series36.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_Correlation.Series.Add(Series36)
    Me.Chart_Comparison_Correlation.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_Correlation.TabIndex = 3
    Me.Chart_Comparison_Correlation.Text = "Chart2"
    '
    'Tab_Comp_Correlation_UpDown
    '
    Me.Tab_Comp_Correlation_UpDown.Controls.Add(Me.Chart_Comparison_CorrelationUpDown)
    Me.Tab_Comp_Correlation_UpDown.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Correlation_UpDown.Name = "Tab_Comp_Correlation_UpDown"
    Me.Tab_Comp_Correlation_UpDown.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Correlation_UpDown.TabIndex = 2
    Me.Tab_Comp_Correlation_UpDown.Text = "Correlation (Up / Down)"
    Me.Tab_Comp_Correlation_UpDown.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_CorrelationUpDown
    '
    Me.Chart_Comparison_CorrelationUpDown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_CorrelationUpDown.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_CorrelationUpDown.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_CorrelationUpDown.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_CorrelationUpDown.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_CorrelationUpDown.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_CorrelationUpDown.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea22.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea22.AxisX.Interval = 1
    ChartArea22.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea22.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea22.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea22.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea22.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea22.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea22.AxisX.MajorTickMark.Enabled = False
    ChartArea22.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisY.Crossing = 0
    ChartArea22.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea22.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea22.AxisY.LabelStyle.Format = "N2"
    ChartArea22.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea22.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea22.AxisY.StartFromZero = False
    ChartArea22.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea22.BackColor = System.Drawing.Color.Transparent
    ChartArea22.BorderColor = System.Drawing.Color.DimGray
    ChartArea22.Name = "Default"
    Me.Chart_Comparison_CorrelationUpDown.ChartAreas.Add(ChartArea22)
    Legend22.BackColor = System.Drawing.Color.Transparent
    Legend22.BorderColor = System.Drawing.Color.Transparent
    Legend22.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend22.DockToChartArea = "Default"
    Legend22.Enabled = False
    Legend22.Name = "Default"
    Me.Chart_Comparison_CorrelationUpDown.Legends.Add(Legend22)
    Me.Chart_Comparison_CorrelationUpDown.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_CorrelationUpDown.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_CorrelationUpDown.Name = "Chart_Comparison_CorrelationUpDown"
    Me.Chart_Comparison_CorrelationUpDown.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series37.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series37.ChartType = "Bar"
    Series37.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series37.Name = "Series1"
    Series37.ShadowOffset = 1
    Series37.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series37.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series38.ChartType = "Bar"
    Series38.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series38.Name = "Series2"
    Series38.ShadowOffset = 1
    Series38.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series38.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_CorrelationUpDown.Series.Add(Series37)
    Me.Chart_Comparison_CorrelationUpDown.Series.Add(Series38)
    Me.Chart_Comparison_CorrelationUpDown.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_CorrelationUpDown.TabIndex = 4
    Me.Chart_Comparison_CorrelationUpDown.Text = "Chart2"
    '
    'Tab_Comp_Beta
    '
    Me.Tab_Comp_Beta.Controls.Add(Me.Chart_Comparison_Beta)
    Me.Tab_Comp_Beta.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Beta.Name = "Tab_Comp_Beta"
    Me.Tab_Comp_Beta.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Beta.TabIndex = 3
    Me.Tab_Comp_Beta.Text = "Beta"
    Me.Tab_Comp_Beta.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_Beta
    '
    Me.Chart_Comparison_Beta.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_Beta.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_Beta.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_Beta.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_Beta.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_Beta.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_Beta.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea23.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea23.AxisX.Interval = 1
    ChartArea23.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea23.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea23.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea23.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea23.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea23.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea23.AxisX.MajorTickMark.Enabled = False
    ChartArea23.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisY.Crossing = 0
    ChartArea23.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea23.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea23.AxisY.LabelStyle.Format = "N2"
    ChartArea23.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea23.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea23.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea23.BackColor = System.Drawing.Color.Transparent
    ChartArea23.BorderColor = System.Drawing.Color.DimGray
    ChartArea23.Name = "Default"
    Me.Chart_Comparison_Beta.ChartAreas.Add(ChartArea23)
    Legend23.BackColor = System.Drawing.Color.Transparent
    Legend23.BorderColor = System.Drawing.Color.Transparent
    Legend23.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend23.DockToChartArea = "Default"
    Legend23.Enabled = False
    Legend23.Name = "Default"
    Me.Chart_Comparison_Beta.Legends.Add(Legend23)
    Me.Chart_Comparison_Beta.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_Beta.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_Beta.Name = "Chart_Comparison_Beta"
    Me.Chart_Comparison_Beta.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series39.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series39.ChartType = "Bar"
    Series39.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series39.Name = "Series1"
    Series39.ShadowOffset = 1
    Series39.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series39.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_Beta.Series.Add(Series39)
    Me.Chart_Comparison_Beta.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_Beta.TabIndex = 4
    Me.Chart_Comparison_Beta.Text = "Chart2"
    '
    'Tab_Comp_Beta_UpDown
    '
    Me.Tab_Comp_Beta_UpDown.Controls.Add(Me.Chart_Comparison_BetaUpDown)
    Me.Tab_Comp_Beta_UpDown.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Beta_UpDown.Name = "Tab_Comp_Beta_UpDown"
    Me.Tab_Comp_Beta_UpDown.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Beta_UpDown.TabIndex = 4
    Me.Tab_Comp_Beta_UpDown.Text = "Beta (Up / Down)"
    Me.Tab_Comp_Beta_UpDown.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_BetaUpDown
    '
    Me.Chart_Comparison_BetaUpDown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_BetaUpDown.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_BetaUpDown.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_BetaUpDown.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_BetaUpDown.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_BetaUpDown.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_BetaUpDown.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea24.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea24.AxisX.Interval = 1
    ChartArea24.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea24.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea24.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea24.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea24.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea24.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea24.AxisX.MajorTickMark.Enabled = False
    ChartArea24.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisY.Crossing = 0
    ChartArea24.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea24.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea24.AxisY.LabelStyle.Format = "N2"
    ChartArea24.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea24.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea24.AxisY.StartFromZero = False
    ChartArea24.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea24.BackColor = System.Drawing.Color.Transparent
    ChartArea24.BorderColor = System.Drawing.Color.DimGray
    ChartArea24.Name = "Default"
    Me.Chart_Comparison_BetaUpDown.ChartAreas.Add(ChartArea24)
    Legend24.BackColor = System.Drawing.Color.Transparent
    Legend24.BorderColor = System.Drawing.Color.Transparent
    Legend24.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend24.DockToChartArea = "Default"
    Legend24.Enabled = False
    Legend24.Name = "Default"
    Me.Chart_Comparison_BetaUpDown.Legends.Add(Legend24)
    Me.Chart_Comparison_BetaUpDown.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_BetaUpDown.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_BetaUpDown.Name = "Chart_Comparison_BetaUpDown"
    Me.Chart_Comparison_BetaUpDown.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series40.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series40.ChartType = "Bar"
    Series40.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series40.Name = "Series1"
    Series40.ShadowOffset = 1
    Series40.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series40.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series41.ChartType = "Bar"
    Series41.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series41.Name = "Series2"
    Series41.ShadowOffset = 1
    Series41.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series41.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_BetaUpDown.Series.Add(Series40)
    Me.Chart_Comparison_BetaUpDown.Series.Add(Series41)
    Me.Chart_Comparison_BetaUpDown.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_BetaUpDown.TabIndex = 5
    Me.Chart_Comparison_BetaUpDown.Text = "Chart2"
    '
    'Tab_Comp_Alpha
    '
    Me.Tab_Comp_Alpha.Controls.Add(Me.Chart_Comparison_Alpha)
    Me.Tab_Comp_Alpha.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Alpha.Name = "Tab_Comp_Alpha"
    Me.Tab_Comp_Alpha.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Alpha.TabIndex = 5
    Me.Tab_Comp_Alpha.Text = "Alpha"
    Me.Tab_Comp_Alpha.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_Alpha
    '
    Me.Chart_Comparison_Alpha.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_Alpha.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_Alpha.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_Alpha.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_Alpha.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_Alpha.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_Alpha.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea25.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea25.AxisX.Interval = 1
    ChartArea25.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea25.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea25.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea25.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea25.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea25.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea25.AxisX.MajorTickMark.Enabled = False
    ChartArea25.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisY.Crossing = 0
    ChartArea25.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea25.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea25.AxisY.LabelStyle.Format = "P2"
    ChartArea25.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea25.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea25.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea25.BackColor = System.Drawing.Color.Transparent
    ChartArea25.BorderColor = System.Drawing.Color.DimGray
    ChartArea25.Name = "Default"
    Me.Chart_Comparison_Alpha.ChartAreas.Add(ChartArea25)
    Legend25.BackColor = System.Drawing.Color.Transparent
    Legend25.BorderColor = System.Drawing.Color.Transparent
    Legend25.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend25.DockToChartArea = "Default"
    Legend25.Enabled = False
    Legend25.Name = "Default"
    Me.Chart_Comparison_Alpha.Legends.Add(Legend25)
    Me.Chart_Comparison_Alpha.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_Alpha.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_Alpha.Name = "Chart_Comparison_Alpha"
    Me.Chart_Comparison_Alpha.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series42.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series42.ChartType = "Bar"
    Series42.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series42.Name = "Series1"
    Series42.ShadowOffset = 1
    Series42.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series42.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_Alpha.Series.Add(Series42)
    Me.Chart_Comparison_Alpha.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_Alpha.TabIndex = 5
    Me.Chart_Comparison_Alpha.Text = "Chart2"
    '
    'Tab_Comp_APR
    '
    Me.Tab_Comp_APR.Controls.Add(Me.Chart_Comparison_APR)
    Me.Tab_Comp_APR.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_APR.Name = "Tab_Comp_APR"
    Me.Tab_Comp_APR.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_APR.TabIndex = 6
    Me.Tab_Comp_APR.Text = "APR"
    Me.Tab_Comp_APR.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_APR
    '
    Me.Chart_Comparison_APR.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_APR.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_APR.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_APR.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_APR.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_APR.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_APR.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea26.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea26.AxisX.Interval = 1
    ChartArea26.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea26.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea26.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea26.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea26.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea26.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea26.AxisX.MajorTickMark.Enabled = False
    ChartArea26.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisY.Crossing = 0
    ChartArea26.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea26.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea26.AxisY.LabelStyle.Format = "P0"
    ChartArea26.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea26.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea26.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea26.BackColor = System.Drawing.Color.Transparent
    ChartArea26.BorderColor = System.Drawing.Color.DimGray
    ChartArea26.Name = "Default"
    Me.Chart_Comparison_APR.ChartAreas.Add(ChartArea26)
    Legend26.BackColor = System.Drawing.Color.Transparent
    Legend26.BorderColor = System.Drawing.Color.Transparent
    Legend26.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend26.DockToChartArea = "Default"
    Legend26.Enabled = False
    Legend26.Name = "Default"
    Me.Chart_Comparison_APR.Legends.Add(Legend26)
    Me.Chart_Comparison_APR.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_APR.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_APR.Name = "Chart_Comparison_APR"
    Me.Chart_Comparison_APR.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series43.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series43.ChartType = "Bar"
    Series43.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series43.Name = "Series1"
    Series43.ShadowOffset = 1
    Series43.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series43.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_APR.Series.Add(Series43)
    Me.Chart_Comparison_APR.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_APR.TabIndex = 6
    Me.Chart_Comparison_APR.Text = "Chart3"
    '
    'Tab_Comp_Volatility
    '
    Me.Tab_Comp_Volatility.Controls.Add(Me.Chart_Comparison_Volatility)
    Me.Tab_Comp_Volatility.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Comp_Volatility.Name = "Tab_Comp_Volatility"
    Me.Tab_Comp_Volatility.Size = New System.Drawing.Size(747, 516)
    Me.Tab_Comp_Volatility.TabIndex = 7
    Me.Tab_Comp_Volatility.Text = "Volatility"
    Me.Tab_Comp_Volatility.UseVisualStyleBackColor = True
    '
    'Chart_Comparison_Volatility
    '
    Me.Chart_Comparison_Volatility.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Comparison_Volatility.BackColor = System.Drawing.Color.Azure
    Me.Chart_Comparison_Volatility.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Comparison_Volatility.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Comparison_Volatility.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Comparison_Volatility.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Comparison_Volatility.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea27.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea27.AxisX.Interval = 1
    ChartArea27.AxisX.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.Number
    ChartArea27.AxisX.LabelsAutoFitStyle = Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont
    ChartArea27.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
    ChartArea27.AxisX.LabelStyle.TruncatedLabels = True
    ChartArea27.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisX.MajorGrid.LineColor = System.Drawing.Color.Transparent
    ChartArea27.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea27.AxisX.MajorTickMark.Enabled = False
    ChartArea27.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisY.Crossing = 0
    ChartArea27.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea27.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea27.AxisY.LabelStyle.Format = "P2"
    ChartArea27.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea27.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea27.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea27.BackColor = System.Drawing.Color.Transparent
    ChartArea27.BorderColor = System.Drawing.Color.DimGray
    ChartArea27.Name = "Default"
    Me.Chart_Comparison_Volatility.ChartAreas.Add(ChartArea27)
    Legend27.BackColor = System.Drawing.Color.Transparent
    Legend27.BorderColor = System.Drawing.Color.Transparent
    Legend27.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend27.DockToChartArea = "Default"
    Legend27.Enabled = False
    Legend27.Name = "Default"
    Me.Chart_Comparison_Volatility.Legends.Add(Legend27)
    Me.Chart_Comparison_Volatility.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Comparison_Volatility.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Comparison_Volatility.Name = "Chart_Comparison_Volatility"
    Me.Chart_Comparison_Volatility.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series44.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series44.ChartType = "Bar"
    Series44.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    Series44.Name = "Series1"
    Series44.ShadowOffset = 1
    Series44.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series44.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Comparison_Volatility.Series.Add(Series44)
    Me.Chart_Comparison_Volatility.Size = New System.Drawing.Size(747, 516)
    Me.Chart_Comparison_Volatility.TabIndex = 6
    Me.Chart_Comparison_Volatility.Text = "Chart4"
    '
    'Radio_ComparisonIsReference
    '
    Me.Radio_ComparisonIsReference.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Radio_ComparisonIsReference.Location = New System.Drawing.Point(557, 78)
    Me.Radio_ComparisonIsReference.Name = "Radio_ComparisonIsReference"
    Me.Radio_ComparisonIsReference.Size = New System.Drawing.Size(170, 17)
    Me.Radio_ComparisonIsReference.TabIndex = 4
    Me.Radio_ComparisonIsReference.TabStop = True
    Me.Radio_ComparisonIsReference.Text = " Comparison is reference"
    Me.Radio_ComparisonIsReference.UseVisualStyleBackColor = True
    '
    'Radio_ListIsReference
    '
    Me.Radio_ListIsReference.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Radio_ListIsReference.Location = New System.Drawing.Point(557, 54)
    Me.Radio_ListIsReference.Name = "Radio_ListIsReference"
    Me.Radio_ListIsReference.Size = New System.Drawing.Size(170, 17)
    Me.Radio_ListIsReference.TabIndex = 3
    Me.Radio_ListIsReference.TabStop = True
    Me.Radio_ListIsReference.Text = "List is Reference"
    Me.Radio_ListIsReference.UseVisualStyleBackColor = True
    '
    'Combo_Compare_Series
    '
    Me.Combo_Compare_Series.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Compare_Series.FormattingEnabled = True
    Me.Combo_Compare_Series.Location = New System.Drawing.Point(249, 29)
    Me.Combo_Compare_Series.MasternameCollection = Nothing
    Me.Combo_Compare_Series.Name = "Combo_Compare_Series"
    '
    '
    '
    Me.Combo_Compare_Series.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_Compare_Series.SelectNoMatch = False
    Me.Combo_Compare_Series.Size = New System.Drawing.Size(500, 20)
    Me.Combo_Compare_Series.TabIndex = 2
    Me.Combo_Compare_Series.ThemeName = "ControlDefault"
    '
    'Button_Compare_DefaultDates
    '
    Me.Button_Compare_DefaultDates.Location = New System.Drawing.Point(442, 55)
    Me.Button_Compare_DefaultDates.Name = "Button_Compare_DefaultDates"
    Me.Button_Compare_DefaultDates.Size = New System.Drawing.Size(72, 39)
    Me.Button_Compare_DefaultDates.TabIndex = 7
    Me.Button_Compare_DefaultDates.Text = "Default Dates"
    Me.Button_Compare_DefaultDates.UseVisualStyleBackColor = True
    '
    'Edit_Compare_Lamda
    '
    Me.Edit_Compare_Lamda.Location = New System.Drawing.Point(68, 78)
    Me.Edit_Compare_Lamda.Name = "Edit_Compare_Lamda"
    Me.Edit_Compare_Lamda.RenaissanceTag = Nothing
    Me.Edit_Compare_Lamda.SelectTextOnFocus = False
    Me.Edit_Compare_Lamda.Size = New System.Drawing.Size(138, 20)
    Me.Edit_Compare_Lamda.TabIndex = 8
    Me.Edit_Compare_Lamda.Text = "1.00"
    Me.Edit_Compare_Lamda.TextFormat = "#,##0.00"
    Me.Edit_Compare_Lamda.Value = 1
    '
    'Date_Compare_DateTo
    '
    Me.Date_Compare_DateTo.CustomFormat = "dd MMM yyyy"
    Me.Date_Compare_DateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_Compare_DateTo.Location = New System.Drawing.Point(288, 55)
    Me.Date_Compare_DateTo.Name = "Date_Compare_DateTo"
    Me.Date_Compare_DateTo.Size = New System.Drawing.Size(138, 20)
    Me.Date_Compare_DateTo.TabIndex = 6
    '
    'Date_Compare_DateFrom
    '
    Me.Date_Compare_DateFrom.CustomFormat = "dd MMM yyyy"
    Me.Date_Compare_DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_Compare_DateFrom.Location = New System.Drawing.Point(68, 55)
    Me.Date_Compare_DateFrom.Name = "Date_Compare_DateFrom"
    Me.Date_Compare_DateFrom.Size = New System.Drawing.Size(138, 20)
    Me.Date_Compare_DateFrom.TabIndex = 5
    Me.Date_Compare_DateFrom.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'Label43
    '
    Me.Label43.AutoSize = True
    Me.Label43.Location = New System.Drawing.Point(6, 81)
    Me.Label43.Name = "Label43"
    Me.Label43.Size = New System.Drawing.Size(39, 13)
    Me.Label43.TabIndex = 18
    Me.Label43.Text = "Lamda"
    '
    'Label44
    '
    Me.Label44.AutoSize = True
    Me.Label44.Location = New System.Drawing.Point(212, 59)
    Me.Label44.Name = "Label44"
    Me.Label44.Size = New System.Drawing.Size(46, 13)
    Me.Label44.TabIndex = 17
    Me.Label44.Text = "Date To"
    '
    'Label45
    '
    Me.Label45.AutoSize = True
    Me.Label45.Location = New System.Drawing.Point(6, 59)
    Me.Label45.Name = "Label45"
    Me.Label45.Size = New System.Drawing.Size(56, 13)
    Me.Label45.TabIndex = 16
    Me.Label45.Text = "Date From"
    '
    'Combo_Compare_Group
    '
    Me.Combo_Compare_Group.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Compare_Group.FormattingEnabled = True
    Me.Combo_Compare_Group.ItemHeight = 13
    Me.Combo_Compare_Group.Location = New System.Drawing.Point(249, 3)
    Me.Combo_Compare_Group.Name = "Combo_Compare_Group"
    Me.Combo_Compare_Group.Size = New System.Drawing.Size(500, 21)
    Me.Combo_Compare_Group.TabIndex = 1
    '
    'Label46
    '
    Me.Label46.AutoSize = True
    Me.Label46.Location = New System.Drawing.Point(6, 6)
    Me.Label46.Name = "Label46"
    Me.Label46.Size = New System.Drawing.Size(69, 13)
    Me.Label46.TabIndex = 0
    Me.Label46.Text = "Select Group"
    '
    'Tab_Charts
    '
    Me.Tab_Charts.Controls.Add(Me.Check_AutoStart)
    Me.Tab_Charts.Controls.Add(Me.Combo_Charts_CompareSeriesGroup)
    Me.Tab_Charts.Controls.Add(Me.Panel2)
    Me.Tab_Charts.Controls.Add(Me.Panel1)
    Me.Tab_Charts.Controls.Add(Me.Label47)
    Me.Tab_Charts.Controls.Add(Me.Combo_Chart_Conditional)
    Me.Tab_Charts.Controls.Add(Me.Button_DefaultDate)
    Me.Tab_Charts.Controls.Add(Me.Label39)
    Me.Tab_Charts.Controls.Add(Me.Text_Chart_RollingPeriod)
    Me.Tab_Charts.Controls.Add(Me.Text_Chart_Lamda)
    Me.Tab_Charts.Controls.Add(Me.Date_Charts_DateTo)
    Me.Tab_Charts.Controls.Add(Me.Date_Charts_DateFrom)
    Me.Tab_Charts.Controls.Add(Me.Label37)
    Me.Tab_Charts.Controls.Add(Me.Label38)
    Me.Tab_Charts.Controls.Add(Me.Label36)
    Me.Tab_Charts.Controls.Add(Me.Label35)
    Me.Tab_Charts.Controls.Add(Me.Combo_Charts_CompareSeriesPertrac)
    Me.Tab_Charts.Controls.Add(Me.Label29)
    Me.Tab_Charts.Controls.Add(Me.TabControl_Charts)
    Me.Tab_Charts.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Charts.Name = "Tab_Charts"
    Me.Tab_Charts.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Charts.TabIndex = 5
    Me.Tab_Charts.Text = "Charts"
    Me.Tab_Charts.UseVisualStyleBackColor = True
    '
    'Check_AutoStart
    '
    Me.Check_AutoStart.Location = New System.Drawing.Point(209, 29)
    Me.Check_AutoStart.Name = "Check_AutoStart"
    Me.Check_AutoStart.Size = New System.Drawing.Size(17, 16)
    Me.Check_AutoStart.TabIndex = 21
    Me.Check_AutoStart.UseVisualStyleBackColor = True
    '
    'Combo_Charts_CompareSeriesGroup
    '
    Me.Combo_Charts_CompareSeriesGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Charts_CompareSeriesGroup.FormattingEnabled = True
    Me.Combo_Charts_CompareSeriesGroup.Location = New System.Drawing.Point(228, 2)
    Me.Combo_Charts_CompareSeriesGroup.Name = "Combo_Charts_CompareSeriesGroup"
    Me.Combo_Charts_CompareSeriesGroup.Size = New System.Drawing.Size(522, 21)
    Me.Combo_Charts_CompareSeriesGroup.TabIndex = 20
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.Radio_Charts_CompareGroup)
    Me.Panel2.Controls.Add(Me.Radio_Charts_ComparePertrac)
    Me.Panel2.Location = New System.Drawing.Point(91, 1)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(136, 24)
    Me.Panel2.TabIndex = 19
    '
    'Radio_Charts_CompareGroup
    '
    Me.Radio_Charts_CompareGroup.AutoSize = True
    Me.Radio_Charts_CompareGroup.Location = New System.Drawing.Point(68, 2)
    Me.Radio_Charts_CompareGroup.Name = "Radio_Charts_CompareGroup"
    Me.Radio_Charts_CompareGroup.Size = New System.Drawing.Size(54, 17)
    Me.Radio_Charts_CompareGroup.TabIndex = 16
    Me.Radio_Charts_CompareGroup.TabStop = True
    Me.Radio_Charts_CompareGroup.Text = "Group"
    Me.Radio_Charts_CompareGroup.UseVisualStyleBackColor = True
    '
    'Radio_Charts_ComparePertrac
    '
    Me.Radio_Charts_ComparePertrac.AutoSize = True
    Me.Radio_Charts_ComparePertrac.Location = New System.Drawing.Point(3, 2)
    Me.Radio_Charts_ComparePertrac.Name = "Radio_Charts_ComparePertrac"
    Me.Radio_Charts_ComparePertrac.Size = New System.Drawing.Size(59, 17)
    Me.Radio_Charts_ComparePertrac.TabIndex = 15
    Me.Radio_Charts_ComparePertrac.TabStop = True
    Me.Radio_Charts_ComparePertrac.Text = "Pertrac"
    Me.Radio_Charts_ComparePertrac.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Text_ScalingFactor)
    Me.Panel1.Controls.Add(Me.Radio_DynamicScalingFactor)
    Me.Panel1.Controls.Add(Me.Radio_SingleScalingFactor)
    Me.Panel1.Location = New System.Drawing.Point(496, 25)
    Me.Panel1.Margin = New System.Windows.Forms.Padding(1)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(226, 49)
    Me.Panel1.TabIndex = 18
    '
    'Text_ScalingFactor
    '
    Me.Text_ScalingFactor.Location = New System.Drawing.Point(136, 3)
    Me.Text_ScalingFactor.Name = "Text_ScalingFactor"
    Me.Text_ScalingFactor.RenaissanceTag = Nothing
    Me.Text_ScalingFactor.SelectTextOnFocus = False
    Me.Text_ScalingFactor.Size = New System.Drawing.Size(56, 20)
    Me.Text_ScalingFactor.TabIndex = 15
    Me.Text_ScalingFactor.Text = "100.0%"
    Me.Text_ScalingFactor.TextFormat = "#,##0.0%"
    Me.Text_ScalingFactor.Value = 1
    '
    'Radio_DynamicScalingFactor
    '
    Me.Radio_DynamicScalingFactor.AutoSize = True
    Me.Radio_DynamicScalingFactor.Location = New System.Drawing.Point(5, 27)
    Me.Radio_DynamicScalingFactor.Name = "Radio_DynamicScalingFactor"
    Me.Radio_DynamicScalingFactor.Size = New System.Drawing.Size(198, 17)
    Me.Radio_DynamicScalingFactor.TabIndex = 16
    Me.Radio_DynamicScalingFactor.TabStop = True
    Me.Radio_DynamicScalingFactor.Text = "Match Volatility to Comparison Series"
    Me.Radio_DynamicScalingFactor.UseVisualStyleBackColor = True
    '
    'Radio_SingleScalingFactor
    '
    Me.Radio_SingleScalingFactor.AutoSize = True
    Me.Radio_SingleScalingFactor.Location = New System.Drawing.Point(5, 4)
    Me.Radio_SingleScalingFactor.Name = "Radio_SingleScalingFactor"
    Me.Radio_SingleScalingFactor.Size = New System.Drawing.Size(125, 17)
    Me.Radio_SingleScalingFactor.TabIndex = 14
    Me.Radio_SingleScalingFactor.TabStop = True
    Me.Radio_SingleScalingFactor.Text = "Single Scaling Factor"
    Me.Radio_SingleScalingFactor.UseVisualStyleBackColor = True
    '
    'Label47
    '
    Me.Label47.Location = New System.Drawing.Point(234, 51)
    Me.Label47.Name = "Label47"
    Me.Label47.Size = New System.Drawing.Size(101, 18)
    Me.Label47.TabIndex = 11
    Me.Label47.Text = "Corr, Alpha, Beta"
    Me.Label47.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_Chart_Conditional
    '
    Me.Combo_Chart_Conditional.FormattingEnabled = True
    Me.Combo_Chart_Conditional.ItemHeight = 13
    Me.Combo_Chart_Conditional.Location = New System.Drawing.Point(341, 49)
    Me.Combo_Chart_Conditional.Name = "Combo_Chart_Conditional"
    Me.Combo_Chart_Conditional.Size = New System.Drawing.Size(151, 21)
    Me.Combo_Chart_Conditional.TabIndex = 12
    '
    'Button_DefaultDate
    '
    Me.Button_DefaultDate.Location = New System.Drawing.Point(209, 50)
    Me.Button_DefaultDate.Name = "Button_DefaultDate"
    Me.Button_DefaultDate.Size = New System.Drawing.Size(18, 20)
    Me.Button_DefaultDate.TabIndex = 6
    Me.Button_DefaultDate.Text = ".."
    Me.Button_DefaultDate.UseVisualStyleBackColor = True
    '
    'Label39
    '
    Me.Label39.AutoSize = True
    Me.Label39.Location = New System.Drawing.Point(463, 29)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(29, 13)
    Me.Label39.TabIndex = 13
    Me.Label39.Text = "mths"
    '
    'Text_Chart_RollingPeriod
    '
    Me.Text_Chart_RollingPeriod.Location = New System.Drawing.Point(414, 25)
    Me.Text_Chart_RollingPeriod.Name = "Text_Chart_RollingPeriod"
    Me.Text_Chart_RollingPeriod.RenaissanceTag = Nothing
    Me.Text_Chart_RollingPeriod.SelectTextOnFocus = False
    Me.Text_Chart_RollingPeriod.Size = New System.Drawing.Size(43, 20)
    Me.Text_Chart_RollingPeriod.TabIndex = 10
    Me.Text_Chart_RollingPeriod.Text = "12"
    Me.Text_Chart_RollingPeriod.TextFormat = "#,##0"
    Me.Text_Chart_RollingPeriod.Value = 12
    '
    'Text_Chart_Lamda
    '
    Me.Text_Chart_Lamda.Location = New System.Drawing.Point(269, 26)
    Me.Text_Chart_Lamda.Name = "Text_Chart_Lamda"
    Me.Text_Chart_Lamda.RenaissanceTag = Nothing
    Me.Text_Chart_Lamda.SelectTextOnFocus = False
    Me.Text_Chart_Lamda.Size = New System.Drawing.Size(56, 20)
    Me.Text_Chart_Lamda.TabIndex = 8
    Me.Text_Chart_Lamda.Text = "1.00"
    Me.Text_Chart_Lamda.TextFormat = "#,##0.00"
    Me.Text_Chart_Lamda.Value = 1
    '
    'Date_Charts_DateTo
    '
    Me.Date_Charts_DateTo.CustomFormat = "dd MMM yyyy"
    Me.Date_Charts_DateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_Charts_DateTo.Location = New System.Drawing.Point(66, 50)
    Me.Date_Charts_DateTo.Name = "Date_Charts_DateTo"
    Me.Date_Charts_DateTo.Size = New System.Drawing.Size(138, 20)
    Me.Date_Charts_DateTo.TabIndex = 5
    '
    'Date_Charts_DateFrom
    '
    Me.Date_Charts_DateFrom.CustomFormat = "dd MMM yyyy"
    Me.Date_Charts_DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_Charts_DateFrom.Location = New System.Drawing.Point(66, 26)
    Me.Date_Charts_DateFrom.Name = "Date_Charts_DateFrom"
    Me.Date_Charts_DateFrom.Size = New System.Drawing.Size(138, 20)
    Me.Date_Charts_DateFrom.TabIndex = 3
    Me.Date_Charts_DateFrom.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'Label37
    '
    Me.Label37.AutoSize = True
    Me.Label37.Location = New System.Drawing.Point(338, 29)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(72, 13)
    Me.Label37.TabIndex = 9
    Me.Label37.Text = "Rolling Period"
    '
    'Label38
    '
    Me.Label38.AutoSize = True
    Me.Label38.Location = New System.Drawing.Point(225, 29)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(39, 13)
    Me.Label38.TabIndex = 7
    Me.Label38.Text = "Lamda"
    '
    'Label36
    '
    Me.Label36.AutoSize = True
    Me.Label36.Location = New System.Drawing.Point(4, 54)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(46, 13)
    Me.Label36.TabIndex = 4
    Me.Label36.Text = "Date To"
    '
    'Label35
    '
    Me.Label35.AutoSize = True
    Me.Label35.Location = New System.Drawing.Point(4, 30)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(56, 13)
    Me.Label35.TabIndex = 2
    Me.Label35.Text = "Date From"
    '
    'Combo_Charts_CompareSeriesPertrac
    '
    Me.Combo_Charts_CompareSeriesPertrac.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Charts_CompareSeriesPertrac.FormattingEnabled = True
    Me.Combo_Charts_CompareSeriesPertrac.Location = New System.Drawing.Point(228, 2)
    Me.Combo_Charts_CompareSeriesPertrac.MasternameCollection = Nothing
    Me.Combo_Charts_CompareSeriesPertrac.Name = "Combo_Charts_CompareSeriesPertrac"
    '
    '
    '
    Me.Combo_Charts_CompareSeriesPertrac.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_Charts_CompareSeriesPertrac.SelectNoMatch = False
    Me.Combo_Charts_CompareSeriesPertrac.Size = New System.Drawing.Size(522, 20)
    Me.Combo_Charts_CompareSeriesPertrac.TabIndex = 1
    Me.Combo_Charts_CompareSeriesPertrac.ThemeName = "ControlDefault"
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.Location = New System.Drawing.Point(4, 5)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(81, 13)
    Me.Label29.TabIndex = 0
    Me.Label29.Text = "Compare Series"
    '
    'TabControl_Charts
    '
    Me.TabControl_Charts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Vami)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Monthly)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Return)
    Me.TabControl_Charts.Controls.Add(Me.Tab_ReturnScatter)
    Me.TabControl_Charts.Controls.Add(Me.Tab_StdDev)
    Me.TabControl_Charts.Controls.Add(Me.Tab_VAR)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Omega)
    Me.TabControl_Charts.Controls.Add(Me.Tab_DrawDown)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Correlation)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Beta)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Alpha)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Quartile)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Ranking)
    Me.TabControl_Charts.Location = New System.Drawing.Point(3, 74)
    Me.TabControl_Charts.Margin = New System.Windows.Forms.Padding(1)
    Me.TabControl_Charts.MinimumSize = New System.Drawing.Size(300, 200)
    Me.TabControl_Charts.Name = "TabControl_Charts"
    Me.TabControl_Charts.SelectedIndex = 0
    Me.TabControl_Charts.Size = New System.Drawing.Size(747, 571)
    Me.TabControl_Charts.TabIndex = 17
    '
    'Tab_Vami
    '
    Me.Tab_Vami.Controls.Add(Me.Chart_Prices)
    Me.Tab_Vami.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Vami.Name = "Tab_Vami"
    Me.Tab_Vami.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Vami.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Vami.TabIndex = 0
    Me.Tab_Vami.Text = "VAMI"
    Me.Tab_Vami.UseVisualStyleBackColor = True
    '
    'Chart_Prices
    '
    Me.Chart_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Prices.BackColor = System.Drawing.Color.Azure
    Me.Chart_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Prices.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea28.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea28.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea28.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea28.AxisX.LabelStyle.Format = "Y"
    ChartArea28.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea28.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea28.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea28.AxisY.LabelStyle.Format = "N0"
    ChartArea28.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea28.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea28.AxisY.StartFromZero = False
    ChartArea28.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea28.BackColor = System.Drawing.Color.Transparent
    ChartArea28.BorderColor = System.Drawing.Color.DimGray
    ChartArea28.Name = "Default"
    Me.Chart_Prices.ChartAreas.Add(ChartArea28)
    Legend28.BackColor = System.Drawing.Color.Transparent
    Legend28.BorderColor = System.Drawing.Color.Transparent
    Legend28.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend28.DockToChartArea = "Default"
    Legend28.Enabled = False
    Legend28.Name = "Default"
    Me.Chart_Prices.Legends.Add(Legend28)
    Me.Chart_Prices.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Prices.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Prices.Name = "Chart_Prices"
    Me.Chart_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series45.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series45.BorderWidth = 2
    Series45.ChartType = "Line"
    Series45.CustomAttributes = "LabelStyle=Top"
    Series45.Name = "Series1"
    Series45.ShadowOffset = 1
    Series45.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series45.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series46.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series46.BorderWidth = 2
    Series46.ChartType = "Line"
    Series46.CustomAttributes = "LabelStyle=Top"
    Series46.Name = "Series2"
    Series46.ShadowOffset = 1
    Series46.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series46.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Prices.Series.Add(Series45)
    Me.Chart_Prices.Series.Add(Series46)
    Me.Chart_Prices.Size = New System.Drawing.Size(739, 545)
    Me.Chart_Prices.TabIndex = 0
    Me.Chart_Prices.Text = "Chart2"
    '
    'Tab_Monthly
    '
    Me.Tab_Monthly.Controls.Add(Me.Chart_Returns)
    Me.Tab_Monthly.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Monthly.Name = "Tab_Monthly"
    Me.Tab_Monthly.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Monthly.TabIndex = 4
    Me.Tab_Monthly.Text = "Monthly Returns"
    Me.Tab_Monthly.UseVisualStyleBackColor = True
    '
    'Chart_Returns
    '
    Me.Chart_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Returns.BackColor = System.Drawing.Color.Azure
    Me.Chart_Returns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Returns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Returns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Returns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Returns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea29.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea29.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea29.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea29.AxisX.LabelStyle.Format = "Y"
    ChartArea29.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea29.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea29.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea29.AxisY.LabelStyle.Format = "P1"
    ChartArea29.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea29.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea29.AxisY.StartFromZero = False
    ChartArea29.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea29.BackColor = System.Drawing.Color.Transparent
    ChartArea29.BorderColor = System.Drawing.Color.DimGray
    ChartArea29.Name = "Default"
    Me.Chart_Returns.ChartAreas.Add(ChartArea29)
    Legend29.BackColor = System.Drawing.Color.Transparent
    Legend29.BorderColor = System.Drawing.Color.Transparent
    Legend29.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend29.DockToChartArea = "Default"
    Legend29.Name = "Default"
    Me.Chart_Returns.Legends.Add(Legend29)
    Me.Chart_Returns.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Returns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Returns.Name = "Chart_Returns"
    Me.Chart_Returns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series47.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series47.Name = "Series1"
    Series47.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series47.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series48.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series48.Name = "Series2"
    Series48.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series48.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Returns.Series.Add(Series47)
    Me.Chart_Returns.Series.Add(Series48)
    Me.Chart_Returns.Size = New System.Drawing.Size(739, 545)
    Me.Chart_Returns.TabIndex = 1
    Me.Chart_Returns.Text = "Monthly Returns"
    '
    'Tab_Return
    '
    Me.Tab_Return.Controls.Add(Me.Chart_RollingReturn)
    Me.Tab_Return.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Return.Name = "Tab_Return"
    Me.Tab_Return.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Return.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Return.TabIndex = 1
    Me.Tab_Return.Text = "Rolling Return"
    Me.Tab_Return.UseVisualStyleBackColor = True
    '
    'Chart_RollingReturn
    '
    Me.Chart_RollingReturn.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_RollingReturn.BackColor = System.Drawing.Color.Azure
    Me.Chart_RollingReturn.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_RollingReturn.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_RollingReturn.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_RollingReturn.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_RollingReturn.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea30.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea30.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea30.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea30.AxisX.LabelStyle.Format = "Y"
    ChartArea30.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea30.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea30.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea30.AxisY.LabelStyle.Format = "P0"
    ChartArea30.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea30.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea30.AxisY.StartFromZero = False
    ChartArea30.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea30.BackColor = System.Drawing.Color.Transparent
    ChartArea30.BorderColor = System.Drawing.Color.DimGray
    ChartArea30.Name = "Default"
    Me.Chart_RollingReturn.ChartAreas.Add(ChartArea30)
    Legend30.BackColor = System.Drawing.Color.Transparent
    Legend30.BorderColor = System.Drawing.Color.Transparent
    Legend30.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend30.DockToChartArea = "Default"
    Legend30.Enabled = False
    Legend30.Name = "Default"
    Me.Chart_RollingReturn.Legends.Add(Legend30)
    Me.Chart_RollingReturn.Location = New System.Drawing.Point(0, 0)
    Me.Chart_RollingReturn.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_RollingReturn.Name = "Chart_RollingReturn"
    Me.Chart_RollingReturn.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series49.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series49.BorderWidth = 2
    Series49.ChartType = "Line"
    Series49.CustomAttributes = "LabelStyle=Top"
    Series49.Name = "Series1"
    Series49.ShadowOffset = 1
    Series49.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series49.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series50.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series50.BorderWidth = 2
    Series50.ChartType = "Line"
    Series50.CustomAttributes = "LabelStyle=Top"
    Series50.Name = "Series2"
    Series50.ShadowOffset = 1
    Series50.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series50.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_RollingReturn.Series.Add(Series49)
    Me.Chart_RollingReturn.Series.Add(Series50)
    Me.Chart_RollingReturn.Size = New System.Drawing.Size(739, 545)
    Me.Chart_RollingReturn.TabIndex = 3
    Me.Chart_RollingReturn.Text = "Chart2"
    '
    'Tab_ReturnScatter
    '
    Me.Tab_ReturnScatter.Controls.Add(Me.Chart_ReturnScatter)
    Me.Tab_ReturnScatter.Location = New System.Drawing.Point(4, 22)
    Me.Tab_ReturnScatter.Name = "Tab_ReturnScatter"
    Me.Tab_ReturnScatter.Size = New System.Drawing.Size(739, 545)
    Me.Tab_ReturnScatter.TabIndex = 10
    Me.Tab_ReturnScatter.Text = "Return Scatter"
    Me.Tab_ReturnScatter.UseVisualStyleBackColor = True
    '
    'Chart_ReturnScatter
    '
    Me.Chart_ReturnScatter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_ReturnScatter.BackColor = System.Drawing.Color.Azure
    Me.Chart_ReturnScatter.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_ReturnScatter.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_ReturnScatter.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_ReturnScatter.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_ReturnScatter.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea31.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea31.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea31.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea31.AxisX.LabelStyle.Format = "P0"
    ChartArea31.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea31.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisX.StartFromZero = False
    ChartArea31.AxisX.Title = "Comparison Series"
    ChartArea31.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    ChartArea31.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea31.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea31.AxisY.LabelStyle.Format = "P0"
    ChartArea31.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea31.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea31.AxisY.StartFromZero = False
    ChartArea31.AxisY.Title = "Instrument Series"
    ChartArea31.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea31.BackColor = System.Drawing.Color.Transparent
    ChartArea31.BorderColor = System.Drawing.Color.DimGray
    ChartArea31.Name = "Default"
    Me.Chart_ReturnScatter.ChartAreas.Add(ChartArea31)
    Legend31.BackColor = System.Drawing.Color.Transparent
    Legend31.BorderColor = System.Drawing.Color.Transparent
    Legend31.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend31.DockToChartArea = "Default"
    Legend31.Name = "Default"
    Me.Chart_ReturnScatter.Legends.Add(Legend31)
    Me.Chart_ReturnScatter.Location = New System.Drawing.Point(0, 0)
    Me.Chart_ReturnScatter.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_ReturnScatter.Name = "Chart_ReturnScatter"
    Me.Chart_ReturnScatter.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series51.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series51.ChartType = "Point"
    Series51.CustomAttributes = "LabelStyle=Top"
    Series51.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series51.Name = "Series1"
    Series51.ShadowOffset = 1
    Series51.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series51.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series52.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series52.ChartType = "Point"
    Series52.CustomAttributes = "LabelStyle=Top"
    Series52.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series52.Name = "Series2"
    Series52.ShadowOffset = 1
    Series52.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series52.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_ReturnScatter.Series.Add(Series51)
    Me.Chart_ReturnScatter.Series.Add(Series52)
    Me.Chart_ReturnScatter.Size = New System.Drawing.Size(739, 545)
    Me.Chart_ReturnScatter.TabIndex = 4
    Me.Chart_ReturnScatter.Text = "Chart2"
    '
    'Tab_StdDev
    '
    Me.Tab_StdDev.Controls.Add(Me.Chart_StdDev)
    Me.Tab_StdDev.Location = New System.Drawing.Point(4, 22)
    Me.Tab_StdDev.Name = "Tab_StdDev"
    Me.Tab_StdDev.Size = New System.Drawing.Size(739, 545)
    Me.Tab_StdDev.TabIndex = 2
    Me.Tab_StdDev.Text = "Volatility"
    Me.Tab_StdDev.UseVisualStyleBackColor = True
    '
    'Chart_StdDev
    '
    Me.Chart_StdDev.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_StdDev.BackColor = System.Drawing.Color.Azure
    Me.Chart_StdDev.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_StdDev.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_StdDev.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_StdDev.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_StdDev.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea32.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea32.AxisX.LabelStyle.Format = "Y"
    ChartArea32.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea32.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea32.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea32.AxisY.LabelStyle.Format = "P0"
    ChartArea32.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea32.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea32.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea32.BackColor = System.Drawing.Color.Transparent
    ChartArea32.BorderColor = System.Drawing.Color.DimGray
    ChartArea32.Name = "Default"
    Me.Chart_StdDev.ChartAreas.Add(ChartArea32)
    Legend32.BackColor = System.Drawing.Color.Transparent
    Legend32.BorderColor = System.Drawing.Color.Transparent
    Legend32.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend32.DockToChartArea = "Default"
    Legend32.Enabled = False
    Legend32.Name = "Default"
    Me.Chart_StdDev.Legends.Add(Legend32)
    Me.Chart_StdDev.Location = New System.Drawing.Point(0, 0)
    Me.Chart_StdDev.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_StdDev.Name = "Chart_StdDev"
    Me.Chart_StdDev.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series53.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series53.BorderWidth = 2
    Series53.ChartType = "Line"
    Series53.CustomAttributes = "LabelStyle=Top"
    Series53.Name = "Series1"
    Series53.ShadowOffset = 1
    Series53.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series53.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series54.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series54.BorderWidth = 2
    Series54.ChartType = "Line"
    Series54.CustomAttributes = "LabelStyle=Top"
    Series54.Name = "Series2"
    Series54.ShadowOffset = 1
    Series54.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series54.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_StdDev.Series.Add(Series53)
    Me.Chart_StdDev.Series.Add(Series54)
    Me.Chart_StdDev.Size = New System.Drawing.Size(739, 545)
    Me.Chart_StdDev.TabIndex = 4
    Me.Chart_StdDev.Text = "Chart2"
    '
    'Tab_VAR
    '
    Me.Tab_VAR.Controls.Add(Me.Chart_VAR)
    Me.Tab_VAR.Controls.Add(Me.Text_Chart_VARConfidence)
    Me.Tab_VAR.Controls.Add(Me.Label59)
    Me.Tab_VAR.Controls.Add(Me.Label58)
    Me.Tab_VAR.Controls.Add(Me.Text_Chart_VARPeriod)
    Me.Tab_VAR.Controls.Add(Me.Label57)
    Me.Tab_VAR.Controls.Add(Me.Label56)
    Me.Tab_VAR.Controls.Add(Me.Combo_Charts_VARSeries)
    Me.Tab_VAR.Location = New System.Drawing.Point(4, 22)
    Me.Tab_VAR.Name = "Tab_VAR"
    Me.Tab_VAR.Size = New System.Drawing.Size(739, 545)
    Me.Tab_VAR.TabIndex = 11
    Me.Tab_VAR.Text = "VAR"
    Me.Tab_VAR.UseVisualStyleBackColor = True
    '
    'Chart_VAR
    '
    Me.Chart_VAR.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_VAR.BackColor = System.Drawing.Color.Azure
    Me.Chart_VAR.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_VAR.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_VAR.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_VAR.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_VAR.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    Me.Chart_VAR.BorderSkin.PageColor = System.Drawing.Color.AliceBlue
    ChartArea33.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea33.AxisX.LabelStyle.Format = "Y"
    ChartArea33.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea33.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea33.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea33.AxisY.LabelStyle.Format = "P0"
    ChartArea33.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea33.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea33.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea33.BackColor = System.Drawing.Color.Transparent
    ChartArea33.BorderColor = System.Drawing.Color.DimGray
    ChartArea33.Name = "Default"
    Me.Chart_VAR.ChartAreas.Add(ChartArea33)
    Legend33.BackColor = System.Drawing.Color.Transparent
    Legend33.BorderColor = System.Drawing.Color.Transparent
    Legend33.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend33.DockToChartArea = "Default"
    Legend33.Enabled = False
    Legend33.Name = "Default"
    Me.Chart_VAR.Legends.Add(Legend33)
    Me.Chart_VAR.Location = New System.Drawing.Point(1, 30)
    Me.Chart_VAR.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_VAR.Name = "Chart_VAR"
    Me.Chart_VAR.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series55.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series55.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.NotSet
    Series55.BorderWidth = 2
    Series55.ChartType = "StackedArea"
    Series55.Color = System.Drawing.Color.Transparent
    Series55.CustomAttributes = "LabelStyle=Top"
    Series55.Name = "Series1"
    Series55.ShadowOffset = 1
    Series55.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series55.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series56.BackHatchStyle = Dundas.Charting.WinControl.ChartHatchStyle.Percent50
    Series56.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series56.BorderWidth = 2
    Series56.ChartType = "StackedArea"
    Series56.CustomAttributes = "LabelStyle=Top"
    Series56.Name = "Series2"
    Series56.ShadowOffset = 1
    Series56.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series56.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series57.BorderWidth = 2
    Series57.ChartType = "Line"
    Series57.Name = "Series3"
    Series57.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series57.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_VAR.Series.Add(Series55)
    Me.Chart_VAR.Series.Add(Series56)
    Me.Chart_VAR.Series.Add(Series57)
    Me.Chart_VAR.Size = New System.Drawing.Size(740, 517)
    Me.Chart_VAR.TabIndex = 8
    Me.Chart_VAR.Text = "Chart2"
    '
    'Text_Chart_VARConfidence
    '
    Me.Text_Chart_VARConfidence.Location = New System.Drawing.Point(253, 5)
    Me.Text_Chart_VARConfidence.Name = "Text_Chart_VARConfidence"
    Me.Text_Chart_VARConfidence.RenaissanceTag = Nothing
    Me.Text_Chart_VARConfidence.SelectTextOnFocus = False
    Me.Text_Chart_VARConfidence.Size = New System.Drawing.Size(36, 20)
    Me.Text_Chart_VARConfidence.TabIndex = 4
    Me.Text_Chart_VARConfidence.Text = "95%"
    Me.Text_Chart_VARConfidence.TextFormat = "#,##0%"
    Me.Text_Chart_VARConfidence.Value = 0.95
    '
    'Label59
    '
    Me.Label59.Location = New System.Drawing.Point(176, 8)
    Me.Label59.Name = "Label59"
    Me.Label59.Size = New System.Drawing.Size(73, 18)
    Me.Label59.TabIndex = 3
    Me.Label59.Text = "Confidence"
    '
    'Label58
    '
    Me.Label58.AutoSize = True
    Me.Label58.Location = New System.Drawing.Point(139, 8)
    Me.Label58.Margin = New System.Windows.Forms.Padding(0)
    Me.Label58.Name = "Label58"
    Me.Label58.Size = New System.Drawing.Size(29, 13)
    Me.Label58.TabIndex = 2
    Me.Label58.Text = "mths"
    '
    'Text_Chart_VARPeriod
    '
    Me.Text_Chart_VARPeriod.Location = New System.Drawing.Point(96, 5)
    Me.Text_Chart_VARPeriod.Name = "Text_Chart_VARPeriod"
    Me.Text_Chart_VARPeriod.RenaissanceTag = Nothing
    Me.Text_Chart_VARPeriod.SelectTextOnFocus = False
    Me.Text_Chart_VARPeriod.Size = New System.Drawing.Size(39, 20)
    Me.Text_Chart_VARPeriod.TabIndex = 1
    Me.Text_Chart_VARPeriod.Text = "3"
    Me.Text_Chart_VARPeriod.TextFormat = "#,##0"
    Me.Text_Chart_VARPeriod.Value = 3
    '
    'Label57
    '
    Me.Label57.Location = New System.Drawing.Point(7, 8)
    Me.Label57.Name = "Label57"
    Me.Label57.Size = New System.Drawing.Size(85, 18)
    Me.Label57.TabIndex = 0
    Me.Label57.Text = "VAR Duration"
    '
    'Label56
    '
    Me.Label56.Location = New System.Drawing.Point(310, 8)
    Me.Label56.Name = "Label56"
    Me.Label56.Size = New System.Drawing.Size(72, 18)
    Me.Label56.TabIndex = 5
    Me.Label56.Text = "VAR Series"
    '
    'Combo_Charts_VARSeries
    '
    Me.Combo_Charts_VARSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Charts_VARSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Charts_VARSeries.Location = New System.Drawing.Point(388, 5)
    Me.Combo_Charts_VARSeries.Name = "Combo_Charts_VARSeries"
    Me.Combo_Charts_VARSeries.Size = New System.Drawing.Size(344, 21)
    Me.Combo_Charts_VARSeries.TabIndex = 6
    '
    'Tab_Omega
    '
    Me.Tab_Omega.Controls.Add(Me.Check_OmegaRatio)
    Me.Tab_Omega.Controls.Add(Me.Label60)
    Me.Tab_Omega.Controls.Add(Me.Edit_OmegaReturn)
    Me.Tab_Omega.Controls.Add(Me.Label61)
    Me.Tab_Omega.Controls.Add(Me.Chart_Omega)
    Me.Tab_Omega.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Omega.Name = "Tab_Omega"
    Me.Tab_Omega.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Omega.TabIndex = 12
    Me.Tab_Omega.Text = "Omega"
    Me.Tab_Omega.UseVisualStyleBackColor = True
    '
    'Check_OmegaRatio
    '
    Me.Check_OmegaRatio.AutoSize = True
    Me.Check_OmegaRatio.Location = New System.Drawing.Point(194, 6)
    Me.Check_OmegaRatio.Name = "Check_OmegaRatio"
    Me.Check_OmegaRatio.Size = New System.Drawing.Size(161, 17)
    Me.Check_OmegaRatio.TabIndex = 6
    Me.Check_OmegaRatio.Text = "Show Ratio as a percentage"
    Me.Check_OmegaRatio.UseVisualStyleBackColor = True
    '
    'Label60
    '
    Me.Label60.AutoSize = True
    Me.Label60.Location = New System.Drawing.Point(135, 7)
    Me.Label60.Margin = New System.Windows.Forms.Padding(0)
    Me.Label60.Name = "Label60"
    Me.Label60.Size = New System.Drawing.Size(29, 13)
    Me.Label60.TabIndex = 5
    Me.Label60.Text = "mths"
    '
    'Edit_OmegaReturn
    '
    Me.Edit_OmegaReturn.Location = New System.Drawing.Point(92, 4)
    Me.Edit_OmegaReturn.Name = "Edit_OmegaReturn"
    Me.Edit_OmegaReturn.RenaissanceTag = Nothing
    Me.Edit_OmegaReturn.SelectTextOnFocus = False
    Me.Edit_OmegaReturn.Size = New System.Drawing.Size(39, 20)
    Me.Edit_OmegaReturn.TabIndex = 4
    Me.Edit_OmegaReturn.Text = "1"
    Me.Edit_OmegaReturn.TextFormat = "#,##0"
    Me.Edit_OmegaReturn.Value = 1
    '
    'Label61
    '
    Me.Label61.Location = New System.Drawing.Point(3, 7)
    Me.Label61.Name = "Label61"
    Me.Label61.Size = New System.Drawing.Size(85, 18)
    Me.Label61.TabIndex = 3
    Me.Label61.Text = "Rolling Return"
    '
    'Chart_Omega
    '
    Me.Chart_Omega.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Omega.BackColor = System.Drawing.Color.Azure
    Me.Chart_Omega.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Omega.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Omega.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Omega.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Omega.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea34.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea34.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea34.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea34.AxisX.LabelStyle.Format = "P0"
    ChartArea34.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea34.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea34.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea34.AxisY.LabelStyle.Format = "N1"
    ChartArea34.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea34.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea34.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea34.BackColor = System.Drawing.Color.Transparent
    ChartArea34.BorderColor = System.Drawing.Color.DimGray
    ChartArea34.Name = "Default"
    Me.Chart_Omega.ChartAreas.Add(ChartArea34)
    Legend34.BackColor = System.Drawing.Color.Transparent
    Legend34.BorderColor = System.Drawing.Color.Transparent
    Legend34.DockToChartArea = "Default"
    Legend34.Name = "Default"
    Me.Chart_Omega.Legends.Add(Legend34)
    Me.Chart_Omega.Location = New System.Drawing.Point(0, 30)
    Me.Chart_Omega.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Omega.Name = "Chart_Omega"
    Me.Chart_Omega.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series58.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series58.BorderWidth = 2
    Series58.ChartType = "Line"
    Series58.CustomAttributes = "LabelStyle=Top"
    Series58.Name = "Series1"
    Series58.ShadowOffset = 1
    Series58.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series58.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Omega.Series.Add(Series58)
    Me.Chart_Omega.Size = New System.Drawing.Size(739, 517)
    Me.Chart_Omega.TabIndex = 1
    Me.Chart_Omega.Text = "Chart2"
    '
    'Tab_DrawDown
    '
    Me.Tab_DrawDown.Controls.Add(Me.Chart_DrawDown)
    Me.Tab_DrawDown.Location = New System.Drawing.Point(4, 22)
    Me.Tab_DrawDown.Name = "Tab_DrawDown"
    Me.Tab_DrawDown.Size = New System.Drawing.Size(739, 545)
    Me.Tab_DrawDown.TabIndex = 3
    Me.Tab_DrawDown.Text = "DrawDown"
    Me.Tab_DrawDown.UseVisualStyleBackColor = True
    '
    'Chart_DrawDown
    '
    Me.Chart_DrawDown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_DrawDown.BackColor = System.Drawing.Color.Azure
    Me.Chart_DrawDown.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_DrawDown.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_DrawDown.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_DrawDown.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_DrawDown.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea35.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea35.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea35.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea35.AxisX.LabelStyle.Format = "Y"
    ChartArea35.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea35.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea35.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea35.AxisY.LabelStyle.Format = "P0"
    ChartArea35.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea35.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea35.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea35.BackColor = System.Drawing.Color.Transparent
    ChartArea35.BorderColor = System.Drawing.Color.DimGray
    ChartArea35.Name = "Default"
    Me.Chart_DrawDown.ChartAreas.Add(ChartArea35)
    Legend35.BackColor = System.Drawing.Color.Transparent
    Legend35.BorderColor = System.Drawing.Color.Transparent
    Legend35.Docking = Dundas.Charting.WinControl.LegendDocking.Bottom
    Legend35.DockToChartArea = "Default"
    Legend35.Name = "Default"
    Me.Chart_DrawDown.Legends.Add(Legend35)
    Me.Chart_DrawDown.Location = New System.Drawing.Point(0, 0)
    Me.Chart_DrawDown.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_DrawDown.Name = "Chart_DrawDown"
    Me.Chart_DrawDown.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series59.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series59.BorderWidth = 2
    Series59.ChartType = "Line"
    Series59.CustomAttributes = "LabelStyle=Top"
    Series59.Name = "Series1"
    Series59.ShadowOffset = 1
    Series59.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series59.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series60.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series60.BorderWidth = 2
    Series60.ChartType = "Line"
    Series60.CustomAttributes = "LabelStyle=Top"
    Series60.Name = "Series2"
    Series60.ShadowOffset = 1
    Series60.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series60.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_DrawDown.Series.Add(Series59)
    Me.Chart_DrawDown.Series.Add(Series60)
    Me.Chart_DrawDown.Size = New System.Drawing.Size(739, 545)
    Me.Chart_DrawDown.TabIndex = 4
    Me.Chart_DrawDown.Text = "Chart2"
    '
    'Tab_Correlation
    '
    Me.Tab_Correlation.Controls.Add(Me.Chart_Correlation)
    Me.Tab_Correlation.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Correlation.Name = "Tab_Correlation"
    Me.Tab_Correlation.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Correlation.TabIndex = 5
    Me.Tab_Correlation.Text = "Correlation"
    Me.Tab_Correlation.UseVisualStyleBackColor = True
    '
    'Chart_Correlation
    '
    Me.Chart_Correlation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Correlation.BackColor = System.Drawing.Color.Azure
    Me.Chart_Correlation.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Correlation.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Correlation.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Correlation.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Correlation.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea36.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea36.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea36.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea36.AxisX.LabelStyle.Format = "Y"
    ChartArea36.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea36.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea36.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea36.AxisY.LabelStyle.Format = "N2"
    ChartArea36.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea36.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea36.AxisY.StartFromZero = False
    ChartArea36.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea36.BackColor = System.Drawing.Color.Transparent
    ChartArea36.BorderColor = System.Drawing.Color.DimGray
    ChartArea36.Name = "Default"
    Me.Chart_Correlation.ChartAreas.Add(ChartArea36)
    Legend36.BackColor = System.Drawing.Color.Transparent
    Legend36.BorderColor = System.Drawing.Color.Transparent
    Legend36.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend36.DockToChartArea = "Default"
    Legend36.Enabled = False
    Legend36.Name = "Default"
    Me.Chart_Correlation.Legends.Add(Legend36)
    Me.Chart_Correlation.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Correlation.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Correlation.Name = "Chart_Correlation"
    Me.Chart_Correlation.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series61.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series61.BorderWidth = 2
    Series61.ChartType = "Line"
    Series61.CustomAttributes = "LabelStyle=Top"
    Series61.Name = "Series1"
    Series61.ShadowOffset = 1
    Series61.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series61.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series62.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series62.BorderWidth = 2
    Series62.ChartType = "Line"
    Series62.CustomAttributes = "LabelStyle=Top"
    Series62.Name = "Series2"
    Series62.ShadowOffset = 1
    Series62.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series62.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Correlation.Series.Add(Series61)
    Me.Chart_Correlation.Series.Add(Series62)
    Me.Chart_Correlation.Size = New System.Drawing.Size(739, 545)
    Me.Chart_Correlation.TabIndex = 5
    Me.Chart_Correlation.Text = "Chart2"
    '
    'Tab_Beta
    '
    Me.Tab_Beta.Controls.Add(Me.Chart_Beta)
    Me.Tab_Beta.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Beta.Name = "Tab_Beta"
    Me.Tab_Beta.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Beta.TabIndex = 7
    Me.Tab_Beta.Text = "Beta"
    Me.Tab_Beta.UseVisualStyleBackColor = True
    '
    'Chart_Beta
    '
    Me.Chart_Beta.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Beta.BackColor = System.Drawing.Color.Azure
    Me.Chart_Beta.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Beta.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Beta.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Beta.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Beta.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea37.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea37.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea37.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea37.AxisX.LabelStyle.Format = "Y"
    ChartArea37.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea37.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea37.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea37.AxisY.LabelStyle.Format = "N2"
    ChartArea37.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea37.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea37.AxisY.StartFromZero = False
    ChartArea37.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea37.BackColor = System.Drawing.Color.Transparent
    ChartArea37.BorderColor = System.Drawing.Color.DimGray
    ChartArea37.Name = "Default"
    Me.Chart_Beta.ChartAreas.Add(ChartArea37)
    Legend37.BackColor = System.Drawing.Color.Transparent
    Legend37.BorderColor = System.Drawing.Color.Transparent
    Legend37.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend37.DockToChartArea = "Default"
    Legend37.Enabled = False
    Legend37.Name = "Default"
    Me.Chart_Beta.Legends.Add(Legend37)
    Me.Chart_Beta.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Beta.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Beta.Name = "Chart_Beta"
    Me.Chart_Beta.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series63.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series63.BorderWidth = 2
    Series63.ChartType = "Line"
    Series63.CustomAttributes = "LabelStyle=Top"
    Series63.Name = "Series1"
    Series63.ShadowOffset = 1
    Series63.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series63.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series64.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series64.BorderWidth = 2
    Series64.ChartType = "Line"
    Series64.CustomAttributes = "LabelStyle=Top"
    Series64.Name = "Series2"
    Series64.ShadowOffset = 1
    Series64.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series64.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Beta.Series.Add(Series63)
    Me.Chart_Beta.Series.Add(Series64)
    Me.Chart_Beta.Size = New System.Drawing.Size(739, 545)
    Me.Chart_Beta.TabIndex = 3
    Me.Chart_Beta.Text = "Chart2"
    '
    'Tab_Alpha
    '
    Me.Tab_Alpha.Controls.Add(Me.Chart_Alpha)
    Me.Tab_Alpha.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Alpha.Name = "Tab_Alpha"
    Me.Tab_Alpha.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Alpha.TabIndex = 8
    Me.Tab_Alpha.Text = "Alpha"
    Me.Tab_Alpha.UseVisualStyleBackColor = True
    '
    'Chart_Alpha
    '
    Me.Chart_Alpha.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Alpha.BackColor = System.Drawing.Color.Azure
    Me.Chart_Alpha.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Alpha.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Alpha.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Alpha.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Alpha.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea38.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea38.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea38.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea38.AxisX.LabelStyle.Format = "Y"
    ChartArea38.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea38.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea38.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea38.AxisY.LabelStyle.Format = "P1"
    ChartArea38.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea38.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea38.AxisY.StartFromZero = False
    ChartArea38.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea38.BackColor = System.Drawing.Color.Transparent
    ChartArea38.BorderColor = System.Drawing.Color.DimGray
    ChartArea38.Name = "Default"
    Me.Chart_Alpha.ChartAreas.Add(ChartArea38)
    Legend38.BackColor = System.Drawing.Color.Transparent
    Legend38.BorderColor = System.Drawing.Color.Transparent
    Legend38.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend38.DockToChartArea = "Default"
    Legend38.Enabled = False
    Legend38.Name = "Default"
    Me.Chart_Alpha.Legends.Add(Legend38)
    Me.Chart_Alpha.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Alpha.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Alpha.Name = "Chart_Alpha"
    Me.Chart_Alpha.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series65.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series65.BorderWidth = 2
    Series65.ChartType = "Line"
    Series65.CustomAttributes = "LabelStyle=Top"
    Series65.Name = "Series1"
    Series65.ShadowOffset = 1
    Series65.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series65.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series66.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series66.BorderWidth = 2
    Series66.ChartType = "Line"
    Series66.CustomAttributes = "LabelStyle=Top"
    Series66.Name = "Series2"
    Series66.ShadowOffset = 1
    Series66.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series66.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Alpha.Series.Add(Series65)
    Me.Chart_Alpha.Series.Add(Series66)
    Me.Chart_Alpha.Size = New System.Drawing.Size(739, 545)
    Me.Chart_Alpha.TabIndex = 4
    Me.Chart_Alpha.Text = "Chart2"
    '
    'Tab_Quartile
    '
    Me.Tab_Quartile.Controls.Add(Me.Numeric_QuantileOutlier)
    Me.Tab_Quartile.Controls.Add(Me.Label62)
    Me.Tab_Quartile.Controls.Add(Me.Label51)
    Me.Tab_Quartile.Controls.Add(Me.Combo_Quartile_ChartData)
    Me.Tab_Quartile.Controls.Add(Me.Label49)
    Me.Tab_Quartile.Controls.Add(Me.Combo_Quartile_HighlightSeries)
    Me.Tab_Quartile.Controls.Add(Me.Chart_Quartile)
    Me.Tab_Quartile.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Quartile.Name = "Tab_Quartile"
    Me.Tab_Quartile.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Quartile.TabIndex = 6
    Me.Tab_Quartile.Text = "Quartile"
    Me.Tab_Quartile.UseVisualStyleBackColor = True
    '
    'Numeric_QuantileOutlier
    '
    Me.Numeric_QuantileOutlier.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Numeric_QuantileOutlier.Location = New System.Drawing.Point(688, 4)
    Me.Numeric_QuantileOutlier.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
    Me.Numeric_QuantileOutlier.Name = "Numeric_QuantileOutlier"
    Me.Numeric_QuantileOutlier.Size = New System.Drawing.Size(48, 20)
    Me.Numeric_QuantileOutlier.TabIndex = 12
    '
    'Label62
    '
    Me.Label62.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label62.Location = New System.Drawing.Point(634, 7)
    Me.Label62.Name = "Label62"
    Me.Label62.Size = New System.Drawing.Size(52, 16)
    Me.Label62.TabIndex = 11
    Me.Label62.Text = "Outlier #"
    '
    'Label51
    '
    Me.Label51.Location = New System.Drawing.Point(3, 7)
    Me.Label51.Name = "Label51"
    Me.Label51.Size = New System.Drawing.Size(66, 16)
    Me.Label51.TabIndex = 10
    Me.Label51.Text = "Chart Data"
    '
    'Combo_Quartile_ChartData
    '
    Me.Combo_Quartile_ChartData.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Quartile_ChartData.Location = New System.Drawing.Point(71, 3)
    Me.Combo_Quartile_ChartData.Name = "Combo_Quartile_ChartData"
    Me.Combo_Quartile_ChartData.Size = New System.Drawing.Size(126, 21)
    Me.Combo_Quartile_ChartData.TabIndex = 9
    '
    'Label49
    '
    Me.Label49.Location = New System.Drawing.Point(203, 7)
    Me.Label49.Name = "Label49"
    Me.Label49.Size = New System.Drawing.Size(50, 16)
    Me.Label49.TabIndex = 8
    Me.Label49.Text = "Highlight"
    '
    'Combo_Quartile_HighlightSeries
    '
    Me.Combo_Quartile_HighlightSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Quartile_HighlightSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Quartile_HighlightSeries.Location = New System.Drawing.Point(255, 3)
    Me.Combo_Quartile_HighlightSeries.Name = "Combo_Quartile_HighlightSeries"
    Me.Combo_Quartile_HighlightSeries.Size = New System.Drawing.Size(376, 21)
    Me.Combo_Quartile_HighlightSeries.TabIndex = 7
    '
    'Chart_Quartile
    '
    Me.Chart_Quartile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Quartile.BackColor = System.Drawing.Color.Azure
    Me.Chart_Quartile.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Quartile.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Quartile.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Quartile.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Quartile.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea39.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea39.AxisX.LabelStyle.Format = "Y"
    ChartArea39.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea39.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisY.LabelStyle.Format = "P0"
    ChartArea39.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea39.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea39.AxisY.StartFromZero = False
    ChartArea39.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea39.BackColor = System.Drawing.Color.Transparent
    ChartArea39.BorderColor = System.Drawing.Color.Empty
    ChartArea39.Name = "Default"
    Me.Chart_Quartile.ChartAreas.Add(ChartArea39)
    Legend39.BackColor = System.Drawing.Color.Transparent
    Legend39.BorderColor = System.Drawing.Color.Transparent
    Legend39.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend39.DockToChartArea = "Default"
    Legend39.Enabled = False
    Legend39.Name = "Default"
    Me.Chart_Quartile.Legends.Add(Legend39)
    Me.Chart_Quartile.Location = New System.Drawing.Point(0, 25)
    Me.Chart_Quartile.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Quartile.Name = "Chart_Quartile"
    Me.Chart_Quartile.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series67.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series67.BorderWidth = 2
    Series67.ChartType = "CandleStick"
    Series67.Color = System.Drawing.Color.Red
    Series67.CustomAttributes = "LabelStyle=Top"
    Series67.Name = "Series1"
    Series67.ShadowOffset = 1
    Series67.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series67.YValuesPerPoint = 4
    Series67.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series68.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series68.BorderWidth = 2
    Series68.ChartArea = ""
    Series68.ChartType = "Point"
    Series68.Color = System.Drawing.Color.Green
    Series68.CustomAttributes = "LabelStyle=Top"
    Series68.MarkerColor = System.Drawing.Color.Green
    Series68.MarkerStyle = Dundas.Charting.WinControl.MarkerStyle.Cross
    Series68.Name = "Series2"
    Series68.ShadowOffset = 1
    Series68.ShowInLegend = False
    Series68.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series68.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Quartile.Series.Add(Series67)
    Me.Chart_Quartile.Series.Add(Series68)
    Me.Chart_Quartile.Size = New System.Drawing.Size(739, 520)
    Me.Chart_Quartile.TabIndex = 6
    Me.Chart_Quartile.Text = "Chart2"
    '
    'Tab_Ranking
    '
    Me.Tab_Ranking.Controls.Add(Me.Label52)
    Me.Tab_Ranking.Controls.Add(Me.Combo_Ranking_ChartData)
    Me.Tab_Ranking.Controls.Add(Me.Label50)
    Me.Tab_Ranking.Controls.Add(Me.Combo_Ranking_HighlightSeries)
    Me.Tab_Ranking.Controls.Add(Me.Chart_Ranking)
    Me.Tab_Ranking.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Ranking.Name = "Tab_Ranking"
    Me.Tab_Ranking.Size = New System.Drawing.Size(739, 545)
    Me.Tab_Ranking.TabIndex = 9
    Me.Tab_Ranking.Text = "Ranking"
    Me.Tab_Ranking.UseVisualStyleBackColor = True
    '
    'Label52
    '
    Me.Label52.Location = New System.Drawing.Point(3, 6)
    Me.Label52.Name = "Label52"
    Me.Label52.Size = New System.Drawing.Size(66, 18)
    Me.Label52.TabIndex = 12
    Me.Label52.Text = "Chart Data"
    '
    'Combo_Ranking_ChartData
    '
    Me.Combo_Ranking_ChartData.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Ranking_ChartData.Location = New System.Drawing.Point(75, 3)
    Me.Combo_Ranking_ChartData.Name = "Combo_Ranking_ChartData"
    Me.Combo_Ranking_ChartData.Size = New System.Drawing.Size(132, 21)
    Me.Combo_Ranking_ChartData.TabIndex = 11
    '
    'Label50
    '
    Me.Label50.Location = New System.Drawing.Point(221, 6)
    Me.Label50.Name = "Label50"
    Me.Label50.Size = New System.Drawing.Size(85, 18)
    Me.Label50.TabIndex = 10
    Me.Label50.Text = "Highlight Series"
    '
    'Combo_Ranking_HighlightSeries
    '
    Me.Combo_Ranking_HighlightSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Ranking_HighlightSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Ranking_HighlightSeries.Location = New System.Drawing.Point(310, 3)
    Me.Combo_Ranking_HighlightSeries.Name = "Combo_Ranking_HighlightSeries"
    Me.Combo_Ranking_HighlightSeries.Size = New System.Drawing.Size(426, 21)
    Me.Combo_Ranking_HighlightSeries.TabIndex = 9
    '
    'Chart_Ranking
    '
    Me.Chart_Ranking.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Ranking.BackColor = System.Drawing.Color.Azure
    Me.Chart_Ranking.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Ranking.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Ranking.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Ranking.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Ranking.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea40.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea40.AxisX.LabelStyle.Format = "Y"
    ChartArea40.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea40.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisY.LabelStyle.Format = "P0"
    ChartArea40.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea40.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea40.AxisY.Maximum = 100
    ChartArea40.AxisY.Minimum = 0
    ChartArea40.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea40.BackColor = System.Drawing.Color.Transparent
    ChartArea40.BorderColor = System.Drawing.Color.DimGray
    ChartArea40.Name = "Default"
    Me.Chart_Ranking.ChartAreas.Add(ChartArea40)
    Legend40.BackColor = System.Drawing.Color.Transparent
    Legend40.BorderColor = System.Drawing.Color.Transparent
    Legend40.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend40.DockToChartArea = "Default"
    Legend40.Enabled = False
    Legend40.Name = "Default"
    Me.Chart_Ranking.Legends.Add(Legend40)
    Me.Chart_Ranking.Location = New System.Drawing.Point(0, 25)
    Me.Chart_Ranking.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Ranking.Name = "Chart_Ranking"
    Me.Chart_Ranking.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series69.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series69.BorderWidth = 2
    Series69.ChartType = "Line"
    Series69.CustomAttributes = "LabelStyle=Top"
    Series69.Name = "Series1"
    Series69.ShadowOffset = 1
    Series69.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series69.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series70.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series70.BorderWidth = 2
    Series70.ChartType = "Line"
    Series70.CustomAttributes = "LabelStyle=Top"
    Series70.Name = "Series2"
    Series70.ShadowOffset = 1
    Series70.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series70.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Ranking.Series.Add(Series69)
    Me.Chart_Ranking.Series.Add(Series70)
    Me.Chart_Ranking.Size = New System.Drawing.Size(739, 520)
    Me.Chart_Ranking.TabIndex = 7
    Me.Chart_Ranking.Text = "Chart2"
    '
    'Tab_Statistics
    '
    Me.Tab_Statistics.AutoScroll = True
    Me.Tab_Statistics.Controls.Add(Me.Grid_Stats_Drawdowns)
    Me.Tab_Statistics.Controls.Add(Me.Grid_SimpleStatistics)
    Me.Tab_Statistics.Controls.Add(Me.Label55)
    Me.Tab_Statistics.Controls.Add(Me.Text_Statistics_RiskFree)
    Me.Tab_Statistics.Controls.Add(Me.Label54)
    Me.Tab_Statistics.Controls.Add(Me.Label53)
    Me.Tab_Statistics.Controls.Add(Me.Combo_Statistics_HighlightSeries)
    Me.Tab_Statistics.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Statistics.Name = "Tab_Statistics"
    Me.Tab_Statistics.Size = New System.Drawing.Size(755, 646)
    Me.Tab_Statistics.TabIndex = 6
    Me.Tab_Statistics.Text = "Statistics"
    Me.Tab_Statistics.UseVisualStyleBackColor = True
    '
    'Grid_Stats_Drawdowns
    '
    Me.Grid_Stats_Drawdowns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Stats_Drawdowns.ColumnInfo = resources.GetString("Grid_Stats_Drawdowns.ColumnInfo")
    Me.Grid_Stats_Drawdowns.Location = New System.Drawing.Point(12, 209)
    Me.Grid_Stats_Drawdowns.MinimumSize = New System.Drawing.Size(400, 200)
    Me.Grid_Stats_Drawdowns.Name = "Grid_Stats_Drawdowns"
    Me.Grid_Stats_Drawdowns.Rows.Count = 1
    Me.Grid_Stats_Drawdowns.Rows.DefaultSize = 17
    Me.Grid_Stats_Drawdowns.Size = New System.Drawing.Size(738, 431)
    Me.Grid_Stats_Drawdowns.StyleInfo = resources.GetString("Grid_Stats_Drawdowns.StyleInfo")
    Me.Grid_Stats_Drawdowns.TabIndex = 19
    '
    'Grid_SimpleStatistics
    '
    Me.Grid_SimpleStatistics.ColumnInfo = resources.GetString("Grid_SimpleStatistics.ColumnInfo")
    Me.Grid_SimpleStatistics.Location = New System.Drawing.Point(12, 79)
    Me.Grid_SimpleStatistics.Name = "Grid_SimpleStatistics"
    Me.Grid_SimpleStatistics.Rows.Count = 7
    Me.Grid_SimpleStatistics.Rows.DefaultSize = 17
    Me.Grid_SimpleStatistics.Size = New System.Drawing.Size(502, 124)
    Me.Grid_SimpleStatistics.StyleInfo = resources.GetString("Grid_SimpleStatistics.StyleInfo")
    Me.Grid_SimpleStatistics.TabIndex = 18
    '
    'Label55
    '
    Me.Label55.AutoSize = True
    Me.Label55.Location = New System.Drawing.Point(167, 33)
    Me.Label55.Name = "Label55"
    Me.Label55.Size = New System.Drawing.Size(93, 13)
    Me.Label55.TabIndex = 17
    Me.Label55.Text = "(For Sharpe Ratio)"
    '
    'Text_Statistics_RiskFree
    '
    Me.Text_Statistics_RiskFree.Location = New System.Drawing.Point(106, 30)
    Me.Text_Statistics_RiskFree.Name = "Text_Statistics_RiskFree"
    Me.Text_Statistics_RiskFree.RenaissanceTag = Nothing
    Me.Text_Statistics_RiskFree.SelectTextOnFocus = False
    Me.Text_Statistics_RiskFree.Size = New System.Drawing.Size(55, 20)
    Me.Text_Statistics_RiskFree.TabIndex = 16
    Me.Text_Statistics_RiskFree.Text = "3.00%"
    Me.Text_Statistics_RiskFree.TextFormat = "#,##0.00##%"
    Me.Text_Statistics_RiskFree.Value = 0.03
    '
    'Label54
    '
    Me.Label54.AutoSize = True
    Me.Label54.Location = New System.Drawing.Point(9, 33)
    Me.Label54.Name = "Label54"
    Me.Label54.Size = New System.Drawing.Size(78, 13)
    Me.Label54.TabIndex = 15
    Me.Label54.Text = "Risk Free Rate"
    '
    'Label53
    '
    Me.Label53.Location = New System.Drawing.Point(9, 6)
    Me.Label53.Name = "Label53"
    Me.Label53.Size = New System.Drawing.Size(91, 18)
    Me.Label53.TabIndex = 14
    Me.Label53.Text = "Statistics Series"
    '
    'Combo_Statistics_HighlightSeries
    '
    Me.Combo_Statistics_HighlightSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Statistics_HighlightSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Statistics_HighlightSeries.Location = New System.Drawing.Point(106, 3)
    Me.Combo_Statistics_HighlightSeries.Name = "Combo_Statistics_HighlightSeries"
    Me.Combo_Statistics_HighlightSeries.Size = New System.Drawing.Size(644, 21)
    Me.Combo_Statistics_HighlightSeries.TabIndex = 13
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FundBrowser_StatusLabel})
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 708)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1118, 22)
    Me.StatusStrip1.TabIndex = 13
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'FundBrowser_StatusLabel
    '
    Me.FundBrowser_StatusLabel.Name = "FundBrowser_StatusLabel"
    Me.FundBrowser_StatusLabel.Size = New System.Drawing.Size(10, 17)
    Me.FundBrowser_StatusLabel.Text = " "
    '
    'frmFundBrowser
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1118, 730)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.Split_Form)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(458, 339)
    Me.Name = "frmFundBrowser"
    Me.Text = "Fund Browser"
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    Me.Split_Form.Panel1.ResumeLayout(False)
    Me.Split_Form.Panel1.PerformLayout()
    Me.Split_Form.Panel2.ResumeLayout(False)
    Me.Split_Form.ResumeLayout(False)
    Me.Tab_InstrumentDetails.ResumeLayout(False)
    Me.Tab_Descriptions.ResumeLayout(False)
    Me.Tab_Descriptions.PerformLayout()
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.Tab_Contacts.ResumeLayout(False)
    Me.Tab_Contacts.PerformLayout()
    Me.Tab_Performance.ResumeLayout(False)
    Me.TabControl_PerformanceTables.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    CType(Me.Grid_Performance_0, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comparisons.ResumeLayout(False)
    Me.Tab_Comparisons.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.TabControl_Comparisons.ResumeLayout(False)
    Me.Tab_Comp_Statistics.ResumeLayout(False)
    CType(Me.Grid_Comp_Statistics, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Correlation.ResumeLayout(False)
    CType(Me.Chart_Comparison_Correlation, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Correlation_UpDown.ResumeLayout(False)
    CType(Me.Chart_Comparison_CorrelationUpDown, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Beta.ResumeLayout(False)
    CType(Me.Chart_Comparison_Beta, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Beta_UpDown.ResumeLayout(False)
    CType(Me.Chart_Comparison_BetaUpDown, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Alpha.ResumeLayout(False)
    CType(Me.Chart_Comparison_Alpha, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_APR.ResumeLayout(False)
    CType(Me.Chart_Comparison_APR, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Comp_Volatility.ResumeLayout(False)
    CType(Me.Chart_Comparison_Volatility, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Combo_Compare_Series, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Charts.ResumeLayout(False)
    Me.Tab_Charts.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    CType(Me.Combo_Charts_CompareSeriesPertrac, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TabControl_Charts.ResumeLayout(False)
    Me.Tab_Vami.ResumeLayout(False)
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Monthly.ResumeLayout(False)
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Return.ResumeLayout(False)
    CType(Me.Chart_RollingReturn, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_ReturnScatter.ResumeLayout(False)
    CType(Me.Chart_ReturnScatter, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_StdDev.ResumeLayout(False)
    CType(Me.Chart_StdDev, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_VAR.ResumeLayout(False)
    Me.Tab_VAR.PerformLayout()
    CType(Me.Chart_VAR, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Omega.ResumeLayout(False)
    Me.Tab_Omega.PerformLayout()
    CType(Me.Chart_Omega, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_DrawDown.ResumeLayout(False)
    CType(Me.Chart_DrawDown, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Correlation.ResumeLayout(False)
    CType(Me.Chart_Correlation, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Beta.ResumeLayout(False)
    CType(Me.Chart_Beta, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Alpha.ResumeLayout(False)
    CType(Me.Chart_Alpha, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Quartile.ResumeLayout(False)
    CType(Me.Numeric_QuantileOutlier, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_Quartile, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Ranking.ResumeLayout(False)
    CType(Me.Chart_Ranking, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Statistics.ResumeLayout(False)
    Me.Tab_Statistics.PerformLayout()
    CType(Me.Grid_Stats_Drawdowns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Grid_SimpleStatistics, System.ComponentModel.ISupportInitialize).EndInit()
    Me.StatusStrip1.ResumeLayout(False)
    Me.StatusStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain
  Private _SelectListToUpdate As Boolean = False
  Private _SelectSelectionToUpdate As Boolean = False
  Private _ChartsToUpdate As Boolean = False
  Private _ComparisonToUpdate As Boolean = False
  Private _ChartsToUpdateTime As Date = Now()
  Private _ComparisonToUpdateTime As Date = Now()
  Private _StartWithCustomSelection As Boolean

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  Private SelectListToUpdate_Object As RenaissanceGlobals.RenaissanceTimerUpdateClass
  Private Date_ValueDate_ValueDateChanged As Boolean

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private _FormOpenFailed As Boolean
  Private InPaint As Boolean
  Private _InUse As Boolean
  Private InFormUpdate_Tick As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  ' Data Structures
  Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

  ' "Charts" Management Arrays

  Private PriceChartArrayList As New ArrayList
  Private ReturnsChartArrayList As New ArrayList
  Private RollingReturnChartArrayList As New ArrayList
  Private ReturnScatterChartArrayList As New ArrayList
  Private StdDevChartArrayList As New ArrayList
  Private VARChartArrayList As New ArrayList
  Private OmegaChartArrayList As New ArrayList
  Private DrawdownChartArrayList As New ArrayList
  Private CorrelationChartArrayList As New ArrayList
  Private BetaChartArrayList As New ArrayList
  Private AlphaChartArrayList As New ArrayList
  Private QuartileChartArrayList As New ArrayList
  Private RankingChartArrayList As New ArrayList

  ' "Comparisons" Data Structures

  Private ComparisonStatsGridList As New ArrayList
  Private CorrelationBarArrayList As New ArrayList
  Private CorrelationUpDownBarArrayList As New ArrayList
  Private BetaBarArrayList As New ArrayList
  Private BetaBarUpDownArrayList As New ArrayList
  Private AlphaBarArrayList As New ArrayList
  Private APRBarArrayList As New ArrayList
  Private VolatilityBarArrayList As New ArrayList

  '

  Private CustomGroupID_SelectedList As Integer
  Private CustomGroupID_WholeList As Integer

  Private MenuSelectedStatsDataLimit As DealingPeriod

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

  Public Property Selection_Pertrac() As Boolean
    Get
      Return Radio_Pertrac.Checked
    End Get
    Set(ByVal value As Boolean)
      If (value) AndAlso (Radio_Pertrac.Checked = False) Then
        Radio_Pertrac.Checked = True
        Application.DoEvents()
      End If
    End Set
  End Property

  Public Sub SetSelectedInstrument(ByVal pPertracID As Integer, ByVal pInstrumentName As String)
    Try
      Me.Combo_SelectFrom.Text = pInstrumentName.Substring(0, Math.Min(2, pInstrumentName.Length))
      Application.DoEvents()

      Me.List_SelectItems.SelectedValue = pPertracID
    Catch ex As Exception
    End Try
  End Sub

  Private Property SelectListToUpdate() As Boolean
    Get
      Return _SelectListToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        If (SelectListToUpdate_Object IsNot Nothing) Then
          SelectListToUpdate_Object.FormToUpdate = True
        End If
      End If

      _SelectListToUpdate = value
    End Set
  End Property

  Private Property SelectSelectionToUpdate() As Boolean
    Get
      Return _SelectSelectionToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        If (SelectListToUpdate_Object IsNot Nothing) Then
          SelectListToUpdate_Object.FormToUpdate = True
        End If
      End If

      _SelectSelectionToUpdate = value
    End Set
  End Property

  Private Property ChartsToUpdate() As Boolean
    Get
      Return _ChartsToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        _ChartsToUpdateTime = Now()

        If (SelectListToUpdate_Object IsNot Nothing) Then
          SelectListToUpdate_Object.FormToUpdate = True
        End If
      End If

      _ChartsToUpdate = value
    End Set
  End Property

  Private Property ComparisonToUpdate() As Boolean
    Get
      Return _ComparisonToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        _ComparisonToUpdateTime = Now()

        If (SelectListToUpdate_Object IsNot Nothing) Then
          SelectListToUpdate_Object.FormToUpdate = True
        End If
      End If

      _ComparisonToUpdate = value
    End Set
  End Property

  Private ReadOnly Property ChartsToUpdateTime() As Date
    Get
      Return _ChartsToUpdateTime
    End Get
  End Property

  Private ReadOnly Property ComparisonToUpdateTime() As Date
    Get
      Return _ComparisonToUpdateTime
    End Get
  End Property

  Public Property StartWithCustomSelection() As Boolean
    Get
      Return _StartWithCustomSelection
    End Get
    Set(ByVal value As Boolean)
      _StartWithCustomSelection = value
    End Set
  End Property

#End Region

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    Try
      Me.Cursor = Cursors.WaitCursor

      _MainForm = pMainForm
      AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate
      AddHandler _MainForm.Genoa_FontSizeChanged, AddressOf Me.FontSizeChanged

      _FormOpenFailed = False
      _InUse = True
      CustomGroupID_SelectedList = pMainForm.UniqueGenoaNumber
      CustomGroupID_WholeList = pMainForm.UniqueGenoaNumber
      MenuSelectedStatsDataLimit = DealingPeriod.Daily

      ' ******************************************************
      ' Form Specific Settings :
      ' ******************************************************

      ' Form Permissioning :-

      THIS_FORM_PermissionArea = Me.Name
      THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

      ' 'This' form ID

      THIS_FORM_FormID = GenoaFormID.frmFundBrowser

      ' Format Event Handlers for form controls

      AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

      AddHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      'AddHandler Combo_Charts_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Charts_CompareSeriesPertrac.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      'AddHandler Combo_Charts_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      'AddHandler Combo_Charts_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_Charts_CompareSeriesGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Charts_CompareSeriesGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Charts_CompareSeriesGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Charts_CompareSeriesGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Comparisons_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Comparisons_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Comparisons_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Comparisons_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      'AddHandler Combo_Compare_Series.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Compare_Series.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      'AddHandler Combo_Compare_Series.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      'AddHandler Combo_Compare_Series.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Compare_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Compare_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Compare_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Compare_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Quartile_HighlightSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Quartile_HighlightSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Quartile_HighlightSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Quartile_HighlightSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Ranking_HighlightSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Ranking_HighlightSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Ranking_HighlightSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Ranking_HighlightSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Quartile_ChartData.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Quartile_ChartData.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Quartile_ChartData.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Quartile_ChartData.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_Ranking_ChartData.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Ranking_ChartData.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Ranking_ChartData.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Ranking_ChartData.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Grid_Performance_0.KeyDown, AddressOf Grid_Performance_KeyDown
      Grid_Performance_0.ContextMenuStrip = New ContextMenuStrip
      Grid_Performance_0.ContextMenuStrip.Tag = Grid_Performance_0
      AddHandler Grid_Performance_0.ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
      Grid_Performance_0.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

      ' Form Control Changed events

      ' Set up the ToolTip
      MainForm.SetFormToolTip(Me, FormTooltip)

      _StartWithCustomSelection = False

      ' ******************************************************
      ' End Form Specific.
      ' ******************************************************

      Try
        InPaint = True

        Radio_ListIsReference.Checked = True

        Call Set_QuartileChartDataCombo()
        Call Set_RankingChartDataCombo()

        If Combo_Quartile_ChartData.Items.Count > 0 Then
          Combo_Quartile_ChartData.SelectedIndex = 0
        End If
        If Combo_Ranking_ChartData.Items.Count > 0 Then
          Combo_Ranking_ChartData.SelectedIndex = 0
        End If

      Catch ex As Exception
      Finally
        InPaint = False
      End Try

      Grid_Performance_0.Styles.Add("Positive").ForeColor = Color.Blue
      Grid_Performance_0.Styles.Add("Negative").ForeColor = Color.Red
      Grid_Performance_0.Styles.Add("TopRow", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
      Grid_Performance_0.Styles.Add("FirstCell", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
      Grid_Performance_0.Rows(0).Style = Grid_Performance_0.Styles("TopRow")
      Grid_Performance_0.SetCellStyle(0, 0, "FirstCell")

      Grid_Comp_Statistics.Styles.Add("Positive").ForeColor = Color.Blue
      Grid_Comp_Statistics.Styles.Add("Negative").ForeColor = Color.Red
      Grid_Comp_Statistics.Styles.Add("TopRow", Grid_Comp_Statistics.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
      Grid_Comp_Statistics.Styles.Add("FirstCell", Grid_Comp_Statistics.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
      Grid_Comp_Statistics.Rows(0).Style = Grid_Comp_Statistics.Styles("TopRow")
      Grid_Comp_Statistics.SetCellStyle(0, 0, "FirstCell")

      Dim thisStyle As C1.Win.C1FlexGrid.CellStyle

      thisStyle = Grid_SimpleStatistics.Styles.Add("PositivePercent")
      thisStyle.ForeColor = Color.Blue
      thisStyle.Format = "#,##0.00%"

      thisStyle = Grid_SimpleStatistics.Styles.Add("NegativePercent")
      thisStyle.ForeColor = Color.Red
      thisStyle.Format = "#,##0.00%"

      thisStyle = Grid_SimpleStatistics.Styles.Add("SharpeRatio")
      thisStyle.ForeColor = Color.Blue
      thisStyle.Format = "#,##0.00"

      Grid_SimpleStatistics.Item(1, 0) = "Return"
      Grid_SimpleStatistics.Item(2, 0) = "Annualised Return"
      Grid_SimpleStatistics.Item(3, 0) = "Volatility"
      Grid_SimpleStatistics.Item(4, 0) = "Sample Volatility"
      Grid_SimpleStatistics.Item(5, 0) = "Sharpe Ratio"
      Grid_SimpleStatistics.Item(6, 0) = "% Positive"

      ' Initialise Chart Arraylists

      PriceChartArrayList.Add(Me.Chart_Prices)
      ReturnsChartArrayList.Add(Me.Chart_Returns)
      RollingReturnChartArrayList.Add(Me.Chart_RollingReturn)
      ReturnScatterChartArrayList.Add(Me.Chart_ReturnScatter)
      StdDevChartArrayList.Add(Me.Chart_StdDev)
      VARChartArrayList.Add(Me.Chart_VAR)
      OmegaChartArrayList.Add(Me.Chart_Omega)
      DrawdownChartArrayList.Add(Me.Chart_DrawDown)
      CorrelationChartArrayList.Add(Me.Chart_Correlation)
      AlphaChartArrayList.Add(Me.Chart_Alpha)
      BetaChartArrayList.Add(Me.Chart_Beta)
      QuartileChartArrayList.Add(Me.Chart_Quartile)
      RankingChartArrayList.Add(Me.Chart_Ranking)

      ' Initialise Comparison Charts / Grid

      ComparisonStatsGridList.Add(Grid_Comp_Statistics)
      CorrelationBarArrayList.Add(Chart_Comparison_Correlation)
      CorrelationUpDownBarArrayList.Add(Chart_Comparison_CorrelationUpDown)
      BetaBarArrayList.Add(Chart_Comparison_Beta)
      BetaBarUpDownArrayList.Add(Chart_Comparison_BetaUpDown)
      AlphaBarArrayList.Add(Chart_Comparison_Alpha)
      APRBarArrayList.Add(Chart_Comparison_APR)
      VolatilityBarArrayList.Add(Chart_Comparison_Volatility)

      MainForm.AddCopyMenuToChart(Me.Chart_Prices)
      MainForm.AddCopyMenuToChart(Me.Chart_Returns)
      MainForm.AddCopyMenuToChart(Me.Chart_RollingReturn)
      MainForm.AddCopyMenuToChart(Me.Chart_ReturnScatter)
      MainForm.AddCopyMenuToChart(Me.Chart_StdDev)
      MainForm.AddCopyMenuToChart(Me.Chart_VAR)
      MainForm.AddCopyMenuToChart(Me.Chart_Omega)
      MainForm.AddCopyMenuToChart(Me.Chart_DrawDown)
      MainForm.AddCopyMenuToChart(Me.Chart_Correlation)
      MainForm.AddCopyMenuToChart(Me.Chart_Beta)
      MainForm.AddCopyMenuToChart(Me.Chart_Alpha)
      MainForm.AddCopyMenuToChart(Me.Chart_Quartile)
      MainForm.AddCopyMenuToChart(Me.Chart_Ranking)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_Correlation)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_CorrelationUpDown)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_Beta)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_BetaUpDown)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_Alpha)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_APR)
      MainForm.AddCopyMenuToChart(Me.Chart_Comparison_Volatility)


    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Call Form_Load(Me, New System.EventArgs)

    Catch ex As Exception
    End Try

    Try

      Me.Menu_ChartsReport_HeaderPage.Checked = True
      Me.Menu_ChartsReport_MultiChart.Checked = True

    Catch ex As Exception
    End Try

  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      ALWAYS_CLOSE_THIS_FORM = True
      Me.Close()

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      FontSizeChanged(Me, New Genoa.GenoaMain.FontChangedEventArgs(MainForm.GenoaFontSize))

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
      _FormOpenFailed = False
      _InUse = True

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False
      Try
        ' Initialise Timer

        Date_ValueDate_ValueDateChanged = False
        SelectListToUpdate_Object = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

        ' Check User permissions
        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

          FormIsValid = False
          _FormOpenFailed = True
          Exit Sub
        End If

        ' Display initial record.

        Check_AutoStart.Checked = False

        Me.TabControl_Charts.SelectedIndex = 0
        Me.Tab_InstrumentDetails.SelectedIndex = 0

        Me.Date_Charts_DateFrom.Value = Renaissance_BaseDate
        Me.Date_Charts_DateTo.Value = Renaissance_EndDate_Data
        Text_Chart_Lamda.Value = 1.0#
        Text_Chart_RollingPeriod.Value = 12.0#
        Text_ScalingFactor.Value = 1.0#
        Radio_SingleScalingFactor.Checked = True
        Radio_Charts_ComparePertrac.Checked = True
        RadioComparison_IsPertrac.Checked = True
        Numeric_QuantileOutlier.Value = 0

        MenuSelectedStatsDataLimit = DealingPeriod.Daily

        Menu_DatePeriod_Daily.Checked = True
        Menu_DatePeriod_Weekly.Checked = False
        Menu_DatePeriod_Fortnightly.Checked = False
        Menu_DatePeriod_Monthly.Checked = False
        Menu_DatePeriod_Quarterly.Checked = False

        ' Localise PerformanceTables

        Dim FirstGrid As C1.Win.C1FlexGrid.C1FlexGrid
        Dim Column As C1.Win.C1FlexGrid.Column

        FirstGrid = Me.Grid_Performance_0
        For Each Column In FirstGrid.Cols
          If System.Enum.IsDefined(GetType(RenaissanceGlobals.Months), Column.Caption) Then
            Column.Caption = CDate(New Date(2000, CInt(System.Enum.Parse(GetType(RenaissanceGlobals.Months), Column.Caption)), 1)).ToString("MMM")
          End If
        Next

      Catch ex As Exception
      Finally
        InPaint = False
      End Try

      ' Initialise Combos
      Call SetComparePertracCombo()
      Call SetCompareGroupCombo()
      Call SetPeriodCombo()
      Call SetChartConditionalCombo()
      Call SetComparisonGroupsCombo()

      ' Initialise Select List
      Try
        If (_StartWithCustomSelection) Then
          If (Not Me.Radio_CustomGroup.Checked) Then
            Radio_CustomGroup.Checked = True
          Else
            ' Populate Select Combo.

            Try
              Combo_SelectFrom.DataSource = Nothing
            Catch ex As Exception
            End Try

            ' Trigger  SelectList Rebuild.

            List_SelectItems.DataSource = Nothing
            List_SelectItems.Text = ""
          End If

        Else

          If (Not Me.Radio_Pertrac.Checked) Then
            Me.Radio_Pertrac.Checked = True  ' Can't be done when InPaint = True.
          Else
            If (Combo_SelectFrom.Items.Count > 0) Then
              If (Combo_SelectFrom.SelectedIndex <> 0) Then
                Combo_SelectFrom.SelectedIndex = 0
              End If
            Else
              Combo_SelectFrom.Text = "A*"
            End If
          End If

        End If
        Application.DoEvents()
      Catch ex As Exception
      End Try
      Date_ValueDate.Value = (Now.Date.AddDays(1 - Now.Date.Date.Day)).AddMonths(1).AddDays(-1)

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    Try
      If (List_SelectItems.Items.Count > 0) Then
        Me.List_SelectItems.SelectedIndex = -1
      End If
    Catch ex As Exception
    End Try

    ' Re-Set Start mode Flag

    _StartWithCustomSelection = False

  End Sub

  Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim HideForm As Boolean

    Try

      ' Hide or Close this form ?
      ' All depends on how many of this form type are Open or in Cache...

      _InUse = False
      MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

      'If (PaintTimer IsNot Nothing) Then
      '	Try
      '		PaintTimer.Stop()
      '	Catch ex As Exception
      '	Finally
      '		PaintTimer = Nothing
      '	End Try
      'End If

      If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
        HideForm = False
      Else

        HideForm = True
        If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
          HideForm = False
        End If
      End If

    Catch ex As Exception
    End Try


    If HideForm = True Then
      Try

        MainForm.HideInFormsCollection(Me)
        Me.Hide() ' NPP Fix

      Catch ex As Exception
      Finally

        e.Cancel = True

      End Try
    Else
      Try
        Dim TabCounter As Integer
        Dim thisTab As TabPage

        For TabCounter = (TabControl_PerformanceTables.TabCount - 1) To 0 Step -1
          If (TabCounter > 0) Then
            thisTab = TabControl_PerformanceTables.TabPages(TabCounter)
            TabControl_PerformanceTables.TabPages.RemoveAt(TabCounter)
            Try
              RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).KeyDown, AddressOf Grid_Performance_KeyDown
              RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
            Catch ex As Exception
            End Try
            Try
              CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Tag = Nothing
              CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip = Nothing
            Catch ex As Exception
            End Try

            Try
              thisTab.Controls.Clear()
              thisTab.Dispose()
            Catch ex As Exception
            End Try
          End If
        Next
      Catch ex As Exception
      End Try

      Try
        MainForm.RemoveFromFormsCollection(Me)
        Call MainForm.PertracData.GetActivePertracInstruments(True, -999)

        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_Charts_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Charts_CompareSeriesPertrac.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_Charts_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_Charts_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Charts_CompareSeriesGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Charts_CompareSeriesGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Charts_CompareSeriesGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Charts_CompareSeriesGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Comparisons_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Comparisons_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Comparisons_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Comparisons_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_Compare_Series.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Compare_Series.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_Compare_Series.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_Compare_Series.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Compare_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Compare_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Compare_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Compare_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Quartile_HighlightSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Quartile_HighlightSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Quartile_HighlightSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Quartile_HighlightSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Ranking_HighlightSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Ranking_HighlightSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Ranking_HighlightSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Ranking_HighlightSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Quartile_ChartData.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Quartile_ChartData.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Quartile_ChartData.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Quartile_ChartData.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Ranking_ChartData.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Ranking_ChartData.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Ranking_ChartData.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Ranking_ChartData.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub

  Private Function FormUpdate_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' Motivation :
		' Sometimes, when charting, parameters may be quickly updated, one wants to defer
		' the chart update until the parameter changes have finished.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False
    Dim SomeFalse As Boolean = False

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InFormUpdate_Tick) OrElse (Not _InUse) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try

    Try
      InFormUpdate_Tick = True

      ' Update Form :

      If (SelectListToUpdate) Then
        SelectListToUpdate = False

        UpdateSelectItemsList()
      End If

      If (SelectSelectionToUpdate) Then
        SelectSelectionToUpdate = False

        UpdateSelectItemsSelection()
      End If

      If (ChartsToUpdate) Then
        Try
          If (Now().Subtract(ChartsToUpdateTime).TotalSeconds >= PAINT_TIMER_DELAY) Then
            Try
              Call Me.PaintAllCharts()
            Catch ex As Exception
            Finally
              ChartsToUpdate = False
            End Try

          Else
            ' Not Yet ready to update
            ' Return False.

            SomeFalse = True
          End If
        Catch ex As Exception
        End Try
      End If

      If (ComparisonToUpdate) Then
        Try
          If (Now().Subtract(ComparisonToUpdateTime).TotalSeconds >= PAINT_TIMER_DELAY) Then
            Try
              Call Me.PaintAllComparison()
            Catch ex As Exception
            Finally
              ComparisonToUpdate = False
            End Try

          Else
            ' Not Yet ready to update
            ' Return False.

            SomeFalse = True
          End If
        Catch ex As Exception
        End Try
      End If

      ' Update After ValueDate.Value Changed

      If (Date_ValueDate_ValueDateChanged) Then
        Date_ValueDate_ValueDateChanged = False

        Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

        ' Trigger  SelectList Rebuild.

        If (Combo_SelectFrom.Items.Count > 0) AndAlso (Combo_SelectFrom.SelectedIndex <> 0) Then
          Combo_SelectFrom.SelectedIndex = 0
        Else
          Call UpdateSelectItemsList()
        End If

      End If

      RVal = Not SomeFalse

    Catch ex As Exception
      RVal = False
    Finally
      InFormUpdate_Tick = False
    End Try

    Return RVal

  End Function

#End Region

  Private Sub FontSizeChanged(ByVal sender As System.Object, ByVal e As GenoaMain.FontChangedEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      MainForm.SetControlFont(Me, e.NewFontSize)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' *****************************************************************************************
    ' Routine to handle changes / updates to tables by this and other windows.
    ' If this, or any other, form posts a change to a table, then it will invoke an update event 
    ' detailing what tables have been altered.
    ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
    ' the 'VeniceAutoUpdate' event of the main Venice form.
    ' Each form may them react as appropriate to changes in any table that might impact it.
    '
    ' *****************************************************************************************

    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    Try

      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
        RefreshForm = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        SetButtonStatus_Flag = True

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPeriod) = True) Or KnowledgeDateChanged Then

        ' Re-Set combo.
        Call Me.SetPeriodCombo()

      End If

      ' SetPertracCombo

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername)) OrElse _
         (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList)) OrElse _
         (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then

        Dim SelectedIDs(-1) As Integer
        Dim ListCounter As Integer

        ' Re-Set combo.
        Call SetComparePertracCombo()

        If Radio_Pertrac.Checked Then

          ' Preserve existing selection

          Try

            If (List_SelectItems.SelectedItems.Count > 0) Then
              ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)

              For ListCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
                SelectedIDs(ListCounter) = CType(List_SelectItems.SelectedItems(ListCounter), DataRowView)(List_SelectItems.ValueMember)
              Next
            End If

          Catch ex As Exception
          End Try

        End If

        ' Clear Pertrac Instruments 'Cache'

        If (PertracInstruments IsNot Nothing) Then
          Try
            PertracInstruments.Dispose()
          Catch ex As Exception
          Finally
            PertracInstruments = Nothing
          End Try
        End If

        If (Radio_Pertrac.Checked) Then
          Try
            ' Trigger  SelectList Rebuild.
            Dim ItemCounter As Integer
            Dim ThisID As Integer

            InPaint = False

            Call Combo_SelectFrom_TextChanged(Combo_SelectFrom, New EventArgs)

            InPaint = True

            If (SelectedIDs.Length > 0) Then
              List_SelectItems.SelectedItems.Clear()

              For ItemCounter = 0 To (List_SelectItems.Items.Count - 1)
                ThisID = List_SelectItems.Items(ItemCounter)(List_SelectItems.ValueMember)

                For ListCounter = 0 To (SelectedIDs.Length - 1)
                  If (SelectedIDs(ListCounter) = ThisID) Then
                    List_SelectItems.SelectedItems.Add(List_SelectItems.Items(ItemCounter))
                    Exit For
                  End If
                Next
              Next

              InPaint = False

              Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

            End If

          Catch ex As Exception
          Finally
            InPaint = True
          End Try
        End If
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then

        Try
          Dim RefreshData As Boolean = False

          InPaint = False

          If (e.UpdateDetail(RenaissanceChangeID.tblCTA_Simulation).Length > 0) Then

            ' Is this Simulation being Shown ?

            Dim ThisID As Integer
            Dim SimIDs() As String = e.UpdateDetail(RenaissanceChangeID.tblCTA_Simulation).Split(New Char() {"|"c, ","c}, StringSplitOptions.RemoveEmptyEntries)

            ' Preserve existing selection

            Try

              If (List_SelectItems.SelectedItems.Count > 0) Then

                For ListCounter As Integer = 0 To (List_SelectItems.SelectedItems.Count - 1)
                  ThisID = CType(List_SelectItems.SelectedItems(ListCounter), DataRowView)(List_SelectItems.ValueMember)

                  If (RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(ThisID) = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.CTA_Simulation) Then
                    If (SimIDs.Contains(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(ThisID).ToString())) Then
                      RefreshData = True
                      Exit For
                    End If
                  End If
                Next

              End If

            Catch ex As Exception
            End Try

          Else

            RefreshData = True

          End If

          If (RefreshData) Then

            ' Repaint charts.

            Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

            ' Repaint Stats tab.

            Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

          End If

        Catch ex As Exception
        Finally
          InPaint = True
        End Try

      End If


      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance)) Then

        ' Re-Set cache.
        ' Me.MainForm.StatFunctions.ClearCache() -- Performed in MainForm.
        ' Me.MainForm.PertracData.ClearDataCache() -- Performed in MainForm.

        Try
          InPaint = False

          ' Repaint charts.

          Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

          ' Repaint Stats tab.

          Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)


        Catch ex As Exception
        Finally
          InPaint = True
        End Try

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Information) = True) Then

        ' Re-Set cache.
        ' Me.MainForm.PertracData.ClearInformationCache() -- Performed in MainForm.

        ' Refresh displayed data.

        Try
          ' Trigger  SelectList Rebuild.

          InPaint = False

          Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        Catch ex As Exception
        Finally
          InPaint = True
        End Try


      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList)) Or KnowledgeDateChanged Then

        ' Re-Set combo.
        Call SetComparisonGroupsCombo()

        Call SetCompareGroupCombo()

        If Radio_ExistingGroups.Checked Then
          Try
            Call MainForm.SetTblGenericCombo( _
            Me.Combo_SelectFrom, _
            RenaissanceStandardDatasets.tblGroupList, _
            "GroupListName", _
            "GroupListID", _
            "", False, True, True, 0, "All Groups")   ' 

          Catch ex As Exception
          Finally
          End Try
        End If

        ' Trigger Repaint etc..

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems)) Or _
        (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData)) Or KnowledgeDateChanged Then

        If Radio_ExistingGroups.Checked Then
          Dim SelectedIDs() As Integer = GetSelectedIDs(List_SelectItems)
          Try
            ' Trigger  SelectList Rebuild.

            InPaint = False
            Call UpdateSelectItemsList()

            SetSelectedIDs(List_SelectItems, SelectedIDs)

            Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

          Catch ex As Exception
          Finally
            InPaint = True
          End Try
        Else ' If (Radio_Pertrac.Checked OrElse Radio_ExistingFunds.Checked) Then
          Try
            InPaint = False

            ' Repaint charts.

            Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

            ' Repaint Stats tab.

            Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

          Catch ex As Exception
          Finally
            InPaint = True
          End Try

        End If

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

        ' Re-Set combo.
        If Radio_ExistingFunds.Checked Then
          Dim SelectedIDs() As Integer = GetSelectedIDs(List_SelectItems)
          Try
            InPaint = False

            Call Radio_ExistingFunds_CheckedChanged(Radio_ExistingFunds, New EventArgs)

            SetSelectedIDs(List_SelectItems, SelectedIDs)
          Catch ex As Exception
          Finally
            InPaint = True
          End Try
        End If

      End If


    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm) OrElse (SetButtonStatus_Flag) Then
      Call SetButtonStatus()
    End If

  End Sub

  Public Function GetSelectedIDs(ByVal List_SelectItems As ListBox) As Integer()
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Dim RVal(-1) As Integer

    Try

      If (List_SelectItems IsNot Nothing) AndAlso (List_SelectItems.SelectedItems.Count > 0) Then
        ReDim RVal(List_SelectItems.SelectedItems.Count - 1)

        Dim ItemCounter As Integer

        Try
          For ItemCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
            RVal(ItemCounter) = List_SelectItems.SelectedItems(ItemCounter)(List_SelectItems.ValueMember)
          Next
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception

    End Try

    Return RVal
  End Function

  Public Sub SetSelectedIDs(ByVal List_SelectItems As ListBox, ByVal SelectItems() As Integer)
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Dim ListCounter As Integer
    Dim ItemCounter As Integer

    Try
      InPaint = True

      If (List_SelectItems.SelectedItems.Count > 0) Then
        List_SelectItems.SelectedItems.Clear()
      End If

      If (SelectItems IsNot Nothing) AndAlso (SelectItems.Length > 0) Then
        For ItemCounter = 0 To (SelectItems.Length - 1)
          For ListCounter = 0 To (List_SelectItems.Items.Count - 1)
            If CInt(List_SelectItems.Items(ListCounter)(List_SelectItems.ValueMember)) = SelectItems(ItemCounter) Then
              If List_SelectItems.SelectedItems.Contains(List_SelectItems.Items(ListCounter)) = False Then
                List_SelectItems.SelectedItems.Add(List_SelectItems.Items(ListCounter))
              End If

              Exit For
            End If
          Next
        Next

      End If
    Catch ex As Exception
    Finally
      InPaint = False
    End Try
  End Sub


#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "


  ' Check User permissions
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetComparePertracCombo()

    Combo_Charts_CompareSeriesPertrac.MasternameCollection = MainForm.MasternameDictionary
    Combo_Compare_Series.MasternameCollection = MainForm.MasternameDictionary
    Combo_Charts_CompareSeriesPertrac.SelectNoMatch = False
    Combo_Compare_Series.SelectNoMatch = False

  End Sub

  Private Sub SetCompareGroupCombo()

    Dim TempTable As New DataTable
    Dim TempTableRow As DataRow
    Dim GroupsTable As DSGroupList.tblGroupListDataTable
    Dim SelectedGroups() As DSGroupList.tblGroupListRow
    Dim GroupRow As DSGroupList.tblGroupListRow

    Try

      TempTable.Columns.Add(New DataColumn("DM", GetType(String)))
      TempTable.Columns.Add(New DataColumn("VM", GetType(Integer)))

      GroupsTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupList), DSGroupList).tblGroupList
      SelectedGroups = GroupsTable.Select("True", "GroupListName")

      ' Dynamic Group

      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of All List Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean)) 'DrillDown_Group_Mean
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of ALL List Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median)) ' DrillDown_Group_Median
      TempTable.Rows.Add(TempTableRow)

      ' Normal Groups.

      For Each GroupRow In SelectedGroups

        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Average Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Median Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Median))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Weighted Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Weighted))
        TempTable.Rows.Add(TempTableRow)

      Next

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
       Me.Combo_Charts_CompareSeriesGroup, _
       TempTable.Select, _
       "DM", _
       "VM", _
       "True", False, True, True, 0, "")     ' 

    ' 

    TempTable = New DataTable

    Try

      TempTable.Columns.Add(New DataColumn("DM", GetType(String)))
      TempTable.Columns.Add(New DataColumn("VM", GetType(Integer)))

      GroupsTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupList), DSGroupList).tblGroupList
      SelectedGroups = GroupsTable.Select("True", "GroupListName")

      ' Dynamic Group

      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of All List Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean)) ' DrillDown_Group_Mean
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of ALL List Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median)) ' DrillDown_Group_Median
      TempTable.Rows.Add(TempTableRow)

      ' Normal Groups.

      For Each GroupRow In SelectedGroups

        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Average Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Median Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Median))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Weighted Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Weighted))
        TempTable.Rows.Add(TempTableRow)

      Next

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
       Me.Combo_Comparisons_Group, _
       TempTable.Select, _
       "DM", _
       "VM", _
       "True", False, True, True, 0, "")     ' 


  End Sub

  Private Sub SetComparisonGroupsCombo()

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_Compare_Group, _
     RenaissanceStandardDatasets.tblGroupList, _
     "GroupListName", _
     "GroupListID", _
     "", False, True, True, 0, "<- Selected Instruments")     ' 

  End Sub

  Private Sub SetChartConditionalCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Chart_Conditional, _
    GetType(StatFunctions.ContingentSelect), _
    False)    ' 

  End Sub

  Private Sub Set_QuartileChartDataCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Quartile_ChartData, _
    GetType(GenoaQuartileDisplayData), _
    False)    ' 

  End Sub

  Private Sub Set_RankingChartDataCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Ranking_ChartData, _
    GetType(GenoaQuartileDisplayData), _
    False)    ' 

  End Sub

  Private Sub SetPeriodCombo()
    Dim PeriodDS As RenaissanceDataClass.DSPeriod
    Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
    Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow
    Dim thisRow As RenaissanceDataClass.DSPeriod.tblPeriodRow

    Dim ParentMenuItem As ToolStripMenuItem
    Dim newMenuItem As ToolStripMenuItem

    PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
    PeriodTbl = PeriodDS.tblPeriod
    SelectedRows = PeriodTbl.Select("True", "PeriodTitle")

    ParentMenuItem = Me.MenuPeriods
    ParentMenuItem.DropDownItems.Clear()

    For Each thisRow In SelectedRows
      newMenuItem = ParentMenuItem.DropDownItems.Add(thisRow.PeriodTitle, Nothing, AddressOf ToolStripCombo_Periods_Click)
      newMenuItem.Tag = thisRow.PeriodID
    Next

    Call ParentMenuItem.DropDownItems.Add(New ToolStripSeparator)

    Dim YearCounter As Integer
    For YearCounter = 2005 To Now.Year
      newMenuItem = ParentMenuItem.DropDownItems.Add(YearCounter.ToString, Nothing, AddressOf ToolStripCombo_Periods_Click)
      newMenuItem.Tag = YearCounter
    Next

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

#Region " Select List Management - Control Events"

  Private Sub Radio_ExistingGroups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingGroups.CheckedChanged
    ' ***********************************************************************************
    ' React to the ExistingGroups radio.
    '
    ' The Select Combo becomes a Combo to select an existing Group,
    ' The SelectList contains all Instruments in the selected Group(s).
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_ExistingGroups.Checked) Then

      Try
        Me.Cursor = Cursors.WaitCursor
        Menu_ChartsReport_CompleteReportPack.Enabled = True
        Date_ValueDate.Enabled = False

        ' Re-Configure Select Combo events.

        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        Catch ex As Exception
        End Try
        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        Catch ex As Exception
        End Try

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        ' Populate Select Combo.

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_SelectFrom, _
        RenaissanceStandardDatasets.tblGroupList, _
        "GroupListName", _
        "GroupListID", _
        "", False, True, True, 0, "All Groups")   ' 

        ' Trigger  SelectList Rebuild.

        If Combo_SelectFrom.Items.Count > 0 Then
          Combo_SelectFrom.SelectedIndex = 0
        End If
        Call UpdateSelectItemsList()

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try

    End If
  End Sub

  Private Sub Radio_Pertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Pertrac.CheckedChanged
    ' ***********************************************************************************
    ' React to the Pertrac Radio button being selected.
    '
    ' The Select combo becomes a Select-As-You-Type edit box for the Select List control.
    ' The SelectList control contains All or Selected Pertrac Instruments.
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_Pertrac.Checked) Then

      Menu_ChartsReport_CompleteReportPack.Enabled = False
      Date_ValueDate.Enabled = False

      ' Re-Configure Select Combo events.

      Try
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      Catch ex As Exception
      End Try
      Try
        RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
      Catch ex As Exception
      End Try

      Try
        Me.Cursor = Cursors.WaitCursor

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        Combo_SelectFrom.DataSource = Nothing
        Combo_SelectFrom.Items.Clear()
        Combo_SelectFrom.Text = "A"

        ' Trigger Select List re-Build.

        Call Combo_SelectFrom_TextChanged(Combo_SelectFrom, New EventArgs)
      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try
    End If

  End Sub

  Private Sub Radio_ExistingFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingFunds.CheckedChanged
    ' ***********************************************************************************
    ' React to the ExistingFunds radio.
    '
    ' The Select Combo becomes a Combo to select an existing Venice Fund,
    ' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_ExistingFunds.Checked) Then

      Try
        Me.Cursor = Cursors.WaitCursor
        Menu_ChartsReport_CompleteReportPack.Enabled = True
        Date_ValueDate.Enabled = True

        ' Re-Configure Select Combo events.

        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        Catch ex As Exception
        End Try
        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        Catch ex As Exception
        End Try

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

        ' Populate Select Combo.

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_SelectFrom, _
        RenaissanceStandardDatasets.tblFund, _
        "FundCode", _
        "FundID", _
        "", False, True, True, 0, "All Funds")   ' 

        ' Trigger  SelectList Rebuild.

        If (Combo_SelectFrom.Items.Count > 0) AndAlso (Combo_SelectFrom.SelectedIndex <> 0) Then
          Combo_SelectFrom.SelectedIndex = 0
        Else
          Call UpdateSelectItemsList()
        End If

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try
    End If
  End Sub

  Private Sub Radio_CustomGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_CustomGroup.CheckedChanged
    ' ***********************************************************************************
    ' React to the ExistingFunds radio.
    '
    ' The Select Combo becomes a Combo to select an existing Venice Fund,
    ' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Radio_CustomGroup.Checked) Then

      Try
        Me.Cursor = Cursors.WaitCursor
        Menu_ChartsReport_CompleteReportPack.Enabled = True
        Date_ValueDate.Enabled = False

        ' Re-Configure Select Combo events.

        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        Catch ex As Exception
        End Try
        Try
          RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        Catch ex As Exception
        End Try

        AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        ' Populate Select Combo.

        Try
          Combo_SelectFrom.DataSource = Nothing
        Catch ex As Exception
        End Try

        ' Trigger  SelectList Rebuild.

        If Radio_CustomGroup.Focused Then
          List_SelectItems.DataSource = Nothing
          List_SelectItems.Text = ""
        End If

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try
    End If
  End Sub

  Private Sub Date_ValueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_ValueDate.ValueChanged
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.InPaint) Then
        Exit Sub
      End If

      If Radio_ExistingFunds.Checked Then

        If (SelectListToUpdate_Object IsNot Nothing) Then
          ' Trigger periodic Update

          Try
            SelectListToUpdate_Object.FormToUpdate = True
            Date_ValueDate_ValueDateChanged = True
          Catch ex As Exception
          End Try
        Else

          Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

          ' Trigger  SelectList Rebuild.

          If (Combo_SelectFrom.Items.Count > 0) AndAlso (Combo_SelectFrom.SelectedIndex <> 0) Then
            Combo_SelectFrom.SelectedIndex = 0
          Else
            Call UpdateSelectItemsList()
          End If

        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Combo_SelectFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.SelectedIndexChanged
    ' ***********************************************************************************
    '
    '
    '
    ' ***********************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.InPaint) Then

        SelectSelectionToUpdate = False
        SelectListToUpdate = True

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub UpdateSelectItemsList()
    ' ***********************************************************************************
    '
    '
    '
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Not Me.InPaint) Then

      Try
        Me.Cursor = Cursors.WaitCursor

        If (Combo_SelectFrom.SelectedIndex >= 0) Then

          If Me.Radio_Pertrac.Checked Then
            ' 
            List_SelectItems.SuspendLayout()

            ' Ensure the DataView is populated.
            If (PertracInstruments Is Nothing) Then
              PertracInstruments = MainForm.PertracData.GetPertracInstruments()
            End If

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.DisplayMember = "ListDescription"
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.ValueMember = "PertracCode"
              End If
            Catch ex As Exception
            End Try

            If (Combo_SelectFrom.SelectedText.Length > 0) Then
              PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.SelectedText & "%'"
            Else
              If (PertracInstruments.Table.Rows.Count > 10000) Then
                PertracInstruments.RowFilter = "False"
              Else
                PertracInstruments.RowFilter = "true"
              End If
            End If

            If (List_SelectItems.DataSource IsNot PertracInstruments) Then
              List_SelectItems.DataSource = PertracInstruments
            End If

            List_SelectItems.ResumeLayout()

            ' Set Dynamic Group Members (Whole List)

            Try
              Dim IDs(PertracInstruments.Count - 1) As Integer
              Dim Ordinal As Integer = PertracInstruments.Table.Columns.IndexOf("PertracCode")
              Dim RowCounter As Integer = 0

              For RowCounter = 0 To (PertracInstruments.Count - 1)
                IDs(RowCounter) = PertracInstruments(RowCounter)(Ordinal)
              Next

              MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, IDs, Nothing)

            Catch ex As Exception
            End Try

          ElseIf (Me.Radio_ExistingGroups.Checked) AndAlso (IsNumeric(Combo_SelectFrom.SelectedValue)) Then
            ' Populate Form with given data.

            Dim tmpCommand As New SqlCommand
            Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

            Try

              tmpCommand.CommandType = CommandType.StoredProcedure
              tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
              tmpCommand.Connection = MainForm.GetGenoaConnection
              tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectFrom.SelectedValue)
              tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
              tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

              List_SelectItems.SuspendLayout()
              List_SelectItems.SelectedItems.Clear()

              If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
                ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
                ListTable.Columns.Add("ListDescription", GetType(String))
                ListTable.Columns.Add("PertracName", GetType(String))
                ListTable.Columns.Add("PertracProvider", GetType(String))
                ListTable.Columns.Add("IndexName", GetType(String))
                ListTable.Columns.Add("IndexProvider", GetType(String))

                ' List_SelectItems.DataSource = ListTable
              Else
                ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
              End If

              Try
                ListTable.Rows.Clear()
                ListTable.Load(tmpCommand.ExecuteReader)
              Catch ex As Exception
              End Try

              Try
                If (ListTable IsNot List_SelectItems.DataSource) Then
                  List_SelectItems.DataSource = Nothing
                End If
                If (List_SelectItems.DisplayMember <> "ListDescription") Then
                  List_SelectItems.DisplayMember = "ListDescription"
                End If
                If (List_SelectItems.ValueMember <> "GroupPertracCode") Then
                  List_SelectItems.ValueMember = "GroupPertracCode"
                End If
              Catch ex As Exception
              End Try

              List_SelectItems.DataSource = ListTable
              List_SelectItems.ResumeLayout()

              ' Set Dynamic Group members and pre-load instrument data if a Group is selected.

              Try
                If (ListTable.Rows.Count > 0) Then

                  Dim SelectedPertracIDs(ListTable.Rows.Count - 1) As Integer
                  Dim PertracCount As Integer

                  For PertracCount = 0 To (ListTable.Rows.Count - 1)
                    SelectedPertracIDs(PertracCount) = ListTable.Rows(PertracCount)("GroupPertracCode")
                  Next

                  If (Combo_SelectFrom.SelectedIndex > 0) Then

                    Try
                      MainForm.GenoaStatusLabel.Text = "Loading Pertrac Returns Series (x" & SelectedPertracIDs.Length.ToString & ")"
                      Application.DoEvents()

                      MainForm.PertracData.LoadPertracTables(SelectedPertracIDs)
                    Catch ex As Exception
                    Finally
                      MainForm.GenoaStatusLabel.Text = ""
                    End Try

                  End If

                  ' Set Dynamic Group Members (Whole List)

                  MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, SelectedPertracIDs, Nothing)

                End If
              Catch ex As Exception
              End Try

            Catch ex As Exception

              Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)

            Finally
              Try
                If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
                  tmpCommand.Connection.Close()
                  tmpCommand.Connection = Nothing
                End If
              Catch ex As Exception
              End Try
            End Try

          ElseIf Me.Radio_ExistingFunds.Checked Then

            List_SelectItems.SuspendLayout()

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.DisplayMember = "ListDescription"
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.ValueMember = "PertracCode"
              End If
            Catch ex As Exception
            End Try

            List_SelectItems.DataSource = MainForm.PertracData.GetActivePertracInstruments(False, CInt(Combo_SelectFrom.SelectedValue), Me.Date_ValueDate.Value)

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DisplayMember = "ListDescription"
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.ValueMember = "PertracCode"
              End If
            Catch ex As Exception
            End Try

            List_SelectItems.ResumeLayout()

            ' Set Dynamic Group members and pre-load instrument data.

            Try
              Dim ListTable As DataTable = List_SelectItems.DataSource

              If (ListTable.Rows.Count > 0) Then
                Dim SelectedPertracIDs(ListTable.Rows.Count - 1) As Integer
                Dim PertracCount As Integer

                For PertracCount = 0 To (ListTable.Rows.Count - 1)
                  SelectedPertracIDs(PertracCount) = ListTable.Rows(PertracCount)("PertracCode")
                Next

                Try
                  MainForm.GenoaStatusLabel.Text = "Loading Pertrac Returns Series (x" & SelectedPertracIDs.Length.ToString & ")"
                  Application.DoEvents()

                  MainForm.PertracData.LoadPertracTables(SelectedPertracIDs)
                Catch ex As Exception
                Finally
                  MainForm.GenoaStatusLabel.Text = ""
                End Try

                ' Set Dynamic Group Members (Whole List)

                MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, SelectedPertracIDs, Nothing)

              End If
            Catch ex As Exception
            End Try

          ElseIf Radio_CustomGroup.Checked Then

            Try

              List_SelectItems.DataSource = Nothing

            Catch ex As Exception
            End Try

          End If

        End If

      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
      End Try

    End If ' Created & Not InPaint

  End Sub

  Private Sub UpdateSelectItemsSelection()
    ' ***********************************************************************************
    ' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
    '
    ' The List is managed by applying a select critera to the PertracInstruments DataView.
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Me.InPaint = False) Then
      If (Combo_SelectFrom.SelectedIndex < 0) Then
        If Me.Radio_Pertrac.Checked Then

          Try
            Me.Cursor = Cursors.WaitCursor

            ' Ensure the DataView is populated.
            If (PertracInstruments Is Nothing) Then
              PertracInstruments = MainForm.PertracData.GetPertracInstruments()
            End If

            ' Set Display and View Members

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.DisplayMember = "ListDescription"
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.DataSource = Nothing
                List_SelectItems.ValueMember = "PertracCode"
              End If
            Catch ex As Exception
            End Try

            ' Set Selection criteria.

            Try
              Dim SelectString As String

              SelectString = Combo_SelectFrom.Text

              SelectString = SelectString.Replace("*", "%")

              If (SelectString.Length > 0) Then
                If (SelectString.Substring(1).Contains("%")) Then
                  SelectString = SelectString.Substring(0, SelectString.Substring(1).IndexOf("%") + 2)
                End If
              End If

              If (SelectString.Length > 0) AndAlso (Not SelectString.EndsWith("%")) Then
                SelectString &= "%"
              End If

              If (SelectString.Length <= 0) Then
                If (PertracInstruments.Table.Rows.Count > 10000) Then
                  If (PertracInstruments.RowFilter <> "False") Then
                    PertracInstruments.RowFilter = "False"
                  End If
                Else
                  PertracInstruments.RowFilter = "true"
                End If
              Else
                If (PertracInstruments.RowFilter <> "Mastername LIKE '" & SelectString & "'") Then
                  PertracInstruments.RowFilter = "Mastername LIKE '" & SelectString & "'"
                End If
              End If
            Catch ex As Exception
              PertracInstruments.RowFilter = "False"
            End Try

            ' Set Dynamic Group Members (Whole List)

            Try
              Dim IDs(PertracInstruments.Count - 1) As Integer
              Dim Ordinal As Integer = PertracInstruments.Table.Columns.IndexOf("PertracCode")
              Dim RowCounter As Integer = 0

              For RowCounter = 0 To (PertracInstruments.Count - 1)
                IDs(RowCounter) = PertracInstruments(RowCounter)(Ordinal)
              Next

              MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, IDs, Nothing)

            Catch ex As Exception
            End Try

            ' Set DataSource

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DisplayMember = ""
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.ValueMember = ""
              End If

              If (List_SelectItems.DataSource IsNot PertracInstruments) Then
                Dim orgInPaint As Boolean = InPaint
                Try
                  InPaint = True

                  List_SelectItems.BeginUpdate()
                  List_SelectItems.DataSource = PertracInstruments
                Catch ex As Exception
                Finally
                  List_SelectItems.EndUpdate()
                  InPaint = orgInPaint
                End Try

              End If

            Catch ex As Exception
            End Try

            ' Set Display and View Members (Double check).

            Try
              If (List_SelectItems.DisplayMember <> "ListDescription") Then
                List_SelectItems.DisplayMember = "ListDescription"
              End If

              If (List_SelectItems.ValueMember <> "PertracCode") Then
                List_SelectItems.ValueMember = "PertracCode"
              End If
            Catch ex As Exception
            End Try

          Catch ex As Exception
          Finally
            Me.Cursor = Cursors.Default
          End Try

        End If
      End If
    End If
  End Sub

  Private Sub Combo_SelectFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.TextChanged
    ' ***********************************************************************************
    ' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
    '
    ' The List is managed by applying a select critera to the PertracInstruments DataView.
    ' ***********************************************************************************

    If (Me.Created) AndAlso (Me.InPaint = False) Then
      If (Combo_SelectFrom.SelectedIndex < 0) Then

        SelectSelectionToUpdate = True

        'If Me.Radio_Pertrac.Checked Then

        '  Try
        '    Me.Cursor = Cursors.WaitCursor

        '    ' Ensure the DataView is populated.
        '    If (PertracInstruments Is Nothing) Then
        '      PertracInstruments = MainForm.PertracData.GetPertracInstruments()
        '    End If

        '    ' Set Display and View Members

        '    Try
        '      If (List_SelectItems.DisplayMember <> "ListDescription") Then
        '        List_SelectItems.DataSource = Nothing
        '        List_SelectItems.DisplayMember = "ListDescription"
        '      End If

        '      If (List_SelectItems.ValueMember <> "PertracCode") Then
        '        List_SelectItems.DataSource = Nothing
        '        List_SelectItems.ValueMember = "PertracCode"
        '      End If
        '    Catch ex As Exception
        '    End Try

        '    ' Set Selection criteria.

        '    Try
        '      Dim SelectString As String

        '      SelectString = Combo_SelectFrom.Text

        '      SelectString = SelectString.Replace("*", "%")

        '      If (SelectString.Length > 0) Then
        '        If (SelectString.Substring(1).Contains("%")) Then
        '          SelectString = SelectString.Substring(0, SelectString.Substring(1).IndexOf("%") + 2)
        '        End If
        '      End If

        '      If (SelectString.Length > 0) AndAlso (Not SelectString.EndsWith("%")) Then
        '        SelectString &= "%"
        '      End If

        '      If (SelectString.Length <= 0) Then
        '        If (PertracInstruments.Table.Rows.Count > 10000) Then
        '          If (PertracInstruments.RowFilter <> "False") Then
        '            PertracInstruments.RowFilter = "False"
        '          End If
        '        Else
        '          PertracInstruments.RowFilter = "true"
        '        End If
        '      Else
        '        If (PertracInstruments.RowFilter <> "Mastername LIKE '" & SelectString & "'") Then
        '          PertracInstruments.RowFilter = "Mastername LIKE '" & SelectString & "'"
        '        End If
        '      End If
        '    Catch ex As Exception
        '      PertracInstruments.RowFilter = "False"
        '    End Try

        '    ' Set Dynamic Group Members (Whole List)

        '    Try
        '      Dim IDs(PertracInstruments.Count - 1) As Integer
        '      Dim Ordinal As Integer = PertracInstruments.Table.Columns.IndexOf("PertracCode")
        '      Dim RowCounter As Integer = 0

        '      For RowCounter = 0 To (PertracInstruments.Count - 1)
        '        IDs(RowCounter) = PertracInstruments(RowCounter)(Ordinal)
        '      Next

        '      MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, IDs, Nothing)

        '    Catch ex As Exception
        '    End Try

        '    ' Set DataSource

        '    Try
        '      If (List_SelectItems.DisplayMember <> "ListDescription") Then
        '        List_SelectItems.DisplayMember = ""
        '      End If

        '      If (List_SelectItems.ValueMember <> "PertracCode") Then
        '        List_SelectItems.ValueMember = ""
        '      End If

        '      If (List_SelectItems.DataSource IsNot PertracInstruments) Then
        '        Dim orgInPaint As Boolean = InPaint
        '        Try
        '          InPaint = True

        '          List_SelectItems.BeginUpdate()
        '          List_SelectItems.DataSource = PertracInstruments
        '        Catch ex As Exception
        '        Finally
        '          List_SelectItems.EndUpdate()
        '          InPaint = orgInPaint
        '        End Try

        '      End If

        '    Catch ex As Exception
        '    End Try

        '    ' Set Display and View Members (Double check).

        '    Try
        '      If (List_SelectItems.DisplayMember <> "ListDescription") Then
        '        List_SelectItems.DisplayMember = "ListDescription"
        '      End If

        '      If (List_SelectItems.ValueMember <> "PertracCode") Then
        '        List_SelectItems.ValueMember = "PertracCode"
        '      End If
        '    Catch ex As Exception
        '    End Try

        '  Catch ex As Exception
        '  Finally
        '    Me.Cursor = Cursors.Default
        '  End Try

        'End If
      End If
    End If
  End Sub

  Private Sub List_SelectItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_SelectItems.SelectedIndexChanged
    ' *****************************************************************************
    ' Update the Instrument charts when Instruments are selected / Unselected
    '
    ' *****************************************************************************

    If (Me.Created = False) Or (InPaint) Then
      Exit Sub
    End If

    Dim PertracID As Integer = 0
    Dim PertracName As String

    Try
      Me.Cursor = Cursors.WaitCursor

      ' Update the 'Quartile' and 'Ranking' chart select combos.

      If List_SelectItems.SelectedItems.Count > 0 Then
        Menu_InformationReport.Enabled = True
        'Menu_ChartsReport.Enabled = True
        Menu_ChartsReport_Basic.Enabled = True
        Menu_ChartsReport_Quartile.Enabled = True
        Menu_ChartsReport_Ranking.Enabled = True
        Menu_ChartsReport_All.Enabled = True

        Dim RowArray(List_SelectItems.SelectedItems.Count - 1) As DataRowView

        List_SelectItems.SelectedItems.CopyTo(RowArray, 0)

        Try
          ' Save List IDs to the relevent Custom Group.

          Dim ItemIDs(List_SelectItems.SelectedItems.Count - 1) As Integer

          For ThisItemIndex As Integer = 0 To (List_SelectItems.SelectedItems.Count - 1)
            ItemIDs(ThisItemIndex) = CInt(RowArray(ThisItemIndex)(List_SelectItems.ValueMember))
          Next

          MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_SelectedList, ItemIDs, Nothing)

        Catch ex As Exception
        End Try


        MainForm.SetTblGenericCombo(Combo_Quartile_HighlightSeries, RowArray, List_SelectItems.DisplayMember, List_SelectItems.ValueMember, "True", False, True, True)
        MainForm.SetTblGenericCombo(Combo_Ranking_HighlightSeries, RowArray, List_SelectItems.DisplayMember, List_SelectItems.ValueMember, "True", False, True, True)
        MainForm.SetTblGenericCombo(Combo_Statistics_HighlightSeries, RowArray, List_SelectItems.DisplayMember, List_SelectItems.ValueMember, "True", False, True, False)
        MainForm.SetTblGenericCombo(Combo_Charts_VARSeries, RowArray, List_SelectItems.DisplayMember, List_SelectItems.ValueMember, "True", False, True, False)

        If (Combo_Quartile_HighlightSeries.SelectedIndex < 0) AndAlso (Combo_Quartile_HighlightSeries.Items.Count > 0) Then
          Combo_Quartile_HighlightSeries.SelectedIndex = 0
        End If
        If (Combo_Ranking_HighlightSeries.SelectedIndex < 0) AndAlso (Combo_Ranking_HighlightSeries.Items.Count > 0) Then
          Combo_Ranking_HighlightSeries.SelectedIndex = 0
        End If
        If (Combo_Statistics_HighlightSeries.SelectedIndex < 0) AndAlso (Combo_Statistics_HighlightSeries.Items.Count > 0) Then
          Combo_Statistics_HighlightSeries.SelectedIndex = 0
        End If
        If (Combo_Charts_VARSeries.SelectedIndex < 0) AndAlso (Combo_Charts_VARSeries.Items.Count > 0) Then
          Combo_Charts_VARSeries.SelectedIndex = 0
        End If
      Else
        Try
          MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_SelectedList, New Integer() {}, New Double() {})
        Catch ex As Exception
        End Try

        Menu_InformationReport.Enabled = False
        'Menu_ChartsReport.Enabled = False
        Menu_ChartsReport_Basic.Enabled = False
        Menu_ChartsReport_Quartile.Enabled = False
        Menu_ChartsReport_Ranking.Enabled = False
        Menu_ChartsReport_All.Enabled = False

        Try
          Combo_Quartile_HighlightSeries.DataSource = Nothing
          Combo_Ranking_HighlightSeries.DataSource = Nothing
          Combo_Statistics_HighlightSeries.DataSource = Nothing
          Combo_Charts_VARSeries.DataSource = Nothing
        Catch ex As Exception
        End Try
        Try
          Me.Combo_Quartile_HighlightSeries.Items.Clear()
          Me.Combo_Ranking_HighlightSeries.Items.Clear()
          Me.Combo_Statistics_HighlightSeries.Items.Clear()
          Me.Combo_Charts_VARSeries.Items.Clear()
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    Try
      Me.Cursor = Cursors.WaitCursor

      ' Display Fund Details
      If (List_SelectItems.SelectedIndex >= 0) Then
        Try
          PertracID = CInt(Nz(List_SelectItems.SelectedItem(List_SelectItems.ValueMember), 0))
          PertracName = CStr(Nz(List_SelectItems.SelectedItem(List_SelectItems.DisplayMember), "<Null>"))
        Catch ex As Exception
          PertracID = 0
          PertracName = "<Error>"
        End Try
      End If

      ' Set Fields.

      Dim InformationArray() As Object
      If PertracID > 0 Then
        InformationArray = MainForm.PertracData.GetInformationValues(PertracID)
      Else
        InformationArray = Nothing
      End If

      If InformationArray IsNot Nothing Then

        ' Strategy
        If (InformationArray(PertracInformationFields.Strategy) IsNot Nothing) Then
          Label_Strategy.Text = Nz(InformationArray(PertracInformationFields.Strategy), "").ToString
        Else
          Label_Strategy.Text = ""
        End If

        ' DataSource
        If (InformationArray(PertracInformationFields.DataVendorName) IsNot Nothing) Then
          Label_DataSource.Text = Nz(InformationArray(PertracInformationFields.DataVendorName), "").ToString
        Else
          Label_DataSource.Text = ""
        End If

        ' Fund Name
        If (InformationArray(PertracInformationFields.FundName) IsNot Nothing) Then
          Label_FundName.Text = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
          Label_FundName2.Text = Nz(InformationArray(PertracInformationFields.FundName), "").ToString
        Else
          Label_FundName.Text = ""
          Label_FundName2.Text = ""
        End If

        ' ManagementCo
        If (InformationArray(PertracInformationFields.CompanyName) IsNot Nothing) Then
          Label_ManagementCo.Text = Nz(InformationArray(PertracInformationFields.CompanyName), "").ToString
          Label_ManagementCo2.Text = Nz(InformationArray(PertracInformationFields.CompanyName), "").ToString
        Else
          Label_ManagementCo.Text = ""
          Label_ManagementCo2.Text = ""
        End If

        ' Description
        If (InformationArray(PertracInformationFields.Description) IsNot Nothing) Then
          Text_Description.Text = Nz(InformationArray(PertracInformationFields.Description), "").ToString
        Else
          Text_Description.Text = ""
        End If

        ' ManagementFee
        If (InformationArray(PertracInformationFields.ManagementFee) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.ManagementFee)) Then
            Label_MgmtFee.Text = CDbl(Nz(InformationArray(PertracInformationFields.ManagementFee), "0")).ToString("#,##0.00") & "%"
          Else
            Label_MgmtFee.Text = Nz(InformationArray(PertracInformationFields.ManagementFee), "").ToString
          End If
        Else
          Label_MgmtFee.Text = ""
        End If

        ' IncentiveFee
        If (InformationArray(PertracInformationFields.IncentiveFee) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.IncentiveFee)) Then
            Label_PerformanceFee.Text = CDbl(InformationArray(PertracInformationFields.IncentiveFee)).ToString("#,##0.00") & "%"
          Else
            Label_PerformanceFee.Text = InformationArray(PertracInformationFields.IncentiveFee).ToString
          End If
        Else
          Label_PerformanceFee.Text = ""
        End If

        ' Hurdle
        If (InformationArray(PertracInformationFields.Hurdle) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.Hurdle)) Then
            Label_Hurdle.Text = CDbl(InformationArray(PertracInformationFields.Hurdle)).ToString("#,##0.00%")
          Else
            Label_Hurdle.Text = InformationArray(PertracInformationFields.Hurdle).ToString
          End If
        Else
          Label_Hurdle.Text = ""
        End If

        ' Currency
        If (InformationArray(PertracInformationFields.Currency) IsNot Nothing) Then
          Label_Currency.Text = InformationArray(PertracInformationFields.Currency).ToString
        Else
          Label_Currency.Text = ""
        End If

        ' FundAssets
        If (InformationArray(PertracInformationFields.FundAssets) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.FundAssets)) Then
            Label_FundAssets.Text = CDbl(InformationArray(PertracInformationFields.FundAssets)).ToString("#,##0")
          Else
            Label_FundAssets.Text = InformationArray(PertracInformationFields.FundAssets).ToString
          End If
        Else
          Label_FundAssets.Text = ""
        End If

        ' FirmAssets
        If (InformationArray(PertracInformationFields.FirmAssets) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.FirmAssets)) Then
            Label_FirmAssets.Text = CDbl(InformationArray(PertracInformationFields.FirmAssets)).ToString("#,##0")
          Else
            Label_FirmAssets.Text = InformationArray(PertracInformationFields.FirmAssets).ToString
          End If
        Else
          Label_FirmAssets.Text = ""
        End If

        ' Domicile
        If (InformationArray(PertracInformationFields.Domicile) IsNot Nothing) Then
          Label_Domicile.Text = InformationArray(PertracInformationFields.Domicile).ToString
        Else
          Label_Domicile.Text = ""
        End If

        ' InvestorType
        If (InformationArray(PertracInformationFields.InvestorType) IsNot Nothing) Then
          Label_InvestorType.Text = InformationArray(PertracInformationFields.InvestorType).ToString
        Else
          Label_InvestorType.Text = ""
        End If

        ' Open
        If (InformationArray(PertracInformationFields.Open) IsNot Nothing) Then
          Label_Open.Text = InformationArray(PertracInformationFields.Open).ToString
        Else
          Label_Open.Text = ""
        End If

        ' Administrator
        If (InformationArray(PertracInformationFields.Administrator) IsNot Nothing) Then
          Label_Administrator.Text = InformationArray(PertracInformationFields.Administrator).ToString
          Label_Administrator2.Text = InformationArray(PertracInformationFields.Administrator).ToString
        Else
          Label_Administrator.Text = ""
          Label_Administrator2.Text = ""
        End If

        ' MinimumInvestment
        If (InformationArray(PertracInformationFields.MinimumInvestment) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.MinimumInvestment)) Then
            Label_MinInvestment.Text = CDbl(InformationArray(PertracInformationFields.MinimumInvestment)).ToString("#,##0")
          Else
            Label_MinInvestment.Text = InformationArray(PertracInformationFields.MinimumInvestment).ToString
          End If
        Else
          Label_MinInvestment.Text = ""
        End If

        ' Subscription
        If (InformationArray(PertracInformationFields.Subscription) IsNot Nothing) Then
          Label_Subscription.Text = InformationArray(PertracInformationFields.Subscription).ToString
        Else
          Label_Subscription.Text = ""
        End If

        ' Redemption
        If (InformationArray(PertracInformationFields.Redemption) IsNot Nothing) Then
          Label_Redemption.Text = InformationArray(PertracInformationFields.Redemption).ToString
        Else
          Label_Redemption.Text = ""
        End If

        ' Lockup
        If (InformationArray(PertracInformationFields.Lockup) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.Lockup)) Then
            Label_Lockup.Text = CDbl(InformationArray(PertracInformationFields.Lockup)).ToString("#,##0")
          Else
            Label_Lockup.Text = InformationArray(PertracInformationFields.Lockup).ToString
          End If
        Else
          Label_Lockup.Text = ""
        End If

        ' notice
        If (InformationArray(PertracInformationFields.Notice) IsNot Nothing) Then
          If IsNumeric(InformationArray(PertracInformationFields.Notice)) Then
            Label_Notice.Text = CDbl(InformationArray(PertracInformationFields.Notice)).ToString("#,##0")
          Else
            Label_Notice.Text = InformationArray(PertracInformationFields.Notice).ToString
          End If
        Else
          Label_Notice.Text = ""
        End If

        ' Address1
        If (InformationArray(PertracInformationFields.Address1) IsNot Nothing) Then
          Label_Address1.Text = InformationArray(PertracInformationFields.Address1).ToString
        Else
          Label_Address1.Text = ""
        End If

        ' Address2
        If (InformationArray(PertracInformationFields.Address2) IsNot Nothing) Then
          Label_Address2.Text = InformationArray(PertracInformationFields.Address2).ToString
        Else
          Label_Address2.Text = ""
        End If

        ' City
        If (InformationArray(PertracInformationFields.City) IsNot Nothing) Then
          Label_City.Text = InformationArray(PertracInformationFields.City).ToString
        Else
          Label_City.Text = ""
        End If

        ' State
        If (InformationArray(PertracInformationFields.State) IsNot Nothing) Then
          Label_State.Text = InformationArray(PertracInformationFields.State).ToString
        Else
          Label_State.Text = ""
        End If

        ' Zip
        If (InformationArray(PertracInformationFields.ZipCode) IsNot Nothing) Then
          Label_Zip.Text = InformationArray(PertracInformationFields.ZipCode).ToString
        Else
          Label_Zip.Text = ""
        End If

        ' Country
        If (InformationArray(PertracInformationFields.Country) IsNot Nothing) Then
          Label_Country.Text = InformationArray(PertracInformationFields.Country).ToString
        Else
          Label_Country.Text = ""
        End If

        ' Contact
        If (InformationArray(PertracInformationFields.Contact) IsNot Nothing) Then
          Label_Contact.Text = InformationArray(PertracInformationFields.Contact).ToString
        Else
          Label_Contact.Text = ""
        End If

        ' ContactPhone
        If (InformationArray(PertracInformationFields.ContactPhone) IsNot Nothing) Then
          Label_ContactPhone.Text = InformationArray(PertracInformationFields.ContactPhone).ToString
        Else
          Label_ContactPhone.Text = ""
        End If

        ' ContactFax
        If (InformationArray(PertracInformationFields.ContactFax) IsNot Nothing) Then
          Label_ContactFax.Text = InformationArray(PertracInformationFields.ContactFax).ToString
        Else
          Label_ContactFax.Text = ""
        End If

        ' Email
        If (InformationArray(PertracInformationFields.Email) IsNot Nothing) Then
          Label_ContactEMail.Text = InformationArray(PertracInformationFields.Email).ToString
        Else
          Label_ContactEMail.Text = ""
        End If

        ' MarketingContact
        If (InformationArray(PertracInformationFields.MarketingContact) IsNot Nothing) Then
          Label_MarketingContact.Text = InformationArray(PertracInformationFields.MarketingContact).ToString
        Else
          Label_MarketingContact.Text = ""
        End If

        ' MarketingContactPhone
        If (InformationArray(PertracInformationFields.MarketingPhone) IsNot Nothing) Then
          Label_MarketingPhone.Text = InformationArray(PertracInformationFields.MarketingPhone).ToString
        Else
          Label_MarketingPhone.Text = ""
        End If

        ' MarketingFax
        If (InformationArray(PertracInformationFields.MarketingFax) IsNot Nothing) Then
          Label_MarketingFax.Text = InformationArray(PertracInformationFields.MarketingFax).ToString
        Else
          Label_MarketingFax.Text = ""
        End If

        ' ManagerName
        If (InformationArray(PertracInformationFields.ManagerName) IsNot Nothing) Then
          Label_Managers.Text = InformationArray(PertracInformationFields.ManagerName).ToString
        Else
          Label_Managers.Text = ""
        End If

        ' ManagerName
        If (InformationArray(PertracInformationFields.Website) IsNot Nothing) Then
          LinkLabel_WebSite.Text = InformationArray(PertracInformationFields.Website).ToString
        Else
          LinkLabel_WebSite.Text = ""
        End If


      Else
        ' Clear Fields

        Label_Strategy.Text = ""
        Label_DataSource.Text = ""
        Label_FundName.Text = ""
        Label_FundName2.Text = ""
        Label_ManagementCo.Text = ""
        Label_ManagementCo2.Text = ""
        Text_Description.Text = ""
        Label_MgmtFee.Text = ""
        Label_PerformanceFee.Text = ""
        Label_Hurdle.Text = ""
        Label_Currency.Text = ""
        Label_FundAssets.Text = ""
        Label_FirmAssets.Text = ""
        Label_Domicile.Text = ""
        Label_InvestorType.Text = ""
        Label_Open.Text = ""
        Label_Administrator.Text = ""
        Label_Administrator2.Text = ""
        Label_MinInvestment.Text = ""
        Label_Subscription.Text = ""
        Label_Redemption.Text = ""
        Label_Lockup.Text = ""
        Label_Notice.Text = ""
        Label_Address1.Text = ""
        Label_Address2.Text = ""
        Label_City.Text = ""
        Label_State.Text = ""
        Label_Zip.Text = ""
        Label_Country.Text = ""
        Label_Contact.Text = ""
        Label_ContactPhone.Text = ""
        Label_ContactFax.Text = ""
        Label_ContactEMail.Text = ""
        Label_MarketingContact.Text = ""
        Label_MarketingPhone.Text = ""
        Label_MarketingFax.Text = ""
        Label_Managers.Text = ""
        LinkLabel_WebSite.Text = ""

      End If

      ' Set Performance Data

      Call SetPerformanceDataGrids()

      ' Set Chart Start Date 

      If (Check_AutoStart.Checked) Then

        Me.Date_Charts_DateFrom.Value = GetLatestStartDateFromList(List_SelectItems)

        ChartsToUpdate = False

      End If

      ' Paint Charts :-

      Call PaintAllCharts()

      ' Update Comparison Stats

      Call PaintAllComparison()


    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

#End Region

#Region " 'Charts' Tab Control events"

  Private Sub Combo_Charts_CompareSeriesPertrac_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_CompareSeriesPertrac.SelectedValueChanged  ' SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeriesPertrac_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_Charts_CompareSeriesGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_CompareSeriesGroup.SelectedIndexChanged   ' 
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeriesGroup_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_Chart_Conditional_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Chart_Conditional.SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    If InPaint = False Then

      PaintCorrelationCharts()

      PaintAlphaCharts()

      PaintBetaCharts()

    End If
  End Sub

  Private Sub Charts_Parameter_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
   Handles Date_Charts_DateFrom.ValueChanged, Date_Charts_DateTo.ValueChanged, Text_Chart_Lamda.ValueChanged, Text_Chart_RollingPeriod.ValueChanged, Text_ScalingFactor.ValueChanged

    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_SingleScalingFactor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SingleScalingFactor.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Radio_SingleScalingFactor.Checked) Then

        Me.Text_ScalingFactor.Enabled = True

        If (Not InPaint) Then

          ChartsToUpdate = True

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_DynamicScalingFactor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_DynamicScalingFactor.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Radio_DynamicScalingFactor.Checked) Then

        Me.Text_ScalingFactor.Enabled = False

        If (Not InPaint) Then

          ChartsToUpdate = True

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Button_DefaultDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DefaultDate.Click
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      Me.Date_Charts_DateFrom.Value = Renaissance_BaseDate
      Me.Date_Charts_DateTo.Value = Renaissance_EndDate_Data
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Text_Chart_VARPeriod_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Chart_VARPeriod.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Chart_VARConfidence_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Text_Chart_VARConfidence.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Charts_VARSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_VARSeries.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Edit_OmegaReturn_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Edit_OmegaReturn.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintOmegaCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_OmegaRatio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmegaRatio.CheckedChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintOmegaCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Quartile_ChartData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Quartile_ChartData.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (InPaint = False) Then
          ' Propogate change to Ranking Combo, if appropriate
          If (Combo_Quartile_ChartData.Focused) Then
            If (Combo_Ranking_ChartData.Items.Count = Combo_Quartile_ChartData.Items.Count) AndAlso (Combo_Ranking_ChartData.SelectedIndex <> Combo_Quartile_ChartData.SelectedIndex) Then
              Combo_Ranking_ChartData.SelectedIndex = Combo_Quartile_ChartData.SelectedIndex
            End If
          End If

          PaintQuartileCharts()

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Quartile_HighlightSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Quartile_HighlightSeries.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (InPaint = False) Then
          ' Propogate change to Ranking Combo, if appropriate
          If (Combo_Quartile_HighlightSeries.Focused) Then
            If (Combo_Ranking_HighlightSeries.Items.Count = Combo_Quartile_HighlightSeries.Items.Count) AndAlso (Combo_Ranking_HighlightSeries.SelectedIndex <> Combo_Quartile_HighlightSeries.SelectedIndex) Then
              Combo_Ranking_HighlightSeries.SelectedIndex = Combo_Quartile_HighlightSeries.SelectedIndex
            End If
          End If

          PaintQuartileCharts()

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Numeric_QuantileOutlier_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Numeric_QuantileOutlier.ValueChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (InPaint = False) Then

          PaintQuartileCharts()

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Ranking_ChartData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Ranking_ChartData.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    If (InPaint = False) Then
      ' Propogate change to Ranking Combo, if appropriate
      If (Combo_Ranking_ChartData.Focused) Then
        If (Combo_Quartile_ChartData.Items.Count = Combo_Ranking_ChartData.Items.Count) AndAlso (Combo_Quartile_ChartData.SelectedIndex <> Combo_Ranking_ChartData.SelectedIndex) Then
          Combo_Quartile_ChartData.SelectedIndex = Combo_Ranking_ChartData.SelectedIndex
        End If
      End If

      PaintRankingCharts()

    End If

  End Sub

  Private Sub Combo_Ranking_HighlightSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Ranking_HighlightSeries.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    If (InPaint = False) Then
      ' Propogate change to Ranking Combo, if appropriate
      If (Combo_Ranking_HighlightSeries.Focused) Then
        If (Combo_Quartile_HighlightSeries.Items.Count = Combo_Ranking_HighlightSeries.Items.Count) AndAlso (Combo_Quartile_HighlightSeries.SelectedIndex <> Combo_Ranking_HighlightSeries.SelectedIndex) Then
          Combo_Quartile_HighlightSeries.SelectedIndex = Combo_Ranking_HighlightSeries.SelectedIndex
        End If
      End If

      PaintRankingCharts()

    End If

  End Sub

  Private Sub Radio_Charts_ComparePertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Charts_ComparePertrac.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        Combo_Charts_CompareSeriesPertrac.Visible = True
        Combo_Charts_CompareSeriesGroup.Visible = False

        ChartsToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub


  Private Sub Radio_Charts_CompareGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Charts_CompareGroup.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Radio_Charts_CompareGroup.Checked) Then
        Combo_Charts_CompareSeriesPertrac.Visible = False
        Combo_Charts_CompareSeriesGroup.Visible = True

        ChartsToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub RadioComparison_IsPertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioComparison_IsPertrac.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (RadioComparison_IsPertrac.Checked) Then
        Combo_Comparisons_Group.Visible = False
        Combo_Compare_Series.Visible = True

        ComparisonToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub RadioComparison_IsGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioComparison_IsGroup.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (RadioComparison_IsGroup.Checked) Then
        Combo_Comparisons_Group.Visible = True
        Combo_Compare_Series.Visible = False

        ComparisonToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub
#End Region

#Region " 'Comparisons' Tab Control Events"

  Private Sub Combo_Compare_Group_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Compare_Group.SelectedIndexChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If InPaint = False Then

      If (Combo_Compare_Group.SelectedIndex <= 0) OrElse (Combo_Compare_Group.SelectedValue Is Nothing) OrElse (Not IsNumeric(Combo_Compare_Group.SelectedValue)) Then
        Exit Sub
      End If

      Dim NewGroupValue As Integer = CInt(Combo_Compare_Group.SelectedValue)

      Try
        Me.Cursor = Cursors.WaitCursor

        If (Radio_ExistingGroups.Checked = False) Then
          Try
            InPaint = True

            Radio_ExistingGroups.Checked = True
            Application.DoEvents()
          Catch ex As Exception
          Finally
            InPaint = False
            UpdateSelectItemsList()
          End Try
        End If

        Try
          Combo_SelectFrom.SelectedValue = NewGroupValue
          Application.DoEvents()
        Catch ex As Exception
        End Try

        Try
          Dim ItemCounter As Integer

          InPaint = True

          For ItemCounter = 0 To (Math.Min(List_SelectItems.Items.Count - 1, 500)) ' Limit Selected Items to 500
            List_SelectItems.SetSelected(ItemCounter, True)
          Next

        Catch ex As Exception
        Finally
          InPaint = False

          Me.List_SelectItems.Refresh()
          List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs())
        End Try
      Catch ex As Exception
      Finally
        Me.Cursor = Cursors.Default
        Combo_Compare_Group.SelectedIndex = 0
      End Try

    End If
  End Sub

  Private Sub Combo_Compare_Series_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Compare_Series.SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ComparisonToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Compare_Series_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_Comparisons_Group_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Comparisons_Group.SelectedIndexChanged   ' 
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ComparisonToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Comparisons_Group_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Comparisons_DateParameter_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
  Handles Date_Compare_DateFrom.ValueChanged, Date_Compare_DateTo.ValueChanged

    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (InPaint = False) Then
        ComparisonToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Comparisons_DateParameter_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Comparisons_Parameter_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
  Handles Edit_Compare_Lamda.ValueChanged, Radio_ListIsReference.CheckedChanged, Radio_ComparisonIsReference.CheckedChanged

    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (InPaint = False) Then
        ComparisonToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Comparisons_Parameter_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Button_Compare_DefaultDates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Compare_DefaultDates.Click
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      Me.Date_Compare_DateFrom.Value = Renaissance_BaseDate
      Me.Date_Compare_DateTo.Value = Renaissance_EndDate_Data
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " 'Statistics' Tab Control Events"

  Private Sub Combo_Statistics_HighlightSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Statistics_HighlightSeries.SelectedIndexChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If (Me.Created) And (InPaint = False) Then
      If (Combo_Statistics_HighlightSeries.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Statistics_HighlightSeries.SelectedValue)) Then
        DrawStatisticsTabGrids(CInt(Combo_Statistics_HighlightSeries.SelectedValue), Text_Statistics_RiskFree.Value)
      Else
        DrawStatisticsTabGrids(0, 0)
      End If
    End If

  End Sub

  Private Sub Text_Statistics_RiskFree_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Text_Statistics_RiskFree.ValueChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If (Me.Created) And (InPaint = False) Then
      If (Combo_Statistics_HighlightSeries.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Statistics_HighlightSeries.SelectedValue)) Then
        DrawStatisticsTabGrids(CInt(Combo_Statistics_HighlightSeries.SelectedValue), Text_Statistics_RiskFree.Value)
      Else
        DrawStatisticsTabGrids(0, 0)
      End If
    End If
  End Sub

  Private Sub DrawStatisticsTabGrids(ByVal pPertracID As Integer, ByVal pRiskFree As Double)
    ' *******************************************************************************
    ' Refresh the Statistics and DrawDown Grids.
    '
    ' *******************************************************************************

    ' 1) Get Simple statistics & DrawDown array.
    '  DrawDownDetails are pre sorted by DrawDown percentage in descending order.

    Dim StatsDatePeriod As DealingPeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(pPertracID), MenuSelectedStatsDataLimit)
    Dim FundStats As StatFunctions.SeriesStatsClass = MainForm.StatFunctions.GetSimpleStats(StatsDatePeriod, CULng(pPertracID), False, pRiskFree, 1.0#)
    Dim DrawDownDetails() As StatFunctions.DrawDownInstanceClass = MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(pPertracID), False, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#)

    ' Paint Stats Grid

    Try
      Grid_SimpleStatistics.Item(1, 1) = FundStats.SimpleReturn.M12
      Grid_SimpleStatistics.Item(1, 2) = FundStats.SimpleReturn.M36
      Grid_SimpleStatistics.Item(1, 3) = FundStats.SimpleReturn.M60
      Grid_SimpleStatistics.Item(1, 4) = FundStats.SimpleReturn.ITD

      Grid_SimpleStatistics.Item(2, 1) = FundStats.AnnualisedReturn.M12
      Grid_SimpleStatistics.Item(2, 2) = FundStats.AnnualisedReturn.M36
      Grid_SimpleStatistics.Item(2, 3) = FundStats.AnnualisedReturn.M60
      Grid_SimpleStatistics.Item(2, 4) = FundStats.AnnualisedReturn.ITD

      Grid_SimpleStatistics.Item(3, 1) = FundStats.Volatility.M12
      Grid_SimpleStatistics.Item(3, 2) = FundStats.Volatility.M36
      Grid_SimpleStatistics.Item(3, 3) = FundStats.Volatility.M60
      Grid_SimpleStatistics.Item(3, 4) = FundStats.Volatility.ITD

      Grid_SimpleStatistics.Item(4, 1) = FundStats.Volatility_Series.M12
      Grid_SimpleStatistics.Item(4, 2) = FundStats.Volatility_Series.M36
      Grid_SimpleStatistics.Item(4, 3) = FundStats.Volatility_Series.M60
      Grid_SimpleStatistics.Item(4, 4) = FundStats.Volatility_Series.ITD

      Grid_SimpleStatistics.Item(5, 1) = FundStats.SharpeRatio.M12
      Grid_SimpleStatistics.Item(5, 2) = FundStats.SharpeRatio.M36
      Grid_SimpleStatistics.Item(5, 3) = FundStats.SharpeRatio.M60
      Grid_SimpleStatistics.Item(5, 4) = FundStats.SharpeRatio.ITD

      Grid_SimpleStatistics.Item(6, 1) = FundStats.PercentPositive.M12
      Grid_SimpleStatistics.Item(6, 2) = FundStats.PercentPositive.M36
      Grid_SimpleStatistics.Item(6, 3) = FundStats.PercentPositive.M60
      Grid_SimpleStatistics.Item(6, 4) = FundStats.PercentPositive.ITD
    Catch ex As Exception
    End Try

    ' Format Stats Grid.

    Dim RowCount As Integer
    Dim ColCount As Integer
    Dim PositiveFormat As String
    Dim NegativeFormat As String

    Try
      For RowCount = 1 To 6
        If RowCount = 5 Then
          PositiveFormat = "SharpeRatio"
          NegativeFormat = "SharpeRatio"
        Else
          PositiveFormat = "PositivePercent"
          NegativeFormat = "NegativePercent"
        End If

        For ColCount = 1 To 4
          If CDbl(Grid_SimpleStatistics.Item(RowCount, ColCount)) < 0 Then
            Grid_SimpleStatistics.SetCellStyle(RowCount, ColCount, NegativeFormat)
          Else
            Grid_SimpleStatistics.SetCellStyle(RowCount, ColCount, PositiveFormat)
          End If
        Next
      Next
    Catch ex As Exception
    End Try

    ' Populate DrawDown Grid :-

    Try
      Me.Grid_Stats_Drawdowns.Rows.Count = DrawDownDetails.Length + 1

      For RowCount = 0 To (DrawDownDetails.Length - 1)
        Grid_Stats_Drawdowns.Item(RowCount + 1, 1) = DrawDownDetails(RowCount).DateFrom
        Grid_Stats_Drawdowns.Item(RowCount + 1, 2) = DrawDownDetails(RowCount).DateTrough
        Grid_Stats_Drawdowns.Item(RowCount + 1, 3) = DrawDownDetails(RowCount).DateTo
        Grid_Stats_Drawdowns.Item(RowCount + 1, 4) = DrawDownDetails(RowCount).DrawDown
        Grid_Stats_Drawdowns.Item(RowCount + 1, 5) = GetPeriodCount(StatsDatePeriod, DrawDownDetails(RowCount).DateFrom, DrawDownDetails(RowCount).DateTrough) & " " & PeriodName(StatsDatePeriod)
        Grid_Stats_Drawdowns.Item(RowCount + 1, 6) = (GetPeriodCount(StatsDatePeriod, DrawDownDetails(RowCount).DateTrough, DrawDownDetails(RowCount).DateTo) - 1).ToString & " " & PeriodName(StatsDatePeriod)
        Grid_Stats_Drawdowns.Item(RowCount + 1, 7) = GetPeriodCount(StatsDatePeriod, DrawDownDetails(RowCount).DateFrom, DrawDownDetails(RowCount).DateTo) & " " & PeriodName(StatsDatePeriod)

        If DrawDownDetails(RowCount).RecoveryOngoing Then
          Grid_Stats_Drawdowns.Item(RowCount + 1, 8) = "and counting"
        Else
          Grid_Stats_Drawdowns.Item(RowCount + 1, 8) = ""
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Charts Menu Events"

  Private Sub ToolStripCombo_Periods_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *********************************************************************************
    ' Event handler for 'Date Period' Menu items.
    '
    '
    ' *********************************************************************************

    If InPaint = False Then
      Try
        Dim thisPeriodID As Integer
        Dim SelectedMenuItem As ToolStripMenuItem

        Dim PeriodDS As RenaissanceDataClass.DSPeriod
        Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
        Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow

        SelectedMenuItem = CType(sender, System.Windows.Forms.ToolStripMenuItem)
        If (SelectedMenuItem IsNot Nothing) AndAlso (IsNumeric(SelectedMenuItem.Tag)) Then
          If (SelectedMenuItem.Text = SelectedMenuItem.Tag.ToString) Then
            ' Year

            Dim SelectedYear As Integer
            Try
              SelectedYear = CInt(SelectedMenuItem.Tag)

              If (SelectedYear >= Renaissance_BaseDate.Year) And (SelectedYear <= Renaissance_EndDate_Data.Year) Then
                ' Charts
                Date_Charts_DateFrom.Value = New Date(SelectedYear - 1, 12, 31)
                Date_Charts_DateTo.Value = New Date(SelectedYear, 12, 31)

                ' Comparisons
                Date_Compare_DateFrom.Value = New Date(SelectedYear - 1, 12, 31)
                Date_Compare_DateTo.Value = New Date(SelectedYear, 12, 31)

                ' Trigger immediate re-paint
                _ChartsToUpdateTime = Now.AddHours(-1)
                _ComparisonToUpdateTime = Now.AddHours(-1)
                Call FormUpdate_Tick()

              End If
            Catch ex As Exception

            End Try
          Else
            ' tblPeriod

            thisPeriodID = CInt(SelectedMenuItem.Tag)

            If (thisPeriodID > 0) Then
              PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
              PeriodTbl = PeriodDS.tblPeriod
              SelectedRows = PeriodTbl.Select("PeriodID=" & thisPeriodID.ToString)

              If (SelectedRows.Length > 0) Then
                Date_Charts_DateFrom.Value = SelectedRows(0).PeriodDateFrom
                Date_Charts_DateTo.Value = SelectedRows(0).PeriodDateTo

                ' Comparisons
                Date_Compare_DateFrom.Value = SelectedRows(0).PeriodDateFrom
                Date_Compare_DateTo.Value = SelectedRows(0).PeriodDateTo

                ' Trigger immediate re-paint
                _ChartsToUpdateTime = Now.AddHours(-1)
                _ComparisonToUpdateTime = Now.AddHours(-1)
                Call FormUpdate_Tick()

              End If
            End If
          End If
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", ToolStripCombo_Periods_Click()", LOG_LEVELS.Error, ex.Message, "Error imposing Period Dates", ex.StackTrace, True)
      End Try

    End If
  End Sub

#End Region

#Region " Chart Paint Functions"

  Private Sub PaintAllCharts()
    ' *********************************************************************************
    ' Does what it says.
    '
    ' Calls all of the chart management routines to ensure all charts are updated.
    ' *********************************************************************************

    If (Me.Created) And (Not Me.Disposing) And (Not Me.IsDisposed) Then

      PaintVAMICharts()

      PaintMonthlyCharts()

      PaintRollingReturnCharts()

      PaintReturnScatterCharts()

      PaintStdDevCharts()

      PaintVARCharts()

      PaintOmegaCharts()

      PaintDrawdownCharts()

      PaintCorrelationCharts()

      PaintAlphaCharts()

      PaintBetaCharts()

      PaintQuartileCharts()

      PaintRankingCharts()

    End If

  End Sub

  Private Sub PaintVAMICharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.VAMI, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_LineChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock PriceChartArrayList
        For Each thisChart In Me.PriceChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.VAMI, thisChart, ComparisonSeries, Nz(Combo_Chart_Conditional.SelectedValue, StatFunctions.ContingentSelect.ConditionAll), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries > 0) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_LineChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintMonthlyCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.MonthlyReturns, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_ReturnsBarChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock ReturnsChartArrayList
        For Each thisChart In Me.ReturnsChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.MonthlyReturns, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            If (ComparisonSeries > 0) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_ReturnsBarChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock
    End If

  End Sub

  Private Sub PaintRollingReturnCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.RollingReturn, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_RollingReturnChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RollingReturnChartArrayList
        For Each thisChart In Me.RollingReturnChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.RollingReturn, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_RollingReturnChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintReturnScatterCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.ReturnScatter, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock ReturnScatterChartArrayList
        For Each thisChart In Me.ReturnScatterChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.ReturnScatter, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintStdDevCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    Dim thisChart As Dundas.Charting.WinControl.Chart

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.StdDev, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_StdDevChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else

      SyncLock StdDevChartArrayList
        For Each thisChart In Me.StdDevChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.StdDev, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            If (ComparisonSeries > 0) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_StdDevChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintVARCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then
      If (IsNumeric(Combo_Charts_VARSeries.SelectedValue)) Then
        StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(CInt(Combo_Charts_VARSeries.SelectedValue)), Me.MenuSelectedStatsDataLimit)
        Set_VARChart(MainForm, StatsDatePeriod, CInt(Combo_Charts_VARSeries.SelectedValue), pThisChartOnly, Combo_Charts_VARSeries.Text, Text_Chart_VARConfidence.Value, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_VARPeriod.Value, Text_Chart_Lamda.Value, GetScalingFactor(MainForm, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, CULng(Combo_Charts_VARSeries.SelectedValue), CULng(ComparisonSeries), StatsDatePeriod), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value) ' Text_ScalingFactor.Value
      End If

    Else

      Dim thisChart As Object

      SyncLock VARChartArrayList

        For Each thisChart In Me.VARChartArrayList

          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            If (Combo_Charts_VARSeries.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Charts_VARSeries.SelectedValue)) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(CInt(Combo_Charts_VARSeries.SelectedValue)), Me.MenuSelectedStatsDataLimit)
              Set_VARChart(MainForm, StatsDatePeriod, CInt(Combo_Charts_VARSeries.SelectedValue), thisChart, Combo_Charts_VARSeries.Text, Text_Chart_VARConfidence.Value, MainForm.StatFunctions.GetPeriodsFromMonth(StatsDatePeriod, Text_Chart_RollingPeriod.Value), Text_Chart_VARPeriod.Value, Text_Chart_Lamda.Value, GetScalingFactor(MainForm, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, CULng(Combo_Charts_VARSeries.SelectedValue), CULng(ComparisonSeries), StatsDatePeriod), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If

        Next

      End SyncLock

    End If

  End Sub

  Private Sub PaintOmegaCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Omega, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, Check_OmegaRatio.Checked)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_OmegaChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked)
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock OmegaChartArrayList
        For Each thisChart In Me.OmegaChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Omega, thisChart, ComparisonSeries, Nz(Combo_Chart_Conditional.SelectedValue, StatFunctions.ContingentSelect.ConditionAll), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, Check_OmegaRatio.Checked)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries > 0) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_OmegaChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintDrawdownCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""
    Dim StatsDatePeriod As DealingPeriod

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.DrawDown, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
          Set_DrawDownChart(MainForm, StatsDatePeriod, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(ComparisonSeries, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock DrawdownChartArrayList
        For Each thisChart In Me.DrawdownChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.DrawDown, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries > 0) Then
              StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(ComparisonSeries), Me.MenuSelectedStatsDataLimit)
              Set_DrawDownChart(MainForm, StatsDatePeriod, ComparisonSeries, thisChart, -1, ComparisonSeriesText, MainForm.StatFunctions.GetPeriodsFromMonth(ComparisonSeries, Text_Chart_RollingPeriod.Value), Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintCorrelationCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Correlation, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock CorrelationChartArrayList
        For Each thisChart In Me.CorrelationChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Correlation, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintAlphaCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Alpha, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock AlphaChartArrayList
        For Each thisChart In Me.AlphaChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Alpha, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintBetaCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Beta, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock BetaChartArrayList
        For Each thisChart In Me.BetaChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Beta, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintQuartileCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DataSeriesToUse As GenoaQuartileDisplayData = GenoaQuartileDisplayData.MonthlyReturns
    Dim HighlightID As Integer = 0
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (Combo_Quartile_HighlightSeries.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_Quartile_HighlightSeries.SelectedValue)) Then
      HighlightID = CInt(Combo_Quartile_HighlightSeries.SelectedValue)
    End If

    If Me.Combo_Quartile_ChartData.SelectedIndex >= 0 Then
      Try
        If IsNumeric(Combo_Quartile_ChartData.SelectedValue) Then
          DataSeriesToUse = CType(Combo_Quartile_ChartData.SelectedValue, GenoaQuartileDisplayData)
        End If
      Catch ex As Exception
      End Try
    End If

    If (pThisChartOnly IsNot Nothing) Then

      ' Resolve Parameters

      Dim pHighlightIndex As Object = HighlightID
      Dim pStartDate As Date = Date_Charts_DateFrom.Value
      Dim pEndDate As Date = Date_Charts_DateTo.Value
      Dim pMonths As Double = Text_Chart_RollingPeriod.Value
      Dim pLamda As Double = Text_Chart_Lamda.Value
      Dim pDynamicScalingFactor As Boolean = Radio_DynamicScalingFactor.Checked
      Dim pScalingFactor As Double = Text_ScalingFactor.Value
      Dim pReferenceScalingFactor As Double = 1.0#

      Dim ParameterDetail() As String
      If (ParameterList IsNot Nothing) AndAlso (ParameterList.Length > 0) Then
        For Each ThisString As String In ParameterList
          ParameterDetail = ThisString.Split(New Char() {"="})

          If (ParameterDetail IsNot Nothing) AndAlso (ParameterDetail.Length = 2) Then
            Select Case ParameterDetail(0).ToUpper

              Case "CHARTDATA"

                If IsNumeric(ParameterDetail(1)) Then
                  DataSeriesToUse = CType(CInt(ParameterDetail(1)), GenoaQuartileDisplayData)
                End If

            End Select
          End If
        Next
      End If

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' If This Sub was called with a Null Parameter list, and
        ' The given chart is from a ViewChart form with an attached Parameter list, then call this Sub
        ' recursively with the given Parameters.

        If (ParameterList Is Nothing) AndAlso (ThisChartOnly.Tag IsNot Nothing) AndAlso (TypeOf ThisChartOnly.Tag Is frmViewChart) AndAlso (CType(ThisChartOnly.Tag, frmViewChart).ParameterList IsNot Nothing) Then

          PaintQuartileCharts(ThisChartOnly, CType(ThisChartOnly.Tag, frmViewChart).ParameterList)

        Else
          ' Otherwise, just paint the chart.

          Try
            Set_CandleChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Quartile, ThisChartOnly, ComparisonSeries, pHighlightIndex, DataSeriesToUse, pStartDate, pEndDate, pMonths, pLamda, MenuSelectedStatsDataLimit, pDynamicScalingFactor, pScalingFactor, 1.0#, CInt(Numeric_QuantileOutlier.Value))
          Catch ex As Exception
          End Try
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock QuartileChartArrayList
        For Each thisChart In Me.QuartileChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              If (ParameterList Is Nothing) AndAlso (thisChart.Tag IsNot Nothing) AndAlso (TypeOf thisChart.Tag Is frmViewChart) AndAlso (CType(thisChart.Tag, frmViewChart).ParameterList IsNot Nothing) Then

                PaintQuartileCharts(thisChart, CType(thisChart.Tag, frmViewChart).ParameterList)

              Else

                Set_CandleChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Quartile, thisChart, ComparisonSeries, HighlightID, DataSeriesToUse, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, CInt(Numeric_QuantileOutlier.Value))

              End If

            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintRankingCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DataSeriesToUse As GenoaQuartileDisplayData = GenoaQuartileDisplayData.MonthlyReturns
    Dim HighlightID As Integer = 0
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try


    If (Combo_Ranking_HighlightSeries.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_Ranking_HighlightSeries.SelectedValue)) Then
      HighlightID = CInt(Combo_Ranking_HighlightSeries.SelectedValue)
    End If

    If Me.Combo_Ranking_ChartData.SelectedIndex >= 0 Then
      Try
        If IsNumeric(Combo_Ranking_ChartData.SelectedValue) Then
          DataSeriesToUse = CType(Combo_Ranking_ChartData.SelectedValue, GenoaQuartileDisplayData)
        End If
      Catch ex As Exception
      End Try
    End If

    If (pThisChartOnly IsNot Nothing) Then
      Dim pHighlightIndex As Object = HighlightID
      Dim pStartDate As Date = Date_Charts_DateFrom.Value
      Dim pEndDate As Date = Date_Charts_DateTo.Value
      Dim pMonths As Double = Text_Chart_RollingPeriod.Value
      Dim pLamda As Double = Text_Chart_Lamda.Value
      Dim pDynamicScalingFactor As Boolean = Radio_DynamicScalingFactor.Checked
      Dim pScalingFactor As Double = Text_ScalingFactor.Value
      Dim pReferenceScalingFactor As Double = 1.0#

      Dim ParameterDetail() As String
      If (ParameterList IsNot Nothing) AndAlso (ParameterList.Length > 0) Then
        For Each ThisString As String In ParameterList
          ParameterDetail = ThisString.Split(New Char() {"="})

          If (ParameterDetail IsNot Nothing) AndAlso (ParameterDetail.Length = 2) Then
            Select Case ParameterDetail(0).ToUpper

              Case "CHARTDATA"

                If IsNumeric(ParameterDetail(1)) Then
                  DataSeriesToUse = CType(CInt(ParameterDetail(1)), GenoaQuartileDisplayData)
                End If

            End Select
          End If
        Next
      End If

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' If This Sub was called with a Null Parameter list, and
        ' The given chart is from a ViewChart form with an attached Parameter list, then call this Sub
        ' recursively with the given Parameters.

        If (ParameterList Is Nothing) AndAlso (ThisChartOnly.Tag IsNot Nothing) AndAlso (TypeOf ThisChartOnly.Tag Is frmViewChart) AndAlso (CType(ThisChartOnly.Tag, frmViewChart).ParameterList IsNot Nothing) Then

          PaintRankingCharts(ThisChartOnly, CType(ThisChartOnly.Tag, frmViewChart).ParameterList)

        Else
          ' Otherwise, just paint the chart.

          Try
            Set_CandleChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Ranking, ThisChartOnly, ComparisonSeries, pHighlightIndex, DataSeriesToUse, pStartDate, pEndDate, pMonths, pLamda, MenuSelectedStatsDataLimit, pDynamicScalingFactor, pScalingFactor, 1.0#, 0)
          Catch ex As Exception
          End Try
        End If

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RankingChartArrayList
        For Each thisChart In Me.RankingChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            If (ParameterList Is Nothing) AndAlso (thisChart.Tag IsNot Nothing) AndAlso (TypeOf thisChart.Tag Is frmViewChart) AndAlso (CType(thisChart.Tag, frmViewChart).ParameterList IsNot Nothing) Then

              PaintRankingCharts(thisChart, CType(thisChart.Tag, frmViewChart).ParameterList)

            Else

              Set_CandleChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Ranking, thisChart, ComparisonSeries, HighlightID, DataSeriesToUse, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, MenuSelectedStatsDataLimit, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, 0)

            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub Chart_Prices_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Prices.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintVAMICharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Returns_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Returns.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintMonthlyCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_RollingReturn_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_RollingReturn.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintRollingReturnCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_ReturnScatter_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_ReturnScatter.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintReturnScatterCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_StdDev_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_StdDev.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintStdDevCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub C1Chart_VAR_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As C1.Win.C1Chart.C1Chart

      If (TypeOf sender Is C1.Win.C1Chart.C1Chart) Then
        thisChart = CType(sender, C1.Win.C1Chart.C1Chart)

        If thisChart.Visible Then
          PaintVARCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_VAR_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_VAR.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintVARCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Omega_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Omega.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintOmegaCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_DrawDown_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_DrawDown.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintDrawdownCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Correlation_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Correlation.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintCorrelationCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Alpha_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Alpha.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintAlphaCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Beta_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Beta.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintBetaCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Quartile_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Quartile.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintQuartileCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Ranking_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Ranking.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintRankingCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Comp_Statistics_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Comp_Statistics.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Grid_Comp_Statistics.Visible) Then
      SetComparisonStatsGrid(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_Correlation_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Correlation.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_Correlation.Visible) Then
      Call SetComparisonCorrelationChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_CorrelationUpDown_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_CorrelationUpDown.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_CorrelationUpDown.Visible) Then
      Call SetComparisonCorrelationUpDownChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_Beta_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Beta.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_Beta.Visible) Then
      Call SetComparisonBetaChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_BetaUpDown_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_BetaUpDown.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_BetaUpDown.Visible) Then
      Call SetComparisonBetaUpDownChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_Alpha_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Alpha.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_Alpha.Visible) Then
      Call SetComparisonAlphaChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_APR_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_APR.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_APR.Visible) Then
      Call SetComparisonAPRChart(sender)
    End If

  End Sub

  Private Sub Chart_Comparison_Volatility_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Volatility.VisibleChanged
    ' *******************************************************************************
    '
    '
    '
    ' *******************************************************************************

    If (Me.Chart_Comparison_Volatility.Visible) Then
      Call SetComparisonVolatilityChart(sender)
    End If

  End Sub

#End Region

#Region " Comparison Stats & Charts Funtions"

  Private Sub PaintAllComparison()

    ' *********************************************************************************
    ' Does what it says.
    '
    ' Calls all of the chart management routines to ensure all charts are updated.
    ' *********************************************************************************

    If (Me.Created) And (Not Me.Disposing) And (Not Me.IsDisposed) Then

      SetComparisonStatsGrid()

      SetComparisonCorrelationChart()

      SetComparisonCorrelationUpDownChart()

      SetComparisonBetaChart()

      SetComparisonBetaUpDownChart()

      SetComparisonAlphaChart()

      SetComparisonAPRChart()

      SetComparisonVolatilityChart()

    End If

  End Sub

  Private Sub SetComparisonStatsGrid(Optional ByVal pGridToPaint As C1.Win.C1FlexGrid.C1FlexGrid = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' **************************************************************************************
    '
    '
    '
    ' **************************************************************************************
    Dim GridCounter As Integer
    Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim StatsDatePeriod As DealingPeriod

    If (Me.Disposing) Then
      Exit Sub
    End If

    For GridCounter = 0 To (ComparisonStatsGridList.Count - 1)
      Try
        thisGrid = ComparisonStatsGridList(GridCounter)

        If (thisGrid.Visible) Then

          If (pGridToPaint Is Nothing) OrElse (pGridToPaint Is thisGrid) Then
            If (thisGrid.Visible) Then
              Dim InstrumentCounter As Integer
              Dim GridRow As Integer

              Dim ListPertracID As Integer
              Dim PertracName As String
              Dim PertracID As Integer = 0
              Dim ReferenceID As Integer = 0

              Dim thisStatObject As StatFunctions.ComparisonStatsClass

              ' Get Column IDs

              Dim Col_Name As Integer = thisGrid.Cols.IndexOf("Col_Name")
              Dim Col_Correlation As Integer = thisGrid.Cols.IndexOf("Col_Correlation")
              Dim Col_CorrelationUp As Integer = thisGrid.Cols.IndexOf("Col_CorrelationUp")
              Dim Col_CorrelationDown As Integer = thisGrid.Cols.IndexOf("Col_CorrelationDown")
              Dim Col_Beta As Integer = thisGrid.Cols.IndexOf("Col_Beta")
              Dim Col_BetaUp As Integer = thisGrid.Cols.IndexOf("Col_BetaUp")
              Dim Col_BetaDown As Integer = thisGrid.Cols.IndexOf("Col_BetaDown")
              Dim Col_Alpha As Integer = thisGrid.Cols.IndexOf("Col_Alpha")
              Dim Col_APR As Integer = thisGrid.Cols.IndexOf("Col_APR")
              Dim Col_Volatility As Integer = thisGrid.Cols.IndexOf("Col_Volatility")
              Dim Col_PertracID As Integer = thisGrid.Cols.IndexOf("Col_PertracID")
              Dim Col_ReferenceID As Integer = thisGrid.Cols.IndexOf("Col_ReferenceID")

              ' Set Reference ID

              If (RadioComparison_IsPertrac.Checked) Then
                If (IsNumeric(Combo_Compare_Series.SelectedValue) = False) Then
                  PertracID = 0
                  ReferenceID = 0
                Else
                  PertracID = CInt(Combo_Compare_Series.SelectedValue)
                  ReferenceID = CInt(Combo_Compare_Series.SelectedValue)
                End If
              Else
                If (IsNumeric(Combo_Comparisons_Group.SelectedValue) = False) Then
                  PertracID = 0
                  ReferenceID = 0
                Else
                  PertracID = CInt(Combo_Comparisons_Group.SelectedValue)
                  ReferenceID = CInt(Combo_Comparisons_Group.SelectedValue)
                End If
              End If

              thisGrid.Rows.Count = List_SelectItems.SelectedItems.Count + 1

              For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
                GridRow = InstrumentCounter + 1
                Try
                  ListPertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
                  PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), ""))
                Catch ex As Exception
                  ListPertracID = 0
                  PertracName = "<Error>"
                End Try

                If (Radio_ListIsReference.Checked) Then
                  ReferenceID = ListPertracID
                Else
                  PertracID = ListPertracID
                End If

                ' thisStatObject = MainForm.StatFunctions.GetComparisonStatsItem(CULng(PertracID), CULng(ReferenceID), False, ListPertracID, Renaissance_BaseDate, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value, Edit_Compare_Period.Value)
                StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(PertracID, ReferenceID), Me.MenuSelectedStatsDataLimit)
                thisStatObject = MainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CULng(PertracID), CULng(ReferenceID), False, ListPertracID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value, 999, 1000)

                thisGrid.Item(GridRow, Col_Name) = PertracName
                thisGrid.Item(GridRow, Col_PertracID) = PertracID
                thisGrid.Item(GridRow, Col_ReferenceID) = ReferenceID

                thisGrid.Item(GridRow, Col_Correlation) = thisStatObject.Correlation
                If (thisStatObject.Correlation < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_Correlation, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_Correlation, "Positive")
                End If

                thisGrid.Item(GridRow, Col_CorrelationUp) = thisStatObject.CorrelationUpOnly
                If (thisStatObject.CorrelationUpOnly < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_CorrelationUp, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_CorrelationUp, "Positive")
                End If

                thisGrid.Item(GridRow, Col_CorrelationDown) = thisStatObject.CorrelationDownOnly
                If (thisStatObject.CorrelationDownOnly < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_CorrelationDown, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_CorrelationDown, "Positive")
                End If

                thisGrid.Item(GridRow, Col_Beta) = thisStatObject.Beta(ListPertracID)
                If (thisStatObject.Beta(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_Beta, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_Beta, "Positive")
                End If

                thisGrid.Item(GridRow, Col_BetaUp) = thisStatObject.BetaUpOnly(ListPertracID)
                If (thisStatObject.BetaUpOnly(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_BetaUp, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_BetaUp, "Positive")
                End If

                thisGrid.Item(GridRow, Col_BetaDown) = thisStatObject.BetaDownOnly(ListPertracID)
                If (thisStatObject.BetaDownOnly(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_BetaDown, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_BetaDown, "Positive")
                End If

                thisGrid.Item(GridRow, Col_Alpha) = thisStatObject.Alpha(ListPertracID)
                If (thisStatObject.Alpha(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_Alpha, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_Alpha, "Positive")
                End If

                thisGrid.Item(GridRow, Col_APR) = thisStatObject.APR(ListPertracID)
                If (thisStatObject.APR(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_APR, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_APR, "Positive")
                End If

                thisGrid.Item(GridRow, Col_Volatility) = thisStatObject.Volatility(ListPertracID)
                If (thisStatObject.Volatility(ListPertracID) < 0) Then
                  thisGrid.SetCellStyle(GridRow, Col_Volatility, "Negative")
                Else
                  thisGrid.SetCellStyle(GridRow, Col_Volatility, "Positive")
                End If

              Next

            End If

          End If
        End If

      Catch ex As Exception
      End Try
    Next

  End Sub

  Private Sub SetComparisonCorrelationChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Correlation, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock CorrelationBarArrayList
          For Each thisChart In Me.CorrelationBarArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Correlation, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try


  End Sub

  Private Sub SetComparisonCorrelationUpDownChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim CompareSeriesID As Object
    If (RadioComparison_IsPertrac.Checked) Then
      CompareSeriesID = Combo_Compare_Series.SelectedValue
    Else
      CompareSeriesID = Combo_Comparisons_Group.SelectedValue
    End If

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_CorrelationUpDown, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock Me.CorrelationUpDownBarArrayList
        For Each thisChart In Me.CorrelationUpDownBarArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_CorrelationUpDown, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub SetComparisonBetaChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Beta, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock Me.BetaBarArrayList
          For Each thisChart In BetaBarArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Beta, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try


  End Sub

  Private Sub SetComparisonBetaUpDownChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_BetaupDown, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock Me.BetaBarUpDownArrayList
          For Each thisChart In BetaBarUpDownArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_BetaupDown, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetComparisonAlphaChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Alpha, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock Me.AlphaBarArrayList
          For Each thisChart In AlphaBarArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Alpha, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try


  End Sub

  Private Sub SetComparisonAPRChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_APR, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock Me.APRBarArrayList
          For Each thisChart In APRBarArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_APR, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetComparisonVolatilityChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      Dim CompareSeriesID As Object
      If (RadioComparison_IsPertrac.Checked) Then
        CompareSeriesID = Combo_Compare_Series.SelectedValue
      Else
        CompareSeriesID = Combo_Comparisons_Group.SelectedValue
      End If

      If (pThisChartOnly IsNot Nothing) Then

        If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

          Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

          Try
            Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Volatility, ThisChartOnly, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)
          Catch ex As Exception
          End Try

        End If

      Else
        Dim thisChart As Dundas.Charting.WinControl.Chart

        SyncLock VolatilityBarArrayList
          For Each thisChart In VolatilityBarArrayList
            If (thisChart.Visible) Then
              ' Set Prices Chart Series Count

              Set_ComparisonChart_FromList(MainForm, List_SelectItems, GenoaChartTypes.Compare_Volatility, thisChart, Radio_ListIsReference.Checked, CompareSeriesID, Date_Compare_DateFrom.Value, Date_Compare_DateTo.Value, Edit_Compare_Lamda.Value)

            End If
          Next
        End SyncLock

      End If

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Performance Chart creation"

  Private Sub SetPerformanceDataGrids()
    ' *********************************************************************************************************
    ' Code to set Monthly Performance Grid.
    '
    ' Note MONTHLY.
    '
    ' *********************************************************************************************************

    Dim TabCounter As Integer
    Dim InstrumentCounter As Integer
    Dim PertracID As Integer
    Dim PertracName As String

    Dim thisTab As TabPage
    Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim thislabel As Label

    ' Initialise Tab Control
    If Me.TabControl_PerformanceTables.TabCount < List_SelectItems.SelectedItems.Count Then
      Dim FirstTab As TabPage
      Dim FirstGrid As C1.Win.C1FlexGrid.C1FlexGrid
      Dim Firstlabel As Label

      FirstTab = TabControl_PerformanceTables.TabPages(0)
      FirstGrid = Me.Grid_Performance_0
      Firstlabel = Me.Label_PT_0

      For TabCounter = TabControl_PerformanceTables.TabCount To (List_SelectItems.SelectedItems.Count - 1)
        TabControl_PerformanceTables.TabPages.Add("TabPage" & (TabCounter + 1).ToString, "Stock" & (TabCounter + 1).ToString)

        thisTab = TabControl_PerformanceTables.TabPages(TabControl_PerformanceTables.TabCount - 1)

        thisTab.AutoScroll = FirstTab.AutoScroll
        thisTab.Location = New System.Drawing.Point(FirstTab.Left, FirstTab.Top)
        thisTab.Padding = New System.Windows.Forms.Padding(FirstTab.Padding.Top)
        thisTab.Size = New System.Drawing.Size(FirstTab.Width, FirstTab.Height)
        thisTab.UseVisualStyleBackColor = FirstTab.UseVisualStyleBackColor

        thislabel = New Label()
        thislabel.BackColor = System.Drawing.SystemColors.Window
        thislabel.BorderStyle = Firstlabel.BorderStyle
        thislabel.Location = New System.Drawing.Point(Firstlabel.Left, Firstlabel.Top)
        thislabel.Name = "Label_PT_" & TabCounter.ToString
        thislabel.Size = New System.Drawing.Size(Firstlabel.Width, Firstlabel.Height)
        thislabel.Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right
        thislabel.Text = "<none>"

        thisGrid = New C1.Win.C1FlexGrid.C1FlexGrid
        Try
          thisGrid.BeginInit()

          thisGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
           Or System.Windows.Forms.AnchorStyles.Left) _
           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
          thisGrid.Cursor = System.Windows.Forms.Cursors.Default
          thisGrid.Location = New System.Drawing.Point(FirstGrid.Left, FirstGrid.Top)
          thisGrid.Name = "Grid_Performance_" & TabCounter.ToString
          thisGrid.Rows.DefaultSize = FirstGrid.Rows.DefaultSize
          thisGrid.Size = New System.Drawing.Size(FirstGrid.Width, FirstGrid.Height)
          thisGrid.TabIndex = 0
          thisGrid.Cols.Count = FirstGrid.Cols.Count
          thisGrid.Rows.Count = 2
          thisGrid.AutoClipboard = False
          '  Private Sub Grid_Performance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Performance_0.KeyDown

          AddHandler thisGrid.KeyDown, AddressOf Grid_Performance_KeyDown
          thisGrid.ContextMenuStrip = New ContextMenuStrip
          thisGrid.ContextMenuStrip.Tag = thisGrid
          AddHandler thisGrid.ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
          thisGrid.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

          Try
            Dim ColCounter As Integer

            For ColCounter = 0 To (FirstGrid.Cols.Count - 1)
              thisGrid.Cols(ColCounter).AllowDragging = FirstGrid.Cols(ColCounter).AllowDragging
              thisGrid.Cols(ColCounter).AllowEditing = FirstGrid.Cols(ColCounter).AllowEditing
              thisGrid.Cols(ColCounter).AllowResizing = FirstGrid.Cols(ColCounter).AllowResizing
              thisGrid.Cols(ColCounter).AllowSorting = FirstGrid.Cols(ColCounter).AllowSorting
              thisGrid.Cols(ColCounter).Caption = FirstGrid.Cols(ColCounter).Caption
              thisGrid.Cols(ColCounter).DataType = FirstGrid.Cols(ColCounter).DataType
              thisGrid.Cols(ColCounter).Format = FirstGrid.Cols(ColCounter).Format
              thisGrid.Cols(ColCounter).TextAlign = FirstGrid.Cols(ColCounter).TextAlign
              thisGrid.Cols(ColCounter).Visible = FirstGrid.Cols(ColCounter).Visible
              thisGrid.Cols(ColCounter).Width = FirstGrid.Cols(ColCounter).Width
            Next
          Catch ex As Exception
          End Try

          thisGrid.Styles.Add("Positive").ForeColor = Color.Blue
          thisGrid.Styles.Add("Negative").ForeColor = Color.Red
          thisGrid.Styles.Add("TopRow", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
          thisGrid.Styles.Add("FirstCell", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter

          thisGrid.Rows(0).Style = Grid_Performance_0.Styles("TopRow")
          thisGrid.SetCellStyle(0, 0, "FirstCell")

        Catch ex As Exception
        Finally
          thisGrid.EndInit()
        End Try

        thisTab.Controls.Add(thislabel)
        thisTab.Controls.Add(thisGrid)

      Next
    End If

    ' Discard Unused TabPages, it does not appear possible just to make them invisible

    If Me.TabControl_PerformanceTables.TabCount > List_SelectItems.SelectedItems.Count Then
      For TabCounter = (TabControl_PerformanceTables.TabCount - 1) To List_SelectItems.SelectedItems.Count Step -1
        If (TabCounter > 0) Then
          thisTab = TabControl_PerformanceTables.TabPages(TabCounter)
          TabControl_PerformanceTables.TabPages.RemoveAt(TabCounter)
          Try
            RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).KeyDown, AddressOf Grid_Performance_KeyDown
            RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
          Catch ex As Exception
          End Try
          Try
            CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Tag = Nothing
            CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip = Nothing
          Catch ex As Exception
          End Try

          Try
            thisTab.Controls.Clear()
            thisTab.Dispose()
          Catch ex As Exception
          End Try
        End If
      Next
    End If

    ' Show all remaining TabPages

    For TabCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
      TabControl_PerformanceTables.TabPages(TabCounter).Show()
    Next

    ' Set Label and Chart Data

    Dim InstrumentDates() As Date
    Dim InstrumentReturns() As Double

    For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
      Try
        PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
        PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
      Catch ex As Exception
        PertracID = 0
        PertracName = "<Error>"
      End Try

      Try
        MainForm.GenoaStatusLabel.Text = "Loading " & PertracName
        MainForm.GenoaStatusStrip.Refresh()
      Catch ex As Exception
      End Try

      'Dim thisPertracData As DataTable
      'Dim SortedRows() As DataRow
      'Dim PerformanceDateOrdinal As Integer
      'Dim ReturnOrdinal As Integer
      Dim StartYear As Integer
      Dim EndYear As Integer
      'Dim thisRow As DataRow
      Dim ThisDate As Date
      Dim ThisReturn As Double
      Dim RowCounter As Integer
      Dim ColCounter As Integer
      Dim YTD_Sum As Double

      Try
        thisTab = TabControl_PerformanceTables.TabPages(InstrumentCounter)
        thislabel = thisTab.Controls("Label_PT_" & InstrumentCounter.ToString)
        thisGrid = thisTab.Controls("Grid_Performance_" & InstrumentCounter.ToString)

        If (thisGrid IsNot Nothing) Then
          thisGrid.Tag = Math.Max(PertracID, 0)
        End If

        If (PertracID > 0) Then
          thisGrid.Rows.Count = 1
          thisTab.Text = Trim(PertracID.ToString & ", " & Trim(PertracName.PadRight(20).Substring(0, 20)))
          thisTab.ToolTipText = PertracName
          thislabel.Text = PertracName

          InstrumentDates = MainForm.StatFunctions.DateSeries(DealingPeriod.Monthly, CULng(PertracID), False, MainForm.StatFunctions.AnnualPeriodCount(DealingPeriod.Monthly), 1.0#, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
          InstrumentReturns = MainForm.StatFunctions.ReturnSeries(DealingPeriod.Monthly, CULng(PertracID), False, MainForm.StatFunctions.AnnualPeriodCount(DealingPeriod.Monthly), 1.0#, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

          If (InstrumentDates IsNot Nothing) AndAlso (InstrumentDates.Length > 0) Then

            StartYear = InstrumentDates(0).Year
            EndYear = InstrumentDates(InstrumentDates.Length - 1).Year

            thisGrid.Rows.Count = (EndYear - StartYear) + 2

            For DateIndex As Integer = 0 To (InstrumentDates.Length - 1)
              ThisDate = InstrumentDates(DateIndex)
              ThisReturn = InstrumentReturns(DateIndex)
              thisGrid.Item((ThisDate.Year - StartYear) + 1, ThisDate.Month) = ThisReturn

              If (ThisReturn < 0) Then
                thisGrid.SetCellStyle((ThisDate.Year - StartYear) + 1, ThisDate.Month, "Negative")
              Else
                thisGrid.SetCellStyle((ThisDate.Year - StartYear) + 1, ThisDate.Month, "Positive")
              End If
            Next

            ' Set YTD
            For RowCounter = 1 To (thisGrid.Rows.Count - 1)
              YTD_Sum = 1
              thisGrid.Item(RowCounter, 0) = (StartYear + RowCounter) - 1

              For ColCounter = 1 To 12
                Try
                  YTD_Sum *= (1.0 + CDbl(thisGrid.Item(RowCounter, ColCounter)))
                Catch ex As Exception
                End Try
              Next

              thisGrid.Item(RowCounter, 13) = (YTD_Sum - 1)
              If ((YTD_Sum - 1) < 0) Then
                thisGrid.SetCellStyle(RowCounter, 13, "Negative")
              Else
                thisGrid.SetCellStyle(RowCounter, 13, "Positive")
              End If
            Next

          End If

          'thisPertracData = MainForm.PertracData.GetPertracTable(DealingPeriod.Monthly, PertracID)
          'If (thisPertracData IsNot Nothing) AndAlso (thisPertracData.Rows.Count > 0) Then
          '  PerformanceDateOrdinal = thisPertracData.Columns.IndexOf("PerformanceDate")
          '  ReturnOrdinal = thisPertracData.Columns.IndexOf("PerformanceReturn")

          '  SortedRows = thisPertracData.Select("True", "PerformanceDate")
          '  StartYear = CDate(SortedRows(0)(PerformanceDateOrdinal)).Year

          '  thisGrid.Rows.Count = (CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)).Year - StartYear) + 2

          '  For Each thisRow In SortedRows
          '    ThisReturn = CDbl(thisRow(ReturnOrdinal))
          '    thisGrid.Item((CDate(thisRow(PerformanceDateOrdinal)).Year - StartYear) + 1, CDate(thisRow(PerformanceDateOrdinal)).Month) = ThisReturn

          '    If (ThisReturn < 0) Then
          '      thisGrid.SetCellStyle((CDate(thisRow(PerformanceDateOrdinal)).Year - StartYear) + 1, CDate(thisRow(PerformanceDateOrdinal)).Month, "Negative")
          '    Else
          '      thisGrid.SetCellStyle((CDate(thisRow(PerformanceDateOrdinal)).Year - StartYear) + 1, CDate(thisRow(PerformanceDateOrdinal)).Month, "Positive")
          '    End If
          '  Next

          '  ' Set YTD
          '  For RowCounter = 1 To (thisGrid.Rows.Count - 1)
          '    YTD_Sum = 1
          '    thisGrid.Item(RowCounter, 0) = (StartYear + RowCounter) - 1

          '    For ColCounter = 1 To 12
          '      Try
          '        YTD_Sum *= (1.0 + CDbl(thisGrid.Item(RowCounter, ColCounter)))
          '      Catch ex As Exception
          '      End Try
          '    Next

          '    thisGrid.Item(RowCounter, 13) = (YTD_Sum - 1)
          '    If ((YTD_Sum - 1) < 0) Then
          '      thisGrid.SetCellStyle(RowCounter, 13, "Negative")
          '    Else
          '      thisGrid.SetCellStyle(RowCounter, 13, "Positive")
          '    End If
          '  Next
          'End If
        Else
          thislabel.Text = ""
          thisGrid.Rows.Count = 1
          thisTab.Hide()
        End If
      Catch ex As Exception
      End Try
    Next

    MainForm.GenoaStatusLabel.Text = ""
    MainForm.GenoaStatusStrip.Refresh()

  End Sub

#End Region

#Region " Chart DoubleClick Events"

  Private Sub Chart_Prices_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Prices.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVAMICharts
    newChartForm.FormChart = Chart_Prices
    newChartForm.ParentChartList = PriceChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock PriceChartArrayList
      PriceChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Returns_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Returns.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintMonthlyCharts
    newChartForm.FormChart = Chart_Returns
    newChartForm.ParentChartList = ReturnsChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock ReturnsChartArrayList
      ReturnsChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_RollingReturn_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_RollingReturn.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintRollingReturnCharts
    newChartForm.FormChart = Chart_RollingReturn
    newChartForm.ParentChartList = RollingReturnChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock RollingReturnChartArrayList
      RollingReturnChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_ReturnScatter_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_ReturnScatter.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintReturnScatterCharts
    newChartForm.FormChart = Chart_ReturnScatter
    newChartForm.ParentChartList = ReturnScatterChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock ReturnScatterChartArrayList
      ReturnScatterChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_StdDev_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_StdDev.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintStdDevCharts
    newChartForm.FormChart = Chart_StdDev
    newChartForm.ParentChartList = StdDevChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock StdDevChartArrayList
      StdDevChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub C1Chart_VAR_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVARCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = VARChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock VARChartArrayList
      VARChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_VAR_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_VAR.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVARCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = VARChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock VARChartArrayList
      VARChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub


  Private Sub Chart_Omega_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Omega.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintOmegaCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = OmegaChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock OmegaChartArrayList
      OmegaChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Drawdown_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_DrawDown.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintDrawdownCharts
    newChartForm.FormChart = Chart_DrawDown
    newChartForm.ParentChartList = DrawdownChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock DrawdownChartArrayList
      DrawdownChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Correlation_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Correlation.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintCorrelationCharts
    newChartForm.FormChart = Chart_Correlation
    newChartForm.ParentChartList = CorrelationChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock CorrelationChartArrayList
      CorrelationChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Alpha_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Alpha.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintAlphaCharts
    newChartForm.FormChart = Chart_Alpha
    newChartForm.ParentChartList = AlphaChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock AlphaChartArrayList
      AlphaChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Beta_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Beta.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintBetaCharts
    newChartForm.FormChart = Chart_Beta
    newChartForm.ParentChartList = BetaChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock BetaChartArrayList
      BetaChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Quartile_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Quartile.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    Try

      newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
      newChartForm.GenoaParentForm = Me
      newChartForm.UpdateChartSub = AddressOf Me.PaintQuartileCharts
      newChartForm.FormChart = Chart_Quartile
      newChartForm.ParentChartList = QuartileChartArrayList
      newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text & ", " & Combo_Quartile_ChartData.Text

      Try
        If IsNumeric(Combo_Quartile_ChartData.SelectedValue) Then
          newChartForm.ParameterList = New String() {"ChartData=" & Combo_Quartile_ChartData.SelectedValue.ToString}
        Else
          newChartForm.ParameterList = Nothing
        End If
      Catch ex As Exception
        newChartForm.ParameterList = Nothing
      End Try

      SyncLock QuartileChartArrayList
        QuartileChartArrayList.Add(newChartForm.DisplayedChart)
      End SyncLock

      PaintQuartileCharts(newChartForm.DisplayedChart)

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Chart_Ranking_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Ranking.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintRankingCharts
    newChartForm.FormChart = Chart_Ranking
    newChartForm.ParentChartList = RankingChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text & ", " & Combo_Ranking_ChartData.Text

    Try
      If IsNumeric(Combo_Quartile_ChartData.SelectedValue) Then
        newChartForm.ParameterList = New String() {"ChartData=" & Combo_Ranking_ChartData.SelectedValue.ToString}
      Else
        newChartForm.ParameterList = Nothing
      End If
    Catch ex As Exception
      newChartForm.ParameterList = Nothing
    End Try

    SyncLock RankingChartArrayList
      RankingChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    ' PaintRankingCharts(newChartForm.DisplayedChart)

  End Sub

  Private Sub Grid_Comp_Statistics_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Comp_Statistics.DoubleClick
    ' *******************************************************************************
    ' If the Grid is Double-Clicked on, Spawn a new Grid Form and update the contents
    ' with this Grid.
    ' *******************************************************************************

    Dim newGridForm As frmViewComparisonGrid

    newGridForm = MainForm.New_GenoaForm(GenoaFormID.frmViewComparisonGrid).Form
    newGridForm.GenoaParentForm = Me
    newGridForm.UpdateGridSub = AddressOf Me.SetComparisonStatsGrid
    newGridForm.FormGrid = Grid_Comp_Statistics
    newGridForm.ParentGridList = ComparisonStatsGridList
    newGridForm.Text = "Comparison Statistics Grid"

    SyncLock ComparisonStatsGridList
      ComparisonStatsGridList.Add(newGridForm.DisplayedGrid)
    End SyncLock

  End Sub

  Private Sub Chart_Comparison_Correlation_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Correlation.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonCorrelationChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_Correlation
    newChartForm.ParentChartList = CorrelationBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      CorrelationBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonCorrelationChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_CorrelationUpDown_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_CorrelationUpDown.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonCorrelationUpDownChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_CorrelationUpDown
    newChartForm.ParentChartList = CorrelationUpDownBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      CorrelationUpDownBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonCorrelationUpDownChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_Beta_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Beta.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonBetaChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_Beta
    newChartForm.ParentChartList = BetaBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      BetaBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonBetaChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_BetaUpDown_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_BetaUpDown.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonBetaUpDownChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_BetaUpDown
    newChartForm.ParentChartList = BetaBarUpDownArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      BetaBarUpDownArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonBetaUpDownChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_Alpha_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Alpha.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonAlphaChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_Alpha
    newChartForm.ParentChartList = AlphaBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      AlphaBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonAlphaChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_APR_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_APR.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonAPRChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_APR
    newChartForm.ParentChartList = APRBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      APRBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonAlphaChart(newChartForm.Chart_Chart)

  End Sub

  Private Sub Chart_Comparison_Volatility_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Comparison_Volatility.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.SetComparisonVolatilityChart
    newChartForm.MenuPeriods.Enabled = False
    newChartForm.FormChart = Chart_Comparison_Volatility
    newChartForm.ParentChartList = VolatilityBarArrayList
    newChartForm.Text = "Chart " & TabControl_Comparisons.SelectedTab.Text

    SyncLock BetaChartArrayList
      VolatilityBarArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

    Call SetComparisonVolatilityChart(newChartForm.Chart_Chart)

  End Sub

#End Region

#Region " Comparison Chart functions"

  Private Sub Set_ComparisonChart_FromList(ByRef pMainForm As GenoaMain, ByRef pList As ListBox, ByVal pChartType As GenoaChartTypes, ByRef pChart As Dundas.Charting.WinControl.Chart, ByVal pRadio_ListIsReference As Boolean, ByRef pReferenceIndex As Object, ByVal pStartDate As Date, ByVal pEndDate As Date, ByVal pLamda As Double)
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Dim InstrumentCounter As Integer
    Dim PertracID As Integer
    Dim ListPertracID As Integer
    Dim PertracName As String
    Dim ReferenceID As Integer = 0
    Dim SeriesOne As Dundas.Charting.WinControl.Series
    Dim SeriesTwo As Dundas.Charting.WinControl.Series = Nothing
    Dim SeriesOneValue As Double
    Dim SeriesTwoValue As Double

    Dim thisStatObject As StatFunctions.ComparisonStatsClass
    Dim PointIndex As Integer
    Dim RequiredChartSeries As Integer = 1
    Dim StatsDatePeriod As DealingPeriod

    Select Case pChartType
      Case GenoaChartTypes.Compare_BetaupDown, GenoaChartTypes.Compare_CorrelationUpDown
        RequiredChartSeries = 2

      Case Else
        RequiredChartSeries = 1
    End Select

    While pChart.Series.Count > RequiredChartSeries
      pChart.Series.RemoveAt(pChart.Series.Count - 1)
    End While

    If (pChart.Series.Count < RequiredChartSeries) Then
      SeriesOne = pChart.Series.Add("Correlation")

      SeriesOne.Type = Dundas.Charting.WinControl.SeriesChartType.Bar
      SeriesOne.XValueType = Dundas.Charting.WinControl.ChartValueTypes.Double
      SeriesOne.YValueType = Dundas.Charting.WinControl.ChartValueTypes.Double
      SeriesOne.CustomAttributes = "BarLabelStyle=Left, LabelStyle=Top"
    End If

    SeriesOne = pChart.Series(0)
    SeriesOne.Points.Clear()

    If (pChart.Series.Count > 1) Then
      SeriesTwo = pChart.Series(1)
      SeriesTwo.Points.Clear()
    End If

    pChart.ChartAreas(0).AxisX.CustomLabels.Clear()

    For InstrumentCounter = 0 To (pList.SelectedItems.Count - 1)
      PertracID = pReferenceIndex
      ReferenceID = pReferenceIndex

      Try
        If (pRadio_ListIsReference) Then
          ReferenceID = CInt(pList.SelectedItems(InstrumentCounter)(pList.ValueMember))
          ListPertracID = ReferenceID
        Else
          PertracID = CInt(pList.SelectedItems(InstrumentCounter)(pList.ValueMember))
          ListPertracID = PertracID
        End If

        PertracName = CStr(pList.SelectedItems(InstrumentCounter)(pList.DisplayMember))
      Catch ex As Exception
      End Try

      StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(PertracID, ReferenceID), Me.MenuSelectedStatsDataLimit)
      thisStatObject = MainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CULng(PertracID), CULng(ReferenceID), False, ListPertracID, pStartDate, pEndDate, Edit_Compare_Lamda.Value, 999, 1000)

      Select Case pChartType

        Case GenoaChartTypes.Compare_Correlation
          SeriesOneValue = thisStatObject.Correlation
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)

        Case GenoaChartTypes.Compare_CorrelationUpDown
          SeriesOneValue = thisStatObject.CorrelationUpOnly
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)
          SeriesTwoValue = thisStatObject.CorrelationDownOnly
          SeriesTwo.Points.Add(SeriesTwoValue)

          If (SeriesTwoValue < 0) Then
            SeriesTwo.Points(PointIndex).Color = Color.Coral
          Else
            SeriesTwo.Points(PointIndex).Color = Color.LightSteelBlue
          End If
          SeriesTwo.Points(PointIndex).BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid

        Case GenoaChartTypes.Compare_Beta
          SeriesOneValue = thisStatObject.Beta(ListPertracID)
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)

        Case GenoaChartTypes.Compare_BetaupDown
          SeriesOneValue = thisStatObject.BetaUpOnly(ListPertracID)
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)
          SeriesTwoValue = thisStatObject.BetaDownOnly(ListPertracID)
          SeriesTwo.Points.Add(SeriesTwoValue)

          If (SeriesTwoValue < 0) Then
            SeriesTwo.Points(PointIndex).Color = Color.Coral
          Else
            SeriesTwo.Points(PointIndex).Color = Color.LightSteelBlue
          End If
          SeriesTwo.Points(PointIndex).BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid

        Case GenoaChartTypes.Compare_Volatility
          SeriesOneValue = thisStatObject.Volatility(ListPertracID) * 100
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)

        Case GenoaChartTypes.Compare_APR
          SeriesOneValue = thisStatObject.APR(ListPertracID) * 100
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)

        Case GenoaChartTypes.Compare_Alpha
          SeriesOneValue = thisStatObject.Alpha(ListPertracID) * 100
          PointIndex = SeriesOne.Points.Add(SeriesOneValue)

      End Select

      ' Set Series One Colour
      If (SeriesOneValue < 0) Then
        SeriesOne.Points(PointIndex).Color = Color.Pink
      Else
        SeriesOne.Points(PointIndex).Color = Color.SteelBlue
      End If

      ' thisSeries.Points(PointIndex).Label = pList.SelectedItems(InstrumentCounter)(pList.DisplayMember)
      SeriesOne.Points(PointIndex).AxisLabel = MainForm.PertracData.GetInformationValue(ListPertracID, PertracInformationFields.FundName).ToString

    Next

    Select Case pChartType
      Case GenoaChartTypes.Compare_BetaupDown, GenoaChartTypes.Compare_CorrelationUpDown
        '

      Case Else
        SeriesOne.Sort(Dundas.Charting.WinControl.PointsSortOrder.Ascending)

    End Select


  End Sub

#End Region

#Region " Grid Context Menu Code / Grid Ctrl-C Code. Also 'Copy-All-Grids-to-Clipboard' Code. "

  Sub PerformanceGrid_cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim CustomFieldID As Integer = 0
    Dim PertracID As Integer = 0
    Dim GroupID As Integer = 0
    Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim ThisMenuStrip As ContextMenuStrip

    If Not (TypeOf (CType(sender, ContextMenuStrip).Tag) Is C1FlexGrid) Then
      Exit Sub
    End If
    ThisGrid = CType(CType(sender, ContextMenuStrip).Tag, C1FlexGrid)
    ThisMenuStrip = ThisGrid.ContextMenuStrip
    If (ThisMenuStrip Is Nothing) Then
      Exit Sub
    End If

    Try

      ' Clear the ContextMenuStrip control's 
      ' Items collection.
      ThisMenuStrip.Items.Clear()

      ThisMenuStrip.Items.Add("Copy Selected prices", Nothing, AddressOf Grid_Menu_Copy)
      ThisMenuStrip.Items.Add("Copy All Prices, This Instrument only", Nothing, AddressOf Grid_Menu_CopyAllRows)
      ThisMenuStrip.Items.Add(New ToolStripSeparator)
      ThisMenuStrip.Items.Add("Copy All Prices, This Instrument only, Native Data period.", Nothing, AddressOf Grid_Menu_CopyAllPrices_Native)
      ThisMenuStrip.Items.Add("Copy All Prices, This Instrument only, " & MainForm.DefaultStatsDatePeriod.ToString & " Data period. (Main Form Default)", Nothing, AddressOf Grid_Menu_CopyAllPrices_Default)
      ThisMenuStrip.Items.Add(New ToolStripSeparator)
      ThisMenuStrip.Items.Add("Copy All Prices, All Selected Instruments as a Grid", Nothing, AddressOf Grid_Menu_CopyAllGridsAsGrid)
      ThisMenuStrip.Items.Add("Copy All Prices, All Selected Instruments as a Long List", Nothing, AddressOf Grid_Menu_CopyAllGridsAsList)

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Menu_Copy(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      Call MainForm.CopyGridSelection(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid), True, True)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllPrices_Native(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      Dim PertracID As ULong = CULng(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid).Tag)
      Dim NativeDataPeriod As DealingPeriod = MainForm.PertracData.GetPertracDataPeriod(PertracID)

      Call MainForm.CopyInstrumentData(PertracID, NativeDataPeriod)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllPrices_Default(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      Dim PertracID As ULong = CULng(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid).Tag)

      CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Close()
      Application.DoEvents()

      Call MainForm.CopyInstrumentData(PertracID, MainForm.DefaultStatsDatePeriod)
    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllRows(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Close()
      Application.DoEvents()

      Call MainForm.CopyGridSelection(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid), True, True, True)
    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllGridsAsGrid(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Call CopyAllPerformanceGrids(True)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllGridsAsList(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Call CopyAllPerformanceGrids(False)
    Catch ex As Exception
    End Try
  End Sub

  Public Sub CopyAllPerformanceGrids(ByVal PresentAsAGrid As Boolean)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************
    Dim xClipString As String = ""
    Dim Clip As New System.Text.StringBuilder
    Dim HeaderString As String = ""

    Dim InstrumentCounter As Integer
    Dim DateCounter As Integer
    Dim ReturnCounter As Integer
    Dim PertracID As Integer
    Dim ThisInstrumentID As ULong
    Dim PertracName As String
    Dim MinDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data
    Dim MaxDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
    Dim PeriodCount As Integer
    Dim TempDateSeries() As Date
    Dim DateArray() As Date
    Dim IDArray() As Integer
    Dim ReturnsArray(-1, -1) As Double
    Dim TempReturnSeries() As Double

    Try
      If (List_SelectItems.SelectedItems.Count <= 0) Then
        Exit Sub
      End If

      If (PresentAsAGrid) Then
        Dim StatsDatePeriod As DealingPeriod = DealingPeriod.Daily
        Dim thisStatsDatePeriod As DealingPeriod

        ' Iterate through selected Instruments in order to get Max / Min dates and thus allow the result Arrays to be dimensioned.

        For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

          Try
            PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
            PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
            ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)

            thisStatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(PertracID), Me.MenuSelectedStatsDataLimit)

            ' Collect Lowest Common DatePeriod.

            If (MainForm.PertracData.AnnualPeriodCount(thisStatsDatePeriod) < MainForm.PertracData.AnnualPeriodCount(StatsDatePeriod)) Then
              StatsDatePeriod = thisStatsDatePeriod
            End If

            Try
              MainForm.GenoaStatusLabel.Text = "Loading " & PertracName
              MainForm.GenoaStatusStrip.Refresh()
            Catch ex As Exception
            End Try

          Catch ex As Exception
            PertracID = 0
            PertracName = "<Error>"
          End Try

          If (PertracID > 0) Then
            TempDateSeries = MainForm.StatFunctions.DateSeries(StatsDatePeriod, ThisInstrumentID, False, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)

            If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then
              If (TempDateSeries(0) < MinDate) Then MinDate = TempDateSeries(0)
              If (TempDateSeries(TempDateSeries.Length - 1) > MaxDate) Then MaxDate = TempDateSeries(TempDateSeries.Length - 1)
            End If
          End If

        Next

        ' Dimension Arrays

        PeriodCount = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(StatsDatePeriod, MinDate, MaxDate)
        ReDim DateArray(PeriodCount - 1)
        ReDim IDArray(List_SelectItems.SelectedItems.Count - 1)
        ReDim ReturnsArray(PeriodCount - 1, List_SelectItems.SelectedItems.Count - 1)

        DateArray(0) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, MinDate, True)
        DateCounter = 1
        While (DateCounter < DateArray.Length)
          DateArray(DateCounter) = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(StatsDatePeriod, DateArray(DateCounter - 1), 1)
          DateCounter += 1
        End While

        ' All data returned in a wide grid

        HeaderString = "Date" & Chr(9)
        For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

          ' Resolve Instrument ID.
          ' Am using the Stats code as it returns data in a standardised format. One value per period only.

          Try
            PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
            PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
            ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)
          Catch ex As Exception
            PertracID = 0
            PertracName = "<Error>"
          End Try

          Try
            MainForm.GenoaStatusLabel.Text = "Loading " & PertracName
            MainForm.GenoaStatusStrip.Refresh()
          Catch ex As Exception
          End Try

          HeaderString &= "(" & PertracID & ") " & PertracName & Chr(9)

          ' Populate the ReturnsArray with the correct data.
          ' Assume that the date ranges for each instrument are likely to differ.

          If (PertracID > 0) Then
            TempDateSeries = MainForm.StatFunctions.DateSeries(StatsDatePeriod, ThisInstrumentID, False, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
            TempReturnSeries = MainForm.StatFunctions.ReturnSeries(StatsDatePeriod, ThisInstrumentID, False, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)

            If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then
              Dim ReturnIndex As Integer

              ReturnIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, MinDate, TempDateSeries(0))

              For ReturnCounter = 0 To (TempDateSeries.Length - 1)
                ReturnsArray(ReturnIndex, InstrumentCounter) = TempReturnSeries(ReturnCounter)
                ReturnIndex += 1
              Next

            End If
          End If

        Next

        HeaderString &= Chr(13)

        ' Build the ClipString 
        ' ReturnsArray(PeriodCount - 1, List_SelectItems.SelectedItems.Count - 1)


        For DateCounter = 0 To (PeriodCount - 1)
          Clip.Append(DateArray(DateCounter).ToString(QUERY_SHORTDATEFORMAT) & Chr(9))

          For ReturnCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
            Clip.Append(ReturnsArray(DateCounter, ReturnCounter).ToString("###0.00####%") & Chr(9))
          Next

          Clip.Append(Chr(13))
        Next

      Else
        ' All data returned in a long Column...
        ' Use Native DataPeriod for Price Series.

        HeaderString = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(13)

        Dim PertracIDString As String
        Dim StatsDatePeriod As DealingPeriod

        For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

          Try
            PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
            PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
            ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)
            StatsDatePeriod = MainForm.PertracData.CoarsestDataPeriod(MainForm.PertracData.GetPertracDataPeriod(PertracID), Me.MenuSelectedStatsDataLimit)
          Catch ex As Exception
            PertracID = 0
            PertracName = "<Error>"
            StatsDatePeriod = DealingPeriod.Monthly
          End Try
          PertracIDString = PertracID.ToString

          Try
            MainForm.GenoaStatusLabel.Text = "Loading " & PertracName
            MainForm.GenoaStatusStrip.Refresh()
          Catch ex As Exception
          End Try

          If (PertracID > 0) Then
            TempDateSeries = MainForm.StatFunctions.DateSeries(StatsDatePeriod, ThisInstrumentID, False, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
            TempReturnSeries = MainForm.StatFunctions.ReturnSeries(StatsDatePeriod, ThisInstrumentID, False, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)


            If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then

              For ReturnCounter = 0 To (TempDateSeries.Length - 1)

                Clip.Append(PertracIDString & Chr(9) & FitDateToPeriod(StatsDatePeriod, TempDateSeries(ReturnCounter), True).ToString(QUERY_SHORTDATEFORMAT) & Chr(9) & TempReturnSeries(ReturnCounter).ToString("###0.00####%") & Chr(13))

              Next

            End If

          End If

        Next

      End If

    Catch ex As Exception
    Finally
      Try
        MainForm.GenoaStatusLabel.Text = ""
        MainForm.GenoaStatusStrip.Refresh()
      Catch ex As Exception
      End Try

      Try
        Clipboard.Clear()
        Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString & Clip.ToString))
      Catch ex As Exception
      End Try
    End Try


  End Sub

  Private Sub Grid_Performance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) ' Handles Grid_Performance_0.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, True)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Comp_Statistics_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Comp_Statistics.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Stats_Drawdowns_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Stats_Drawdowns.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_SimpleStatistics_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_SimpleStatistics.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub List_SelectItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles List_SelectItems.KeyDown
    ' *************************************************************************************
    ' Clipboard 'Copy' functionality
    '
    '
    ' *************************************************************************************

    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyListSelection(CType(sender, ListBox))
        e.SuppressKeyPress = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private _validData As Boolean

  Private Sub List_SelectItems_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_SelectItems.DragEnter
    'get the data
    Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

    'no empty data
    If DragString Is Nothing OrElse DragString.Length = 0 Then
      _validData = False
      Return
    End If

    If (DragString IsNot Nothing) AndAlso (DragString.StartsWith(GENOA_DRAG_IDs_HEADER)) Then
      If DragString.Split(",").Length > 1 Then
        _validData = True
        Return
      End If
    End If

    _validData = False

  End Sub

  Private Sub List_SelectItems_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles List_SelectItems.DragLeave
    _validData = False
  End Sub

  Private Sub List_SelectItems_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_SelectItems.DragOver
    If _validData Then
      e.Effect = DragDropEffects.Copy
    Else
      e.Effect = DragDropEffects.None
    End If

  End Sub

  Private Sub List_SelectItems_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_SelectItems.DragDrop
    ' ***********************************************************************************
    '
    '
    '
    ' ***********************************************************************************

    Radio_CustomGroup.Checked = True
    Application.DoEvents()

    'get the data
    Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

    'no empty data
    If (DragString Is Nothing) OrElse (DragString.StartsWith(GENOA_DRAG_IDs_HEADER) = False) Then
      Return
    End If

    ' Process

    Dim ID_Strings() As String
    Dim ID_Counter As Integer
    Dim IDs(-1) As Integer

    Try
      ' Get Dragged IDs

      ID_Strings = DragString.Split(",")

      ' Note, the first element should be the Header String, so don't count it as an ID.
      If (ID_Strings Is Nothing) OrElse (ID_Strings.Length <= 1) Then
        Return
      End If

      IDs = Array.CreateInstance(GetType(Integer), ID_Strings.Length - 1)

      ' Cycle through the Selected Items, adding each one (if it does not already exist) to the GroupItems List

      For ID_Counter = 1 To (ID_Strings.Length - 1)
        If IsNumeric(ID_Strings(ID_Counter)) Then
          IDs(ID_Counter - 1) = CInt(ID_Strings(ID_Counter))
        End If
      Next

      SetCustomListInstrumentIDs(IDs)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Droping Instruments on the Items List.", ex.StackTrace, True)
    End Try

  End Sub

  Public Sub SetCustomListInstrumentIDs(ByVal pIDs() As Integer)
    ' ***********************************************************************************
    '
    '
    ' CustomGroupID_WholeList
    ' ***********************************************************************************
    Dim OrgCursor As System.Windows.Forms.Cursor = Cursors.Default

    Try
      OrgCursor = Me.Cursor
      Me.Cursor = Cursors.WaitCursor

      Try

        If (Not Radio_CustomGroup.Checked) Then
          Radio_CustomGroup.Checked = True

          Application.DoEvents()
        End If

        Combo_SelectFrom.Text = ""

      Catch ex As Exception
      End Try

      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Me.Radio_CustomGroup.Checked) Then

        ' Process
        Dim ListTable As New DataTable
        Dim ListTableRow As DataRow
        Dim InformationArray() As Object

        Dim PertracID As Integer
        Dim ID_Counter As Integer

        ' Set Dynamic Group Members 

        MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_WholeList, pIDs, Nothing)

        ' Set Table Columns

        ListTable.Columns.Add(New System.Data.DataColumn("PertracCode", GetType(Integer)))
        ListTable.Columns.Add(New System.Data.DataColumn("Mastername", GetType(String)))
        ListTable.Columns.Add(New System.Data.DataColumn("DataVendorName", GetType(String)))
        ListTable.Columns.Add(New System.Data.DataColumn("ListDescription", GetType(String)))

        ' Cycle through the Selected Items, adding each one (if it does not already exist) to the GroupItems List

        If (pIDs IsNot Nothing) AndAlso (pIDs.Length > 0) Then

          For ID_Counter = 0 To (pIDs.Length - 1)
            PertracID = pIDs(ID_Counter)

            If (PertracID > 0) Then
              ListTableRow = ListTable.NewRow

              ListTableRow("PertracCode") = PertracID

              InformationArray = MainForm.PertracData.GetInformationValues(PertracID)

              If (InformationArray IsNot Nothing) Then

                Try
                  ListTableRow("Mastername") = Nz(InformationArray(PertracInformationFields.FundName), "<No Name>").ToString
                  ListTableRow("DataVendorName") = Nz(InformationArray(PertracInformationFields.DataVendorName), "<No Name>").ToString
                  ListTableRow("ListDescription") = ListTableRow("Mastername").ToString & " (" & ListTableRow("DataVendorName").ToString & ")"

                Catch ex As Exception
                End Try

                ListTable.Rows.Add(ListTableRow)

              End If

            End If

          Next

        End If

        List_SelectItems.SelectedIndex = (-1)

        Try

          If (ListTable IsNot Nothing) Then
            List_SelectItems.DataSource = Nothing
          End If
          If (List_SelectItems.DisplayMember <> "ListDescription") Then
            List_SelectItems.DisplayMember = "ListDescription"
          End If
          If (List_SelectItems.ValueMember <> "PertracCode") Then
            List_SelectItems.ValueMember = "PertracCode"
          End If

        Catch ex As Exception
        End Try

        List_SelectItems.DataSource = ListTable

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Instruments on the Items List.", ex.StackTrace, True)
    Finally
      Me.Cursor = OrgCursor
    End Try

  End Sub

#End Region

  Private Sub Check_AutoStart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AutoStart.CheckedChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not InPaint) Then

        If (Check_AutoStart.Checked) Then

          Me.Date_Charts_DateFrom.Value = GetLatestStartDateFromList(List_SelectItems)

          ChartsToUpdate = True
          ComparisonToUpdate = True

        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Function GetIDsFromList(ByVal pList As ListBox) As Integer()
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim RVal(-1) As Integer
    Dim InstrumentCounter As Integer

    Try
      If (pList.SelectedItems.Count > 0) Then

        ReDim RVal(pList.SelectedItems.Count - 1)

        For InstrumentCounter = 0 To (pList.SelectedItems.Count - 1)
          RVal(InstrumentCounter) = CInt(pList.SelectedItems(InstrumentCounter)(pList.ValueMember))
        Next

      End If
    Catch ex As Exception
      ReDim RVal(-1)
    End Try

    Return RVal

  End Function

  Private Function GetLatestStartDateFromList(ByVal pList As ListBox) As Date
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim RVal As Date = Renaissance_BaseDate
    Dim PertracIDs() As Integer = GetIDsFromList(pList)
    Dim InstrumentCounter As Integer
    Dim ThisDate As Date
    Dim EarliestDate As Date = Renaissance_BaseDate

    If (PertracIDs IsNot Nothing) AndAlso (PertracIDs.Count > 0) Then

      For InstrumentCounter = 0 To (PertracIDs.Count - 1)

        ThisDate = MainForm.StatFunctions.GetStartDate(PertracIDs(InstrumentCounter))

        If (ThisDate > EarliestDate) Then
          EarliestDate = ThisDate
        End If

      Next

    End If

    ' Add in the Comparison Series

    Dim ComparisonSeries As Integer = (0)

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
    End Try

    If (ComparisonSeries > 0) Then
      ThisDate = MainForm.StatFunctions.GetStartDate(ComparisonSeries)

      If (ThisDate > EarliestDate) Then
        EarliestDate = ThisDate
      End If
    End If

    ' Return date as appropriate 

    If (EarliestDate < Renaissance_EndDate_Data) Then
      Return EarliestDate
    Else
      Return Renaissance_BaseDate
    End If

  End Function

  Private Sub Menu_InformationReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_InformationReport.Click
    ' *************************************************************************************
    '
    ' Generate the Fund Information report for the selected fund(s).
    '
    ' *************************************************************************************

    Dim SelectedIDs(-1) As Integer
    Dim IndexCounter As Integer
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    Try
      If Me.List_SelectItems.SelectedItems.Count <= 0 Then
        Exit Sub
      End If

      Me.Cursor = Cursors.WaitCursor

      ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)

      For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
        SelectedIDs(IndexCounter) = CInt(List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember))
      Next

      Application.DoEvents()

      MainForm.MainReportHandler.InformationReport(SelectedIDs, ComparisonSeries, Nothing)

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub Menu_ChartsReport_Basic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ChartsReport_Basic.Click
    ' *************************************************************************************
    '
    ' *************************************************************************************

    ShowChartsReport(True, False, False)
  End Sub

  Private Sub Menu_ChartsReport_Quartile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ChartsReport_Quartile.Click
    ' *************************************************************************************
    '
    ' *************************************************************************************

    ShowChartsReport(False, True, False)
  End Sub

  Private Sub Menu_ChartsReport_Ranking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ChartsReport_Ranking.Click
    ' *************************************************************************************
    '
    ' *************************************************************************************

    ShowChartsReport(False, False, True)
  End Sub

  Private Sub Menu_ChartsReport_All_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ChartsReport_All.Click
    ' *************************************************************************************
    '
    ' *************************************************************************************

    ShowChartsReport(True, True, True)
  End Sub

  Private Sub Menu_ChartsReport_CompleteReportPack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ChartsReport_CompleteReportPack.Click
    ' *************************************************************************************
    ' Run Complete report pack for the selected group.
    '
    ' *************************************************************************************
    Dim FormControls As ArrayList = Nothing
    Dim ReportPageArray As New ArrayList
    Dim InitialHeaderFlag As Boolean
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    Try
      FormControls = MainForm.DisableFormControls(Me)
      Me.Cursor = Cursors.WaitCursor
      InitialHeaderFlag = Me.Menu_ChartsReport_HeaderPage.Checked

      ' Confirm Comparison Series

      If (ComparisonSeries <= 0) Then
        If (MessageBox.Show("The Charts Comparison series does not appear to be set. Do you wish to continue ?", "Continue?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> MsgBoxResult.Ok) Then
          Exit Sub
        End If
      End If

      ' Run Basic Reports

      Dim ThisIndex As Integer

      For ThisIndex = 0 To (List_SelectItems.Items.Count - 1)
        List_SelectItems.SelectedItems.Clear()
        List_SelectItems.SelectedIndex = ThisIndex

        Application.DoEvents()

        ' Run Basic Reports

        GetChartsReportPageArray(True, False, False, True, ReportPageArray)

      Next

      ' Select All

      For ThisIndex = 0 To (List_SelectItems.Items.Count - 1)
        If Not List_SelectItems.SelectedIndices.Contains(ThisIndex) Then
          ' List_SelectItems.SelectedIndex = ThisIndex
          List_SelectItems.SelectedIndices.Add(ThisIndex)
        End If
      Next

      Application.DoEvents()

      ' Run Quartile and Ranking Reports with each instrument highlighted.

      Menu_ChartsReport_HeaderPage.Checked = True

      For ThisIndex = 1 To (Combo_Quartile_HighlightSeries.Items.Count - 1)

        Combo_Quartile_HighlightSeries.SelectedIndex = ThisIndex

        Combo_Ranking_HighlightSeries.SelectedIndex = ThisIndex

        Application.DoEvents()

        ' Get Quartile Charts

        GetChartsReportPageArray(False, True, False, True, ReportPageArray)

        ' Turn Header page off.

        If (Not InitialHeaderFlag) AndAlso (Menu_ChartsReport_HeaderPage.Checked) Then
          Menu_ChartsReport_HeaderPage.Checked = False
        End If

        ' Get Ranking charts.

        GetChartsReportPageArray(False, False, True, True, ReportPageArray)

      Next

      ' Display accumulated chart pages.

      ShowChartsReport(ReportPageArray)

    Catch ex As Exception
    Finally
      Menu_ChartsReport_HeaderPage.Checked = InitialHeaderFlag

      MainForm.EnableFormControls(FormControls)
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub ShowChartsReport_Org(ByVal DoNormalReports As Boolean, ByVal DoQuartileCharts As Boolean, ByVal DoRankingCharts As Boolean)

    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Dim SelectedIDs(-1) As Integer
    Dim SelectedNames(-1) As String
    Dim IndexCounter As Integer
    Dim ReferenceID As Integer = 0
    Dim ShowHeaderPage As Boolean = True
    Dim MultiChartReport As Boolean = False

    Try
      If Me.List_SelectItems.SelectedItems.Count <= 0 Then
        Exit Sub
      End If

      Me.Cursor = Cursors.WaitCursor

      ' 
      ShowHeaderPage = Me.Menu_ChartsReport_HeaderPage.Checked
      MultiChartReport = Me.Menu_ChartsReport_MultiChart.Checked

      ' Get selected Instrument IDs

      ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)
      ReDim SelectedNames(List_SelectItems.SelectedItems.Count - 1)

      For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
        SelectedIDs(IndexCounter) = CInt(List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember))
        SelectedNames(IndexCounter) = CStr(List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.DisplayMember))
      Next

      ' Build Report Pages

      Dim newForm As frmViewReport
      'Dim tempPrintPreview As C1.Win.C1Preview.C1PrintPreviewControl = Nothing
      Dim PageImageArray As New ArrayList
      Dim HeaderReport As C1.C1Report.C1Report
      Dim ChartReport As C1.C1Report.C1Report = Nothing

      newForm = MainForm.New_GenoaForm(GenoaFormID.frmViewReport).Form
      newForm.Opacity = 0

      Try
        Dim ReportTable As New DataTable
        Dim ReportRow As DataRow

        ' Build Header Page

        If (ShowHeaderPage) Then

          ReportTable.Columns.Add("GroupMember", GetType(String))
          For IndexCounter = 0 To (SelectedNames.Length - 1)
            ReportRow = ReportTable.NewRow

            ReportRow(0) = SelectedNames(IndexCounter)
            ReportTable.Rows.Add(ReportRow)
          Next

          HeaderReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartHeaderReport", "ChartHeaderReport")
          HeaderReport.DataSource.Recordset = ReportTable

          Try
            HeaderReport.Fields("Field_DateFrom").Text = Date_Charts_DateFrom.Value.ToString(REPORT_DATEFORMAT)
            HeaderReport.Fields("Field_DateTo").Text = Date_Charts_DateTo.Value.ToString(REPORT_DATEFORMAT)
            HeaderReport.Fields("Field_Lambda").Text = Text_Chart_Lamda.Value.ToString
            HeaderReport.Fields("Field_RollingPeriod").Text = Text_Chart_RollingPeriod.Value.ToString
            HeaderReport.Fields("Field_ConditionalCorr").Text = Combo_Chart_Conditional.SelectedItem(Combo_Chart_Conditional.DisplayMember).ToString

            Dim ComparisonSeries As Integer = (0)
            Dim ComparisonSeriesText As String = ""

            Try
              If (Radio_Charts_ComparePertrac.Checked) Then
                If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
                  ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
                  ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.SelectedItem(Combo_Charts_CompareSeriesPertrac.DisplayMember).ToString
                End If
              Else
                If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
                  ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
                  ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.SelectedItem(Combo_Charts_CompareSeriesGroup.DisplayMember).ToString
                End If
              End If
            Catch ex As Exception
              ComparisonSeries = 0
              ComparisonSeriesText = ""
            End Try

            If (ComparisonSeries > 0) Then
              HeaderReport.Fields("Field_HighlightSeries").Text = ComparisonSeriesText
            Else
              HeaderReport.Fields("Field_HighlightSeries").Text = "<None>"
            End If

          Catch ex As Exception
          End Try

          HeaderReport.Render()
          PageImageArray.AddRange(HeaderReport.PageImages)

        End If

        ' ****************************************************
        ' Now Produce Chart Pages.
        ' ****************************************************

        Dim ChartCount As Integer
        Dim MultiChartCount As Integer = (-1)
        Dim ThisChart As Dundas.Charting.WinControl.Chart = Nothing
        Dim StartingReport As Integer = 0
        Dim ChartTitle As String
        Dim ChartPending As Boolean = False

        If (Not DoNormalReports) Then
          StartingReport = 9
        End If

        For ChartCount = StartingReport To (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)

          If (MultiChartReport) Then
            If (MultiChartCount < 0) Then

              If (MultiChartReport) AndAlso (ChartPending) Then
                ChartReport.Render()
                PageImageArray.AddRange(ChartReport.PageImages)
                ThisChart = Nothing
                ChartPending = False
              End If

              ChartReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartPageReport", "ChartMultiPageReport")
              MultiChartCount = 0
            End If
          Else
            ChartReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartPageReport", "ChartPageReport")
          End If

          If (ChartReport Is Nothing) Then
            Exit For
          Else

            ChartReport.Fields("Field_HighlightSeries").Text = ""
            ChartReport.Fields("Field_HighlightSeriesLabel").Visible = False
            ThisChart = Nothing
            ChartTitle = ""

            Select Case ChartCount

              Case 0
                ' Chart_Prices - VAMI
                ' ChartReport.Fields("Field_ChartTitle").Text = "VAMI"
                ChartTitle = "VAMI"
                ThisChart = Chart_Prices
                PaintVAMICharts(ThisChart)

              Case 1
                ' Chart_Returns - Monthly Returns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Monthly Returns"
                ChartTitle = "Monthly Returns"
                ThisChart = Chart_Returns
                PaintMonthlyCharts(ThisChart)

              Case 2
                ' Chart_RollingReturn - Rolling Returns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Rolling Returns"
                ChartTitle = "Rolling Returns"
                ThisChart = Chart_RollingReturn
                PaintRollingReturnCharts(ThisChart)

              Case 3
                ' Chart_ReturnScatter - Return Scatter
                ' ChartReport.Fields("Field_ChartTitle").Text = "Returns Scatter chart"
                ChartTitle = "Returns Scatter chart"
                ThisChart = Chart_ReturnScatter
                PaintReturnScatterCharts(ThisChart)

              Case 4
                ' Chart_StdDev - volatility
                ' ChartReport.Fields("Field_ChartTitle").Text = "Volatility"
                ChartTitle = "Volatility"
                ThisChart = Chart_StdDev
                PaintStdDevCharts(ThisChart)

              Case 5
                ' Chart_DrawDown - DrawDowns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Drawdowns"
                ChartTitle = "Drawdowns"
                ThisChart = Chart_DrawDown
                PaintDrawdownCharts(ThisChart)

              Case 6
                ' Chart_Correlation
                ' ChartReport.Fields("Field_ChartTitle").Text = "Correlation"
                ChartTitle = "Correlation"
                ThisChart = Chart_Correlation
                PaintCorrelationCharts(ThisChart)

              Case 7
                ' Chart_Beta
                ' ChartReport.Fields("Field_ChartTitle").Text = "Beta"
                ChartTitle = "Beta"
                ThisChart = Chart_Beta
                PaintBetaCharts(ThisChart)

              Case 8
                ' Chart_Alpha
                ' ChartReport.Fields("Field_ChartTitle").Text = "Alpha"
                ChartTitle = "Alpha"
                ThisChart = Chart_Alpha
                PaintAlphaCharts(ThisChart)

              Case 9 To (8 + Combo_Quartile_ChartData.Items.Count)
                ' Chart_Quartile
                If (DoQuartileCharts) Then

                  Combo_Quartile_ChartData.SelectedIndex = (ChartCount - 9)
                  Application.DoEvents()

                  ' ChartReport.Fields("Field_ChartTitle").Text = "Quartile - " & Combo_Quartile_ChartData.SelectedItem(Combo_Quartile_ChartData.DisplayMember)
                  ChartTitle = "Quartile - " & Combo_Quartile_ChartData.SelectedItem(Combo_Quartile_ChartData.DisplayMember)

                  ThisChart = Chart_Quartile
                  PaintQuartileCharts(ThisChart)

                  If (Combo_Quartile_HighlightSeries.SelectedIndex > 0) Then
                    ChartReport.Fields("Field_HighlightSeries").Text = Combo_Quartile_HighlightSeries.SelectedItem(Combo_Quartile_HighlightSeries.DisplayMember).ToString
                    ChartReport.Fields("Field_HighlightSeriesLabel").Visible = True
                  End If

                End If

              Case (9 + Combo_Quartile_ChartData.Items.Count) To (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)
                ' Chart_Ranking

                If (DoRankingCharts) Then

                  Combo_Ranking_ChartData.SelectedIndex = (ChartCount - (9 + Combo_Quartile_ChartData.Items.Count))
                  Application.DoEvents()

                  ' ChartReport.Fields("Field_ChartTitle").Text = "Ranking - " & Combo_Ranking_ChartData.SelectedItem(Combo_Ranking_ChartData.DisplayMember)
                  ChartTitle = "Ranking - " & Combo_Ranking_ChartData.SelectedItem(Combo_Ranking_ChartData.DisplayMember)
                  ThisChart = Chart_Ranking
                  PaintRankingCharts(ThisChart)
                  ' 
                  If (Combo_Ranking_HighlightSeries.SelectedIndex > 0) Then
                    ChartReport.Fields("Field_HighlightSeries").Text = Combo_Ranking_HighlightSeries.SelectedItem(Combo_Ranking_HighlightSeries.DisplayMember).ToString
                    ChartReport.Fields("Field_HighlightSeriesLabel").Visible = True
                  End If

                End If

            End Select

            ' ****************************************************
            ' Add Chart to the report set, if Chart has been set.
            '
            ' ****************************************************

            If (ThisChart IsNot Nothing) Then

              ThisChart.Refresh()
              Application.DoEvents()

              Dim memStream As MemoryStream = New MemoryStream
              ThisChart.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

              If (MultiChartReport) Then
                ChartReport.Fields("Chart_Chart" & MultiChartCount.ToString).Picture = Image.FromStream(memStream)
                ChartReport.Fields("Chart_Chart" & MultiChartCount.ToString).Visible = True
                ChartReport.Fields("Field_ChartTitle" & MultiChartCount.ToString).Text = ChartTitle
                ChartPending = True
                MultiChartCount += 1

                ' Print Report if this is the Last report, or is a 'Sixth' report.

                If (ChartCount = (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)) OrElse (MultiChartCount >= 6) Then

                  ChartReport.Render()
                  PageImageArray.AddRange(ChartReport.PageImages)
                  ChartPending = False
                  MultiChartCount = (-1)
                End If

              Else

                ChartReport.Fields("Chart_Chart").Picture = Image.FromStream(memStream)
                ChartReport.Fields("Field_ChartTitle").Text = ChartTitle

                ChartReport.Render()
                PageImageArray.AddRange(ChartReport.PageImages)

              End If

            End If

          End If ' (ChartReport isnot Nothing)

        Next

        ' Catch final MultiPage Report, if it has not been rendered already.

        If (MultiChartReport) AndAlso (ChartPending) Then
          ChartReport.Render()
          PageImageArray.AddRange(ChartReport.PageImages)
          ThisChart = Nothing
          MultiChartCount = 0
        End If

      Catch ex As Exception
      Finally

        newForm.ReportPreview.Document = PageImageArray

      End Try

      ' Display Report Form.

      Try
        If Not (newForm Is Nothing) Then
          Dim OCount As Integer
          newForm.Show()

          If MainForm.EnableReportFadeIn Then

            Try
              Dim TStart As Date = Now()
              Dim TEnd As Date

              For OCount = 0 To 100 Step 5
                newForm.Opacity = OCount / 100
                Threading.Thread.Sleep(10)
              Next

              TEnd = Now()
              If (TEnd - TStart).TotalMilliseconds >= 1000 Then
                MainForm.DisableReportFadeIn()
              End If

            Catch ex As Exception
            End Try

          Else
            newForm.Opacity = 1.0#
          End If

        End If
      Catch ex As Exception
      End Try

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub ShowChartsReport(ByVal DoNormalReports As Boolean, ByVal DoQuartileCharts As Boolean, ByVal DoRankingCharts As Boolean)
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try

      ShowChartsReport(GetChartsReportPageArray(DoNormalReports, DoQuartileCharts, DoRankingCharts, False, Nothing))

    Catch ex As Exception
    End Try

  End Sub

  Private Function GetChartsReportPageArray(ByVal DoNormalReports As Boolean, ByVal DoQuartileCharts As Boolean, ByVal DoRankingCharts As Boolean, ByVal pShowGroupInTitle As Boolean, ByRef pPageImageArray As ArrayList) As ArrayList
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Dim SelectedIDs(-1) As Integer
    Dim SelectedNames(-1) As String
    Dim IndexCounter As Integer
    Dim ReferenceID As Integer = 0
    Dim ShowHeaderPage As Boolean = True
    Dim MultiChartReport As Boolean = False
    Dim PageImageArray As ArrayList = Nothing

    Try
      If (pPageImageArray Is Nothing) Then
        PageImageArray = New ArrayList
      Else
        PageImageArray = pPageImageArray
      End If

      If Me.List_SelectItems.SelectedItems.Count <= 0 Then
        Return PageImageArray
        Exit Function
      End If

      ' 
      ShowHeaderPage = Me.Menu_ChartsReport_HeaderPage.Checked
      MultiChartReport = Me.Menu_ChartsReport_MultiChart.Checked

      ' Get selected Instrument IDs

      ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)
      ReDim SelectedNames(List_SelectItems.SelectedItems.Count - 1)

      For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
        SelectedIDs(IndexCounter) = CInt(List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember))
        SelectedNames(IndexCounter) = CStr(List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.DisplayMember))
      Next

      ' Build Report Pages

      Dim HeaderReport As C1.C1Report.C1Report
      Dim ChartReport As C1.C1Report.C1Report = Nothing

      Try
        Dim ReportTable As New DataTable
        Dim ReportRow As DataRow

        ' Build Header Page

        If (ShowHeaderPage) Then

          ReportTable.Columns.Add("GroupMember", GetType(String))
          For IndexCounter = 0 To (SelectedNames.Length - 1)
            ReportRow = ReportTable.NewRow

            ReportRow(0) = SelectedNames(IndexCounter)
            ReportTable.Rows.Add(ReportRow)
          Next

          HeaderReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartHeaderReport", "ChartHeaderReport")
          HeaderReport.DataSource.Recordset = ReportTable

          Try
            HeaderReport.Fields("Field_DateFrom").Text = Date_Charts_DateFrom.Value.ToString(REPORT_DATEFORMAT)
            HeaderReport.Fields("Field_DateTo").Text = Date_Charts_DateTo.Value.ToString(REPORT_DATEFORMAT)
            HeaderReport.Fields("Field_Lambda").Text = Text_Chart_Lamda.Value.ToString
            HeaderReport.Fields("Field_RollingPeriod").Text = Text_Chart_RollingPeriod.Value.ToString
            HeaderReport.Fields("Field_ConditionalCorr").Text = Combo_Chart_Conditional.SelectedItem(Combo_Chart_Conditional.DisplayMember).ToString

            Dim ComparisonSeries As Integer = (0)
            Dim ComparisonSeriesText As String = ""

            Try
              If (Radio_Charts_ComparePertrac.Checked) Then
                If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
                  ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
                  ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.SelectedItem(Combo_Charts_CompareSeriesPertrac.DisplayMember).ToString
                End If
              Else
                If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
                  ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
                  ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.SelectedItem(Combo_Charts_CompareSeriesGroup.DisplayMember).ToString
                End If
              End If
            Catch ex As Exception
              ComparisonSeries = 0
              ComparisonSeriesText = ""
            End Try

            If (ComparisonSeries > 0) Then
              HeaderReport.Fields("Field_HighlightSeries").Text = ComparisonSeriesText
            Else
              HeaderReport.Fields("Field_HighlightSeries").Text = "<None>"
            End If

          Catch ex As Exception
          End Try

          HeaderReport.Render()
          PageImageArray.AddRange(HeaderReport.PageImages)

        End If

        ' ****************************************************
        ' Now Produce Chart Pages.
        ' ****************************************************

        Dim ChartCount As Integer
        Dim MultiChartCount As Integer = (-1)
        Dim ThisChart As Dundas.Charting.WinControl.Chart = Nothing
        Dim StartingReport As Integer = 0
        Dim ChartTitle As String
        Dim ChartPending As Boolean = False

        If (Not DoNormalReports) Then
          StartingReport = 9
        End If

        For ChartCount = StartingReport To (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)

          ' Get Chart Report definition.

          If (MultiChartReport) Then
            If (MultiChartCount < 0) Then

              If (MultiChartReport) AndAlso (ChartPending) Then
                ChartReport.Render()
                PageImageArray.AddRange(ChartReport.PageImages)
                ThisChart = Nothing
                ChartPending = False
              End If

              ChartReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartPageReport", "ChartMultiPageReport")
              MultiChartCount = 0

              ' Initialise parts of the Report

              If (ChartReport IsNot Nothing) Then
                ChartReport.Fields("Field_HighlightSeries").Text = ""
                ChartReport.Fields("Field_HighlightSeriesLabel").Visible = False

                If (pShowGroupInTitle) OrElse ((Radio_ExistingGroups.Checked Or Radio_ExistingFunds.Checked) AndAlso (List_SelectItems.SelectedItems.Count = List_SelectItems.Items.Count)) Then
                  ChartReport.Fields("Field_ReportName").Text &= ", " & Combo_SelectFrom.Text
                End If

                If (DoNormalReports) Then
                  If (List_SelectItems.SelectedItems.Count = 1) Then
                    ChartReport.Fields("Field_HighlightSeries").Text = List_SelectItems.SelectedItems(0)(List_SelectItems.DisplayMember).ToString
                  End If
                End If
              End If

            End If
          Else
            ChartReport = MainForm.MainReportHandler.GetReportDefinition(0, "rptChartPageReport", "ChartPageReport")

            ' Initialise parts of the Report

            If (ChartReport IsNot Nothing) Then

              ChartReport.Fields("Field_HighlightSeries").Text = ""
              ChartReport.Fields("Field_HighlightSeriesLabel").Visible = False

              If (pShowGroupInTitle) Then
                ChartReport.Fields("Field_ReportName").Text &= ", " & Combo_SelectFrom.Text
              End If

              If (DoNormalReports) Then
                If (List_SelectItems.SelectedItems.Count = 1) Then
                  ChartReport.Fields("Field_HighlightSeries").Text = List_SelectItems.SelectedItems(0)(List_SelectItems.DisplayMember).ToString
                End If
              End If

            End If

          End If

          If (ChartReport Is Nothing) Then
            Exit For
          Else

            ThisChart = Nothing
            ChartTitle = ""

            Select Case ChartCount

              Case 0
                ' Chart_Prices - VAMI
                ' ChartReport.Fields("Field_ChartTitle").Text = "VAMI"
                ChartTitle = "VAMI"
                ThisChart = Chart_Prices
                PaintVAMICharts(ThisChart)

              Case 1
                ' Chart_Returns - Monthly Returns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Monthly Returns"
                ChartTitle = "Monthly Returns"
                ThisChart = Chart_Returns
                PaintMonthlyCharts(ThisChart)

              Case 2
                ' Chart_RollingReturn - Rolling Returns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Rolling Returns"
                ChartTitle = "Rolling Returns"
                ThisChart = Chart_RollingReturn
                PaintRollingReturnCharts(ThisChart)

              Case 3
                ' Chart_ReturnScatter - Return Scatter
                ' ChartReport.Fields("Field_ChartTitle").Text = "Returns Scatter chart"
                ChartTitle = "Returns Scatter chart"
                ThisChart = Chart_ReturnScatter
                PaintReturnScatterCharts(ThisChart)

              Case 4
                ' Chart_StdDev - volatility
                ' ChartReport.Fields("Field_ChartTitle").Text = "Volatility"
                ChartTitle = "Volatility"
                ThisChart = Chart_StdDev
                PaintStdDevCharts(ThisChart)

              Case 5
                ' Chart_DrawDown - DrawDowns
                ' ChartReport.Fields("Field_ChartTitle").Text = "Drawdowns"
                ChartTitle = "Drawdowns"
                ThisChart = Chart_DrawDown
                PaintDrawdownCharts(ThisChart)

              Case 6
                ' Chart_Correlation
                ' ChartReport.Fields("Field_ChartTitle").Text = "Correlation"
                ChartTitle = "Correlation"
                ThisChart = Chart_Correlation
                PaintCorrelationCharts(ThisChart)

              Case 7
                ' Chart_Beta
                ' ChartReport.Fields("Field_ChartTitle").Text = "Beta"
                ChartTitle = "Beta"
                ThisChart = Chart_Beta
                PaintBetaCharts(ThisChart)

              Case 8
                ' Chart_Alpha
                ' ChartReport.Fields("Field_ChartTitle").Text = "Alpha"
                ChartTitle = "Alpha"
                ThisChart = Chart_Alpha
                PaintAlphaCharts(ThisChart)

              Case 9 To (8 + Combo_Quartile_ChartData.Items.Count)
                ' Chart_Quartile
                If (DoQuartileCharts) Then

                  Combo_Quartile_ChartData.SelectedIndex = (ChartCount - 9)
                  Application.DoEvents()

                  ' ChartReport.Fields("Field_ChartTitle").Text = "Quartile - " & Combo_Quartile_ChartData.SelectedItem(Combo_Quartile_ChartData.DisplayMember)
                  ChartTitle = "Quartile - " & Combo_Quartile_ChartData.SelectedItem(Combo_Quartile_ChartData.DisplayMember)

                  ThisChart = Chart_Quartile
                  PaintQuartileCharts(ThisChart)

                  If (Combo_Quartile_HighlightSeries.SelectedIndex > 0) Then
                    ChartReport.Fields("Field_HighlightSeries").Text = Combo_Quartile_HighlightSeries.SelectedItem(Combo_Quartile_HighlightSeries.DisplayMember).ToString
                    ChartReport.Fields("Field_HighlightSeriesLabel").Visible = True
                  End If

                End If

              Case (9 + Combo_Quartile_ChartData.Items.Count) To (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)
                ' Chart_Ranking

                If (DoRankingCharts) Then

                  Combo_Ranking_ChartData.SelectedIndex = (ChartCount - (9 + Combo_Quartile_ChartData.Items.Count))
                  Application.DoEvents()

                  ' ChartReport.Fields("Field_ChartTitle").Text = "Ranking - " & Combo_Ranking_ChartData.SelectedItem(Combo_Ranking_ChartData.DisplayMember)
                  ChartTitle = "Ranking - " & Combo_Ranking_ChartData.SelectedItem(Combo_Ranking_ChartData.DisplayMember)
                  ThisChart = Chart_Ranking
                  PaintRankingCharts(ThisChart)
                  ' 
                  If (Combo_Ranking_HighlightSeries.SelectedIndex > 0) Then
                    ChartReport.Fields("Field_HighlightSeries").Text = Combo_Ranking_HighlightSeries.SelectedItem(Combo_Ranking_HighlightSeries.DisplayMember).ToString
                    ChartReport.Fields("Field_HighlightSeriesLabel").Visible = True
                  End If

                End If

            End Select

            ' ****************************************************
            ' Add Chart to the report set, if Chart has been set.
            '
            ' ****************************************************

            If (ThisChart IsNot Nothing) Then

              ThisChart.Refresh()
              Application.DoEvents()

              Dim memStream As MemoryStream = New MemoryStream
              ThisChart.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

              If (MultiChartReport) Then
                ChartReport.Fields("Chart_Chart" & MultiChartCount.ToString).Picture = Image.FromStream(memStream)
                ChartReport.Fields("Chart_Chart" & MultiChartCount.ToString).Visible = True
                ChartReport.Fields("Field_ChartTitle" & MultiChartCount.ToString).Text = ChartTitle
                ChartPending = True
                MultiChartCount += 1

                ' Print Report if this is the Last report, or is a 'Sixth' report.

                If (ChartCount = (8 + Combo_Quartile_ChartData.Items.Count + Combo_Ranking_ChartData.Items.Count)) OrElse (MultiChartCount >= 6) Then

                  ChartReport.Render()
                  PageImageArray.AddRange(ChartReport.PageImages)
                  ChartPending = False
                  MultiChartCount = (-1)
                End If

              Else

                ChartReport.Fields("Chart_Chart").Picture = Image.FromStream(memStream)
                ChartReport.Fields("Field_ChartTitle").Text = ChartTitle

                ChartReport.Render()
                PageImageArray.AddRange(ChartReport.PageImages)

              End If

            End If

          End If ' (ChartReport isnot Nothing)

        Next

        ' Catch final MultiPage Report, if it has not been rendered already.

        If (MultiChartReport) AndAlso (ChartPending) Then
          ChartReport.Render()
          PageImageArray.AddRange(ChartReport.PageImages)
          ThisChart = Nothing
          MultiChartCount = 0
        End If

      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try

    Return PageImageArray

  End Function

  Private Sub ShowChartsReport(ByRef pPageImageArray As ArrayList)
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try

      Dim newForm As frmViewReport

      newForm = MainForm.New_GenoaForm(GenoaFormID.frmViewReport).Form

      ' Display Report Form.

      Try

        If Not (newForm Is Nothing) Then

          newForm.Opacity = 0

          newForm.ReportPreview.Document = pPageImageArray

          Dim OCount As Integer
          newForm.Show()

          If MainForm.EnableReportFadeIn Then

            Try
              Dim TStart As Date = Now()
              Dim TEnd As Date

              For OCount = 0 To 100 Step 5
                newForm.Opacity = OCount / 100
                Threading.Thread.Sleep(10)
              Next

              TEnd = Now()
              If (TEnd - TStart).TotalMilliseconds >= 1000 Then
                MainForm.DisableReportFadeIn()
              End If

            Catch ex As Exception
            End Try

          Else
            newForm.Opacity = 1.0#
          End If
        End If

      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try


  End Sub

  Private Sub Menu_DatePeriod_Daily_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_DatePeriod_Daily.Click
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Me.MenuSelectedStatsDataLimit = DealingPeriod.Daily

        Menu_DatePeriod_Daily.Checked = True
        Menu_DatePeriod_Weekly.Checked = False
        Menu_DatePeriod_Fortnightly.Checked = False
        Menu_DatePeriod_Monthly.Checked = False
        Menu_DatePeriod_Quarterly.Checked = False

        ' Repaint charts.

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        ' Repaint Stats tab.

        Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_DatePeriod_Weekly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_DatePeriod_Weekly.Click
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Me.MenuSelectedStatsDataLimit = DealingPeriod.Weekly

        Menu_DatePeriod_Daily.Checked = False
        Menu_DatePeriod_Weekly.Checked = True
        Menu_DatePeriod_Fortnightly.Checked = False
        Menu_DatePeriod_Monthly.Checked = False
        Menu_DatePeriod_Quarterly.Checked = False

        ' Repaint charts.

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        ' Repaint Stats tab.

        Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_DatePeriod_Fortnightly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_DatePeriod_Fortnightly.Click
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Me.MenuSelectedStatsDataLimit = DealingPeriod.Fortnightly

        Menu_DatePeriod_Daily.Checked = False
        Menu_DatePeriod_Weekly.Checked = False
        Menu_DatePeriod_Fortnightly.Checked = True
        Menu_DatePeriod_Monthly.Checked = False
        Menu_DatePeriod_Quarterly.Checked = False

        ' Repaint charts.

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        ' Repaint Stats tab.

        Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub MonthlyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_DatePeriod_Monthly.Click
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Me.MenuSelectedStatsDataLimit = DealingPeriod.Monthly

        Menu_DatePeriod_Daily.Checked = False
        Menu_DatePeriod_Weekly.Checked = False
        Menu_DatePeriod_Fortnightly.Checked = False
        Menu_DatePeriod_Monthly.Checked = True
        Menu_DatePeriod_Quarterly.Checked = False

        ' Repaint charts.

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        ' Repaint Stats tab.

        Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_DatePeriod_Quarterly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_DatePeriod_Quarterly.Click
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Me.MenuSelectedStatsDataLimit = DealingPeriod.Quarterly

        Menu_DatePeriod_Daily.Checked = False
        Menu_DatePeriod_Weekly.Checked = False
        Menu_DatePeriod_Fortnightly.Checked = False
        Menu_DatePeriod_Monthly.Checked = False
        Menu_DatePeriod_Quarterly.Checked = True

        ' Repaint charts.

        Call List_SelectItems_SelectedIndexChanged(List_SelectItems, New EventArgs)

        ' Repaint Stats tab.

        Call Combo_Statistics_HighlightSeries_SelectedIndexChanged(Combo_Statistics_HighlightSeries, New EventArgs)

      End If
    Catch ex As Exception
    End Try
  End Sub


  Private Sub Menu_Debug_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Debug.Click

    Dim SelectdIDs() As Integer = GetSelectedIDs(Me.List_SelectItems)
    Dim NetDeltas() As RenaissancePertracAndStatsGlobals.NetDeltaItemClass
    Dim InstrumentDates() As Date

    If (SelectdIDs IsNot Nothing) AndAlso (SelectdIDs.Length > 0) Then

      InstrumentDates = MainForm.StatFunctions.DateSeries(MenuSelectedStatsDataLimit, CULng(SelectdIDs(0)), False, MainForm.StatFunctions.AnnualPeriodCount(MenuSelectedStatsDataLimit), 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
      NetDeltas = MainForm.StatFunctions.NetDeltas(MenuSelectedStatsDataLimit, CULng(SelectdIDs(0)), False, MainForm.StatFunctions.AnnualPeriodCount(MenuSelectedStatsDataLimit), 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data, True)

      Dim FP As Integer = FreeFile()
      Dim thisFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
      Dim thisFileName As String = InputBox("FileName ?", "", "NetDeltas.csv")
      Dim pFileName As String = Path.Combine(thisFilePath, thisFileName)
      Dim ThisRowString As String
      Dim InstrumentIndex As Integer
      Dim DateIndex As Integer
      Dim InformationArray() As Object

      Try
        FileOpen(FP, pFileName, OpenMode.Output)

        ThisRowString = " ,"

        For InstrumentIndex = 0 To (NetDeltas.Length - 1)
          ThisRowString &= NetDeltas(InstrumentIndex).PertracId.ToString & ","

          If NetDeltas(InstrumentIndex).DeltaArray.Length <> InstrumentDates.Length Then
            MessageBox.Show("Data array length mismatch")
          End If
        Next

        PrintLine(FP, New Object() {ThisRowString})

        ThisRowString = " ,"

        For InstrumentIndex = 0 To (NetDeltas.Length - 1)
          InformationArray = MainForm.PertracData.GetInformationValues(CInt(NetDeltas(InstrumentIndex).PertracId))
          ThisRowString &= """" & Nz(InformationArray(PertracInformationFields.FundName), "").ToString & ""","
        Next

        PrintLine(FP, New Object() {ThisRowString})

        For DateIndex = 0 To (InstrumentDates.Length - 1)

          ThisRowString = InstrumentDates(DateIndex).ToOADate.ToString & ","

          For InstrumentIndex = 0 To (NetDeltas.Length - 1)
            ThisRowString &= NetDeltas(InstrumentIndex).DeltaArray(DateIndex).ToString("#,##0.0000####") & ","
          Next

          PrintLine(FP, New Object() {ThisRowString})

        Next

      Catch ex As Exception
      Finally
        Try
          FileClose(FP)
        Catch ex As Exception
        End Try
      End Try

    End If

  End Sub

End Class
