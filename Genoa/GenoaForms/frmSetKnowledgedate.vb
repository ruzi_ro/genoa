Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


Public Class frmSetKnowledgedate

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnSetKnowledgeDate As System.Windows.Forms.Button
  Friend WithEvents btnClose As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Radio_WholeDay As System.Windows.Forms.RadioButton
  Friend WithEvents Date_KnowledgeDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Radio_PreciseTime As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_HistoricKD As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_KDLive As System.Windows.Forms.RadioButton
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSetKnowledgeDate = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Date_KnowledgeDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Radio_WholeDay = New System.Windows.Forms.RadioButton
    Me.Radio_PreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_HistoricKD = New System.Windows.Forms.RadioButton
    Me.Radio_KDLive = New System.Windows.Forms.RadioButton
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Panel1.SuspendLayout()
    Me.Panel5.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.Location = New System.Drawing.Point(224, 149)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSetKnowledgeDate
    '
    Me.btnSetKnowledgeDate.Location = New System.Drawing.Point(52, 149)
    Me.btnSetKnowledgeDate.Name = "btnSetKnowledgeDate"
    Me.btnSetKnowledgeDate.Size = New System.Drawing.Size(148, 28)
    Me.btnSetKnowledgeDate.TabIndex = 5
    Me.btnSetKnowledgeDate.Text = "Set KnowledgeDate"
    '
    'btnClose
    '
    Me.btnClose.Location = New System.Drawing.Point(308, 149)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 8
    Me.btnClose.Text = "&Close"
    '
    'Date_KnowledgeDate
    '
    Me.Date_KnowledgeDate.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDate.Location = New System.Drawing.Point(180, 80)
    Me.Date_KnowledgeDate.Name = "Date_KnowledgeDate"
    Me.Date_KnowledgeDate.Size = New System.Drawing.Size(252, 20)
    Me.Date_KnowledgeDate.TabIndex = 9
    '
    'Label1
    '
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(8, 12)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(284, 24)
    Me.Label1.TabIndex = 42
    Me.Label1.Text = "Set Knowledgedate"
    '
    'Radio_WholeDay
    '
    Me.Radio_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_WholeDay.Location = New System.Drawing.Point(4, 4)
    Me.Radio_WholeDay.Name = "Radio_WholeDay"
    Me.Radio_WholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_WholeDay.TabIndex = 43
    Me.Radio_WholeDay.Text = "WholeDay"
    '
    'Radio_PreciseTime
    '
    Me.Radio_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_PreciseTime.Location = New System.Drawing.Point(128, 4)
    Me.Radio_PreciseTime.Name = "Radio_PreciseTime"
    Me.Radio_PreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_PreciseTime.TabIndex = 44
    Me.Radio_PreciseTime.Text = "Precise Time"
    '
    'Radio_HistoricKD
    '
    Me.Radio_HistoricKD.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_HistoricKD.Location = New System.Drawing.Point(8, 80)
    Me.Radio_HistoricKD.Name = "Radio_HistoricKD"
    Me.Radio_HistoricKD.Size = New System.Drawing.Size(148, 16)
    Me.Radio_HistoricKD.TabIndex = 45
    Me.Radio_HistoricKD.Text = "Historic KnowledgeDate"
    '
    'Radio_KDLive
    '
    Me.Radio_KDLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_KDLive.Location = New System.Drawing.Point(8, 44)
    Me.Radio_KDLive.Name = "Radio_KDLive"
    Me.Radio_KDLive.Size = New System.Drawing.Size(148, 16)
    Me.Radio_KDLive.TabIndex = 46
    Me.Radio_KDLive.Text = "'LIVE' KnowledgeDate"
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Panel2)
    Me.Panel1.Location = New System.Drawing.Point(8, 132)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(428, 2)
    Me.Panel1.TabIndex = 47
    '
    'Panel2
    '
    Me.Panel2.Location = New System.Drawing.Point(0, 116)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(428, 2)
    Me.Panel2.TabIndex = 48
    '
    'Panel5
    '
    Me.Panel5.Controls.Add(Me.Radio_PreciseTime)
    Me.Panel5.Controls.Add(Me.Radio_WholeDay)
    Me.Panel5.Location = New System.Drawing.Point(180, 104)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(248, 24)
    Me.Panel5.TabIndex = 51
    '
    'frmSetKnowledgedate
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(444, 190)
    Me.Controls.Add(Me.Panel5)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Radio_KDLive)
    Me.Controls.Add(Me.Radio_HistoricKD)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_KnowledgeDate)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSetKnowledgeDate)
    Me.Controls.Add(Me.btnCancel)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmSetKnowledgedate"
    Me.Text = "Set Knowledgedate"
    Me.Panel1.ResumeLayout(False)
    Me.Panel5.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Genoa form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Genoa form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = GenoaFormID.frmSetKnowledgeDate

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    ' Form Control Changed events

    AddHandler Radio_KDLive.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_WholeDay.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Date_KnowledgeDate.ValueChanged, AddressOf Me.FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

  Public Sub SetKnowledgeDate(ByVal pKnowledgeDate As Date)

    Call Form_Load(Me, New System.EventArgs)

    If pKnowledgeDate <= KNOWLEDGEDATE_NOW Then
      Me.Radio_KDLive.Checked = True
    Else
      Me.Radio_HistoricKD.Checked = True

      If (pKnowledgeDate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
        Me.Radio_WholeDay.Checked = True
        Date_KnowledgeDate.Value = pKnowledgeDate.Date
      Else
        Me.Radio_PreciseTime.Checked = True
        Date_KnowledgeDate.Value = pKnowledgeDate
      End If
    End If

    Application.DoEvents()

    Call btnSetKnowledgeDate_Click(Me, New EventArgs)

  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos


    ' Initialise controls

    Call GetFormData()

		MainForm.SetComboSelectionLengths(Me)

  End Sub

  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.NoCache_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        ' Form Control Changed events

        RemoveHandler Radio_KDLive.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_WholeDay.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_KnowledgeDate.ValueChanged, AddressOf Me.FormControlChanged

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'GenoaAutoUpdate' event of the main Genoa form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************

    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    ' Update form buttons to reflect form changes.
    ' *****************************************************************************

    If (Me.HasUpdatePermission) Then
      FormChanged = True
      Call SetButtonStatus()
    End If

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

 


#End Region

#Region " SetButton / Control Events (Form Specific Code) "

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.HasUpdatePermission) And (FormChanged = True) Then
      Me.btnSetKnowledgeDate.Enabled = True
      Me.btnCancel.Enabled = True
    Else
      Me.btnSetKnowledgeDate.Enabled = False
      Me.btnCancel.Enabled = False
    End If

  End Sub

  Private Sub GetFormData()
    ' *****************************************************************************
    ' Display the current Knowledgedate on the KD form.
    '
    ' *****************************************************************************

    If MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW Then
      ' If KD is Live, then ...

      Me.Radio_HistoricKD.Checked = False
      Me.Radio_KDLive.Checked = True

      Me.Date_KnowledgeDate.Value = Now.Date

      Me.Radio_WholeDay.Checked = True
    Else
      ' KD is not Live, set buttons appropriate to the given time.
      ' Assume that a time of 23:59:59 or later represents the Whole Day.

      Me.Radio_KDLive.Checked = False
      Me.Radio_HistoricKD.Checked = True

      Me.Date_KnowledgeDate.Value = MainForm.Main_Knowledgedate

      If MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
        Me.Radio_WholeDay.Checked = True
        Me.Radio_PreciseTime.Checked = False
      Else
        Me.Radio_PreciseTime.Checked = True
        Me.Radio_WholeDay.Checked = False
      End If
    End If

    Call SetButtonStatus()
  End Sub

  Private Sub Radio_KDType_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_KDLive.CheckedChanged, Radio_HistoricKD.CheckedChanged
    ' *****************************************************************************
    ' Update the form to reflect changes in the Live / Historic KD Radio Buttons.
    ' *****************************************************************************

    If Me.Radio_KDLive.Checked = True Then
      Me.Date_KnowledgeDate.Enabled = False
      Me.Radio_WholeDay.Enabled = False
      Me.Radio_PreciseTime.Enabled = False
    Else
      Me.Date_KnowledgeDate.Enabled = True
      Me.Radio_WholeDay.Enabled = True
      Me.Radio_PreciseTime.Enabled = True
    End If
  End Sub

  Private Sub Radio_WholeDay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_WholeDay.CheckedChanged, Radio_PreciseTime.CheckedChanged
    ' *****************************************************************************
    ' Update the form to reflect changes in the WholeDaye  / Precise-Time KD Radio Buttons.
    ' *****************************************************************************

    If Me.Radio_WholeDay.Checked = True Then
      Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
    Else
      Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
    End If
  End Sub

#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' *****************************************************************************
    ' Cancel Changes, redisplay form.
    ' *****************************************************************************

    FormChanged = False
    Call GetFormData()

  End Sub

  Private Sub btnSetKnowledgeDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetKnowledgeDate.Click
    ' *****************************************************************************
    ' Save Changes, if any, without prompting.
    ' *****************************************************************************

    If (FormChanged = True) Then

      If (Me.Radio_KDLive.Checked = True) Then
        ' LIVE Knowledgedate - By Radio Button

        MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW

      Else

        If IsDate(Me.Date_KnowledgeDate.Value) Then
          ' Valid Date field

          If CDate(Me.Date_KnowledgeDate.Value) <= KNOWLEDGEDATE_NOW Then
            ' Date is <= KNOWLEDGEDATE_NOW (1 Jan 1900), Use 'LIVE'

            Me.Radio_KDLive.Checked = True
            MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW
          Else
            ' Use given date

            If Me.Radio_WholeDay.Checked = True Then
              ' Set Knowledgedate to the last second of the day.

              MainForm.Main_Knowledgedate = CDate(CDate(Me.Date_KnowledgeDate.Value).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
            Else
              ' Set Knowledgedate to the precise time given.

              MainForm.Main_Knowledgedate = CDate(Me.Date_KnowledgeDate.Value)
            End If
          End If

        Else
          ' Invalid Date Field, use 'LIVE'

          Me.Radio_KDLive.Checked = True
          MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW
        End If
      End If

      FormChanged = False
      Call SetButtonStatus()

      Dim MessageString As String
      If MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW Then
        MessageString = "LIVE"
      Else
        If MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
          MessageString = MainForm.Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
        Else
          MessageString = MainForm.Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
        End If
      End If
      MessageBox.Show("KnowledgeDate changed to " & MessageString, "Changed KnowledgeDate", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

    End If

  End Sub


  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


End Class
