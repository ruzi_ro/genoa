Imports System.IO
Imports RenaissanceGlobals


Public Class frmViewChart
  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents Chart_Chart As Dundas.Charting.WinControl.Chart
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents UpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_MirrorChanges As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MenuPeriods As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Chart_C1Chart As C1.Win.C1Chart.C1Chart

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
		Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
		Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewChart))
		Me.Chart_Chart = New Dundas.Charting.WinControl.Chart
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.UpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_MirrorChanges = New System.Windows.Forms.ToolStripMenuItem
		Me.MenuPeriods = New System.Windows.Forms.ToolStripMenuItem
		Me.Chart_C1Chart = New C1.Win.C1Chart.C1Chart
		CType(Me.Chart_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.RootMenu.SuspendLayout()
		CType(Me.Chart_C1Chart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Chart_Chart
		'
		Me.Chart_Chart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_Chart.BackColor = System.Drawing.Color.Azure
		Me.Chart_Chart.BackGradientEndColor = System.Drawing.Color.SkyBlue
		Me.Chart_Chart.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
		Me.Chart_Chart.BorderLineColor = System.Drawing.Color.LightGray
		Me.Chart_Chart.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
		Me.Chart_Chart.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
		ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
		ChartArea1.AxisX.LabelStyle.Format = "Y"
		ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.LabelStyle.Format = "N0"
		ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.StartFromZero = False
		ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
		ChartArea1.BackColor = System.Drawing.Color.Transparent
		ChartArea1.BorderColor = System.Drawing.Color.DimGray
		ChartArea1.Name = "Default"
		Me.Chart_Chart.ChartAreas.Add(ChartArea1)
		Legend1.BackColor = System.Drawing.Color.Transparent
		Legend1.BorderColor = System.Drawing.Color.Transparent
		Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
		Legend1.DockToChartArea = "Default"
		Legend1.Enabled = False
		Legend1.Name = "Default"
		Me.Chart_Chart.Legends.Add(Legend1)
		Me.Chart_Chart.Location = New System.Drawing.Point(0, 24)
		Me.Chart_Chart.Margin = New System.Windows.Forms.Padding(1)
		Me.Chart_Chart.Name = "Chart_Chart"
		Me.Chart_Chart.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
		Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series1.BorderWidth = 2
		Series1.ChartType = "Line"
		Series1.CustomAttributes = "LabelStyle=Top"
		Series1.Name = "Series1"
		Series1.ShadowOffset = 1
		Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series2.BorderWidth = 2
		Series2.ChartType = "Line"
		Series2.CustomAttributes = "LabelStyle=Top"
		Series2.Name = "Series2"
		Series2.ShadowOffset = 1
		Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Me.Chart_Chart.Series.Add(Series1)
		Me.Chart_Chart.Series.Add(Series2)
		Me.Chart_Chart.Size = New System.Drawing.Size(333, 312)
		Me.Chart_Chart.TabIndex = 2
		Me.Chart_Chart.Text = "Chart2"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UpdatesToolStripMenuItem, Me.MenuPeriods})
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(333, 24)
		Me.RootMenu.TabIndex = 13
		Me.RootMenu.Text = "MenuStrip1"
		'
		'UpdatesToolStripMenuItem
		'
		Me.UpdatesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_MirrorChanges})
		Me.UpdatesToolStripMenuItem.Name = "UpdatesToolStripMenuItem"
		Me.UpdatesToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
		Me.UpdatesToolStripMenuItem.Text = "Updates"
		'
		'Menu_MirrorChanges
		'
		Me.Menu_MirrorChanges.Checked = True
		Me.Menu_MirrorChanges.CheckState = System.Windows.Forms.CheckState.Checked
		Me.Menu_MirrorChanges.Name = "Menu_MirrorChanges"
		Me.Menu_MirrorChanges.Size = New System.Drawing.Size(232, 22)
		Me.Menu_MirrorChanges.Text = "Mirror changes to parent chart"
		'
		'MenuPeriods
		'
		Me.MenuPeriods.Name = "MenuPeriods"
		Me.MenuPeriods.Size = New System.Drawing.Size(54, 20)
		Me.MenuPeriods.Text = "Periods"
		'
		'Chart_C1Chart
		'
		Me.Chart_C1Chart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_C1Chart.Location = New System.Drawing.Point(0, 24)
		Me.Chart_C1Chart.Name = "Chart_C1Chart"
		Me.Chart_C1Chart.PropBag = resources.GetString("Chart_C1Chart.PropBag")
		Me.Chart_C1Chart.Size = New System.Drawing.Size(333, 312)
		Me.Chart_C1Chart.TabIndex = 14
		Me.Chart_C1Chart.Visible = False
		'
		'frmViewChart
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(333, 335)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Chart_Chart)
		Me.Controls.Add(Me.Chart_C1Chart)
		Me.Name = "frmViewChart"
		Me.Text = "Genoa Chart"
		CType(Me.Chart_Chart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.RootMenu.ResumeLayout(False)
		Me.RootMenu.PerformLayout()
		CType(Me.Chart_C1Chart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Local Variable Declaration"

	Private WithEvents _MainForm As GenoaMain
	Private WithEvents _GenoaParentForm As Form

	Private _FormOpenFailed As Boolean

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	Private _FormChart As Object

	Private _ParameterList() As String

	Private _ParentChartList As ArrayList

	Private InPaint As Boolean

	Public Delegate Sub UpdateChartDelegate(ByVal UpdateChart As Object, ByVal ParameterList() As String)

	Private _UpdateChartSub As UpdateChartDelegate

	Private ChartContextMenuStrip As ContextMenuStrip

#End Region

#Region " Form Properties"

	Public ReadOnly Property DisplayedChart() As Object
		Get
			Return _FormChart
		End Get
	End Property

	Public Property ParentChartList() As ArrayList
		Get
			Return _ParentChartList
		End Get
		Set(ByVal value As ArrayList)
			_ParentChartList = value
		End Set
	End Property

	Public WriteOnly Property FormChart() As Object
		' *******************************************************************************
		' Set the Parent chart control.
		' Copy Format and Data from the parent control.
		'
		' *******************************************************************************

		'Get
		'	Return _FormChart
		'End Get

		Set(ByVal value As Object)

			Try

				If (TypeOf value Is Dundas.Charting.WinControl.Chart) Then
					Dim ThisChart As Dundas.Charting.WinControl.Chart = Nothing

					Try

						ThisChart = CType(value, Dundas.Charting.WinControl.Chart)
						_FormChart = Chart_Chart

						Chart_Chart.Visible = True
						Chart_C1Chart.Visible = False

						' Copy Chart details using Chart Serialisation.

						Dim myStream As New System.IO.MemoryStream()
						ThisChart.Serializer.Content = Dundas.Charting.WinControl.SerializationContent.All

						ThisChart.ChartAreas(0).AxisY.Enabled = Dundas.Charting.WinControl.AxisEnabled.False
						ThisChart.Serializer.Save(myStream)
						ThisChart.ChartAreas(0).AxisY.Enabled = Dundas.Charting.WinControl.AxisEnabled.True

						Chart_Chart.Serializer.Load(myStream)
						Chart_Chart.ChartAreas(0).AxisY.Enabled = Dundas.Charting.WinControl.AxisEnabled.True
						myStream.Dispose()

						' Since this does not seem to work properly, re-create the data series from the parent chart.

						Dim ThisSeries As Dundas.Charting.WinControl.Series
						Dim SeriesCounter As Integer
						Dim PointCounter As Integer
						Dim thisPoint As Dundas.Charting.WinControl.DataPoint

						Chart_Chart.Series.Clear()

						For SeriesCounter = 0 To (ThisChart.Series.Count - 1)

							ThisSeries = Chart_Chart.Series.Add("PS" & SeriesCounter.ToString)
							ThisSeries.XValueType = ThisChart.Series(SeriesCounter).XValueType
							ThisSeries.YValueType = ThisChart.Series(SeriesCounter).YValueType

							For PointCounter = 0 To (ThisChart.Series(SeriesCounter).Points.Count - 1)
								thisPoint = ThisChart.Series(SeriesCounter).Points(PointCounter)

								ThisSeries.Points.AddXY(thisPoint.XValue, thisPoint.YValues(0))
								If (thisPoint.ToolTip IsNot Nothing) AndAlso (thisPoint.ToolTip.Length > 0) Then
									ThisSeries.Points(ThisSeries.Points.Count - 1).ToolTip = thisPoint.ToolTip
								End If
							Next

							ThisSeries.Font = New Font("Arial", 8)
							ThisSeries.CustomAttributes = ThisChart.Series(SeriesCounter).CustomAttributes
							ThisSeries.ChartType = ThisChart.Series(SeriesCounter).ChartType
							ThisSeries.ShadowOffset = ThisChart.Series(SeriesCounter).ShadowOffset
							ThisSeries.LegendText = ThisChart.Series(SeriesCounter).LegendText
							ThisSeries.MarkerStyle = ThisChart.Series(SeriesCounter).MarkerStyle
							ThisSeries.MarkerSize = ThisChart.Series(SeriesCounter).MarkerSize
							ThisSeries.MarkerStep = ThisChart.Series(SeriesCounter).MarkerStep
							ThisSeries.MarkerColor = ThisChart.Series(SeriesCounter).MarkerColor
							ThisSeries.MarkerBorderColor = ThisChart.Series(SeriesCounter).MarkerBorderColor
							ThisSeries.Color = ThisChart.Series(SeriesCounter).Color
							ThisSeries.BackHatchStyle = ThisChart.Series(SeriesCounter).BackHatchStyle

							' Set Label Text

							If (ThisSeries.Points.Count > 0) Then
								ThisSeries.Points(ThisSeries.Points.Count - 1).Label = ThisChart.Series(SeriesCounter).Points(ThisChart.Series(SeriesCounter).Points.Count - 1).Label
								ThisSeries.ChartArea = Chart_Chart.ChartAreas(0).Name
							Else
								ThisSeries.ChartArea = ""
							End If

						Next
					Catch ex As Exception

					End Try

					Try
						If (ThisChart IsNot Nothing) Then

							' Set other chart details.

							Chart_Chart.ChartAreas(0).CursorX.UserEnabled = True
							Chart_Chart.ChartAreas(0).CursorX.UserSelection = True
							Chart_Chart.ChartAreas(0).AxisX.View.Zoomable = True
							Chart_Chart.ChartAreas(0).AxisX.View.ZoomReset(0)
							Chart_Chart.ChartAreas(0).AxisX.LabelStyle.Format = ThisChart.ChartAreas(0).AxisX.LabelStyle.Format

							Chart_Chart.Width = Me.Width - 7
							Chart_Chart.Height = Me.Height - (Chart_Chart.Top + 32)

							Chart_Chart.ChartAreas(0).AxisX.View.Zoom(ThisChart.ChartAreas(0).AxisX.View.Position, ThisChart.ChartAreas(0).AxisX.View.Size, ThisChart.ChartAreas(0).AxisX.View.SizeType)

							Chart_Chart.ResetAutoValues()

						End If

					Catch ex As Exception

					End Try

				ElseIf (TypeOf value Is C1.Win.C1Chart.C1Chart) Then

					Dim ThisChart As C1.Win.C1Chart.C1Chart

					Try
						ThisChart = CType(value, C1.Win.C1Chart.C1Chart)
						_FormChart = Chart_C1Chart

						Chart_Chart.Visible = False
						Chart_C1Chart.Visible = True

						Chart_C1Chart.LoadChartFromString(ThisChart.SaveChartToString())

						Chart_C1Chart.SetBounds(Chart_Chart.Left, Chart_Chart.Top, Me.Width - 7, Me.Height - (Chart_Chart.Top + 32))

					Catch ex As Exception

					End Try

				End If


			Catch ex As Exception
			End Try

		End Set
	End Property


	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)

		End Set
	End Property

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Me.Close()
	End Sub

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		Get
			Return _MainForm
		End Get
	End Property

	Public Property GenoaParentForm() As Form
		Get
			Return _GenoaParentForm
		End Get
		Set(ByVal value As Form)
			_GenoaParentForm = value
		End Set
	End Property

	Public Property UpdateChartSub() As UpdateChartDelegate
		Get
			Return _UpdateChartSub
		End Get
		Set(ByVal value As UpdateChartDelegate)
			_UpdateChartSub = value
		End Set
	End Property

	Public Property ParameterList() As String()
		Get
			Return _ParameterList
		End Get
		Set(ByVal value() As String)
			_ParameterList = value
		End Set
	End Property

	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

	End Sub

#End Region

	Public Sub New(ByRef pMainForm As GenoaMain)
		Me.New(pMainForm, Nothing)
	End Sub

	Public Sub New(ByRef pMainForm As GenoaMain, ByRef pParentForm As Form)
		Me.New()

		_MainForm = pMainForm
		_GenoaParentForm = pParentForm
		_UpdateChartSub = Nothing
		_ParameterList = Nothing

		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		ChartContextMenuStrip = New ContextMenuStrip
		ChartContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

		AddHandler ChartContextMenuStrip.Opening, AddressOf cms_Opening
		AddHandler Chart_C1Chart.Resize, AddressOf MainForm.C1Chart_Resize

		Chart_Chart.ContextMenuStrip = ChartContextMenuStrip
		Chart_Chart.Tag = Me
		Chart_C1Chart.ContextMenuStrip = ChartContextMenuStrip
		Chart_C1Chart.Tag = Me

	End Sub

	Private Sub frmViewChart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Try

			InPaint = True
			Call SetPeriodCombo()
			MainForm.C1Chart_Resize(Chart_C1Chart, New EventArgs)

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

	Private Sub ParentClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles _GenoaParentForm.FormClosed
		' ***********************************************
		' Close this Form if the Parent Form closes.
		'
		' ***********************************************

		Me.Close()
	End Sub

	Private Sub frmViewChart_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ***********************************************
		' Tidy up the parent chart list array to stop future updates.
		'
		' ***********************************************

		Try
			MainForm.RemoveFromFormsCollection(Me)
		Catch ex As Exception
		End Try

		Try
			Chart_Chart.Visible = False

			If (_ParentChartList IsNot Nothing) Then
				SyncLock _ParentChartList
					If _ParentChartList.Contains(_FormChart) Then
						_ParentChartList.Remove(_FormChart)
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try

		Try
			Chart_Chart.Tag = Nothing
			Chart_C1Chart.Tag = Nothing
		Catch ex As Exception
		End Try

	End Sub

	Private Sub frmViewChart_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		' ***********************************************
		' Tidy up the parent chart list array to stop future updates.
		'
		' ***********************************************

		Try
			Chart_Chart.Visible = False

			If (_ParentChartList IsNot Nothing) Then
				SyncLock _ParentChartList
					If _ParentChartList.Contains(_FormChart) Then
						_ParentChartList.Remove(_FormChart)
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try

		Try
			Chart_Chart.Tag = Nothing
			Chart_C1Chart.Tag = Nothing
		Catch ex As Exception
		End Try

	End Sub

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True

		Try
			KnowledgeDateChanged = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPeriod) = True) Or KnowledgeDateChanged Then

				' Re-Set combo.
				Call Me.SetPeriodCombo()

			End If

		Catch ex As Exception
		Finally
			InPaint = OrgInPaint
		End Try

	End Sub

	Private Sub Menu_MirrorChanges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Menu_MirrorChanges.Click
		Menu_MirrorChanges.Checked = Not Menu_MirrorChanges.Checked
	End Sub

	Private Sub Menu_MirrorChanges_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_MirrorChanges.CheckedChanged
		' ***********************************************
		' Enable / Disable chart updates by adding / Removing 
		' The Chart_Chart control from / to the registered
		' Chart List Array.
		'
		' ***********************************************
		Try
			If Me.Menu_MirrorChanges.Checked Then
				If (Me._ParentChartList IsNot Nothing) Then
					SyncLock _ParentChartList
						If (_ParentChartList.Contains(_FormChart) = False) Then
							_ParentChartList.Add(_FormChart)
						End If
					End SyncLock

					' refresh chart.
					If (_UpdateChartSub Is Nothing) Then
						Me.FormChart = Me._FormChart
					Else
						_UpdateChartSub(_FormChart, _ParameterList)
					End If

				End If
			Else
				If (Me._ParentChartList IsNot Nothing) Then
					SyncLock _ParentChartList
						If _ParentChartList.Contains(_FormChart) Then
							_ParentChartList.Remove(_FormChart)
						End If
					End SyncLock
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub SetPeriodCombo()
		Dim PeriodDS As RenaissanceDataClass.DSPeriod
		Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
		Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow
		Dim thisRow As RenaissanceDataClass.DSPeriod.tblPeriodRow

		Dim ParentMenuItem As ToolStripMenuItem
		Dim newMenuItem As ToolStripMenuItem

		PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
		PeriodTbl = PeriodDS.tblPeriod
		SelectedRows = PeriodTbl.Select("True", "PeriodTitle")

		ParentMenuItem = Me.MenuPeriods
		ParentMenuItem.DropDownItems.Clear()

		For Each thisRow In SelectedRows
			newMenuItem = ParentMenuItem.DropDownItems.Add(thisRow.PeriodTitle, Nothing, AddressOf ToolStripCombo_Periods_Click)
			newMenuItem.Tag = thisRow.PeriodID
		Next

	End Sub

	Private Sub ToolStripCombo_Periods_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *********************************************************************************************************
		'
		'
		' *********************************************************************************************************

		Try

			If InPaint = False Then
				Try
					Dim thisPeriodID As Integer
					Dim SelectedMenuItem As ToolStripMenuItem
					Dim StartDate As Date
					Dim EndDate As Date
					Dim PeriodDS As RenaissanceDataClass.DSPeriod
					Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
					Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow

					SelectedMenuItem = CType(sender, System.Windows.Forms.ToolStripMenuItem)
					If (SelectedMenuItem IsNot Nothing) AndAlso (IsNumeric(SelectedMenuItem.Tag)) Then
						thisPeriodID = CInt(SelectedMenuItem.Tag)

						If (thisPeriodID > 0) Then
							PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
							PeriodTbl = PeriodDS.tblPeriod
							SelectedRows = PeriodTbl.Select("PeriodID=" & thisPeriodID.ToString)

							If (SelectedRows.Length > 0) Then
								StartDate = SelectedRows(0).PeriodDateFrom
								EndDate = SelectedRows(0).PeriodDateTo

								If (Chart_Chart.Visible) Then

									Chart_Chart.ChartAreas(0).AxisX.View.Position = StartDate.ToOADate
									Chart_Chart.ChartAreas(0).AxisX.View.Size = (Me.Chart_Chart.ChartAreas(0).AxisX.ValueToPosition(EndDate.ToOADate) - Me.Chart_Chart.ChartAreas(0).AxisX.ValueToPosition(StartDate.ToOADate)) + 1

								ElseIf (Chart_C1Chart.Visible) Then
									If (EndDate > Now.Date) Then
										EndDate = Now.Date.AddMonths(1)
										EndDate = EndDate.AddDays(0 - EndDate.Day)
									End If

									Chart_C1Chart.ChartArea.AxisX.Min = (StartDate.AddMonths(-1)).ToOADate
									Chart_C1Chart.ChartArea.AxisX.Max = (EndDate.AddMonths(-1)).ToOADate

								End If

							End If
						End If
					End If
				Catch ex As Exception
				End Try
			End If

		Catch ex As Exception
		End Try
	End Sub

	Sub cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************


		' Add custom item (Form)

		e.Cancel = False

		ChartContextMenuStrip.Items.Clear()
		ChartContextMenuStrip.Items.Add("Copy to clipboard", Nothing, AddressOf Menu_CopyToClipboard)
		ChartContextMenuStrip.Items.Add("Zoom Reset", Nothing, AddressOf Menu_ZoomReset)

	End Sub

	Private Sub Menu_CopyToClipboard(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		'Dim memStream As MemoryStream = New MemoryStream
		'Chart_Chart.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

		'My.Computer.Clipboard.SetImage(Image.FromStream(memStream))

		Dim memStream As MemoryStream = New MemoryStream

		If (TypeOf _FormChart Is Dundas.Charting.WinControl.Chart) Then

			CType(_FormChart, Dundas.Charting.WinControl.Chart).SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

		ElseIf (TypeOf _FormChart Is C1.Win.C1Chart.C1Chart) Then

			CType(_FormChart, C1.Win.C1Chart.C1Chart).SaveImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

		End If

		My.Computer.Clipboard.SetImage(Image.FromStream(memStream))

	End Sub

	Private Sub Menu_ZoomReset(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			If (TypeOf _FormChart Is Dundas.Charting.WinControl.Chart) Then
				_FormChart.ChartAreas(0).AxisX.View.ZoomReset(0)
				_FormChart.ChartAreas(0).CursorX.Position = -999
				_FormChart.ChartAreas(0).AxisY.View.ZoomReset(0)
				_FormChart.ChartAreas(0).CursorY.Position = -999

			ElseIf (TypeOf _FormChart Is C1.Win.C1Chart.C1Chart) Then

			End If

		Catch ex As Exception
		End Try

	End Sub


End Class
