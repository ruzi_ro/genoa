

Public Class frmViewReport
	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	Friend WithEvents ReportPreview As C1.Win.C1Preview.C1PrintPreviewControl

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.ReportPreview = New C1.Win.C1Preview.C1PrintPreviewControl
		CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.ReportPreview.SuspendLayout()
		Me.SuspendLayout()
		'
		'ReportPreview
		'
		Me.ReportPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ReportPreview.Location = New System.Drawing.Point(1, 1)
		Me.ReportPreview.Name = "ReportPreview"
		'
		'ReportPreview.PreviewPane
		'
		Me.ReportPreview.PreviewPane.ExportOptions.Content = New C1.Win.C1Preview.ExporterOptions(-1) {}
		Me.ReportPreview.PreviewPane.IntegrateExternalTools = True
		Me.ReportPreview.PreviewPane.TabIndex = 0
		Me.ReportPreview.Size = New System.Drawing.Size(885, 728)
		Me.ReportPreview.TabIndex = 0
		'
		'frmViewReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(888, 729)
		Me.Controls.Add(Me.ReportPreview)
		Me.Name = "frmViewReport"
		Me.Text = "Genoa Report"
		CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ReportPreview.ResumeLayout(False)
		Me.ReportPreview.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

#End Region

#Region " Local Variable Declaration"

	Private _MainForm As GenoaMain
	Private _FormOpenFailed As Boolean

	' Form ToolTip
	Private FormTooltip As New ToolTip()

#End Region

#Region " Form Properties"

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)

		End Set
	End Property

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Me.Close()

	End Sub

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		Get
			Return _MainForm
		End Get
	End Property

	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		ReportPreview.Document = Nothing
	End Sub

	Public Property ReportDocument() As System.Drawing.Printing.PrintDocument
		Get
			Return ReportPreview.Document
		End Get
		Set(ByVal Value As System.Drawing.Printing.PrintDocument)
			ReportPreview.Document = Value
		End Set
	End Property

#End Region

	Public Sub New(ByRef pMainForm As GenoaMain)
		Me.New()

		_MainForm = pMainForm
		_FormOpenFailed = False

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub


	Private Sub frmViewReport_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

		Try
			If (ReportPreview.PreviewPane.Busy) Then
				e.Cancel = True
				Exit Sub
			End If

			MainForm.RemoveFromFormsCollection(Me)
		Catch ex As Exception
		End Try

	End Sub

End Class
