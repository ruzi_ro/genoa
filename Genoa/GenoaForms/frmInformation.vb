Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass


Public Class frmInformation

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents Edit_F1 As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectMasterName As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents Label_F1 As System.Windows.Forms.Label
	Friend WithEvents Panel_InformationEdit As System.Windows.Forms.Panel
	Friend WithEvents Edit_FUserDescription As System.Windows.Forms.TextBox
	Friend WithEvents Label_FUserDescription As System.Windows.Forms.Label
	Friend WithEvents Edit_FDescription As System.Windows.Forms.TextBox
	Friend WithEvents Label_FDescription As System.Windows.Forms.Label
	Friend WithEvents Edit_F41 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F41 As System.Windows.Forms.Label
	Friend WithEvents Edit_F40 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F40 As System.Windows.Forms.Label
	Friend WithEvents Edit_F39 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F39 As System.Windows.Forms.Label
	Friend WithEvents Edit_F38 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F38 As System.Windows.Forms.Label
	Friend WithEvents Edit_F37 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F37 As System.Windows.Forms.Label
	Friend WithEvents Edit_F36 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F36 As System.Windows.Forms.Label
	Friend WithEvents Edit_F35 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F35 As System.Windows.Forms.Label
	Friend WithEvents Edit_F34 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F34 As System.Windows.Forms.Label
	Friend WithEvents Edit_F33 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F33 As System.Windows.Forms.Label
	Friend WithEvents Edit_F32 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F32 As System.Windows.Forms.Label
	Friend WithEvents Edit_F31 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F31 As System.Windows.Forms.Label
	Friend WithEvents Edit_F30 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F30 As System.Windows.Forms.Label
	Friend WithEvents Edit_F29 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F29 As System.Windows.Forms.Label
	Friend WithEvents Edit_F28 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F28 As System.Windows.Forms.Label
	Friend WithEvents Edit_F27 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F27 As System.Windows.Forms.Label
	Friend WithEvents Edit_F26 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F26 As System.Windows.Forms.Label
	Friend WithEvents Edit_F25 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F25 As System.Windows.Forms.Label
	Friend WithEvents Edit_F24 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F24 As System.Windows.Forms.Label
	Friend WithEvents Edit_F23 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F23 As System.Windows.Forms.Label
	Friend WithEvents Edit_F22 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F22 As System.Windows.Forms.Label
	Friend WithEvents Edit_F21 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F21 As System.Windows.Forms.Label
	Friend WithEvents Edit_F20 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F20 As System.Windows.Forms.Label
	Friend WithEvents Edit_F19 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F19 As System.Windows.Forms.Label
	Friend WithEvents Edit_F18 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F18 As System.Windows.Forms.Label
	Friend WithEvents Edit_F17 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F17 As System.Windows.Forms.Label
	Friend WithEvents Edit_F16 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F16 As System.Windows.Forms.Label
	Friend WithEvents Edit_F15 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F15 As System.Windows.Forms.Label
	Friend WithEvents Edit_F14 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F14 As System.Windows.Forms.Label
	Friend WithEvents Edit_F13 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F13 As System.Windows.Forms.Label
	Friend WithEvents Edit_F12 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F12 As System.Windows.Forms.Label
	Friend WithEvents Edit_F11 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F11 As System.Windows.Forms.Label
	Friend WithEvents Edit_F10 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F10 As System.Windows.Forms.Label
	Friend WithEvents Edit_F9 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F9 As System.Windows.Forms.Label
	Friend WithEvents Edit_F8 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F8 As System.Windows.Forms.Label
	Friend WithEvents Edit_F7 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F7 As System.Windows.Forms.Label
	Friend WithEvents Edit_F6 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F6 As System.Windows.Forms.Label
	Friend WithEvents Edit_F5 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F5 As System.Windows.Forms.Label
	Friend WithEvents Edit_F4 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F4 As System.Windows.Forms.Label
	Friend WithEvents Edit_F3 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F3 As System.Windows.Forms.Label
	Friend WithEvents Edit_F2 As System.Windows.Forms.TextBox
	Friend WithEvents Label_F2 As System.Windows.Forms.Label
	Friend WithEvents Label51 As System.Windows.Forms.Label
	Friend WithEvents editDataVendor As System.Windows.Forms.TextBox
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.Edit_F1 = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectMasterName = New FCP_TelerikControls.FCP_RadComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Label_F1 = New System.Windows.Forms.Label
		Me.Panel_InformationEdit = New System.Windows.Forms.Panel
		Me.Label51 = New System.Windows.Forms.Label
		Me.Edit_FUserDescription = New System.Windows.Forms.TextBox
		Me.Label_FUserDescription = New System.Windows.Forms.Label
		Me.Edit_FDescription = New System.Windows.Forms.TextBox
		Me.Label_FDescription = New System.Windows.Forms.Label
		Me.Edit_F41 = New System.Windows.Forms.TextBox
		Me.Label_F41 = New System.Windows.Forms.Label
		Me.Edit_F40 = New System.Windows.Forms.TextBox
		Me.Label_F40 = New System.Windows.Forms.Label
		Me.Edit_F39 = New System.Windows.Forms.TextBox
		Me.Label_F39 = New System.Windows.Forms.Label
		Me.Edit_F38 = New System.Windows.Forms.TextBox
		Me.Label_F38 = New System.Windows.Forms.Label
		Me.Edit_F37 = New System.Windows.Forms.TextBox
		Me.Label_F37 = New System.Windows.Forms.Label
		Me.Edit_F36 = New System.Windows.Forms.TextBox
		Me.Label_F36 = New System.Windows.Forms.Label
		Me.Edit_F35 = New System.Windows.Forms.TextBox
		Me.Label_F35 = New System.Windows.Forms.Label
		Me.Edit_F34 = New System.Windows.Forms.TextBox
		Me.Label_F34 = New System.Windows.Forms.Label
		Me.Edit_F33 = New System.Windows.Forms.TextBox
		Me.Label_F33 = New System.Windows.Forms.Label
		Me.Edit_F32 = New System.Windows.Forms.TextBox
		Me.Label_F32 = New System.Windows.Forms.Label
		Me.Edit_F31 = New System.Windows.Forms.TextBox
		Me.Label_F31 = New System.Windows.Forms.Label
		Me.Edit_F30 = New System.Windows.Forms.TextBox
		Me.Label_F30 = New System.Windows.Forms.Label
		Me.Edit_F29 = New System.Windows.Forms.TextBox
		Me.Label_F29 = New System.Windows.Forms.Label
		Me.Edit_F28 = New System.Windows.Forms.TextBox
		Me.Label_F28 = New System.Windows.Forms.Label
		Me.Edit_F27 = New System.Windows.Forms.TextBox
		Me.Label_F27 = New System.Windows.Forms.Label
		Me.Edit_F26 = New System.Windows.Forms.TextBox
		Me.Label_F26 = New System.Windows.Forms.Label
		Me.Edit_F25 = New System.Windows.Forms.TextBox
		Me.Label_F25 = New System.Windows.Forms.Label
		Me.Edit_F24 = New System.Windows.Forms.TextBox
		Me.Label_F24 = New System.Windows.Forms.Label
		Me.Edit_F23 = New System.Windows.Forms.TextBox
		Me.Label_F23 = New System.Windows.Forms.Label
		Me.Edit_F22 = New System.Windows.Forms.TextBox
		Me.Label_F22 = New System.Windows.Forms.Label
		Me.Edit_F21 = New System.Windows.Forms.TextBox
		Me.Label_F21 = New System.Windows.Forms.Label
		Me.Edit_F20 = New System.Windows.Forms.TextBox
		Me.Label_F20 = New System.Windows.Forms.Label
		Me.Edit_F19 = New System.Windows.Forms.TextBox
		Me.Label_F19 = New System.Windows.Forms.Label
		Me.Edit_F18 = New System.Windows.Forms.TextBox
		Me.Label_F18 = New System.Windows.Forms.Label
		Me.Edit_F17 = New System.Windows.Forms.TextBox
		Me.Label_F17 = New System.Windows.Forms.Label
		Me.Edit_F16 = New System.Windows.Forms.TextBox
		Me.Label_F16 = New System.Windows.Forms.Label
		Me.Edit_F15 = New System.Windows.Forms.TextBox
		Me.Label_F15 = New System.Windows.Forms.Label
		Me.Edit_F14 = New System.Windows.Forms.TextBox
		Me.Label_F14 = New System.Windows.Forms.Label
		Me.Edit_F13 = New System.Windows.Forms.TextBox
		Me.Label_F13 = New System.Windows.Forms.Label
		Me.Edit_F12 = New System.Windows.Forms.TextBox
		Me.Label_F12 = New System.Windows.Forms.Label
		Me.Edit_F11 = New System.Windows.Forms.TextBox
		Me.Label_F11 = New System.Windows.Forms.Label
		Me.Edit_F10 = New System.Windows.Forms.TextBox
		Me.Label_F10 = New System.Windows.Forms.Label
		Me.Edit_F9 = New System.Windows.Forms.TextBox
		Me.Label_F9 = New System.Windows.Forms.Label
		Me.Edit_F8 = New System.Windows.Forms.TextBox
		Me.Label_F8 = New System.Windows.Forms.Label
		Me.Edit_F7 = New System.Windows.Forms.TextBox
		Me.Label_F7 = New System.Windows.Forms.Label
		Me.Edit_F6 = New System.Windows.Forms.TextBox
		Me.Label_F6 = New System.Windows.Forms.Label
		Me.Edit_F5 = New System.Windows.Forms.TextBox
		Me.Label_F5 = New System.Windows.Forms.Label
		Me.Edit_F4 = New System.Windows.Forms.TextBox
		Me.Label_F4 = New System.Windows.Forms.Label
		Me.Edit_F3 = New System.Windows.Forms.TextBox
		Me.Label_F3 = New System.Windows.Forms.Label
		Me.Edit_F2 = New System.Windows.Forms.TextBox
		Me.Label_F2 = New System.Windows.Forms.Label
		Me.editDataVendor = New System.Windows.Forms.TextBox
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
		Me.Panel_InformationEdit.SuspendLayout()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(941, 33)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(50, 20)
		Me.editAuditID.TabIndex = 1
		'
		'Edit_F1
		'
		Me.Edit_F1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F1.Location = New System.Drawing.Point(121, 5)
		Me.Edit_F1.MaxLength = 100
		Me.Edit_F1.Name = "Edit_F1"
		Me.Edit_F1.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F1.TabIndex = 0
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(434, 632)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 6
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(349, 632)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 4
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectMasterName
		'
    Me.Combo_SelectMasterName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectMasterName.FormattingEnabled = True
    Me.Combo_SelectMasterName.Location = New System.Drawing.Point(121, 33)
    Me.Combo_SelectMasterName.MasternameCollection = Nothing
    Me.Combo_SelectMasterName.Name = "Combo_SelectMasterName"
    '
    '
    '
    Me.Combo_SelectMasterName.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_SelectMasterName.SelectNoMatch = False
    Me.Combo_SelectMasterName.Size = New System.Drawing.Size(669, 20)
    Me.Combo_SelectMasterName.TabIndex = 0
    Me.Combo_SelectMasterName.ThemeName = "ControlDefault"
    '
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Location = New System.Drawing.Point(131, 623)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(176, 48)
		Me.Panel1.TabIndex = 3
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(6, 33)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 18
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(5, 59)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(988, 10)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(518, 632)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 7
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(1004, 24)
		Me.RootMenu.TabIndex = 9
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Label_F1
		'
		Me.Label_F1.Location = New System.Drawing.Point(6, 8)
		Me.Label_F1.Name = "Label_F1"
		Me.Label_F1.Size = New System.Drawing.Size(111, 17)
		Me.Label_F1.TabIndex = 86
		Me.Label_F1.Text = "F1"
		'
		'Panel_InformationEdit
		'
		Me.Panel_InformationEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel_InformationEdit.AutoScroll = True
		Me.Panel_InformationEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_InformationEdit.Controls.Add(Me.Label51)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_FUserDescription)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_FUserDescription)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_FDescription)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_FDescription)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F41)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F41)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F40)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F40)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F39)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F39)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F38)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F38)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F37)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F37)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F36)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F36)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F35)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F35)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F34)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F34)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F33)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F33)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F32)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F32)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F31)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F31)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F30)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F30)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F29)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F29)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F28)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F28)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F27)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F27)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F26)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F26)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F25)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F25)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F24)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F24)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F23)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F23)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F22)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F22)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F21)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F21)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F20)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F20)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F19)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F19)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F18)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F18)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F17)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F17)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F16)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F16)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F15)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F15)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F14)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F14)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F13)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F13)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F12)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F12)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F11)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F11)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F10)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F10)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F9)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F9)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F8)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F8)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F7)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F7)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F6)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F6)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F5)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F5)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F4)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F4)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F3)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F3)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F2)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F2)
		Me.Panel_InformationEdit.Controls.Add(Me.Edit_F1)
		Me.Panel_InformationEdit.Controls.Add(Me.Label_F1)
		Me.Panel_InformationEdit.Location = New System.Drawing.Point(9, 78)
		Me.Panel_InformationEdit.Name = "Panel_InformationEdit"
		Me.Panel_InformationEdit.Size = New System.Drawing.Size(988, 529)
		Me.Panel_InformationEdit.TabIndex = 2
		'
		'Label51
		'
		Me.Label51.Location = New System.Drawing.Point(6, 1270)
		Me.Label51.Name = "Label51"
		Me.Label51.Size = New System.Drawing.Size(111, 17)
		Me.Label51.TabIndex = 187
		Me.Label51.Text = " "
		'
		'Edit_FUserDescription
		'
		Me.Edit_FUserDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_FUserDescription.Location = New System.Drawing.Point(121, 1177)
		Me.Edit_FUserDescription.MaxLength = 100
		Me.Edit_FUserDescription.Multiline = True
		Me.Edit_FUserDescription.Name = "Edit_FUserDescription"
		Me.Edit_FUserDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.Edit_FUserDescription.Size = New System.Drawing.Size(832, 100)
		Me.Edit_FUserDescription.TabIndex = 42
		'
		'Label_FUserDescription
		'
		Me.Label_FUserDescription.Location = New System.Drawing.Point(6, 1180)
		Me.Label_FUserDescription.Name = "Label_FUserDescription"
		Me.Label_FUserDescription.Size = New System.Drawing.Size(111, 17)
		Me.Label_FUserDescription.TabIndex = 171
		Me.Label_FUserDescription.Text = "User Description"
		'
		'Edit_FDescription
		'
		Me.Edit_FDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_FDescription.Location = New System.Drawing.Point(121, 1071)
		Me.Edit_FDescription.MaxLength = 100
		Me.Edit_FDescription.Multiline = True
		Me.Edit_FDescription.Name = "Edit_FDescription"
		Me.Edit_FDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.Edit_FDescription.Size = New System.Drawing.Size(832, 100)
		Me.Edit_FDescription.TabIndex = 41
		'
		'Label_FDescription
		'
		Me.Label_FDescription.Location = New System.Drawing.Point(6, 1074)
		Me.Label_FDescription.Name = "Label_FDescription"
		Me.Label_FDescription.Size = New System.Drawing.Size(111, 17)
		Me.Label_FDescription.TabIndex = 169
		Me.Label_FDescription.Text = "Description"
		'
		'Edit_F41
		'
		Me.Edit_F41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F41.Location = New System.Drawing.Point(121, 1045)
		Me.Edit_F41.MaxLength = 100
		Me.Edit_F41.Name = "Edit_F41"
		Me.Edit_F41.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F41.TabIndex = 40
		'
		'Label_F41
		'
		Me.Label_F41.Location = New System.Drawing.Point(6, 1048)
		Me.Label_F41.Name = "Label_F41"
		Me.Label_F41.Size = New System.Drawing.Size(111, 17)
		Me.Label_F41.TabIndex = 167
		Me.Label_F41.Text = "F41"
		'
		'Edit_F40
		'
		Me.Edit_F40.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F40.Location = New System.Drawing.Point(121, 1019)
		Me.Edit_F40.MaxLength = 100
		Me.Edit_F40.Name = "Edit_F40"
		Me.Edit_F40.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F40.TabIndex = 39
		'
		'Label_F40
		'
		Me.Label_F40.Location = New System.Drawing.Point(6, 1022)
		Me.Label_F40.Name = "Label_F40"
		Me.Label_F40.Size = New System.Drawing.Size(111, 17)
		Me.Label_F40.TabIndex = 165
		Me.Label_F40.Text = "F40"
		'
		'Edit_F39
		'
		Me.Edit_F39.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F39.Location = New System.Drawing.Point(121, 993)
		Me.Edit_F39.MaxLength = 100
		Me.Edit_F39.Name = "Edit_F39"
		Me.Edit_F39.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F39.TabIndex = 38
		'
		'Label_F39
		'
		Me.Label_F39.Location = New System.Drawing.Point(6, 996)
		Me.Label_F39.Name = "Label_F39"
		Me.Label_F39.Size = New System.Drawing.Size(111, 17)
		Me.Label_F39.TabIndex = 163
		Me.Label_F39.Text = "F39"
		'
		'Edit_F38
		'
		Me.Edit_F38.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F38.Location = New System.Drawing.Point(121, 967)
		Me.Edit_F38.MaxLength = 100
		Me.Edit_F38.Name = "Edit_F38"
		Me.Edit_F38.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F38.TabIndex = 37
		'
		'Label_F38
		'
		Me.Label_F38.Location = New System.Drawing.Point(6, 970)
		Me.Label_F38.Name = "Label_F38"
		Me.Label_F38.Size = New System.Drawing.Size(111, 17)
		Me.Label_F38.TabIndex = 161
		Me.Label_F38.Text = "F38"
		'
		'Edit_F37
		'
		Me.Edit_F37.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F37.Location = New System.Drawing.Point(121, 941)
		Me.Edit_F37.MaxLength = 100
		Me.Edit_F37.Name = "Edit_F37"
		Me.Edit_F37.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F37.TabIndex = 36
		'
		'Label_F37
		'
		Me.Label_F37.Location = New System.Drawing.Point(6, 944)
		Me.Label_F37.Name = "Label_F37"
		Me.Label_F37.Size = New System.Drawing.Size(111, 17)
		Me.Label_F37.TabIndex = 159
		Me.Label_F37.Text = "F37"
		'
		'Edit_F36
		'
		Me.Edit_F36.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F36.Location = New System.Drawing.Point(121, 915)
		Me.Edit_F36.MaxLength = 100
		Me.Edit_F36.Name = "Edit_F36"
		Me.Edit_F36.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F36.TabIndex = 35
		'
		'Label_F36
		'
		Me.Label_F36.Location = New System.Drawing.Point(6, 918)
		Me.Label_F36.Name = "Label_F36"
		Me.Label_F36.Size = New System.Drawing.Size(111, 17)
		Me.Label_F36.TabIndex = 157
		Me.Label_F36.Text = "F36"
		'
		'Edit_F35
		'
		Me.Edit_F35.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F35.Location = New System.Drawing.Point(121, 889)
		Me.Edit_F35.MaxLength = 100
		Me.Edit_F35.Name = "Edit_F35"
		Me.Edit_F35.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F35.TabIndex = 34
		'
		'Label_F35
		'
		Me.Label_F35.Location = New System.Drawing.Point(6, 892)
		Me.Label_F35.Name = "Label_F35"
		Me.Label_F35.Size = New System.Drawing.Size(111, 17)
		Me.Label_F35.TabIndex = 155
		Me.Label_F35.Text = "F35"
		'
		'Edit_F34
		'
		Me.Edit_F34.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F34.Location = New System.Drawing.Point(121, 863)
		Me.Edit_F34.MaxLength = 100
		Me.Edit_F34.Name = "Edit_F34"
		Me.Edit_F34.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F34.TabIndex = 33
		'
		'Label_F34
		'
		Me.Label_F34.Location = New System.Drawing.Point(6, 866)
		Me.Label_F34.Name = "Label_F34"
		Me.Label_F34.Size = New System.Drawing.Size(111, 17)
		Me.Label_F34.TabIndex = 153
		Me.Label_F34.Text = "F34"
		'
		'Edit_F33
		'
		Me.Edit_F33.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F33.Location = New System.Drawing.Point(121, 837)
		Me.Edit_F33.MaxLength = 100
		Me.Edit_F33.Name = "Edit_F33"
		Me.Edit_F33.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F33.TabIndex = 32
		'
		'Label_F33
		'
		Me.Label_F33.Location = New System.Drawing.Point(6, 840)
		Me.Label_F33.Name = "Label_F33"
		Me.Label_F33.Size = New System.Drawing.Size(111, 17)
		Me.Label_F33.TabIndex = 151
		Me.Label_F33.Text = "F33"
		'
		'Edit_F32
		'
		Me.Edit_F32.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F32.Location = New System.Drawing.Point(121, 811)
		Me.Edit_F32.MaxLength = 100
		Me.Edit_F32.Name = "Edit_F32"
		Me.Edit_F32.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F32.TabIndex = 31
		'
		'Label_F32
		'
		Me.Label_F32.Location = New System.Drawing.Point(6, 814)
		Me.Label_F32.Name = "Label_F32"
		Me.Label_F32.Size = New System.Drawing.Size(111, 17)
		Me.Label_F32.TabIndex = 149
		Me.Label_F32.Text = "F32"
		'
		'Edit_F31
		'
		Me.Edit_F31.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F31.Location = New System.Drawing.Point(121, 785)
		Me.Edit_F31.MaxLength = 100
		Me.Edit_F31.Name = "Edit_F31"
		Me.Edit_F31.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F31.TabIndex = 30
		'
		'Label_F31
		'
		Me.Label_F31.Location = New System.Drawing.Point(6, 788)
		Me.Label_F31.Name = "Label_F31"
		Me.Label_F31.Size = New System.Drawing.Size(111, 17)
		Me.Label_F31.TabIndex = 147
		Me.Label_F31.Text = "F31"
		'
		'Edit_F30
		'
		Me.Edit_F30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F30.Location = New System.Drawing.Point(121, 759)
		Me.Edit_F30.MaxLength = 100
		Me.Edit_F30.Name = "Edit_F30"
		Me.Edit_F30.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F30.TabIndex = 29
		'
		'Label_F30
		'
		Me.Label_F30.Location = New System.Drawing.Point(6, 762)
		Me.Label_F30.Name = "Label_F30"
		Me.Label_F30.Size = New System.Drawing.Size(111, 17)
		Me.Label_F30.TabIndex = 145
		Me.Label_F30.Text = "F30"
		'
		'Edit_F29
		'
		Me.Edit_F29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F29.Location = New System.Drawing.Point(121, 733)
		Me.Edit_F29.MaxLength = 100
		Me.Edit_F29.Name = "Edit_F29"
		Me.Edit_F29.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F29.TabIndex = 28
		'
		'Label_F29
		'
		Me.Label_F29.Location = New System.Drawing.Point(6, 736)
		Me.Label_F29.Name = "Label_F29"
		Me.Label_F29.Size = New System.Drawing.Size(111, 17)
		Me.Label_F29.TabIndex = 143
		Me.Label_F29.Text = "F29"
		'
		'Edit_F28
		'
		Me.Edit_F28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F28.Location = New System.Drawing.Point(121, 707)
		Me.Edit_F28.MaxLength = 100
		Me.Edit_F28.Name = "Edit_F28"
		Me.Edit_F28.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F28.TabIndex = 27
		'
		'Label_F28
		'
		Me.Label_F28.Location = New System.Drawing.Point(6, 710)
		Me.Label_F28.Name = "Label_F28"
		Me.Label_F28.Size = New System.Drawing.Size(111, 17)
		Me.Label_F28.TabIndex = 141
		Me.Label_F28.Text = "F28"
		'
		'Edit_F27
		'
		Me.Edit_F27.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F27.Location = New System.Drawing.Point(121, 681)
		Me.Edit_F27.MaxLength = 100
		Me.Edit_F27.Name = "Edit_F27"
		Me.Edit_F27.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F27.TabIndex = 26
		'
		'Label_F27
		'
		Me.Label_F27.Location = New System.Drawing.Point(6, 684)
		Me.Label_F27.Name = "Label_F27"
		Me.Label_F27.Size = New System.Drawing.Size(111, 17)
		Me.Label_F27.TabIndex = 139
		Me.Label_F27.Text = "F27"
		'
		'Edit_F26
		'
		Me.Edit_F26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F26.Location = New System.Drawing.Point(121, 655)
		Me.Edit_F26.MaxLength = 100
		Me.Edit_F26.Name = "Edit_F26"
		Me.Edit_F26.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F26.TabIndex = 25
		'
		'Label_F26
		'
		Me.Label_F26.Location = New System.Drawing.Point(6, 658)
		Me.Label_F26.Name = "Label_F26"
		Me.Label_F26.Size = New System.Drawing.Size(111, 17)
		Me.Label_F26.TabIndex = 137
		Me.Label_F26.Text = "F26"
		'
		'Edit_F25
		'
		Me.Edit_F25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F25.Location = New System.Drawing.Point(121, 629)
		Me.Edit_F25.MaxLength = 100
		Me.Edit_F25.Name = "Edit_F25"
		Me.Edit_F25.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F25.TabIndex = 24
		'
		'Label_F25
		'
		Me.Label_F25.Location = New System.Drawing.Point(6, 632)
		Me.Label_F25.Name = "Label_F25"
		Me.Label_F25.Size = New System.Drawing.Size(111, 17)
		Me.Label_F25.TabIndex = 135
		Me.Label_F25.Text = "F25"
		'
		'Edit_F24
		'
		Me.Edit_F24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F24.Location = New System.Drawing.Point(121, 603)
		Me.Edit_F24.MaxLength = 100
		Me.Edit_F24.Name = "Edit_F24"
		Me.Edit_F24.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F24.TabIndex = 23
		'
		'Label_F24
		'
		Me.Label_F24.Location = New System.Drawing.Point(6, 606)
		Me.Label_F24.Name = "Label_F24"
		Me.Label_F24.Size = New System.Drawing.Size(111, 17)
		Me.Label_F24.TabIndex = 133
		Me.Label_F24.Text = "F24"
		'
		'Edit_F23
		'
		Me.Edit_F23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F23.Location = New System.Drawing.Point(121, 577)
		Me.Edit_F23.MaxLength = 100
		Me.Edit_F23.Name = "Edit_F23"
		Me.Edit_F23.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F23.TabIndex = 22
		'
		'Label_F23
		'
		Me.Label_F23.Location = New System.Drawing.Point(6, 580)
		Me.Label_F23.Name = "Label_F23"
		Me.Label_F23.Size = New System.Drawing.Size(111, 17)
		Me.Label_F23.TabIndex = 131
		Me.Label_F23.Text = "F23"
		'
		'Edit_F22
		'
		Me.Edit_F22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F22.Location = New System.Drawing.Point(121, 551)
		Me.Edit_F22.MaxLength = 100
		Me.Edit_F22.Name = "Edit_F22"
		Me.Edit_F22.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F22.TabIndex = 21
		'
		'Label_F22
		'
		Me.Label_F22.Location = New System.Drawing.Point(6, 554)
		Me.Label_F22.Name = "Label_F22"
		Me.Label_F22.Size = New System.Drawing.Size(111, 17)
		Me.Label_F22.TabIndex = 129
		Me.Label_F22.Text = "F22"
		'
		'Edit_F21
		'
		Me.Edit_F21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F21.Location = New System.Drawing.Point(121, 525)
		Me.Edit_F21.MaxLength = 100
		Me.Edit_F21.Name = "Edit_F21"
		Me.Edit_F21.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F21.TabIndex = 20
		'
		'Label_F21
		'
		Me.Label_F21.Location = New System.Drawing.Point(6, 528)
		Me.Label_F21.Name = "Label_F21"
		Me.Label_F21.Size = New System.Drawing.Size(111, 17)
		Me.Label_F21.TabIndex = 127
		Me.Label_F21.Text = "F21"
		'
		'Edit_F20
		'
		Me.Edit_F20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F20.Location = New System.Drawing.Point(121, 499)
		Me.Edit_F20.MaxLength = 100
		Me.Edit_F20.Name = "Edit_F20"
		Me.Edit_F20.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F20.TabIndex = 19
		'
		'Label_F20
		'
		Me.Label_F20.Location = New System.Drawing.Point(6, 502)
		Me.Label_F20.Name = "Label_F20"
		Me.Label_F20.Size = New System.Drawing.Size(111, 17)
		Me.Label_F20.TabIndex = 125
		Me.Label_F20.Text = "F20"
		'
		'Edit_F19
		'
		Me.Edit_F19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F19.Location = New System.Drawing.Point(121, 473)
		Me.Edit_F19.MaxLength = 100
		Me.Edit_F19.Name = "Edit_F19"
		Me.Edit_F19.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F19.TabIndex = 18
		'
		'Label_F19
		'
		Me.Label_F19.Location = New System.Drawing.Point(6, 476)
		Me.Label_F19.Name = "Label_F19"
		Me.Label_F19.Size = New System.Drawing.Size(111, 17)
		Me.Label_F19.TabIndex = 123
		Me.Label_F19.Text = "F19"
		'
		'Edit_F18
		'
		Me.Edit_F18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F18.Location = New System.Drawing.Point(121, 447)
		Me.Edit_F18.MaxLength = 100
		Me.Edit_F18.Name = "Edit_F18"
		Me.Edit_F18.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F18.TabIndex = 17
		'
		'Label_F18
		'
		Me.Label_F18.Location = New System.Drawing.Point(6, 450)
		Me.Label_F18.Name = "Label_F18"
		Me.Label_F18.Size = New System.Drawing.Size(111, 17)
		Me.Label_F18.TabIndex = 121
		Me.Label_F18.Text = "F18"
		'
		'Edit_F17
		'
		Me.Edit_F17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F17.Location = New System.Drawing.Point(121, 421)
		Me.Edit_F17.MaxLength = 100
		Me.Edit_F17.Name = "Edit_F17"
		Me.Edit_F17.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F17.TabIndex = 16
		'
		'Label_F17
		'
		Me.Label_F17.Location = New System.Drawing.Point(6, 424)
		Me.Label_F17.Name = "Label_F17"
		Me.Label_F17.Size = New System.Drawing.Size(111, 17)
		Me.Label_F17.TabIndex = 119
		Me.Label_F17.Text = "F17"
		'
		'Edit_F16
		'
		Me.Edit_F16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F16.Location = New System.Drawing.Point(121, 395)
		Me.Edit_F16.MaxLength = 100
		Me.Edit_F16.Name = "Edit_F16"
		Me.Edit_F16.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F16.TabIndex = 15
		'
		'Label_F16
		'
		Me.Label_F16.Location = New System.Drawing.Point(6, 398)
		Me.Label_F16.Name = "Label_F16"
		Me.Label_F16.Size = New System.Drawing.Size(111, 17)
		Me.Label_F16.TabIndex = 117
		Me.Label_F16.Text = "F16"
		'
		'Edit_F15
		'
		Me.Edit_F15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F15.Location = New System.Drawing.Point(121, 369)
		Me.Edit_F15.MaxLength = 100
		Me.Edit_F15.Name = "Edit_F15"
		Me.Edit_F15.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F15.TabIndex = 14
		'
		'Label_F15
		'
		Me.Label_F15.Location = New System.Drawing.Point(6, 372)
		Me.Label_F15.Name = "Label_F15"
		Me.Label_F15.Size = New System.Drawing.Size(111, 17)
		Me.Label_F15.TabIndex = 115
		Me.Label_F15.Text = "F15"
		'
		'Edit_F14
		'
		Me.Edit_F14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F14.Location = New System.Drawing.Point(121, 343)
		Me.Edit_F14.MaxLength = 100
		Me.Edit_F14.Name = "Edit_F14"
		Me.Edit_F14.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F14.TabIndex = 13
		'
		'Label_F14
		'
		Me.Label_F14.Location = New System.Drawing.Point(6, 346)
		Me.Label_F14.Name = "Label_F14"
		Me.Label_F14.Size = New System.Drawing.Size(111, 17)
		Me.Label_F14.TabIndex = 113
		Me.Label_F14.Text = "F14"
		'
		'Edit_F13
		'
		Me.Edit_F13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F13.Location = New System.Drawing.Point(121, 317)
		Me.Edit_F13.MaxLength = 100
		Me.Edit_F13.Name = "Edit_F13"
		Me.Edit_F13.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F13.TabIndex = 12
		'
		'Label_F13
		'
		Me.Label_F13.Location = New System.Drawing.Point(6, 320)
		Me.Label_F13.Name = "Label_F13"
		Me.Label_F13.Size = New System.Drawing.Size(111, 17)
		Me.Label_F13.TabIndex = 111
		Me.Label_F13.Text = "F13"
		'
		'Edit_F12
		'
		Me.Edit_F12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F12.Location = New System.Drawing.Point(121, 291)
		Me.Edit_F12.MaxLength = 100
		Me.Edit_F12.Name = "Edit_F12"
		Me.Edit_F12.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F12.TabIndex = 11
		'
		'Label_F12
		'
		Me.Label_F12.Location = New System.Drawing.Point(6, 294)
		Me.Label_F12.Name = "Label_F12"
		Me.Label_F12.Size = New System.Drawing.Size(111, 17)
		Me.Label_F12.TabIndex = 109
		Me.Label_F12.Text = "F12"
		'
		'Edit_F11
		'
		Me.Edit_F11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F11.Location = New System.Drawing.Point(121, 265)
		Me.Edit_F11.MaxLength = 100
		Me.Edit_F11.Name = "Edit_F11"
		Me.Edit_F11.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F11.TabIndex = 10
		'
		'Label_F11
		'
		Me.Label_F11.Location = New System.Drawing.Point(6, 268)
		Me.Label_F11.Name = "Label_F11"
		Me.Label_F11.Size = New System.Drawing.Size(111, 17)
		Me.Label_F11.TabIndex = 107
		Me.Label_F11.Text = "F11"
		'
		'Edit_F10
		'
		Me.Edit_F10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F10.Location = New System.Drawing.Point(121, 239)
		Me.Edit_F10.MaxLength = 100
		Me.Edit_F10.Name = "Edit_F10"
		Me.Edit_F10.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F10.TabIndex = 9
		'
		'Label_F10
		'
		Me.Label_F10.Location = New System.Drawing.Point(6, 242)
		Me.Label_F10.Name = "Label_F10"
		Me.Label_F10.Size = New System.Drawing.Size(111, 17)
		Me.Label_F10.TabIndex = 105
		Me.Label_F10.Text = "F10"
		'
		'Edit_F9
		'
		Me.Edit_F9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F9.Location = New System.Drawing.Point(121, 213)
		Me.Edit_F9.MaxLength = 100
		Me.Edit_F9.Name = "Edit_F9"
		Me.Edit_F9.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F9.TabIndex = 8
		'
		'Label_F9
		'
		Me.Label_F9.Location = New System.Drawing.Point(6, 216)
		Me.Label_F9.Name = "Label_F9"
		Me.Label_F9.Size = New System.Drawing.Size(111, 17)
		Me.Label_F9.TabIndex = 103
		Me.Label_F9.Text = "F9"
		'
		'Edit_F8
		'
		Me.Edit_F8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F8.Location = New System.Drawing.Point(121, 187)
		Me.Edit_F8.MaxLength = 100
		Me.Edit_F8.Name = "Edit_F8"
		Me.Edit_F8.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F8.TabIndex = 7
		'
		'Label_F8
		'
		Me.Label_F8.Location = New System.Drawing.Point(6, 190)
		Me.Label_F8.Name = "Label_F8"
		Me.Label_F8.Size = New System.Drawing.Size(111, 17)
		Me.Label_F8.TabIndex = 101
		Me.Label_F8.Text = "F8"
		'
		'Edit_F7
		'
		Me.Edit_F7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F7.Location = New System.Drawing.Point(121, 161)
		Me.Edit_F7.MaxLength = 100
		Me.Edit_F7.Name = "Edit_F7"
		Me.Edit_F7.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F7.TabIndex = 6
		'
		'Label_F7
		'
		Me.Label_F7.Location = New System.Drawing.Point(6, 164)
		Me.Label_F7.Name = "Label_F7"
		Me.Label_F7.Size = New System.Drawing.Size(111, 17)
		Me.Label_F7.TabIndex = 99
		Me.Label_F7.Text = "F7"
		'
		'Edit_F6
		'
		Me.Edit_F6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F6.Location = New System.Drawing.Point(121, 135)
		Me.Edit_F6.MaxLength = 100
		Me.Edit_F6.Name = "Edit_F6"
		Me.Edit_F6.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F6.TabIndex = 5
		'
		'Label_F6
		'
		Me.Label_F6.Location = New System.Drawing.Point(6, 138)
		Me.Label_F6.Name = "Label_F6"
		Me.Label_F6.Size = New System.Drawing.Size(111, 17)
		Me.Label_F6.TabIndex = 97
		Me.Label_F6.Text = "F6"
		'
		'Edit_F5
		'
		Me.Edit_F5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F5.Location = New System.Drawing.Point(121, 109)
		Me.Edit_F5.MaxLength = 100
		Me.Edit_F5.Name = "Edit_F5"
		Me.Edit_F5.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F5.TabIndex = 4
		'
		'Label_F5
		'
		Me.Label_F5.Location = New System.Drawing.Point(6, 112)
		Me.Label_F5.Name = "Label_F5"
		Me.Label_F5.Size = New System.Drawing.Size(111, 17)
		Me.Label_F5.TabIndex = 95
		Me.Label_F5.Text = "F5"
		'
		'Edit_F4
		'
		Me.Edit_F4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F4.Location = New System.Drawing.Point(121, 83)
		Me.Edit_F4.MaxLength = 100
		Me.Edit_F4.Name = "Edit_F4"
		Me.Edit_F4.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F4.TabIndex = 3
		'
		'Label_F4
		'
		Me.Label_F4.Location = New System.Drawing.Point(6, 86)
		Me.Label_F4.Name = "Label_F4"
		Me.Label_F4.Size = New System.Drawing.Size(111, 17)
		Me.Label_F4.TabIndex = 93
		Me.Label_F4.Text = "F4"
		'
		'Edit_F3
		'
		Me.Edit_F3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F3.Location = New System.Drawing.Point(121, 57)
		Me.Edit_F3.MaxLength = 100
		Me.Edit_F3.Name = "Edit_F3"
		Me.Edit_F3.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F3.TabIndex = 2
		'
		'Label_F3
		'
		Me.Label_F3.Location = New System.Drawing.Point(6, 60)
		Me.Label_F3.Name = "Label_F3"
		Me.Label_F3.Size = New System.Drawing.Size(111, 17)
		Me.Label_F3.TabIndex = 91
		Me.Label_F3.Text = "F3"
		'
		'Edit_F2
		'
		Me.Edit_F2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Edit_F2.Location = New System.Drawing.Point(121, 31)
		Me.Edit_F2.MaxLength = 100
		Me.Edit_F2.Name = "Edit_F2"
		Me.Edit_F2.Size = New System.Drawing.Size(832, 20)
		Me.Edit_F2.TabIndex = 1
		'
		'Label_F2
		'
		Me.Label_F2.Location = New System.Drawing.Point(6, 34)
		Me.Label_F2.Name = "Label_F2"
		Me.Label_F2.Size = New System.Drawing.Size(111, 17)
		Me.Label_F2.TabIndex = 89
		Me.Label_F2.Text = "F2"
		'
		'editDataVendor
		'
		Me.editDataVendor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editDataVendor.Enabled = False
		Me.editDataVendor.Location = New System.Drawing.Point(796, 34)
		Me.editDataVendor.Name = "editDataVendor"
		Me.editDataVendor.Size = New System.Drawing.Size(139, 20)
		Me.editDataVendor.TabIndex = 78
		'
		'frmInformation
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(1004, 683)
		Me.Controls.Add(Me.editDataVendor)
		Me.Controls.Add(Me.Panel_InformationEdit)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectMasterName)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(458, 265)
		Me.Name = "frmInformation"
		Me.Text = "Add/Edit Mastername"
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
		Me.Panel_InformationEdit.ResumeLayout(False)
		Me.Panel_InformationEdit.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String

	Private GenericUpdateObject As RenaissanceTimerUpdateClass

	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  Private THIS_FORM_SelectingCombo As FCP_TelerikControls.FCP_RadComboBox
  Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private InformationDataset As RenaissanceDataClass.DSInformation			' Form Specific !!!!
	Private InformationTable As RenaissanceDataClass.DSInformation.tblInformationDataTable
	Private InformationAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

  Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private InGetFormData_Tick As Boolean
	Private AddNewRecord As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public ReadOnly Property FormChangedFlag() As Boolean
		Get
			Return FormChanged
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectMasterName
		THIS_FORM_NewMoveToControl = Me.Edit_F1

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "Mastername"
		THIS_FORM_OrderBy = "Mastername"

		THIS_FORM_ValueMember = "ID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmInformation

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.Mastername	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Edit_F1.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F2.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F3.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F4.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F5.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F6.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F7.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F8.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F9.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F10.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F11.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F12.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F13.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F14.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F15.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F16.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F17.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F18.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F19.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F20.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F21.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F22.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F23.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F24.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F25.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F26.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F27.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F28.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F29.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F30.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F31.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F32.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F33.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F34.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F35.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F36.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F37.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F38.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F39.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F40.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_F41.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_FDescription.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_FUserDescription.TextChanged, AddressOf Me.FormControlChanged

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		InformationAdaptor = New SqlDataAdapter
    MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION), InformationAdaptor, RenaissanceStandardDatasets.Information.TableName)
		InformationDataset = New DSInformation
		InformationTable = InformationDataset.tblInformation

    Me.RootMenu.PerformLayout()

		Try
			InPaint = True


		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "Mastername"
		THIS_FORM_OrderBy = "Mastername"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

    If (MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION) Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Call SetSortedRows()

		' Initialise Timer

		GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick)

		' Display initial record.

		thisPosition = 0
    If MainForm.MasternameDictionary.Count > 0 Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(0)))
    Else
      Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
      Call GetFormData(Nothing)
    End If

		InPaint = False


	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
		MainForm.RemoveFormUpdate(Me)	' Remove Form Update reference, will be re-established in Load() if this form is cached.

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Edit_F1.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F2.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F3.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F4.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F5.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F6.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F7.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F8.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F9.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F10.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F11.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F12.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F13.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F14.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F15.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F16.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F17.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F18.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F19.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F20.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F21.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F22.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F23.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F24.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F25.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F26.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F27.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F28.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F29.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F30.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F31.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F32.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F33.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F34.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F35.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F36.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F37.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F38.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F39.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F40.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_F41.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_FDescription.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_FUserDescription.TextChanged, AddressOf Me.FormControlChanged

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged

				RemoveHandler Edit_F1.TextChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If



		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or (e.TableChanged(RenaissanceChangeID.Information) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

			' Set SelectingCombo Index.

      If (Me.MainForm.MasternameDictionary.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
          thisAuditID = (-1)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedValue = thisAuditID
        Catch ex As Exception
        End Try
      End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData(thisAuditID) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

    THIS_FORM_SelectingCombo.MasternameCollection = MainForm.MasternameDictionary

    'Dim OrgInPaint As Boolean

    'Dim thisrow As DataRow
    'Dim thisDrowDownWidth As Integer
    'Dim SizingBitmap As Bitmap
    'Dim SizingGraphics As Graphics
    'Dim SelectString As String = "True"

    '' Form Specific Selection Combo :-
    'If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    '' Code is pretty Generic from here on...

    '' Set paint local so that changes to the selection combo do not trigger form updates.

    'OrgInPaint = InPaint
    'InPaint = True

    '' Get selected Row sets, or exit if no data is present.
    'If myDataset Is Nothing Then
    '	ReDim SortedRows(0)
    '	ReDim SelectBySortedRows(0)
    'Else
    '	SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
    '	SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
    'End If

    '' Set Combo data source
    'THIS_FORM_SelectingCombo.DataSource = SortedRows
    'THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    'THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    '' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    'thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
    'For Each thisrow In SortedRows

    '	' Compute the string dimensions in the given font
    '	SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
    '	SizingGraphics = Graphics.FromImage(SizingBitmap)
    '	Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
    '	If (stringSize.Width) > thisDrowDownWidth Then
    '		thisDrowDownWidth = CInt(stringSize.Width)
    '	End If
    'Next

    'THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    'InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Function GetFormData_Tick() As Boolean
		' *******************************************************************************
		'
		' Callback process for the Generic Form Update Process.
		'
		' Function MUST return True / False on Update.
		' True will clear the update request. False will not.
		'
		' *******************************************************************************
		Dim RVal As Boolean = False

		If (InGetFormData_Tick) OrElse (Not _InUse) Then
			Return False
			Exit Function
		End If

		Try
			InGetFormData_Tick = True

			' Find the correct data row, then show it...
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

      GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))

			RVal = True

		Catch ex As Exception
			RVal = False
		Finally
			InGetFormData_Tick = False
		End Try

		Return RVal

	End Function

  Private Sub GetFormData(ByVal pPertracID As Integer)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean
    Dim Counter As Integer
    Dim ThisTextBox As Windows.Forms.TextBox
    Dim FieldName As String

    If (MainForm.MasternameDictionary.IndexOfKey(pPertracID) < 0) Then
      pPertracID = 0
      thisAuditID = 0
      btnNavFirst_Click(Nothing, New EventArgs)
      Exit Sub
    End If

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.

    Try
      OrgInpaint = InPaint
      InPaint = True

      If (pPertracID <= 0) Or (FormIsValid = False) Or (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.

        thisAuditID = (-1)

        Me.editAuditID.Text = ""
        Me.editDataVendor.Text = "."

        Me.Edit_F1.Text = ""
        Me.Edit_F2.Text = ""
        Me.Edit_F3.Text = ""
        Me.Edit_F4.Text = ""
        Me.Edit_F5.Text = ""
        Me.Edit_F6.Text = ""
        Me.Edit_F7.Text = ""
        Me.Edit_F8.Text = ""
        Me.Edit_F9.Text = ""
        Me.Edit_F10.Text = ""
        Me.Edit_F11.Text = ""
        Me.Edit_F12.Text = ""
        Me.Edit_F13.Text = ""
        Me.Edit_F14.Text = ""
        Me.Edit_F15.Text = ""
        Me.Edit_F16.Text = ""
        Me.Edit_F17.Text = ""
        Me.Edit_F18.Text = ""
        Me.Edit_F19.Text = ""
        Me.Edit_F20.Text = ""
        Me.Edit_F31.Text = ""
        Me.Edit_F32.Text = ""
        Me.Edit_F33.Text = ""
        Me.Edit_F34.Text = ""
        Me.Edit_F35.Text = ""
        Me.Edit_F36.Text = ""
        Me.Edit_F37.Text = ""
        Me.Edit_F38.Text = ""
        Me.Edit_F39.Text = ""
        Me.Edit_F40.Text = ""
        Me.Edit_F41.Text = ""
        Me.Edit_FDescription.Text = ""
        Me.Edit_FUserDescription.Text = ""

        If AddNewRecord = True Then
          Me.btnCancel.Enabled = True
          Me.THIS_FORM_SelectingCombo.Enabled = False
        Else
          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True
        End If

      Else

        ' Populate Form with given data.
        Try

          Dim ThisInformationRow As DSInformation.tblInformationRow

          ThisInformationRow = MainForm.PertracData.GetInformationRow(pPertracID)
          thisAuditID = pPertracID

          Me.editAuditID.Text = thisAuditID.ToString

          ' The Form labels are updated on the change in 'editDataVendor.Text'.

          If (editDataVendor.Text <> Nz(ThisInformationRow.DataVendorName, ".")) Then
            editDataVendor.Text = Nz(ThisInformationRow.DataVendorName, ".")
          End If

          ' Set 'F' fields.

          For Counter = 1 To 41
            FieldName = "F" & Counter.ToString
            ThisTextBox = Panel_InformationEdit.Controls("Edit_" & FieldName)

            If (ThisTextBox IsNot Nothing) Then
              If (Not ThisInformationRow.Table.Columns.Contains(FieldName)) OrElse (ThisInformationRow.IsNull(FieldName)) Then
                ThisTextBox.Text = ""
              Else
                ThisTextBox.Text = ThisInformationRow(FieldName).ToString
              End If
            End If
          Next

          If (ThisInformationRow.IsDescriptionNull) Then
            Me.Edit_FDescription.Text = ""
          Else
            Me.Edit_FDescription.Text = ThisInformationRow.Description
          End If
          If (ThisInformationRow.IsUserDescriptionNull) Then
            Me.Edit_FUserDescription.Text = ""
          Else
            Me.Edit_FUserDescription.Text = ThisInformationRow.UserDescription
          End If

          AddNewRecord = False
          ' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True

        Catch ex As Exception

          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
          Call GetFormData(Nothing)

        End Try

      End If

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
    Finally
      Application.DoEvents()
      InPaint = OrgInpaint
    End Try

    ' Restore 'Paint' flag.
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' Note that GroupList Entries are also added from the FundSearch Form. Changes
		' to the GroupList table should be reflected there also.
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
    Position = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
      Return False
      Exit Function
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

    End If


    Try
      ' Set 'Paint' flag.
      InPaint = True

      ' *************************************************************
      ' Lock the Data Table, to prevent update conflicts.
      ' *************************************************************

      Dim ThisInformationRow As DSInformation.tblInformationRow
      ThisInformationRow = MainForm.PertracData.GetInformationRow(thisAuditID)

      ' Initiate Edit,
      ThisInformationRow.BeginEdit()

      ' Set Data Values

      ThisInformationRow.F1 = Edit_F1.Text
      ThisInformationRow.F2 = Edit_F2.Text
      ThisInformationRow.F3 = Edit_F3.Text
      ThisInformationRow.F4 = Edit_F4.Text
      ThisInformationRow.F5 = Edit_F5.Text
      ThisInformationRow.F6 = Edit_F6.Text
      ThisInformationRow.F7 = Edit_F7.Text
      ThisInformationRow.F8 = Edit_F8.Text
      ThisInformationRow.F9 = Edit_F9.Text
      ThisInformationRow.F10 = Edit_F10.Text
      ThisInformationRow.F11 = Edit_F11.Text
      ThisInformationRow.F12 = Edit_F12.Text
      ThisInformationRow.F13 = Edit_F13.Text
      ThisInformationRow.F14 = Edit_F14.Text
      ThisInformationRow.F15 = Edit_F15.Text
      ThisInformationRow.F16 = Edit_F16.Text
      ThisInformationRow.F17 = Edit_F17.Text
      ThisInformationRow.F18 = Edit_F18.Text
      ThisInformationRow.F19 = Edit_F19.Text
      ThisInformationRow.F20 = Edit_F20.Text
      ThisInformationRow.F21 = Edit_F21.Text
      ThisInformationRow.F22 = Edit_F22.Text
      ThisInformationRow.F23 = Edit_F23.Text
      ThisInformationRow.F24 = Edit_F24.Text
      ThisInformationRow.F25 = Edit_F25.Text
      ThisInformationRow.F26 = Edit_F26.Text
      ThisInformationRow.F27 = Edit_F27.Text
      ThisInformationRow.F28 = Edit_F28.Text
      ThisInformationRow.F29 = Edit_F29.Text
      ThisInformationRow.F30 = Edit_F30.Text
      ThisInformationRow.F31 = Edit_F31.Text
      ThisInformationRow.F32 = Edit_F32.Text
      ThisInformationRow.F33 = Edit_F33.Text
      ThisInformationRow.F34 = Edit_F34.Text
      ThisInformationRow.F35 = Edit_F35.Text
      ThisInformationRow.F36 = Edit_F36.Text
      ThisInformationRow.F37 = Edit_F37.Text
      ThisInformationRow.F38 = Edit_F38.Text
      ThisInformationRow.F39 = Edit_F39.Text
      ThisInformationRow.F40 = Edit_F40.Text
      ThisInformationRow.F41 = Edit_F41.Text
      ThisInformationRow.Description = Edit_FDescription.Text
      ThisInformationRow.UserDescription = Edit_FUserDescription.Text

      ThisInformationRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      UpdateRows(0) = ThisInformationRow

      ' Post Additions / Updates to the underlying table :-
      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          InformationAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          InformationAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, InformationAdaptor, UpdateRows)
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try

    Catch ex As Exception
      ErrFlag = True
      ErrMessage = ex.Message
      ErrStack = ex.StackTrace
    Finally
      InPaint = False
    End Try


    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Information, thisAuditID.ToString)

    Call MainForm.Main_RaiseEvent(UpdateMessage)

	End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.Edit_F1.Enabled = True
			Me.Edit_F2.Enabled = True
			Me.Edit_F3.Enabled = True
			Me.Edit_F4.Enabled = True
			Me.Edit_F5.Enabled = True
			Me.Edit_F6.Enabled = True
			Me.Edit_F7.Enabled = True
			Me.Edit_F8.Enabled = True
			Me.Edit_F9.Enabled = True
			Me.Edit_F10.Enabled = True
			Me.Edit_F11.Enabled = True
			Me.Edit_F12.Enabled = True
			Me.Edit_F13.Enabled = True
			Me.Edit_F14.Enabled = True
			Me.Edit_F15.Enabled = True
			Me.Edit_F16.Enabled = True
			Me.Edit_F17.Enabled = True
			Me.Edit_F18.Enabled = True
			Me.Edit_F19.Enabled = True
			Me.Edit_F20.Enabled = True
			Me.Edit_F31.Enabled = True
			Me.Edit_F32.Enabled = True
			Me.Edit_F33.Enabled = True
			Me.Edit_F34.Enabled = True
			Me.Edit_F35.Enabled = True
			Me.Edit_F36.Enabled = True
			Me.Edit_F37.Enabled = True
			Me.Edit_F38.Enabled = True
			Me.Edit_F39.Enabled = True
			Me.Edit_F40.Enabled = True
			Me.Edit_F41.Enabled = True
			Me.Edit_FDescription.Enabled = True
			Me.Edit_FUserDescription.Enabled = True

		Else

			Me.Edit_F1.Enabled = False
			Me.Edit_F2.Enabled = False
			Me.Edit_F3.Enabled = False
			Me.Edit_F4.Enabled = False
			Me.Edit_F5.Enabled = False
			Me.Edit_F6.Enabled = False
			Me.Edit_F7.Enabled = False
			Me.Edit_F8.Enabled = False
			Me.Edit_F9.Enabled = False
			Me.Edit_F10.Enabled = False
			Me.Edit_F11.Enabled = False
			Me.Edit_F12.Enabled = False
			Me.Edit_F13.Enabled = False
			Me.Edit_F14.Enabled = False
			Me.Edit_F15.Enabled = False
			Me.Edit_F16.Enabled = False
			Me.Edit_F17.Enabled = False
			Me.Edit_F18.Enabled = False
			Me.Edit_F19.Enabled = False
			Me.Edit_F20.Enabled = False
			Me.Edit_F31.Enabled = False
			Me.Edit_F32.Enabled = False
			Me.Edit_F33.Enabled = False
			Me.Edit_F34.Enabled = False
			Me.Edit_F35.Enabled = False
			Me.Edit_F36.Enabled = False
			Me.Edit_F37.Enabled = False
			Me.Edit_F38.Enabled = False
			Me.Edit_F39.Enabled = False
			Me.Edit_F40.Enabled = False
			Me.Edit_F41.Enabled = False
			Me.Edit_FDescription.Enabled = False
			Me.Edit_FUserDescription.Enabled = False

		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
    thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

		If (GenericUpdateObject IsNot Nothing) Then
			Try
				GenericUpdateObject.FormToUpdate = True	 '	GetFormDataToUpdate = True
			Catch ex As Exception
			End Try
		Else
      Call GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))
    End If

	End Sub


	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

    If thisPosition < (MainForm.MasternameDictionary.Count - 1) Then thisPosition += 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

    thisPosition = MainForm.MasternameDictionary.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < MainForm.MasternameDictionary.Count) Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(thisPosition)))
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

	End Sub

	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *************************************************************
		' Close Form
		' *************************************************************


		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region







	Private Sub editDataVendor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles editDataVendor.TextChanged
		' ***********************************************************************************************
		'
		'
		' ***********************************************************************************************

		Try

			Dim InformationTable As New DSInformation.tblInformationDataTable
			Dim LabelArray(InformationTable.Columns.Count) As String
			Dim ThisLabel As Windows.Forms.Label
			Dim FieldName As String
			Dim Counter As Integer
			Dim MappingArray() As Integer

			Try
				For Counter = 0 To (InformationTable.Columns.Count - 1)
					LabelArray(Counter) = InformationTable.Columns(Counter).ColumnName
				Next

				If (MainForm.InformationColumnCache.ContainsKey(editDataVendor.Text.ToUpper)) Then

					MappingArray = MainForm.InformationColumnCache.Item(editDataVendor.Text.ToUpper)

					For Counter = 0 To (MappingArray.Length - 1)
						If (MappingArray(Counter) >= 0) Then
							LabelArray(MappingArray(Counter)) = CType(Counter, PertracInformationFields).ToString
						End If
					Next
				End If

				For Counter = 1 To 41
					Try
						FieldName = "F" & Counter.ToString
						ThisLabel = Panel_InformationEdit.Controls("Label_" & FieldName)

						If (ThisLabel IsNot Nothing) AndAlso (InformationTable.Columns.Contains(FieldName)) Then
							ThisLabel.Text = LabelArray(InformationTable.Columns(FieldName).Ordinal)
						Else
							ThisLabel.Text = FieldName
						End If
					Catch ex As Exception
					End Try
				Next

			Catch ex As Exception
			End Try

		Catch ex As Exception
		End Try

	End Sub

End Class
