Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceControls


Public Class frmCustomFields

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents edit_FieldName As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnAdd As System.Windows.Forms.Button
	Friend WithEvents btnDelete As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents Combo_SelectEntityID As System.Windows.Forms.ComboBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents lbl_CustomType As System.Windows.Forms.Label
	Friend WithEvents lbl_FieldName As System.Windows.Forms.Label
	Friend WithEvents Date_StartDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents Label_DecisionDate As System.Windows.Forms.Label
	Friend WithEvents Date_EndDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Combo_CustomFieldType As System.Windows.Forms.ComboBox
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Edit_PeriodCount As RenaissanceControls.NumericTextBox
	Friend WithEvents Radio_MAX As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_MIN As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_First As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Last As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Average As System.Windows.Forms.RadioButton
  Friend WithEvents Check_IsVolatile As System.Windows.Forms.CheckBox
  Friend WithEvents Check_IsSearchable As System.Windows.Forms.CheckBox
	Friend WithEvents Check_IsOptimisable As System.Windows.Forms.CheckBox
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Check_IRR As System.Windows.Forms.CheckBox
	Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents Text_DefaultValue As System.Windows.Forms.TextBox
  Friend WithEvents btnClose As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.edit_FieldName = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectEntityID = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.lbl_CustomType = New System.Windows.Forms.Label
		Me.lbl_FieldName = New System.Windows.Forms.Label
		Me.Date_StartDate = New System.Windows.Forms.DateTimePicker
		Me.Label_DecisionDate = New System.Windows.Forms.Label
		Me.Date_EndDate = New System.Windows.Forms.DateTimePicker
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_CustomFieldType = New System.Windows.Forms.ComboBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Edit_PeriodCount = New RenaissanceControls.NumericTextBox
		Me.Radio_MAX = New System.Windows.Forms.RadioButton
		Me.Radio_MIN = New System.Windows.Forms.RadioButton
		Me.Radio_First = New System.Windows.Forms.RadioButton
		Me.Radio_Last = New System.Windows.Forms.RadioButton
		Me.Radio_Average = New System.Windows.Forms.RadioButton
		Me.Check_IsVolatile = New System.Windows.Forms.CheckBox
		Me.Check_IsSearchable = New System.Windows.Forms.CheckBox
		Me.Check_IsOptimisable = New System.Windows.Forms.CheckBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Check_IRR = New System.Windows.Forms.CheckBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.Text_DefaultValue = New System.Windows.Forms.TextBox
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(389, 33)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(50, 20)
		Me.editAuditID.TabIndex = 1
		'
		'edit_FieldName
		'
		Me.edit_FieldName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.edit_FieldName.Location = New System.Drawing.Point(121, 76)
		Me.edit_FieldName.MaxLength = 100
		Me.edit_FieldName.Name = "edit_FieldName"
		Me.edit_FieldName.Size = New System.Drawing.Size(320, 20)
		Me.edit_FieldName.TabIndex = 2
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(176, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 4
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(153, 440)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 19
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(237, 440)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 20
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(65, 440)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 18
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectEntityID
		'
		Me.Combo_SelectEntityID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectEntityID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectEntityID.Location = New System.Drawing.Point(121, 33)
		Me.Combo_SelectEntityID.Name = "Combo_SelectEntityID"
		Me.Combo_SelectEntityID.Size = New System.Drawing.Size(262, 21)
		Me.Combo_SelectEntityID.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Location = New System.Drawing.Point(101, 384)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(260, 48)
		Me.Panel1.TabIndex = 17
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(6, 33)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 30
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(5, 59)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(436, 10)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(321, 440)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 21
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(452, 24)
		Me.RootMenu.TabIndex = 22
		Me.RootMenu.Text = "MenuStrip1"
		'
		'lbl_CustomType
		'
		Me.lbl_CustomType.AutoSize = True
		Me.lbl_CustomType.Location = New System.Drawing.Point(6, 105)
		Me.lbl_CustomType.Name = "lbl_CustomType"
		Me.lbl_CustomType.Size = New System.Drawing.Size(94, 13)
		Me.lbl_CustomType.TabIndex = 80
		Me.lbl_CustomType.Text = "Custom Field Type"
		'
		'lbl_FieldName
		'
		Me.lbl_FieldName.Location = New System.Drawing.Point(6, 79)
		Me.lbl_FieldName.Name = "lbl_FieldName"
		Me.lbl_FieldName.Size = New System.Drawing.Size(111, 17)
		Me.lbl_FieldName.TabIndex = 86
		Me.lbl_FieldName.Text = "Field Name"
		'
		'Date_StartDate
		'
		Me.Date_StartDate.Location = New System.Drawing.Point(121, 155)
		Me.Date_StartDate.Name = "Date_StartDate"
		Me.Date_StartDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_StartDate.TabIndex = 5
		'
		'Label_DecisionDate
		'
		Me.Label_DecisionDate.Location = New System.Drawing.Point(6, 159)
		Me.Label_DecisionDate.Name = "Label_DecisionDate"
		Me.Label_DecisionDate.Size = New System.Drawing.Size(100, 16)
		Me.Label_DecisionDate.TabIndex = 102
		Me.Label_DecisionDate.Text = "Date From"
		'
		'Date_EndDate
		'
		Me.Date_EndDate.Location = New System.Drawing.Point(121, 181)
		Me.Date_EndDate.Name = "Date_EndDate"
		Me.Date_EndDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_EndDate.TabIndex = 6
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(6, 185)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(100, 16)
		Me.Label2.TabIndex = 104
		Me.Label2.Text = "Date To"
		'
		'Combo_CustomFieldType
		'
		Me.Combo_CustomFieldType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_CustomFieldType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_CustomFieldType.FormattingEnabled = True
		Me.Combo_CustomFieldType.Location = New System.Drawing.Point(121, 102)
		Me.Combo_CustomFieldType.Name = "Combo_CustomFieldType"
		Me.Combo_CustomFieldType.Size = New System.Drawing.Size(320, 21)
		Me.Combo_CustomFieldType.TabIndex = 3
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(6, 210)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(111, 17)
		Me.Label3.TabIndex = 107
		Me.Label3.Text = "Period Count"
		'
		'Edit_PeriodCount
		'
		Me.Edit_PeriodCount.Location = New System.Drawing.Point(121, 207)
		Me.Edit_PeriodCount.MaxLength = 100
		Me.Edit_PeriodCount.Name = "Edit_PeriodCount"
		Me.Edit_PeriodCount.RenaissanceTag = Nothing
		Me.Edit_PeriodCount.Size = New System.Drawing.Size(174, 20)
		Me.Edit_PeriodCount.TabIndex = 7
		Me.Edit_PeriodCount.Text = "0"
		Me.Edit_PeriodCount.TextFormat = "#,##0"
		Me.Edit_PeriodCount.Value = 0
		'
		'Radio_MAX
		'
		Me.Radio_MAX.AutoSize = True
		Me.Radio_MAX.Location = New System.Drawing.Point(121, 233)
		Me.Radio_MAX.Name = "Radio_MAX"
		Me.Radio_MAX.Size = New System.Drawing.Size(99, 17)
		Me.Radio_MAX.TabIndex = 8
		Me.Radio_MAX.TabStop = True
		Me.Radio_MAX.Text = "Maximum Value"
		Me.Radio_MAX.UseVisualStyleBackColor = True
		'
		'Radio_MIN
		'
		Me.Radio_MIN.AutoSize = True
		Me.Radio_MIN.Location = New System.Drawing.Point(249, 233)
		Me.Radio_MIN.Name = "Radio_MIN"
		Me.Radio_MIN.Size = New System.Drawing.Size(96, 17)
		Me.Radio_MIN.TabIndex = 9
		Me.Radio_MIN.TabStop = True
		Me.Radio_MIN.Text = "Minimum Value"
		Me.Radio_MIN.UseVisualStyleBackColor = True
		'
		'Radio_First
		'
		Me.Radio_First.AutoSize = True
		Me.Radio_First.Location = New System.Drawing.Point(121, 256)
		Me.Radio_First.Name = "Radio_First"
		Me.Radio_First.Size = New System.Drawing.Size(74, 17)
		Me.Radio_First.TabIndex = 10
		Me.Radio_First.TabStop = True
		Me.Radio_First.Text = "First Value"
		Me.Radio_First.UseVisualStyleBackColor = True
		'
		'Radio_Last
		'
		Me.Radio_Last.AutoSize = True
		Me.Radio_Last.Location = New System.Drawing.Point(249, 256)
		Me.Radio_Last.Name = "Radio_Last"
		Me.Radio_Last.Size = New System.Drawing.Size(75, 17)
		Me.Radio_Last.TabIndex = 11
		Me.Radio_Last.TabStop = True
		Me.Radio_Last.Text = "Last Value"
		Me.Radio_Last.UseVisualStyleBackColor = True
		'
		'Radio_Average
		'
		Me.Radio_Average.AutoSize = True
		Me.Radio_Average.Location = New System.Drawing.Point(121, 279)
		Me.Radio_Average.Name = "Radio_Average"
		Me.Radio_Average.Size = New System.Drawing.Size(95, 17)
		Me.Radio_Average.TabIndex = 12
		Me.Radio_Average.TabStop = True
		Me.Radio_Average.Text = "Average Value"
		Me.Radio_Average.UseVisualStyleBackColor = True
		'
		'Check_IsVolatile
		'
		Me.Check_IsVolatile.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_IsVolatile.Location = New System.Drawing.Point(22, 338)
		Me.Check_IsVolatile.Name = "Check_IsVolatile"
		Me.Check_IsVolatile.Size = New System.Drawing.Size(112, 17)
		Me.Check_IsVolatile.TabIndex = 14
		Me.Check_IsVolatile.Text = "Field Is Volatile"
		Me.Check_IsVolatile.UseVisualStyleBackColor = True
		'
		'Check_IsSearchable
		'
		Me.Check_IsSearchable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_IsSearchable.Location = New System.Drawing.Point(154, 338)
		Me.Check_IsSearchable.Name = "Check_IsSearchable"
		Me.Check_IsSearchable.Size = New System.Drawing.Size(122, 17)
		Me.Check_IsSearchable.TabIndex = 15
		Me.Check_IsSearchable.Text = "Field Is Searchable"
		Me.Check_IsSearchable.UseVisualStyleBackColor = True
		'
		'Check_IsOptimisable
		'
		Me.Check_IsOptimisable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_IsOptimisable.Location = New System.Drawing.Point(291, 338)
		Me.Check_IsOptimisable.Name = "Check_IsOptimisable"
		Me.Check_IsOptimisable.Size = New System.Drawing.Size(122, 17)
		Me.Check_IsOptimisable.TabIndex = 16
		Me.Check_IsOptimisable.Text = "Field Is Optimisable"
		Me.Check_IsOptimisable.UseVisualStyleBackColor = True
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(6, 235)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(111, 17)
		Me.Label4.TabIndex = 108
		Me.Label4.Text = "Field Value is :"
		'
		'Check_IRR
		'
		Me.Check_IRR.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_IRR.Location = New System.Drawing.Point(5, 306)
		Me.Check_IRR.Name = "Check_IRR"
		Me.Check_IRR.Size = New System.Drawing.Size(190, 17)
		Me.Check_IRR.TabIndex = 13
		Me.Check_IRR.Text = "Calculate as Annualised Return"
		Me.Check_IRR.UseVisualStyleBackColor = True
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(6, 132)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(111, 17)
		Me.Label5.TabIndex = 110
		Me.Label5.Text = "Default Value"
		'
		'Text_DefaultValue
		'
		Me.Text_DefaultValue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_DefaultValue.Location = New System.Drawing.Point(121, 129)
		Me.Text_DefaultValue.MaxLength = 100
		Me.Text_DefaultValue.Name = "Text_DefaultValue"
		Me.Text_DefaultValue.Size = New System.Drawing.Size(320, 20)
		Me.Text_DefaultValue.TabIndex = 4
		'
		'frmCustomFields
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(452, 480)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Text_DefaultValue)
		Me.Controls.Add(Me.Check_IRR)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Check_IsOptimisable)
		Me.Controls.Add(Me.Check_IsSearchable)
		Me.Controls.Add(Me.Check_IsVolatile)
		Me.Controls.Add(Me.Radio_Average)
		Me.Controls.Add(Me.Radio_Last)
		Me.Controls.Add(Me.Radio_First)
		Me.Controls.Add(Me.Radio_MIN)
		Me.Controls.Add(Me.Radio_MAX)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Edit_PeriodCount)
		Me.Controls.Add(Me.Combo_CustomFieldType)
		Me.Controls.Add(Me.Date_EndDate)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Date_StartDate)
		Me.Controls.Add(Me.Label_DecisionDate)
		Me.Controls.Add(Me.lbl_FieldName)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectEntityID)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.edit_FieldName)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.lbl_CustomType)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(458, 339)
		Me.Name = "frmCustomFields"
		Me.Text = "Add/Edit Custom Fields"
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	Private THIS_FORM_SelectingCombo As ComboBox
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private myDataset As RenaissanceDataClass.DSPertracCustomFields			 ' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
	Private myConnection As SqlConnection
	Private myAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

	Private SortedRows() As DataRow
	Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow		' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private AddNewRecord As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectEntityID
		THIS_FORM_NewMoveToControl = Me.edit_FieldName

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "FieldName"
		THIS_FORM_OrderBy = "FieldName"

		THIS_FORM_ValueMember = "FieldID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmCustomFields

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblPertracCustomFields	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler edit_FieldName.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events

		AddHandler edit_FieldName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_CustomFieldType.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_StartDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_EndDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_PeriodCount.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_MAX.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_MIN.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_First.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_Last.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_Average.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IRR.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsVolatile.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsOptimisable.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsSearchable.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Text_DefaultValue.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_CustomFieldType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_CustomFieldType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_CustomFieldType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_CustomFieldType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType


		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		'Me.Controls.Add(MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent))
		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		Try
			InPaint = True

			Call SetFieldTypeCombo()
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "FieldName"
		THIS_FORM_OrderBy = "FieldName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates

		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler edit_FieldName.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler edit_FieldName.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_CustomFieldType.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_StartDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_EndDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_PeriodCount.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_MAX.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_MIN.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_First.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_Last.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_Average.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsVolatile.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsOptimisable.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsSearchable.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IRR.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_DefaultValue.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_CustomFieldType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_CustomFieldType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_CustomFieldType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_CustomFieldType.KeyUp, AddressOf MainForm.ComboSelectAsYouType


			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' *********************************************************************************************
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		' *********************************************************************************************

		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If



		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetFieldTypeCombo()
		Call MainForm.SetTblGenericCombo( _
		Me.Combo_CustomFieldType, _
		GetType(RenaissanceGlobals.PertracCustomFieldTypes))
	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""
			Me.edit_FieldName.Text = ""
			Me.Text_DefaultValue.Text = ""

			Me.Combo_CustomFieldType.SelectedIndex = 0

			Me.Date_StartDate.Value = #1/1/1900#
			Me.Date_EndDate.Value = #1/1/3000#
			Me.Edit_PeriodCount.Value = 0

			Me.Radio_MAX.Checked = True
			Me.Radio_MIN.Checked = False
			Me.Radio_First.Checked = False
			Me.Radio_Last.Checked = False
			Me.Radio_Average.Checked = False

			Me.Check_IRR.Checked = False
			Me.Check_IsVolatile.Checked = False
			Me.Check_IsOptimisable.Checked = True
			Me.Check_IsSearchable.Checked = True

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.edit_FieldName.Text = thisDataRow.FieldName
				Me.Text_DefaultValue.Text = thisDataRow.DefaultValue

				Me.Combo_CustomFieldType.SelectedValue = thisDataRow.CustomFieldType

				Me.Date_StartDate.Value = thisDataRow.StartDate
				Me.Date_EndDate.Value = thisDataRow.EndDate
				Me.Edit_PeriodCount.Value = thisDataRow.FieldPeriodCount

				Me.Radio_MAX.Checked = thisDataRow.IsMaxValue
				Me.Radio_MIN.Checked = thisDataRow.IsMinValue
				Me.Radio_First.Checked = thisDataRow.IsFirstValue
				Me.Radio_Last.Checked = thisDataRow.IsLastValue
				Me.Radio_Average.Checked = thisDataRow.IsAverageValue

				Me.Check_IRR.Checked = thisDataRow.IsIRR

				Me.Check_IsVolatile.Checked = thisDataRow.FieldIsVolatile
				Me.Check_IsOptimisable.Checked = thisDataRow.FieldIsOptimisable
				Me.Check_IsSearchable.Checked = thisDataRow.FieldIsSearchable

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add : "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If


		Try
			' Set 'Paint' flag.
			InPaint = True

			' *************************************************************
			' Lock the Data Table, to prevent update conflicts.
			' *************************************************************
			SyncLock myTable

				' Initiate Edit,
				thisDataRow.BeginEdit()

				' Set Data Values

				thisDataRow.FieldName = edit_FieldName.Text
				LogString &= ", FieldName = " & edit_FieldName.Text

				thisDataRow.CustomFieldType = Me.Combo_CustomFieldType.SelectedValue
				LogString &= ", CustomFieldType = " & Combo_CustomFieldType.Text

				If Date_StartDate.Enabled Then
					thisDataRow.StartDate = Me.Date_StartDate.Value
				Else
					thisDataRow.StartDate = Renaissance_BaseDate
				End If

				If Date_EndDate.Enabled Then
					thisDataRow.EndDate = Me.Date_EndDate.Value
				Else
					thisDataRow.EndDate = Renaissance_BaseDate
				End If

				thisDataRow.FieldPeriodCount = Me.Edit_PeriodCount.Value

				thisDataRow.IsMaxValue = Me.Radio_MAX.Checked
				thisDataRow.IsMinValue = Me.Radio_MIN.Checked
				thisDataRow.IsFirstValue = Me.Radio_First.Checked
				thisDataRow.IsLastValue = Me.Radio_Last.Checked
				thisDataRow.IsAverageValue = Me.Radio_Average.Checked

				thisDataRow.IsIRR = Me.Check_IRR.Checked

				thisDataRow.FieldIsVolatile = Me.Check_IsVolatile.Checked
				thisDataRow.FieldIsOptimisable = Me.Check_IsOptimisable.Checked
				thisDataRow.FieldIsSearchable = Me.Check_IsSearchable.Checked

				Dim ThisFieldType As PertracCustomFieldTypes
				ThisFieldType = CType(thisDataRow.CustomFieldType, PertracCustomFieldTypes)

				If System.Enum.IsDefined(GetType(PertracCustomFieldTypes_NotCalculated), ThisFieldType.ToString) Then
					thisDataRow.FieldIsCalculated = False
				Else
					thisDataRow.FieldIsCalculated = True
				End If

				thisDataRow.DefaultValue = ""
				Dim DefaultValue As Object

				Select Case ThisFieldType
					Case PertracCustomFieldTypes.CustomString
						thisDataRow.FieldDataType = RenaissanceDataType.TextType
						thisDataRow.DefaultValue = Text_DefaultValue.Text

					Case PertracCustomFieldTypes.CustomDate
						thisDataRow.FieldDataType = RenaissanceDataType.DateType

						If (Text_DefaultValue.Text.Length > 0) Then
							DefaultValue = ConvertValue(Text_DefaultValue.Text, GetType(Date))
							If (DefaultValue IsNot Nothing) AndAlso (IsDate(DefaultValue)) Then
								thisDataRow.DefaultValue = CDate(DefaultValue).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
							End If
						End If

					Case PertracCustomFieldTypes.CustomBoolean
						thisDataRow.FieldDataType = RenaissanceDataType.BooleanType

						If (Text_DefaultValue.Text.Length > 0) Then
							DefaultValue = ConvertValue(Text_DefaultValue.Text, GetType(Boolean))
							If DefaultValue IsNot Nothing Then
								thisDataRow.DefaultValue = CBool(DefaultValue).ToString
							End If
						End If

					Case PertracCustomFieldTypes.CustomNumeric
						thisDataRow.FieldDataType = RenaissanceDataType.NumericType

						If (Text_DefaultValue.Text.Length > 0) AndAlso (ConvertIsNumeric(Text_DefaultValue.Text)) Then
							DefaultValue = ConvertValue(Text_DefaultValue.Text, GetType(Double))
							If DefaultValue IsNot Nothing Then
								thisDataRow.DefaultValue = CDbl(DefaultValue).ToString
							End If
						End If

					Case PertracCustomFieldTypes.ReturnDate
						thisDataRow.FieldDataType = RenaissanceDataType.DateType

					Case PertracCustomFieldTypes.Alpha, PertracCustomFieldTypes.DrawDown, PertracCustomFieldTypes.DrawUp, PertracCustomFieldTypes.PeriodReturn, PertracCustomFieldTypes.SingleReturn, PertracCustomFieldTypes.Volatility
						thisDataRow.FieldDataType = RenaissanceDataType.PercentageType Or RenaissanceDataType.NumericType

					Case Else
						thisDataRow.FieldDataType = RenaissanceDataType.NumericType

				End Select

				thisDataRow.FieldDetails = ""

				thisDataRow.EndEdit()
				InPaint = False

				' Add and Update DataRow. 

				ErrFlag = False

				If AddNewRecord = True Then
					Try
						myTable.Rows.Add(thisDataRow)
					Catch ex As Exception
						ErrFlag = True
						ErrMessage = ex.Message
						ErrStack = ex.StackTrace
					End Try
				End If

				UpdateRows(0) = thisDataRow

				' Post Additions / Updates to the underlying table :-
				Dim temp As Integer
				Try
					If (ErrFlag = False) Then
						myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
						myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

						temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
					End If

				Catch ex As Exception

					ErrMessage = ex.Message
					ErrFlag = True
					ErrStack = ex.StackTrace

				End Try

				thisAuditID = thisDataRow.AuditID

			End SyncLock

		Catch ex As Exception
			If (ErrFlag = False) Then
				ErrFlag = True
				ErrMessage = ex.Message
				ErrStack = ex.StackTrace
			End If
		Finally
			InPaint = False
		End Try


		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.edit_FieldName.Enabled = True
			Me.Combo_CustomFieldType.Enabled = True

			Call Combo_CustomFieldType_SelectedIndexChanged(Me.Combo_CustomFieldType, New EventArgs)

			'  Me.Date_StartDate.Enabled = True
			'  Me.Date_EndDate.Enabled = True
			'  Me.Edit_PeriodCount.Enabled = True
			'  Me.Radio_MAX.Enabled = True
			'  Me.Radio_MIN.Enabled = True
			'  Me.Radio_First.Enabled = True
			'  Me.Radio_Last.Enabled = True
			'  Me.Radio_Average.Enabled = True
			'  Me.Check_IsVolatile.Enabled = True

			Me.Check_IsOptimisable.Enabled = True
			Me.Check_IsSearchable.Enabled = True

		Else

			Me.edit_FieldName.Enabled = False
			Me.Combo_CustomFieldType.Enabled = False
			Me.Date_StartDate.Enabled = False
			Me.Date_EndDate.Enabled = False
			Me.Edit_PeriodCount.Enabled = False
			Me.Radio_MAX.Enabled = False
			Me.Radio_MIN.Enabled = False
			Me.Radio_First.Enabled = False
			Me.Radio_Last.Enabled = False
			Me.Radio_Average.Enabled = False
			Me.Check_IRR.Enabled = False
			Me.Check_IsVolatile.Enabled = False
			Me.Check_IsOptimisable.Enabled = False
			Me.Check_IsSearchable.Enabled = False

		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.edit_FieldName.Text.Length <= 0 Then
			pReturnString = "The Group Name must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " From Control Event Code (Form Specific Code)"

  Private Sub Combo_CustomFieldType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_CustomFieldType.SelectedIndexChanged
    ' *********************************************************************************************
    '
    '
    ' *********************************************************************************************

    Dim IsNotCalculated As Boolean = False
    Dim ThisFieldType As PertracCustomFieldTypes = PertracCustomFieldTypes.ReturnDate ' Arbitrary default.

    Try
      If (Combo_CustomFieldType.SelectedIndex >= 0) Then
        If (Combo_CustomFieldType.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_CustomFieldType.SelectedValue)) Then

          ThisFieldType = CType(Combo_CustomFieldType.SelectedValue, PertracCustomFieldTypes)

          If System.Enum.IsDefined(GetType(PertracCustomFieldTypes_NotCalculated), ThisFieldType.ToString) Then
            IsNotCalculated = True
          End If

        End If
      End If
    Catch ex As Exception
    End Try

    If IsNotCalculated Then
      Me.Date_StartDate.Enabled = False
      Me.Date_EndDate.Enabled = False
      Me.Edit_PeriodCount.Enabled = False
      Me.Radio_MAX.Enabled = False
      Me.Radio_MIN.Enabled = False
      Me.Radio_First.Enabled = False
      Me.Radio_Last.Enabled = False
      Me.Radio_Average.Enabled = False
      Me.Check_IsVolatile.Enabled = False
      Check_IsVolatile.Checked = False
      Me.Check_IRR.Enabled = False
			Check_IRR.Checked = False
			Me.Text_DefaultValue.Enabled = True
    Else
      Select Case ThisFieldType
        Case PertracCustomFieldTypes.PeriodReturn
          Me.Check_IRR.Enabled = True
          Me.Edit_PeriodCount.Enabled = True
          Me.Date_StartDate.Enabled = True
          Me.Date_EndDate.Enabled = True

        Case PertracCustomFieldTypes.DrawDown, PertracCustomFieldTypes.DrawUp
          Me.Check_IRR.Enabled = False
          Me.Check_IRR.Checked = False
          Me.Edit_PeriodCount.Enabled = False
          Me.Date_StartDate.Enabled = False
          Me.Date_EndDate.Enabled = False

        Case PertracCustomFieldTypes.ReturnDate
          Me.Check_IRR.Enabled = False
          Me.Check_IRR.Checked = False
          Me.Edit_PeriodCount.Enabled = False
          Me.Date_StartDate.Enabled = True
          Me.Date_EndDate.Enabled = True

        Case Else
          Me.Check_IRR.Enabled = False
          Me.Check_IRR.Checked = False
          Me.Edit_PeriodCount.Enabled = True
          Me.Date_StartDate.Enabled = True
          Me.Date_EndDate.Enabled = True

      End Select

      Me.Radio_MAX.Enabled = True
      Me.Radio_MIN.Enabled = True
      Me.Radio_First.Enabled = True
      Me.Radio_Last.Enabled = True
      Me.Radio_Average.Enabled = True
      Me.Check_IsVolatile.Enabled = True
			Me.Text_DefaultValue.Enabled = False
		End If

  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

  End Sub

  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Application.DoEvents()

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
    Me.btnCancel.Enabled = True
    Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region





End Class
