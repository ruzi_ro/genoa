Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceGenoaGroupsGlobals
Imports RenaissanceDataClass


Public Class frmDrillDown

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm, IRenaissanceSearchUpdateEvent

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Grid_Results As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents DrillDown_ProgressBar As System.Windows.Forms.ProgressBar
  Friend WithEvents Text_Description As System.Windows.Forms.TextBox
  Friend WithEvents Split_DrillDown As System.Windows.Forms.SplitContainer
  Friend WithEvents Split_DrillDown_GridAndChart As System.Windows.Forms.SplitContainer
  Friend WithEvents Chart_VAMI As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_MonthlyReturns As Dundas.Charting.WinControl.Chart
  Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ShowCharts As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Label_ChartStock As System.Windows.Forms.Label
  Friend WithEvents Button_HideCharts As System.Windows.Forms.Button
  Friend WithEvents Menu_GridFields As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_AnnualisedReturn_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearReturns As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_Volatility_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_StandardDeviation_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_PercentPositive_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SharpeRatio_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearReturns_P1 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearReturns_P2 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearReturns_P3 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearReturns_P4 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearRmtheturns_P5 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_YearRmtheturns_P6 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_M12 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_M24 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_M36 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_M60 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_ITD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_YTD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Fields_SimpleReturns_CustomPeriod As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents CustomPeriodStartDateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_GridFields_StartDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_GridFields_EndDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents btnClose As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDrillDown))
    Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Title1 As Dundas.Charting.WinControl.Title = New Dundas.Charting.WinControl.Title
    Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Title2 As Dundas.Charting.WinControl.Title = New Dundas.Charting.WinControl.Title
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ShowCharts = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_GridFields = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearReturns = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearReturns_P1 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearReturns_P2 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearReturns_P3 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearReturns_P4 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearRmtheturns_P5 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_YearRmtheturns_P6 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SimpleReturns_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_AnnualisedReturn_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_Volatility_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_StandardDeviation_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_PercentPositive_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_M12 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_M24 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_M36 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_M60 = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_ITD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_YTD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Fields_SharpeRatio_CustomPeriod = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.CustomPeriodStartDateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_GridFields_StartDate = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_GridFields_EndDate = New System.Windows.Forms.ToolStripMenuItem
    Me.Grid_Results = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.DrillDown_ProgressBar = New System.Windows.Forms.ProgressBar
    Me.Text_Description = New System.Windows.Forms.TextBox
    Me.Split_DrillDown = New System.Windows.Forms.SplitContainer
    Me.Split_DrillDown_GridAndChart = New System.Windows.Forms.SplitContainer
    Me.Label_ChartStock = New System.Windows.Forms.Label
    Me.Chart_MonthlyReturns = New Dundas.Charting.WinControl.Chart
    Me.Chart_VAMI = New Dundas.Charting.WinControl.Chart
    Me.Button_HideCharts = New System.Windows.Forms.Button
    Me.RootMenu.SuspendLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Split_DrillDown.Panel1.SuspendLayout()
    Me.Split_DrillDown.Panel2.SuspendLayout()
    Me.Split_DrillDown.SuspendLayout()
    Me.Split_DrillDown_GridAndChart.Panel1.SuspendLayout()
    Me.Split_DrillDown_GridAndChart.Panel2.SuspendLayout()
    Me.Split_DrillDown_GridAndChart.SuspendLayout()
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(845, 554)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 13
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.Menu_GridFields})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(923, 24)
    Me.RootMenu.TabIndex = 14
    Me.RootMenu.Text = "MenuStrip1"
    '
    'ToolStripMenuItem1
    '
    Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_ShowCharts})
    Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
    Me.ToolStripMenuItem1.Size = New System.Drawing.Size(51, 20)
    Me.ToolStripMenuItem1.Text = "Charts"
    '
    'Menu_ShowCharts
    '
    Me.Menu_ShowCharts.Checked = True
    Me.Menu_ShowCharts.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_ShowCharts.Name = "Menu_ShowCharts"
    Me.Menu_ShowCharts.Size = New System.Drawing.Size(146, 22)
    Me.Menu_ShowCharts.Text = "Show Charts"
    '
    'Menu_GridFields
    '
    Me.Menu_GridFields.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_YearReturns, Me.Menu_Fields_SimpleReturns, Me.Menu_Fields_AnnualisedReturn, Me.Menu_Fields_Volatility, Me.Menu_Fields_StandardDeviation, Me.Menu_Fields_PercentPositive, Me.Menu_Fields_SharpeRatio, Me.ToolStripSeparator1, Me.CustomPeriodStartDateToolStripMenuItem, Me.Menu_GridFields_StartDate, Me.Menu_GridFields_EndDate})
    Me.Menu_GridFields.Name = "Menu_GridFields"
    Me.Menu_GridFields.Size = New System.Drawing.Size(68, 20)
    Me.Menu_GridFields.Text = "Grid Fields"
    '
    'Menu_Fields_YearReturns
    '
    Me.Menu_Fields_YearReturns.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_YearReturns_P1, Me.Menu_Fields_YearReturns_P2, Me.Menu_Fields_YearReturns_P3, Me.Menu_Fields_YearReturns_P4, Me.Menu_Fields_YearRmtheturns_P5, Me.Menu_Fields_YearRmtheturns_P6})
    Me.Menu_Fields_YearReturns.Name = "Menu_Fields_YearReturns"
    Me.Menu_Fields_YearReturns.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_YearReturns.Text = "Year Returns"
    '
    'Menu_Fields_YearReturns_P1
    '
    Me.Menu_Fields_YearReturns_P1.Name = "Menu_Fields_YearReturns_P1"
    Me.Menu_Fields_YearReturns_P1.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearReturns_P1.Text = "P1"
    '
    'Menu_Fields_YearReturns_P2
    '
    Me.Menu_Fields_YearReturns_P2.Name = "Menu_Fields_YearReturns_P2"
    Me.Menu_Fields_YearReturns_P2.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearReturns_P2.Text = "P2"
    '
    'Menu_Fields_YearReturns_P3
    '
    Me.Menu_Fields_YearReturns_P3.Name = "Menu_Fields_YearReturns_P3"
    Me.Menu_Fields_YearReturns_P3.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearReturns_P3.Text = "P3"
    '
    'Menu_Fields_YearReturns_P4
    '
    Me.Menu_Fields_YearReturns_P4.Name = "Menu_Fields_YearReturns_P4"
    Me.Menu_Fields_YearReturns_P4.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearReturns_P4.Text = "P4"
    '
    'Menu_Fields_YearRmtheturns_P5
    '
    Me.Menu_Fields_YearRmtheturns_P5.Name = "Menu_Fields_YearRmtheturns_P5"
    Me.Menu_Fields_YearRmtheturns_P5.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearRmtheturns_P5.Text = "P5"
    '
    'Menu_Fields_YearRmtheturns_P6
    '
    Me.Menu_Fields_YearRmtheturns_P6.Name = "Menu_Fields_YearRmtheturns_P6"
    Me.Menu_Fields_YearRmtheturns_P6.Size = New System.Drawing.Size(97, 22)
    Me.Menu_Fields_YearRmtheturns_P6.Text = "P6"
    '
    'Menu_Fields_SimpleReturns
    '
    Me.Menu_Fields_SimpleReturns.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_SimpleReturns_M12, Me.Menu_Fields_SimpleReturns_M24, Me.Menu_Fields_SimpleReturns_M36, Me.Menu_Fields_SimpleReturns_M60, Me.Menu_Fields_SimpleReturns_ITD, Me.Menu_Fields_SimpleReturns_YTD, Me.Menu_Fields_SimpleReturns_CustomPeriod})
    Me.Menu_Fields_SimpleReturns.Name = "Menu_Fields_SimpleReturns"
    Me.Menu_Fields_SimpleReturns.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_SimpleReturns.Text = "Simple Returns"
    '
    'Menu_Fields_SimpleReturns_M12
    '
    Me.Menu_Fields_SimpleReturns_M12.Name = "Menu_Fields_SimpleReturns_M12"
    Me.Menu_Fields_SimpleReturns_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_M12.Text = "12 Month"
    '
    'Menu_Fields_SimpleReturns_M24
    '
    Me.Menu_Fields_SimpleReturns_M24.Name = "Menu_Fields_SimpleReturns_M24"
    Me.Menu_Fields_SimpleReturns_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_M24.Text = "24 Month"
    '
    'Menu_Fields_SimpleReturns_M36
    '
    Me.Menu_Fields_SimpleReturns_M36.Name = "Menu_Fields_SimpleReturns_M36"
    Me.Menu_Fields_SimpleReturns_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_M36.Text = "36 Month"
    '
    'Menu_Fields_SimpleReturns_M60
    '
    Me.Menu_Fields_SimpleReturns_M60.Name = "Menu_Fields_SimpleReturns_M60"
    Me.Menu_Fields_SimpleReturns_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_M60.Text = "60 Month"
    '
    'Menu_Fields_SimpleReturns_ITD
    '
    Me.Menu_Fields_SimpleReturns_ITD.Name = "Menu_Fields_SimpleReturns_ITD"
    Me.Menu_Fields_SimpleReturns_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_SimpleReturns_YTD
    '
    Me.Menu_Fields_SimpleReturns_YTD.Name = "Menu_Fields_SimpleReturns_YTD"
    Me.Menu_Fields_SimpleReturns_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_YTD.Text = "Year To Date"
    '
    'Menu_Fields_SimpleReturns_CustomPeriod
    '
    Me.Menu_Fields_SimpleReturns_CustomPeriod.Name = "Menu_Fields_SimpleReturns_CustomPeriod"
    Me.Menu_Fields_SimpleReturns_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SimpleReturns_CustomPeriod.Text = "Custom Period"
    '
    'Menu_Fields_AnnualisedReturn
    '
    Me.Menu_Fields_AnnualisedReturn.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_AnnualisedReturn_M12, Me.Menu_Fields_AnnualisedReturn_M24, Me.Menu_Fields_AnnualisedReturn_M36, Me.Menu_Fields_AnnualisedReturn_M60, Me.Menu_Fields_AnnualisedReturn_ITD, Me.Menu_Fields_AnnualisedReturn_YTD, Me.Menu_Fields_AnnualisedReturn_CustomPeriod})
    Me.Menu_Fields_AnnualisedReturn.Name = "Menu_Fields_AnnualisedReturn"
    Me.Menu_Fields_AnnualisedReturn.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_AnnualisedReturn.Text = "Annualised Return"
    '
    'Menu_Fields_AnnualisedReturn_M12
    '
    Me.Menu_Fields_AnnualisedReturn_M12.Name = "Menu_Fields_AnnualisedReturn_M12"
    Me.Menu_Fields_AnnualisedReturn_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_M12.Text = "12 Month"
    '
    'Menu_Fields_AnnualisedReturn_M24
    '
    Me.Menu_Fields_AnnualisedReturn_M24.Name = "Menu_Fields_AnnualisedReturn_M24"
    Me.Menu_Fields_AnnualisedReturn_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_M24.Text = "24 Month"
    '
    'Menu_Fields_AnnualisedReturn_M36
    '
    Me.Menu_Fields_AnnualisedReturn_M36.Name = "Menu_Fields_AnnualisedReturn_M36"
    Me.Menu_Fields_AnnualisedReturn_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_M36.Text = "36 Month"
    '
    'Menu_Fields_AnnualisedReturn_M60
    '
    Me.Menu_Fields_AnnualisedReturn_M60.Name = "Menu_Fields_AnnualisedReturn_M60"
    Me.Menu_Fields_AnnualisedReturn_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_M60.Text = "60 Month"
    '
    'Menu_Fields_AnnualisedReturn_ITD
    '
    Me.Menu_Fields_AnnualisedReturn_ITD.Name = "Menu_Fields_AnnualisedReturn_ITD"
    Me.Menu_Fields_AnnualisedReturn_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_AnnualisedReturn_YTD
    '
    Me.Menu_Fields_AnnualisedReturn_YTD.Name = "Menu_Fields_AnnualisedReturn_YTD"
    Me.Menu_Fields_AnnualisedReturn_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_YTD.Text = "Year To Date"
    '
    'Menu_Fields_AnnualisedReturn_CustomPeriod
    '
    Me.Menu_Fields_AnnualisedReturn_CustomPeriod.Name = "Menu_Fields_AnnualisedReturn_CustomPeriod"
    Me.Menu_Fields_AnnualisedReturn_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_AnnualisedReturn_CustomPeriod.Text = "Custom Period"
    '
    'Menu_Fields_Volatility
    '
    Me.Menu_Fields_Volatility.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_Volatility_M12, Me.Menu_Fields_Volatility_M24, Me.Menu_Fields_Volatility_M36, Me.Menu_Fields_Volatility_M60, Me.Menu_Fields_Volatility_ITD, Me.Menu_Fields_Volatility_YTD, Me.Menu_Fields_Volatility_CustomPeriod})
    Me.Menu_Fields_Volatility.Name = "Menu_Fields_Volatility"
    Me.Menu_Fields_Volatility.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_Volatility.Text = "Volatility"
    '
    'Menu_Fields_Volatility_M12
    '
    Me.Menu_Fields_Volatility_M12.Name = "Menu_Fields_Volatility_M12"
    Me.Menu_Fields_Volatility_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_M12.Text = "12 Month"
    '
    'Menu_Fields_Volatility_M24
    '
    Me.Menu_Fields_Volatility_M24.Name = "Menu_Fields_Volatility_M24"
    Me.Menu_Fields_Volatility_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_M24.Text = "24 Month"
    '
    'Menu_Fields_Volatility_M36
    '
    Me.Menu_Fields_Volatility_M36.Name = "Menu_Fields_Volatility_M36"
    Me.Menu_Fields_Volatility_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_M36.Text = "36 Month"
    '
    'Menu_Fields_Volatility_M60
    '
    Me.Menu_Fields_Volatility_M60.Name = "Menu_Fields_Volatility_M60"
    Me.Menu_Fields_Volatility_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_M60.Text = "60 Month"
    '
    'Menu_Fields_Volatility_ITD
    '
    Me.Menu_Fields_Volatility_ITD.Name = "Menu_Fields_Volatility_ITD"
    Me.Menu_Fields_Volatility_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_Volatility_YTD
    '
    Me.Menu_Fields_Volatility_YTD.Name = "Menu_Fields_Volatility_YTD"
    Me.Menu_Fields_Volatility_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_YTD.Text = "Year To Date"
    '
    'Menu_Fields_Volatility_CustomPeriod
    '
    Me.Menu_Fields_Volatility_CustomPeriod.Name = "Menu_Fields_Volatility_CustomPeriod"
    Me.Menu_Fields_Volatility_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_Volatility_CustomPeriod.Text = "Custom Period"
    '
    'Menu_Fields_StandardDeviation
    '
    Me.Menu_Fields_StandardDeviation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_StandardDeviation_M12, Me.Menu_Fields_StandardDeviation_M24, Me.Menu_Fields_StandardDeviation_M36, Me.Menu_Fields_StandardDeviation_M60, Me.Menu_Fields_StandardDeviation_ITD, Me.Menu_Fields_StandardDeviation_YTD, Me.Menu_Fields_StandardDeviation_CustomPeriod})
    Me.Menu_Fields_StandardDeviation.Name = "Menu_Fields_StandardDeviation"
    Me.Menu_Fields_StandardDeviation.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_StandardDeviation.Text = "Standard Deviation"
    '
    'Menu_Fields_StandardDeviation_M12
    '
    Me.Menu_Fields_StandardDeviation_M12.Name = "Menu_Fields_StandardDeviation_M12"
    Me.Menu_Fields_StandardDeviation_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_M12.Text = "12 Month"
    '
    'Menu_Fields_StandardDeviation_M24
    '
    Me.Menu_Fields_StandardDeviation_M24.Name = "Menu_Fields_StandardDeviation_M24"
    Me.Menu_Fields_StandardDeviation_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_M24.Text = "24 Month"
    '
    'Menu_Fields_StandardDeviation_M36
    '
    Me.Menu_Fields_StandardDeviation_M36.Name = "Menu_Fields_StandardDeviation_M36"
    Me.Menu_Fields_StandardDeviation_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_M36.Text = "36 Month"
    '
    'Menu_Fields_StandardDeviation_M60
    '
    Me.Menu_Fields_StandardDeviation_M60.Name = "Menu_Fields_StandardDeviation_M60"
    Me.Menu_Fields_StandardDeviation_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_M60.Text = "60 Month"
    '
    'Menu_Fields_StandardDeviation_ITD
    '
    Me.Menu_Fields_StandardDeviation_ITD.Name = "Menu_Fields_StandardDeviation_ITD"
    Me.Menu_Fields_StandardDeviation_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_StandardDeviation_YTD
    '
    Me.Menu_Fields_StandardDeviation_YTD.Name = "Menu_Fields_StandardDeviation_YTD"
    Me.Menu_Fields_StandardDeviation_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_YTD.Text = "Year To Date"
    '
    'Menu_Fields_StandardDeviation_CustomPeriod
    '
    Me.Menu_Fields_StandardDeviation_CustomPeriod.Name = "Menu_Fields_StandardDeviation_CustomPeriod"
    Me.Menu_Fields_StandardDeviation_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_StandardDeviation_CustomPeriod.Text = "Custom Period"
    '
    'Menu_Fields_PercentPositive
    '
    Me.Menu_Fields_PercentPositive.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_PercentPositive_M12, Me.Menu_Fields_PercentPositive_M24, Me.Menu_Fields_PercentPositive_M36, Me.Menu_Fields_PercentPositive_M60, Me.Menu_Fields_PercentPositive_ITD, Me.Menu_Fields_PercentPositive_YTD, Me.Menu_Fields_PercentPositive_CustomPeriod})
    Me.Menu_Fields_PercentPositive.Name = "Menu_Fields_PercentPositive"
    Me.Menu_Fields_PercentPositive.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_PercentPositive.Text = "Percent Positive"
    '
    'Menu_Fields_PercentPositive_M12
    '
    Me.Menu_Fields_PercentPositive_M12.Name = "Menu_Fields_PercentPositive_M12"
    Me.Menu_Fields_PercentPositive_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_M12.Text = "12 Month"
    '
    'Menu_Fields_PercentPositive_M24
    '
    Me.Menu_Fields_PercentPositive_M24.Name = "Menu_Fields_PercentPositive_M24"
    Me.Menu_Fields_PercentPositive_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_M24.Text = "24 Month"
    '
    'Menu_Fields_PercentPositive_M36
    '
    Me.Menu_Fields_PercentPositive_M36.Name = "Menu_Fields_PercentPositive_M36"
    Me.Menu_Fields_PercentPositive_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_M36.Text = "36 Month"
    '
    'Menu_Fields_PercentPositive_M60
    '
    Me.Menu_Fields_PercentPositive_M60.Name = "Menu_Fields_PercentPositive_M60"
    Me.Menu_Fields_PercentPositive_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_M60.Text = "60 Month"
    '
    'Menu_Fields_PercentPositive_ITD
    '
    Me.Menu_Fields_PercentPositive_ITD.Name = "Menu_Fields_PercentPositive_ITD"
    Me.Menu_Fields_PercentPositive_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_PercentPositive_YTD
    '
    Me.Menu_Fields_PercentPositive_YTD.Name = "Menu_Fields_PercentPositive_YTD"
    Me.Menu_Fields_PercentPositive_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_YTD.Text = "Year To Date"
    '
    'Menu_Fields_PercentPositive_CustomPeriod
    '
    Me.Menu_Fields_PercentPositive_CustomPeriod.Name = "Menu_Fields_PercentPositive_CustomPeriod"
    Me.Menu_Fields_PercentPositive_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_PercentPositive_CustomPeriod.Text = "Custom Period"
    '
    'Menu_Fields_SharpeRatio
    '
    Me.Menu_Fields_SharpeRatio.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Fields_SharpeRatio_M12, Me.Menu_Fields_SharpeRatio_M24, Me.Menu_Fields_SharpeRatio_M36, Me.Menu_Fields_SharpeRatio_M60, Me.Menu_Fields_SharpeRatio_ITD, Me.Menu_Fields_SharpeRatio_YTD, Me.Menu_Fields_SharpeRatio_CustomPeriod})
    Me.Menu_Fields_SharpeRatio.Name = "Menu_Fields_SharpeRatio"
    Me.Menu_Fields_SharpeRatio.Size = New System.Drawing.Size(185, 22)
    Me.Menu_Fields_SharpeRatio.Text = "Sharpe Ratio (0%)"
    '
    'Menu_Fields_SharpeRatio_M12
    '
    Me.Menu_Fields_SharpeRatio_M12.Name = "Menu_Fields_SharpeRatio_M12"
    Me.Menu_Fields_SharpeRatio_M12.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_M12.Text = "12 Month"
    '
    'Menu_Fields_SharpeRatio_M24
    '
    Me.Menu_Fields_SharpeRatio_M24.Name = "Menu_Fields_SharpeRatio_M24"
    Me.Menu_Fields_SharpeRatio_M24.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_M24.Text = "24 Month"
    '
    'Menu_Fields_SharpeRatio_M36
    '
    Me.Menu_Fields_SharpeRatio_M36.Name = "Menu_Fields_SharpeRatio_M36"
    Me.Menu_Fields_SharpeRatio_M36.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_M36.Text = "36 Month"
    '
    'Menu_Fields_SharpeRatio_M60
    '
    Me.Menu_Fields_SharpeRatio_M60.Name = "Menu_Fields_SharpeRatio_M60"
    Me.Menu_Fields_SharpeRatio_M60.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_M60.Text = "60 Month"
    '
    'Menu_Fields_SharpeRatio_ITD
    '
    Me.Menu_Fields_SharpeRatio_ITD.Name = "Menu_Fields_SharpeRatio_ITD"
    Me.Menu_Fields_SharpeRatio_ITD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_ITD.Text = "Inception To Date"
    '
    'Menu_Fields_SharpeRatio_YTD
    '
    Me.Menu_Fields_SharpeRatio_YTD.Name = "Menu_Fields_SharpeRatio_YTD"
    Me.Menu_Fields_SharpeRatio_YTD.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_YTD.Text = "Year To Date"
    '
    'Menu_Fields_SharpeRatio_CustomPeriod
    '
    Me.Menu_Fields_SharpeRatio_CustomPeriod.Name = "Menu_Fields_SharpeRatio_CustomPeriod"
    Me.Menu_Fields_SharpeRatio_CustomPeriod.Size = New System.Drawing.Size(171, 22)
    Me.Menu_Fields_SharpeRatio_CustomPeriod.Text = "Custom Period"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(182, 6)
    '
    'CustomPeriodStartDateToolStripMenuItem
    '
    Me.CustomPeriodStartDateToolStripMenuItem.Name = "CustomPeriodStartDateToolStripMenuItem"
    Me.CustomPeriodStartDateToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
    Me.CustomPeriodStartDateToolStripMenuItem.Text = "Custom Period Dates"
    '
    'Menu_GridFields_StartDate
    '
    Me.Menu_GridFields_StartDate.Name = "Menu_GridFields_StartDate"
    Me.Menu_GridFields_StartDate.Size = New System.Drawing.Size(185, 22)
    Me.Menu_GridFields_StartDate.Text = "Start :"
    '
    'Menu_GridFields_EndDate
    '
    Me.Menu_GridFields_EndDate.Name = "Menu_GridFields_EndDate"
    Me.Menu_GridFields_EndDate.Size = New System.Drawing.Size(185, 22)
    Me.Menu_GridFields_EndDate.Text = "End :"
    '
    'Grid_Results
    '
    Me.Grid_Results.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Results.ColumnInfo = resources.GetString("Grid_Results.ColumnInfo")
    Me.Grid_Results.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Results.Location = New System.Drawing.Point(1, 1)
    Me.Grid_Results.Name = "Grid_Results"
    Me.Grid_Results.Rows.DefaultSize = 17
    Me.Grid_Results.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Results.Size = New System.Drawing.Size(618, 415)
    Me.Grid_Results.TabIndex = 15
    '
    'DrillDown_ProgressBar
    '
    Me.DrillDown_ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.DrillDown_ProgressBar.Location = New System.Drawing.Point(12, 554)
    Me.DrillDown_ProgressBar.Name = "DrillDown_ProgressBar"
    Me.DrillDown_ProgressBar.Size = New System.Drawing.Size(610, 23)
    Me.DrillDown_ProgressBar.TabIndex = 16
    Me.DrillDown_ProgressBar.Visible = False
    '
    'Text_Description
    '
    Me.Text_Description.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_Description.Location = New System.Drawing.Point(1, 1)
    Me.Text_Description.Multiline = True
    Me.Text_Description.Name = "Text_Description"
    Me.Text_Description.ReadOnly = True
    Me.Text_Description.ScrollBars = System.Windows.Forms.ScrollBars.Both
    Me.Text_Description.Size = New System.Drawing.Size(916, 92)
    Me.Text_Description.TabIndex = 17
    Me.Text_Description.WordWrap = False
    '
    'Split_DrillDown
    '
    Me.Split_DrillDown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_DrillDown.Location = New System.Drawing.Point(2, 26)
    Me.Split_DrillDown.Name = "Split_DrillDown"
    Me.Split_DrillDown.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'Split_DrillDown.Panel1
    '
    Me.Split_DrillDown.Panel1.Controls.Add(Me.Text_Description)
    '
    'Split_DrillDown.Panel2
    '
    Me.Split_DrillDown.Panel2.Controls.Add(Me.Split_DrillDown_GridAndChart)
    Me.Split_DrillDown.Size = New System.Drawing.Size(918, 517)
    Me.Split_DrillDown.SplitterDistance = 94
    Me.Split_DrillDown.TabIndex = 18
    '
    'Split_DrillDown_GridAndChart
    '
    Me.Split_DrillDown_GridAndChart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_DrillDown_GridAndChart.Location = New System.Drawing.Point(1, 1)
    Me.Split_DrillDown_GridAndChart.Name = "Split_DrillDown_GridAndChart"
    '
    'Split_DrillDown_GridAndChart.Panel1
    '
    Me.Split_DrillDown_GridAndChart.Panel1.Controls.Add(Me.Grid_Results)
    Me.Split_DrillDown_GridAndChart.Panel1MinSize = 0
    '
    'Split_DrillDown_GridAndChart.Panel2
    '
    Me.Split_DrillDown_GridAndChart.Panel2.Controls.Add(Me.Label_ChartStock)
    Me.Split_DrillDown_GridAndChart.Panel2.Controls.Add(Me.Chart_MonthlyReturns)
    Me.Split_DrillDown_GridAndChart.Panel2.Controls.Add(Me.Chart_VAMI)
    Me.Split_DrillDown_GridAndChart.Panel2MinSize = 0
    Me.Split_DrillDown_GridAndChart.Size = New System.Drawing.Size(916, 417)
    Me.Split_DrillDown_GridAndChart.SplitterDistance = 620
    Me.Split_DrillDown_GridAndChart.TabIndex = 16
    '
    'Label_ChartStock
    '
    Me.Label_ChartStock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ChartStock.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_ChartStock.Location = New System.Drawing.Point(3, 5)
    Me.Label_ChartStock.Name = "Label_ChartStock"
    Me.Label_ChartStock.Size = New System.Drawing.Size(286, 22)
    Me.Label_ChartStock.TabIndex = 4
    '
    'Chart_MonthlyReturns
    '
    Me.Chart_MonthlyReturns.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.Chart_MonthlyReturns.BackColor = System.Drawing.Color.Azure
    Me.Chart_MonthlyReturns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_MonthlyReturns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_MonthlyReturns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea1.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisX.LabelStyle.Format = "Y"
    ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisY.LabelStyle.Format = "P0"
    ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.StartFromZero = False
    ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.BackColor = System.Drawing.Color.Transparent
    ChartArea1.BorderColor = System.Drawing.Color.DimGray
    ChartArea1.Name = "Default"
    Me.Chart_MonthlyReturns.ChartAreas.Add(ChartArea1)
    Legend1.BackColor = System.Drawing.Color.Transparent
    Legend1.BorderColor = System.Drawing.Color.Transparent
    Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend1.DockToChartArea = "Default"
    Legend1.Enabled = False
    Legend1.Name = "Default"
    Me.Chart_MonthlyReturns.Legends.Add(Legend1)
    Me.Chart_MonthlyReturns.Location = New System.Drawing.Point(1, 229)
    Me.Chart_MonthlyReturns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_MonthlyReturns.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_MonthlyReturns.Name = "Chart_MonthlyReturns"
    Me.Chart_MonthlyReturns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series1.BorderWidth = 2
    Series1.ChartType = "Line"
    Series1.CustomAttributes = "LabelStyle=Top"
    Series1.Name = "Series1"
    Series1.ShadowOffset = 1
    Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series2.BorderWidth = 2
    Series2.ChartType = "Line"
    Series2.CustomAttributes = "LabelStyle=Top"
    Series2.Name = "Series2"
    Series2.ShadowOffset = 1
    Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_MonthlyReturns.Series.Add(Series1)
    Me.Chart_MonthlyReturns.Series.Add(Series2)
    Me.Chart_MonthlyReturns.Size = New System.Drawing.Size(290, 188)
    Me.Chart_MonthlyReturns.TabIndex = 3
    Me.Chart_MonthlyReturns.Text = "Chart2"
    Title1.Name = "Title1"
    Title1.Text = "Rolling 12 Mth Return"
    Me.Chart_MonthlyReturns.Titles.Add(Title1)
    '
    'Chart_VAMI
    '
    Me.Chart_VAMI.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.Chart_VAMI.BackColor = System.Drawing.Color.Azure
    Me.Chart_VAMI.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_VAMI.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_VAMI.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_VAMI.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_VAMI.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea2.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisX.LabelStyle.Format = "Y"
    ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisY.LabelStyle.Format = "N0"
    ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.StartFromZero = False
    ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.BackColor = System.Drawing.Color.Transparent
    ChartArea2.BorderColor = System.Drawing.Color.DimGray
    ChartArea2.Name = "Default"
    Me.Chart_VAMI.ChartAreas.Add(ChartArea2)
    Legend2.BackColor = System.Drawing.Color.Transparent
    Legend2.BorderColor = System.Drawing.Color.Transparent
    Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend2.DockToChartArea = "Default"
    Legend2.Enabled = False
    Legend2.Name = "Default"
    Me.Chart_VAMI.Legends.Add(Legend2)
    Me.Chart_VAMI.Location = New System.Drawing.Point(1, 37)
    Me.Chart_VAMI.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_VAMI.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_VAMI.Name = "Chart_VAMI"
    Me.Chart_VAMI.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series3.BorderWidth = 2
    Series3.ChartType = "Line"
    Series3.CustomAttributes = "LabelStyle=Top"
    Series3.Name = "Series1"
    Series3.ShadowOffset = 1
    Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series4.BorderWidth = 2
    Series4.ChartType = "Line"
    Series4.CustomAttributes = "LabelStyle=Top"
    Series4.Name = "Series2"
    Series4.ShadowOffset = 1
    Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_VAMI.Series.Add(Series3)
    Me.Chart_VAMI.Series.Add(Series4)
    Me.Chart_VAMI.Size = New System.Drawing.Size(290, 190)
    Me.Chart_VAMI.TabIndex = 2
    Me.Chart_VAMI.Text = "Chart2"
    Title2.Name = "Title1"
    Title2.Text = "VAMI"
    Me.Chart_VAMI.Titles.Add(Title2)
    '
    'Button_HideCharts
    '
    Me.Button_HideCharts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_HideCharts.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_HideCharts.Location = New System.Drawing.Point(696, 554)
    Me.Button_HideCharts.Name = "Button_HideCharts"
    Me.Button_HideCharts.Size = New System.Drawing.Size(117, 28)
    Me.Button_HideCharts.TabIndex = 19
    Me.Button_HideCharts.Text = "Hide Charts"
    '
    'frmDrillDown
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(923, 589)
    Me.Controls.Add(Me.Button_HideCharts)
    Me.Controls.Add(Me.Split_DrillDown)
    Me.Controls.Add(Me.DrillDown_ProgressBar)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(300, 200)
    Me.Name = "frmDrillDown"
    Me.Text = "Drilldown"
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Split_DrillDown.Panel1.ResumeLayout(False)
    Me.Split_DrillDown.Panel1.PerformLayout()
    Me.Split_DrillDown.Panel2.ResumeLayout(False)
    Me.Split_DrillDown.ResumeLayout(False)
    Me.Split_DrillDown_GridAndChart.Panel1.ResumeLayout(False)
    Me.Split_DrillDown_GridAndChart.Panel2.ResumeLayout(False)
    Me.Split_DrillDown_GridAndChart.ResumeLayout(False)
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

  Private Const DRILL_DOWN_SAMPLE_SIZE As Integer = 1000
  Private Const DRILLDOWN_GROUP_PRICE_SERIES_PERIODS As Integer = 60
  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

#Region " Form Locals and Constants "

  Public Event SearchUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer) Implements IRenaissanceSearchUpdateEvent.RenaissanceSearchUpdateEvent
  Public Event StatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer) Implements IRenaissanceSearchUpdateEvent.RenaissanceGroupStatsUpdateEvent

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain
  Private WithEvents _DrilldownParent As IRenaissanceSearchUpdateEvent = Nothing

  Private DrillDown_GraphGroupID As Integer

  Dim DrilldownMenu As ToolStripMenuItem = Nothing

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  Private GenericUpdateObject As RenaissanceGlobals.RenaissanceTimerUpdateClass
  Private InTimedChartUpdate_Tick As Boolean = False

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Data Structures

  Private myConnection As SqlConnection

  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  Private ParentDynamicGroupID As Integer
  Private DrillDownFieldIDs() As Integer
  Private DrillDownFieldIsCustom() As Boolean
  Private IsCompoundGroup As Boolean
  Private DrillDownDescription As String

  Private WithEvents DrillDownUpdateWorker As System.ComponentModel.BackgroundWorker = Nothing

  ' Active Element.

  Private _IsOverCancelButton As Boolean
  Private _InUse As Boolean
  Private _ParentGroup As Integer = 0
  Private MyGroupMembers() As Integer

  Private ReservedGroupIDs As New ArrayList
  Private StringNumerator As New SortedDictionary(Of String, Integer)
  Private SectionDictionary As New SortedDictionary(Of String, Integer)
  Private _CompoundGroups As New SortedDictionary(Of Integer, Integer())
  Private ActiveGroups As New ArrayList

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private _FormOpenFailed As Boolean
  Private InPaint As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  '

  Dim CustomStatsStartDateControl As ToolStripMonthCalendar
  Dim CustomStatsEndDateControl As ToolStripMonthCalendar

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return _IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      _IsOverCancelButton = Value
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

  Public ReadOnly Property CompoundGroups() As System.Collections.Generic.SortedDictionary(Of Integer, Integer()) Implements RenaissanceGenoaGroupsGlobals.IRenaissanceSearchUpdateEvent.CompoundGroups
    Get
      Return _CompoundGroups
    End Get

  End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value
    End Set
  End Property

#End Region

  Public Class ArrayComparerClass
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
      ' ************************************************************************
      ' Implement class to compare two arrays.
      ' Nothing is deemed to be less than something.
      '
      ' ************************************************************************

      Try

        If (TypeOf x Is Array) AndAlso (TypeOf y Is Array) Then
          Dim xArray As Array = x
          Dim yArray As Array = y

          If (x Is Nothing) AndAlso (y Is Nothing) Then
            Return 0

          ElseIf (x Is Nothing) Then
            Return (-1)

          ElseIf (y Is Nothing) Then
            Return (1)

          Else
            ' Compare
            Dim RVal As Integer = 0

            For Index As Integer = 0 To (Math.Min(xArray.Length, yArray.Length) - 1)
              RVal = CType(xArray(Index), IComparable).CompareTo(yArray(Index))

              If RVal <> 0 Then
                Return RVal
              End If

            Next

            If (xArray.Length = yArray.Length) Then
              Return 0
            ElseIf xArray.Length < yArray.Length Then
              Return (-1)
            Else
              Return (1)
            End If

          End If


        Else

          Return 0

        End If

      Catch ex As Exception
      End Try

      Return 0

    End Function

  End Class

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True
    _DefaultStatsDatePeriod = MainForm.DefaultStatsDatePeriod

    DrillDown_GraphGroupID = pMainForm.UniqueGenoaNumber

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = GenoaFormID.frmDrillDown

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList  ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Menu_Fields_AnnualisedReturn_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_AnnualisedReturn_CustomPeriod.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_Volatility_CustomPeriod.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_StandardDeviation_CustomPeriod.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_PercentPositive_CustomPeriod.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SharpeRatio_CustomPeriod.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_M12.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_M24.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_M36.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_M60.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_ITD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_YTD.Click, AddressOf Menu_Field_Clicked
    AddHandler Menu_Fields_SimpleReturns_CustomPeriod.Click, AddressOf Menu_Field_Clicked

    ' Form Control Changed events

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)

    ''Me.Controls.Add(MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent))
    'MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
    Me.RootMenu.PerformLayout()

    Try
      InPaint = True

      DrilldownMenu = SetGroupAndDrillDownMenu(RootMenu)

      Me.Split_DrillDown_GridAndChart.Panel1MinSize = 100
      Me.Split_DrillDown_GridAndChart.Panel2MinSize = 160

      CustomStatsStartDateControl = New ToolStripMonthCalendar
      CustomStatsStartDateControl.Name = "Custom_StartDate"
      CustomStatsStartDateControl.MonthCalendarControl.SetDate(CDate("1 Jan 2005"))
      CustomStatsStartDateControl.MonthCalendarControl.ShowToday = True
      Menu_GridFields_StartDate.Text = "Start : " & CustomStatsStartDateControl.MonthCalendarControl.SelectionStart.ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)

      AddHandler CustomStatsStartDateControl.DateChanged, AddressOf CustomStatsStartDateControl_DateChanged

      Menu_GridFields_StartDate.DropDownItems.Add(CustomStatsStartDateControl)

      CustomStatsEndDateControl = New ToolStripMonthCalendar
      CustomStatsEndDateControl.Name = "Custom_EndDate"
      CustomStatsEndDateControl.MonthCalendarControl.SetDate(Now.Date.AddDays(0 - Now.Day))
      CustomStatsEndDateControl.MonthCalendarControl.ShowToday = True

      Menu_GridFields_EndDate.Text = "End : " & CustomStatsEndDateControl.MonthCalendarControl.SelectionStart.ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)

      AddHandler CustomStatsEndDateControl.DateChanged, AddressOf CustomStatsEndDateControl_DateChanged

      Me.Menu_GridFields_EndDate.DropDownItems.Add(CustomStatsEndDateControl)


    Catch ex As Exception
    Finally
      InPaint = False
    End Try

  End Sub

  Private Sub CustomStatsStartDateControl_DateChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Try
      Menu_GridFields_StartDate.Text = "Start : " & e.Start.ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
      ' Menu_GridFields_StartDate.HideDropDown()

      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Me.InUse) AndAlso (Me.Visible) Then
        SyncLock ActiveGroups
          If (ActiveGroups.Count > 0) Then
            MainForm.ClearGroupUpdateRequest(ActiveGroups.ToArray(GetType(Integer)), Me)
          End If
        End SyncLock

        PostGetDataUpdateRequests(True)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub CustomStatsEndDateControl_DateChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Try
      Menu_GridFields_EndDate.Text = "End : " & e.Start.ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
      ' Menu_GridFields_EndDate.HideDropDown()

      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Me.InUse) AndAlso (Me.Visible) Then
        SyncLock ActiveGroups
          If (ActiveGroups.Count > 0) Then
            MainForm.ClearGroupUpdateRequest(ActiveGroups.ToArray(GetType(Integer)), Me)
          End If
        End SyncLock

        PostGetDataUpdateRequests(True)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_Field_Clicked(ByVal sender As Object, ByVal e As EventArgs)
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Try
      Dim thisMenuItem As ToolStripMenuItem
      Dim ColName As String

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        thisMenuItem = CType(sender, ToolStripMenuItem)
        ColName = thisMenuItem.Name.Substring(12)

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If Grid_Results.Cols.Contains(ColName) Then
          Grid_Results.Cols(ColName).Visible = thisMenuItem.Checked
        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetGridColsVisible()
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Try

      Grid_Results.Cols("AnnualisedReturn_M12").Visible = Menu_Fields_AnnualisedReturn_M12.Checked
      Grid_Results.Cols("AnnualisedReturn_M24").Visible = Menu_Fields_AnnualisedReturn_M24.Checked
      Grid_Results.Cols("AnnualisedReturn_M36").Visible = Menu_Fields_AnnualisedReturn_M36.Checked
      Grid_Results.Cols("AnnualisedReturn_M60").Visible = Menu_Fields_AnnualisedReturn_M60.Checked
      Grid_Results.Cols("AnnualisedReturn_ITD").Visible = Menu_Fields_AnnualisedReturn_ITD.Checked
      Grid_Results.Cols("AnnualisedReturn_YTD").Visible = Menu_Fields_AnnualisedReturn_YTD.Checked
      Grid_Results.Cols("AnnualisedReturn_CustomPeriod").Visible = Menu_Fields_AnnualisedReturn_CustomPeriod.Checked
      Grid_Results.Cols("Volatility_M12").Visible = Menu_Fields_Volatility_M12.Checked
      Grid_Results.Cols("Volatility_M24").Visible = Menu_Fields_Volatility_M24.Checked
      Grid_Results.Cols("Volatility_M36").Visible = Menu_Fields_Volatility_M36.Checked
      Grid_Results.Cols("Volatility_M60").Visible = Menu_Fields_Volatility_M60.Checked
      Grid_Results.Cols("Volatility_ITD").Visible = Menu_Fields_Volatility_ITD.Checked
      Grid_Results.Cols("Volatility_YTD").Visible = Menu_Fields_Volatility_YTD.Checked
      Grid_Results.Cols("Volatility_CustomPeriod").Visible = Menu_Fields_Volatility_CustomPeriod.Checked
      Grid_Results.Cols("StandardDeviation_M12").Visible = Menu_Fields_StandardDeviation_M12.Checked
      Grid_Results.Cols("StandardDeviation_M24").Visible = Menu_Fields_StandardDeviation_M24.Checked
      Grid_Results.Cols("StandardDeviation_M36").Visible = Menu_Fields_StandardDeviation_M36.Checked
      Grid_Results.Cols("StandardDeviation_M60").Visible = Menu_Fields_StandardDeviation_M60.Checked
      Grid_Results.Cols("StandardDeviation_ITD").Visible = Menu_Fields_StandardDeviation_ITD.Checked
      Grid_Results.Cols("StandardDeviation_YTD").Visible = Menu_Fields_StandardDeviation_YTD.Checked
      Grid_Results.Cols("StandardDeviation_CustomPeriod").Visible = Menu_Fields_StandardDeviation_CustomPeriod.Checked
      Grid_Results.Cols("PercentPositive_M12").Visible = Menu_Fields_PercentPositive_M12.Checked
      Grid_Results.Cols("PercentPositive_M24").Visible = Menu_Fields_PercentPositive_M24.Checked
      Grid_Results.Cols("PercentPositive_M36").Visible = Menu_Fields_PercentPositive_M36.Checked
      Grid_Results.Cols("PercentPositive_M60").Visible = Menu_Fields_PercentPositive_M60.Checked
      Grid_Results.Cols("PercentPositive_ITD").Visible = Menu_Fields_PercentPositive_ITD.Checked
      Grid_Results.Cols("PercentPositive_YTD").Visible = Menu_Fields_PercentPositive_YTD.Checked
      Grid_Results.Cols("PercentPositive_CustomPeriod").Visible = Menu_Fields_PercentPositive_CustomPeriod.Checked
      Grid_Results.Cols("SharpeRatio_M12").Visible = Menu_Fields_SharpeRatio_M12.Checked
      Grid_Results.Cols("SharpeRatio_M24").Visible = Menu_Fields_SharpeRatio_M24.Checked
      Grid_Results.Cols("SharpeRatio_M36").Visible = Menu_Fields_SharpeRatio_M36.Checked
      Grid_Results.Cols("SharpeRatio_M60").Visible = Menu_Fields_SharpeRatio_M60.Checked
      Grid_Results.Cols("SharpeRatio_ITD").Visible = Menu_Fields_SharpeRatio_ITD.Checked
      Grid_Results.Cols("SharpeRatio_YTD").Visible = Menu_Fields_SharpeRatio_YTD.Checked
      Grid_Results.Cols("SharpeRatio_CustomPeriod").Visible = Menu_Fields_SharpeRatio_CustomPeriod.Checked
      Grid_Results.Cols("SimpleReturns_M12").Visible = Menu_Fields_SimpleReturns_M12.Checked
      Grid_Results.Cols("SimpleReturns_M24").Visible = Menu_Fields_SimpleReturns_M24.Checked
      Grid_Results.Cols("SimpleReturns_M36").Visible = Menu_Fields_SimpleReturns_M36.Checked
      Grid_Results.Cols("SimpleReturns_M60").Visible = Menu_Fields_SimpleReturns_M60.Checked
      Grid_Results.Cols("SimpleReturns_ITD").Visible = Menu_Fields_SimpleReturns_ITD.Checked
      Grid_Results.Cols("SimpleReturns_YTD").Visible = Menu_Fields_SimpleReturns_YTD.Checked
      Grid_Results.Cols("SimpleReturns_CustomPeriod").Visible = Menu_Fields_SimpleReturns_CustomPeriod.Checked

    Catch ex As Exception
    End Try

  End Sub

  Private Sub ClearFieldMenuChecked()
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Try

      Menu_Fields_AnnualisedReturn_M12.Checked = False
      Menu_Fields_AnnualisedReturn_M24.Checked = False
      Menu_Fields_AnnualisedReturn_M36.Checked = False
      Menu_Fields_AnnualisedReturn_M60.Checked = False
      Menu_Fields_AnnualisedReturn_ITD.Checked = False
      Menu_Fields_AnnualisedReturn_YTD.Checked = False
      Menu_Fields_AnnualisedReturn_CustomPeriod.Checked = False
      Menu_Fields_Volatility_M12.Checked = False
      Menu_Fields_Volatility_M24.Checked = False
      Menu_Fields_Volatility_M36.Checked = False
      Menu_Fields_Volatility_M60.Checked = False
      Menu_Fields_Volatility_ITD.Checked = False
      Menu_Fields_Volatility_YTD.Checked = False
      Menu_Fields_Volatility_CustomPeriod.Checked = False
      Menu_Fields_StandardDeviation_M12.Checked = False
      Menu_Fields_StandardDeviation_M24.Checked = False
      Menu_Fields_StandardDeviation_M36.Checked = False
      Menu_Fields_StandardDeviation_M60.Checked = False
      Menu_Fields_StandardDeviation_ITD.Checked = False
      Menu_Fields_StandardDeviation_YTD.Checked = False
      Menu_Fields_StandardDeviation_CustomPeriod.Checked = False
      Menu_Fields_PercentPositive_M12.Checked = False
      Menu_Fields_PercentPositive_M24.Checked = False
      Menu_Fields_PercentPositive_M36.Checked = False
      Menu_Fields_PercentPositive_M60.Checked = False
      Menu_Fields_PercentPositive_ITD.Checked = False
      Menu_Fields_PercentPositive_YTD.Checked = False
      Menu_Fields_PercentPositive_CustomPeriod.Checked = False
      Menu_Fields_SharpeRatio_M12.Checked = False
      Menu_Fields_SharpeRatio_M24.Checked = False
      Menu_Fields_SharpeRatio_M36.Checked = False
      Menu_Fields_SharpeRatio_M60.Checked = False
      Menu_Fields_SharpeRatio_ITD.Checked = False
      Menu_Fields_SharpeRatio_YTD.Checked = False
      Menu_Fields_SharpeRatio_CustomPeriod.Checked = False
      Menu_Fields_SimpleReturns_M12.Checked = False
      Menu_Fields_SimpleReturns_M24.Checked = False
      Menu_Fields_SimpleReturns_M36.Checked = False
      Menu_Fields_SimpleReturns_M60.Checked = False
      Menu_Fields_SimpleReturns_ITD.Checked = False
      Menu_Fields_SimpleReturns_YTD.Checked = False
      Menu_Fields_SimpleReturns_CustomPeriod.Checked = False

    Catch ex As Exception
    End Try

  End Sub

  Public Sub SetDrilldownParameters(ByRef StatsDatePeriod As DealingPeriod, ByRef pDrilldownParent As IRenaissanceSearchUpdateEvent, ByVal pGroupID As Integer, ByVal pFieldIDHeirarchy() As Integer, ByVal pIsCustomFieldHeirarchy() As Boolean, ByVal pIsCompoundGroup As Boolean, ByVal pCustomStartDate As Date, ByVal pCustomEndDate As Date, ByVal pDescription As String)
    ' ****************************************************************
    '
    '
    ' ****************************************************************
    Try
      CustomStatsStartDateControl.MonthCalendarControl.SetDate(pCustomStartDate)
      CustomStatsEndDateControl.MonthCalendarControl.SetDate(pCustomEndDate)
    Catch ex As Exception
    End Try

    Try

      If (_DrilldownParent IsNot Nothing) Then
        RemoveHandler _DrilldownParent.RenaissanceSearchUpdateEvent, AddressOf DrilldownGroupUpdate
        RemoveHandler _DrilldownParent.RenaissanceGroupStatsUpdateEvent, AddressOf ParentGroupStatsUpdate
      End If

    Catch ex As Exception
    End Try

    Try

      If (pDrilldownParent IsNot Nothing) Then
        _DrilldownParent = pDrilldownParent
        AddHandler _DrilldownParent.RenaissanceSearchUpdateEvent, AddressOf DrilldownGroupUpdate
        AddHandler _DrilldownParent.RenaissanceGroupStatsUpdateEvent, AddressOf ParentGroupStatsUpdate
      End If

      DefaultStatsDatePeriod = StatsDatePeriod

      DrillDownFieldIDs = pFieldIDHeirarchy
      DrillDownFieldIsCustom = pIsCustomFieldHeirarchy
      DrillDownDescription = pDescription

      Me.Text_Description.Text = DrillDownDescription

      ParentDynamicGroupID = pGroupID
      DrillDownFieldIDs = pFieldIDHeirarchy
      DrillDownFieldIsCustom = pIsCustomFieldHeirarchy
      IsCompoundGroup = pIsCompoundGroup

      Call DrilldownGroupUpdate(Nothing, ParentDynamicGroupID)

      If (Me.Visible) Then
        GetFormData()
      End If

      Dim ThisGroupID As Integer = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(pGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median)

      If MainForm.StatFunctions.StatCacheItemExist(StatsDatePeriod, ThisGroupID, False) Then

        Set_LineChart(MainForm, StatsDatePeriod, ThisGroupID, Chart_VAMI, 1, "Universe", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now(), "Left")

        Set_RollingReturnChart(MainForm, StatsDatePeriod, ThisGroupID, Chart_MonthlyReturns, 1, "Universe", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now(), "Left")

      End If

    Catch ex As Exception

    End Try

  End Sub

#Region " DrillDown Menu related Code"

  Private Function SetGroupAndDrillDownMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim DrillDownMenu As ToolStripMenuItem
    Dim thisSubMenu As ToolStripMenuItem = Nothing
    Dim newMenuItem As ToolStripMenuItem

    Try

      If RootMenu.Items.ContainsKey("Menu_SaveAsGroup") Then
        RootMenu.Items.Remove(RootMenu.Items("Menu_SaveAsGroup"))
      End If

      DrillDownMenu = New ToolStripMenuItem("Menu_SaveAsGroup")
      DrillDownMenu.Name = "Menu_SaveAsGroup"
      RootMenu.Items.Add(DrillDownMenu)

      Dim thisDSGroups As RenaissanceDataClass.DSGroupList
      Dim thisGroupTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
      Dim ThisGroup As RenaissanceDataClass.DSGroupList.tblGroupListRow
      Dim SelectedGroups() As RenaissanceDataClass.DSGroupList.tblGroupListRow
      Dim thisMenuItem As ToolStripMenuItem
      Dim HasPermission As Boolean = False
      Dim GroupCounter As Integer

      thisDSGroups = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)
      thisGroupTable = thisDSGroups.tblGroupList
      SelectedGroups = thisGroupTable.Select("True", "GroupListName")

      Try

        thisMenuItem = New ToolStripMenuItem("<New Group>", Nothing, AddressOf Me.Menu_SaveToGroup_Click)
        thisMenuItem.Tag = 0
        DrillDownMenu.DropDownItems.Add(thisMenuItem)
        DrillDownMenu.DropDownItems.Add(New ToolStripSeparator)

        Dim Permissions As Integer

        Permissions = MainForm.CheckPermissions("frmGroupList", RenaissanceGlobals.PermissionFeatureType.TypeForm)

        If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then

          thisMenuItem.Enabled = False

        End If

        Permissions = MainForm.CheckPermissions("frmGroupMembers", RenaissanceGlobals.PermissionFeatureType.TypeForm)

        If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0) OrElse ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0) Then

          HasPermission = True

        End If


        GroupCounter = 0
        For GroupCounter = 0 To (SelectedGroups.Length - 1) ' Each ThisGroup In SelectedGroups
          ThisGroup = SelectedGroups(GroupCounter)

          If (GroupCounter Mod 20) = 0 Then
            If (GroupCounter = 0) Then
              If (GroupCounter + 19) >= (SelectedGroups.Length - 1) Then
                thisSubMenu = New ToolStripMenuItem("Save As Group (A - Z)")
              Else
                thisSubMenu = New ToolStripMenuItem("Save As Group (A - " & SelectedGroups(Math.Min(GroupCounter + 19, SelectedGroups.Length - 1)).GroupListName.Substring(0, 1) & ")")
              End If
            ElseIf (GroupCounter + 19) >= (SelectedGroups.Length - 1) Then
              thisSubMenu = New ToolStripMenuItem("Save As Group (" & ThisGroup.GroupListName.Substring(0, 1) & " - Z)")
            Else
              thisSubMenu = New ToolStripMenuItem("Save As Group (" & ThisGroup.GroupListName.Substring(0, 1) & " - " & SelectedGroups(Math.Min(GroupCounter + 19, SelectedGroups.Length - 1)).GroupListName.Substring(0, 1) & ")")
            End If
            DrillDownMenu.DropDownItems.Add(thisSubMenu)
          End If

          thisMenuItem = New ToolStripMenuItem(ThisGroup.GroupListName, Nothing, AddressOf Me.Menu_SaveToGroup_Click)
          thisMenuItem.Tag = ThisGroup.GroupListID
          thisMenuItem.Enabled = HasPermission

          thisSubMenu.DropDownItems.Add(thisMenuItem)
        Next

      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try

    ' *******************************************************************************************
    ' *******************************************************************************************

    If RootMenu.Items.ContainsKey("Menu_DrillDownfields") Then
      RootMenu.Items.Remove(RootMenu.Items("Menu_DrillDownfields"))
    End If

    DrillDownMenu = New ToolStripMenuItem("Drill Down by field")

    Try
      DrillDownMenu.Name = "Menu_DrillDownfields"

      Dim FieldNames(-1) As String
      Dim FieldCount As Integer


      thisSubMenu = New ToolStripMenuItem("Static Fields (A-K)")
      DrillDownMenu.DropDownItems.Add(thisSubMenu)

      FieldNames = System.Enum.GetNames(GetType(RenaissanceGlobals.PertracInformationFields))
      Array.Sort(FieldNames)

      For FieldCount = 0 To (FieldNames.Length - 1)
        If (FieldNames(FieldCount) <> "MaxValue") AndAlso (FieldNames(FieldCount).Substring(0, 1).ToUpper <= "K") Then
          newMenuItem = thisSubMenu.DropDownItems.Add(FieldNames(FieldCount), Nothing, AddressOf Me.DrilldownGridField)

          newMenuItem.Name = "MenuDrilldown_StaticField_" & FieldNames(FieldCount)
          newMenuItem.Tag = newMenuItem.Name
        End If
      Next

      thisSubMenu = New ToolStripMenuItem("Static Fields (L-Z)")
      DrillDownMenu.DropDownItems.Add(thisSubMenu)

      For FieldCount = 0 To (FieldNames.Length - 1)
        If (FieldNames(FieldCount) <> "MaxValue") AndAlso (FieldNames(FieldCount).Substring(0, 1).ToUpper > "K") Then
          newMenuItem = thisSubMenu.DropDownItems.Add(FieldNames(FieldCount), Nothing, AddressOf Me.DrilldownGridField)

          newMenuItem.Name = "MenuDrilldown_StaticField_" & FieldNames(FieldCount)
          newMenuItem.Tag = newMenuItem.Name
        End If
      Next

      DrillDownMenu.DropDownItems.Add(New ToolStripSeparator)

      thisSubMenu = New ToolStripMenuItem("Custom Fields")
      DrillDownMenu.DropDownItems.Add(thisSubMenu)

      Dim FieldsTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
      Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
      Dim FieldsRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
      Dim RowCount As Integer

      FieldsTable = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0)
      SelectedRows = FieldsTable.Select("True", "FieldName")

      For RowCount = 0 To (SelectedRows.Length - 1)
        FieldsRow = SelectedRows(RowCount)

        newMenuItem = thisSubMenu.DropDownItems.Add(FieldsRow.FieldName, Nothing, AddressOf Me.DrilldownGridField)
        newMenuItem.Name = "MenuDrilldown_CustomField_" & FieldsRow.FieldID.ToString
        newMenuItem.Tag = newMenuItem.Name

      Next

      RootMenu.Items.Add(DrillDownMenu)
      Return DrillDownMenu

    Catch ex As Exception
    End Try

    Return DrillDownMenu

  End Function

  Private Sub DrilldownGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try

      Dim SenderName As String
      Dim Col_Is_Custom As Boolean = False
      Dim FieldID As Integer = 0
      Dim FieldName As String = ""

      If (TypeOf sender Is ToolStripMenuItem) Then
        SenderName = CType(sender, ToolStripMenuItem).Name
        FieldName = CType(sender, ToolStripMenuItem).Text
      ElseIf (TypeOf sender Is ToolStripItem) Then
        SenderName = CType(sender, ToolStripItem).Name
        FieldName = CType(sender, ToolStripItem).Text
      Else
        Exit Sub
      End If

      If (SenderName.StartsWith("MenuDrilldown_")) Then
        SenderName = SenderName.Substring(14)

        If (SenderName.StartsWith("StaticField_")) Then
          FieldID = CInt(System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SenderName.Substring(12)))

        ElseIf (SenderName.StartsWith("CustomField_")) Then
          Col_Is_Custom = True
          FieldID = CInt(SenderName.Substring(12))
        Else
          Exit Sub
        End If

      ElseIf (SenderName.StartsWith("cmsMenuDrilldown_")) Then
        SenderName = SenderName.Substring(17)

        If (SenderName.StartsWith("StaticField_")) Then
          FieldID = CInt(System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SenderName.Substring(12)))
        ElseIf (SenderName.StartsWith("CustomField_")) Then
          Col_Is_Custom = True
        Else
          Exit Sub
        End If

      Else
        Exit Sub
      End If

      ' Create a new Custom Group

      ' Get Selected Groups, Build New Compound Group, Launch new drill Down Form.

      Dim GroupArray() As Integer = Nothing
      Dim NewCompoundGroupID As Integer = (-1)
      Dim NewDescriptionText As System.Text.StringBuilder

      NewDescriptionText = New System.Text.StringBuilder(DrillDownDescription & vbCrLf & New String(Chr(9), (DrillDownFieldIDs.Length)) & "Drill down by " & FieldName)

      Try

        ' First, Which lines / Groups are selected.

        If Grid_Results.Rows.Selected.Count > 0 Then

          GroupArray = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

          For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)
            GroupArray(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")

            NewDescriptionText.Append(vbCrLf & New String(Chr(9), (DrillDownFieldIDs.Length)) & " > ")

            For FieldCounter As Integer = 0 To (DrillDownFieldIDs.Length - 1)
              If (FieldCounter > 0) Then
                NewDescriptionText.Append(",")
              End If
              NewDescriptionText.Append(Grid_Results.Rows.Selected(RowCounter)("Group" & FieldCounter.ToString))
            Next

          Next

          Array.Sort(GroupArray)

        End If

        ' If rows have been selected, check that it does not match an existing group.

        If (GroupArray IsNot Nothing) Then
          Dim GroupCounter As Integer

          ' Does this selection of groups match an existing Custom Group ?

          If (_CompoundGroups.Count > 0) Then
            Dim ExistingCompoundGroups(_CompoundGroups.Count - 1) As Integer
            Dim ArrayComparer As New ArrayComparerClass

            _CompoundGroups.Keys.CopyTo(ExistingCompoundGroups, 0)

            For GroupCounter = 0 To (_CompoundGroups.Count - 1)
              If (ArrayComparer.Compare(GroupArray, _CompoundGroups(ExistingCompoundGroups(GroupCounter))) = 0) Then
                NewCompoundGroupID = ExistingCompoundGroups(GroupCounter)
                Exit For
              End If
            Next

          End If

          SyncLock _CompoundGroups

            ' Get new Group ID if an existing matching group was not found.
            ' Add New group to Compound Groups dictionary.

            If (NewCompoundGroupID <= 0) Then

              NewCompoundGroupID = MainForm.UniqueGenoaNumber

              If Not _CompoundGroups.ContainsKey(NewCompoundGroupID) Then
                _CompoundGroups.Add(NewCompoundGroupID, GroupArray)
              Else
                _CompoundGroups(NewCompoundGroupID) = GroupArray
              End If

              SetCombinedGroup(NewCompoundGroupID, GroupArray)

              MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(NewCompoundGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median), Me, AddressOf GroupStatsUpdateEvent, MainForm.StatFunctions.GetDynamicGroupMemberCount(NewCompoundGroupID), -1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

            End If

          End SyncLock

        End If

      Catch ex As Exception
      End Try

      ' OK, Now open a drilldown form giving the Custom Group ID, the Drilldown field ID and indicate whether or not it is a Custom or Static field.

      If (GroupArray IsNot Nothing) Then

        Dim NewDrilldownForm As frmDrillDown

        Dim NewFieldID(DrillDownFieldIDs.Length) As Integer
        Dim NewIsCustom(DrillDownFieldIsCustom.Length) As Boolean

        Array.Copy(DrillDownFieldIDs, 0, NewFieldID, 0, DrillDownFieldIDs.Length)
        NewFieldID(NewFieldID.Length - 1) = FieldID

        Array.Copy(DrillDownFieldIsCustom, 0, NewIsCustom, 0, DrillDownFieldIsCustom.Length)
        NewIsCustom(NewIsCustom.Length - 1) = Col_Is_Custom


        NewDrilldownForm = CType(MainForm.New_GenoaForm(GenoaFormID.frmDrillDown).Form, frmDrillDown)

        NewDrilldownForm.SetDrilldownParameters(DefaultStatsDatePeriod, Me, NewCompoundGroupID, NewFieldID, NewIsCustom, False, CustomStatsStartDateControl.MonthCalendarControl.SelectionStart, CustomStatsEndDateControl.MonthCalendarControl.SelectionStart, NewDescriptionText.ToString)

        If (Not NewDrilldownForm.IsDisposed) Then
          NewDrilldownForm.Show()
        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_SaveToGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************
    Dim thisMenuItem As ToolStripMenuItem
    Dim ThisListID As Integer = 0
    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

    Try
      If (TypeOf sender Is ToolStripMenuItem) Then
        thisMenuItem = CType(sender, ToolStripMenuItem)
      Else
        Exit Sub
      End If

      If (Grid_Results.Rows.Count <= 1) Then
        Exit Sub
      End If

      ' Check permissions

      Try

        Dim Permissions As Integer

        Permissions = MainForm.CheckPermissions("frmGroupMembers", RenaissanceGlobals.PermissionFeatureType.TypeForm)

        If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
          MainForm.LogError(Me.Name & ", Menu_SaveToGroup_Click()", LOG_LEVELS.Warning, "", "You do not have permission to save Group Members.", "", True)

          Exit Sub
        End If

      Catch ex As Exception
      End Try

      ' 

      If (thisMenuItem.Tag IsNot Nothing) AndAlso (IsNumeric(thisMenuItem.Tag)) Then
        ThisListID = CInt(thisMenuItem.Tag)
      End If

      ' Get reference to the GroupList and GroupItems tables.

      Dim GroupListDataset As RenaissanceDataClass.DSGroupList
      Dim GroupListTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
      Dim newGroupListRow As RenaissanceDataClass.DSGroupList.tblGroupListRow

      GroupListDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)
      GroupListTable = GroupListDataset.tblGroupList

      Dim GroupItemsDataset As RenaissanceDataClass.DSGroupItems
      Dim GroupItemsTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

      GroupItemsDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)
      GroupItemsTable = GroupItemsDataset.tblGroupItems

      ' Add or Edit ?

      If (ThisListID <= 0) Then
        ' Add New Group

        Try

          Dim Permissions As Integer

          Permissions = MainForm.CheckPermissions("frmGroupList", RenaissanceGlobals.PermissionFeatureType.TypeForm)

          If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
            MainForm.LogError(Me.Name & ", Menu_SaveToGroup_Click()", LOG_LEVELS.Warning, "", "You do not have permission to save Add Groups.", "", True)

            Exit Sub
          End If

        Catch ex As Exception
        End Try

        Dim NewGroupName As String

        NewGroupName = InputBox("Please Enter New Group Name", "New Group", "")

        If (NewGroupName.Length <= 0) Then
          Exit Sub
        End If

        newGroupListRow = GroupListTable.NewtblGroupListRow

        newGroupListRow.GroupListName = NewGroupName
        newGroupListRow.GroupGroup = ""
        newGroupListRow.GroupDateFrom = Renaissance_BaseDate
        newGroupListRow.GroupDateTo = Renaissance_EndDate_Data
        newGroupListRow.DefaultConstraintGroup = 0
        newGroupListRow.DefaultCovarianceMatrix = 0

        GroupListTable.Rows.Add(newGroupListRow)
        MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, New DataRow() {newGroupListRow})
        ThisListID = newGroupListRow.GroupListID

        UpdateMessage.TableChanged(RenaissanceChangeID.tblGroupList) = True

      Else
        ' Overwrite existing group :-

        ' Confirm..

        If (MessageBox.Show("Do you want to overwrite the '" & thisMenuItem.Text & "' Group?", "Confirm Overwite Group", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes) Then
          Exit Sub
        End If

      End If

      ' OK, Now Get the Item records for Group 'ThisListID' and Add / Update as necessary.

      ' First, get an array of InstrumentIds.

      Dim PertracIds() As Integer
      Dim ItemCounter As Integer
      Dim IDCounter As Integer
      Dim FoundFlag As Boolean


      If Grid_Results.Rows.Selected.Count > 0 Then

        PertracIds = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

        For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)

          PertracIds(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")

        Next

      Else

        PertracIds = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Count)

        For RowCounter As Integer = 1 To (Grid_Results.Rows.Count - 1)

          PertracIds(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")

        Next

      End If

      Array.Sort(PertracIds)

      If (PertracIds IsNot Nothing) AndAlso (PertracIds.Length > 0) Then

        PertracIds = SetCombinedGroup(PertracIds)

      End If

      ' 

      Dim SelectedItems() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim thisItem As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim NewItem As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

      SelectedItems = GroupItemsTable.Select("GroupID=" & ThisListID.ToString)

      If (SelectedItems IsNot Nothing) AndAlso (SelectedItems.Length > 0) Then

        ' Delete UnUsed Items / Update Existing Items as necessary

        For ItemCounter = 0 To (SelectedItems.Length - 1)
          FoundFlag = False
          thisItem = SelectedItems(ItemCounter)

          For IDCounter = 0 To (PertracIds.Length - 1)
            If (PertracIds(IDCounter) > 0) AndAlso (PertracIds(IDCounter) = thisItem.GroupPertracCode) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If (FoundFlag) Then
            ' This Existing Item exists in the New Pertrac IDs list, So leave it alone.

            ' Clear Pertrac ID so it is not added in the second pass.
            PertracIds(IDCounter) = 0

          Else
            ' This Existing Item does not exist in the New Pertrac IDs list, So Delete

            thisItem.Delete()

          End If

        Next

      End If

      ' Add Items for Ids that were not Updates.

      For IDCounter = 0 To (PertracIds.Length - 1)
        If (PertracIds(IDCounter) > 0) Then
          ' Add New Item

          NewItem = GroupItemsTable.NewtblGroupItemsRow

          NewItem.GroupID = ThisListID
          NewItem.GroupItemID = 0
          NewItem.GroupPertracCode = PertracIds(IDCounter)
          NewItem.GroupIndexCode = 0
          NewItem.GroupSector = ""

          GroupItemsTable.Rows.Add(NewItem)

        End If
      Next

      MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems, GroupItemsTable)

      UpdateMessage.TableChanged(RenaissanceChangeID.tblGroupItems) = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Search items to a Group", ex.StackTrace, True)
    End Try

    ' Propagate changes

    Call MainForm.Main_RaiseEvent(UpdateMessage)

  End Sub

#End Region

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)

  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Me.Text = "Drilldown"

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    Try
      GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf TimedChartUpdate_Tick, 500)

      If (Not Grid_Results.Styles.Contains("Positive")) Then
        Grid_Results.Styles.Add("Positive").ForeColor = Color.Blue
      End If
      If (Not Grid_Results.Styles.Contains("Negative")) Then
        Grid_Results.Styles.Add("Negative").ForeColor = Color.Red
      End If
      If (Not Grid_Results.Styles.Contains("PositiveInterim")) Then
        Grid_Results.Styles.Add("PositiveInterim").ForeColor = Color.Blue
        Grid_Results.Styles.Add("PositiveInterim").BackColor = Color.LemonChiffon
      End If
      If (Not Grid_Results.Styles.Contains("NegativeInterim")) Then
        Grid_Results.Styles.Add("NegativeInterim").ForeColor = Color.Red
        Grid_Results.Styles.Add("NegativeInterim").BackColor = Color.LemonChiffon
      End If

      Dim NewCol As C1.Win.C1FlexGrid.Column
      Dim thisName As String
      Dim FieldNames() As String = {"AnnualisedReturn_M12", "AnnualisedReturn_M24", "AnnualisedReturn_M36", "AnnualisedReturn_M60", "AnnualisedReturn_ITD", "AnnualisedReturn_YTD", "AnnualisedReturn_CustomPeriod", _
                                    "Volatility_M12", "Volatility_M24", "Volatility_M36", "Volatility_M60", "Volatility_ITD", "Volatility_YTD", "Volatility_CustomPeriod", _
                                    "PercentPositive_M12", "PercentPositive_M24", "PercentPositive_M36", "PercentPositive_M60", "PercentPositive_ITD", "PercentPositive_YTD", "PercentPositive_CustomPeriod", _
                                    "SimpleReturns_M12", "SimpleReturns_M24", "SimpleReturns_M36", "SimpleReturns_M60", "SimpleReturns_ITD", "SimpleReturns_YTD", "SimpleReturns_CustomPeriod"}
      Dim FieldCaptions() As String = {"12M Annualised", "24M Annualised", "36M Annualised", "60M Annualised", "ITD Annualised", "YTD Annualised", "Custom Annualised", _
                                    "12M Volatility", "24M Volatility", "36M Volatility", "60M Volatility", "ITD Volatility", "YTD Volatility", "Custom Volatility", _
                                    "12M % Positive", "24M % Positive", "36M % Positive", "60M % Positive", "ITD % Positive", "YTD % Positive", "Custom % Positive", _
                                    "12M Return", "24M Return", "36M Return", "60M Return", "ITD Return", "YTD Return", "Custom Return"}

      For FieldIndex As Integer = 0 To (FieldNames.Length - 1)
        thisName = FieldNames(FieldIndex)

        If (Not Grid_Results.Cols.Contains(thisName)) Then
          NewCol = Grid_Results.Cols.Add()
          NewCol.Name = thisName
          NewCol.Caption = FieldCaptions(FieldIndex)
          NewCol.Format = "#,##0.00%"
          NewCol.DataType = GetType(Double)
          NewCol.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter

          NewCol.AllowDragging = True
          NewCol.AllowEditing = False
          NewCol.AllowMerging = False
          NewCol.AllowResizing = True
          NewCol.AllowSorting = True

        End If
      Next

      FieldNames = New String() {"SharpeRatio_M12", "SharpeRatio_M24", "SharpeRatio_M36", "SharpeRatio_M60", "SharpeRatio_ITD", "SharpeRatio_YTD", "SharpeRatio_CustomPeriod"}
      FieldCaptions = New String() {"12M Sharpe", "24M Sharpe", "36M Sharpe", "60M Sharpe", "ITD Sharpe", "YTD Sharpe", "Custom Sharpe"}

      For FieldIndex As Integer = 0 To (FieldNames.Length - 1)
        thisName = FieldNames(FieldIndex)

        If (Not Grid_Results.Cols.Contains(thisName)) Then
          NewCol = Grid_Results.Cols.Add()
          NewCol.Name = thisName
          NewCol.Caption = FieldCaptions(FieldIndex)
          NewCol.Format = "#,##0.00"
          NewCol.DataType = GetType(Double)
          NewCol.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter

          NewCol.AllowDragging = True
          NewCol.AllowEditing = False
          NewCol.AllowMerging = False
          NewCol.AllowResizing = True
          NewCol.AllowSorting = True

        End If
      Next

      FieldNames = New String() {"StandardDeviation_M12", "StandardDeviation_M24", "StandardDeviation_M36", "StandardDeviation_M60", "StandardDeviation_ITD", "StandardDeviation_YTD", "StandardDeviation_CustomPeriod"}
      FieldCaptions = New String() {"12M Std Dev", "24M Std Dev", "36M Std Dev", "60M Std Dev", "ITD Std Dev", "YTD Std Dev", "Custom Std Dev"}

      For FieldIndex As Integer = 0 To (FieldNames.Length - 1)
        thisName = FieldNames(FieldIndex)

        If (Not Grid_Results.Cols.Contains(thisName)) Then
          NewCol = Grid_Results.Cols.Add()
          NewCol.Name = thisName
          NewCol.Caption = FieldCaptions(FieldIndex)
          NewCol.Format = "#,##0.0000"
          NewCol.DataType = GetType(Double)
          NewCol.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter

          NewCol.AllowDragging = True
          NewCol.AllowEditing = False
          NewCol.AllowMerging = False
          NewCol.AllowResizing = True
          NewCol.AllowSorting = True

        End If
      Next

      ' Set default visible fields.

      ClearFieldMenuChecked()

      Menu_Fields_AnnualisedReturn_M12.Checked = True
      Menu_Fields_AnnualisedReturn_M36.Checked = True
      Menu_Fields_AnnualisedReturn_M60.Checked = True
      Menu_Fields_Volatility_M60.Checked = True

      SetGridColsVisible()

    Catch ex As Exception
    End Try

    Try

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()

      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    Finally

      InPaint = False

    End Try

    ' Display initial record.

    Call GetFormData()

  End Sub

  Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False
    MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

    ' Clear existing group Work Items

    Try
      SyncLock ActiveGroups
        If (ActiveGroups.Count > 0) Then
          MainForm.ClearGroupUpdateRequest(ActiveGroups.ToArray(GetType(Integer)), Me)
        End If
      End SyncLock

    Catch ex As Exception
    End Try

    Try

      If (Me._DrilldownParent IsNot Nothing) Then

        ' Disassociate from 'Parent' form.

        RemoveHandler _DrilldownParent.RenaissanceSearchUpdateEvent, AddressOf DrilldownGroupUpdate

        ' Clear Compound Group definition (to save the parent form re-calculating it.)

        If (_DrilldownParent.CompoundGroups IsNot Nothing) Then

          SyncLock _DrilldownParent.CompoundGroups

            If (_DrilldownParent.CompoundGroups.ContainsKey(ParentDynamicGroupID)) Then
              _DrilldownParent.CompoundGroups.Remove(ParentDynamicGroupID)
            End If

          End SyncLock
        End If

      End If

    Catch ex As Exception
    End Try

    ' Add Section Groups to the Reserved List

    For Each SubGroup As Integer In SectionDictionary.Values
      If (Not ReservedGroupIDs.Contains(SubGroup)) Then
        ReservedGroupIDs.Add(SubGroup)
      End If
    Next

    ' Add Compound Group IDs to the Reserved List.

    SyncLock _CompoundGroups

      For Each SubGroup As Integer In _CompoundGroups.Keys
        If (Not ReservedGroupIDs.Contains(SubGroup)) Then
          ReservedGroupIDs.Add(SubGroup)
        End If
      Next

    End SyncLock

    '

    Try

      For Each SubGroup As Integer In ReservedGroupIDs
        Try
          ' Warn Sub Forms of this Forms Closure

          RaiseEvent SearchUpdateEvent(Me, -SubGroup)

          ' Clear Dynamic Group entry

          MainForm.StatFunctions.SetDynamicGroupMembers(SubGroup, Nothing, Nothing, DRILLDOWN_GROUP_PRICE_SERIES_PERIODS)

        Catch ex As Exception
        End Try
      Next

      If (_CompoundGroups IsNot Nothing) AndAlso (_CompoundGroups.Count > 0) Then

        SyncLock _CompoundGroups

          For Each ThisKey As Integer In _CompoundGroups.Keys
            Try

              ' Warn Sub Forms of this Forms Closure

              RaiseEvent SearchUpdateEvent(Me, -ThisKey)

              ' Clear Dynamic Group entry

              MainForm.StatFunctions.SetDynamicGroupMembers(ThisKey, Nothing, Nothing, DRILLDOWN_GROUP_PRICE_SERIES_PERIODS)

            Catch ex As Exception
            End Try
          Next

          _CompoundGroups.Clear()

        End SyncLock

      End If

      SectionDictionary.Clear()
      StringNumerator.Clear()

      ParentDynamicGroupID = 0
      DrillDownFieldIDs = Array.CreateInstance(GetType(Integer), 0)
      DrillDownFieldIsCustom = Array.CreateInstance(GetType(Boolean), 0)
      IsCompoundGroup = False
      MyGroupMembers = Nothing

    Catch ex As Exception
    End Try

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        ' Form Control Changed events

      Catch ex As Exception
      End Try
    End If

  End Sub

#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint

    Try

      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
        RefreshForm = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        SetButtonStatus_Flag = True

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) OrElse (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) OrElse KnowledgeDateChanged Then

        DrilldownMenu = SetGroupAndDrillDownMenu(RootMenu)

      End If


      ' ****************************************************************
      ' Changes to the Main FORM table :-
      ' ****************************************************************

      ' ****************************************************************
      ' Repaint if not currently in Edit Mode
      '
      ' ****************************************************************

    Catch ex As Exception
    Finally

      InPaint = OrgInPaint

    End Try

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm) Then
      GetFormData() ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

  Delegate Sub ParentGroupStatsUpdateEventDelegate(ByVal sender As System.Object, ByVal pGroupID As Integer)

  Private Sub ParentGroupStatsUpdate(ByVal sender As System.Object, ByVal pGroupID As Integer)
    ' ****************************************************************
    ' Event Triggered if the Parent updates the underlying Group for this 
    ' form - Designed specifically for the 'Whole Universe' chart line.
    '
    ' ****************************************************************
    Try

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (Me.InvokeRequired) Then

          Dim d As New ParentGroupStatsUpdateEventDelegate(AddressOf ParentGroupStatsUpdate)

          Me.Invoke(d, New Object() {sender, pGroupID})

        Else

          Dim ThisGroupID As Integer = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(pGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median)

          Set_LineChart(MainForm, DefaultStatsDatePeriod, ThisGroupID, Chart_VAMI, 1, "Universe", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now(), "Left")

          Set_RollingReturnChart(MainForm, DefaultStatsDatePeriod, ThisGroupID, Chart_MonthlyReturns, 1, "Universe", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now(), "Left")

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Delegate Sub DrilldownGroupUpdateDelegate(ByVal sender As System.Object, ByVal pGroupID As Integer)

  Private Sub DrilldownGroupUpdate(ByVal sender As System.Object, ByVal pGroupID As Integer)
    ' ****************************************************************
    ' Routine to Handle Group Update event.
    '
    ' This event can be triggered from a worker thread, thus a delegate is used
    ' to marshal it back to the UI thread - To allow the UI to be modified :-
    ' Form Cursor & Progress Bar especially.
    '
    ' ****************************************************************

    If (Me.InvokeRequired) Then

      Try

        Dim d As New DrilldownGroupUpdateDelegate(AddressOf DrilldownGroupUpdate)

        Me.Invoke(d, New Object() {sender, pGroupID})

      Catch ex As Exception
      End Try

    Else

      Try

        ' Validate this message :
        '
        ' Does it relate to the Custom Group of interest ?
        ' Does it signify the closure of the parent form ?
        '

        If (pGroupID < 0) AndAlso ((-pGroupID) = ParentDynamicGroupID) Then
          ' If -(This Group ID) is given, take this as signal that the parent form is closing 

          Try

            If (_DrilldownParent IsNot Nothing) Then
              RemoveHandler _DrilldownParent.RenaissanceSearchUpdateEvent, AddressOf DrilldownGroupUpdate
            End If

            _DrilldownParent = Nothing
            ParentDynamicGroupID = 0

            Me.Text = "Drilldown (Parent Closed - Will not update)"

          Catch ex As Exception
          End Try

          Exit Sub

        ElseIf (pGroupID <> ParentDynamicGroupID) Then
          ' Only listen to changes in the Group ID of interest to this form

          Exit Sub
        End If

      Catch ex As Exception
      End Try

      Try
        If (ParentDynamicGroupID <= 0) Then
          Exit Sub
        End If
      Catch ex As Exception
      End Try

      ' Clear existing group Work Items

      Try
        SyncLock ActiveGroups
          If (ActiveGroups.Count > 0) Then
            MainForm.ClearGroupUpdateRequest(ActiveGroups.ToArray(GetType(Integer)), Me)
          End If
        End SyncLock

      Catch ex As Exception
      End Try

      ' Clear Existing Worker Thread.

      Try

        If (DrillDownUpdateWorker IsNot Nothing) Then

          If (DrillDownUpdateWorker.IsBusy) Then
            DrillDownUpdateWorker.CancelAsync()
          End If

          Try

            RemoveHandler DrillDownUpdateWorker.DoWork, AddressOf BuildSubGroups
            RemoveHandler DrillDownUpdateWorker.RunWorkerCompleted, AddressOf BuildSubGroupsCompleted
            RemoveHandler DrillDownUpdateWorker.ProgressChanged, AddressOf DrillDownWorkerThreadProgressEvent

          Catch ex As Exception
          End Try
        End If

        DrillDownUpdateWorker = New System.ComponentModel.BackgroundWorker
        DrillDownUpdateWorker.WorkerReportsProgress = True
        DrillDownUpdateWorker.WorkerSupportsCancellation = True

        AddHandler DrillDownUpdateWorker.DoWork, AddressOf BuildSubGroups
        AddHandler DrillDownUpdateWorker.RunWorkerCompleted, AddressOf BuildSubGroupsCompleted
        AddHandler DrillDownUpdateWorker.ProgressChanged, AddressOf DrillDownWorkerThreadProgressEvent

        Me.Cursor = Cursors.WaitCursor
        DrillDown_ProgressBar.Value = DrillDown_ProgressBar.Minimum
        DrillDown_ProgressBar.Visible = True

        DrillDownUpdateWorker.RunWorkerAsync()

      Catch ex As Exception

      End Try

    End If


  End Sub

  Private Sub BuildSubGroups(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
    ' ********************************************************************************
    '
    '
    '
    ' ********************************************************************************

    Dim worker As System.ComponentModel.BackgroundWorker = CType(sender, System.ComponentModel.BackgroundWorker)

    Try

      ' OK, Has the Group Changed ?

      Dim NewGroupMembers() As Integer = Nothing
      Dim HasChanged As Boolean = False
      ' Dim FieldDataComparer As New ArrayComparerClass

      MainForm.StatFunctions.GetDynamicGroupMembers(ParentDynamicGroupID, NewGroupMembers, Nothing, -1, True)

      Array.Sort(NewGroupMembers)

      If (NewGroupMembers Is Nothing) Then
        If (MyGroupMembers IsNot Nothing) Then
          HasChanged = True
        End If
      Else
        If (MyGroupMembers Is Nothing) Then
          HasChanged = True
        ElseIf (NewGroupMembers.Length <> MyGroupMembers.Length) Then
          HasChanged = True
        Else
          For Index As Integer = 0 To (NewGroupMembers.Length - 1)
            If (NewGroupMembers(Index) <> MyGroupMembers(Index)) Then
              HasChanged = True
              Exit For
            End If
          Next
        End If
      End If

      ' Exit if Worker is being cancelled

      If (worker.CancellationPending) Then
        e.Cancel = True
        Exit Sub
      End If

      ' Exit if No Group changes

      If (Not HasChanged) Then
        e.Cancel = True
        Exit Sub
      End If

      ' Post 'Clear Grid' event

      worker.ReportProgress(-1)

      ' MySubGroups
      ' OK - Get Field Data
      Dim FieldDataComparer As New ArrayComparerClass

      Dim SectionData(NewGroupMembers.Length - 1) As Array ' Array od String Arrays
      Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
      Dim FieldIndex As Integer
      Dim MemberIndex As Integer
      Dim FieldsData() As String = Nothing
      Dim FieldFormatStrings(DrillDownFieldIDs.Length) As String
      Dim FieldTypes(DrillDownFieldIDs.Length) As System.Type
      Dim InfoValues() As Object = Nothing

      ' Get Custom Field Format Strings

      For FieldIndex = 0 To (DrillDownFieldIDs.Length - 1)

        FieldFormatStrings(FieldIndex) = ""
        FieldTypes(FieldIndex) = GetType(String)

        If DrillDownFieldIsCustom(FieldIndex) Then

          thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, DrillDownFieldIDs(FieldIndex), "")

          If (thisCustomFieldDefinition IsNot Nothing) Then
            If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
              FieldFormatStrings(FieldIndex) = ""
              FieldTypes(FieldIndex) = GetType(String)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
              FieldFormatStrings(FieldIndex) = "yyyy MM dd" ' DISPLAYMEMBER_DATEFORMAT
              FieldTypes(FieldIndex) = GetType(Date)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
              FieldFormatStrings(FieldIndex) = ""
              FieldTypes(FieldIndex) = GetType(Boolean)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.PercentageType) = RenaissanceDataType.PercentageType Then
              FieldFormatStrings(FieldIndex) = "#,##0.00%"
              FieldTypes(FieldIndex) = GetType(Double)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) = RenaissanceDataType.NumericType Then
              FieldFormatStrings(FieldIndex) = "#,##0.00"
              FieldTypes(FieldIndex) = GetType(Double)
            End If
          End If
        End If
      Next

      MyGroupMembers = NewGroupMembers

      For MemberIndex = 0 To (MyGroupMembers.Length - 1)

        ' Exit if Worker is being cancelled

        If (worker.CancellationPending) Then
          e.Cancel = True
          Exit Sub
        End If

        If (MemberIndex Mod 25) = 0 Then
          worker.ReportProgress(MemberIndex)
        End If

        ' Get Field Data

        FieldsData = Array.CreateInstance(GetType(String), DrillDownFieldIDs.Length)

        For FieldIndex = 0 To (DrillDownFieldIDs.Length - 1)

          If DrillDownFieldIsCustom(FieldIndex) Then

            If (FieldTypes(FieldIndex).Name.StartsWith("Date")) Then
              FieldsData(FieldIndex) = CDate(Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, MyGroupMembers(MemberIndex)), FieldTypes(FieldIndex))).ToString(FieldFormatStrings(FieldIndex))

            Else

              FieldsData(FieldIndex) = Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, MyGroupMembers(MemberIndex)), FieldTypes(FieldIndex)).ToString ' (FieldFormatStrings(FieldIndex))

            End If

          Else
            'If (InfoValues Is Nothing) Then
            '  InfoValues = MainForm.PertracData.GetInformationValues(MyGroupMembers(MemberIndex), False)
            'End If

            'FieldsData(FieldIndex) = Nz(InfoValues(DrillDownFieldIDs(FieldIndex)), "").ToString

            FieldsData(FieldIndex) = Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex) Or CustomFieldDataCacheClass.STATIC_FIELD_FLAG, 0, MyGroupMembers(MemberIndex)), "").ToString ' (FieldFormatStrings(FieldIndex))

          End If

          SectionData(MemberIndex) = FieldsData

        Next FieldIndex

        InfoValues = Nothing

      Next MemberIndex

      ' Now sort by Group Field

      Array.Sort(SectionData, MyGroupMembers, FieldDataComparer)

      ' Build Sub-Groups

      Dim SubGroupCount As Integer = 0
      Dim ThisSubGroupID As Integer = 0
      Dim GroupStartIndex As Integer = 0
      Dim GroupEndIndex As Integer = (-1)
      Dim SectionKey As String = ""
      Dim SectionText As String
      Dim ThisSectionData As Array

      ' Then Clear Active Groups List.

      SyncLock ActiveGroups
        Try
          'Dim ThisGroup As Integer

          'For Each ThisGroup In ActiveGroups

          '  MainForm.StatFunctions.SetDynamicGroupMembers(ThisGroup, Nothing, Nothing)

          'Next

        Catch ex As Exception
        Finally
          ActiveGroups.Clear()
        End Try
      End SyncLock

      For MemberIndex = 1 To (MyGroupMembers.Length)

        ' Exit if Worker is being cancelled

        If (worker.CancellationPending) Then
          e.Cancel = True
          Exit Sub
        End If

        ' Build Group

        If (MemberIndex >= MyGroupMembers.Length) OrElse (FieldDataComparer.Compare(SectionData(MemberIndex), SectionData(MemberIndex - 1)) <> 0) Then
          GroupStartIndex = GroupEndIndex + 1

          ' Save Group

          GroupEndIndex = MemberIndex - 1

          ' Build Section key
          ' StringNumerator

          ThisSectionData = SectionData(GroupStartIndex)

          For FieldIndex = 0 To (ThisSectionData.Length - 1)
            SectionText = CType(ThisSectionData(FieldIndex), String)

            If (Not StringNumerator.ContainsKey(SectionText)) Then
              StringNumerator.Add(SectionText, StringNumerator.Count + 1)
            End If

            If (FieldIndex <= 0) Then
              SectionKey = StringNumerator(SectionText).ToString
            Else
              SectionKey &= "|" & StringNumerator(SectionText).ToString
            End If
          Next

          If (SectionDictionary.ContainsKey(SectionKey)) Then
            ThisSubGroupID = SectionDictionary(SectionKey)
          Else
            If (ReservedGroupIDs.Count > 0) Then
              ThisSubGroupID = CInt(ReservedGroupIDs(0))
              ReservedGroupIDs.Remove(ThisSubGroupID)
            Else
              ThisSubGroupID = MainForm.UniqueGenoaNumber
            End If
            SectionDictionary.Add(SectionKey, ThisSubGroupID)
          End If

          Dim SubGroupIDs((GroupEndIndex - GroupStartIndex)) As Integer

          Array.Copy(MyGroupMembers, GroupStartIndex, SubGroupIDs, 0, (GroupEndIndex - GroupStartIndex) + 1)

          If (Not worker.CancellationPending) Then
            MainForm.StatFunctions.SetDynamicGroupMembers(ThisSubGroupID, SubGroupIDs, Nothing, DRILLDOWN_GROUP_PRICE_SERIES_PERIODS)
          Else
            e.Cancel = True
            Exit Sub
          End If

          If (Not worker.CancellationPending) Then
            SyncLock ActiveGroups
              ActiveGroups.Add(ThisSubGroupID)
            End SyncLock
          Else
            e.Cancel = True
            Exit Sub
          End If

          SubGroupCount += 1

        End If

      Next

      ' Refresh Composite Groups.

      If (_CompoundGroups IsNot Nothing) AndAlso (_CompoundGroups.Count > 0) Then
        Dim GroupArray() As Integer

        For Each ThisKey As Integer In _CompoundGroups.Keys
          Try

            ' Exit if Worker is being cancelled

            If (worker.CancellationPending) Then
              e.Cancel = True
              Exit Sub
            End If

            ' Refresh Group 

            GroupArray = _CompoundGroups(ThisKey)

            If (GroupArray IsNot Nothing) AndAlso (GroupArray.Length > 0) Then
              SetCombinedGroup(ThisKey, GroupArray)

              RaiseEvent SearchUpdateEvent(Me, ThisKey)

              MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(ThisKey, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median), Me, AddressOf GroupStatsUpdateEvent, MainForm.StatFunctions.GetDynamicGroupMemberCount(ThisKey), -1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

            End If

          Catch ex As Exception
          End Try
        Next

      End If

    Catch ex As Exception
    Finally

      ' Exit if Worker is being cancelled

      If (worker.CancellationPending) Then
        e.Cancel = True
      End If

    End Try

  End Sub

  Private Sub GroupStatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer, ByVal IsInterimUpdate As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)
    ' *****************************************************************************************
    ' When the Stats engine updates stats for this 'CompoundGroups' entry, notify dependant
    ' forms so that they may update their 'Universe' chart lines.
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        RaiseEvent StatsUpdateEvent(sender, GroupID)

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub BuildSubGroupsCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    ' *****************************************************************************************
    '
    ' *****************************************************************************************

    Try

      If (Not e.Cancelled) Then

        Me.Cursor = Cursors.Default
        DrillDown_ProgressBar.Visible = False

        ' Paint Form

        If (Me.Visible) Then
          GetFormData()
        End If

        ' 

        If (Grid_Results.Rows.Count > 1) Then
          If (Grid_Results.Rows.Selected.Count <= 0) Then
            Grid_Results.Rows(1).Selected = True
          End If

          GenericUpdateObject.FormToUpdate = True

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub DrillDownWorkerThreadProgressEvent(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)

    Try
      If (e.ProgressPercentage < 0) Then

        Grid_Results.Rows.Count = 1

      Else

        DrillDown_ProgressBar.Value = Math.Max(DrillDown_ProgressBar.Minimum, (DrillDown_ProgressBar.Value + 1) Mod (DrillDown_ProgressBar.Maximum + 1))
        DrillDown_ProgressBar.Refresh()

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Function SetCombinedGroup(ByVal GroupsToCombine() As Integer) As Integer()
    ' ********************************************************************************
    ' Combine one or more dynamic groups into a single group.
    '
    ' ********************************************************************************

    Dim RVal(-1) As Integer

    Try
      If (GroupsToCombine IsNot Nothing) AndAlso (GroupsToCombine.Length > 0) Then

        Dim SubGroupMembers(GroupsToCombine.Length - 1) As Array
        Dim GroupCount As Integer
        Dim GroupMemberCount As Integer = 0
        Dim TempMembersArray() As Integer = Nothing

        SyncLock ActiveGroups

          For GroupCount = 0 To (GroupsToCombine.Length - 1)

            ' Only use groups that are currently Active.

            If (ActiveGroups.Contains(GroupsToCombine(GroupCount))) Then

              MainForm.StatFunctions.GetDynamicGroupMembers(GroupsToCombine(GroupCount), TempMembersArray, Nothing, -1, False)
              SubGroupMembers(GroupCount) = TempMembersArray

              If (TempMembersArray IsNot Nothing) AndAlso (TempMembersArray.Length > 0) Then
                GroupMemberCount += TempMembersArray.Length
              End If

            Else

              SubGroupMembers(GroupCount) = Nothing

            End If

          Next

        End SyncLock

        If (GroupMemberCount > 0) Then
          RVal = Array.CreateInstance(GetType(Integer), GroupMemberCount)

          GroupMemberCount = 0

          For GroupCount = 0 To (GroupsToCombine.Length - 1)
            If (SubGroupMembers(GroupCount) IsNot Nothing) AndAlso (SubGroupMembers(GroupCount).Length > 0) Then
              Array.Copy(SubGroupMembers(GroupCount), 0, RVal, GroupMemberCount, SubGroupMembers(GroupCount).Length)
              GroupMemberCount += SubGroupMembers(GroupCount).Length
            End If
          Next

          Array.Sort(RVal)

        End If
      End If
    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Sub SetCombinedGroup(ByVal pDestinationGroupID As Integer, ByVal GroupsToCombine() As Integer)
    ' ********************************************************************************
    ' Combine one or more dynamic groups into a single (given) group.
    '
    ' ********************************************************************************
    Dim RVal(-1) As Integer

    Try

      MainForm.StatFunctions.SetDynamicGroupMembers(pDestinationGroupID, SetCombinedGroup(GroupsToCombine), Nothing, DRILLDOWN_GROUP_PRICE_SERIES_PERIODS)


    Catch ex As Exception

    End Try

    'Try
    '  If (GroupsToCombine IsNot Nothing) AndAlso (GroupsToCombine.Length > 0) Then

    '    Dim SubGroupMembers(GroupsToCombine.Length - 1) As Array
    '    Dim GroupCount As Integer
    '    Dim GroupMemberCount As Integer = 0
    '    Dim TempMembersArray() As Integer = Nothing

    '    SyncLock ActiveGroups

    '      For GroupCount = 0 To (GroupsToCombine.Length - 1)

    '        ' Only use groups that are currently Active.

    '        If (ActiveGroups.Contains(GroupsToCombine(GroupCount))) Then

    '          MainForm.StatFunctions.GetDynamicGroupMembers(GroupsToCombine(GroupCount), TempMembersArray, Nothing, -1, False)
    '          SubGroupMembers(GroupCount) = TempMembersArray

    '          If (TempMembersArray IsNot Nothing) AndAlso (TempMembersArray.Length > 0) Then
    '            GroupMemberCount += TempMembersArray.Length
    '          End If

    '        Else

    '          SubGroupMembers(GroupCount) = Nothing

    '        End If

    '      Next

    '    End SyncLock

    '    If (GroupMemberCount > 0) Then
    '      RVal = Array.CreateInstance(GetType(Integer), GroupMemberCount)

    '      GroupMemberCount = 0

    '      For GroupCount = 0 To (GroupsToCombine.Length - 1)
    '        If (SubGroupMembers(GroupCount) IsNot Nothing) AndAlso (SubGroupMembers(GroupCount).Length > 0) Then
    '          Array.Copy(SubGroupMembers(GroupCount), 0, RVal, GroupMemberCount, SubGroupMembers(GroupCount).Length)
    '          GroupMemberCount += SubGroupMembers(GroupCount).Length
    '        End If
    '      Next

    '      Array.Sort(RVal)

    '    End If
    '  End If
    'Catch ex As Exception
    'Finally

    '  MainForm.StatFunctions.SetDynamicGroupMembers(pDestinationGroupID, RVal, Nothing)

    'End Try

  End Sub


#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions

  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "



#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub GetFormData()
    ' Routine to populate form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint

    Try

      InPaint = True


      If (FormIsValid = False) Or (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.

        Grid_Results.Rows.Count = 1

      Else

        ' Populate Form with given data.

        Try
          Dim ThisGridRow As C1.Win.C1FlexGrid.Row
          Dim ThisGroupID As Integer
          Dim ThisPertracID As Integer

          Dim FieldIndex As Integer
          Dim FieldTypes(DrillDownFieldIDs.Length) As System.Type
          Dim FieldFormatStrings(DrillDownFieldIDs.Length) As String
          Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow

          'Dim ThisSimpleStats As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass

          Grid_Results.Rows.Count = 1

          ' Group Cols
          ' Assumes Group0 column already exists !

          For FieldIndex = 0 To (DrillDownFieldIDs.Length - 1)
            If Me.Grid_Results.Cols.Contains("Group" & FieldIndex.ToString) Then
              Grid_Results.Cols("Group" & FieldIndex.ToString).Visible = True
            Else
              Grid_Results.Cols.Add().Name = "Group" & FieldIndex.ToString
              Grid_Results.Cols("Group" & FieldIndex.ToString).Visible = True
              Grid_Results.Cols("Group" & FieldIndex.ToString).Move(Grid_Results.Cols("Group" & (FieldIndex - 1).ToString).Index + 1)
            End If
          Next

          FieldIndex = DrillDownFieldIDs.Length

          While Me.Grid_Results.Cols.Contains("Group" & FieldIndex.ToString)
            Grid_Results.Cols("Group" & FieldIndex.ToString).Visible = False

            FieldIndex += 1
          End While

          For FieldIndex = 0 To (DrillDownFieldIDs.Length - 1)

            FieldFormatStrings(FieldIndex) = ""
            FieldTypes(FieldIndex) = GetType(String)

            If DrillDownFieldIsCustom(FieldIndex) Then

              thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, DrillDownFieldIDs(FieldIndex), "")

              Try
                Grid_Results.Cols("Group" & FieldIndex.ToString).Caption = thisCustomFieldDefinition.FieldName
              Catch ex As Exception
              End Try

              If (thisCustomFieldDefinition IsNot Nothing) Then
                If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
                  FieldFormatStrings(FieldIndex) = ""
                  FieldTypes(FieldIndex) = GetType(String)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
                  FieldFormatStrings(FieldIndex) = DISPLAYMEMBER_DATEFORMAT
                  FieldTypes(FieldIndex) = GetType(Date)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
                  FieldFormatStrings(FieldIndex) = ""
                  FieldTypes(FieldIndex) = GetType(Boolean)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.PercentageType) = RenaissanceDataType.PercentageType Then
                  FieldFormatStrings(FieldIndex) = "#,##0.00%"
                  FieldTypes(FieldIndex) = GetType(Double)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) = RenaissanceDataType.NumericType Then
                  FieldFormatStrings(FieldIndex) = "#,##0.00"
                  FieldTypes(FieldIndex) = GetType(Double)
                End If
              End If

            Else
              ' Static Field

              Try
                Grid_Results.Cols("Group" & FieldIndex.ToString).Caption = CType(DrillDownFieldIDs(FieldIndex), RenaissanceGlobals.PertracInformationFields).ToString
              Catch ex As Exception
              End Try

            End If
          Next

          ' Group

          ' ActiveGroups
          Dim GroupIndex As Integer

          For GroupIndex = 0 To (ActiveGroups.Count - 1)

            ThisGroupID = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(ActiveGroups(GroupIndex)), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median))
            ThisGridRow = Me.Grid_Results.Rows.Add()
            ThisPertracID = CInt(MainForm.StatFunctions.GetDynamicGroupMember_First(ThisGroupID))

            ' InstrumentCount

            ThisGridRow("ID") = CUInt(ActiveGroups(GroupIndex))
            ThisGridRow("InstrumentCount") = MainForm.StatFunctions.GetDynamicGroupMemberCount(ThisGroupID)

            Dim FieldNames() As String = {"AnnualisedReturn_M12", "AnnualisedReturn_M24", "AnnualisedReturn_M36", "AnnualisedReturn_M60", "AnnualisedReturn_ITD", "AnnualisedReturn_YTD", "AnnualisedReturn_CustomPeriod", _
                                          "Volatility_M12", "Volatility_M24", "Volatility_M36", "Volatility_M60", "Volatility_ITD", "Volatility_YTD", "Volatility_CustomPeriod", _
                                          "PercentPositive_M12", "PercentPositive_M24", "PercentPositive_M36", "PercentPositive_M60", "PercentPositive_ITD", "PercentPositive_YTD", "PercentPositive_CustomPeriod", _
                                          "SimpleReturns_M12", "SimpleReturns_M24", "SimpleReturns_M36", "SimpleReturns_M60", "SimpleReturns_ITD", "SimpleReturns_YTD", "SimpleReturns_CustomPeriod", _
                                          "SharpeRatio_M12", "SharpeRatio_M24", "SharpeRatio_M36", "SharpeRatio_M60", "SharpeRatio_ITD", "SharpeRatio_YTD", "SharpeRatio_CustomPeriod", _
                                          "StandardDeviation_M12", "StandardDeviation_M24", "StandardDeviation_M36", "StandardDeviation_M60", "StandardDeviation_ITD", "StandardDeviation_YTD", "StandardDeviation_CustomPeriod"}

            For Each thisFieldName As String In FieldNames
              Grid_Results.SetCellStyle(Grid_Results.Rows.Count - 1, Grid_Results.Cols(thisFieldName).SafeIndex, "Positive")
            Next

            For FieldIndex = 0 To (DrillDownFieldIDs.Length - 1)

              If DrillDownFieldIsCustom(FieldIndex) Then

                If (FieldTypes(FieldIndex).Name.StartsWith("Date")) Then

                  ThisGridRow("Group" & FieldIndex.ToString) = CDate(Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, ThisPertracID), FieldTypes(FieldIndex))).ToString(FieldFormatStrings(FieldIndex))

                ElseIf (FieldTypes(FieldIndex).Name.StartsWith("Double")) Then

                  ThisGridRow("Group" & FieldIndex.ToString) = CDbl(Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, ThisPertracID), FieldTypes(FieldIndex))).ToString(FieldFormatStrings(FieldIndex))

                ElseIf (FieldTypes(FieldIndex).Name.StartsWith("Int")) Then

                  ThisGridRow("Group" & FieldIndex.ToString) = CInt(Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, ThisPertracID), FieldTypes(FieldIndex))).ToString(FieldFormatStrings(FieldIndex))

                Else

                  ThisGridRow("Group" & FieldIndex.ToString) = Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex), 0, ThisPertracID), FieldTypes(FieldIndex)).ToString ' (FieldFormatStrings(FieldIndex))

                End If

              Else

                ThisGridRow("Group" & FieldIndex.ToString) = Nz(MainForm.CustomFieldDataCache.GetDataPoint(DrillDownFieldIDs(FieldIndex) Or CustomFieldDataCacheClass.STATIC_FIELD_FLAG, 0, ThisPertracID), "").ToString

              End If

            Next ' Field Index

          Next ' Group Index

          PostGetDataUpdateRequests()

        Catch ex As Exception

          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)

        End Try

      End If

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)

    Finally

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint

    End Try

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  Private Sub PostGetDataUpdateRequests(Optional ByVal OnlyFullSetQueries As Boolean = False)

    Try
      Dim GroupIndex As Integer
      Dim ThisGroupID As Integer
      Dim CustomStartDate As Date = CustomStatsStartDateControl.MonthCalendarControl.SelectionStart
      Dim CustomEndDate As Date = CustomStatsEndDateControl.MonthCalendarControl.SelectionStart

      ' Post Update Requests

      ' For Large Groups, calculate a 'Quick' result, and Then a Full sample Size result.

      Dim GroupSize As Integer

      For GroupIndex = 0 To (ActiveGroups.Count - 1)
        ThisGroupID = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(ActiveGroups(GroupIndex)), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median))
        GroupSize = MainForm.StatFunctions.GetDynamicGroupMemberCount(ThisGroupID)

        If (Not OnlyFullSetQueries) AndAlso (GroupSize > DrillDownUpdateProcessClass.BIG_GROUP_RESCHEDULE_THRESHOLD) Then
          MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, ThisGroupID, Me, AddressOf DynamicGroupStatsUpdateEvent, GroupSize, CInt(DrillDownUpdateProcessClass.BIG_GROUP_RESCHEDULE_THRESHOLD / 2), True, CustomStartDate, CustomEndDate)
        Else
          MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, ThisGroupID, Me, AddressOf DynamicGroupStatsUpdateEvent, GroupSize, -1, False, CustomStartDate, CustomEndDate)
        End If
      Next

      If (Not OnlyFullSetQueries) Then
        For GroupIndex = 0 To (ActiveGroups.Count - 1)
          ThisGroupID = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(ActiveGroups(GroupIndex)), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median))
          GroupSize = MainForm.StatFunctions.GetDynamicGroupMemberCount(ThisGroupID)

          If (GroupSize > DrillDownUpdateProcessClass.BIG_GROUP_RESCHEDULE_THRESHOLD) Then
            MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, ThisGroupID, Me, AddressOf DynamicGroupStatsUpdateEvent, GroupSize, -1, False, CustomStartDate, CustomEndDate)
          End If
        Next
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

  End Sub

  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 


    Return RVal

  End Function

  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "



#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *************************************************************
    ' Close Form
    ' *************************************************************

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

  Private Sub Grid_Results_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Results.DoubleClick
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Grid_Results.MouseRow > 0) Then

        Try
          Dim newBrowserForm As frmFundBrowser

          If (Grid_Results.Rows.Selected.Count > 0) Then

            newBrowserForm = CType(MainForm.New_GenoaForm(GenoaFormID.frmFundBrowser, False).Form, frmFundBrowser)

            Application.DoEvents()

            newBrowserForm.StartWithCustomSelection = True

            newBrowserForm.Show()

            Dim GroupArray() As Integer = Nothing
            Dim PertracIDs() As Integer = Nothing

            GroupArray = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

            For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)
              GroupArray(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")
            Next

            PertracIDs = SetCombinedGroup(GroupArray)

            newBrowserForm.SetCustomListInstrumentIDs(PertracIDs)

          End If

        Catch ex As Exception
        End Try

      End If

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub Grid_Results_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Results.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Results_BeforeMouseDown(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.BeforeMouseDownEventArgs) Handles Grid_Results.BeforeMouseDown
    ' *****************************************************************************************
    ' Handle the initiation of Drag-Grop functionality from the Fund Search Form.
    '
    ' Essentially, when Drag-Drop is initiated, the 'DoDragDrop' function is fed a comma serarated
    ' string of Pertrac IDs with a standard header string.
    ' *****************************************************************************************

    Dim ThisMouseRow As Integer = Grid_Results.MouseRow
    Dim RowSelected As Boolean = False

    If (ThisMouseRow > 0) AndAlso (Control.ModifierKeys = Keys.None) Then
      If (e.Button = Windows.Forms.MouseButtons.Left) AndAlso (e.Clicks = 1) Then
        If (Grid_Results.Rows.Selected.Count > 0) Then
          Dim SelectString As New System.Text.StringBuilder(GENOA_DRAG_IDs_HEADER)

          If (Grid_Results.Rows.Selected.Contains(Grid_Results.Rows(ThisMouseRow))) Then
            Dim GroupArray() As Integer = Nothing
            Dim PertracIDs() As Integer = Nothing

            GroupArray = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

            For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)
              GroupArray(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")
            Next

            PertracIDs = SetCombinedGroup(GroupArray)

            For InstrumentCounter As Integer = 0 To (PertracIDs.Length - 1)
              SelectString.Append("," & PertracIDs(InstrumentCounter).ToString)
            Next

            Grid_Results.DoDragDrop(SelectString.ToString, DragDropEffects.Copy)
            e.Cancel = True

            Exit Sub
          End If
        End If
      End If
    End If
  End Sub

  Private Sub Grid_Results_SelChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Results.SelChange
    ' *****************************************************************************************
    ' Modify to use Genoa Form update code to invoke delayed update to Charts.
    '
    ' It was found that if this event was used directly, then the interartion between the Background
    ' Group Update task and this event could mean that Custom Group Stats Cache items were being 
    ' cleared (by a subsequent SetGroup() call) before the PaintCharts() function had a chance
    ' to run : thus we ended up calculating group price series on the User Interface thread.
    ' I tried using SyncLocks to cure this, but it was not completely successful.
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (Not Split_DrillDown_GridAndChart.Panel2Collapsed) Then
          GenericUpdateObject.FormToUpdate = True
        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Menu_ShowCharts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ShowCharts.Click
    ' *****************************************************************************************
    ' Toggle Chart visibility
    '
    ' *****************************************************************************************

    Try

      Menu_ShowCharts.Checked = Not Menu_ShowCharts.Checked

      Split_DrillDown_GridAndChart.Panel2Collapsed = Not Menu_ShowCharts.Checked

      If (Not Split_DrillDown_GridAndChart.Panel2Collapsed) Then
        GenericUpdateObject.FormToUpdate = True
        Split_DrillDown_GridAndChart_Resize(Split_DrillDown_GridAndChart, New EventArgs)
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Button_HideCharts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_HideCharts.Click
    ' *****************************************************************************************
    ' Toggle chart visibility and set button text accordingly.
    '
    ' *****************************************************************************************

    Try

      Menu_ShowCharts_Click(Nothing, New System.EventArgs)

      If (Menu_ShowCharts.Checked) Then
        Button_HideCharts.Text = "Hide Charts"
      Else
        Button_HideCharts.Text = "Show Charts"
      End If

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  Private Sub DynamicGroupStatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer, ByVal IsInterimUpdate As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (Me.InvokeRequired) Then

          Dim d As New GenoaGlobals.GroupStatsUpdateDelegate(AddressOf DynamicGroupStatsUpdateEvent)

          Me.Invoke(d, New Object() {sender, GroupID, IsInterimUpdate, UpdateResults})

        Else

          Dim RowIndex As Integer
          Dim ThisGridRow As C1.Win.C1FlexGrid.Row
          Dim StrippedGroupID As Integer

          StrippedGroupID = CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(GroupID)))

          If (GroupID > 0) AndAlso (UpdateResults IsNot Nothing) Then

            For RowIndex = 0 To (Me.Grid_Results.Rows.Count - 1)

              Try

                ThisGridRow = Me.Grid_Results.Rows(RowIndex)

                If (Not ThisGridRow.IsNew) AndAlso (IsNumeric(ThisGridRow("ID"))) AndAlso (CInt(ThisGridRow("ID")) = StrippedGroupID) Then

                  Try

                    ThisGridRow("AnnualisedReturn_M12") = UpdateResults.AnnualisedReturn.M12
                    ThisGridRow("AnnualisedReturn_M24") = UpdateResults.AnnualisedReturn.M24
                    ThisGridRow("AnnualisedReturn_M36") = UpdateResults.AnnualisedReturn.M36
                    ThisGridRow("AnnualisedReturn_M60") = UpdateResults.AnnualisedReturn.M60
                    ThisGridRow("AnnualisedReturn_ITD") = UpdateResults.AnnualisedReturn.ITD
                    ThisGridRow("AnnualisedReturn_YTD") = UpdateResults.AnnualisedReturn.YTD
                    ThisGridRow("AnnualisedReturn_CustomPeriod") = UpdateResults.AnnualisedReturn.CTD

                    ThisGridRow("Volatility_M12") = UpdateResults.Volatility.M12
                    ThisGridRow("Volatility_M24") = UpdateResults.Volatility.M24
                    ThisGridRow("Volatility_M36") = UpdateResults.Volatility.M36
                    ThisGridRow("Volatility_M60") = UpdateResults.Volatility.M60
                    ThisGridRow("Volatility_ITD") = UpdateResults.Volatility.ITD
                    ThisGridRow("Volatility_YTD") = UpdateResults.Volatility.YTD
                    ThisGridRow("Volatility_CustomPeriod") = UpdateResults.Volatility.CTD

                    ThisGridRow("StandardDeviation_M12") = UpdateResults.StandardDeviation.M12
                    ThisGridRow("StandardDeviation_M24") = UpdateResults.StandardDeviation.M24
                    ThisGridRow("StandardDeviation_M36") = UpdateResults.StandardDeviation.M36
                    ThisGridRow("StandardDeviation_M60") = UpdateResults.StandardDeviation.M60
                    ThisGridRow("StandardDeviation_ITD") = UpdateResults.StandardDeviation.ITD
                    ThisGridRow("StandardDeviation_YTD") = UpdateResults.StandardDeviation.YTD
                    ThisGridRow("StandardDeviation_CustomPeriod") = UpdateResults.StandardDeviation.CTD

                    ThisGridRow("PercentPositive_M12") = UpdateResults.PercentPositive.M12
                    ThisGridRow("PercentPositive_M24") = UpdateResults.PercentPositive.M24
                    ThisGridRow("PercentPositive_M36") = UpdateResults.PercentPositive.M36
                    ThisGridRow("PercentPositive_M60") = UpdateResults.PercentPositive.M60
                    ThisGridRow("PercentPositive_ITD") = UpdateResults.PercentPositive.ITD
                    ThisGridRow("PercentPositive_YTD") = UpdateResults.PercentPositive.YTD
                    ThisGridRow("PercentPositive_CustomPeriod") = UpdateResults.PercentPositive.CTD

                    ThisGridRow("SharpeRatio_M12") = UpdateResults.SharpeRatio.M12
                    ThisGridRow("SharpeRatio_M24") = UpdateResults.SharpeRatio.M24
                    ThisGridRow("SharpeRatio_M36") = UpdateResults.SharpeRatio.M36
                    ThisGridRow("SharpeRatio_M60") = UpdateResults.SharpeRatio.M60
                    ThisGridRow("SharpeRatio_ITD") = UpdateResults.SharpeRatio.ITD
                    ThisGridRow("SharpeRatio_YTD") = UpdateResults.SharpeRatio.YTD
                    ThisGridRow("SharpeRatio_CustomPeriod") = UpdateResults.SharpeRatio.CTD

                    ThisGridRow("SimpleReturns_M12") = UpdateResults.SimpleReturn.M12
                    ThisGridRow("SimpleReturns_M24") = UpdateResults.SimpleReturn.M24
                    ThisGridRow("SimpleReturns_M36") = UpdateResults.SimpleReturn.M36
                    ThisGridRow("SimpleReturns_M60") = UpdateResults.SimpleReturn.M60
                    ThisGridRow("SimpleReturns_ITD") = UpdateResults.SimpleReturn.ITD
                    ThisGridRow("SimpleReturns_YTD") = UpdateResults.SimpleReturn.YTD
                    ThisGridRow("SimpleReturns_CustomPeriod") = UpdateResults.SimpleReturn.CTD

                    Dim FieldNames() As String = {"AnnualisedReturn_M12", "AnnualisedReturn_M24", "AnnualisedReturn_M36", "AnnualisedReturn_M60", "AnnualisedReturn_ITD", "AnnualisedReturn_YTD", "AnnualisedReturn_CustomPeriod", _
                              "Volatility_M12", "Volatility_M24", "Volatility_M36", "Volatility_M60", "Volatility_ITD", "Volatility_YTD", "Volatility_CustomPeriod", _
                              "PercentPositive_M12", "PercentPositive_M24", "PercentPositive_M36", "PercentPositive_M60", "PercentPositive_ITD", "PercentPositive_YTD", "PercentPositive_CustomPeriod", _
                              "SimpleReturns_M12", "SimpleReturns_M24", "SimpleReturns_M36", "SimpleReturns_M60", "SimpleReturns_ITD", "SimpleReturns_YTD", "SimpleReturns_CustomPeriod", _
                              "SharpeRatio_M12", "SharpeRatio_M24", "SharpeRatio_M36", "SharpeRatio_M60", "SharpeRatio_ITD", "SharpeRatio_YTD", "SharpeRatio_CustomPeriod", _
                              "StandardDeviation_M12", "StandardDeviation_M24", "StandardDeviation_M36", "StandardDeviation_M60", "StandardDeviation_ITD", "StandardDeviation_YTD", "StandardDeviation_CustomPeriod"}

                    For Each thisFieldName As String In FieldNames
                      SetCellFormat(RowIndex, Grid_Results.Cols(thisFieldName).SafeIndex, IsInterimUpdate, CDbl(ThisGridRow(thisFieldName)))
                    Next

                  Catch ex As Exception
                  End Try
      
                  Exit For

                End If

              Catch ex As Exception
              End Try

            Next

          End If

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetCellFormat(ByVal pRow As Integer, ByVal pCol As Integer, ByVal pIsInterim As Boolean, ByVal pValue As Double)

    Try

      If (pIsInterim) Then
        If (pValue >= 0) Then
          Grid_Results.SetCellStyle(pRow, pCol, "PositiveInterim")
        Else
          Grid_Results.SetCellStyle(pRow, pCol, "NegativeInterim")
        End If
      Else
        If (pValue >= 0) Then
          Grid_Results.SetCellStyle(pRow, pCol, "Positive")
        Else
          Grid_Results.SetCellStyle(pRow, pCol, "Negative")
        End If
      End If

    Catch ex As Exception

    End Try


  End Sub

  Private Sub DynamicGroupChartUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer, ByVal IsInterimUpdate As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)
    ' *****************************************************************************************
    ' Repaint charts after the Chart-Related Group calculation is complete.
    '
    ' *****************************************************************************************

    Dim ThisGroupID As Integer

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (Me.InvokeRequired) Then

          Dim d As New GenoaGlobals.GroupStatsUpdateDelegate(AddressOf DynamicGroupChartUpdateEvent)

          Me.Invoke(d, New Object() {sender, GroupID, IsInterimUpdate, UpdateResults})

        Else

          ThisGroupID = GroupID

          ' Label_ChartStock.Text = MainForm.PertracData.GetInformationValue(ThisPertracID, PertracInformationFields.FundName)

          Set_LineChart(MainForm, DefaultStatsDatePeriod, ThisGroupID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())

          Set_RollingReturnChart(MainForm, DefaultStatsDatePeriod, ThisGroupID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())

        End If

      End If
    Catch ex As Exception

    End Try

  End Sub

  Private Sub Split_DrillDown_GridAndChart_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Split_DrillDown_GridAndChart.Resize
    ' *****************************************************************************************
    ' Set Chart dimensions and locations after a resize event.
    '
    ' *****************************************************************************************

    Try
      Me.Chart_VAMI.Left = 1
      Me.Chart_VAMI.Top = Label_ChartStock.Top + Label_ChartStock.Height
      Me.Chart_VAMI.Height = Math.Min((Split_DrillDown_GridAndChart.Panel2.Height - Label_ChartStock.Height) / 2, Me.Chart_VAMI.Width * 1.5)
      Me.Chart_VAMI.Width = Split_DrillDown_GridAndChart.Panel2.Width - 2

      Me.Chart_MonthlyReturns.Left = 1
      Me.Chart_MonthlyReturns.Top = Me.Chart_VAMI.Top + Me.Chart_VAMI.Height
      Me.Chart_MonthlyReturns.Height = Me.Chart_VAMI.Height
      Me.Chart_MonthlyReturns.Width = Split_DrillDown_GridAndChart.Panel2.Width - 2

    Catch ex As Exception

    End Try

  End Sub

  Private Function TimedChartUpdate_Tick() As Boolean
    ' *****************************************************************************************
    '
    ' Initiate the process to update Group charts.
    '
    ' *****************************************************************************************

    Dim GroupArray() As Integer = Nothing

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InTimedChartUpdate_Tick) OrElse (Not _InUse) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try


    Try
      InTimedChartUpdate_Tick = True

      If (Chart_VAMI.Series.Count > 0) Then
        Chart_VAMI.Series(0).ChartArea = ""

        'If (Chart_VAMI.Series.Count > 1) Then
        '  Chart_VAMI.Series(1).ChartArea = ""
        'End If
      End If

      If (Chart_MonthlyReturns.Series.Count > 0) Then
        Chart_MonthlyReturns.Series(0).ChartArea = ""

        'If (Chart_MonthlyReturns.Series.Count > 1) Then
        '  Chart_MonthlyReturns.Series(1).ChartArea = ""
        'End If
      End If

      ' First, Which lines / Groups are selected.

      If Grid_Results.Rows.Selected.Count > 0 Then

        ' Create Dynamic Group

        GroupArray = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

        For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)

          GroupArray(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")

        Next

        Array.Sort(GroupArray)

        ' Set Group and initiate time series calculation.

        If (GroupArray IsNot Nothing) AndAlso (GroupArray.Length > 0) Then
          Dim ThisGroupID As Integer

          ThisGroupID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(DrillDown_GraphGroupID), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median)

          MainForm.ClearGroupChartUpdateRequest(New Integer() {ThisGroupID}, Me)

          SetCombinedGroup(DrillDown_GraphGroupID, GroupArray)

          MainForm.PostNewChartUpdateRequest(DefaultStatsDatePeriod, ThisGroupID, Me, AddressOf DynamicGroupChartUpdateEvent, MainForm.StatFunctions.GetDynamicGroupMemberCount(ThisGroupID), -1, False)

        End If

      End If

    Catch ex As Exception
    Finally
      InTimedChartUpdate_Tick = False
    End Try

    Return True

  End Function

  Private Sub Split_DrillDown_GridAndChart_SplitterMoved(ByVal sender As System.Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles Split_DrillDown_GridAndChart.SplitterMoved
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Split_DrillDown_GridAndChart_Resize(sender, New EventArgs)

    Catch ex As Exception

    End Try
  End Sub




End Class


