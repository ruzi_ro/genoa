Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceGenoaGroupsGlobals
Imports RenaissanceControls
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions

Imports Genoa.MaxFunctions

Public Class frmFundSearch

	Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm, IRenaissanceSearchUpdateEvent


#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Friend WithEvents Panel_SearchInstruments As System.Windows.Forms.Panel
	Friend WithEvents Grid_Results As C1.Win.C1FlexGrid.C1FlexGrid
	Friend WithEvents StatusStrip_Search As System.Windows.Forms.StatusStrip
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
	Friend WithEvents StatusLabel_Search As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents Combo_AndOr2 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_SelectValue2 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_Condition2 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_SelectField2 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_AndOr1 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_SelectValue1 As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_Condition1 As System.Windows.Forms.ComboBox
  Friend WithEvents Label_InformationSelectWhere As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectField1 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue5 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition5 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField5 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr4 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue4 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition4 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField4 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr3 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue3 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition3 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField3 As System.Windows.Forms.ComboBox
  Friend WithEvents Radio_Custom5 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static5 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Custom4 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static4 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Custom3 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static3 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Custom2 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static2 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SearchFieldType As System.Windows.Forms.Label
  Friend WithEvents Radio_Custom1 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static1 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SelectCount5 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount4 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount3 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount2 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount1 As System.Windows.Forms.Label
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Split_Results As System.Windows.Forms.SplitContainer
  Friend WithEvents Label_ChartStock As System.Windows.Forms.Label
  Friend WithEvents Chart_MonthlyReturns As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_VAMI As Dundas.Charting.WinControl.Chart
  Friend WithEvents Btn_ShowCarts As System.Windows.Forms.Button
  Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Options_SetValuecombos As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Options_CustomFieldChanges As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents SplitButton_Save As System.Windows.Forms.ToolStripSplitButton
  Friend WithEvents SplitButton_Cancel As System.Windows.Forms.ToolStripSplitButton
  Friend WithEvents Menu_Options_ShowAllResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Combo_SelectValue5b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue4b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue3b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue2b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue1b As System.Windows.Forms.ComboBox
  Friend WithEvents Split_FundSearch As System.Windows.Forms.SplitContainer
  Friend WithEvents Combo_SelectValue21b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue20b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue19b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue18b As System.Windows.Forms.ComboBox
  Friend WithEvents Panel21 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom21 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static21 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel20 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom20 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static20 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel19 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom19 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static19 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel18 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom18 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static18 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SelectCount21 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount20 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount19 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount18 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectValue21 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition21 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField21 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr20 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue20 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition20 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField20 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr19 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue19 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition19 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField19 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr18 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue18 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition18 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField18 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr17 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue17b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue16b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue15b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue14b As System.Windows.Forms.ComboBox
  Friend WithEvents Panel17 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom17 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static17 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel16 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom16 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static16 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel15 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom15 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static15 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel14 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom14 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static14 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SelectCount17 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount16 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount15 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount14 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectValue17 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition17 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField17 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr16 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue16 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition16 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField16 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr15 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue15 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition15 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField15 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr14 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue14 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition14 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField14 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr13 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue13b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue12b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue11b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue10b As System.Windows.Forms.ComboBox
  Friend WithEvents Panel13 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom13 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static13 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel12 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom12 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static12 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel11 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom11 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static11 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel10 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom10 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static10 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SelectCount13 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount12 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount11 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount10 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectValue13 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition13 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField13 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr12 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue12 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition12 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField12 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr11 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue11 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition11 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField11 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr10 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue10 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition10 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField10 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr9 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue9b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue8b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue7b As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue6b As System.Windows.Forms.ComboBox
  Friend WithEvents Panel9 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom9 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static9 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel8 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom8 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static8 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel7 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom7 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static7 As System.Windows.Forms.RadioButton
  Friend WithEvents Panel6 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Custom6 As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Static6 As System.Windows.Forms.RadioButton
  Friend WithEvents Label_SelectCount9 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount8 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount7 As System.Windows.Forms.Label
  Friend WithEvents Label_SelectCount6 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectValue9 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition9 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField9 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr8 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue8 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition8 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField8 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr7 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue7 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition7 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField7 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr6 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectValue6 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_Condition6 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_SelectField6 As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_AndOr5 As System.Windows.Forms.ComboBox
  Friend WithEvents Menu_SavedSearches As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ComboBox_SavedSearches As System.Windows.Forms.ToolStripComboBox
  Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_SelectAll As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_SaveToGroup As System.Windows.Forms.ToolStripMenuItem

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundSearch))
    Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Title1 As Dundas.Charting.WinControl.Title = New Dundas.Charting.WinControl.Title
    Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Title2 As Dundas.Charting.WinControl.Title = New Dundas.Charting.WinControl.Title
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_SelectAll = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_SaveToGroup = New System.Windows.Forms.ToolStripMenuItem
    Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Options_SetValuecombos = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Options_CustomFieldChanges = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Options_ShowAllResults = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_SavedSearches = New System.Windows.Forms.ToolStripMenuItem
    Me.ComboBox_SavedSearches = New System.Windows.Forms.ToolStripComboBox
    Me.Panel_SearchInstruments = New System.Windows.Forms.Panel
    Me.Combo_SelectValue21b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue20b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue19b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue18b = New System.Windows.Forms.ComboBox
    Me.Panel21 = New System.Windows.Forms.Panel
    Me.Radio_Custom21 = New System.Windows.Forms.RadioButton
    Me.Radio_Static21 = New System.Windows.Forms.RadioButton
    Me.Panel20 = New System.Windows.Forms.Panel
    Me.Radio_Custom20 = New System.Windows.Forms.RadioButton
    Me.Radio_Static20 = New System.Windows.Forms.RadioButton
    Me.Panel19 = New System.Windows.Forms.Panel
    Me.Radio_Custom19 = New System.Windows.Forms.RadioButton
    Me.Radio_Static19 = New System.Windows.Forms.RadioButton
    Me.Panel18 = New System.Windows.Forms.Panel
    Me.Radio_Custom18 = New System.Windows.Forms.RadioButton
    Me.Radio_Static18 = New System.Windows.Forms.RadioButton
    Me.Label_SelectCount21 = New System.Windows.Forms.Label
    Me.Label_SelectCount20 = New System.Windows.Forms.Label
    Me.Label_SelectCount19 = New System.Windows.Forms.Label
    Me.Label_SelectCount18 = New System.Windows.Forms.Label
    Me.Combo_SelectValue21 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition21 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField21 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr20 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue20 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition20 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField20 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr19 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue19 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition19 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField19 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr18 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue18 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition18 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField18 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr17 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue17b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue16b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue15b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue14b = New System.Windows.Forms.ComboBox
    Me.Panel17 = New System.Windows.Forms.Panel
    Me.Radio_Custom17 = New System.Windows.Forms.RadioButton
    Me.Radio_Static17 = New System.Windows.Forms.RadioButton
    Me.Panel16 = New System.Windows.Forms.Panel
    Me.Radio_Custom16 = New System.Windows.Forms.RadioButton
    Me.Radio_Static16 = New System.Windows.Forms.RadioButton
    Me.Panel15 = New System.Windows.Forms.Panel
    Me.Radio_Custom15 = New System.Windows.Forms.RadioButton
    Me.Radio_Static15 = New System.Windows.Forms.RadioButton
    Me.Panel14 = New System.Windows.Forms.Panel
    Me.Radio_Custom14 = New System.Windows.Forms.RadioButton
    Me.Radio_Static14 = New System.Windows.Forms.RadioButton
    Me.Label_SelectCount17 = New System.Windows.Forms.Label
    Me.Label_SelectCount16 = New System.Windows.Forms.Label
    Me.Label_SelectCount15 = New System.Windows.Forms.Label
    Me.Label_SelectCount14 = New System.Windows.Forms.Label
    Me.Combo_SelectValue17 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition17 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField17 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr16 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue16 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition16 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField16 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr15 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue15 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition15 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField15 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr14 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue14 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition14 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField14 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr13 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue13b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue12b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue11b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue10b = New System.Windows.Forms.ComboBox
    Me.Panel13 = New System.Windows.Forms.Panel
    Me.Radio_Custom13 = New System.Windows.Forms.RadioButton
    Me.Radio_Static13 = New System.Windows.Forms.RadioButton
    Me.Panel12 = New System.Windows.Forms.Panel
    Me.Radio_Custom12 = New System.Windows.Forms.RadioButton
    Me.Radio_Static12 = New System.Windows.Forms.RadioButton
    Me.Panel11 = New System.Windows.Forms.Panel
    Me.Radio_Custom11 = New System.Windows.Forms.RadioButton
    Me.Radio_Static11 = New System.Windows.Forms.RadioButton
    Me.Panel10 = New System.Windows.Forms.Panel
    Me.Radio_Custom10 = New System.Windows.Forms.RadioButton
    Me.Radio_Static10 = New System.Windows.Forms.RadioButton
    Me.Label_SelectCount13 = New System.Windows.Forms.Label
    Me.Label_SelectCount12 = New System.Windows.Forms.Label
    Me.Label_SelectCount11 = New System.Windows.Forms.Label
    Me.Label_SelectCount10 = New System.Windows.Forms.Label
    Me.Combo_SelectValue13 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition13 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField13 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr12 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue12 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition12 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField12 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr11 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue11 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition11 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField11 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr10 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue10 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition10 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField10 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr9 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue9b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue8b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue7b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue6b = New System.Windows.Forms.ComboBox
    Me.Panel9 = New System.Windows.Forms.Panel
    Me.Radio_Custom9 = New System.Windows.Forms.RadioButton
    Me.Radio_Static9 = New System.Windows.Forms.RadioButton
    Me.Panel8 = New System.Windows.Forms.Panel
    Me.Radio_Custom8 = New System.Windows.Forms.RadioButton
    Me.Radio_Static8 = New System.Windows.Forms.RadioButton
    Me.Panel7 = New System.Windows.Forms.Panel
    Me.Radio_Custom7 = New System.Windows.Forms.RadioButton
    Me.Radio_Static7 = New System.Windows.Forms.RadioButton
    Me.Panel6 = New System.Windows.Forms.Panel
    Me.Radio_Custom6 = New System.Windows.Forms.RadioButton
    Me.Radio_Static6 = New System.Windows.Forms.RadioButton
    Me.Label_SelectCount9 = New System.Windows.Forms.Label
    Me.Label_SelectCount8 = New System.Windows.Forms.Label
    Me.Label_SelectCount7 = New System.Windows.Forms.Label
    Me.Label_SelectCount6 = New System.Windows.Forms.Label
    Me.Combo_SelectValue9 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition9 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField9 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr8 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue8 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition8 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField8 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr7 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue7 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition7 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField7 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr6 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue6 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition6 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField6 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr5 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue5b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue4b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue3b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue2b = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue1b = New System.Windows.Forms.ComboBox
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Radio_Custom5 = New System.Windows.Forms.RadioButton
    Me.Radio_Static5 = New System.Windows.Forms.RadioButton
    Me.Panel4 = New System.Windows.Forms.Panel
    Me.Radio_Custom4 = New System.Windows.Forms.RadioButton
    Me.Radio_Static4 = New System.Windows.Forms.RadioButton
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.Radio_Custom3 = New System.Windows.Forms.RadioButton
    Me.Radio_Static3 = New System.Windows.Forms.RadioButton
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Radio_Custom2 = New System.Windows.Forms.RadioButton
    Me.Radio_Static2 = New System.Windows.Forms.RadioButton
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Radio_Custom1 = New System.Windows.Forms.RadioButton
    Me.Radio_Static1 = New System.Windows.Forms.RadioButton
    Me.Label_SelectCount5 = New System.Windows.Forms.Label
    Me.Label_SelectCount4 = New System.Windows.Forms.Label
    Me.Label_SelectCount3 = New System.Windows.Forms.Label
    Me.Label_SelectCount2 = New System.Windows.Forms.Label
    Me.Label_SelectCount1 = New System.Windows.Forms.Label
    Me.Combo_SelectValue5 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition5 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField5 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr4 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue4 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition4 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField4 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr3 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue3 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition3 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField3 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr2 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue2 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition2 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField2 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr1 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectValue1 = New System.Windows.Forms.ComboBox
    Me.Combo_Condition1 = New System.Windows.Forms.ComboBox
    Me.Combo_SelectField1 = New System.Windows.Forms.ComboBox
    Me.Btn_ShowCarts = New System.Windows.Forms.Button
    Me.Label_SearchFieldType = New System.Windows.Forms.Label
    Me.Label_InformationSelectWhere = New System.Windows.Forms.Label
    Me.Grid_Results = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.StatusStrip_Search = New System.Windows.Forms.StatusStrip
    Me.SplitButton_Save = New System.Windows.Forms.ToolStripSplitButton
    Me.SplitButton_Cancel = New System.Windows.Forms.ToolStripSplitButton
    Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
    Me.StatusLabel_Search = New System.Windows.Forms.ToolStripStatusLabel
    Me.Split_Results = New System.Windows.Forms.SplitContainer
    Me.Label_ChartStock = New System.Windows.Forms.Label
    Me.Chart_MonthlyReturns = New Dundas.Charting.WinControl.Chart
    Me.Chart_VAMI = New Dundas.Charting.WinControl.Chart
    Me.Split_FundSearch = New System.Windows.Forms.SplitContainer
    Me.RootMenu.SuspendLayout()
    Me.Panel_SearchInstruments.SuspendLayout()
    Me.Panel21.SuspendLayout()
    Me.Panel20.SuspendLayout()
    Me.Panel19.SuspendLayout()
    Me.Panel18.SuspendLayout()
    Me.Panel17.SuspendLayout()
    Me.Panel16.SuspendLayout()
    Me.Panel15.SuspendLayout()
    Me.Panel14.SuspendLayout()
    Me.Panel13.SuspendLayout()
    Me.Panel12.SuspendLayout()
    Me.Panel11.SuspendLayout()
    Me.Panel10.SuspendLayout()
    Me.Panel9.SuspendLayout()
    Me.Panel8.SuspendLayout()
    Me.Panel7.SuspendLayout()
    Me.Panel6.SuspendLayout()
    Me.Panel5.SuspendLayout()
    Me.Panel4.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.Panel1.SuspendLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.StatusStrip_Search.SuspendLayout()
    Me.Split_Results.Panel1.SuspendLayout()
    Me.Split_Results.Panel2.SuspendLayout()
    Me.Split_Results.SuspendLayout()
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Split_FundSearch.Panel1.SuspendLayout()
    Me.Split_FundSearch.Panel2.SuspendLayout()
    Me.Split_FundSearch.SuspendLayout()
    Me.SuspendLayout()
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.Menu_SavedSearches, Me.ComboBox_SavedSearches})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1070, 24)
    Me.RootMenu.TabIndex = 2
    Me.RootMenu.Text = "MenuStrip1"
    '
    'EditToolStripMenuItem
    '
    Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_SelectAll, Me.ToolStripSeparator3, Me.Menu_SaveToGroup})
    Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
    Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
    Me.EditToolStripMenuItem.Text = "&Edit"
    '
    'Menu_SelectAll
    '
    Me.Menu_SelectAll.Name = "Menu_SelectAll"
    Me.Menu_SelectAll.Size = New System.Drawing.Size(151, 22)
    Me.Menu_SelectAll.Text = "Select &All"
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(148, 6)
    '
    'Menu_SaveToGroup
    '
    Me.Menu_SaveToGroup.Name = "Menu_SaveToGroup"
    Me.Menu_SaveToGroup.Size = New System.Drawing.Size(151, 22)
    Me.Menu_SaveToGroup.Text = "Save To &Group"
    '
    'OptionsToolStripMenuItem
    '
    Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Options_SetValuecombos, Me.ToolStripSeparator1, Me.Menu_Options_CustomFieldChanges, Me.ToolStripSeparator2, Me.Menu_Options_ShowAllResults})
    Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
    Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
    Me.OptionsToolStripMenuItem.Text = "&Options"
    '
    'Menu_Options_SetValuecombos
    '
    Me.Menu_Options_SetValuecombos.Checked = True
    Me.Menu_Options_SetValuecombos.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_Options_SetValuecombos.Name = "Menu_Options_SetValuecombos"
    Me.Menu_Options_SetValuecombos.Size = New System.Drawing.Size(316, 22)
    Me.Menu_Options_SetValuecombos.Text = "Fill Value Combos with existing data"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(313, 6)
    '
    'Menu_Options_CustomFieldChanges
    '
    Me.Menu_Options_CustomFieldChanges.Checked = True
    Me.Menu_Options_CustomFieldChanges.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_Options_CustomFieldChanges.Name = "Menu_Options_CustomFieldChanges"
    Me.Menu_Options_CustomFieldChanges.Size = New System.Drawing.Size(316, 22)
    Me.Menu_Options_CustomFieldChanges.Text = "Instantly reflect changes to Custom Field Data"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(313, 6)
    '
    'Menu_Options_ShowAllResults
    '
    Me.Menu_Options_ShowAllResults.Name = "Menu_Options_ShowAllResults"
    Me.Menu_Options_ShowAllResults.Size = New System.Drawing.Size(316, 22)
    Me.Menu_Options_ShowAllResults.Text = "Show All Search results."
    '
    'Menu_SavedSearches
    '
    Me.Menu_SavedSearches.Name = "Menu_SavedSearches"
    Me.Menu_SavedSearches.Size = New System.Drawing.Size(99, 20)
    Me.Menu_SavedSearches.Text = "&Saved Searches"
    '
    'ComboBox_SavedSearches
    '
    Me.ComboBox_SavedSearches.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.ComboBox_SavedSearches.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.ComboBox_SavedSearches.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.ComboBox_SavedSearches.Name = "ComboBox_SavedSearches"
    Me.ComboBox_SavedSearches.Size = New System.Drawing.Size(121, 23)
    Me.ComboBox_SavedSearches.Visible = False
    '
    'Panel_SearchInstruments
    '
    Me.Panel_SearchInstruments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_SearchInstruments.AutoScroll = True
    Me.Panel_SearchInstruments.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue21b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue20b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue19b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue18b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel21)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount21)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue21)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition21)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField21)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField20)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField19)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField18)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue17b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue16b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue15b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue14b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField17)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField16)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField15)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField14)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue13b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue12b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue11b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue10b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField13)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField12)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField11)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField10)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue9b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue8b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue7b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue6b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField9)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField8)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField7)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField6)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue5b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue4b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue3b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue2b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue1b)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Panel1)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Label_SelectCount1)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField5)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField4)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField3)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField2)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_AndOr1)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectValue1)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_Condition1)
    Me.Panel_SearchInstruments.Controls.Add(Me.Combo_SelectField1)
    Me.Panel_SearchInstruments.Location = New System.Drawing.Point(3, 3)
    Me.Panel_SearchInstruments.Name = "Panel_SearchInstruments"
    Me.Panel_SearchInstruments.Size = New System.Drawing.Size(1063, 166)
    Me.Panel_SearchInstruments.TabIndex = 0
    '
    'Combo_SelectValue21b
    '
    Me.Combo_SelectValue21b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue21b.Location = New System.Drawing.Point(677, 544)
    Me.Combo_SelectValue21b.Name = "Combo_SelectValue21b"
    Me.Combo_SelectValue21b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue21b.TabIndex = 147
    Me.Combo_SelectValue21b.Visible = False
    '
    'Combo_SelectValue20b
    '
    Me.Combo_SelectValue20b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue20b.Location = New System.Drawing.Point(677, 517)
    Me.Combo_SelectValue20b.Name = "Combo_SelectValue20b"
    Me.Combo_SelectValue20b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue20b.TabIndex = 140
    Me.Combo_SelectValue20b.Visible = False
    '
    'Combo_SelectValue19b
    '
    Me.Combo_SelectValue19b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue19b.Location = New System.Drawing.Point(677, 490)
    Me.Combo_SelectValue19b.Name = "Combo_SelectValue19b"
    Me.Combo_SelectValue19b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue19b.TabIndex = 133
    Me.Combo_SelectValue19b.Visible = False
    '
    'Combo_SelectValue18b
    '
    Me.Combo_SelectValue18b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue18b.Location = New System.Drawing.Point(677, 463)
    Me.Combo_SelectValue18b.Name = "Combo_SelectValue18b"
    Me.Combo_SelectValue18b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue18b.TabIndex = 126
    Me.Combo_SelectValue18b.Visible = False
    '
    'Panel21
    '
    Me.Panel21.Controls.Add(Me.Radio_Custom21)
    Me.Panel21.Controls.Add(Me.Radio_Static21)
    Me.Panel21.Location = New System.Drawing.Point(3, 542)
    Me.Panel21.Name = "Panel21"
    Me.Panel21.Size = New System.Drawing.Size(143, 24)
    Me.Panel21.TabIndex = 142
    '
    'Radio_Custom21
    '
    Me.Radio_Custom21.AutoSize = True
    Me.Radio_Custom21.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom21.Name = "Radio_Custom21"
    Me.Radio_Custom21.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom21.TabIndex = 1
    Me.Radio_Custom21.TabStop = True
    Me.Radio_Custom21.Text = "Custom"
    Me.Radio_Custom21.UseVisualStyleBackColor = True
    '
    'Radio_Static21
    '
    Me.Radio_Static21.AutoSize = True
    Me.Radio_Static21.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static21.Name = "Radio_Static21"
    Me.Radio_Static21.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static21.TabIndex = 0
    Me.Radio_Static21.TabStop = True
    Me.Radio_Static21.Text = "Static"
    Me.Radio_Static21.UseVisualStyleBackColor = True
    '
    'Panel20
    '
    Me.Panel20.Controls.Add(Me.Radio_Custom20)
    Me.Panel20.Controls.Add(Me.Radio_Static20)
    Me.Panel20.Location = New System.Drawing.Point(3, 515)
    Me.Panel20.Name = "Panel20"
    Me.Panel20.Size = New System.Drawing.Size(143, 24)
    Me.Panel20.TabIndex = 135
    '
    'Radio_Custom20
    '
    Me.Radio_Custom20.AutoSize = True
    Me.Radio_Custom20.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom20.Name = "Radio_Custom20"
    Me.Radio_Custom20.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom20.TabIndex = 1
    Me.Radio_Custom20.TabStop = True
    Me.Radio_Custom20.Text = "Custom"
    Me.Radio_Custom20.UseVisualStyleBackColor = True
    '
    'Radio_Static20
    '
    Me.Radio_Static20.AutoSize = True
    Me.Radio_Static20.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static20.Name = "Radio_Static20"
    Me.Radio_Static20.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static20.TabIndex = 0
    Me.Radio_Static20.TabStop = True
    Me.Radio_Static20.Text = "Static"
    Me.Radio_Static20.UseVisualStyleBackColor = True
    '
    'Panel19
    '
    Me.Panel19.Controls.Add(Me.Radio_Custom19)
    Me.Panel19.Controls.Add(Me.Radio_Static19)
    Me.Panel19.Location = New System.Drawing.Point(3, 488)
    Me.Panel19.Name = "Panel19"
    Me.Panel19.Size = New System.Drawing.Size(143, 24)
    Me.Panel19.TabIndex = 128
    '
    'Radio_Custom19
    '
    Me.Radio_Custom19.AutoSize = True
    Me.Radio_Custom19.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom19.Name = "Radio_Custom19"
    Me.Radio_Custom19.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom19.TabIndex = 1
    Me.Radio_Custom19.TabStop = True
    Me.Radio_Custom19.Text = "Custom"
    Me.Radio_Custom19.UseVisualStyleBackColor = True
    '
    'Radio_Static19
    '
    Me.Radio_Static19.AutoSize = True
    Me.Radio_Static19.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static19.Name = "Radio_Static19"
    Me.Radio_Static19.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static19.TabIndex = 0
    Me.Radio_Static19.TabStop = True
    Me.Radio_Static19.Text = "Static"
    Me.Radio_Static19.UseVisualStyleBackColor = True
    '
    'Panel18
    '
    Me.Panel18.Controls.Add(Me.Radio_Custom18)
    Me.Panel18.Controls.Add(Me.Radio_Static18)
    Me.Panel18.Location = New System.Drawing.Point(3, 461)
    Me.Panel18.Name = "Panel18"
    Me.Panel18.Size = New System.Drawing.Size(143, 24)
    Me.Panel18.TabIndex = 121
    '
    'Radio_Custom18
    '
    Me.Radio_Custom18.AutoSize = True
    Me.Radio_Custom18.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom18.Name = "Radio_Custom18"
    Me.Radio_Custom18.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom18.TabIndex = 1
    Me.Radio_Custom18.TabStop = True
    Me.Radio_Custom18.Text = "Custom"
    Me.Radio_Custom18.UseVisualStyleBackColor = True
    '
    'Radio_Static18
    '
    Me.Radio_Static18.AutoSize = True
    Me.Radio_Static18.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static18.Name = "Radio_Static18"
    Me.Radio_Static18.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static18.TabIndex = 0
    Me.Radio_Static18.TabStop = True
    Me.Radio_Static18.Text = "Static"
    Me.Radio_Static18.UseVisualStyleBackColor = True
    '
    'Label_SelectCount21
    '
    Me.Label_SelectCount21.Location = New System.Drawing.Point(880, 545)
    Me.Label_SelectCount21.Name = "Label_SelectCount21"
    Me.Label_SelectCount21.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount21.TabIndex = 148
    Me.Label_SelectCount21.Text = "0"
    '
    'Label_SelectCount20
    '
    Me.Label_SelectCount20.Location = New System.Drawing.Point(880, 518)
    Me.Label_SelectCount20.Name = "Label_SelectCount20"
    Me.Label_SelectCount20.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount20.TabIndex = 141
    Me.Label_SelectCount20.Text = "0"
    '
    'Label_SelectCount19
    '
    Me.Label_SelectCount19.Location = New System.Drawing.Point(880, 491)
    Me.Label_SelectCount19.Name = "Label_SelectCount19"
    Me.Label_SelectCount19.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount19.TabIndex = 134
    Me.Label_SelectCount19.Text = "0"
    '
    'Label_SelectCount18
    '
    Me.Label_SelectCount18.Location = New System.Drawing.Point(880, 464)
    Me.Label_SelectCount18.Name = "Label_SelectCount18"
    Me.Label_SelectCount18.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount18.TabIndex = 127
    Me.Label_SelectCount18.Text = "0"
    '
    'Combo_SelectValue21
    '
    Me.Combo_SelectValue21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue21.Location = New System.Drawing.Point(471, 544)
    Me.Combo_SelectValue21.Name = "Combo_SelectValue21"
    Me.Combo_SelectValue21.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue21.TabIndex = 146
    '
    'Combo_Condition21
    '
    Me.Combo_Condition21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition21.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition21.Location = New System.Drawing.Point(397, 544)
    Me.Combo_Condition21.MaxDropDownItems = 10
    Me.Combo_Condition21.Name = "Combo_Condition21"
    Me.Combo_Condition21.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition21.TabIndex = 145
    '
    'Combo_SelectField21
    '
    Me.Combo_SelectField21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField21.Location = New System.Drawing.Point(232, 544)
    Me.Combo_SelectField21.Name = "Combo_SelectField21"
    Me.Combo_SelectField21.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField21.TabIndex = 144
    '
    'Combo_AndOr20
    '
    Me.Combo_AndOr20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr20.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr20.Location = New System.Drawing.Point(158, 544)
    Me.Combo_AndOr20.Name = "Combo_AndOr20"
    Me.Combo_AndOr20.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr20.TabIndex = 143
    '
    'Combo_SelectValue20
    '
    Me.Combo_SelectValue20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue20.Location = New System.Drawing.Point(471, 517)
    Me.Combo_SelectValue20.Name = "Combo_SelectValue20"
    Me.Combo_SelectValue20.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue20.TabIndex = 139
    '
    'Combo_Condition20
    '
    Me.Combo_Condition20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition20.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition20.Location = New System.Drawing.Point(397, 517)
    Me.Combo_Condition20.MaxDropDownItems = 10
    Me.Combo_Condition20.Name = "Combo_Condition20"
    Me.Combo_Condition20.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition20.TabIndex = 138
    '
    'Combo_SelectField20
    '
    Me.Combo_SelectField20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField20.Location = New System.Drawing.Point(232, 517)
    Me.Combo_SelectField20.Name = "Combo_SelectField20"
    Me.Combo_SelectField20.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField20.TabIndex = 137
    '
    'Combo_AndOr19
    '
    Me.Combo_AndOr19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr19.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr19.Location = New System.Drawing.Point(158, 517)
    Me.Combo_AndOr19.Name = "Combo_AndOr19"
    Me.Combo_AndOr19.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr19.TabIndex = 136
    '
    'Combo_SelectValue19
    '
    Me.Combo_SelectValue19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue19.Location = New System.Drawing.Point(471, 490)
    Me.Combo_SelectValue19.Name = "Combo_SelectValue19"
    Me.Combo_SelectValue19.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue19.TabIndex = 132
    '
    'Combo_Condition19
    '
    Me.Combo_Condition19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition19.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition19.Location = New System.Drawing.Point(397, 490)
    Me.Combo_Condition19.MaxDropDownItems = 10
    Me.Combo_Condition19.Name = "Combo_Condition19"
    Me.Combo_Condition19.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition19.TabIndex = 131
    '
    'Combo_SelectField19
    '
    Me.Combo_SelectField19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField19.Location = New System.Drawing.Point(232, 490)
    Me.Combo_SelectField19.Name = "Combo_SelectField19"
    Me.Combo_SelectField19.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField19.TabIndex = 130
    '
    'Combo_AndOr18
    '
    Me.Combo_AndOr18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr18.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr18.Location = New System.Drawing.Point(158, 490)
    Me.Combo_AndOr18.Name = "Combo_AndOr18"
    Me.Combo_AndOr18.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr18.TabIndex = 129
    '
    'Combo_SelectValue18
    '
    Me.Combo_SelectValue18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue18.Location = New System.Drawing.Point(471, 463)
    Me.Combo_SelectValue18.Name = "Combo_SelectValue18"
    Me.Combo_SelectValue18.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue18.TabIndex = 125
    '
    'Combo_Condition18
    '
    Me.Combo_Condition18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition18.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition18.Location = New System.Drawing.Point(397, 463)
    Me.Combo_Condition18.MaxDropDownItems = 10
    Me.Combo_Condition18.Name = "Combo_Condition18"
    Me.Combo_Condition18.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition18.TabIndex = 124
    '
    'Combo_SelectField18
    '
    Me.Combo_SelectField18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField18.Location = New System.Drawing.Point(232, 463)
    Me.Combo_SelectField18.Name = "Combo_SelectField18"
    Me.Combo_SelectField18.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField18.TabIndex = 123
    '
    'Combo_AndOr17
    '
    Me.Combo_AndOr17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr17.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr17.Location = New System.Drawing.Point(158, 463)
    Me.Combo_AndOr17.Name = "Combo_AndOr17"
    Me.Combo_AndOr17.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr17.TabIndex = 122
    '
    'Combo_SelectValue17b
    '
    Me.Combo_SelectValue17b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue17b.Location = New System.Drawing.Point(677, 436)
    Me.Combo_SelectValue17b.Name = "Combo_SelectValue17b"
    Me.Combo_SelectValue17b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue17b.TabIndex = 119
    Me.Combo_SelectValue17b.Visible = False
    '
    'Combo_SelectValue16b
    '
    Me.Combo_SelectValue16b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue16b.Location = New System.Drawing.Point(677, 409)
    Me.Combo_SelectValue16b.Name = "Combo_SelectValue16b"
    Me.Combo_SelectValue16b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue16b.TabIndex = 112
    Me.Combo_SelectValue16b.Visible = False
    '
    'Combo_SelectValue15b
    '
    Me.Combo_SelectValue15b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue15b.Location = New System.Drawing.Point(677, 382)
    Me.Combo_SelectValue15b.Name = "Combo_SelectValue15b"
    Me.Combo_SelectValue15b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue15b.TabIndex = 105
    Me.Combo_SelectValue15b.Visible = False
    '
    'Combo_SelectValue14b
    '
    Me.Combo_SelectValue14b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue14b.Location = New System.Drawing.Point(677, 355)
    Me.Combo_SelectValue14b.Name = "Combo_SelectValue14b"
    Me.Combo_SelectValue14b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue14b.TabIndex = 98
    Me.Combo_SelectValue14b.Visible = False
    '
    'Panel17
    '
    Me.Panel17.Controls.Add(Me.Radio_Custom17)
    Me.Panel17.Controls.Add(Me.Radio_Static17)
    Me.Panel17.Location = New System.Drawing.Point(3, 434)
    Me.Panel17.Name = "Panel17"
    Me.Panel17.Size = New System.Drawing.Size(143, 24)
    Me.Panel17.TabIndex = 114
    '
    'Radio_Custom17
    '
    Me.Radio_Custom17.AutoSize = True
    Me.Radio_Custom17.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom17.Name = "Radio_Custom17"
    Me.Radio_Custom17.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom17.TabIndex = 1
    Me.Radio_Custom17.TabStop = True
    Me.Radio_Custom17.Text = "Custom"
    Me.Radio_Custom17.UseVisualStyleBackColor = True
    '
    'Radio_Static17
    '
    Me.Radio_Static17.AutoSize = True
    Me.Radio_Static17.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static17.Name = "Radio_Static17"
    Me.Radio_Static17.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static17.TabIndex = 0
    Me.Radio_Static17.TabStop = True
    Me.Radio_Static17.Text = "Static"
    Me.Radio_Static17.UseVisualStyleBackColor = True
    '
    'Panel16
    '
    Me.Panel16.Controls.Add(Me.Radio_Custom16)
    Me.Panel16.Controls.Add(Me.Radio_Static16)
    Me.Panel16.Location = New System.Drawing.Point(3, 407)
    Me.Panel16.Name = "Panel16"
    Me.Panel16.Size = New System.Drawing.Size(143, 24)
    Me.Panel16.TabIndex = 107
    '
    'Radio_Custom16
    '
    Me.Radio_Custom16.AutoSize = True
    Me.Radio_Custom16.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom16.Name = "Radio_Custom16"
    Me.Radio_Custom16.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom16.TabIndex = 1
    Me.Radio_Custom16.TabStop = True
    Me.Radio_Custom16.Text = "Custom"
    Me.Radio_Custom16.UseVisualStyleBackColor = True
    '
    'Radio_Static16
    '
    Me.Radio_Static16.AutoSize = True
    Me.Radio_Static16.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static16.Name = "Radio_Static16"
    Me.Radio_Static16.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static16.TabIndex = 0
    Me.Radio_Static16.TabStop = True
    Me.Radio_Static16.Text = "Static"
    Me.Radio_Static16.UseVisualStyleBackColor = True
    '
    'Panel15
    '
    Me.Panel15.Controls.Add(Me.Radio_Custom15)
    Me.Panel15.Controls.Add(Me.Radio_Static15)
    Me.Panel15.Location = New System.Drawing.Point(3, 380)
    Me.Panel15.Name = "Panel15"
    Me.Panel15.Size = New System.Drawing.Size(143, 24)
    Me.Panel15.TabIndex = 100
    '
    'Radio_Custom15
    '
    Me.Radio_Custom15.AutoSize = True
    Me.Radio_Custom15.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom15.Name = "Radio_Custom15"
    Me.Radio_Custom15.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom15.TabIndex = 1
    Me.Radio_Custom15.TabStop = True
    Me.Radio_Custom15.Text = "Custom"
    Me.Radio_Custom15.UseVisualStyleBackColor = True
    '
    'Radio_Static15
    '
    Me.Radio_Static15.AutoSize = True
    Me.Radio_Static15.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static15.Name = "Radio_Static15"
    Me.Radio_Static15.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static15.TabIndex = 0
    Me.Radio_Static15.TabStop = True
    Me.Radio_Static15.Text = "Static"
    Me.Radio_Static15.UseVisualStyleBackColor = True
    '
    'Panel14
    '
    Me.Panel14.Controls.Add(Me.Radio_Custom14)
    Me.Panel14.Controls.Add(Me.Radio_Static14)
    Me.Panel14.Location = New System.Drawing.Point(3, 353)
    Me.Panel14.Name = "Panel14"
    Me.Panel14.Size = New System.Drawing.Size(143, 24)
    Me.Panel14.TabIndex = 93
    '
    'Radio_Custom14
    '
    Me.Radio_Custom14.AutoSize = True
    Me.Radio_Custom14.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom14.Name = "Radio_Custom14"
    Me.Radio_Custom14.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom14.TabIndex = 1
    Me.Radio_Custom14.TabStop = True
    Me.Radio_Custom14.Text = "Custom"
    Me.Radio_Custom14.UseVisualStyleBackColor = True
    '
    'Radio_Static14
    '
    Me.Radio_Static14.AutoSize = True
    Me.Radio_Static14.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static14.Name = "Radio_Static14"
    Me.Radio_Static14.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static14.TabIndex = 0
    Me.Radio_Static14.TabStop = True
    Me.Radio_Static14.Text = "Static"
    Me.Radio_Static14.UseVisualStyleBackColor = True
    '
    'Label_SelectCount17
    '
    Me.Label_SelectCount17.Location = New System.Drawing.Point(880, 437)
    Me.Label_SelectCount17.Name = "Label_SelectCount17"
    Me.Label_SelectCount17.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount17.TabIndex = 120
    Me.Label_SelectCount17.Text = "0"
    '
    'Label_SelectCount16
    '
    Me.Label_SelectCount16.Location = New System.Drawing.Point(880, 410)
    Me.Label_SelectCount16.Name = "Label_SelectCount16"
    Me.Label_SelectCount16.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount16.TabIndex = 113
    Me.Label_SelectCount16.Text = "0"
    '
    'Label_SelectCount15
    '
    Me.Label_SelectCount15.Location = New System.Drawing.Point(880, 383)
    Me.Label_SelectCount15.Name = "Label_SelectCount15"
    Me.Label_SelectCount15.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount15.TabIndex = 106
    Me.Label_SelectCount15.Text = "0"
    '
    'Label_SelectCount14
    '
    Me.Label_SelectCount14.Location = New System.Drawing.Point(880, 356)
    Me.Label_SelectCount14.Name = "Label_SelectCount14"
    Me.Label_SelectCount14.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount14.TabIndex = 99
    Me.Label_SelectCount14.Text = "0"
    '
    'Combo_SelectValue17
    '
    Me.Combo_SelectValue17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue17.Location = New System.Drawing.Point(471, 436)
    Me.Combo_SelectValue17.Name = "Combo_SelectValue17"
    Me.Combo_SelectValue17.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue17.TabIndex = 118
    '
    'Combo_Condition17
    '
    Me.Combo_Condition17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition17.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition17.Location = New System.Drawing.Point(397, 436)
    Me.Combo_Condition17.MaxDropDownItems = 10
    Me.Combo_Condition17.Name = "Combo_Condition17"
    Me.Combo_Condition17.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition17.TabIndex = 117
    '
    'Combo_SelectField17
    '
    Me.Combo_SelectField17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField17.Location = New System.Drawing.Point(232, 436)
    Me.Combo_SelectField17.Name = "Combo_SelectField17"
    Me.Combo_SelectField17.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField17.TabIndex = 116
    '
    'Combo_AndOr16
    '
    Me.Combo_AndOr16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr16.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr16.Location = New System.Drawing.Point(158, 436)
    Me.Combo_AndOr16.Name = "Combo_AndOr16"
    Me.Combo_AndOr16.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr16.TabIndex = 115
    '
    'Combo_SelectValue16
    '
    Me.Combo_SelectValue16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue16.Location = New System.Drawing.Point(471, 409)
    Me.Combo_SelectValue16.Name = "Combo_SelectValue16"
    Me.Combo_SelectValue16.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue16.TabIndex = 111
    '
    'Combo_Condition16
    '
    Me.Combo_Condition16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition16.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition16.Location = New System.Drawing.Point(397, 409)
    Me.Combo_Condition16.MaxDropDownItems = 10
    Me.Combo_Condition16.Name = "Combo_Condition16"
    Me.Combo_Condition16.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition16.TabIndex = 110
    '
    'Combo_SelectField16
    '
    Me.Combo_SelectField16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField16.Location = New System.Drawing.Point(232, 409)
    Me.Combo_SelectField16.Name = "Combo_SelectField16"
    Me.Combo_SelectField16.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField16.TabIndex = 109
    '
    'Combo_AndOr15
    '
    Me.Combo_AndOr15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr15.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr15.Location = New System.Drawing.Point(158, 409)
    Me.Combo_AndOr15.Name = "Combo_AndOr15"
    Me.Combo_AndOr15.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr15.TabIndex = 108
    '
    'Combo_SelectValue15
    '
    Me.Combo_SelectValue15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue15.Location = New System.Drawing.Point(471, 382)
    Me.Combo_SelectValue15.Name = "Combo_SelectValue15"
    Me.Combo_SelectValue15.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue15.TabIndex = 104
    '
    'Combo_Condition15
    '
    Me.Combo_Condition15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition15.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition15.Location = New System.Drawing.Point(397, 382)
    Me.Combo_Condition15.MaxDropDownItems = 10
    Me.Combo_Condition15.Name = "Combo_Condition15"
    Me.Combo_Condition15.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition15.TabIndex = 103
    '
    'Combo_SelectField15
    '
    Me.Combo_SelectField15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField15.Location = New System.Drawing.Point(232, 382)
    Me.Combo_SelectField15.Name = "Combo_SelectField15"
    Me.Combo_SelectField15.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField15.TabIndex = 102
    '
    'Combo_AndOr14
    '
    Me.Combo_AndOr14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr14.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr14.Location = New System.Drawing.Point(158, 382)
    Me.Combo_AndOr14.Name = "Combo_AndOr14"
    Me.Combo_AndOr14.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr14.TabIndex = 101
    '
    'Combo_SelectValue14
    '
    Me.Combo_SelectValue14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue14.Location = New System.Drawing.Point(471, 355)
    Me.Combo_SelectValue14.Name = "Combo_SelectValue14"
    Me.Combo_SelectValue14.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue14.TabIndex = 97
    '
    'Combo_Condition14
    '
    Me.Combo_Condition14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition14.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition14.Location = New System.Drawing.Point(397, 355)
    Me.Combo_Condition14.MaxDropDownItems = 10
    Me.Combo_Condition14.Name = "Combo_Condition14"
    Me.Combo_Condition14.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition14.TabIndex = 96
    '
    'Combo_SelectField14
    '
    Me.Combo_SelectField14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField14.Location = New System.Drawing.Point(232, 355)
    Me.Combo_SelectField14.Name = "Combo_SelectField14"
    Me.Combo_SelectField14.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField14.TabIndex = 95
    '
    'Combo_AndOr13
    '
    Me.Combo_AndOr13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr13.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr13.Location = New System.Drawing.Point(158, 355)
    Me.Combo_AndOr13.Name = "Combo_AndOr13"
    Me.Combo_AndOr13.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr13.TabIndex = 94
    '
    'Combo_SelectValue13b
    '
    Me.Combo_SelectValue13b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue13b.Location = New System.Drawing.Point(677, 328)
    Me.Combo_SelectValue13b.Name = "Combo_SelectValue13b"
    Me.Combo_SelectValue13b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue13b.TabIndex = 91
    Me.Combo_SelectValue13b.Visible = False
    '
    'Combo_SelectValue12b
    '
    Me.Combo_SelectValue12b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue12b.Location = New System.Drawing.Point(677, 301)
    Me.Combo_SelectValue12b.Name = "Combo_SelectValue12b"
    Me.Combo_SelectValue12b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue12b.TabIndex = 84
    Me.Combo_SelectValue12b.Visible = False
    '
    'Combo_SelectValue11b
    '
    Me.Combo_SelectValue11b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue11b.Location = New System.Drawing.Point(677, 274)
    Me.Combo_SelectValue11b.Name = "Combo_SelectValue11b"
    Me.Combo_SelectValue11b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue11b.TabIndex = 77
    Me.Combo_SelectValue11b.Visible = False
    '
    'Combo_SelectValue10b
    '
    Me.Combo_SelectValue10b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue10b.Location = New System.Drawing.Point(677, 247)
    Me.Combo_SelectValue10b.Name = "Combo_SelectValue10b"
    Me.Combo_SelectValue10b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue10b.TabIndex = 70
    Me.Combo_SelectValue10b.Visible = False
    '
    'Panel13
    '
    Me.Panel13.Controls.Add(Me.Radio_Custom13)
    Me.Panel13.Controls.Add(Me.Radio_Static13)
    Me.Panel13.Location = New System.Drawing.Point(3, 326)
    Me.Panel13.Name = "Panel13"
    Me.Panel13.Size = New System.Drawing.Size(143, 24)
    Me.Panel13.TabIndex = 86
    '
    'Radio_Custom13
    '
    Me.Radio_Custom13.AutoSize = True
    Me.Radio_Custom13.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom13.Name = "Radio_Custom13"
    Me.Radio_Custom13.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom13.TabIndex = 1
    Me.Radio_Custom13.TabStop = True
    Me.Radio_Custom13.Text = "Custom"
    Me.Radio_Custom13.UseVisualStyleBackColor = True
    '
    'Radio_Static13
    '
    Me.Radio_Static13.AutoSize = True
    Me.Radio_Static13.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static13.Name = "Radio_Static13"
    Me.Radio_Static13.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static13.TabIndex = 0
    Me.Radio_Static13.TabStop = True
    Me.Radio_Static13.Text = "Static"
    Me.Radio_Static13.UseVisualStyleBackColor = True
    '
    'Panel12
    '
    Me.Panel12.Controls.Add(Me.Radio_Custom12)
    Me.Panel12.Controls.Add(Me.Radio_Static12)
    Me.Panel12.Location = New System.Drawing.Point(3, 299)
    Me.Panel12.Name = "Panel12"
    Me.Panel12.Size = New System.Drawing.Size(143, 24)
    Me.Panel12.TabIndex = 79
    '
    'Radio_Custom12
    '
    Me.Radio_Custom12.AutoSize = True
    Me.Radio_Custom12.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom12.Name = "Radio_Custom12"
    Me.Radio_Custom12.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom12.TabIndex = 1
    Me.Radio_Custom12.TabStop = True
    Me.Radio_Custom12.Text = "Custom"
    Me.Radio_Custom12.UseVisualStyleBackColor = True
    '
    'Radio_Static12
    '
    Me.Radio_Static12.AutoSize = True
    Me.Radio_Static12.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static12.Name = "Radio_Static12"
    Me.Radio_Static12.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static12.TabIndex = 0
    Me.Radio_Static12.TabStop = True
    Me.Radio_Static12.Text = "Static"
    Me.Radio_Static12.UseVisualStyleBackColor = True
    '
    'Panel11
    '
    Me.Panel11.Controls.Add(Me.Radio_Custom11)
    Me.Panel11.Controls.Add(Me.Radio_Static11)
    Me.Panel11.Location = New System.Drawing.Point(3, 272)
    Me.Panel11.Name = "Panel11"
    Me.Panel11.Size = New System.Drawing.Size(143, 24)
    Me.Panel11.TabIndex = 72
    '
    'Radio_Custom11
    '
    Me.Radio_Custom11.AutoSize = True
    Me.Radio_Custom11.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom11.Name = "Radio_Custom11"
    Me.Radio_Custom11.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom11.TabIndex = 1
    Me.Radio_Custom11.TabStop = True
    Me.Radio_Custom11.Text = "Custom"
    Me.Radio_Custom11.UseVisualStyleBackColor = True
    '
    'Radio_Static11
    '
    Me.Radio_Static11.AutoSize = True
    Me.Radio_Static11.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static11.Name = "Radio_Static11"
    Me.Radio_Static11.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static11.TabIndex = 0
    Me.Radio_Static11.TabStop = True
    Me.Radio_Static11.Text = "Static"
    Me.Radio_Static11.UseVisualStyleBackColor = True
    '
    'Panel10
    '
    Me.Panel10.Controls.Add(Me.Radio_Custom10)
    Me.Panel10.Controls.Add(Me.Radio_Static10)
    Me.Panel10.Location = New System.Drawing.Point(3, 245)
    Me.Panel10.Name = "Panel10"
    Me.Panel10.Size = New System.Drawing.Size(143, 24)
    Me.Panel10.TabIndex = 65
    '
    'Radio_Custom10
    '
    Me.Radio_Custom10.AutoSize = True
    Me.Radio_Custom10.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom10.Name = "Radio_Custom10"
    Me.Radio_Custom10.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom10.TabIndex = 1
    Me.Radio_Custom10.TabStop = True
    Me.Radio_Custom10.Text = "Custom"
    Me.Radio_Custom10.UseVisualStyleBackColor = True
    '
    'Radio_Static10
    '
    Me.Radio_Static10.AutoSize = True
    Me.Radio_Static10.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static10.Name = "Radio_Static10"
    Me.Radio_Static10.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static10.TabIndex = 0
    Me.Radio_Static10.TabStop = True
    Me.Radio_Static10.Text = "Static"
    Me.Radio_Static10.UseVisualStyleBackColor = True
    '
    'Label_SelectCount13
    '
    Me.Label_SelectCount13.Location = New System.Drawing.Point(880, 329)
    Me.Label_SelectCount13.Name = "Label_SelectCount13"
    Me.Label_SelectCount13.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount13.TabIndex = 92
    Me.Label_SelectCount13.Text = "0"
    '
    'Label_SelectCount12
    '
    Me.Label_SelectCount12.Location = New System.Drawing.Point(880, 302)
    Me.Label_SelectCount12.Name = "Label_SelectCount12"
    Me.Label_SelectCount12.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount12.TabIndex = 85
    Me.Label_SelectCount12.Text = "0"
    '
    'Label_SelectCount11
    '
    Me.Label_SelectCount11.Location = New System.Drawing.Point(880, 275)
    Me.Label_SelectCount11.Name = "Label_SelectCount11"
    Me.Label_SelectCount11.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount11.TabIndex = 78
    Me.Label_SelectCount11.Text = "0"
    '
    'Label_SelectCount10
    '
    Me.Label_SelectCount10.Location = New System.Drawing.Point(880, 248)
    Me.Label_SelectCount10.Name = "Label_SelectCount10"
    Me.Label_SelectCount10.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount10.TabIndex = 71
    Me.Label_SelectCount10.Text = "0"
    '
    'Combo_SelectValue13
    '
    Me.Combo_SelectValue13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue13.Location = New System.Drawing.Point(471, 328)
    Me.Combo_SelectValue13.Name = "Combo_SelectValue13"
    Me.Combo_SelectValue13.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue13.TabIndex = 90
    '
    'Combo_Condition13
    '
    Me.Combo_Condition13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition13.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition13.Location = New System.Drawing.Point(397, 328)
    Me.Combo_Condition13.MaxDropDownItems = 10
    Me.Combo_Condition13.Name = "Combo_Condition13"
    Me.Combo_Condition13.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition13.TabIndex = 89
    '
    'Combo_SelectField13
    '
    Me.Combo_SelectField13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField13.Location = New System.Drawing.Point(232, 328)
    Me.Combo_SelectField13.Name = "Combo_SelectField13"
    Me.Combo_SelectField13.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField13.TabIndex = 88
    '
    'Combo_AndOr12
    '
    Me.Combo_AndOr12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr12.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr12.Location = New System.Drawing.Point(158, 328)
    Me.Combo_AndOr12.Name = "Combo_AndOr12"
    Me.Combo_AndOr12.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr12.TabIndex = 87
    '
    'Combo_SelectValue12
    '
    Me.Combo_SelectValue12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue12.Location = New System.Drawing.Point(471, 301)
    Me.Combo_SelectValue12.Name = "Combo_SelectValue12"
    Me.Combo_SelectValue12.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue12.TabIndex = 83
    '
    'Combo_Condition12
    '
    Me.Combo_Condition12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition12.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition12.Location = New System.Drawing.Point(397, 301)
    Me.Combo_Condition12.MaxDropDownItems = 10
    Me.Combo_Condition12.Name = "Combo_Condition12"
    Me.Combo_Condition12.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition12.TabIndex = 82
    '
    'Combo_SelectField12
    '
    Me.Combo_SelectField12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField12.Location = New System.Drawing.Point(232, 301)
    Me.Combo_SelectField12.Name = "Combo_SelectField12"
    Me.Combo_SelectField12.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField12.TabIndex = 81
    '
    'Combo_AndOr11
    '
    Me.Combo_AndOr11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr11.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr11.Location = New System.Drawing.Point(158, 301)
    Me.Combo_AndOr11.Name = "Combo_AndOr11"
    Me.Combo_AndOr11.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr11.TabIndex = 80
    '
    'Combo_SelectValue11
    '
    Me.Combo_SelectValue11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue11.Location = New System.Drawing.Point(471, 274)
    Me.Combo_SelectValue11.Name = "Combo_SelectValue11"
    Me.Combo_SelectValue11.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue11.TabIndex = 76
    '
    'Combo_Condition11
    '
    Me.Combo_Condition11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition11.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition11.Location = New System.Drawing.Point(397, 274)
    Me.Combo_Condition11.MaxDropDownItems = 10
    Me.Combo_Condition11.Name = "Combo_Condition11"
    Me.Combo_Condition11.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition11.TabIndex = 75
    '
    'Combo_SelectField11
    '
    Me.Combo_SelectField11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField11.Location = New System.Drawing.Point(232, 274)
    Me.Combo_SelectField11.Name = "Combo_SelectField11"
    Me.Combo_SelectField11.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField11.TabIndex = 74
    '
    'Combo_AndOr10
    '
    Me.Combo_AndOr10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr10.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr10.Location = New System.Drawing.Point(158, 274)
    Me.Combo_AndOr10.Name = "Combo_AndOr10"
    Me.Combo_AndOr10.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr10.TabIndex = 73
    '
    'Combo_SelectValue10
    '
    Me.Combo_SelectValue10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue10.Location = New System.Drawing.Point(471, 247)
    Me.Combo_SelectValue10.Name = "Combo_SelectValue10"
    Me.Combo_SelectValue10.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue10.TabIndex = 69
    '
    'Combo_Condition10
    '
    Me.Combo_Condition10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition10.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition10.Location = New System.Drawing.Point(397, 247)
    Me.Combo_Condition10.MaxDropDownItems = 10
    Me.Combo_Condition10.Name = "Combo_Condition10"
    Me.Combo_Condition10.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition10.TabIndex = 68
    '
    'Combo_SelectField10
    '
    Me.Combo_SelectField10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField10.Location = New System.Drawing.Point(232, 247)
    Me.Combo_SelectField10.Name = "Combo_SelectField10"
    Me.Combo_SelectField10.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField10.TabIndex = 67
    '
    'Combo_AndOr9
    '
    Me.Combo_AndOr9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr9.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr9.Location = New System.Drawing.Point(158, 247)
    Me.Combo_AndOr9.Name = "Combo_AndOr9"
    Me.Combo_AndOr9.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr9.TabIndex = 66
    '
    'Combo_SelectValue9b
    '
    Me.Combo_SelectValue9b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue9b.Location = New System.Drawing.Point(677, 220)
    Me.Combo_SelectValue9b.Name = "Combo_SelectValue9b"
    Me.Combo_SelectValue9b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue9b.TabIndex = 63
    Me.Combo_SelectValue9b.Visible = False
    '
    'Combo_SelectValue8b
    '
    Me.Combo_SelectValue8b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue8b.Location = New System.Drawing.Point(677, 193)
    Me.Combo_SelectValue8b.Name = "Combo_SelectValue8b"
    Me.Combo_SelectValue8b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue8b.TabIndex = 56
    Me.Combo_SelectValue8b.Visible = False
    '
    'Combo_SelectValue7b
    '
    Me.Combo_SelectValue7b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue7b.Location = New System.Drawing.Point(677, 166)
    Me.Combo_SelectValue7b.Name = "Combo_SelectValue7b"
    Me.Combo_SelectValue7b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue7b.TabIndex = 49
    Me.Combo_SelectValue7b.Visible = False
    '
    'Combo_SelectValue6b
    '
    Me.Combo_SelectValue6b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue6b.Location = New System.Drawing.Point(677, 139)
    Me.Combo_SelectValue6b.Name = "Combo_SelectValue6b"
    Me.Combo_SelectValue6b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue6b.TabIndex = 42
    Me.Combo_SelectValue6b.Visible = False
    '
    'Panel9
    '
    Me.Panel9.Controls.Add(Me.Radio_Custom9)
    Me.Panel9.Controls.Add(Me.Radio_Static9)
    Me.Panel9.Location = New System.Drawing.Point(3, 218)
    Me.Panel9.Name = "Panel9"
    Me.Panel9.Size = New System.Drawing.Size(143, 24)
    Me.Panel9.TabIndex = 58
    '
    'Radio_Custom9
    '
    Me.Radio_Custom9.AutoSize = True
    Me.Radio_Custom9.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom9.Name = "Radio_Custom9"
    Me.Radio_Custom9.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom9.TabIndex = 1
    Me.Radio_Custom9.TabStop = True
    Me.Radio_Custom9.Text = "Custom"
    Me.Radio_Custom9.UseVisualStyleBackColor = True
    '
    'Radio_Static9
    '
    Me.Radio_Static9.AutoSize = True
    Me.Radio_Static9.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static9.Name = "Radio_Static9"
    Me.Radio_Static9.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static9.TabIndex = 0
    Me.Radio_Static9.TabStop = True
    Me.Radio_Static9.Text = "Static"
    Me.Radio_Static9.UseVisualStyleBackColor = True
    '
    'Panel8
    '
    Me.Panel8.Controls.Add(Me.Radio_Custom8)
    Me.Panel8.Controls.Add(Me.Radio_Static8)
    Me.Panel8.Location = New System.Drawing.Point(3, 191)
    Me.Panel8.Name = "Panel8"
    Me.Panel8.Size = New System.Drawing.Size(143, 24)
    Me.Panel8.TabIndex = 51
    '
    'Radio_Custom8
    '
    Me.Radio_Custom8.AutoSize = True
    Me.Radio_Custom8.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom8.Name = "Radio_Custom8"
    Me.Radio_Custom8.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom8.TabIndex = 1
    Me.Radio_Custom8.TabStop = True
    Me.Radio_Custom8.Text = "Custom"
    Me.Radio_Custom8.UseVisualStyleBackColor = True
    '
    'Radio_Static8
    '
    Me.Radio_Static8.AutoSize = True
    Me.Radio_Static8.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static8.Name = "Radio_Static8"
    Me.Radio_Static8.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static8.TabIndex = 0
    Me.Radio_Static8.TabStop = True
    Me.Radio_Static8.Text = "Static"
    Me.Radio_Static8.UseVisualStyleBackColor = True
    '
    'Panel7
    '
    Me.Panel7.Controls.Add(Me.Radio_Custom7)
    Me.Panel7.Controls.Add(Me.Radio_Static7)
    Me.Panel7.Location = New System.Drawing.Point(3, 164)
    Me.Panel7.Name = "Panel7"
    Me.Panel7.Size = New System.Drawing.Size(143, 24)
    Me.Panel7.TabIndex = 44
    '
    'Radio_Custom7
    '
    Me.Radio_Custom7.AutoSize = True
    Me.Radio_Custom7.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom7.Name = "Radio_Custom7"
    Me.Radio_Custom7.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom7.TabIndex = 1
    Me.Radio_Custom7.TabStop = True
    Me.Radio_Custom7.Text = "Custom"
    Me.Radio_Custom7.UseVisualStyleBackColor = True
    '
    'Radio_Static7
    '
    Me.Radio_Static7.AutoSize = True
    Me.Radio_Static7.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static7.Name = "Radio_Static7"
    Me.Radio_Static7.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static7.TabIndex = 0
    Me.Radio_Static7.TabStop = True
    Me.Radio_Static7.Text = "Static"
    Me.Radio_Static7.UseVisualStyleBackColor = True
    '
    'Panel6
    '
    Me.Panel6.Controls.Add(Me.Radio_Custom6)
    Me.Panel6.Controls.Add(Me.Radio_Static6)
    Me.Panel6.Location = New System.Drawing.Point(3, 137)
    Me.Panel6.Name = "Panel6"
    Me.Panel6.Size = New System.Drawing.Size(143, 24)
    Me.Panel6.TabIndex = 37
    '
    'Radio_Custom6
    '
    Me.Radio_Custom6.AutoSize = True
    Me.Radio_Custom6.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom6.Name = "Radio_Custom6"
    Me.Radio_Custom6.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom6.TabIndex = 1
    Me.Radio_Custom6.TabStop = True
    Me.Radio_Custom6.Text = "Custom"
    Me.Radio_Custom6.UseVisualStyleBackColor = True
    '
    'Radio_Static6
    '
    Me.Radio_Static6.AutoSize = True
    Me.Radio_Static6.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static6.Name = "Radio_Static6"
    Me.Radio_Static6.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static6.TabIndex = 0
    Me.Radio_Static6.TabStop = True
    Me.Radio_Static6.Text = "Static"
    Me.Radio_Static6.UseVisualStyleBackColor = True
    '
    'Label_SelectCount9
    '
    Me.Label_SelectCount9.Location = New System.Drawing.Point(880, 221)
    Me.Label_SelectCount9.Name = "Label_SelectCount9"
    Me.Label_SelectCount9.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount9.TabIndex = 64
    Me.Label_SelectCount9.Text = "0"
    '
    'Label_SelectCount8
    '
    Me.Label_SelectCount8.Location = New System.Drawing.Point(880, 194)
    Me.Label_SelectCount8.Name = "Label_SelectCount8"
    Me.Label_SelectCount8.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount8.TabIndex = 57
    Me.Label_SelectCount8.Text = "0"
    '
    'Label_SelectCount7
    '
    Me.Label_SelectCount7.Location = New System.Drawing.Point(880, 167)
    Me.Label_SelectCount7.Name = "Label_SelectCount7"
    Me.Label_SelectCount7.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount7.TabIndex = 50
    Me.Label_SelectCount7.Text = "0"
    '
    'Label_SelectCount6
    '
    Me.Label_SelectCount6.Location = New System.Drawing.Point(880, 140)
    Me.Label_SelectCount6.Name = "Label_SelectCount6"
    Me.Label_SelectCount6.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount6.TabIndex = 43
    Me.Label_SelectCount6.Text = "0"
    '
    'Combo_SelectValue9
    '
    Me.Combo_SelectValue9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue9.Location = New System.Drawing.Point(471, 220)
    Me.Combo_SelectValue9.Name = "Combo_SelectValue9"
    Me.Combo_SelectValue9.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue9.TabIndex = 62
    '
    'Combo_Condition9
    '
    Me.Combo_Condition9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition9.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition9.Location = New System.Drawing.Point(397, 220)
    Me.Combo_Condition9.MaxDropDownItems = 10
    Me.Combo_Condition9.Name = "Combo_Condition9"
    Me.Combo_Condition9.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition9.TabIndex = 61
    '
    'Combo_SelectField9
    '
    Me.Combo_SelectField9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField9.Location = New System.Drawing.Point(232, 220)
    Me.Combo_SelectField9.Name = "Combo_SelectField9"
    Me.Combo_SelectField9.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField9.TabIndex = 60
    '
    'Combo_AndOr8
    '
    Me.Combo_AndOr8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr8.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr8.Location = New System.Drawing.Point(158, 220)
    Me.Combo_AndOr8.Name = "Combo_AndOr8"
    Me.Combo_AndOr8.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr8.TabIndex = 59
    '
    'Combo_SelectValue8
    '
    Me.Combo_SelectValue8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue8.Location = New System.Drawing.Point(471, 193)
    Me.Combo_SelectValue8.Name = "Combo_SelectValue8"
    Me.Combo_SelectValue8.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue8.TabIndex = 55
    '
    'Combo_Condition8
    '
    Me.Combo_Condition8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition8.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition8.Location = New System.Drawing.Point(397, 193)
    Me.Combo_Condition8.MaxDropDownItems = 10
    Me.Combo_Condition8.Name = "Combo_Condition8"
    Me.Combo_Condition8.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition8.TabIndex = 54
    '
    'Combo_SelectField8
    '
    Me.Combo_SelectField8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField8.Location = New System.Drawing.Point(232, 193)
    Me.Combo_SelectField8.Name = "Combo_SelectField8"
    Me.Combo_SelectField8.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField8.TabIndex = 53
    '
    'Combo_AndOr7
    '
    Me.Combo_AndOr7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr7.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr7.Location = New System.Drawing.Point(158, 193)
    Me.Combo_AndOr7.Name = "Combo_AndOr7"
    Me.Combo_AndOr7.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr7.TabIndex = 52
    '
    'Combo_SelectValue7
    '
    Me.Combo_SelectValue7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue7.Location = New System.Drawing.Point(471, 166)
    Me.Combo_SelectValue7.Name = "Combo_SelectValue7"
    Me.Combo_SelectValue7.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue7.TabIndex = 48
    '
    'Combo_Condition7
    '
    Me.Combo_Condition7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition7.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition7.Location = New System.Drawing.Point(397, 166)
    Me.Combo_Condition7.MaxDropDownItems = 10
    Me.Combo_Condition7.Name = "Combo_Condition7"
    Me.Combo_Condition7.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition7.TabIndex = 47
    '
    'Combo_SelectField7
    '
    Me.Combo_SelectField7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField7.Location = New System.Drawing.Point(232, 166)
    Me.Combo_SelectField7.Name = "Combo_SelectField7"
    Me.Combo_SelectField7.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField7.TabIndex = 46
    '
    'Combo_AndOr6
    '
    Me.Combo_AndOr6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr6.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr6.Location = New System.Drawing.Point(158, 166)
    Me.Combo_AndOr6.Name = "Combo_AndOr6"
    Me.Combo_AndOr6.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr6.TabIndex = 45
    '
    'Combo_SelectValue6
    '
    Me.Combo_SelectValue6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue6.Location = New System.Drawing.Point(471, 139)
    Me.Combo_SelectValue6.Name = "Combo_SelectValue6"
    Me.Combo_SelectValue6.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue6.TabIndex = 41
    '
    'Combo_Condition6
    '
    Me.Combo_Condition6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition6.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition6.Location = New System.Drawing.Point(397, 139)
    Me.Combo_Condition6.MaxDropDownItems = 10
    Me.Combo_Condition6.Name = "Combo_Condition6"
    Me.Combo_Condition6.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition6.TabIndex = 40
    '
    'Combo_SelectField6
    '
    Me.Combo_SelectField6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField6.Location = New System.Drawing.Point(232, 139)
    Me.Combo_SelectField6.Name = "Combo_SelectField6"
    Me.Combo_SelectField6.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField6.TabIndex = 39
    '
    'Combo_AndOr5
    '
    Me.Combo_AndOr5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr5.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr5.Location = New System.Drawing.Point(158, 139)
    Me.Combo_AndOr5.Name = "Combo_AndOr5"
    Me.Combo_AndOr5.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr5.TabIndex = 38
    '
    'Combo_SelectValue5b
    '
    Me.Combo_SelectValue5b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue5b.Location = New System.Drawing.Point(677, 112)
    Me.Combo_SelectValue5b.Name = "Combo_SelectValue5b"
    Me.Combo_SelectValue5b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue5b.TabIndex = 34
    Me.Combo_SelectValue5b.Visible = False
    '
    'Combo_SelectValue4b
    '
    Me.Combo_SelectValue4b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue4b.Location = New System.Drawing.Point(677, 85)
    Me.Combo_SelectValue4b.Name = "Combo_SelectValue4b"
    Me.Combo_SelectValue4b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue4b.TabIndex = 27
    Me.Combo_SelectValue4b.Visible = False
    '
    'Combo_SelectValue3b
    '
    Me.Combo_SelectValue3b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue3b.Location = New System.Drawing.Point(677, 58)
    Me.Combo_SelectValue3b.Name = "Combo_SelectValue3b"
    Me.Combo_SelectValue3b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue3b.TabIndex = 20
    Me.Combo_SelectValue3b.Visible = False
    '
    'Combo_SelectValue2b
    '
    Me.Combo_SelectValue2b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue2b.Location = New System.Drawing.Point(677, 31)
    Me.Combo_SelectValue2b.Name = "Combo_SelectValue2b"
    Me.Combo_SelectValue2b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue2b.TabIndex = 13
    Me.Combo_SelectValue2b.Visible = False
    '
    'Combo_SelectValue1b
    '
    Me.Combo_SelectValue1b.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue1b.Location = New System.Drawing.Point(677, 4)
    Me.Combo_SelectValue1b.Name = "Combo_SelectValue1b"
    Me.Combo_SelectValue1b.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue1b.TabIndex = 6
    Me.Combo_SelectValue1b.Visible = False
    '
    'Panel5
    '
    Me.Panel5.Controls.Add(Me.Radio_Custom5)
    Me.Panel5.Controls.Add(Me.Radio_Static5)
    Me.Panel5.Location = New System.Drawing.Point(3, 110)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(143, 24)
    Me.Panel5.TabIndex = 29
    '
    'Radio_Custom5
    '
    Me.Radio_Custom5.AutoSize = True
    Me.Radio_Custom5.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom5.Name = "Radio_Custom5"
    Me.Radio_Custom5.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom5.TabIndex = 1
    Me.Radio_Custom5.TabStop = True
    Me.Radio_Custom5.Text = "Custom"
    Me.Radio_Custom5.UseVisualStyleBackColor = True
    '
    'Radio_Static5
    '
    Me.Radio_Static5.AutoSize = True
    Me.Radio_Static5.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static5.Name = "Radio_Static5"
    Me.Radio_Static5.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static5.TabIndex = 0
    Me.Radio_Static5.TabStop = True
    Me.Radio_Static5.Text = "Static"
    Me.Radio_Static5.UseVisualStyleBackColor = True
    '
    'Panel4
    '
    Me.Panel4.Controls.Add(Me.Radio_Custom4)
    Me.Panel4.Controls.Add(Me.Radio_Static4)
    Me.Panel4.Location = New System.Drawing.Point(3, 83)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(143, 24)
    Me.Panel4.TabIndex = 22
    '
    'Radio_Custom4
    '
    Me.Radio_Custom4.AutoSize = True
    Me.Radio_Custom4.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom4.Name = "Radio_Custom4"
    Me.Radio_Custom4.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom4.TabIndex = 1
    Me.Radio_Custom4.TabStop = True
    Me.Radio_Custom4.Text = "Custom"
    Me.Radio_Custom4.UseVisualStyleBackColor = True
    '
    'Radio_Static4
    '
    Me.Radio_Static4.AutoSize = True
    Me.Radio_Static4.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static4.Name = "Radio_Static4"
    Me.Radio_Static4.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static4.TabIndex = 0
    Me.Radio_Static4.TabStop = True
    Me.Radio_Static4.Text = "Static"
    Me.Radio_Static4.UseVisualStyleBackColor = True
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.Radio_Custom3)
    Me.Panel3.Controls.Add(Me.Radio_Static3)
    Me.Panel3.Location = New System.Drawing.Point(3, 56)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(143, 24)
    Me.Panel3.TabIndex = 15
    '
    'Radio_Custom3
    '
    Me.Radio_Custom3.AutoSize = True
    Me.Radio_Custom3.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom3.Name = "Radio_Custom3"
    Me.Radio_Custom3.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom3.TabIndex = 1
    Me.Radio_Custom3.TabStop = True
    Me.Radio_Custom3.Text = "Custom"
    Me.Radio_Custom3.UseVisualStyleBackColor = True
    '
    'Radio_Static3
    '
    Me.Radio_Static3.AutoSize = True
    Me.Radio_Static3.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static3.Name = "Radio_Static3"
    Me.Radio_Static3.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static3.TabIndex = 0
    Me.Radio_Static3.TabStop = True
    Me.Radio_Static3.Text = "Static"
    Me.Radio_Static3.UseVisualStyleBackColor = True
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.Radio_Custom2)
    Me.Panel2.Controls.Add(Me.Radio_Static2)
    Me.Panel2.Location = New System.Drawing.Point(3, 29)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(143, 24)
    Me.Panel2.TabIndex = 8
    '
    'Radio_Custom2
    '
    Me.Radio_Custom2.AutoSize = True
    Me.Radio_Custom2.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom2.Name = "Radio_Custom2"
    Me.Radio_Custom2.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom2.TabIndex = 1
    Me.Radio_Custom2.TabStop = True
    Me.Radio_Custom2.Text = "Custom"
    Me.Radio_Custom2.UseVisualStyleBackColor = True
    '
    'Radio_Static2
    '
    Me.Radio_Static2.AutoSize = True
    Me.Radio_Static2.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static2.Name = "Radio_Static2"
    Me.Radio_Static2.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static2.TabIndex = 0
    Me.Radio_Static2.TabStop = True
    Me.Radio_Static2.Text = "Static"
    Me.Radio_Static2.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Radio_Custom1)
    Me.Panel1.Controls.Add(Me.Radio_Static1)
    Me.Panel1.Location = New System.Drawing.Point(3, 2)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(143, 24)
    Me.Panel1.TabIndex = 2
    '
    'Radio_Custom1
    '
    Me.Radio_Custom1.AutoSize = True
    Me.Radio_Custom1.Location = New System.Drawing.Point(66, 3)
    Me.Radio_Custom1.Name = "Radio_Custom1"
    Me.Radio_Custom1.Size = New System.Drawing.Size(60, 17)
    Me.Radio_Custom1.TabIndex = 1
    Me.Radio_Custom1.Text = "Custom"
    Me.Radio_Custom1.UseVisualStyleBackColor = True
    '
    'Radio_Static1
    '
    Me.Radio_Static1.AutoSize = True
    Me.Radio_Static1.Location = New System.Drawing.Point(4, 3)
    Me.Radio_Static1.Name = "Radio_Static1"
    Me.Radio_Static1.Size = New System.Drawing.Size(52, 17)
    Me.Radio_Static1.TabIndex = 0
    Me.Radio_Static1.Text = "Static"
    Me.Radio_Static1.UseVisualStyleBackColor = True
    '
    'Label_SelectCount5
    '
    Me.Label_SelectCount5.Location = New System.Drawing.Point(880, 113)
    Me.Label_SelectCount5.Name = "Label_SelectCount5"
    Me.Label_SelectCount5.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount5.TabIndex = 35
    Me.Label_SelectCount5.Text = "0"
    '
    'Label_SelectCount4
    '
    Me.Label_SelectCount4.Location = New System.Drawing.Point(880, 86)
    Me.Label_SelectCount4.Name = "Label_SelectCount4"
    Me.Label_SelectCount4.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount4.TabIndex = 28
    Me.Label_SelectCount4.Text = "0"
    '
    'Label_SelectCount3
    '
    Me.Label_SelectCount3.Location = New System.Drawing.Point(880, 59)
    Me.Label_SelectCount3.Name = "Label_SelectCount3"
    Me.Label_SelectCount3.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount3.TabIndex = 21
    Me.Label_SelectCount3.Text = "0"
    '
    'Label_SelectCount2
    '
    Me.Label_SelectCount2.Location = New System.Drawing.Point(880, 32)
    Me.Label_SelectCount2.Name = "Label_SelectCount2"
    Me.Label_SelectCount2.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount2.TabIndex = 14
    Me.Label_SelectCount2.Text = "0"
    '
    'Label_SelectCount1
    '
    Me.Label_SelectCount1.Location = New System.Drawing.Point(880, 5)
    Me.Label_SelectCount1.Name = "Label_SelectCount1"
    Me.Label_SelectCount1.Size = New System.Drawing.Size(51, 18)
    Me.Label_SelectCount1.TabIndex = 7
    Me.Label_SelectCount1.Text = "0"
    '
    'Combo_SelectValue5
    '
    Me.Combo_SelectValue5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue5.Location = New System.Drawing.Point(471, 112)
    Me.Combo_SelectValue5.Name = "Combo_SelectValue5"
    Me.Combo_SelectValue5.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue5.TabIndex = 33
    '
    'Combo_Condition5
    '
    Me.Combo_Condition5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition5.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition5.Location = New System.Drawing.Point(397, 112)
    Me.Combo_Condition5.MaxDropDownItems = 10
    Me.Combo_Condition5.Name = "Combo_Condition5"
    Me.Combo_Condition5.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition5.TabIndex = 32
    '
    'Combo_SelectField5
    '
    Me.Combo_SelectField5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField5.Location = New System.Drawing.Point(232, 112)
    Me.Combo_SelectField5.Name = "Combo_SelectField5"
    Me.Combo_SelectField5.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField5.TabIndex = 31
    '
    'Combo_AndOr4
    '
    Me.Combo_AndOr4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr4.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr4.Location = New System.Drawing.Point(158, 112)
    Me.Combo_AndOr4.Name = "Combo_AndOr4"
    Me.Combo_AndOr4.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr4.TabIndex = 30
    '
    'Combo_SelectValue4
    '
    Me.Combo_SelectValue4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue4.Location = New System.Drawing.Point(471, 85)
    Me.Combo_SelectValue4.Name = "Combo_SelectValue4"
    Me.Combo_SelectValue4.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue4.TabIndex = 26
    '
    'Combo_Condition4
    '
    Me.Combo_Condition4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition4.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition4.Location = New System.Drawing.Point(397, 85)
    Me.Combo_Condition4.MaxDropDownItems = 10
    Me.Combo_Condition4.Name = "Combo_Condition4"
    Me.Combo_Condition4.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition4.TabIndex = 25
    '
    'Combo_SelectField4
    '
    Me.Combo_SelectField4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField4.Location = New System.Drawing.Point(232, 85)
    Me.Combo_SelectField4.Name = "Combo_SelectField4"
    Me.Combo_SelectField4.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField4.TabIndex = 24
    '
    'Combo_AndOr3
    '
    Me.Combo_AndOr3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr3.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr3.Location = New System.Drawing.Point(158, 85)
    Me.Combo_AndOr3.Name = "Combo_AndOr3"
    Me.Combo_AndOr3.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr3.TabIndex = 23
    '
    'Combo_SelectValue3
    '
    Me.Combo_SelectValue3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue3.Location = New System.Drawing.Point(471, 58)
    Me.Combo_SelectValue3.Name = "Combo_SelectValue3"
    Me.Combo_SelectValue3.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue3.TabIndex = 19
    '
    'Combo_Condition3
    '
    Me.Combo_Condition3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition3.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition3.Location = New System.Drawing.Point(397, 58)
    Me.Combo_Condition3.MaxDropDownItems = 10
    Me.Combo_Condition3.Name = "Combo_Condition3"
    Me.Combo_Condition3.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition3.TabIndex = 18
    '
    'Combo_SelectField3
    '
    Me.Combo_SelectField3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField3.Location = New System.Drawing.Point(232, 58)
    Me.Combo_SelectField3.Name = "Combo_SelectField3"
    Me.Combo_SelectField3.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField3.TabIndex = 17
    '
    'Combo_AndOr2
    '
    Me.Combo_AndOr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr2.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr2.Location = New System.Drawing.Point(158, 58)
    Me.Combo_AndOr2.Name = "Combo_AndOr2"
    Me.Combo_AndOr2.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr2.TabIndex = 16
    '
    'Combo_SelectValue2
    '
    Me.Combo_SelectValue2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue2.Location = New System.Drawing.Point(471, 31)
    Me.Combo_SelectValue2.Name = "Combo_SelectValue2"
    Me.Combo_SelectValue2.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue2.TabIndex = 12
    '
    'Combo_Condition2
    '
    Me.Combo_Condition2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition2.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition2.Location = New System.Drawing.Point(397, 31)
    Me.Combo_Condition2.MaxDropDownItems = 10
    Me.Combo_Condition2.Name = "Combo_Condition2"
    Me.Combo_Condition2.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition2.TabIndex = 11
    '
    'Combo_SelectField2
    '
    Me.Combo_SelectField2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField2.Location = New System.Drawing.Point(232, 31)
    Me.Combo_SelectField2.Name = "Combo_SelectField2"
    Me.Combo_SelectField2.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField2.TabIndex = 10
    '
    'Combo_AndOr1
    '
    Me.Combo_AndOr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr1.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr1.Location = New System.Drawing.Point(158, 31)
    Me.Combo_AndOr1.Name = "Combo_AndOr1"
    Me.Combo_AndOr1.Size = New System.Drawing.Size(68, 21)
    Me.Combo_AndOr1.TabIndex = 9
    '
    'Combo_SelectValue1
    '
    Me.Combo_SelectValue1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectValue1.Location = New System.Drawing.Point(471, 4)
    Me.Combo_SelectValue1.Name = "Combo_SelectValue1"
    Me.Combo_SelectValue1.Size = New System.Drawing.Size(200, 21)
    Me.Combo_SelectValue1.TabIndex = 5
    '
    'Combo_Condition1
    '
    Me.Combo_Condition1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Condition1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Condition1.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like", "Between (Inclusive)"})
    Me.Combo_Condition1.Location = New System.Drawing.Point(397, 4)
    Me.Combo_Condition1.MaxDropDownItems = 10
    Me.Combo_Condition1.Name = "Combo_Condition1"
    Me.Combo_Condition1.Size = New System.Drawing.Size(68, 21)
    Me.Combo_Condition1.TabIndex = 4
    '
    'Combo_SelectField1
    '
    Me.Combo_SelectField1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectField1.Location = New System.Drawing.Point(232, 4)
    Me.Combo_SelectField1.Name = "Combo_SelectField1"
    Me.Combo_SelectField1.Size = New System.Drawing.Size(159, 21)
    Me.Combo_SelectField1.TabIndex = 3
    '
    'Btn_ShowCarts
    '
    Me.Btn_ShowCarts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Btn_ShowCarts.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Btn_ShowCarts.Location = New System.Drawing.Point(968, 32)
    Me.Btn_ShowCarts.Name = "Btn_ShowCarts"
    Me.Btn_ShowCarts.Size = New System.Drawing.Size(96, 23)
    Me.Btn_ShowCarts.TabIndex = 111
    Me.Btn_ShowCarts.Text = "Hide Charts"
    '
    'Label_SearchFieldType
    '
    Me.Label_SearchFieldType.Location = New System.Drawing.Point(20, 37)
    Me.Label_SearchFieldType.Name = "Label_SearchFieldType"
    Me.Label_SearchFieldType.Size = New System.Drawing.Size(112, 18)
    Me.Label_SearchFieldType.TabIndex = 0
    Me.Label_SearchFieldType.Text = "Search Field Type :"
    '
    'Label_InformationSelectWhere
    '
    Me.Label_InformationSelectWhere.AutoSize = True
    Me.Label_InformationSelectWhere.Location = New System.Drawing.Point(158, 37)
    Me.Label_InformationSelectWhere.Name = "Label_InformationSelectWhere"
    Me.Label_InformationSelectWhere.Size = New System.Drawing.Size(133, 13)
    Me.Label_InformationSelectWhere.TabIndex = 1
    Me.Label_InformationSelectWhere.Text = "Information Select Where :"
    '
    'Grid_Results
    '
    Me.Grid_Results.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Results.ColumnInfo = resources.GetString("Grid_Results.ColumnInfo")
    Me.Grid_Results.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Results.Location = New System.Drawing.Point(1, 1)
    Me.Grid_Results.Name = "Grid_Results"
    Me.Grid_Results.Rows.DefaultSize = 17
    Me.Grid_Results.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Results.Size = New System.Drawing.Size(752, 452)
    Me.Grid_Results.TabIndex = 0
    '
    'StatusStrip_Search
    '
    Me.StatusStrip_Search.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SplitButton_Save, Me.SplitButton_Cancel, Me.ToolStripProgressBar1, Me.StatusLabel_Search})
    Me.StatusStrip_Search.Location = New System.Drawing.Point(0, 697)
    Me.StatusStrip_Search.Name = "StatusStrip_Search"
    Me.StatusStrip_Search.Size = New System.Drawing.Size(1070, 22)
    Me.StatusStrip_Search.TabIndex = 1
    '
    'SplitButton_Save
    '
    Me.SplitButton_Save.BackColor = System.Drawing.SystemColors.Control
    Me.SplitButton_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.SplitButton_Save.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold)
    Me.SplitButton_Save.ForeColor = System.Drawing.Color.MediumBlue
    Me.SplitButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.SplitButton_Save.Name = "SplitButton_Save"
    Me.SplitButton_Save.Size = New System.Drawing.Size(128, 22)
    Me.SplitButton_Save.Text = "Save Changes"
    Me.SplitButton_Save.Visible = False
    '
    'SplitButton_Cancel
    '
    Me.SplitButton_Cancel.BackColor = System.Drawing.SystemColors.Control
    Me.SplitButton_Cancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.SplitButton_Cancel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold)
    Me.SplitButton_Cancel.ForeColor = System.Drawing.Color.Crimson
    Me.SplitButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.SplitButton_Cancel.Name = "SplitButton_Cancel"
    Me.SplitButton_Cancel.Size = New System.Drawing.Size(141, 22)
    Me.SplitButton_Cancel.Text = "Cancel Changes"
    Me.SplitButton_Cancel.Visible = False
    '
    'ToolStripProgressBar1
    '
    Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
    Me.ToolStripProgressBar1.Size = New System.Drawing.Size(200, 18)
    Me.ToolStripProgressBar1.Visible = False
    '
    'StatusLabel_Search
    '
    Me.StatusLabel_Search.Name = "StatusLabel_Search"
    Me.StatusLabel_Search.Size = New System.Drawing.Size(22, 17)
    Me.StatusLabel_Search.Text = "     "
    '
    'Split_Results
    '
    Me.Split_Results.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_Results.Location = New System.Drawing.Point(3, 3)
    Me.Split_Results.Name = "Split_Results"
    '
    'Split_Results.Panel1
    '
    Me.Split_Results.Panel1.Controls.Add(Me.Grid_Results)
    '
    'Split_Results.Panel2
    '
    Me.Split_Results.Panel2.Controls.Add(Me.Label_ChartStock)
    Me.Split_Results.Panel2.Controls.Add(Me.Chart_MonthlyReturns)
    Me.Split_Results.Panel2.Controls.Add(Me.Chart_VAMI)
    Me.Split_Results.Size = New System.Drawing.Size(1063, 455)
    Me.Split_Results.SplitterDistance = 755
    Me.Split_Results.TabIndex = 17
    '
    'Label_ChartStock
    '
    Me.Label_ChartStock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ChartStock.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_ChartStock.Location = New System.Drawing.Point(6, 4)
    Me.Label_ChartStock.Name = "Label_ChartStock"
    Me.Label_ChartStock.Size = New System.Drawing.Size(292, 15)
    Me.Label_ChartStock.TabIndex = 0
    '
    'Chart_MonthlyReturns
    '
    Me.Chart_MonthlyReturns.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_MonthlyReturns.BackColor = System.Drawing.Color.Azure
    Me.Chart_MonthlyReturns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_MonthlyReturns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_MonthlyReturns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea1.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisX.LabelStyle.Format = "Y"
    ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisY.LabelStyle.Format = "P0"
    ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.StartFromZero = False
    ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.BackColor = System.Drawing.Color.Transparent
    ChartArea1.BorderColor = System.Drawing.Color.DimGray
    ChartArea1.Name = "Default"
    Me.Chart_MonthlyReturns.ChartAreas.Add(ChartArea1)
    Legend1.BackColor = System.Drawing.Color.Transparent
    Legend1.BorderColor = System.Drawing.Color.Transparent
    Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend1.DockToChartArea = "Default"
    Legend1.Enabled = False
    Legend1.Name = "Default"
    Me.Chart_MonthlyReturns.Legends.Add(Legend1)
    Me.Chart_MonthlyReturns.Location = New System.Drawing.Point(3, 239)
    Me.Chart_MonthlyReturns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_MonthlyReturns.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_MonthlyReturns.Name = "Chart_MonthlyReturns"
    Me.Chart_MonthlyReturns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series1.BorderWidth = 2
    Series1.ChartType = "Line"
    Series1.CustomAttributes = "LabelStyle=Top"
    Series1.Name = "Series1"
    Series1.ShadowOffset = 1
    Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series2.BorderWidth = 2
    Series2.ChartType = "Line"
    Series2.CustomAttributes = "LabelStyle=Top"
    Series2.Name = "Series2"
    Series2.ShadowOffset = 1
    Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_MonthlyReturns.Series.Add(Series1)
    Me.Chart_MonthlyReturns.Series.Add(Series2)
    Me.Chart_MonthlyReturns.Size = New System.Drawing.Size(296, 200)
    Me.Chart_MonthlyReturns.TabIndex = 2
    Me.Chart_MonthlyReturns.Text = "Chart2"
    Title1.Name = "Title1"
    Title1.Text = "Rolling 12 Mth Return"
    Me.Chart_MonthlyReturns.Titles.Add(Title1)
    '
    'Chart_VAMI
    '
    Me.Chart_VAMI.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_VAMI.BackColor = System.Drawing.Color.Azure
    Me.Chart_VAMI.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_VAMI.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_VAMI.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_VAMI.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_VAMI.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea2.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisX.LabelStyle.Format = "Y"
    ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisY.LabelStyle.Format = "N0"
    ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.StartFromZero = False
    ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.BackColor = System.Drawing.Color.Transparent
    ChartArea2.BorderColor = System.Drawing.Color.DimGray
    ChartArea2.Name = "Default"
    Me.Chart_VAMI.ChartAreas.Add(ChartArea2)
    Legend2.BackColor = System.Drawing.Color.Transparent
    Legend2.BorderColor = System.Drawing.Color.Transparent
    Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend2.DockToChartArea = "Default"
    Legend2.Enabled = False
    Legend2.Name = "Default"
    Me.Chart_VAMI.Legends.Add(Legend2)
    Me.Chart_VAMI.Location = New System.Drawing.Point(4, 24)
    Me.Chart_VAMI.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_VAMI.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_VAMI.Name = "Chart_VAMI"
    Me.Chart_VAMI.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series3.BorderWidth = 2
    Series3.ChartType = "Line"
    Series3.CustomAttributes = "LabelStyle=Top"
    Series3.Name = "Series1"
    Series3.ShadowOffset = 1
    Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series4.BorderWidth = 2
    Series4.ChartType = "Line"
    Series4.CustomAttributes = "LabelStyle=Top"
    Series4.Name = "Series2"
    Series4.ShadowOffset = 1
    Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_VAMI.Series.Add(Series3)
    Me.Chart_VAMI.Series.Add(Series4)
    Me.Chart_VAMI.Size = New System.Drawing.Size(296, 200)
    Me.Chart_VAMI.TabIndex = 1
    Me.Chart_VAMI.Text = "Chart2"
    Title2.Name = "Title1"
    Title2.Text = "VAMI"
    Me.Chart_VAMI.Titles.Add(Title2)
    '
    'Split_FundSearch
    '
    Me.Split_FundSearch.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_FundSearch.Location = New System.Drawing.Point(1, 58)
    Me.Split_FundSearch.Name = "Split_FundSearch"
    Me.Split_FundSearch.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'Split_FundSearch.Panel1
    '
    Me.Split_FundSearch.Panel1.Controls.Add(Me.Panel_SearchInstruments)
    '
    'Split_FundSearch.Panel2
    '
    Me.Split_FundSearch.Panel2.Controls.Add(Me.Split_Results)
    Me.Split_FundSearch.Size = New System.Drawing.Size(1069, 637)
    Me.Split_FundSearch.SplitterDistance = 172
    Me.Split_FundSearch.TabIndex = 18
    '
    'frmFundSearch
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1070, 719)
    Me.Controls.Add(Me.Split_FundSearch)
    Me.Controls.Add(Me.Label_InformationSelectWhere)
    Me.Controls.Add(Me.StatusStrip_Search)
    Me.Controls.Add(Me.Label_SearchFieldType)
    Me.Controls.Add(Me.Btn_ShowCarts)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(930, 600)
    Me.Name = "frmFundSearch"
    Me.Text = "Fund Search"
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    Me.Panel_SearchInstruments.ResumeLayout(False)
    Me.Panel21.ResumeLayout(False)
    Me.Panel21.PerformLayout()
    Me.Panel20.ResumeLayout(False)
    Me.Panel20.PerformLayout()
    Me.Panel19.ResumeLayout(False)
    Me.Panel19.PerformLayout()
    Me.Panel18.ResumeLayout(False)
    Me.Panel18.PerformLayout()
    Me.Panel17.ResumeLayout(False)
    Me.Panel17.PerformLayout()
    Me.Panel16.ResumeLayout(False)
    Me.Panel16.PerformLayout()
    Me.Panel15.ResumeLayout(False)
    Me.Panel15.PerformLayout()
    Me.Panel14.ResumeLayout(False)
    Me.Panel14.PerformLayout()
    Me.Panel13.ResumeLayout(False)
    Me.Panel13.PerformLayout()
    Me.Panel12.ResumeLayout(False)
    Me.Panel12.PerformLayout()
    Me.Panel11.ResumeLayout(False)
    Me.Panel11.PerformLayout()
    Me.Panel10.ResumeLayout(False)
    Me.Panel10.PerformLayout()
    Me.Panel9.ResumeLayout(False)
    Me.Panel9.PerformLayout()
    Me.Panel8.ResumeLayout(False)
    Me.Panel8.PerformLayout()
    Me.Panel7.ResumeLayout(False)
    Me.Panel7.PerformLayout()
    Me.Panel6.ResumeLayout(False)
    Me.Panel6.PerformLayout()
    Me.Panel5.ResumeLayout(False)
    Me.Panel5.PerformLayout()
    Me.Panel4.ResumeLayout(False)
    Me.Panel4.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    CType(Me.Grid_Results, System.ComponentModel.ISupportInitialize).EndInit()
    Me.StatusStrip_Search.ResumeLayout(False)
    Me.StatusStrip_Search.PerformLayout()
    Me.Split_Results.Panel1.ResumeLayout(False)
    Me.Split_Results.Panel2.ResumeLayout(False)
    Me.Split_Results.ResumeLayout(False)
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Split_FundSearch.Panel1.ResumeLayout(False)
    Me.Split_FundSearch.Panel2.ResumeLayout(False)
    Me.Split_FundSearch.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Private Classes"

	Private Class SelectionItemClass
		' *****************************************************************************************
		' Private class designed to hold the selection details and update status of a single
		' Fund Selection criteria.
		' This class also holds an integer array of Pertrac IDs representing the results of this
		' Search. The intention of this is that each search selection is only recalculated when
		' it is changed and the Net selection result is a simple boolean combination of values
		' in the PertradID arrays.
		' If the query item is Static and is against database fields defined as containing Numeric values
		' then this class may also maintain a dataset of numeric converted values for this Static field.
		' This means that if the selection criteria are altered, a new results set may be generated without
		' refering to the database and performing the numeric conversion of each field value.
		'
		' As the selection controls are altered, they will update the relevant values held in the
		' associated 'SelectionItemClass' object. That object will ensure the various data parameters
		' remain consistent, will report on whether the selection criteria are valid and will 
		' flag if an update is required.
		' This Update flag is designed to operate in conjunction with the regular update polling 
		' mechanism implemented on this Genoa Form.
		' The idea is that the funds will not be re-queried after each key stroke, the update process 
		' should let the user finish typing the selection criteria before performing the search.
		' This functionality is solely designed to improve performance.
		'
		' *****************************************************************************************

		Public Const UPDATE_DELAY_Secs As Double = 0.75

		Private _IsStaticField As Boolean
		Private _IsCustomField As Boolean
		Private _SelectFieldID As String
		Private _ConditionOperand As String
		Private _SelectValue As String
		Private _SelectValue2 As String
		Private _NumericValuesTable As DataTable

		Private _ID_Array() As Integer

		Private _IsValidSelection As Boolean
		Private _UpdateFlag As Boolean
		Private _FlagTime As Date

		Public Property IsStaticField() As Boolean
			' *****************************************************************************************
			' Static vs Custom field.
			'
			' Altering this value will reset the Numeric values array (_NumericValuesTable).
			' *****************************************************************************************

			Get
				Return _IsStaticField
			End Get
			Set(ByVal value As Boolean)
				If (value) Then
					If (_IsStaticField <> value) Then
						UpdateFlag = True
					End If

					If (_NumericValuesTable IsNot Nothing) Then
						_NumericValuesTable = Nothing
					End If

					_IsStaticField = value
					_IsCustomField = Not _IsStaticField
					IsValidSelection = CheckValidSelection()
				End If
			End Set
		End Property

		Public Property IsCustomField() As Boolean
			' *****************************************************************************************
			' Static vs Custom field.
			'
			' Altering this value will reset the Numeric values array (_NumericValuesTable).
			' *****************************************************************************************

			Get
				Return _IsCustomField
			End Get
			Set(ByVal value As Boolean)
				If (value) Then
					If (_IsCustomField <> value) Then
						UpdateFlag = True
					End If

					If (_NumericValuesTable IsNot Nothing) Then
						_NumericValuesTable = Nothing
					End If

					_IsCustomField = value
					_IsStaticField = Not _IsCustomField
					IsValidSelection = CheckValidSelection()
				End If
			End Set
		End Property

		Public Property SelectFieldID() As String
			' *****************************************************************************************
			' Sets / Returns the Select Field ID.
			'
			' For Static fields this is the text representation of the 'PertracInformationFields' enumeration.
			' For Custom Fields this is the numeric representation of the Custom Field ID.
			' *****************************************************************************************

			Get
				Return _SelectFieldID
			End Get
			Set(ByVal value As String)
				If Not (_SelectFieldID = value) Then
					_SelectFieldID = value

					If (_NumericValuesTable IsNot Nothing) Then
						_NumericValuesTable = Nothing
					End If

					UpdateFlag = True

					IsValidSelection = CheckValidSelection()
				End If

			End Set
		End Property

		Public Property ConditionOperand() As String
			' *****************************************************************************************
			' String representation of the condition operand. e.g. '<', '>', '=', 'LIKE' etc...
			'
			' *****************************************************************************************

			Get
				Return _ConditionOperand
			End Get
			Set(ByVal value As String)
				value = value.Trim.ToUpper

				If Not (_ConditionOperand = value) Then
					_ConditionOperand = value
					UpdateFlag = True

					IsValidSelection = CheckValidSelection()
				End If

			End Set
		End Property

		Public Property NumericValuesTable() As DataTable
			' *****************************************************************************************
			' For selection against Static fields defined as being a Numeric, the class may cache a 
			' derived dataset of numeric values against which to query. This is done solely for 
			' performance reasons.
			' *****************************************************************************************

			Get
				Return _NumericValuesTable
			End Get
			Set(ByVal value As DataTable)
				If (_NumericValuesTable IsNot Nothing) AndAlso (value IsNot Nothing) Then
					SyncLock _NumericValuesTable
						_NumericValuesTable = value
					End SyncLock
				Else
					_NumericValuesTable = value
				End If
			End Set
		End Property

		Public Property SelectValue() As String
			' *****************************************************************************************
			' Fund Selection Criteia.
			'
			' *****************************************************************************************

			Get
				Return _SelectValue
			End Get
			Set(ByVal value As String)
				If Not (_SelectValue = value) Then
					_SelectValue = value
					UpdateFlag = True

					IsValidSelection = CheckValidSelection()
				End If

			End Set
		End Property

		Public Property SelectValue2() As String
			' *****************************************************************************************
			' Fund Selection Criteia.
			'
			' *****************************************************************************************

			Get
				Return _SelectValue2
			End Get
			Set(ByVal value As String)
				If Not (_SelectValue2 = value) Then
					_SelectValue2 = value
					UpdateFlag = True

					IsValidSelection = CheckValidSelection()
				End If

			End Set
		End Property

		Public Property ID_Array() As Integer()
			' *****************************************************************************************
			' Array of integers representing the result (PertracIDs) set of this selection criteria object.
			'
			' *****************************************************************************************

			Get
				Return _ID_Array
			End Get
			Set(ByVal value() As Integer)
				_ID_Array = value
			End Set
		End Property

		Private WriteOnly Property IsValidSelection() As Boolean
			' *****************************************************************************************
			' This class performs simple validation of the selection parameters. Invalid or incomplete 
			' selection parameters are flagged as such, reducing the amount of invalid data searching.
			'
			' *****************************************************************************************

			Set(ByVal value As Boolean)
				If (value = False) Then
					_ID_Array = Nothing
				End If

				_IsValidSelection = value
			End Set
		End Property

		Public ReadOnly Property SelectionIsValid() As Boolean
			' *****************************************************************************************
			' This class performs simple validation of the selection parameters. Invalid or incomplete 
			' selection parameters are flagged as such, reducing the amount of invalid data searching.
			'
			' *****************************************************************************************

			Get
				Return _IsValidSelection
			End Get
		End Property

		Public Property UpdateFlag() As Boolean
			' *****************************************************************************************
			' The Genoa fund search update process will poll the search criteria periodically to determine
			' if Fund Searches are due to be performed.
			' This flag indicates such. However there is a defined delay between the last parameter change
			' and this flag turning true which is designed to limit the Search updating while parameters are 
			' being quickly altered.
			'
			' *****************************************************************************************

			Get
				If (_UpdateFlag) AndAlso (SelectionIsValid) Then
					If (Now() - _FlagTime).TotalSeconds >= UPDATE_DELAY_Secs Then
						Return True
					Else
						Return False
					End If
				Else
					Return False
				End If
			End Get
			Set(ByVal value As Boolean)
				_UpdateFlag = value

				If (_UpdateFlag) Then
					_FlagTime = Now()
				End If
			End Set
		End Property

		Public Sub New()
			' *****************************************************************************************
			'
			'
			' *****************************************************************************************

			Reset()

		End Sub

		Public Sub Reset()
			' *****************************************************************************************
			'
			'
			' *****************************************************************************************

			_IsStaticField = False
			_IsCustomField = False
			_SelectFieldID = 0
			_ConditionOperand = ""
			_SelectValue = ""
			_SelectValue2 = ""
			_NumericValuesTable = Nothing
			_ID_Array = Nothing
			_IsValidSelection = False
			_UpdateFlag = False
			_FlagTime = Now()

		End Sub

		Private Function CheckValidSelection() As Boolean
			' *****************************************************************************************
			' Simple check for selection validity.
			'
			' *****************************************************************************************

			' Check Boolean Flags
			If Not (_IsStaticField Xor _IsCustomField) Then
				Return False
			End If

			' Field ID 

			If (_SelectFieldID.Length <= 0) Then
				Return False
			End If

			If (_IsCustomField) Then
				If (IsNumeric(_SelectFieldID) = False) Then
					Return False
				ElseIf (CInt(_SelectFieldID) <= 0) Then
					Return False
				End If
			End If

			' Condition

			Select Case _ConditionOperand
				Case "=", "<>", "<", "<=", ">", ">=", "LIKE", "NOT LIKE"


				Case Else
					If (_ConditionOperand.StartsWith("BETWEEN") = False) Then
						Return False
					End If
			End Select

			Return True

		End Function

	End Class

#End Region

  Public Event SearchUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer) Implements IRenaissanceSearchUpdateEvent.RenaissanceSearchUpdateEvent
  Public Event StatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer) Implements IRenaissanceSearchUpdateEvent.RenaissanceGroupStatsUpdateEvent
  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain
	Private WithEvents PaintTimer As System.Windows.Forms.Timer

	Dim StaticFieldsMenu As ToolStripMenuItem = Nothing
  Dim CustomFieldsMenu As ToolStripMenuItem = Nothing
  Dim DrilldownMenu As ToolStripMenuItem = Nothing

	Dim SaveCurrentSearchMenu As ToolStripMenuItem = Nothing
	Dim DeleteCurrentSearchMenu As ToolStripMenuItem = Nothing
	Dim CurrentSavedSearchID As Integer
  Private CompoundArray(-1) As Integer

  Private CustomGroupID_CompoundArray As Integer = -1

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Form Status Flags

	Private FormIsValid As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private In_PaintTimer_Tick As Boolean
	Private _InUse As Boolean
	Private SelectControlChanged As Boolean
	Private FormChanged As Boolean
	Private CustomFieldsChanged As String

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' 'Select' ID Arrays

	Private Const SELECTION_ITEM_COUNT As Integer = 21
	Private SelectionItems(SELECTION_ITEM_COUNT - 1) As SelectionItemClass
	Private LastIsValidStatus(SELECTION_ITEM_COUNT - 1) As Boolean	' Comparison Array of 'IsValid' flags, Can then tell if the Valid flag has changed.

	' Integer Array - Indicates which Static Fields are selected on the Static Fields menu, 
	' thus determines which fields are shown on the Results grid.
	Private Grid_StaticFields_Display(PertracInformationFields.MaxValue) As Boolean

	' Dictionary of Integer keys and boolean values relating to Custom Field IDs. A 'True' value
	' indicates which Custom Fieldsare to appear on the results grid.
	Private Grid_CustomFields_Display_Dictionary As New Dictionary(Of Integer, Boolean)
	Friend CustomFieldDataCache As CustomFieldDataCacheClass	' New

	' Arbitrary limit of instruments to show on the Results grid.
	Private Const MAX_GRID_DISPLAY_ROWS As Integer = 500

	Private GridContextMenuStrip As ContextMenuStrip
	Private GridClickMouseRow As Integer = 0
	Private GridClickMouseCol As Integer = 0

  Private UseFixedIdArray As Boolean = False

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

  Public ReadOnly Property CompoundGroups() As System.Collections.Generic.SortedDictionary(Of Integer, Integer()) Implements RenaissanceGenoaGroupsGlobals.IRenaissanceSearchUpdateEvent.CompoundGroups
    Get
      Return Nothing
    End Get

  End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value
    End Set
  End Property

#End Region


#Region " New(), Field Menu Functions"

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		Try
			Me.Cursor = Cursors.WaitCursor

			_MainForm = pMainForm
			AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

      _DefaultStatsDatePeriod = MainForm.DefaultStatsDatePeriod

			_FormOpenFailed = False
			_InUse = True
      UseFixedIdArray = False

			' ******************************************************
			' Set pointer to Custom Field Data Cache
			' ******************************************************

			CustomFieldDataCache = MainForm.CustomFieldDataCache

			' ******************************************************
			' Form Specific Settings :
			' ******************************************************

			' Form Permissioning :-

			THIS_FORM_PermissionArea = Me.Name
			THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

			' 'This' form ID

			THIS_FORM_FormID = GenoaFormID.frmFundSearch

			' Format Event Handlers for form controls

			AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

			AddHandler Combo_SelectField1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectField1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectField1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectField1.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			AddHandler Combo_Condition1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_Condition1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_Condition1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			AddHandler Combo_SelectValue1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectValue1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectValue1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectValue1.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			AddHandler Combo_SelectField2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectField2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectField2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectField2.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			AddHandler Combo_Condition2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_Condition2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_Condition2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			AddHandler Combo_SelectValue2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectValue2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectValue2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectValue2.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			AddHandler Combo_SelectField3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectField3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectField3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectField3.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			AddHandler Combo_Condition3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_Condition3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_Condition3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			AddHandler Combo_SelectValue3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectValue3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectValue3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectValue3.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			AddHandler Combo_SelectField4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectField4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectField4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectField4.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			AddHandler Combo_Condition4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_Condition4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_Condition4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			AddHandler Combo_SelectValue4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectValue4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectValue4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectValue4.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			AddHandler Combo_SelectField5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectField5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectField5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectField5.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			AddHandler Combo_Condition5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_Condition5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_Condition5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			AddHandler Combo_SelectValue5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectValue5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectValue5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectValue5.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			' ----

			AddHandler Combo_AndOr1.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr2.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr3.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr4.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr5.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr6.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr7.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr8.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr9.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr10.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr11.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr12.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr13.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr14.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr15.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr16.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr17.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr18.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr19.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
			AddHandler Combo_AndOr20.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged

			AddHandler Radio_Static1.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static2.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static3.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static4.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static5.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static6.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static7.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static8.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static9.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static10.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static11.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static12.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static13.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static14.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static15.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static16.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static17.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static18.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static19.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static20.CheckedChanged, AddressOf Radio_Static_CheckedChanged
			AddHandler Radio_Static21.CheckedChanged, AddressOf Radio_Static_CheckedChanged

			AddHandler Radio_Custom1.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom2.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom3.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom4.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom5.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom6.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom7.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom8.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom9.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom10.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom11.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom12.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom13.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom14.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom15.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom16.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom17.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom18.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom19.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom20.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
			AddHandler Radio_Custom21.CheckedChanged, AddressOf Radio_Custom_CheckedChanged

			AddHandler Combo_SelectField1.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField2.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField3.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField4.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField5.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField6.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField7.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField8.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField9.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField10.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField11.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField12.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField13.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField14.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField15.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField16.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField17.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField18.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField19.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField20.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
			AddHandler Combo_SelectField21.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged

			AddHandler Combo_Condition1.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition2.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition3.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition4.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition5.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition6.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition7.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition8.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition9.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition10.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition11.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition12.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition13.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition14.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition15.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition16.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition17.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition18.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition19.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition20.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
			AddHandler Combo_Condition21.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged

			AddHandler Combo_SelectValue1.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue2.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue3.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue4.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue5.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue6.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue7.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue8.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue9.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue10.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue11.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue12.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue13.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue14.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue15.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue16.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue17.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue18.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue19.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue20.TextChanged, AddressOf Combo_SelectValue_TextChanged
			AddHandler Combo_SelectValue21.TextChanged, AddressOf Combo_SelectValue_TextChanged

			AddHandler Combo_SelectValue1b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue2b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue3b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue4b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue5b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue6b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue7b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue8b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue9b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue10b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue11b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue12b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue13b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue14b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue15b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue16b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue17b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue18b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue19b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue20b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
			AddHandler Combo_SelectValue21b.TextChanged, AddressOf Combo_SelectValueB_TextChanged

			' Form Control Changed events

			' Set up the ToolTip
			MainForm.SetFormToolTip(Me, FormTooltip)

			SelectControlChanged = False
			Try
				Dim Counter As Integer

				For Counter = LBound(SelectionItems) To UBound(SelectionItems)
					SelectionItems(Counter) = New SelectionItemClass
				Next
			Catch ex As Exception
			End Try
			'SelectionItems(0) = New SelectionItemClass
			'SelectionItems(1) = New SelectionItemClass
			'SelectionItems(2) = New SelectionItemClass
			'SelectionItems(3) = New SelectionItemClass
			'SelectionItems(4) = New SelectionItemClass

			' ******************************************************
			' End Form Specific.
			' ******************************************************

			Try
				InPaint = True

				' Initialise Condition and AndOR Combos.

				Dim ControlCounter As Integer
				Dim thisComboCondition As ComboBox
				Dim thisComboAndOr As ComboBox

				For ControlCounter = 1 To SELECTION_ITEM_COUNT
					Try
						thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & ControlCounter.ToString), ComboBox)
						thisComboAndOr = CType(Panel_SearchInstruments.Controls("Combo_AndOr" & ControlCounter.ToString), ComboBox)

						If (thisComboCondition IsNot Nothing) Then
							thisComboCondition.SelectedIndex = 0
						End If

						If (thisComboAndOr IsNot Nothing) Then ' only goes 1 to 20.
							thisComboAndOr.SelectedIndex = 0
						End If
					Catch ex As Exception
					End Try
				Next

				GridContextMenuStrip = New ContextMenuStrip
				AddHandler GridContextMenuStrip.Opening, AddressOf cms_Opening
				Grid_Results.ContextMenuStrip = GridContextMenuStrip
				Grid_Results.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

			Catch ex As Exception
			Finally
				InPaint = False
			End Try

			StaticFieldsMenu = SetStaticFieldSelectMenu(RootMenu)
			CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)
      DrilldownMenu = SetDrillDownMenu(RootMenu)

		Catch ex As Exception
		Finally
			Me.Cursor = Cursors.Default
		End Try

	End Sub

  Public Sub New(ByVal pMainForm As GenoaMain, ByVal FixedIdArray() As Integer)
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Me.New(pMainForm)

    InitialiseForFixedArray(FixedIdArray)

  End Sub

  Private Function SetStaticFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the Static Fields Menu.
    '
    ' Add an item for each field in the PertracInformationFields enumeration, when selected the item
    ' will show or hide the associated field on the grid.
    ' ***************************************************************************************

    Dim FieldNames(-1) As String
    Dim FieldCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Static &Fields")
    FieldsMenu.Name = "Menu_StaticFields"

    Dim newMenuItem As ToolStripMenuItem

    FieldNames = System.Enum.GetNames(GetType(RenaissanceGlobals.PertracInformationFields))
    Array.Sort(FieldNames)

    For FieldCount = 0 To (FieldNames.Length - 1)
      If (FieldNames(FieldCount) <> "MaxValue") Then
        newMenuItem = FieldsMenu.DropDownItems.Add(FieldNames(FieldCount), Nothing, AddressOf Me.ShowGridField)

        newMenuItem.Name = "Menu_StaticField_" & FieldNames(FieldCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  Private Function SetCustomFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the tblPertracCustomFields table, when selected the item
    ' will show or hide the associated field on the grid.
    ' ***************************************************************************************

    Dim FieldsTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
    Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
    Dim FieldsRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
    Dim FieldNames(-1) As String
    Dim RowCount As Integer

    FieldsTable = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0)
    SelectedRows = FieldsTable.Select("True", "FieldName")
    Grid_CustomFields_Display_Dictionary.Clear()

    Dim FieldsMenu As New ToolStripMenuItem("Custom &Fields")
    FieldsMenu.Name = "Menu_CustomFields"

    Dim newMenuItem As ToolStripMenuItem

    ReDim FieldNames(SelectedRows.Length - 1)
    For RowCount = 0 To (SelectedRows.Length - 1)
      FieldsRow = SelectedRows(RowCount)

      FieldNames(RowCount) = FieldsRow.FieldName

      newMenuItem = FieldsMenu.DropDownItems.Add(FieldsRow.FieldName, Nothing, AddressOf Me.ShowGridField)
      newMenuItem.Name = "Menu_CustomField_" & FieldsRow.FieldID.ToString

      Grid_CustomFields_Display_Dictionary.Add(FieldsRow.FieldID, False)
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  Private Function SetDrillDownMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim DrillDownMenu As New ToolStripMenuItem("Drill Down by field")

    Try

      Dim FieldNames(-1) As String
      Dim FieldCount As Integer

      DrillDownMenu.Name = "Menu_StaticFields"

      Dim newMenuItem As ToolStripMenuItem

      FieldNames = System.Enum.GetNames(GetType(RenaissanceGlobals.PertracInformationFields))
      Array.Sort(FieldNames)

      For FieldCount = 0 To (FieldNames.Length - 1)
        If (FieldNames(FieldCount) <> "MaxValue") Then
          newMenuItem = DrillDownMenu.DropDownItems.Add(FieldNames(FieldCount), Nothing, AddressOf Me.DrilldownGridField)

          newMenuItem.Name = "MenuDrilldown_StaticField_" & FieldNames(FieldCount)
          newMenuItem.Tag = newMenuItem.Name
        End If
      Next

      DrillDownMenu.DropDownItems.Add(New ToolStripSeparator)

      Dim FieldsTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
      Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
      Dim FieldsRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
      Dim RowCount As Integer

      FieldsTable = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0)
      SelectedRows = FieldsTable.Select("True", "FieldName")

      For RowCount = 0 To (SelectedRows.Length - 1)
        FieldsRow = SelectedRows(RowCount)

        newMenuItem = DrillDownMenu.DropDownItems.Add(FieldsRow.FieldName, Nothing, AddressOf Me.DrilldownGridField)
        newMenuItem.Name = "MenuDrilldown_CustomField_" & FieldsRow.FieldID.ToString
        newMenuItem.Tag = newMenuItem.Name

      Next

      RootMenu.Items.Add(DrillDownMenu)
      Return DrillDownMenu

    Catch ex As Exception
    End Try

    Return DrillDownMenu

  End Function

  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************
    Dim thisMenuItem As ToolStripMenuItem = Nothing

    Try
      Me.Cursor = Cursors.WaitCursor

      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)
        FieldName = thisMenuItem.Name

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (FieldName.StartsWith("Menu_StaticField_")) Then
          Dim FieldID As RenaissanceGlobals.PertracInformationFields

          FieldName = FieldName.Substring(17)
          FieldID = System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), FieldName)

          Grid_StaticFields_Display(FieldID) = thisMenuItem.Checked

          If (thisMenuItem.Checked = False) Then
            Dim ThisColumnName As String

            ThisColumnName = "Static_" & FieldID.ToString

            If Grid_Results.Cols.Contains(ThisColumnName) Then
              Grid_Results.Cols.Remove(ThisColumnName)
            End If
          End If
        ElseIf (FieldName.StartsWith("Menu_CustomField_")) Then
          Dim FieldID As Integer

          FieldID = CInt(FieldName.Substring(17))

          If Grid_CustomFields_Display_Dictionary.ContainsKey(FieldID) Then
            Grid_CustomFields_Display_Dictionary.Item(FieldID) = thisMenuItem.Checked
          Else
            Grid_CustomFields_Display_Dictionary.Add(FieldID, thisMenuItem.Checked)
          End If

          If (thisMenuItem.Checked = False) Then
            Dim ThisColumnName As String

            ThisColumnName = "Custom_" & FieldID.ToString

            If Grid_Results.Cols.Contains(ThisColumnName) Then
              Grid_Results.Cols.Remove(ThisColumnName)
            End If
          End If
        Else
          Exit Sub
        End If

      End If
    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    If (InPaint = False) AndAlso (thisMenuItem IsNot Nothing) AndAlso (thisMenuItem.Checked) Then
      PaintGrid()
    End If

  End Sub

  Private Sub DrilldownGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      Dim DescriptionString As String
      Dim SenderName As String
      Dim Col_Is_Custom As Boolean = False
      Dim FieldID As Integer = 0

      If (TypeOf sender Is ToolStripMenuItem) Then
        SenderName = CType(sender, ToolStripMenuItem).Name
        DescriptionString = "Fund Search" & vbCrLf & "  " & CType(sender, ToolStripMenuItem).Text
      ElseIf (TypeOf sender Is ToolStripItem) Then
        SenderName = CType(sender, ToolStripItem).Name
        DescriptionString = "Fund Search" & vbCrLf & "  " & CType(sender, ToolStripItem).Text
      Else
        Exit Sub
      End If

      If (SenderName.StartsWith("MenuDrilldown_")) Then
        SenderName = SenderName.Substring(14)

        If (SenderName.StartsWith("StaticField_")) Then
          FieldID = CInt(System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SenderName.Substring(12)))
        ElseIf (SenderName.StartsWith("CustomField_")) Then
          Col_Is_Custom = True
          FieldID = CInt(SenderName.Substring(12))
        Else
          Exit Sub
        End If

      ElseIf (SenderName.StartsWith("cmsMenuDrilldown_")) Then
        SenderName = SenderName.Substring(17)

        If (SenderName.StartsWith("StaticField_")) Then
          FieldID = CInt(System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SenderName.Substring(12)))
        ElseIf (SenderName.StartsWith("CustomField_")) Then
          Col_Is_Custom = True
          FieldID = CInt(SenderName.Substring(12))
        Else
          Exit Sub
        End If

      Else
        Exit Sub
      End If

      ' OK, Now open a drilldown form giving the Custom Group ID, the Drilldown field ID and indicate whether or not it is a Custom or Static field.

      Dim NewDrilldownForm As frmDrillDown

      NewDrilldownForm = CType(MainForm.New_GenoaForm(GenoaFormID.frmDrillDown).Form, frmDrillDown)

      NewDrilldownForm.SetDrilldownParameters(DefaultStatsDatePeriod, Me, CustomGroupID_CompoundArray, New Integer() {FieldID}, New Boolean() {Col_Is_Custom}, False, Now.Date.AddDays(0 - Now.Date.Day).AddYears(-4), Now.Date.AddDays(0 - Now.Date.Day), DescriptionString)

      If (Not NewDrilldownForm.IsDisposed) Then
        NewDrilldownForm.Show()
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Menu_Options_SetValueCombos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Options_SetValuecombos.Click
    ' ************************************************************************
    '
    '
    ' ************************************************************************

    If (Me.Created) Then
      Menu_Options_SetValuecombos.Checked = Not Menu_Options_SetValuecombos.Checked

      If (Menu_Options_SetValuecombos.Checked) Then
        ' Populate Select Value Combos

        Try
          Dim ControlCounter As Integer
          Dim thisComboSelectField As ComboBox = Nothing

          For ControlCounter = 1 To SELECTION_ITEM_COUNT
            thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & ControlCounter.ToString), ComboBox)

            If (thisComboSelectField IsNot Nothing) Then
              Call Combo_SelectField_SelectedIndexChanged(thisComboSelectField, New System.EventArgs)
            End If
          Next

        Catch ex As Exception
        End Try

      Else
        ' Clear SelectValue Combos.

        Try
          Dim ControlCounter As Integer
          Dim thisComboSelectValue As ComboBox = Nothing

          For ControlCounter = 1 To SELECTION_ITEM_COUNT
            thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ControlCounter.ToString), ComboBox)

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.DataSource = Nothing
            End If
          Next

        Catch ex As Exception
        End Try

      End If
    End If
  End Sub

  Private Sub Menu_Options_ShowAllResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Options_ShowAllResults.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Menu_Options_ShowAllResults.Checked = Not Menu_Options_ShowAllResults.Checked

    If (Menu_Options_ShowAllResults.Checked) Then

      ' Expand Grid ?

      If (Grid_Results.Tag IsNot Nothing) AndAlso (IsNumeric(Grid_Results.Tag)) Then
        Dim CompoundArrayLength As Integer

        CompoundArrayLength = CInt(Grid_Results.Tag)

        If (Grid_Results.Cols.Count <= CompoundArrayLength) Then
          PaintGrid(Grid_Results.Cols.Count - 1, True, True)
        End If
      End If
    End If
  End Sub

  Private Sub Menu_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_SelectAll.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Grid_Results.Rows.Count > 1) Then
        Grid_Results.Select(1, 0, Grid_Results.Rows.Count - 1, Grid_Results.Cols.Count - 1)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_SaveToGroup_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Menu_SaveToGroup.MouseEnter
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Menu_SaveToGroup.DropDownItems.Count <= 0) Then

        Dim thisDSGroups As RenaissanceDataClass.DSGroupList
        Dim thisGroupTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
        Dim ThisGroup As RenaissanceDataClass.DSGroupList.tblGroupListRow
        Dim SelectedGroups() As RenaissanceDataClass.DSGroupList.tblGroupListRow
        Dim thisMenuItem As ToolStripMenuItem

        thisDSGroups = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)
        thisGroupTable = thisDSGroups.tblGroupList
        SelectedGroups = thisGroupTable.Select("True", "GroupListName")

        Try

          thisMenuItem = New ToolStripMenuItem("<New Group>", Nothing, AddressOf Me.Menu_SaveToGroup_Click)
          thisMenuItem.Tag = 0
          Menu_SaveToGroup.DropDownItems.Add(thisMenuItem)
          Menu_SaveToGroup.DropDownItems.Add(New ToolStripSeparator)

          Dim Permissions As Integer

          Permissions = MainForm.CheckPermissions("frmGroupList", RenaissanceGlobals.PermissionFeatureType.TypeForm)

          If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then

            thisMenuItem.Enabled = False

          End If

        Catch ex As Exception
        End Try

        Try

          Dim Permissions As Integer
          Dim HasPermission As Boolean = False

          Permissions = MainForm.CheckPermissions("frmGroupMembers", RenaissanceGlobals.PermissionFeatureType.TypeForm)

          If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0) OrElse ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0) Then

            HasPermission = True

          End If

          For Each ThisGroup In SelectedGroups
            thisMenuItem = New ToolStripMenuItem(ThisGroup.GroupListName, Nothing, AddressOf Me.Menu_SaveToGroup_Click)
            thisMenuItem.Tag = ThisGroup.GroupListID
            thisMenuItem.Enabled = HasPermission

            Menu_SaveToGroup.DropDownItems.Add(thisMenuItem)
          Next

        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub BuildSavedSearchesMenu()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim thisWindowsMenu As ToolStripMenuItem
    Dim ThisDSSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList
    Dim ThistblSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListDataTable
    Dim SelectedSearchRows() As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListRow
    Dim ThisSearchRow As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListRow
    Dim ItemCount As Integer
    Dim thisMenuItem As ToolStripMenuItem

    Try
      ' Get Saved Searches Menu

      thisWindowsMenu = Me.Menu_SavedSearches

      ' Clear Existing Items

      If (thisWindowsMenu.DropDownItems.Count > 0) Then
        thisWindowsMenu.DropDownItems.Clear()
      End If
      ComboBox_SavedSearches.Items.Clear()
      ComboBox_SavedSearches.Items.Add("")

      ' Get Source Data 

      ThisDSSavedSearchList = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList)
      ThistblSavedSearchList = ThisDSSavedSearchList.tblGenoaSavedSearchList

      ' Build Menu

      thisMenuItem = New ToolStripMenuItem("&Save Current Search Criteria as New.", Nothing, AddressOf Me.Menu_SaveSearch_Click)
      thisWindowsMenu.DropDownItems.Add(thisMenuItem)
      thisMenuItem = New ToolStripMenuItem("&Save Current Criteria As ''", Nothing, AddressOf Me.Menu_SaveSearchOverwrite_Click)
      thisWindowsMenu.DropDownItems.Add(thisMenuItem)
      thisMenuItem.Visible = False
      SaveCurrentSearchMenu = thisMenuItem
      thisMenuItem = New ToolStripMenuItem("&Delete Search ''", Nothing, AddressOf Me.Menu_DeleteSearch_Click)
      thisWindowsMenu.DropDownItems.Add(thisMenuItem)
      thisMenuItem.Visible = False
      DeleteCurrentSearchMenu = thisMenuItem

      thisWindowsMenu.DropDownItems.Add(New ToolStripSeparator)

      If (ThistblSavedSearchList.Rows.Count > 0) Then

        SelectedSearchRows = ThistblSavedSearchList.Select("True", "SearchName")

        For ItemCount = 0 To (SelectedSearchRows.Length - 1)

          ThisSearchRow = SelectedSearchRows(ItemCount)

          thisMenuItem = New ToolStripMenuItem(ThisSearchRow.SearchName, Nothing, AddressOf Me.Menu_SavedSearchesItem_Click)
          thisMenuItem.Tag = ThisSearchRow.SearchID

          If (Me.CurrentSavedSearchID = ThisSearchRow.SearchID) Then
            ' Set 'Save' and 'Delete' Menu Items.

            SaveCurrentSearchMenu.Tag = CInt(CurrentSavedSearchID)
            DeleteCurrentSearchMenu.Tag = CInt(CurrentSavedSearchID)
          End If

          If (SaveCurrentSearchMenu IsNot Nothing) AndAlso (IsNumeric(SaveCurrentSearchMenu.Tag)) AndAlso (CInt(SaveCurrentSearchMenu.Tag) = ThisSearchRow.SearchID) Then
            SaveCurrentSearchMenu.Text = "&Save Current Search Criteria As '" & ThisSearchRow.SearchName & "'"
            SaveCurrentSearchMenu.Visible = True
          End If
          If (DeleteCurrentSearchMenu IsNot Nothing) AndAlso (IsNumeric(DeleteCurrentSearchMenu.Tag)) AndAlso (CInt(DeleteCurrentSearchMenu.Tag) = ThisSearchRow.SearchID) Then
            DeleteCurrentSearchMenu.Text = "&Delete Search '" & ThisSearchRow.SearchName & "'"
            DeleteCurrentSearchMenu.Visible = True
          End If

          thisMenuItem.Name = "Menu_SavedSearch_" & ItemCount.ToString
          thisWindowsMenu.DropDownItems.Add(thisMenuItem)
          ComboBox_SavedSearches.Items.Add(thisMenuItem.Text)

        Next

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error building Saved Search Menu", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Menu_SavedSearchesItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisSearchMenu As ToolStripMenuItem
    Dim ThisSearchID As Integer

    Try
      ' Get Calling Menu

      If Not (TypeOf sender Is ToolStripMenuItem) Then
        Exit Sub
      End If

      ThisSearchMenu = CType(sender, ToolStripMenuItem)

      ' Resolve SavedSearchID

      If Not (IsNumeric(ThisSearchMenu.Tag)) Then
        Exit Sub
      End If

      ThisSearchID = CInt(ThisSearchMenu.Tag)

      ' Set 'Save' and 'Delete' Menu Items.

      SaveCurrentSearchMenu.Text = "&Save Current Search Criteria As '" & ThisSearchMenu.Text & "'"
      SaveCurrentSearchMenu.Tag = CInt(ThisSearchMenu.Tag)
      SaveCurrentSearchMenu.Visible = True
      DeleteCurrentSearchMenu.Text = "&Delete Search '" & ThisSearchMenu.Text & "'"
      DeleteCurrentSearchMenu.Tag = CInt(ThisSearchMenu.Tag)
      DeleteCurrentSearchMenu.Visible = True

      ' Load Search Details

      Load_SavedSearch(ThisSearchID)

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Load_SavedSearch(ByVal pSearchID As Integer)
    ' *****************************************************************************************
    ' Loads the Given Saved Search.
    '
    ' *****************************************************************************************

    Dim ThisDSSavedSearchItem As RenaissanceDataClass.DSGenoaSavedSearchItems
    Dim ThistblSavedSearchItem As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsDataTable
    Dim SelectedSearchRows() As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsRow
    Dim ThisSearchRow As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsRow
    Dim ItemCount As Integer
    Dim ThisSearchID As Integer

    Dim thisRadioPanel As Panel
    Dim thisCustomRadio As RadioButton
    Dim thisStaticRadio As RadioButton
    Dim thisComboSelectField As ComboBox
    Dim thisComboSelectValue As ComboBox
    Dim thisComboSelectValue2 As ComboBox
    Dim thisComboCondition As ComboBox
    Dim thisComboAndOr As ComboBox = Nothing
    Dim ControlCounter As Integer

    Dim Org_InPaint As Boolean = InPaint

    Try
      ' Inpaint must be false as we need the Select Control events to process correctly.

      InPaint = False

      Me.Cursor = Cursors.WaitCursor
      ThisSearchID = pSearchID

      ' Get Search Details

      ThisDSSavedSearchItem = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchItems)
      ThistblSavedSearchItem = ThisDSSavedSearchItem.tblGenoaSavedSearchItems

      SelectedSearchRows = ThistblSavedSearchItem.Select("SearchID=" & ThisSearchID.ToString, "SearchItemID")

      ' Apply New Criteria

      If (SelectedSearchRows IsNot Nothing) AndAlso (SelectedSearchRows.Length > 0) AndAlso (ThisSearchID > 0) Then

        For ItemCount = 1 To (MIN(SELECTION_ITEM_COUNT, SelectedSearchRows.Length))
          ThisSearchRow = SelectedSearchRows(ItemCount - 1)

          ' Get Controls

          thisRadioPanel = CType(Panel_SearchInstruments.Controls("Panel" & ItemCount.ToString), Panel)
          thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & ItemCount.ToString), RadioButton)
          thisStaticRadio = CType(thisRadioPanel.Controls("Radio_Static" & ItemCount.ToString), RadioButton)

          thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & ItemCount.ToString), ComboBox)
          thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ItemCount.ToString), ComboBox)
          thisComboSelectValue2 = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ItemCount.ToString & "b"), ComboBox)
          thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & ItemCount.ToString), ComboBox)
          If (ItemCount > 1) Then
            thisComboAndOr = CType(Panel_SearchInstruments.Controls("Combo_AndOr" & (ItemCount - 1).ToString), ComboBox)
          End If

          ' Set Values

          If (ThisSearchRow.FieldIsCustom) Then
            If (Not thisCustomRadio.Checked) Then
              thisCustomRadio.Checked = True
            End If
          Else
            If (Not thisStaticRadio.Checked) Then
              thisStaticRadio.Checked = True
            End If
          End If
          Application.DoEvents()

          If (CStr(thisComboSelectField.SelectedValue) <> (ThisSearchRow.SelectFieldID)) Then
            thisComboSelectField.SelectedValue = ThisSearchRow.SelectFieldID
          End If
          If (thisComboCondition.Text.ToUpper <> ThisSearchRow.ConditionOperand.ToUpper) Then
            thisComboCondition.Text = ThisSearchRow.ConditionOperand
          End If
          If (thisComboSelectValue.Text <> ThisSearchRow.SelectValue1) Then
            thisComboSelectValue.Text = ThisSearchRow.SelectValue1
          End If
          If (thisComboSelectValue2.Text <> ThisSearchRow.SelectValue2) Then
            thisComboSelectValue2.Text = ThisSearchRow.SelectValue2
          End If

          If (ItemCount > 1) Then
            If (ThisSearchRow.IsOR) Then
              If (thisComboAndOr.SelectedIndex <> 1) Then
                thisComboAndOr.SelectedIndex = 1
              End If
            Else
              If (thisComboAndOr.SelectedIndex <> 0) Then
                thisComboAndOr.SelectedIndex = 0
              End If
            End If
          End If

        Next

        ' Clear remaining Criteria

        Try

          For ControlCounter = (MIN(SELECTION_ITEM_COUNT, SelectedSearchRows.Length) + 1) To SELECTION_ITEM_COUNT
            Try
              thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & ControlCounter.ToString), ComboBox)
              thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ControlCounter.ToString), ComboBox)
              thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & ControlCounter.ToString), ComboBox)

              If (thisComboSelectField IsNot Nothing) AndAlso (thisComboSelectField.Items.Count > 0) Then
                thisComboSelectField.SelectedIndex = 0
              End If

              If (thisComboCondition IsNot Nothing) Then
                thisComboCondition.SelectedIndex = 0
              End If

              If (thisComboSelectValue IsNot Nothing) Then
                thisComboSelectValue.Text = ""
              End If

            Catch ex As Exception
            End Try
          Next

        Catch ex As Exception
        End Try

      Else

        ClearAllSearchCriteria()

      End If

      CurrentSavedSearchID = ThisSearchID

    Catch ex As Exception
    Finally

      Me.Cursor = Cursors.Default
      Application.DoEvents()
      InPaint = Org_InPaint

    End Try

  End Sub

  Private Sub ClearAllSearchCriteria()
    ' *****************************************************************************************
    '
    ' Clear Existing Criteria
    '
    ' *****************************************************************************************

    Dim thisComboSelectField As ComboBox
    Dim thisComboSelectValue As ComboBox
    Dim thisComboCondition As ComboBox
    Dim thisComboAndOr As ComboBox = Nothing
    Dim ControlCounter As Integer

    Try

      For ControlCounter = 1 To SELECTION_ITEM_COUNT
        Try
          thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & ControlCounter.ToString), ComboBox)
          thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ControlCounter.ToString), ComboBox)
          thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & ControlCounter.ToString), ComboBox)

          If (thisComboSelectField IsNot Nothing) AndAlso (thisComboSelectField.Items.Count > 0) Then
            thisComboSelectField.SelectedIndex = 0
          End If

          If (thisComboCondition IsNot Nothing) Then
            thisComboCondition.SelectedIndex = 0
          End If

          If (thisComboSelectValue IsNot Nothing) Then
            thisComboSelectValue.Text = ""
          End If

        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_SaveSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************
    Dim ThisDSSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList
    Dim ThistblSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListDataTable
    Dim ThisSearchRow As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListRow

    Try

      ' Get Source Data 

      ThisDSSavedSearchList = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList)
      ThistblSavedSearchList = ThisDSSavedSearchList.tblGenoaSavedSearchList

      Dim NewSearchName As String = InputBox("Saved Search Description : ", "Save New Search.", "")

      If (NewSearchName.Length > 0) Then
        ThisSearchRow = ThistblSavedSearchList.NewtblGenoaSavedSearchListRow

        ThisSearchRow.SearchName = NewSearchName
        ThisSearchRow.VisibleToAllUsers = True

        ThistblSavedSearchList.Rows.Add(ThisSearchRow)
        MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList, New DataRow() {ThisSearchRow})

        If (ThisSearchRow.IsSearchIDNull = False) AndAlso (ThisSearchRow.SearchID > 0) Then
          SaveCurrentCriteriaAs(ThisSearchRow.SearchID)
        End If

        Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblGenoaSavedSearchList))

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Search", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Menu_SaveSearchOverwrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************

    Try

      If (SaveCurrentSearchMenu IsNot Nothing) AndAlso (IsNumeric(SaveCurrentSearchMenu.Tag)) Then
        SaveCurrentCriteriaAs(CInt(SaveCurrentSearchMenu.Tag))
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub SaveCurrentCriteriaAs(ByVal pTargetSavedSearchID As Integer)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************
    Dim ThisDSSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList
    Dim ThistblSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListDataTable
    Dim SelectedSearchRows() As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListRow
    Dim ThisDSSavedSearchItems As RenaissanceDataClass.DSGenoaSavedSearchItems
    Dim ThistblSavedSearchItems As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsDataTable
    Dim SelectedSearchItemRows() As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsRow

    Dim SelectCounter As Integer
    Dim SaveCounter As Integer
    Dim ThisItemRow As RenaissanceDataClass.DSGenoaSavedSearchItems.tblGenoaSavedSearchItemsRow
    Dim thisComboAndOr As ComboBox

    Try
      If (pTargetSavedSearchID <= 0) Then
        Exit Sub
      End If

      ' Get SearchList Entry, check it exists

      ThisDSSavedSearchList = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList)
      ThistblSavedSearchList = ThisDSSavedSearchList.tblGenoaSavedSearchList
      SelectedSearchRows = ThistblSavedSearchList.Select("SearchID=" & pTargetSavedSearchID.ToString)

      ' If the 'List' entry exists...

      If (SelectedSearchRows.Length > 0) Then

        ' Get referneces to the 'Items' table.

        ThisDSSavedSearchItems = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchItems)
        ThistblSavedSearchItems = ThisDSSavedSearchItems.tblGenoaSavedSearchItems

        ' Select any existing rows for this Search list.

        SelectedSearchItemRows = ThistblSavedSearchItems.Select("SearchID=" & pTargetSavedSearchID.ToString, "SearchItemID")

        SaveCounter = 0
        For SelectCounter = 0 To (SELECTION_ITEM_COUNT - 1)

          If (SelectionItems(SelectCounter).SelectionIsValid) Then

            If (SaveCounter < SelectedSearchItemRows.Length) Then
              ThisItemRow = SelectedSearchItemRows(SaveCounter)
            Else
              ThisItemRow = ThistblSavedSearchItems.NewtblGenoaSavedSearchItemsRow
            End If

            ThisItemRow.SearchID = pTargetSavedSearchID
            ThisItemRow.FieldIsStatic = SelectionItems(SelectCounter).IsStaticField
            ThisItemRow.FieldIsCustom = SelectionItems(SelectCounter).IsCustomField

            If (SelectCounter > 0) Then
              thisComboAndOr = CType(Panel_SearchInstruments.Controls("Combo_AndOr" & SelectCounter.ToString), ComboBox)
              ThisItemRow.IsAND = False
              ThisItemRow.IsOR = False

              If (thisComboAndOr.SelectedIndex = 1) Then
                ThisItemRow.IsOR = True
              Else
                ThisItemRow.IsAND = True
              End If
            Else

              ThisItemRow.IsAND = False
              ThisItemRow.IsOR = False

            End If

            ThisItemRow.SelectFieldID = SelectionItems(SelectCounter).SelectFieldID
            ThisItemRow.ConditionOperand = SelectionItems(SelectCounter).ConditionOperand
            ThisItemRow.SelectValue1 = SelectionItems(SelectCounter).SelectValue
            ThisItemRow.SelectValue2 = SelectionItems(SelectCounter).SelectValue2

            ' If this is a new row, add it to the table.

            If (ThisItemRow.RowState And DataRowState.Detached) = DataRowState.Detached Then
              ThistblSavedSearchItems.Rows.Add(ThisItemRow)
            End If

            SaveCounter += 1
          End If

        Next

        ' Delete any spare existing entries.

        If (SaveCounter < SelectedSearchItemRows.Length) Then
          While (SaveCounter < SelectedSearchItemRows.Length)
            SelectedSearchItemRows(SaveCounter).Delete()

            SaveCounter += 1
          End While
        End If

        ' Save Changes

        MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchItems, ThistblSavedSearchItems)
        CurrentSavedSearchID = pTargetSavedSearchID

        Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblGenoaSavedSearchItems))

      Else

        MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "In SaveCurrentCriteriaAs(), The expected SearchID (" & pTargetSavedSearchID.ToString & "), does not seem to exist.", "", True)

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error In SaveCurrentCriteriaAs()", ex.StackTrace, True)

    End Try

  End Sub

  Private Sub Menu_DeleteSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************
    Dim ThisSearchMenu As ToolStripMenuItem
    Dim ThisSearchID As Integer

    Dim ThisDSSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList
    Dim ThistblSavedSearchList As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListDataTable
    Dim SelectedSearchRows() As RenaissanceDataClass.DSGenoaSavedSearchList.tblGenoaSavedSearchListRow

    Try
      ' DeleteCurrentSearchMenu

      ' Get Calling Menu

      If Not (TypeOf sender Is ToolStripMenuItem) Then
        Exit Sub
      End If

      ThisSearchMenu = CType(sender, ToolStripMenuItem)

      ' Resolve SavedSearchID

      If Not (IsNumeric(ThisSearchMenu.Tag)) Then
        Exit Sub
      End If

      ThisSearchID = CInt(ThisSearchMenu.Tag)

      ' Get Search Details

      ThisDSSavedSearchList = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList)
      ThistblSavedSearchList = ThisDSSavedSearchList.tblGenoaSavedSearchList

      SelectedSearchRows = ThistblSavedSearchList.Select("SearchID=" & ThisSearchID.ToString, "SearchID")

      If (SelectedSearchRows IsNot Nothing) AndAlso (SelectedSearchRows.Length > 0) Then
        SelectedSearchRows(0).Delete()

        MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGenoaSavedSearchList, ThistblSavedSearchList)
        MainForm.LogError(Me.Name, LOG_LEVELS.Changes, "Delete, tblGenoaSavedSearchList, SearchID=" & ThisSearchID.ToString, "", "", False)

        Dim thisMessage As New RenaissanceUpdateEventArgs

        thisMessage.TableChanged(RenaissanceChangeID.tblGenoaSavedSearchList) = True
        thisMessage.TableChanged(RenaissanceChangeID.tblGenoaSavedSearchItems) = True
        MainForm.Main_RaiseEvent(thisMessage)

        Load_SavedSearch(0)

      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Menu_SaveToGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    '
    ' *****************************************************************************************
    Dim thisMenuItem As ToolStripMenuItem
    Dim ThisListID As Integer = 0
    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

    Try
      If (TypeOf sender Is ToolStripMenuItem) Then
        thisMenuItem = CType(sender, ToolStripMenuItem)
      Else
        Exit Sub
      End If

      ' Check permissions

      Try

        Dim Permissions As Integer

        Permissions = MainForm.CheckPermissions("frmGroupMembers", RenaissanceGlobals.PermissionFeatureType.TypeForm)

        If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
          MainForm.LogError(Me.Name & ", Menu_SaveToGroup_Click()", LOG_LEVELS.Warning, "", "You do not have permission to save Group Members.", "", True)

          Exit Sub
        End If

      Catch ex As Exception
      End Try

      ' 

      If (thisMenuItem.Tag IsNot Nothing) AndAlso (IsNumeric(thisMenuItem.Tag)) Then
        ThisListID = CInt(thisMenuItem.Tag)
      End If

      ' Get reference to the GroupList and GroupItems tables.

      Dim GroupListDataset As RenaissanceDataClass.DSGroupList
      Dim GroupListTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
      Dim newGroupListRow As RenaissanceDataClass.DSGroupList.tblGroupListRow

      GroupListDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)
      GroupListTable = GroupListDataset.tblGroupList

      Dim GroupItemsDataset As RenaissanceDataClass.DSGroupItems
      Dim GroupItemsTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

      GroupItemsDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)
      GroupItemsTable = GroupItemsDataset.tblGroupItems

      ' Add or Edit ?

      If (ThisListID <= 0) Then
        ' Add New Group

        Try

          Dim Permissions As Integer

          Permissions = MainForm.CheckPermissions("frmGroupList", RenaissanceGlobals.PermissionFeatureType.TypeForm)

          If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
            MainForm.LogError(Me.Name & ", Menu_SaveToGroup_Click()", LOG_LEVELS.Warning, "", "You do not have permission to save Add Groups.", "", True)

            Exit Sub
          End If

        Catch ex As Exception
        End Try

        Dim NewGroupName As String

        NewGroupName = InputBox("Please Enter New Group Name", "New Group", "")

        If (NewGroupName.Length <= 0) Then
          Exit Sub
        End If

        newGroupListRow = GroupListTable.NewtblGroupListRow

        newGroupListRow.GroupListName = NewGroupName
        newGroupListRow.GroupGroup = ""
        newGroupListRow.GroupDateFrom = Renaissance_BaseDate
        newGroupListRow.GroupDateTo = Renaissance_EndDate_Data
        newGroupListRow.DefaultConstraintGroup = 0
        newGroupListRow.DefaultCovarianceMatrix = 0

        GroupListTable.Rows.Add(newGroupListRow)
        MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, New DataRow() {newGroupListRow})
        ThisListID = newGroupListRow.GroupListID

        UpdateMessage.TableChanged(RenaissanceChangeID.tblGroupList) = True

      Else
        ' Overwrite existing group :-

        ' Confirm..

        If (MessageBox.Show("Do you want to overwrite the '" & thisMenuItem.Text & "' Group?", "Confirm Overwite Group", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes) Then
          Exit Sub
        End If

      End If

      ' OK, Now Get the Item records for Group 'ThisListID' and Add / Update as necessary.

      ' First, get an array of InstrumentIds.

      Dim Col_GroupPertracCode As Integer = Grid_Results.Cols("ID").SafeIndex
      Dim PertracIds(Grid_Results.Rows.Count - 1) As Integer
      Dim ItemCounter As Integer
      Dim IDCounter As Integer
      Dim FoundFlag As Boolean

      For IDCounter = 1 To (Grid_Results.Rows.Count - 1)
        If (IsNumeric(Grid_Results.Item(IDCounter, Col_GroupPertracCode))) Then
          PertracIds(IDCounter - 1) = CInt(Grid_Results.Item(IDCounter, Col_GroupPertracCode))
        End If
      Next

      ' 

      Dim SelectedItems() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim thisItem As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim NewItem As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

      SelectedItems = GroupItemsTable.Select("GroupID=" & ThisListID.ToString)

      If (SelectedItems IsNot Nothing) AndAlso (SelectedItems.Length > 0) Then

        ' Delete UnUsed Items / Update Existing Items as necessary

        For ItemCounter = 0 To (SelectedItems.Length - 1)
          FoundFlag = False
          thisItem = SelectedItems(ItemCounter)

          For IDCounter = 0 To (PertracIds.Length - 1)
            If (PertracIds(IDCounter) > 0) AndAlso (PertracIds(IDCounter) = thisItem.GroupPertracCode) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If (FoundFlag) Then
            ' This Existing Item exists in the New Pertrac IDs list, So leave it alone.

            ' Clear Pertrac ID so it is not added in the second pass.
            PertracIds(IDCounter) = 0

          Else
            ' This Existing Item does not exist in the New Pertrac IDs list, So Delete

            thisItem.Delete()

          End If

        Next

      End If

      ' Add Items for Ids that were not Updates.

      For IDCounter = 0 To (PertracIds.Length - 1)
        If (PertracIds(IDCounter) > 0) Then
          ' Add New Item

          NewItem = GroupItemsTable.NewtblGroupItemsRow

          NewItem.GroupID = ThisListID
          NewItem.GroupItemID = 0
          NewItem.GroupPertracCode = PertracIds(IDCounter)
          NewItem.GroupIndexCode = 0
          NewItem.GroupSector = ""

          GroupItemsTable.Rows.Add(NewItem)

        End If
      Next

      MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems, GroupItemsTable)

      UpdateMessage.TableChanged(RenaissanceChangeID.tblGroupItems) = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Search items to a Group", ex.StackTrace, True)
    End Try

    ' Propagate changes

    Call MainForm.Main_RaiseEvent(UpdateMessage)

  End Sub

#End Region

  Public Sub InitialiseForFixedArray(ByVal FixedIdArray() As Integer)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      UseFixedIdArray = True

      ' Copy given IDs to CompoundArray

      If (FixedIdArray Is Nothing) Then
        CompoundArray = Nothing
      Else
        CompoundArray = Array.CreateInstance(GetType(Integer), FixedIdArray.Length)

        If (FixedIdArray.Length > 0) Then
          Array.Copy(FixedIdArray, CompoundArray, FixedIdArray.Length)
        End If
      End If

      Set_CustomGroupID_CompoundArray()

      ' Hide / Show Controls / Menus as necessary

      Menu_SavedSearches.Enabled = False

      Split_FundSearch.Panel1Collapsed = True

      Label_SearchFieldType.Visible = False

      Label_InformationSelectWhere.Visible = False

      Me.Text = "Selected Fund View"

      PaintGrid()

    Catch ex As Exception
      CompoundArray = Nothing
      Set_CustomGroupID_CompoundArray()
    End Try

  End Sub

  Public Sub InitialiseForNormalSearch()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      CompoundArray = Nothing
      Set_CustomGroupID_CompoundArray()

      UseFixedIdArray = False

      Menu_SavedSearches.Enabled = True

      Split_FundSearch.Panel1Collapsed = False

      Label_SearchFieldType.Visible = True

      Label_InformationSelectWhere.Visible = True

      Me.Text = "Fund Search"

      PaintGrid()

    Catch ex As Exception
    End Try

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '

  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
    ' *****************************************************************************************
    ' Sub called when a form is re-used, after being hidden but not destroyed when closed
    ' at an earlier point.
    ' *****************************************************************************************

    InitialiseForNormalSearch()

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ' *****************************************************************************************
    ' Forced closure of this form.
    '
    ' *****************************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      CustomGroupID_CompoundArray = MainForm.UniqueGenoaNumber

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
      _FormOpenFailed = False
      _InUse = True

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If


      ' Initialise Timer

      If (PaintTimer IsNot Nothing) Then
        Try
          PaintTimer.Stop()
        Catch ex As Exception
        Finally
          PaintTimer = Nothing
        End Try
      End If
      Try
        PaintTimer = New System.Windows.Forms.Timer
        In_PaintTimer_Tick = False
        SelectControlChanged = False
        PaintTimer.Interval = (SelectionItemClass.UPDATE_DELAY_Secs * 1000) / 2

        AddHandler PaintTimer.Tick, AddressOf PaintTimer_Tick
        PaintTimer.Start()
      Catch ex As Exception
      End Try


      ' Initialse form

      IsOverCancelButton = False
      Try
        ' Check User permissions
        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

          FormIsValid = False
          _FormOpenFailed = True
          Exit Sub
        End If

        ' Initialise Combos
        ' Display initial record.

        Dim ControlCounter As Integer
        Dim thisRadioPanel As Panel
        Dim thisCustomRadio As RadioButton
        Dim thisStaticRadio As RadioButton
        Dim thisComboSelectField As ComboBox
        Dim thisComboSelectValue As ComboBox
        Dim thisComboCondition As ComboBox

        For ControlCounter = 1 To SELECTION_ITEM_COUNT
          Try
            thisRadioPanel = CType(Panel_SearchInstruments.Controls("Panel" & ControlCounter.ToString), Panel)
            thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & ControlCounter.ToString), RadioButton)
            thisStaticRadio = CType(thisRadioPanel.Controls("Radio_Static" & ControlCounter.ToString), RadioButton)

            thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & ControlCounter.ToString), ComboBox)
            thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & ControlCounter.ToString), ComboBox)
            thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & ControlCounter.ToString), ComboBox)

            If (thisStaticRadio IsNot Nothing) AndAlso (thisStaticRadio.Checked = False) Then
              thisStaticRadio.Checked = True
              SelectionItems(ControlCounter - 1).UpdateFlag = True
            End If

            If (thisComboSelectField IsNot Nothing) AndAlso (thisComboSelectField.Items.Count > 0) Then
              thisComboSelectField.SelectedIndex = 0
              SelectionItems(ControlCounter - 1).UpdateFlag = True
            End If

            If (thisComboCondition IsNot Nothing) Then
              thisComboCondition.SelectedIndex = 0
              SelectionItems(ControlCounter - 1).UpdateFlag = True
            End If

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.Text = ""
              SelectionItems(ControlCounter - 1).UpdateFlag = True
            End If

          Catch ex As Exception
          End Try

          If SelectionItems(ControlCounter - 1).UpdateFlag Then
            SelectControlChanged = True
          End If
        Next

        InPaint = True

        ' Set Initial Grid Fields

        Try
          CType(StaticFieldsMenu.DropDownItems("Menu_StaticField_" & PertracInformationFields.FundName.ToString), ToolStripMenuItem).Checked = True
          CType(StaticFieldsMenu.DropDownItems("Menu_StaticField_" & PertracInformationFields.FundAssets.ToString), ToolStripMenuItem).Checked = True
        Catch ex As Exception
        End Try
        Grid_StaticFields_Display(CInt(PertracInformationFields.FundName)) = True
        Grid_StaticFields_Display(CInt(PertracInformationFields.FundAssets)) = True

        ' Set Search Menu.

        BuildSavedSearchesMenu()

        ' Clear Search fields

        ClearAllSearchCriteria()

      Catch ex As Exception
      Finally
        InPaint = False
      End Try

      Me.Grid_Results.Rows.Count = 1

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim HideForm As Boolean

    ' Clear Custon Group cache

    Try

      If (CustomGroupID_CompoundArray > 0) Then
        ' Setting Null group members will also have the effect of clearing the Stats & Pertrac data caches.

        MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_CompoundArray, Nothing, Nothing)

        RaiseEvent SearchUpdateEvent(Me, -CustomGroupID_CompoundArray) ' Note negative GroupID.

        MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_CompoundArray, Nothing, Nothing)

      End If
    Catch ex As Exception
    End Try

    Try

      ' Hide or Close this form ?
      ' All depends on how many of this form type are Open or in Cache...

      _InUse = False

      If (PaintTimer IsNot Nothing) Then
        Try
          PaintTimer.Stop()
        Catch ex As Exception
        Finally
          PaintTimer = Nothing
        End Try
      End If

      If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
        HideForm = False
      Else

        HideForm = True
        If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
          HideForm = False
        End If
      End If

    Catch ex As Exception
    End Try

    If HideForm = True Then

      Try

        MainForm.HideInFormsCollection(Me)
        Me.Hide() ' NPP Fix

        e.Cancel = True

      Catch ex As Exception
      End Try

    Else

      Try

        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_SelectField1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectField1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectField1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectField1.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Condition1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Condition1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Condition1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectValue1.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectValue1.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectValue1.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectValue1.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        RemoveHandler Combo_SelectField2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectField2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectField2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectField2.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Condition2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Condition2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Condition2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectValue2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectValue2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectValue2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectValue2.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        RemoveHandler Combo_SelectField3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectField3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectField3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectField3.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Condition3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Condition3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Condition3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectValue3.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectValue3.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectValue3.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectValue3.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        RemoveHandler Combo_SelectField4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectField4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectField4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectField4.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Condition4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Condition4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Condition4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectValue4.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectValue4.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectValue4.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectValue4.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        RemoveHandler Combo_SelectField5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectField5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectField5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectField5.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Condition5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Condition5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Condition5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectValue5.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectValue5.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectValue5.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectValue5.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        ' ----

        RemoveHandler Combo_AndOr1.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr2.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr3.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr4.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr5.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr6.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr7.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr8.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr9.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr10.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr11.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr12.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr13.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr14.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr15.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr16.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr17.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr18.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr19.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged
        RemoveHandler Combo_AndOr20.SelectedIndexChanged, AddressOf Combo_AndOr_SelectedIndexChanged

        RemoveHandler Radio_Static1.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static2.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static3.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static4.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static5.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static6.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static7.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static8.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static9.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static10.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static11.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static12.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static13.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static14.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static15.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static16.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static17.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static18.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static19.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static20.CheckedChanged, AddressOf Radio_Static_CheckedChanged
        RemoveHandler Radio_Static21.CheckedChanged, AddressOf Radio_Static_CheckedChanged

        RemoveHandler Radio_Custom1.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom2.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom3.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom4.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom5.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom6.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom7.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom8.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom9.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom10.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom11.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom12.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom13.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom14.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom15.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom16.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom17.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom18.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom19.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom20.CheckedChanged, AddressOf Radio_Custom_CheckedChanged
        RemoveHandler Radio_Custom21.CheckedChanged, AddressOf Radio_Custom_CheckedChanged

        RemoveHandler Combo_SelectField1.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField2.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField3.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField4.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField5.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField6.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField7.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField8.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField9.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField10.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField11.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField12.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField13.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField14.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField15.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField16.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField17.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField18.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField19.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField20.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged
        RemoveHandler Combo_SelectField21.SelectedIndexChanged, AddressOf Combo_SelectField_SelectedIndexChanged

        RemoveHandler Combo_Condition1.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition2.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition3.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition4.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition5.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition6.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition7.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition8.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition9.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition10.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition11.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition12.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition13.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition14.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition15.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition16.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition17.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition18.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition19.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition20.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged
        RemoveHandler Combo_Condition21.SelectedIndexChanged, AddressOf Combo_Condition_SelectedIndexChanged

        RemoveHandler Combo_SelectValue1.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue2.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue3.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue4.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue5.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue6.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue7.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue8.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue9.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue10.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue11.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue12.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue13.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue14.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue15.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue16.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue17.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue18.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue19.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue20.TextChanged, AddressOf Combo_SelectValue_TextChanged
        RemoveHandler Combo_SelectValue21.TextChanged, AddressOf Combo_SelectValue_TextChanged

        RemoveHandler Combo_SelectValue1b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue2b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue3b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue4b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue5b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue6b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue7b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue8b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue9b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue10b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue11b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue12b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue13b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue14b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue15b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue16b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue17b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue18b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue19b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue20b.TextChanged, AddressOf Combo_SelectValueB_TextChanged
        RemoveHandler Combo_SelectValue21b.TextChanged, AddressOf Combo_SelectValueB_TextChanged

      Catch ex As Exception
      End Try

    End If

  End Sub

  Private Sub PaintTimer_Tick(ByVal Sender As Object, ByVal e As EventArgs)
    ' *******************************************************************************
    ' Tick event handler to control conditional update of Selection.
    '
    ' Motivation :
    ' Sometimes, when Selection parameters may be quickly updated, one wants to defer
    ' the Grid update until the Selection Changes have finished.
    '
    ' Each Selection Line maintains it's own parameter object, including the Integer array
    ' of matching Pertrac IDs. In order to improve performance, I have tried to limit the 
    ' amount of re-querying done for each selection line. Changing one selection parameter should 
    ' not cause the other lines to re-query. The result set is then obtained by logical 
    ' combination of the separate integer arrays.
    '
    ' Note.
    ' We want to run the Grid update if any of the Selection controls have changed, as
    ' this may have invalidated a select line, but we do not want the run the Selection 
    ' queries until the Update flags turn True (being subject to a 1 sec update delay).
    ' However once the Update queries turn True the Selection process must be run.
    ' This is the reason for the seemingly convoluted use of the 'SelectControlChanged' flag
    ' and the polling of the 'SelectionItems(ItemCounter).UpdateFlag' values.
    '
    ' *******************************************************************************
    Dim PendingUpdate As Boolean = False
    Dim ItemCounter As Integer

    ' Already running or Use Fixed ID Array ?

    Try
      If (In_PaintTimer_Tick) OrElse (UseFixedIdArray) Then
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    ' Update Flags pending ?

    For ItemCounter = 0 To (SelectionItems.Length - 1)
      If (SelectionItems(ItemCounter).UpdateFlag) Then
        PendingUpdate = True
        Exit For
      End If
    Next

    ' Select controls changed ?
    If (Not SelectControlChanged) And (Not PendingUpdate) Then
      Exit Sub
    End If

    ' OK, run selection process as appropriate.

    Try
      Dim UpdatePerformed As Boolean = False

      In_PaintTimer_Tick = True
      Me.Cursor = Cursors.WaitCursor

      For ItemCounter = 0 To (SelectionItems.Length - 1)
        If (SelectionItems(ItemCounter).SelectionIsValid = False) AndAlso (LastIsValidStatus(ItemCounter)) Then
          SelectionItems(ItemCounter).ID_Array = Array.CreateInstance(GetType(Integer), 0)
          UpdatePerformed = True
        End If
        LastIsValidStatus(ItemCounter) = SelectionItems(ItemCounter).SelectionIsValid

        If (SelectionItems(ItemCounter).UpdateFlag) Then

          If (SelectionItems(ItemCounter).IsStaticField) Then
            SelectionItems(ItemCounter).ID_Array = GetStaticFieldSelection(SelectionItems(ItemCounter))
          ElseIf (SelectionItems(ItemCounter).IsCustomField) Then
            SelectionItems(ItemCounter).ID_Array = GetCustomFieldSelection(SelectionItems(ItemCounter))
          End If

          SelectionItems(ItemCounter).UpdateFlag = False
          UpdatePerformed = True
        End If
      Next

      If UpdatePerformed Then
        SetCompoundIDArray()

        PaintGrid()

        RaiseEvent SearchUpdateEvent(Me, CustomGroupID_CompoundArray)

        MainForm.PostNewGroupUpdateRequest(DefaultStatsDatePeriod, RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_CompoundArray, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median), Me, AddressOf GroupStatsUpdateEvent, MainForm.StatFunctions.GetDynamicGroupMemberCount(CustomGroupID_CompoundArray), -1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

      End If

    Catch ex As Exception
    Finally
      In_PaintTimer_Tick = False
      SelectControlChanged = False
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub GroupStatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer, ByVal IsInterimUpdate As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)
    ' *****************************************************************************************
    ' When the Stats engine updates stats for this 'CustomGroupID_CompoundArray', notify dependant
    ' forms so that they may update their 'Universe' chart lines.
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        RaiseEvent StatsUpdateEvent(sender, GroupID)

      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' *****************************************************************************************
    ' Routine to handle changes / updates to tables by this and other windows.
    ' If this, or any other, form posts a change to a table, then it will invoke an update event 
    ' detailing what tables have been altered.
    ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
    ' the 'VeniceAutoUpdate' event of the main Venice form.
    ' Each form may them react as appropriate to changes in any table that might impact it.
    '
    ' *****************************************************************************************

    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    Try
      OrgInPaint = InPaint

      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
        RefreshForm = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************


      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        ' Clear SaveToGroup Menu (Permissions may have changed)

        If (Menu_SaveToGroup.DropDownItems.Count > 0) Then
          Menu_SaveToGroup.DropDownItems.Clear()
        End If

        SetButtonStatus_Flag = True

        ' Check Custom Column Edit permissions
        Dim ColumnCount As Integer

        Try
          If (Me.HasUpdatePermission) Then
            Dim ColumnName As String

            For ColumnCount = 0 To (Grid_Results.Cols.Count - 1)
              ColumnName = Me.Grid_Results.Cols(ColumnCount).Name
              Dim Col_Is_Custom As Boolean = False
              Dim CustomFieldID As Integer = 0
              Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing

              If ColumnName.StartsWith("Custom_") Then
                Col_Is_Custom = True
                CustomFieldID = CInt(ColumnName.Substring(7))
                thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

                Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
                  Case PertracCustomFieldTypes.CustomBoolean
                    Grid_Results.Cols(ColumnCount).AllowEditing = True

                  Case PertracCustomFieldTypes.CustomDate
                    Grid_Results.Cols(ColumnCount).AllowEditing = True

                  Case PertracCustomFieldTypes.CustomNumeric
                    Grid_Results.Cols(ColumnCount).AllowEditing = True

                  Case PertracCustomFieldTypes.CustomString
                    Grid_Results.Cols(ColumnCount).AllowEditing = True
                End Select
              End If
            Next
          Else
            For ColumnCount = 0 To (Grid_Results.Cols.Count - 1)
              Grid_Results.Cols(ColumnCount).AllowEditing = False
            Next
          End If
        Catch ex As Exception
        End Try

      End If

      ' tblPertracCustomFields

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) Or KnowledgeDateChanged Then
        Dim ExistingSelectedFieldIDs() As Integer
        Dim FieldKeys() As Integer

        ' Update Grid_CustomFields_Display_Dictionary for new or removed Custom fields

        If (Grid_CustomFields_Display_Dictionary.Count > 0) Then
          Dim FieldCounter As Integer

          ReDim FieldKeys(Grid_CustomFields_Display_Dictionary.Count - 1)
          ReDim ExistingSelectedFieldIDs(Grid_CustomFields_Display_Dictionary.Count - 1)

          Grid_CustomFields_Display_Dictionary.Keys.CopyTo(FieldKeys, 0)

          For FieldCounter = 0 To (FieldKeys.Length - 1)
            ExistingSelectedFieldIDs(FieldCounter) = (-1)

            If Grid_CustomFields_Display_Dictionary(FieldKeys(FieldCounter)) Then
              ExistingSelectedFieldIDs(FieldCounter) = FieldKeys(FieldCounter)
            End If
          Next

          Grid_CustomFields_Display_Dictionary.Clear()
          RootMenu.Items.Remove(CustomFieldsMenu)
          CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)
          DrilldownMenu = SetDrillDownMenu(RootMenu)

          For FieldCounter = 0 To (ExistingSelectedFieldIDs.Length - 1)
            If (ExistingSelectedFieldIDs(FieldCounter) >= 0) Then
              SetCustomMenuChecked(ExistingSelectedFieldIDs(FieldCounter).ToString)
            End If
          Next

        Else
          Grid_CustomFields_Display_Dictionary.Clear()
          RootMenu.Items.Remove(CustomFieldsMenu)
          CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)
          DrilldownMenu = SetDrillDownMenu(RootMenu)
        End If

        ' Update Field Combos.

        Try
          Dim thisRadioPanel As Panel
          Dim RadioCounter As Integer
          Dim thisCustomRadio As RadioButton
          Dim thisSelectField As ComboBox

          For RadioCounter = 1 To SELECTION_ITEM_COUNT
            Try
              thisRadioPanel = CType(Panel_SearchInstruments.Controls("Panel" & RadioCounter.ToString), Panel)
              thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & RadioCounter.ToString), RadioButton)

              If (thisCustomRadio IsNot Nothing) AndAlso (thisCustomRadio.Checked) Then
                thisSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & RadioCounter.ToString), ComboBox)

                MainForm.SetTblGenericCombo(thisSelectField, _
                 RenaissanceStandardDatasets.tblPertracCustomFields, _
                 "FieldName", _
                 "FieldID", _
                 "", True, True, True, 0, "")
              End If

            Catch ex As Exception
            End Try
          Next

        Catch ex As Exception

        End Try
      End If

      ' Information

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Information) = True) Then
        Dim SelectCounter As Integer

        For SelectCounter = 0 To (SelectionItems.Length - 1)
          If (SelectionItems(SelectCounter).IsStaticField) Then
            If (SelectionItems(SelectCounter).NumericValuesTable IsNot Nothing) Then
              SyncLock SelectionItems(SelectCounter).NumericValuesTable
                SelectionItems(SelectCounter).NumericValuesTable = Nothing
              End SyncLock
            End If

            SelectionItems(SelectCounter).UpdateFlag = True
          End If
        Next
      End If

      ' tblPertracCustomFieldData

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFieldData) = True) Or KnowledgeDateChanged Then
        Dim SelectCounter As Integer

        ' CustomField_DataCache

        For SelectCounter = 0 To (SelectionItems.Length - 1)
          If (SelectionItems(SelectCounter).IsCustomField) Then
            If (SelectionItems(SelectCounter).NumericValuesTable IsNot Nothing) Then
              SyncLock SelectionItems(SelectCounter).NumericValuesTable
                SelectionItems(SelectCounter).NumericValuesTable = Nothing
              End SyncLock
            End If

            If (Menu_Options_CustomFieldChanges.Checked) Then
              SelectionItems(SelectCounter).UpdateFlag = True
            End If
          End If
        Next
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaSavedSearchList) = True) Then
        BuildSavedSearchesMenu()
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaSavedSearchItems) = True) Then
        If (CurrentSavedSearchID > 0) Then
          Load_SavedSearch(CurrentSavedSearchID)
        End If
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Then
        If (Menu_SaveToGroup.DropDownItems.Count > 0) Then
          Menu_SaveToGroup.DropDownItems.Clear()
        End If
      End If

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm) OrElse (SetButtonStatus_Flag) Then
      If (FormChanged = False) Then

        SetCompoundIDArray()

        PaintGrid()

        RaiseEvent SearchUpdateEvent(Me, CustomGroupID_CompoundArray)

      End If

      Call SetButtonStatus()
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "


  ' Check User permissions
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

#End Region

#Region " Form Control Events"

  Private Sub Radio_Static_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles Radio_Static1.CheckedChanged
    ' *****************************************************************************************
    ' Genericised CheckedChanged Event for 'Radio_Static<n>' Controls.
    ' For this to work, it is vital that the naming convention of the select controls remains 
    ' consistent!
    '
    ' 
    ' *****************************************************************************************

    Dim thisRadioPanel As Panel
    Dim thisStaticRadio As RadioButton
    Dim thisCustomRadio As RadioButton
    Dim thisComboSelectField As ComboBox = Nothing
    Dim thisComboCondition As ComboBox = Nothing
    Dim thisComboSelectValue As ComboBox = Nothing
    Dim IndexNumber As Integer

    Try
      thisStaticRadio = CType(sender, RadioButton)

      If (thisStaticRadio IsNot Nothing) Then

        If (thisStaticRadio.Name.StartsWith("Radio_Static")) Then
          IndexNumber = CInt(thisStaticRadio.Name.Substring("Radio_Static".Length))
        Else
          ' ???
          Exit Sub
        End If

        If (thisStaticRadio.Checked) Then
          ' Collect references to relevant controls.

          Try
            thisRadioPanel = CType(thisStaticRadio.Parent, Panel)
            thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & IndexNumber.ToString), RadioButton)

            thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & IndexNumber.ToString), ComboBox)
            thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & IndexNumber.ToString), ComboBox)
            thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString), ComboBox)

            If (thisCustomRadio IsNot Nothing) Then
              thisCustomRadio.Checked = False
            End If

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.DataSource = Nothing
            End If

          Catch ex As Exception
          End Try

          ' Set Select Field Combo.

          MainForm.SetTblGenericCombo(thisComboSelectField, _
           RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracFieldMapping, _
           "FCP_FieldName", _
           "FCP_FieldName", _
           "", True, True, True, "", "")

          Try
            ' Set basic Combo values for this select line.

            If (thisComboSelectField IsNot Nothing) Then
              thisComboSelectField.SelectedIndex = 0
            End If

            If (thisComboCondition IsNot Nothing) Then
              thisComboCondition.SelectedIndex = 0
            End If

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.Text = ""
            End If

          Catch ex As Exception
          End Try

          SelectionItems(IndexNumber - 1).IsStaticField = thisStaticRadio.Checked
          SelectControlChanged = True
        End If

      End If ' thisStaticRadio ISNOT Nothing

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Radio_Static_CheckedChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

    CurrentSavedSearchID = 0

  End Sub

  Private Sub Radio_Custom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles Radio_Custom1.CheckedChanged
    ' *****************************************************************************************
    ' Genericised CheckedChanged Event for 'Radio_Custom<n>' Controls.
    ' For this to work, it is vital that the naming convention of the select controls remains 
    ' consistent!
    ' *****************************************************************************************

    Dim thisRadioPanel As Panel
    Dim thisStaticRadio As RadioButton
    Dim thisCustomRadio As RadioButton
    Dim thisComboSelectField As ComboBox = Nothing
    Dim thisComboCondition As ComboBox = Nothing
    Dim thisComboSelectValue As ComboBox = Nothing
    Dim IndexNumber As Integer

    Try
      thisCustomRadio = CType(sender, RadioButton)

      If (thisCustomRadio IsNot Nothing) Then

        If (thisCustomRadio.Name.StartsWith("Radio_Custom")) Then
          IndexNumber = CInt(thisCustomRadio.Name.Substring("Radio_Custom".Length))
        Else
          ' ???
          Exit Sub
        End If

        If (thisCustomRadio.Checked) Then
          ' Get Object references relating to this Select Line.

          Try
            thisRadioPanel = CType(thisCustomRadio.Parent, Panel)
            thisStaticRadio = CType(thisRadioPanel.Controls("Radio_Static" & IndexNumber.ToString), RadioButton)
            thisComboSelectField = CType(Panel_SearchInstruments.Controls("Combo_SelectField" & IndexNumber.ToString), ComboBox)
            thisComboCondition = CType(Panel_SearchInstruments.Controls("Combo_Condition" & IndexNumber.ToString), ComboBox)
            thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString), ComboBox)

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.DataSource = Nothing
            End If

            If (thisStaticRadio IsNot Nothing) Then
              thisStaticRadio.Checked = False
            End If

          Catch ex As Exception
          End Try

          ' Set SelectField Combo.

          MainForm.SetTblGenericCombo(thisComboSelectField, _
           RenaissanceStandardDatasets.tblPertracCustomFields, _
           "FieldName", _
           "FieldID", _
           "FieldIsSearchable<>0", True, True, True, 0, "")

          ' Set Default Selection values.

          Try
            If (thisComboSelectField IsNot Nothing) Then
              thisComboSelectField.SelectedIndex = 0
            End If

            If (thisComboCondition IsNot Nothing) Then
              thisComboCondition.SelectedIndex = 0
            End If

            If (thisComboSelectValue IsNot Nothing) Then
              thisComboSelectValue.Text = ""
            End If

          Catch ex As Exception
          End Try

          SelectionItems(IndexNumber - 1).IsCustomField = thisCustomRadio.Checked
          SelectControlChanged = True
        End If

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Radio_Custom_CheckedChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

    CurrentSavedSearchID = 0

  End Sub

  ' --

  Private Sub SetStaticMenuChecked(ByRef pFieldname As String)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If System.Enum.IsDefined(GetType(PertracInformationFields), pFieldname) Then
        Dim StaticFieldID As PertracInformationFields
        StaticFieldID = System.Enum.Parse(GetType(PertracInformationFields), pFieldname)

        Try
          CType(StaticFieldsMenu.DropDownItems("Menu_StaticField_" & StaticFieldID.ToString), ToolStripMenuItem).Checked = True
        Catch ex As Exception
        End Try
        Grid_StaticFields_Display(CInt(StaticFieldID)) = True
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", SetStaticMenuChecked", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub SetCustomMenuChecked(ByRef pFieldID As String)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim FieldID As Integer

    Try

      If IsNumeric(pFieldID) = False Then
        Exit Sub
      End If

      FieldID = CInt(pFieldID)

      Try
        CType(CustomFieldsMenu.DropDownItems("Menu_CustomField_" & FieldID.ToString), ToolStripMenuItem).Checked = True
      Catch ex As Exception
      End Try

      Grid_CustomFields_Display_Dictionary(FieldID) = True

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", SetCustomMenuChecked", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Combo_AndOr_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Not (InPaint Or UseFixedIdArray)) Then
        SetCompoundIDArray()
        PaintGrid()
        CurrentSavedSearchID = 0

        RaiseEvent SearchUpdateEvent(Me, CustomGroupID_CompoundArray) ' Update Event.

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Combo_AndOr_SelectedIndexChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_SelectField_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles Combo_SelectField1.SelectedIndexChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim thisComboSelectField As ComboBox = Nothing
    Dim thisRadioPanel As Panel
    Dim thisStaticRadio As RadioButton = Nothing
    Dim thisCustomRadio As RadioButton = Nothing
    Dim thisComboSelectValue As ComboBox = Nothing
    Dim thisComboSelectValueB As ComboBox = Nothing
    Dim IndexNumber As Integer
    Dim SelectionItemsNumber As Integer

    If (InPaint = False) Then
      CurrentSavedSearchID = 0

      Try
        thisComboSelectField = CType(sender, ComboBox)

        If (thisComboSelectField IsNot Nothing) Then

          If (thisComboSelectField.Name.StartsWith("Combo_SelectField")) Then
            IndexNumber = CInt(thisComboSelectField.Name.Substring("Combo_SelectField".Length))
            SelectionItemsNumber = IndexNumber - 1
          Else
            ' ???
            Exit Sub
          End If

          thisRadioPanel = CType(Panel_SearchInstruments.Controls("Panel" & IndexNumber.ToString), Panel)
          If (thisRadioPanel IsNot Nothing) Then
            thisStaticRadio = CType(thisRadioPanel.Controls("Radio_Static" & IndexNumber.ToString), RadioButton)
            thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & IndexNumber.ToString), RadioButton)
          End If
          thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString), ComboBox)
          thisComboSelectValueB = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString & "b"), ComboBox)

          If (thisStaticRadio IsNot Nothing) AndAlso (thisCustomRadio IsNot Nothing) AndAlso (thisComboSelectValue IsNot Nothing) AndAlso (thisComboSelectValueB IsNot Nothing) Then

            If (thisComboSelectField.SelectedIndex < 0) Then
              SelectionItems(SelectionItemsNumber).SelectFieldID = ""

            ElseIf (thisComboSelectField.SelectedValue Is Nothing) Then
              SelectionItems(SelectionItemsNumber).SelectFieldID = ""

            ElseIf (thisComboSelectField.SelectedValue.ToString = "") Then
              SelectionItems(SelectionItemsNumber).SelectFieldID = ""

            Else
              SelectionItems(SelectionItemsNumber).SelectFieldID = thisComboSelectField.SelectedValue.ToString

              If thisStaticRadio.Checked Then
                If (Menu_Options_SetValuecombos.Checked) Then
                  Dim InformationField As PertracInformationFields

                  If System.Enum.IsDefined(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItems(SelectionItemsNumber).SelectFieldID) Then
                    InformationField = System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItems(SelectionItemsNumber).SelectFieldID)
                  End If

                  Select Case InformationField
                    Case PertracInformationFields.Address1, PertracInformationFields.Address2, PertracInformationFields.CompanyName, PertracInformationFields.Contact, PertracInformationFields.ContactFax, PertracInformationFields.ContactPhone, _
                     PertracInformationFields.DataVendorID, PertracInformationFields.Description, PertracInformationFields.Email, PertracInformationFields.FirmAssets, PertracInformationFields.FundAssets, PertracInformationFields.FundName, PertracInformationFields.Hurdle, _
                     PertracInformationFields.ManagerName, PertracInformationFields.Mastername, PertracInformationFields.MarketingContact, PertracInformationFields.MarketingFax, PertracInformationFields.MarketingPhone, PertracInformationFields.UserDescription, PertracInformationFields.Website, PertracInformationFields.ZipCode

                      thisComboSelectValue.DataSource = Nothing
                      If (thisComboSelectValueB.Visible) Then
                        thisComboSelectValueB.DataSource = Nothing
                      End If

                    Case PertracInformationFields.DataPeriod

                      thisComboSelectValue.DataSource = System.Enum.GetNames(GetType(RenaissanceGlobals.DealingPeriod))
                      If (thisComboSelectValueB.Visible) Then
                        thisComboSelectValueB.DataSource = System.Enum.GetNames(GetType(RenaissanceGlobals.DealingPeriod))
                      End If

                    Case Else

                      thisComboSelectValue.DataSource = GetStaticFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                      If (thisComboSelectValueB.Visible) Then
                        thisComboSelectValueB.DataSource = GetStaticFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                      End If

                  End Select

                End If
                SetStaticMenuChecked(SelectionItems(SelectionItemsNumber).SelectFieldID)

              ElseIf thisCustomRadio.Checked Then
                If (Menu_Options_SetValuecombos.Checked) Then
                  thisComboSelectValue.DataSource = GetCustomFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                  If (thisComboSelectValueB.Visible) Then
                    thisComboSelectValueB.DataSource = GetCustomFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                  End If
                End If

                If (thisComboSelectField.SelectedValue IsNot Nothing) AndAlso (CInt(thisComboSelectField.SelectedValue) > 0) Then
                  SetCustomMenuChecked(SelectionItems(SelectionItemsNumber).SelectFieldID)
                End If
              End If

            End If

          End If ' Controls not nothing

        End If ' thisComboSelectField IsNot Nothing

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", Combo_SelectField_SelectedIndexChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      End Try

      Try
        If (thisComboSelectValueB IsNot Nothing) AndAlso (thisComboSelectValueB.Visible = False) Then
          thisComboSelectValueB.Text = ""
        End If
      Catch ex As Exception
        MainForm.LogError(Me.Name & ", Combo_SelectField_SelectedIndexChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      End Try

      SelectControlChanged = True
    End If
  End Sub

  Private Sub Combo_Condition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles Combo_Condition1.SelectedIndexChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim thisComboCondition As ComboBox
    Dim thisRadioPanel As Panel
    Dim thisStaticRadio As RadioButton = Nothing
    Dim thisCustomRadio As RadioButton = Nothing
    Dim thisComboSelectValue As ComboBox = Nothing
    Dim thisComboSelectValueB As ComboBox = Nothing

    Dim IndexNumber As Integer
    Dim SelectionItemsNumber As Integer

    Try
      CurrentSavedSearchID = 0

      ' Get Applicable controls :

      thisComboCondition = CType(sender, ComboBox)

      If (thisComboCondition IsNot Nothing) Then

        If (thisComboCondition.Name.StartsWith("Combo_Condition")) Then
          IndexNumber = CInt(thisComboCondition.Name.Substring("Combo_Condition".Length))
          SelectionItemsNumber = IndexNumber - 1
        Else
          ' ???
          Exit Sub
        End If

        thisRadioPanel = CType(Panel_SearchInstruments.Controls("Panel" & IndexNumber.ToString), Panel)
        If (thisRadioPanel IsNot Nothing) Then
          thisStaticRadio = CType(thisRadioPanel.Controls("Radio_Static" & IndexNumber.ToString), RadioButton)
          thisCustomRadio = CType(thisRadioPanel.Controls("Radio_Custom" & IndexNumber.ToString), RadioButton)
        End If
        thisComboSelectValue = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString), ComboBox)
        thisComboSelectValueB = CType(Panel_SearchInstruments.Controls("Combo_SelectValue" & IndexNumber.ToString & "b"), ComboBox)

        ' Action :

        If (thisStaticRadio IsNot Nothing) AndAlso (thisCustomRadio IsNot Nothing) AndAlso (thisComboSelectValue IsNot Nothing) AndAlso (thisComboSelectValueB IsNot Nothing) Then

          If (InPaint = False) Then
            If (thisComboCondition.SelectedIndex < 0) Then
              SelectionItems(SelectionItemsNumber).ConditionOperand = ""
            Else
              SelectionItems(SelectionItemsNumber).ConditionOperand = thisComboCondition.Text
            End If

            SelectControlChanged = True
          End If

          If (thisComboCondition.Text.StartsWith("Between")) Then
            thisComboSelectValue.Width = 200
            thisComboSelectValueB.Visible = True

            If (SelectionItems(SelectionItemsNumber).SelectFieldID.Length > 0) Then
              If thisStaticRadio.Checked Then
                If (Menu_Options_SetValuecombos.Checked) Then
                  If (thisComboSelectValueB.Visible) Then
                    thisComboSelectValueB.DataSource = GetStaticFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                  End If
                End If

              ElseIf thisCustomRadio.Checked Then
                If (Menu_Options_SetValuecombos.Checked) Then
                  If (thisComboSelectValueB.Visible) Then
                    thisComboSelectValueB.DataSource = GetCustomFieldDistinctStrings(SelectionItems(SelectionItemsNumber))
                  End If
                End If
              End If
            End If
          Else
            thisComboSelectValue.Width = 406
            thisComboSelectValueB.Visible = False
          End If

        End If

      End If ' thisComboCondition IsNot Nothing

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Combo_Condition_SelectedIndexChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Combo_SelectValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles Combo_SelectValue1.TextChanged
    If (InPaint = False) Then
      ' *****************************************************************************************
      '
      '
      ' *****************************************************************************************
      Dim thisComboSelectValue As ComboBox = Nothing

      Dim IndexNumber As Integer
      Dim SelectionItemsNumber As Integer

      Try
        CurrentSavedSearchID = 0

        thisComboSelectValue = CType(sender, ComboBox)

        If (thisComboSelectValue IsNot Nothing) Then

          If (thisComboSelectValue.Name.StartsWith("Combo_SelectValue")) Then
            IndexNumber = CInt(thisComboSelectValue.Name.Substring("Combo_SelectValue".Length))
            SelectionItemsNumber = IndexNumber - 1
          Else
            ' ???
            Exit Sub
          End If

          SelectionItems(SelectionItemsNumber).SelectValue = thisComboSelectValue.Text
          If (thisComboSelectValue.SelectedIndex < 0) Then
            thisComboSelectValue.Tag = Nothing
          End If

        End If ' thisComboSelectValue IsNot Nothing

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", Combo_SelectValue_TextChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      End Try

      SelectControlChanged = True
    End If
  End Sub

  Private Sub Combo_SelectValueB_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles Combo_SelectValue1b.TextChanged
    If (InPaint = False) Then
      ' *****************************************************************************************
      '
      '
      ' *****************************************************************************************

      Dim thisComboSelectValue As ComboBox = Nothing

      Dim IndexNumber As Integer
      Dim SelectionItemsNumber As Integer

      Try
        CurrentSavedSearchID = 0

        thisComboSelectValue = CType(sender, ComboBox)

        If (thisComboSelectValue IsNot Nothing) Then

          If (thisComboSelectValue.Name.StartsWith("Combo_SelectValue")) Then
            IndexNumber = CInt(thisComboSelectValue.Name.Substring("Combo_SelectValue".Length, thisComboSelectValue.Name.Length - ("Combo_SelectValueB".Length)))
            SelectionItemsNumber = IndexNumber - 1
          Else
            ' ???
            Exit Sub
          End If

          SelectionItems(SelectionItemsNumber).SelectValue2 = thisComboSelectValue.Text
          If (thisComboSelectValue.SelectedIndex < 0) Then
            thisComboSelectValue.Tag = Nothing
          End If

        End If ' thisComboSelectValue IsNot Nothing

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", Combo_SelectValueB_TextChanged", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      End Try

      SelectControlChanged = True
    End If
  End Sub

#Region " Old Code"

  'Private Sub Combo_SelectField2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectField2.SelectedIndexChanged
  '	If (InPaint = False) Then

  '		Try
  '			If (Combo_SelectField2.SelectedIndex < 0) Then
  '				SelectionItems(1).SelectFieldID = ""
  '			ElseIf (Combo_SelectField2.SelectedValue Is Nothing) Then
  '				SelectionItems(1).SelectFieldID = ""
  '			Else
  '				SelectionItems(1).SelectFieldID = Combo_SelectField2.SelectedValue.ToString

  '				If Me.Radio_Static2.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue2.DataSource = GetStaticFieldDistinctStrings(SelectionItems(1))
  '						If (Combo_SelectValue1b.Visible) Then
  '							Me.Combo_SelectValue2b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(1))
  '						End If
  '					End If
  '					SetStaticMenuChecked(SelectionItems(1).SelectFieldID)

  '				ElseIf Me.Radio_Custom2.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue2.DataSource = GetCustomFieldDistinctStrings(SelectionItems(1))
  '						If (Combo_SelectValue2b.Visible) Then
  '							Me.Combo_SelectValue2b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(1))
  '						End If
  '					End If

  '					If (Combo_SelectField2.SelectedValue IsNot Nothing) AndAlso (CInt(Combo_SelectField2.SelectedValue) > 0) Then
  '						SetCustomMenuChecked(SelectionItems(1).SelectFieldID)
  '					End If
  '				End If

  '			End If
  '		Catch ex As Exception
  '		End Try

  '		If Combo_SelectValue2b.Visible = False Then
  '			Combo_SelectValue2b.Text = ""
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_Condition2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Condition2.SelectedIndexChanged
  '	If (InPaint = False) Then
  '		If (Combo_Condition2.SelectedIndex < 0) Then
  '			SelectionItems(1).ConditionOperand = ""
  '		Else
  '			SelectionItems(1).ConditionOperand = Combo_Condition2.Text
  '		End If

  '		SelectControlChanged = True
  '	End If

  '	If (Combo_Condition2.Text.StartsWith("Between")) Then
  '		Me.Combo_SelectValue2.Width = 200
  '		Me.Combo_SelectValue2b.Visible = True

  '		If (SelectionItems(1).SelectFieldID.Length > 0) Then
  '			If Me.Radio_Static2.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue2b.Visible) Then
  '						Me.Combo_SelectValue2b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(1))
  '					End If
  '				End If

  '			ElseIf Me.Radio_Custom2.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue2b.Visible) Then
  '						Me.Combo_SelectValue2b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(1))
  '					End If
  '				End If
  '			End If
  '		End If

  '	Else
  '		Me.Combo_SelectValue2.Width = 406
  '		Me.Combo_SelectValue2b.Visible = False
  '	End If

  'End Sub

  'Private Sub Combo_SelectValue2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue2.TextChanged
  '	If (InPaint = False) Then
  '		SelectionItems(1).SelectValue = Combo_SelectValue2.Text
  '		If (Combo_SelectValue2.SelectedIndex < 0) Then
  '			Combo_SelectValue2.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectValue2b_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue2b.TextChanged
  '	If (InPaint = False) Then
  '		' *****************************************************************************************
  '		'
  '		'
  '		' *****************************************************************************************

  '		SelectionItems(1).SelectValue2 = Combo_SelectValue2b.Text
  '		If (Combo_SelectValue2b.SelectedIndex < 0) Then
  '			Combo_SelectValue2b.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectField3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectField3.SelectedIndexChanged
  '	If (InPaint = False) Then

  '		Try
  '			If (Combo_SelectField3.SelectedIndex < 0) Then
  '				SelectionItems(2).SelectFieldID = ""
  '			ElseIf (Combo_SelectField3.SelectedValue Is Nothing) Then
  '				SelectionItems(2).SelectFieldID = ""
  '			Else
  '				SelectionItems(2).SelectFieldID = Combo_SelectField3.SelectedValue.ToString

  '				If Me.Radio_Static3.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue3.DataSource = GetStaticFieldDistinctStrings(SelectionItems(2))
  '						If (Combo_SelectValue3b.Visible) Then
  '							Me.Combo_SelectValue3b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(2))
  '						End If
  '					End If
  '					SetStaticMenuChecked(SelectionItems(2).SelectFieldID)

  '				ElseIf Me.Radio_Custom3.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue3.DataSource = GetCustomFieldDistinctStrings(SelectionItems(2))
  '						If (Combo_SelectValue3b.Visible) Then
  '							Me.Combo_SelectValue3b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(2))
  '						End If
  '					End If

  '					If (Combo_SelectField3.SelectedValue IsNot Nothing) AndAlso (CInt(Combo_SelectField3.SelectedValue) > 0) Then
  '						SetCustomMenuChecked(SelectionItems(2).SelectFieldID)
  '					End If
  '				End If

  '			End If
  '		Catch ex As Exception
  '		End Try

  '		If Combo_SelectValue3b.Visible = False Then
  '			Combo_SelectValue3b.Text = ""
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_Condition3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Condition3.SelectedIndexChanged
  '	If (InPaint = False) Then
  '		If (Combo_Condition3.SelectedIndex < 0) Then
  '			SelectionItems(2).ConditionOperand = ""
  '		Else
  '			SelectionItems(2).ConditionOperand = Combo_Condition3.Text
  '		End If

  '		SelectControlChanged = True
  '	End If

  '	If (Combo_Condition3.Text.StartsWith("Between")) Then
  '		Me.Combo_SelectValue3.Width = 200
  '		Me.Combo_SelectValue3b.Visible = True

  '		If (SelectionItems(2).SelectFieldID.Length > 0) Then
  '			If Me.Radio_Static3.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue3b.Visible) Then
  '						Me.Combo_SelectValue3b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(2))
  '					End If
  '				End If

  '			ElseIf Me.Radio_Custom3.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue3b.Visible) Then
  '						Me.Combo_SelectValue3b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(2))
  '					End If
  '				End If
  '			End If
  '		End If

  '	Else
  '		Me.Combo_SelectValue3.Width = 406
  '		Me.Combo_SelectValue3b.Visible = False
  '	End If

  'End Sub

  'Private Sub Combo_SelectValue3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue3.TextChanged
  '	If (InPaint = False) Then
  '		SelectionItems(2).SelectValue = Combo_SelectValue3.Text
  '		If (Combo_SelectValue3.SelectedIndex < 0) Then
  '			Combo_SelectValue3.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectValue3b_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue3b.TextChanged
  '	If (InPaint = False) Then
  '		' *****************************************************************************************
  '		'
  '		'
  '		' *****************************************************************************************

  '		SelectionItems(2).SelectValue2 = Combo_SelectValue3b.Text
  '		If (Combo_SelectValue3b.SelectedIndex < 0) Then
  '			Combo_SelectValue3b.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectField4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectField4.SelectedIndexChanged
  '	If (InPaint = False) Then

  '		Try
  '			If (Combo_SelectField4.SelectedIndex < 0) Then
  '				SelectionItems(3).SelectFieldID = ""
  '			ElseIf (Combo_SelectField4.SelectedValue Is Nothing) Then
  '				SelectionItems(3).SelectFieldID = ""
  '			Else
  '				SelectionItems(3).SelectFieldID = Combo_SelectField4.SelectedValue.ToString

  '				If Me.Radio_Static4.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue4.DataSource = GetStaticFieldDistinctStrings(SelectionItems(3))
  '						If (Combo_SelectValue4b.Visible) Then
  '							Me.Combo_SelectValue4b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(3))
  '						End If
  '					End If
  '					SetStaticMenuChecked(SelectionItems(3).SelectFieldID)

  '				ElseIf Me.Radio_Custom4.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue4.DataSource = GetCustomFieldDistinctStrings(SelectionItems(3))
  '						If (Combo_SelectValue4b.Visible) Then
  '							Me.Combo_SelectValue4b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(3))
  '						End If
  '					End If

  '					If (Combo_SelectField4.SelectedValue IsNot Nothing) AndAlso (CInt(Combo_SelectField4.SelectedValue) > 0) Then
  '						SetCustomMenuChecked(SelectionItems(3).SelectFieldID)
  '					End If
  '				End If

  '			End If
  '		Catch ex As Exception
  '		End Try

  '		If Combo_SelectValue4b.Visible = False Then
  '			Combo_SelectValue4b.Text = ""
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_Condition4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Condition4.SelectedIndexChanged
  '	If (InPaint = False) Then
  '		If (Combo_Condition4.SelectedIndex < 0) Then
  '			SelectionItems(3).ConditionOperand = ""
  '		Else
  '			SelectionItems(3).ConditionOperand = Combo_Condition4.Text
  '		End If

  '		SelectControlChanged = True
  '	End If

  '	If (Combo_Condition4.Text.StartsWith("Between")) Then
  '		Me.Combo_SelectValue4.Width = 200
  '		Me.Combo_SelectValue4b.Visible = True

  '		If (SelectionItems(3).SelectFieldID.Length > 0) Then
  '			If Me.Radio_Static4.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue4b.Visible) Then
  '						Me.Combo_SelectValue4b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(3))
  '					End If
  '				End If

  '			ElseIf Me.Radio_Custom4.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue4b.Visible) Then
  '						Me.Combo_SelectValue4b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(3))
  '					End If
  '				End If
  '			End If
  '		End If

  '	Else
  '		Me.Combo_SelectValue4.Width = 406
  '		Me.Combo_SelectValue4b.Visible = False
  '	End If

  'End Sub

  'Private Sub Combo_SelectValue4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue4.TextChanged
  '	If (InPaint = False) Then
  '		SelectionItems(3).SelectValue = Combo_SelectValue4.Text
  '		If (Combo_SelectValue4.SelectedIndex < 0) Then
  '			Combo_SelectValue4.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectValue4b_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue4b.TextChanged
  '	If (InPaint = False) Then
  '		' *****************************************************************************************
  '		'
  '		'
  '		' *****************************************************************************************

  '		SelectionItems(3).SelectValue2 = Combo_SelectValue4b.Text
  '		If (Combo_SelectValue4b.SelectedIndex < 0) Then
  '			Combo_SelectValue4b.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectField5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectField5.SelectedIndexChanged
  '	If (InPaint = False) Then

  '		Try
  '			If (Combo_SelectField5.SelectedIndex < 0) Then
  '				SelectionItems(4).SelectFieldID = ""
  '			ElseIf (Combo_SelectField5.SelectedValue Is Nothing) Then
  '				SelectionItems(4).SelectFieldID = ""
  '			Else
  '				SelectionItems(4).SelectFieldID = Combo_SelectField5.SelectedValue.ToString

  '				If Me.Radio_Static5.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue5.DataSource = GetStaticFieldDistinctStrings(SelectionItems(4))
  '						If (Combo_SelectValue5b.Visible) Then
  '							Me.Combo_SelectValue5b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(4))
  '						End If
  '					End If
  '					SetStaticMenuChecked(SelectionItems(4).SelectFieldID)

  '				ElseIf Me.Radio_Custom5.Checked Then
  '					If (Menu_Options_SetValuecombos.Checked) Then
  '						Me.Combo_SelectValue5.DataSource = GetCustomFieldDistinctStrings(SelectionItems(4))
  '						If (Combo_SelectValue5b.Visible) Then
  '							Me.Combo_SelectValue5b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(4))
  '						End If
  '					End If

  '					If (Combo_SelectField5.SelectedValue IsNot Nothing) AndAlso (CInt(Combo_SelectField5.SelectedValue) > 0) Then
  '						SetCustomMenuChecked(SelectionItems(4).SelectFieldID)
  '					End If
  '				End If

  '			End If
  '		Catch ex As Exception
  '		End Try

  '		If Combo_SelectValue5b.Visible = False Then
  '			Combo_SelectValue5b.Text = ""
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_Condition5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Condition5.SelectedIndexChanged
  '	If (InPaint = False) Then
  '		If (Combo_Condition5.SelectedIndex < 0) Then
  '			SelectionItems(4).ConditionOperand = ""
  '		Else
  '			SelectionItems(4).ConditionOperand = Combo_Condition5.Text
  '		End If

  '		SelectControlChanged = True
  '	End If

  '	If (Combo_Condition5.Text.StartsWith("Between")) Then
  '		Me.Combo_SelectValue5.Width = 200
  '		Me.Combo_SelectValue5b.Visible = True

  '		If (SelectionItems(4).SelectFieldID.Length > 0) Then
  '			If Me.Radio_Static5.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue5b.Visible) Then
  '						Me.Combo_SelectValue5b.DataSource = GetStaticFieldDistinctStrings(SelectionItems(4))
  '					End If
  '				End If

  '			ElseIf Me.Radio_Custom5.Checked Then
  '				If (Menu_Options_SetValuecombos.Checked) Then
  '					If (Combo_SelectValue5b.Visible) Then
  '						Me.Combo_SelectValue5b.DataSource = GetCustomFieldDistinctStrings(SelectionItems(4))
  '					End If
  '				End If
  '			End If
  '		End If

  '	Else
  '		Me.Combo_SelectValue5.Width = 406
  '		Me.Combo_SelectValue5b.Visible = False
  '	End If

  'End Sub

  'Private Sub Combo_SelectValue5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue5.TextChanged
  '	If (InPaint = False) Then
  '		SelectionItems(4).SelectValue = Combo_SelectValue5.Text
  '		If (Combo_SelectValue5.SelectedIndex < 0) Then
  '			Combo_SelectValue5.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

  'Private Sub Combo_SelectValue5b_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectValue5b.TextChanged
  '	If (InPaint = False) Then
  '		' *****************************************************************************************
  '		'
  '		'
  '		' *****************************************************************************************

  '		SelectionItems(4).SelectValue2 = Combo_SelectValue5b.Text
  '		If (Combo_SelectValue5b.SelectedIndex < 0) Then
  '			Combo_SelectValue5b.Tag = Nothing
  '		End If

  '		SelectControlChanged = True
  '	End If
  'End Sub

#End Region

  ' --

  Private Sub Panel_SearchInstruments_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles Panel_SearchInstruments.Scroll
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      ' Allow the Selection combos to repaint as the scroll bar scrolls.

      Application.DoEvents()
    Catch ex As Exception
    End Try

  End Sub

  ' --

  Private Sub Btn_ShowCarts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ShowCarts.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Split_Results.Panel2Collapsed = Not Split_Results.Panel2Collapsed

      If Split_Results.Panel2Collapsed Then
        Btn_ShowCarts.Text = "Show Charts"
      Else
        Btn_ShowCarts.Text = "Hide Charts"
        Split_Results_Resize(Split_Results, New EventArgs)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Split_Results_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Split_Results.Resize
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Chart_MonthlyReturns.Top = ((Split_Results.Panel2.Height - Label_ChartStock.Height) / 2) + Label_ChartStock.Height
      Me.Chart_MonthlyReturns.Height = (Split_Results.Panel2.Height - Label_ChartStock.Height) / 2
      Me.Chart_VAMI.Height = (Split_Results.Panel2.Height - Label_ChartStock.Height) / 2

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Grid_Results_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Results.CellChanged
    Dim Changed_Ordinal As Integer = Grid_Results.Cols.IndexOf("HasChanged")
    Dim CustomFieldID As Integer
    Dim ColumnName As String

    Try
      If (InPaint = False) AndAlso (e.Col <> Changed_Ordinal) AndAlso (e.Row > 0) Then

        SplitButton_Save.Visible = True
        SplitButton_Cancel.Visible = True
        Grid_Results.Item(e.Row, Changed_Ordinal) = "x"
        FormChanged = True

        ColumnName = Grid_Results.Cols(e.Col).Name
        If ColumnName.StartsWith("Custom_") AndAlso (IsNumeric(ColumnName.Substring(7))) Then
          CustomFieldID = CInt(ColumnName.Substring(7))

          If (CustomFieldsChanged.Length <= 0) OrElse (Not CustomFieldsChanged.EndsWith(",")) Then
            CustomFieldsChanged &= ","
          End If

          If (Not CustomFieldsChanged.Contains("," & CustomFieldID.ToString & ".0,")) Then
            CustomFieldsChanged &= CustomFieldID.ToString & ".0,"
          End If
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Results_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Results.EnterCell
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Static Dim lastPertracID As Integer

    Dim ThisPertracID As Integer

    If (InPaint = False) AndAlso (Grid_Results.Row >= 0) AndAlso (Grid_Results.Col >= 0) Then
      Try
        If (Grid_Results.Cols.Contains("ID")) Then
          Dim Col_GroupPertracCode As Integer = Grid_Results.Cols("ID").SafeIndex

          If (IsNumeric(Grid_Results.Item(Grid_Results.Row, Col_GroupPertracCode))) Then
            ThisPertracID = Grid_Results.Item(Grid_Results.Row, Col_GroupPertracCode)

            If (ThisPertracID <> lastPertracID) Then
              lastPertracID = ThisPertracID
              Label_ChartStock.Text = MainForm.PertracData.GetInformationValue(ThisPertracID, PertracInformationFields.FundName)

              Set_LineChart(MainForm, DefaultStatsDatePeriod, ThisPertracID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())

              Set_RollingReturnChart(MainForm, DefaultStatsDatePeriod, ThisPertracID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())

            End If
          End If
        End If

      Catch ex As Exception

      End Try
    End If
  End Sub

  Dim MouseDownPosition As System.Drawing.Point
  Dim MouseDownTime As Date

  Private Sub Grid_Results_BeforeMouseDown(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.BeforeMouseDownEventArgs) Handles Grid_Results.BeforeMouseDown
    ' *****************************************************************************************
    ' Handle the initiation of Drag-Grop functionality from the Fund Search Form.
    '
    ' Essentially, when Drag-Drop is initiated, the 'DoDragDrop' function is fed a comma serarated
    ' string of Pertrac IDs with a standard header string.
    ' *****************************************************************************************

    Dim ThisMouseRow As Integer = Grid_Results.MouseRow
    Dim RowSelected As Boolean = False
    Dim ThisRow As C1.Win.C1FlexGrid.Row
    Dim ID_Ordinal As Integer = Grid_Results.Cols.IndexOf("ID")

    MouseDownPosition = New Point(e.X, e.Y)
    MouseDownTime = Now

    If (ThisMouseRow > 0) AndAlso (Control.ModifierKeys = Keys.None) Then
      If (e.Button = Windows.Forms.MouseButtons.Left) AndAlso (e.Clicks = 1) Then
        If (Grid_Results.Rows.Selected.Count > 0) Then
          Dim SelectString As New System.Text.StringBuilder(GENOA_DRAG_IDs_HEADER, (Grid_Results.Rows.Selected.Count * 6) + 20)

          If (Grid_Results.Rows.Selected.Contains(Grid_Results.Rows(ThisMouseRow))) Then
            For Each ThisRow In Grid_Results.Rows.Selected
              If (ThisRow(ID_Ordinal) IsNot Nothing) AndAlso (IsNumeric(ThisRow(ID_Ordinal).ToString)) Then
                SelectString.Append("," & ThisRow(ID_Ordinal).ToString)
              End If
            Next

            Grid_Results.DoDragDrop(SelectString.ToString, DragDropEffects.Copy)
            e.Cancel = True

            Exit Sub
          End If
        End If
      End If
    End If
  End Sub

  Private Sub ViewRowsInBrowser(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim IDs() As Integer

      If (Grid_Results.Rows.Selected.Count > 0) Then
        Dim newBrowserForm As frmFundBrowser

        IDs = Array.CreateInstance(GetType(Integer), Grid_Results.Rows.Selected.Count)

        For RowCounter As Integer = 0 To (Grid_Results.Rows.Selected.Count - 1)
          IDs(RowCounter) = Grid_Results.Rows.Selected(RowCounter)("ID")
        Next

        newBrowserForm = CType(MainForm.New_GenoaForm(GenoaFormID.frmFundBrowser, False).Form, frmFundBrowser)

        Application.DoEvents()

        newBrowserForm.StartWithCustomSelection = True

        newBrowserForm.Show()

        newBrowserForm.SetCustomListInstrumentIDs(IDs)

      End If

    Catch ex As Exception

    End Try
  End Sub


  Private Sub Grid_Results_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Results.DoubleClick
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Grid_Results.Row <= 0) OrElse (Grid_Results.MouseRow <= 0) Then
        Exit Sub
      End If

      Dim newBrowserForm As frmFundBrowser
      Dim ThisPertracID As Integer
      Dim Col_GroupPertracCode As Integer = Grid_Results.Cols("ID").SafeIndex

      ThisPertracID = Grid_Results.Item(Grid_Results.Row, Col_GroupPertracCode)
      If (ThisPertracID <= 0) Then
        Exit Sub
      End If

      ''newBrowserForm = CType(MainForm.New_GenoaForm(GenoaFormID.frmFundBrowser, False).Form, frmFundBrowser)

      ''Application.DoEvents()

      ''newBrowserForm.StartWithCustomSelection = True

      ''newBrowserForm.Show()

      ''newBrowserForm.SetCustomListInstrumentIDs(New Integer() {ThisPertracID})

      newBrowserForm = MainForm.ShowForm_AddNew(GenoaFormID.frmFundBrowser)
      newBrowserForm.SetCustomListInstrumentIDs(New Integer() {ThisPertracID})


      'Dim InfoValues() As Object
      'InfoValues = MainForm.PertracData.GetInformationValues(ThisPertracID)

      'FundBrowserForm = MainForm.ShowForm_AddNew(GenoaFormID.frmFundBrowser)
      'FundBrowserForm.Selection_Pertrac = True
      'Application.DoEvents()

      'FundBrowserForm.SetSelectedInstrument(ThisPertracID, Nz(InfoValues(RenaissanceGlobals.PertracInformationFields.FundName), ""))

      newBrowserForm.Focus()

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Context Menu Code, Custom Field Item Copy. "

  Sub cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      ' Acquire references to the owning control and item.

      ' Dim c As Control = GridContextMenuStrip.SourceControl
      ' Dim tsi As ToolStripDropDownItem = GridContextMenuStrip.OwnerItem

      ' Clear the ContextMenuStrip control's 
      ' Items collection.

      GridContextMenuStrip.Items.Clear()

      Dim Col_Name As String
      Dim Col_Is_Custom As Boolean = False
      Dim CustomFieldID As Integer = 0
      Dim StaticFieldID As PertracInformationFields
      Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing

      GridClickMouseRow = Grid_Results.MouseRow
      GridClickMouseCol = Grid_Results.MouseCol

      If (GridClickMouseCol < 0) Then
        Exit Sub
      End If

      Col_Name = Me.Grid_Results.Cols(GridClickMouseCol).Name
      If Col_Name.StartsWith("Custom_") Then
        Col_Is_Custom = True
        CustomFieldID = CInt(Col_Name.Substring(7))
        thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")
      ElseIf Col_Name.StartsWith("Static_") Then
        Col_Is_Custom = False
        StaticFieldID = CType(System.Enum.Parse(GetType(PertracInformationFields), Col_Name.Substring(7)), PertracInformationFields)
      End If

      If FormChanged Then
        GridContextMenuStrip.Items.Add("Save Changes to Custom Fields", Nothing, AddressOf Event_SaveChanges)
        GridContextMenuStrip.Items.Add("Cancel Changes to Custom Fields", Nothing, AddressOf Event_CancelChanges)
        GridContextMenuStrip.Items.Add(New ToolStripSeparator)
      End If

      If (Col_Is_Custom) AndAlso (GridClickMouseRow > 0) AndAlso (thisCustomFieldDefinition IsNot Nothing) Then
        Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
          Case PertracCustomFieldTypes.CustomBoolean, PertracCustomFieldTypes.CustomDate, PertracCustomFieldTypes.CustomNumeric, PertracCustomFieldTypes.CustomString
            GridContextMenuStrip.Items.Add("Copy this Custom value to all other instruments", Nothing, AddressOf CopyThisCustomValue)
            GridContextMenuStrip.Items.Add(New ToolStripSeparator)
        End Select
      End If

      ' Drill Down ?

      Dim DrilldownIntem As ToolStripItem

      DrilldownIntem = GridContextMenuStrip.Items.Add("Drilldown by " & Grid_Results.Cols(GridClickMouseCol).Caption, Nothing, AddressOf DrilldownGridField)
      If (Col_Is_Custom) Then
        DrilldownIntem.Name = "cmsMenuDrilldown_CustomField_" & CustomFieldID.ToString
      Else
        DrilldownIntem.Name = "cmsMenuDrilldown_StaticField_" & StaticFieldID.ToString
      End If
      DrilldownIntem.Tag = Col_Name

      ' Show in Browser ?

      If (GridClickMouseRow > 0) Then
        GridContextMenuStrip.Items.Add(New ToolStripSeparator)
        GridContextMenuStrip.Items.Add("View selected Instruments in the Fund Browser.", Nothing, AddressOf ViewRowsInBrowser)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SplitButton_Save_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SplitButton_Save.ButtonClick
    If (FormChanged) Then
      SetFormData()
    End If
    If (FormChanged = False) Then
      SplitButton_Save.Visible = False
      SplitButton_Cancel.Visible = False
    End If
  End Sub

  Private Sub SplitButton_Cancel_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SplitButton_Cancel.ButtonClick
    FormChanged = False
    PaintGrid()
  End Sub

  Private Sub Event_SaveChanges(ByVal sender As Object, ByVal e As System.EventArgs)
    If (FormChanged) Then
      SetFormData()
    End If
  End Sub

  Private Sub Event_CancelChanges(ByVal sender As Object, ByVal e As System.EventArgs)
    FormChanged = False
    PaintGrid()
  End Sub

  Private Sub CopyThisCustomValue(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim GridRow As Integer

    If (GridClickMouseRow <= 0) Then
      Exit Sub
    End If

    ' Expand Grid ?

    If (Grid_Results.Tag IsNot Nothing) AndAlso (IsNumeric(Grid_Results.Tag)) Then
      Dim CompoundArrayLength As Integer

      CompoundArrayLength = CInt(Grid_Results.Tag)

      If (Grid_Results.Cols.Count <= CompoundArrayLength) Then
        PaintGrid(Grid_Results.Cols.Count - 1, True, True)
      End If
    End If

    ' Copy Field

    For GridRow = 1 To (Me.Grid_Results.Rows.Count - 1)
      If (GridRow <> GridClickMouseRow) Then
        Grid_Results.Item(GridRow, GridClickMouseCol) = Grid_Results.Item(GridClickMouseRow, GridClickMouseCol)
      End If
    Next

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    If (FormChanged) Then
      SplitButton_Save.Visible = True
      SplitButton_Cancel.Visible = True
    Else
      SplitButton_Save.Visible = False
      SplitButton_Cancel.Visible = False
    End If

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

  End Sub

  Private Sub SetCompoundIDArray()
    ' ******************************************************************************************
    ' Create Combined Array of IDs reflecting the search results ('CompoundArray').
    ' Also Set Results labels. These show the number of instruments selected, or 'False' for invalid criteria.
    ' ******************************************************************************************

    If (UseFixedIdArray) Then
      Exit Sub
    End If

    Try

      Dim Counter As Integer
      Dim thisComboAndOr As ComboBox
      Dim ThisSelectCountLabel As Label
      Dim FirstSelectItem As Boolean = True
      Dim ItemStatus As String = ""

      CompoundArray = Array.CreateInstance(GetType(Integer), 0)

      For Counter = 0 To (SelectionItems.Length - 1)

        If (SelectionItems(Counter).SelectionIsValid) AndAlso (SelectionItems(Counter).ID_Array IsNot Nothing) Then

          If (FirstSelectItem) Then
            If (SelectionItems(Counter).ID_Array.Length > 0) Then
              CompoundArray = Array.CreateInstance(GetType(Integer), SelectionItems(Counter).ID_Array.Length)
              Array.Copy(SelectionItems(Counter).ID_Array, CompoundArray, SelectionItems(Counter).ID_Array.Length)
              FirstSelectItem = False
            End If
          Else
            thisComboAndOr = CType(Panel_SearchInstruments.Controls("Combo_AndOr" & Counter.ToString), ComboBox)

            CompoundArray = CombineIntegerArray(CStr(thisComboAndOr.Text), CompoundArray, SelectionItems(Counter).ID_Array)
          End If

          ItemStatus = SelectionItems(Counter).ID_Array.Length.ToString

        Else

          ItemStatus = "False"

        End If

        ' Set Label.

        ThisSelectCountLabel = CType(Panel_SearchInstruments.Controls("Label_SelectCount" & (Counter + 1).ToString), Label)
        If (ThisSelectCountLabel IsNot Nothing) Then
          ThisSelectCountLabel.Text = ItemStatus
        End If

      Next

    Catch ex As Exception
    Finally

      Set_CustomGroupID_CompoundArray()

    End Try

  End Sub

  Private Sub Set_CustomGroupID_CompoundArray()
    ' *************************************************************
    '
    '
    '
    ' *************************************************************

    Try
      If (CustomGroupID_CompoundArray > 0) Then

        MainForm.StatFunctions.SetDynamicGroupMembers(CustomGroupID_CompoundArray, CompoundArray, Nothing)

        ' RaiseEvent SearchUpdateEvent(Me, CustomGroupID_CompoundArray)

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub PaintGrid(Optional ByVal StartingResultsItem As Integer = 0, Optional ByVal ExpandGridToFitResults As Boolean = False, Optional ByVal SkipFormChangedChecks As Boolean = False)
    ' *************************************************************
    '
    '
    '
    ' *************************************************************

    ' Dim CompoundArray(-1) As Integer

    ' Dim Counter As Integer
    ' Dim ItemStatus As String = ""
    ' Dim FirstSelectItem As Boolean = True

    Dim OrgCursor As Cursor = Cursors.Default
    Dim DoCacheInformationItems As Boolean = True

    Try
      OrgCursor = Me.Cursor
      Me.Cursor = Cursors.WaitCursor

      If (SkipFormChangedChecks = False) Then
        If (FormChanged) Then
          SetFormData(True)
        End If

        FormChanged = False
        SplitButton_Save.Visible = False
        SplitButton_Cancel.Visible = False
      End If

      Me.StatusLabel_Search.Text = ""
      Grid_Results.Tag = 0
      CustomFieldsChanged = ""

      Label_ChartStock.Text = ""
      Set_LineChart(MainForm, DefaultStatsDatePeriod, 0, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())
      Set_RollingReturnChart(MainForm, DefaultStatsDatePeriod, 0, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1, 1.0#, Now.AddMonths(-36), Now())

      ' Quick Exit ?

      If (CompoundArray Is Nothing) OrElse (CompoundArray.Length <= 0) Then
        Me.Grid_Results.Rows.Count = 1
        Exit Sub
      End If

      ' Set Grid Row Count.

      If (ExpandGridToFitResults Or Menu_Options_ShowAllResults.Checked) Then
        Me.Grid_Results.Rows.Count = (CompoundArray.Length + 1)
      Else
        If (FormChanged = True) Then
          If (Me.Grid_Results.Rows.Count >= (CompoundArray.Length + 1)) Then
            Grid_Results.Rows.Count = (CompoundArray.Length + 1)
          Else
            Me.Grid_Results.Rows.Count = (MIN(CompoundArray.Length, MAX_GRID_DISPLAY_ROWS) + 1)
          End If
        Else
          Me.Grid_Results.Rows.Count = (MIN(CompoundArray.Length, MAX_GRID_DISPLAY_ROWS) + 1)
        End If
      End If

      ' Ensure the Pertrac 'Information' cache is large enough.

      If MainForm.PertracData.MAX_InformationCacheSize < Math.Min(CInt(Grid_Results.Rows.Count * 1.1), 1000) Then
        MainForm.PertracData.MAX_InformationCacheSize = Math.Min(CInt(Grid_Results.Rows.Count * 1.1), 1000)
      End If

      If MainForm.PertracData.MAX_InformationCacheSize < CInt(Grid_Results.Rows.Count * 1.01) Then
        DoCacheInformationItems = False
      End If
      ' 

      Dim ArrayCount As Integer
      Dim GridCount As Integer
      Dim ColumnCount As Integer
      Dim ThisColumn As C1.Win.C1FlexGrid.Column
      Dim ThisColumnName As String
      Dim StaticFieldID As PertracInformationFields

      Dim InfoValues() As Object

      Dim ID_Ordinal As Integer = Grid_Results.Cols.IndexOf("ID")
      Dim Changed_Ordinal As Integer = Grid_Results.Cols.IndexOf("HasChanged")

      Grid_Results.Cols(Changed_Ordinal).Visible = True
      Grid_Results.Cols(ID_Ordinal).Visible = True

      ' Establish which columns should be shown, add them if they do not exist

      Dim StaticFields_ColumnMap(RenaissanceGlobals.PertracInformationFields.MaxValue - 1) As Integer
      Dim CustomFields_KeyMap(Grid_CustomFields_Display_Dictionary.Count - 1) As Integer
      Dim CustomFields_ColumnMap(Grid_CustomFields_Display_Dictionary.Count - 1) As Integer

      ' Static Columns

      For ColumnCount = 0 To (RenaissanceGlobals.PertracInformationFields.MaxValue - 1)
        ThisColumn = Nothing
        StaticFields_ColumnMap(ColumnCount) = (-1)

        If System.Enum.IsDefined(GetType(PertracInformationFields), ColumnCount) Then
          StaticFieldID = CType(ColumnCount, PertracInformationFields)

          If (Grid_StaticFields_Display(ColumnCount)) Then
            ThisColumnName = "Static_" & StaticFieldID.ToString

            If Not Grid_Results.Cols.Contains(ThisColumnName) Then
              ThisColumn = Grid_Results.Cols.Add()
              ThisColumn.Name = ThisColumnName
              ThisColumn.Caption = StaticFieldID.ToString
              ThisColumn.Visible = True
              ThisColumn.DataType = GetType(String)
              ThisColumn.AllowDragging = True
              ThisColumn.AllowEditing = False
              ThisColumn.AllowResizing = True
              ThisColumn.AllowSorting = True

            End If

            StaticFields_ColumnMap(ColumnCount) = Grid_Results.Cols.IndexOf(ThisColumnName)
          End If
        Else
          Grid_StaticFields_Display(ColumnCount) = False
        End If
      Next

      ' Custom Columns

      Dim CustomFieldID As Integer
      Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow

      Grid_CustomFields_Display_Dictionary.Keys.CopyTo(CustomFields_KeyMap, 0)

      For ColumnCount = 0 To (CustomFields_KeyMap.Length - 1)
        CustomFieldID = CustomFields_KeyMap(ColumnCount)
        CustomFields_ColumnMap(ColumnCount) = (-1)
        ThisColumn = Nothing

        If (CustomFieldID > 0) AndAlso (Grid_CustomFields_Display_Dictionary(CustomFieldID)) Then
          ThisColumnName = "Custom_" & CustomFieldID.ToString

          ' Check that Custom Data is present for the selected field(s)

          If CustomFieldDataCache.GetCustomFieldDataStatus(CustomFieldID, 0) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
            CustomFieldDataCache.LoadCustomFieldData(CustomFieldID, 0)
          End If

          ' Add Column if necessary

          If Not Grid_Results.Cols.Contains(ThisColumnName) Then
            ThisColumn = Grid_Results.Cols.Add()
            ThisColumn.Name = ThisColumnName
            ThisColumn.Caption = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFields_KeyMap(ColumnCount), "FieldName")
            ThisColumn.Visible = True
            ThisColumn.DataType = GetType(String)
            ThisColumn.AllowDragging = True
            ThisColumn.AllowEditing = False
            ThisColumn.AllowResizing = True
            ThisColumn.AllowSorting = True
          End If

          ' Format the New column

          If (ThisColumn IsNot Nothing) Then ' A newly added column
            thisCustomFieldDefinition = Nothing
            Try
              thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

              If (thisCustomFieldDefinition IsNot Nothing) Then
                If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
                  ThisColumn.DataType = GetType(String)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
                  ThisColumn.DataType = GetType(Date)
                  ThisColumn.Format = DISPLAYMEMBER_DATEFORMAT

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
                  ThisColumn.DataType = GetType(Boolean)

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.PercentageType) = RenaissanceDataType.PercentageType Then
                  ThisColumn.DataType = GetType(Double)
                  ThisColumn.Format = "#,##0.00%"

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) = RenaissanceDataType.NumericType Then
                  ThisColumn.DataType = GetType(Double)
                  ThisColumn.Format = "#,##0.00"
                End If

                ' Editable Column ?

                If (Me.HasUpdatePermission) Then
                  Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
                    Case PertracCustomFieldTypes.CustomBoolean
                      ThisColumn.AllowEditing = True

                    Case PertracCustomFieldTypes.CustomDate
                      ThisColumn.AllowEditing = True

                    Case PertracCustomFieldTypes.CustomNumeric
                      ThisColumn.AllowEditing = True

                    Case PertracCustomFieldTypes.CustomString
                      ThisColumn.AllowEditing = True

                  End Select
                End If

              End If
            Catch ex As Exception
            End Try
          End If

          CustomFields_ColumnMap(ColumnCount) = Grid_Results.Cols.IndexOf(ThisColumnName)
        End If
      Next

      ' Paint Grid
      Dim ThisCustomValue As Object

      Try
        InPaint = True

        For ArrayCount = StartingResultsItem To (Grid_Results.Rows.Count - 2)
          If ArrayCount >= CompoundArray.Length Then
            Exit For
          End If

          GridCount = ArrayCount + 1

          InfoValues = MainForm.PertracData.GetInformationValues(CompoundArray(ArrayCount), DoCacheInformationItems)

          Grid_Results.Item(GridCount, Changed_Ordinal) = ""
          Grid_Results.Item(GridCount, ID_Ordinal) = CompoundArray(ArrayCount)

          ' Static Fields :-

          If (InfoValues IsNot Nothing) Then
            For ColumnCount = 0 To (StaticFields_ColumnMap.Length - 1)
              If StaticFields_ColumnMap(ColumnCount) > 0 Then
                Grid_Results.Item(GridCount, StaticFields_ColumnMap(ColumnCount)) = Nz(InfoValues(ColumnCount), "")
              End If
            Next
          Else
            For ColumnCount = 0 To (StaticFields_ColumnMap.Length - 1)
              If StaticFields_ColumnMap(ColumnCount) > 0 Then
                Grid_Results.Item(GridCount, StaticFields_ColumnMap(ColumnCount)) = ""
              End If
            Next
          End If

          '  Custom Fields :-

          For ColumnCount = 0 To (CustomFields_KeyMap.Length - 1)

            If CustomFields_ColumnMap(ColumnCount) > 0 Then
              CustomFieldID = CustomFields_KeyMap(ColumnCount)

              Try
                ThisCustomValue = CustomFieldDataCache.GetDataPoint(CustomFieldID, 0, CompoundArray(ArrayCount))

                If (ThisCustomValue Is Nothing) OrElse (ThisCustomValue Is DBNull.Value) Then
                  ThisCustomValue = Nz(ThisCustomValue, Grid_Results.Cols(CustomFields_ColumnMap(ColumnCount)).DataType)
                End If

                Grid_Results.Item(GridCount, CustomFields_ColumnMap(ColumnCount)) = ThisCustomValue
              Catch ex As Exception
                ' Set Zero value, appropriate to column type.
                Grid_Results.Item(GridCount, CustomFields_ColumnMap(ColumnCount)) = Nz(Nothing, Grid_Results.Cols(CustomFields_ColumnMap(ColumnCount)).DataType)
              End Try

            End If
          Next

          ' Status Message

          If (ArrayCount - StartingResultsItem) Mod 200 = 0 Then
            StatusLabel_Search.Text = "Populating results grid [" & (ArrayCount - StartingResultsItem).ToString & " of " & (Grid_Results.Rows.Count - 1).ToString & "]"
            Me.Refresh()
          End If

        Next

        Grid_Results.Tag = CompoundArray.Length
        If (CompoundArray.Length > (Grid_Results.Rows.Count - 1)) Then
          Me.StatusLabel_Search.Text = "SearchResults : Total " & CompoundArray.Length.ToString & " Instruments, Grid display limited to " & (Grid_Results.Rows.Count - 1).ToString & "."
        Else
          Me.StatusLabel_Search.Text = "SearchResults : Total " & CompoundArray.Length.ToString & " Instruments."
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error painting results grid.", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error painting results grid.", ex.StackTrace, True)
    Finally
      Me.Cursor = OrgCursor
    End Try

  End Sub

  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String = ""
    Dim ErrFlag As Boolean = False
    Dim ErrStack As String = ""

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And (Me.HasInsertPermission = False) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)

      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************

    If Me.SplitButton_Save.Visible = False Then
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String = ""
    Dim UpdateRows(0) As DataRow

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' *************************************************************
    ' Process Update(s)
    ' *************************************************************

    Dim FieldDataUpdated As Boolean = False
    Dim ColumnCount As Integer
    Dim RowCounter As Integer
    Dim ColumnName As String
    Dim CustomFieldID As Integer
    Dim PertracID As Integer
    Dim Col_Is_Custom As Boolean
    Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing

    ' Dim DSCustomData As RenaissanceDataClass.DSPertracCustomFieldData
    Dim tblCustomData As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable
    ' Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow
    Dim ThisRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow
    Dim AdpCustomData As New SqlDataAdapter
    Dim ThisConnection As SqlConnection = Nothing
    Dim ThisGridItem As Object

    Dim Changed_Ordinal As Integer = Grid_Results.Cols.IndexOf("HasChanged")
    Dim ID_Ordinal As Integer = Grid_Results.Cols.IndexOf("ID")

    Try
      ThisConnection = MainForm.GetGenoaConnection
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(ThisConnection, AdpCustomData, RenaissanceStandardDatasets.tblPertracCustomFieldData.TableName)
      AdpCustomData.InsertCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      AdpCustomData.UpdateCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      'AddHandler AdpCustomData.RowUpdating, AddressOf OnRowUpdating
      'AddHandler AdpCustomData.RowUpdated, AddressOf OnRowUpdated
      'AddHandler AdpCustomData.FillError, AddressOf OnRowFillError

      InPaint = True

      For ColumnCount = 0 To (Grid_Results.Cols.Count - 1)
        ColumnName = Grid_Results.Cols(ColumnCount).Name
        CustomFieldID = (-1)
        Col_Is_Custom = False

        If ColumnName.StartsWith("Custom_") AndAlso (IsNumeric(ColumnName.Substring(7))) Then
          CustomFieldID = CInt(ColumnName.Substring(7))
          thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

          ' Only allow the update of 'Custom' fields
          If (thisCustomFieldDefinition IsNot Nothing) Then
            Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
              Case PertracCustomFieldTypes.CustomBoolean, PertracCustomFieldTypes.CustomDate, PertracCustomFieldTypes.CustomNumeric, PertracCustomFieldTypes.CustomString
                Col_Is_Custom = True

              Case Else
                Col_Is_Custom = False

            End Select
          End If
        End If

        ' ********************************************************
        ' When Saving Custom field data, rather than get the existing data and 
        ' update the datarows as appropriate, I have chosen to simply create
        ' a new table and add all changes as new rows. The underlying INSERT command
        ' has been written to check for existing records and INSERT / UPDATE as appropriate.
        ' 
        ' This approach has been taken as I believe it should be faster, especially for 
        ' mass updates.
        ' ********************************************************

        If (Col_Is_Custom) AndAlso (CustomFieldsChanged.Contains("," & CustomFieldID.ToString & ".")) Then
          ' Get Existing Custom Values
          Dim ThisFieldDataUpdated As Boolean = False
          Dim UpdateCount As Integer = 0

          ' DSCustomData = New RenaissanceDataClass.DSPertracCustomFieldData
          tblCustomData = New RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable

          For RowCounter = 1 To (Grid_Results.Rows.Count - 1)
            If (CStr(Grid_Results.Item(RowCounter, Changed_Ordinal)) = "x") Then
              PertracID = CInt(Grid_Results.Item(RowCounter, ID_Ordinal))

              ThisRow = tblCustomData.NewtblPertracCustomFieldDataRow

              ThisRow.FieldDataID = 0
              ThisRow.CustomFieldID = CustomFieldID
              ThisRow.PertracID = PertracID
              ThisRow.GroupID = 0
              ThisRow.FieldNumericData = 0
              ThisRow.FieldTextData = ""
              ThisRow.FieldDateData = Renaissance_BaseDate
              ThisRow.FieldBooleanData = False
              ThisRow.UpdateRequired = False
              ThisRow.LatestReturnDate = Renaissance_BaseDate

              Try
                ThisGridItem = Grid_Results.Item(RowCounter, ColumnCount)

                If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) Then
                  If (ThisGridItem IsNot Nothing) Then
                    ThisRow.FieldTextData = ThisGridItem.ToString
                  End If

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) Then
                  If IsNumeric(ThisGridItem) Then
                    ThisRow.FieldNumericData = CDbl(ThisGridItem)
                  End If

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) Then
                  If IsDate(ThisGridItem) Then
                    ThisRow.FieldDateData = CDate(ThisGridItem)
                  End If

                ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) Then
                  ThisRow.FieldBooleanData = CBool(ThisGridItem)

                End If
              Catch ex As Exception
              End Try

              If (ThisRow.RowState And DataRowState.Detached) Then
                tblCustomData.Rows.Add(ThisRow)
              End If

              ThisFieldDataUpdated = True
            End If

            If (ThisFieldDataUpdated) AndAlso (tblCustomData.Rows.Count >= 200) Then

              FieldDataUpdated = True
              ThisFieldDataUpdated = False
              UpdateCount += 200
              StatusLabel_Search.Text = "Updating Custom Fields [" & UpdateCount.ToString & "]"
              Me.Refresh()

              CustomFieldDataCache.ClearDataCacheGroupData(CustomFieldID, 0)
              MainForm.AdaptorUpdate(Me.Name, AdpCustomData, tblCustomData)

              tblCustomData.Rows.Clear()
              tblCustomData = Nothing
              tblCustomData = New RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable

            End If

          Next

          If (ThisFieldDataUpdated) AndAlso (tblCustomData.Rows.Count >= 200) Then
            FieldDataUpdated = True

            CustomFieldDataCache.ClearDataCacheGroupData(CustomFieldID, 0)

            MainForm.AdaptorUpdate(Me.Name, AdpCustomData, tblCustomData)

          End If

          tblCustomData = Nothing
          ' DSCustomData = Nothing
        End If

      Next

    Catch ex As Exception
      ErrMessage = ex.Message
      ErrFlag = True
      ErrStack = ex.StackTrace
    Finally
      InPaint = False

      If (ThisConnection IsNot Nothing) Then
        Try
          ThisConnection.Close()
        Catch ex As Exception
        End Try
        ThisConnection = Nothing
      End If
    End Try

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
      Exit Function
    End If

    ' Finish off

    FormChanged = False
    SplitButton_Save.Visible = False
    SplitButton_Cancel.Visible = False

    ' Propagate changes

    Try

      If FieldDataUpdated Then
        Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPertracCustomFieldData, CustomFieldsChanged)

        MainForm.ReloadTable(RenaissanceChangeID.tblPertracCustomFieldData, False)
        Call MainForm.Main_RaiseEvent(UpdateMessage)
        PaintGrid()
      End If

    Catch ex As Exception
    End Try

    CustomFieldsChanged = ""
    Return True

  End Function

#End Region

#Region " Static and Custom Field search functions. Also SetCustomField_DataCache()"

  Private Function GetStaticFieldDistinctStrings(ByRef SelectionItem As SelectionItemClass) As String()

    ' ************************************************************************
    ' Returns a String Array of distinct values held in the Pertrac Information
    ' table relating to the Static Field selected in the given 'SelectionItemClass'.
    '
    ' This function is used to populate the datasource for the 'Value' combos.
    '
    ' Function returns a One-Line string array in an error or invalid situation.
    '
    ' ************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      Dim InformationField As PertracInformationFields

      ' Is a Static field selected ?
      If (Not SelectionItem.IsStaticField) Then
        Return New String() {""}
      End If

      ' Resolve Selected FieldId
      Try
        If System.Enum.IsDefined(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItem.SelectFieldID) Then
          InformationField = System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItem.SelectFieldID)
        Else
          Return New String() {""}
        End If
      Catch ex As Exception
        Return New String() {""}
      End Try

      ' Don't bother with the 'Description' and 'UserDescription' fields.
      ' These are of type 'ntext' and the SELECT DISTINCT command will fail anyway.
      If (InformationField = PertracInformationFields.Description) OrElse (InformationField = PertracInformationFields.UserDescription) Then
        Return New String() {""}
      End If

      ' Validate - Don't allow 'MaxValue'.

      If (InformationField = PertracInformationFields.MaxValue) Then
        ' Not a valid search field
        Return New String() {""}
      End If

      ' First Get Information Field relationships

      Dim tblFieldMappings As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingDataTable = Nothing
      Dim SelectedFieldMappings() As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
      Dim ThisFieldMapping As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
      Dim FieldCount As Integer
      Dim SelectString As String = ""

      Try
        tblFieldMappings = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblPertracFieldMapping), RenaissanceDataClass.DSPertracFieldMapping).tblPertracFieldMapping
      Catch ex As Exception
      End Try

      If (tblFieldMappings Is Nothing) OrElse (tblFieldMappings.Rows.Count <= 0) Then
        ' Apparently failed to get the table
        Return New String() {""}
      End If

      SelectedFieldMappings = tblFieldMappings.Select("FCP_FieldName='" & InformationField.ToString & "'", "DataProvider")
      If (SelectedFieldMappings Is Nothing) OrElse (SelectedFieldMappings.Length <= 0) Then
        ' No Mapped Fields
        Return New String() {""}
      End If

      ' ************************************
      ' Construct the select String.
      ' ************************************

      For FieldCount = 0 To (SelectedFieldMappings.Length - 1)
        ThisFieldMapping = SelectedFieldMappings(FieldCount)

        If (ThisFieldMapping.PertracField.Length > 0) Then
          Dim TempSelectString As String

          ' Original Pertrac 

          TempSelectString = "SELECT DISTINCT " & ThisFieldMapping.PertracField & " AS 'PertracField' FROM [MASTERSQL].[dbo].[Information] WHERE (DataVendorName='" & ThisFieldMapping.DataProvider & "')"
          If SelectString.Length > 0 Then
            SelectString &= " UNION "
          End If
          SelectString &= TempSelectString

          ' New Tables.

          TempSelectString = "SELECT DISTINCT " & ThisFieldMapping.PertracField & " AS 'PertracField' FROM [MASTERSQL].[dbo].[tblInstrumentInformation] WHERE (DataVendorName='" & ThisFieldMapping.DataProvider & "')"
          If SelectString.Length > 0 Then
            SelectString &= " UNION "
          End If
          SelectString &= TempSelectString
        End If
      Next

      ' Get Text Selection

      Dim tblTextSelection As New DataTable
      Dim tmpCommand As New SqlCommand

      If SelectString.Length > 0 Then
        Try
          tmpCommand.CommandType = CommandType.Text
          tmpCommand.CommandText = SelectString
          tmpCommand.Connection = MainForm.GetGenoaConnection
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          tblTextSelection.Load(tmpCommand.ExecuteReader)

        Catch SQL_Ex As SqlException
          If (SQL_Ex.Message.Contains("DISTINCT") = False) Then
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, SQL_Ex.Message, "Error Selecting from [Information]" & vbCrLf & tmpCommand.CommandText, SQL_Ex.StackTrace, True)
          End If
          Return New String() {""}

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from [Information]" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
          Return New String() {""}

        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try
      End If

      ' Resolve Result to String Array

      Dim RVal(-1) As String

      If (tblTextSelection IsNot Nothing) AndAlso (tblTextSelection.Rows.Count > 0) Then
        Dim ElementCounter As Integer

        ReDim RVal(tblTextSelection.Rows.Count - 1)

        For ElementCounter = 0 To (tblTextSelection.Rows.Count - 1)
          RVal(ElementCounter) = Nz(tblTextSelection.Rows(ElementCounter)(0), "")
        Next

        Array.Sort(RVal)

        ' Return Valid String array.

        Return RVal
      End If

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    ' Catchall.

    Return New String() {""}

  End Function

  Private Function GetCustomFieldDistinctStrings(ByRef SelectionItem As SelectionItemClass) As String()
    ' ************************************************************************
    ' Returns a String Array of distinct values held in the Custom Field Data Table
    ' table relating to the Custom Field selected in the given 'SelectionItemClass'.
    '
    ' This function is used to populate the datasource for the 'Value' combos.
    '
    ' Function returns a One-Line string array in an error or invalid situation.
    '
    ' ************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      Dim CustomFieldDefinition As DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
      Dim CustomFieldID As Integer = 0
      Dim CustomFieldDataType As RenaissanceDataType = RenaissanceDataType.None
      Dim SelectString As String = ""
      Dim DataFieldName As String = ""
      Dim IsDate As Boolean = False

      Dim TmpObject As Object

      ' Is a Static field selected ?
      If (SelectionItem.IsCustomField = False) OrElse (IsNumeric(SelectionItem.SelectFieldID) = False) OrElse (CInt(SelectionItem.SelectFieldID) <= 0) Then
        Return New String() {""}
      End If

      Try
        ' Get Selection Criteria

        CustomFieldID = CInt(SelectionItem.SelectFieldID)

        ' Get Custom Field Definition

        TmpObject = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")
        If (TmpObject Is Nothing) Then
          Return New String() {""}
        End If
        CustomFieldDefinition = CType(TmpObject, DSPertracCustomFields.tblPertracCustomFieldsRow)
        CustomFieldDataType = CustomFieldDefinition.FieldDataType

      Catch ex As Exception
        Return New String() {""}
      End Try

      ' WHERE String 
      DataFieldName = "[FieldNumericData]"

      If (CustomFieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
        DataFieldName = "[FieldTextData]"

      ElseIf (CustomFieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
        ' DataFieldName = "CONVERT(varchar(20), [FieldDateData], 106)" '"FieldDateData"
        DataFieldName = "[FieldDateData]"
        IsDate = True

      ElseIf (CustomFieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
        DataFieldName = "[FieldBooleanData]"

        Return New String() {"True", "False"}

      Else ' Numeric
        DataFieldName = "[FieldNumericData]"

        ' Don't do this for Numeric, it just returns too much data.

        Return New String() {""}

      End If

      ' Build Selection String 

      SelectString = "SELECT DISTINCT " & DataFieldName & " AS DataField FROM fn_tblPertracCustomFieldData_CacheSelect(@GroupID, @CustomFieldID, @KnowledgeDate) ORDER BY " & DataFieldName

      ' Execute Select

      Dim tblCustomSelection As New DataTable
      Dim thisRow As DataRow
      Dim tmpCommand As New SqlCommand

      If SelectString.Length > 0 Then
        Try
          tmpCommand.CommandType = CommandType.Text
          tmpCommand.CommandText = SelectString
          tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = 0
          tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = CustomFieldID
          tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

          tmpCommand.Connection = MainForm.GetGenoaConnection
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          tblCustomSelection.Load(tmpCommand.ExecuteReader)

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting DISTINCT from Custom Field Data" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
          Return New String() {""}

        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try
      End If

      ' Resolve Result to String Array

      Dim FieldDataOrdinal As Integer = (-1)
      Dim ResultsArray(-1) As String

      Try
        If (tblCustomSelection IsNot Nothing) AndAlso (tblCustomSelection.Rows.Count > 0) Then
          Dim ElementCounter As Integer

          FieldDataOrdinal = tblCustomSelection.Columns.IndexOf("DataField")

          ReDim ResultsArray(tblCustomSelection.Rows.Count - 1)

          For ElementCounter = 0 To (tblCustomSelection.Rows.Count - 1)
            thisRow = tblCustomSelection.Rows(ElementCounter)

            ResultsArray(ElementCounter) = ""

            Try
              If (IsDate) Then
                ResultsArray(ElementCounter) = CDate(thisRow(FieldDataOrdinal)).ToString(DISPLAYMEMBER_DATEFORMAT)
              Else
                ResultsArray(ElementCounter) = thisRow(FieldDataOrdinal).ToString
              End If
            Catch ex As Exception
            End Try

          Next

          Try
            tblCustomSelection.Clear()
          Catch ex As Exception
          End Try
        Else
          Return New String() {""}
        End If
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting DISTINCT Values from Custom Field query" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
        Return New String() {""}
      End Try

      tblCustomSelection = Nothing

      If ResultsArray.Length <= 0 Then
        Return New String() {""}
      End If

      Return ResultsArray

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    ' catchall

    Return New String() {""}

  End Function

  Private Function GetStaticFieldSelection(ByRef SelectionItem As SelectionItemClass) As Integer()
    ' ************************************************************************
    ' Return Array of PertracIDs relating to the given Static Field Selection.
    '
    '
    ' ************************************************************************

    Dim InformationField As PertracInformationFields
    Dim SelectCondition As String
    Dim SelectValue As String
    Dim SelectValue2 As String

    ' Is a Static field selected ?
    If (SelectionItem.IsStaticField = False) Then
      Return Array.CreateInstance(GetType(Integer), 0)
    End If

    ' Validate the selected Field, get selection criteria.

    Try
      If System.Enum.IsDefined(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItem.SelectFieldID) Then
        InformationField = System.Enum.Parse(GetType(RenaissanceGlobals.PertracInformationFields), SelectionItem.SelectFieldID)
      Else
        Return Array.CreateInstance(GetType(Integer), 0)
      End If

      SelectCondition = SelectionItem.ConditionOperand
      SelectValue = SelectionItem.SelectValue
      SelectValue2 = SelectionItem.SelectValue2

      ' Replace '*' wildcards for SQL '%' wildcards.

      If (SelectCondition.ToUpper = "LIKE") OrElse (SelectCondition.ToUpper = "NOT LIKE") Then
        SelectValue = SelectValue.Replace(CChar("*"), CChar("%"))
      End If

    Catch ex As Exception
      Return Array.CreateInstance(GetType(Integer), 0)
    End Try
    Dim RVal() As Integer
    Dim FieldIsNumeric As Boolean = False

    ReDim RVal(-1)

    ' Validate

    If (InformationField = PertracInformationFields.MaxValue) Then
      ' Not a valid search field
      Return RVal
    End If

    ' First Get Information Field relationships

    Dim tblFieldMappings As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingDataTable = Nothing
    Dim SelectedFieldMappings() As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
    Dim ThisFieldMapping As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
    Dim FieldCount As Integer
    Dim SelectString As String = ""

    Try
      tblFieldMappings = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblPertracFieldMapping), RenaissanceDataClass.DSPertracFieldMapping).tblPertracFieldMapping
    Catch ex As Exception
    End Try

    If (tblFieldMappings Is Nothing) OrElse (tblFieldMappings.Rows.Count <= 0) Then
      ' Apparently failed to get the table
      Return RVal
    End If

    ' Mastername vs Information table 

    Select Case InformationField

      Case PertracInformationFields.DataPeriod, PertracInformationFields.Mastername

        If (SelectValue.Length > 0) Then

          If (InformationField = PertracInformationFields.DataPeriod) Then

            If (Not IsNumeric(SelectValue)) Then

              If System.Enum.GetNames(GetType(DealingPeriod)).Contains(SelectValue) Then
                SelectValue = CInt(System.Enum.Parse(GetType(DealingPeriod), SelectValue)).ToString
              Else
                SelectValue = "0"
              End If

            End If

            If (SelectCondition.ToUpper.StartsWith("BETWEEN")) Then
              If (SelectValue2.Length > 0) Then

                If (Not IsNumeric(SelectValue2)) Then

                  If System.Enum.GetNames(GetType(DealingPeriod)).Contains(SelectValue2) Then
                    SelectValue2 = CInt(System.Enum.Parse(GetType(DealingPeriod), SelectValue2)).ToString
                  Else
                    SelectValue2 = "0"
                  End If

                End If

                SelectString = "([DataPeriod] >= " & SelectValue & ") AND ([DataPeriod] <= " & SelectValue2 & ")"
              End If
            Else
              SelectString = "[DataPeriod] " & SelectCondition & " " & SelectValue
            End If

          ElseIf (InformationField = PertracInformationFields.Mastername) Then

            If (SelectCondition.ToUpper.StartsWith("BETWEEN")) Then
              If (SelectValue2.Length > 0) Then
                SelectString = "([Mastername] >= '" & SelectValue & "') AND ([Mastername] <= '" & SelectValue2 & "')"
              End If
            Else
              SelectString = "[Mastername] " & SelectCondition & " '" & SelectValue & "'"
            End If

          End If

        End If

        ' Get Text Selection

        Dim tblTextSelection As New DataTable
        Dim tmpCommand As New SqlCommand

        If SelectString.Length > 0 Then
          Try
            tmpCommand.CommandType = CommandType.Text
            tmpCommand.CommandText = "SELECT [ID] AS [ID] FROM dbo.fn_tblMastername() WHERE (" & SelectString & ")"
            tmpCommand.Connection = MainForm.GetGenoaConnection
            tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

            tblTextSelection.Load(tmpCommand.ExecuteReader)

          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from fn_tblMastername()" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)

          Finally
            Try
              If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
                tmpCommand.Connection.Close()
                tmpCommand.Connection = Nothing
              End If
            Catch ex As Exception
            End Try
          End Try
        End If

        ' Resolve Result to Integer Array

        Dim ResultsArray(-1) As Integer

        If (tblTextSelection IsNot Nothing) AndAlso (tblTextSelection.Rows.Count > 0) Then
          Dim ElementCounter As Integer
          Dim thisElement As Object

          ReDim ResultsArray(tblTextSelection.Rows.Count - 1)

          For ElementCounter = 0 To (tblTextSelection.Rows.Count - 1)
            ResultsArray(ElementCounter) = 0

            thisElement = tblTextSelection.Rows(ElementCounter)(0)

            If IsNumeric(thisElement) Then
              ResultsArray(ElementCounter) = CInt(thisElement)
            End If
          Next
        End If

        ' Combine with existing RVal, the result of a Numeric field selection (if necessary).

        If ResultsArray.Length > 0 Then
          RVal = ResultsArray
        End If

      Case Else

        SelectedFieldMappings = tblFieldMappings.Select("FCP_FieldName='" & InformationField.ToString & "'", "DataProvider")
        If (SelectedFieldMappings Is Nothing) OrElse (SelectedFieldMappings.Length <= 0) Then
          ' No Mapped Fields
          Return RVal
        End If

        ' ************************************
        ' Construct the select String
        ' ************************************

        Dim HasNumericFields As Boolean = False

        For FieldCount = 0 To (SelectedFieldMappings.Length - 1)
          ThisFieldMapping = SelectedFieldMappings(FieldCount)

          If (ThisFieldMapping.PertracField.Length > 0) Then
            If (ThisFieldMapping.FieldIsNumeric) AndAlso (SelectCondition.ToUpper <> "LIKE") AndAlso (SelectCondition.ToUpper <> "NOT LIKE") Then
              HasNumericFields = True

            Else
              Dim TempSelectString As String

              If (SelectCondition.ToUpper.StartsWith("BETWEEN")) Then
                TempSelectString = "((DataVendorName='" & ThisFieldMapping.DataProvider & "') AND (" & ThisFieldMapping.PertracField & " >= '" & SelectValue & "') AND (" & ThisFieldMapping.PertracField & " <= '" & SelectValue2 & "'))"
              Else
                TempSelectString = "((DataVendorName='" & ThisFieldMapping.DataProvider & "') AND (" & ThisFieldMapping.PertracField & " " & SelectCondition & " '" & SelectValue & "'))"
              End If

              If SelectString.Length > 0 Then
                SelectString &= " OR "
              End If
              SelectString &= TempSelectString

            End If
          End If
        Next

        ' Get Fieldvalues relating to fields defined as being Numeric.

        If (HasNumericFields) Then
          Dim TempRows() As DataRow

          If (SelectionItem.NumericValuesTable Is Nothing) Then
            SelectionItem.NumericValuesTable = GetNumericInformationTable(SelectedFieldMappings)
          End If

          If (SelectionItem.NumericValuesTable IsNot Nothing) AndAlso (SelectionItem.NumericValuesTable.Rows.Count > 0) Then
            Dim ValueOrdinal As Integer = SelectionItem.NumericValuesTable.Columns.IndexOf("NumericValue")
            Dim IDOrdinal As Integer = SelectionItem.NumericValuesTable.Columns.IndexOf("ID")

            If (SelectCondition.ToUpper.StartsWith("BETWEEN")) Then
              TempRows = SelectionItem.NumericValuesTable.Select("(NumericValue >= " & GetNumericValue(SelectValue).ToString & ") AND (NumericValue <= " & GetNumericValue(SelectValue2).ToString & ")")
            Else
              TempRows = SelectionItem.NumericValuesTable.Select("NumericValue " & SelectCondition & GetNumericValue(SelectValue).ToString)
            End If

            If (TempRows IsNot Nothing) AndAlso (TempRows.Length > 0) Then
              Dim TempRVal(TempRows.Length - 1) As Integer
              Dim RowCount As Integer

              For RowCount = 0 To (TempRows.Length - 1)
                TempRVal(RowCount) = CInt(TempRows(RowCount)(IDOrdinal))
              Next

              Array.Sort(TempRVal)
              RVal = CombineIntegerArray("OR", RVal, TempRVal)
            End If

          End If
        End If

        ' Get Text Selection

        Dim tblTextSelection As New DataTable
        Dim tmpCommand As New SqlCommand

        If SelectString.Length > 0 Then
          Try
            tmpCommand.CommandType = CommandType.Text
            tmpCommand.CommandText = "SELECT [ID] AS [ID] FROM [MASTERSQL].[dbo].[Information] WHERE (" & SelectString & ") UNION SELECT ([InstrumentID] + 1048576) AS [ID] FROM [MASTERSQL].[dbo].[tblInstrumentInformation] WHERE (" & SelectString & ")"
            tmpCommand.Connection = MainForm.GetGenoaConnection
            tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

            tblTextSelection.Load(tmpCommand.ExecuteReader)

          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from [Information]" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)

          Finally
            Try
              If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
                tmpCommand.Connection.Close()
                tmpCommand.Connection = Nothing
              End If
            Catch ex As Exception
            End Try
          End Try
        End If

        ' Resolve Result to Integer Array

        Dim ResultsArray(-1) As Integer

        If (tblTextSelection IsNot Nothing) AndAlso (tblTextSelection.Rows.Count > 0) Then
          Dim ElementCounter As Integer
          Dim thisElement As Object

          ReDim ResultsArray(tblTextSelection.Rows.Count - 1)

          For ElementCounter = 0 To (tblTextSelection.Rows.Count - 1)
            ResultsArray(ElementCounter) = 0

            thisElement = tblTextSelection.Rows(ElementCounter)(0)

            If IsNumeric(thisElement) Then
              ResultsArray(ElementCounter) = CInt(thisElement)
            End If
          Next
        End If

        ' Combine with existing RVal, the result of a Numeric field selection (if necessary).

        If ResultsArray.Length > 0 Then
          RVal = CombineIntegerArray("OR", RVal, ResultsArray)
        End If

    End Select

    ' Return Final Integer array

    If RVal.Length > 0 Then
      Array.Sort(RVal)
    End If

    Return RVal

  End Function

  Private Function GetNumericInformationTable(ByVal SelectedFieldMappings() As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow) As DataTable
    ' ************************************************************************
    ' Return a Data Table representing the Numeric Pertrac 
    ' Information values for the Numerically mapped Static fields given in the 
    ' function parameter. 
    '
    ' The resulting dataset can then be queried using the .Select() method to return
    ' Information values relating to a given selection.
    '
    ' ************************************************************************

    Dim FieldCount As Integer
    Dim DataProviderInString As String = ""
    Dim CASEString As String = ""
    Dim CASEMultiplierString As String = ""

    Dim ThisFieldMapping As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow

    ' Build SQL 'CASE' statements for use in the ultimate SELECT query.

    For FieldCount = 0 To (SelectedFieldMappings.Length - 1)
      ThisFieldMapping = SelectedFieldMappings(FieldCount)

      If (ThisFieldMapping.PertracField.Length > 0) Then
        If (ThisFieldMapping.FieldIsNumeric) Then

          ' Case String

          CASEString &= "WHEN DataVendorName='" & ThisFieldMapping.DataProvider & "' THEN " & ThisFieldMapping.PertracField & " "
          CASEMultiplierString &= "WHEN DataVendorName='" & ThisFieldMapping.DataProvider & "' THEN " & ThisFieldMapping.Multiplier.ToString & " "

          ' Provider IN String

          If (DataProviderInString.Length > 0) Then
            DataProviderInString &= ","
          End If
          DataProviderInString &= "'" & ThisFieldMapping.DataProvider & "'"
        End If
      End If
    Next

    If (CASEString.Length <= 0) OrElse (DataProviderInString.Length <= 0) Then
      Return New DataTable
    End If

    ' Build Complete Select String 

    Dim SQLSelectString As String

    SQLSelectString = "SELECT ID, 'FieldValue'=CASE " & CASEString & " END, 'FieldMultiplier'=CASE " & CASEMultiplierString & " END FROM [MASTERSQL].[dbo].[Information] WHERE DataVendorName IN (" & DataProviderInString & ")"

    ' Submit SQL Query.

    Dim tblNumericSelection As New DataTable
    Dim tmpCommand As New SqlCommand

    If SQLSelectString.Length > 0 Then
      Try
        tmpCommand.CommandType = CommandType.Text
        tmpCommand.CommandText = SQLSelectString
        tmpCommand.Connection = MainForm.GetGenoaConnection
        tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        tblNumericSelection.Load(tmpCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from [Information]" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)

      Finally
        Try
          If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
            tmpCommand.Connection.Close()
            tmpCommand.Connection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try
    End If

    ' Exit if no rows selected.

    If (tblNumericSelection Is Nothing) OrElse (tblNumericSelection.Rows.Count <= 0) Then
      Return New DataTable
    End If

    tblNumericSelection.Columns.Add("NumericValue", GetType(Double))
    If (tblNumericSelection.Rows.Count <= 0) Then
      Return tblNumericSelection
    End If

    ' Populate Numeric Field Values.

    Dim RowCount As Integer
    Dim ThisRow As DataRow
    Dim FieldOrdinal As Integer = tblNumericSelection.Columns.IndexOf("FieldValue")
    Dim ValueOrdinal As Integer = tblNumericSelection.Columns.IndexOf("NumericValue")
    Dim MultiplierOrdinal As Integer = tblNumericSelection.Columns.IndexOf("FieldMultiplier")
    Dim ThisFieldValue As String
    Dim ThisValue As Double

    For RowCount = 0 To (tblNumericSelection.Rows.Count - 1)
      ThisRow = tblNumericSelection.Rows(RowCount)

      If ThisRow.IsNull(FieldOrdinal) Then
        ThisFieldValue = "0"
        ThisValue = 0
      Else
        ThisFieldValue = ThisRow(FieldOrdinal).ToString
        ThisValue = GetNumericValue(ThisFieldValue) * CDbl(ThisRow(MultiplierOrdinal))
      End If

      ThisRow(ValueOrdinal) = ThisValue
    Next

    Return tblNumericSelection

  End Function

  Private Function GetNumericValue(ByVal pStringValue As String) As Double
    ' ************************************************************************
    ' Return Best conversion of the given string to a Double.
    '
    '
    ' ************************************************************************

    Try
      ' OK, lets start
      Dim StringValue As String = pStringValue

      ' Easy Case...

      If (IsNumeric(StringValue)) Then
        Return CDbl(StringValue)
      End If

      ' Eliminate ',', '%'
      StringValue = StringValue.Replace(",", "")
      StringValue = StringValue.Replace("%", "")
      StringValue = StringValue.Replace("$", "")

      If (StringValue.Length <= 0) Then
        Return 0
      End If

      If (IsNumeric(StringValue)) Then
        Return CDbl(StringValue)
      End If

      ' Strip leading Non-Numeric values

      Dim FirstNumeric As Integer = 0
      For FirstNumeric = 0 To (StringValue.Length - 1)
        If (Char.IsNumber(StringValue.Chars(FirstNumeric))) OrElse (StringValue.Chars(FirstNumeric) = "-") Then
          Exit For
        End If
      Next

      If FirstNumeric < StringValue.Length Then
        StringValue = StringValue.Substring(FirstNumeric)
      Else
        Return 0
      End If

      If (IsNumeric(StringValue)) Then
        Return CDbl(StringValue)
      End If

      ' Strip Trailing Non-Numeric values

      FirstNumeric = 0
      For FirstNumeric = 0 To (StringValue.Length - 1)
        If (Char.IsNumber(StringValue.Chars(FirstNumeric)) = False) Then
          Exit For
        End If
      Next

      If FirstNumeric < StringValue.Length Then
        StringValue = StringValue.Substring(0, FirstNumeric)
      Else
        Return 0
      End If

      If (IsNumeric(StringValue)) Then
        Return CDbl(StringValue)
      End If

      Return 0
    Catch ex As Exception
      Return 0
    End Try

    Return 0

  End Function

  Private Function GetCustomFieldSelection(ByRef SelectionItem As SelectionItemClass) As Integer()
    ' ************************************************************************
    ' Return Array of PertracIDs relating to the given Custom Field Selection.
    '
    '
    ' ************************************************************************

    Dim CustomFieldID As Integer = 0
    Dim SelectCondition As String = ""
    Dim SelectValue As String = ""
    Dim SelectValue2 As String = ""
    Dim ResultsArray() As Integer

    ' Is a Custom field selected ?
    If (SelectionItem.IsCustomField = False) Then
      Return Array.CreateInstance(GetType(Integer), 0)
    End If

    ' Validate the selected Field, get selection criteria.

    Try
      If (IsNumeric(SelectionItem.SelectFieldID) = False) OrElse (CInt(SelectionItem.SelectFieldID) <= 0) Then
        Return Array.CreateInstance(GetType(Integer), 0)
      End If

      ' Get Selection Criteria

      CustomFieldID = CInt(SelectionItem.SelectFieldID)
      SelectCondition = SelectionItem.ConditionOperand
      SelectValue = SelectionItem.SelectValue
      SelectValue2 = SelectionItem.SelectValue2

      ResultsArray = CustomFieldDataCache.GetMatchingInstrumentIDs(CustomFieldID, 0, SelectCondition, SelectValue, SelectValue2)

    Catch ex As Exception
      Return Array.CreateInstance(GetType(Integer), 0)
    End Try

    If ResultsArray.Length > 0 Then
      Array.Sort(ResultsArray)
    End If

    Return ResultsArray

  End Function


  Private Function CombineIntegerArray(ByVal pCombineType As String, ByRef pArrayOne() As Integer, ByRef pArrayTwo() As Integer) As Integer()
    ' ************************************************************************
    ' Function to combine two integer arrays of PertracIDs according to the 
    ' given Boolean operator : 'OR' or 'AND'.
    '
    '
    ' ************************************************************************

    Dim RVal(-1) As Integer
    Dim tmpArray() As Integer

    If (pCombineType Is Nothing) OrElse (pCombineType.Trim.ToUpper = "OR") Then
      ' Check Simple cases
      If (pArrayOne.Length <= 0) OrElse (pArrayTwo.Length <= 0) Then
        If (pArrayOne.Length <= 0) AndAlso (pArrayTwo.Length <= 0) Then
          ReDim RVal(-1)
          Return RVal
        ElseIf (pArrayOne.Length <= 0) Then
          ReDim RVal(pArrayTwo.Length - 1)
          pArrayTwo.CopyTo(RVal, 0)
          Return RVal
        ElseIf (pArrayTwo.Length <= 0) Then
          ReDim RVal(pArrayOne.Length - 1)
          pArrayOne.CopyTo(RVal, 0)
          Return RVal
        End If
      End If

      ' Sort the parameter arrays

      Array.Sort(pArrayOne)
      Array.Sort(pArrayTwo)

      ReDim tmpArray(pArrayOne.Length + pArrayTwo.Length)

      ' Combine arrays

      Dim CounterOne As Integer
      Dim CounterTwo As Integer
      Dim ResultCounter As Integer
      Dim LoopFlag As Boolean

      CounterOne = 0
      CounterTwo = 0
      ResultCounter = 0
      LoopFlag = True

      ' Perform 'OR' Combination

      While LoopFlag
        If (CounterOne >= pArrayOne.Length) Then

          While (CounterTwo < pArrayTwo.Length)
            tmpArray(ResultCounter) = pArrayTwo(CounterTwo)

            ResultCounter += 1
            CounterTwo += 1
          End While

          LoopFlag = False

        ElseIf (CounterTwo >= pArrayTwo.Length) Then

          While (CounterOne < pArrayOne.Length)

            tmpArray(ResultCounter) = pArrayOne(CounterOne)

            ResultCounter += 1
            CounterOne += 1
          End While

          LoopFlag = False

        Else
          If (pArrayOne(CounterOne) < pArrayTwo(CounterTwo)) Then

            tmpArray(ResultCounter) = pArrayOne(CounterOne)

            CounterOne += 1
          ElseIf (pArrayOne(CounterOne) > pArrayTwo(CounterTwo)) Then

            tmpArray(ResultCounter) = pArrayTwo(CounterTwo)

            CounterTwo += 1
          Else

            tmpArray(ResultCounter) = pArrayOne(CounterOne)

            CounterOne += 1
            CounterTwo += 1
          End If

          ResultCounter += 1
        End If
      End While


      ' Return derived array.

      ReDim RVal(ResultCounter - 1)
      Array.Copy(tmpArray, RVal, ResultCounter)

      Return RVal

    Else
      ' 'AND'

      ' Check Simple cases

      If (pArrayOne.Length <= 0) OrElse (pArrayTwo.Length <= 0) Then
        Return RVal
      End If

      ' Sort the parameter arrays

      Array.Sort(pArrayOne)
      Array.Sort(pArrayTwo)

      ReDim tmpArray(MAX(pArrayOne.Length, pArrayTwo.Length))

      ' Combine arrays

      Dim CounterOne As Integer
      Dim CounterTwo As Integer
      Dim ResultCounter As Integer
      Dim LoopFlag As Boolean

      CounterOne = 0
      CounterTwo = 0
      ResultCounter = 0
      LoopFlag = True

      ' Perform 'OR' Combination

      While LoopFlag
        If (CounterOne >= pArrayOne.Length) OrElse (CounterTwo >= pArrayTwo.Length) Then

          LoopFlag = False

        Else

          If (pArrayOne(CounterOne) < pArrayTwo(CounterTwo)) Then

            CounterOne += 1
          ElseIf (pArrayOne(CounterOne) > pArrayTwo(CounterTwo)) Then

            CounterTwo += 1
          Else

            tmpArray(ResultCounter) = pArrayOne(CounterOne)

            CounterOne += 1
            CounterTwo += 1
            ResultCounter += 1
          End If

        End If
      End While

      ' Return derived array.

      If (ResultCounter > 0) Then
        ReDim RVal(ResultCounter - 1)
        Array.Copy(tmpArray, RVal, ResultCounter)
      End If

      Return RVal
    End If

    Return RVal

  End Function



#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  Private Sub Grid_Results_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Results.KeyDown
    ' *************************************************************************************
    ' Clipboard 'Copy' functionality
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), False, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub ComboBox_SavedSearches_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_SavedSearches.SelectedIndexChanged, ComboBox_SavedSearches.SelectedIndexChanged
    ' ************************************************************************
    '
    '
    ' ************************************************************************
    Dim ThisCombo As System.Windows.Forms.ToolStripComboBox = Nothing
    Dim thisMenuItem As ToolStripMenuItem

    Try

      If (TypeOf sender Is System.Windows.Forms.ToolStripComboBox) Then
        ThisCombo = sender
      Else
        Exit Sub
      End If

      If (ThisCombo.SelectedIndex <= 0) Then

        If (ThisCombo IsNot Nothing) AndAlso (ThisCombo.SelectedIndex < 0) AndAlso (ThisCombo.Items.Count > 0) Then
          ThisCombo.SelectedIndex = 0
        End If

        Exit Sub
      End If

      thisMenuItem = Menu_SavedSearches.DropDownItems("Menu_SavedSearch_" & (ThisCombo.SelectedIndex - 1).ToString)

      Menu_SavedSearchesItem_Click(thisMenuItem, New System.EventArgs)

    Catch ex As Exception
    Finally

      If (ThisCombo IsNot Nothing) AndAlso (ThisCombo.Items.Count > 0) Then
        ThisCombo.SelectedIndex = 0
      End If

    End Try
  End Sub





End Class
