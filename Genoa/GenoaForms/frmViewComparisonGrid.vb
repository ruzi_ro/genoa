Imports RenaissanceGlobals


Public Class frmViewComparisonGrid
	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents UpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_MirrorChanges As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Grid_Comp_Statistics As C1.Win.C1FlexGrid.C1FlexGrid

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewComparisonGrid))
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.UpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_MirrorChanges = New System.Windows.Forms.ToolStripMenuItem
		Me.Grid_Comp_Statistics = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.RootMenu.SuspendLayout()
		CType(Me.Grid_Comp_Statistics, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UpdatesToolStripMenuItem})
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(731, 24)
		Me.RootMenu.TabIndex = 13
		Me.RootMenu.Text = "MenuStrip1"
		'
		'UpdatesToolStripMenuItem
		'
		Me.UpdatesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_MirrorChanges})
		Me.UpdatesToolStripMenuItem.Name = "UpdatesToolStripMenuItem"
		Me.UpdatesToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
		Me.UpdatesToolStripMenuItem.Text = "Updates"
		'
		'Menu_MirrorChanges
		'
		Me.Menu_MirrorChanges.Checked = True
		Me.Menu_MirrorChanges.CheckState = System.Windows.Forms.CheckState.Checked
		Me.Menu_MirrorChanges.Name = "Menu_MirrorChanges"
		Me.Menu_MirrorChanges.Size = New System.Drawing.Size(232, 22)
		Me.Menu_MirrorChanges.Text = "Mirror changes to parent chart"
		'
		'Grid_Comp_Statistics
		'
		Me.Grid_Comp_Statistics.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Comp_Statistics.ColumnInfo = resources.GetString("Grid_Comp_Statistics.ColumnInfo")
		Me.Grid_Comp_Statistics.Cursor = System.Windows.Forms.Cursors.Default
		Me.Grid_Comp_Statistics.Location = New System.Drawing.Point(0, 24)
		Me.Grid_Comp_Statistics.Name = "Grid_Comp_Statistics"
		Me.Grid_Comp_Statistics.Rows.Count = 2
		Me.Grid_Comp_Statistics.Rows.DefaultSize = 17
		Me.Grid_Comp_Statistics.Size = New System.Drawing.Size(731, 447)
		Me.Grid_Comp_Statistics.TabIndex = 14
		'
		'frmViewComparisonGrid
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(731, 471)
		Me.Controls.Add(Me.Grid_Comp_Statistics)
		Me.Controls.Add(Me.RootMenu)
		Me.Name = "frmViewComparisonGrid"
		Me.Text = "Genoa Comparison Grid"
		Me.RootMenu.ResumeLayout(False)
		Me.RootMenu.PerformLayout()
		CType(Me.Grid_Comp_Statistics, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Local Variable Declaration"

	Private WithEvents _MainForm As GenoaMain
	Private WithEvents _GenoaParentForm As Form

	Private _FormOpenFailed As Boolean

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	Private _FormGrid As C1.Win.C1FlexGrid.C1FlexGrid

	Private _ParentGridList As ArrayList

	Private InPaint As Boolean

	Public Delegate Sub UpdateGridDelegate(ByVal UpdateGrid As C1.Win.C1FlexGrid.C1FlexGrid)

	Private _UpdateGridSub As UpdateGridDelegate

#End Region

#Region " Form Properties"

	Public ReadOnly Property DisplayedGrid() As C1.Win.C1FlexGrid.C1FlexGrid
		Get
			Return Grid_Comp_Statistics
		End Get
	End Property

	Public Property ParentGridList() As ArrayList
		Get
			Return _ParentGridList
		End Get
		Set(ByVal value As ArrayList)
			_ParentGridList = value
		End Set
	End Property

	Public Property FormGrid() As C1.Win.C1FlexGrid.C1FlexGrid
		' *******************************************************************************
		' Set the Parent chart control.
		' Copy Format and Data from the parent control.
		'
		' *******************************************************************************

		Get
			Return _FormGrid
		End Get

		Set(ByVal value As C1.Win.C1FlexGrid.C1FlexGrid)

			Try
				Dim Row As Integer
				Dim Col As Integer
				Dim TargetColID As Integer

				_FormGrid = value

				Grid_Comp_Statistics.Rows.Count = _FormGrid.Rows.Count
				
				For Col = 0 To (_FormGrid.Cols.Count - 1)
					TargetColID = Grid_Comp_Statistics.Cols.IndexOf(_FormGrid.Cols(Col).Name)

					For Row = 1 To (_FormGrid.Rows.Count - 1)
						Grid_Comp_Statistics.Item(Row, TargetColID) = _FormGrid.Item(Row, Col)

						If (Col > 0) Then
							If (CDbl(Grid_Comp_Statistics.Item(Row, TargetColID)) < 0) Then
								Grid_Comp_Statistics.SetCellStyle(Row, TargetColID, "Negative")
							Else
								Grid_Comp_Statistics.SetCellStyle(Row, TargetColID, "Positive")
							End If
						End If
					Next
				Next
			Catch ex As Exception
			End Try

		End Set
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)

		End Set
	End Property

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Me.Close()
	End Sub

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		Get
			Return _MainForm
		End Get
	End Property

	Public Property GenoaParentForm() As Form
		Get
			Return _GenoaParentForm
		End Get
		Set(ByVal value As Form)
			_GenoaParentForm = value
		End Set
	End Property

	Public Property UpdateGridSub() As UpdateGridDelegate
		Get
			Return _UpdateGridSub
		End Get
		Set(ByVal value As UpdateGridDelegate)
			_UpdateGridSub = value
		End Set
	End Property

	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

	End Sub


#End Region

	Public Sub New(ByRef pMainForm As GenoaMain)
		Me.New()

		_MainForm = pMainForm
		_GenoaParentForm = Nothing
		_UpdateGridSub = Nothing

		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		Grid_Comp_Statistics.Styles.Add("Positive").ForeColor = Color.Blue
		Grid_Comp_Statistics.Styles.Add("Negative").ForeColor = Color.Red
		Grid_Comp_Statistics.Styles.Add("TopRow", Grid_Comp_Statistics.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
		Grid_Comp_Statistics.Styles.Add("FirstCell", Grid_Comp_Statistics.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
		Grid_Comp_Statistics.Rows(0).Style = Grid_Comp_Statistics.Styles("TopRow")
		Grid_Comp_Statistics.SetCellStyle(0, 0, "FirstCell")

		_FormOpenFailed = False

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub

	Private Sub frmViewComparisonGrid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


	End Sub

	Private Sub ParentClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles _GenoaParentForm.FormClosed
		' ***********************************************
		' Close this Form if the Parent Form closes.
		'
		' ***********************************************

		Me.Close()
	End Sub

	Private Sub frmViewComparisonGrid_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ***********************************************
		' Tidy up the parent chart list array to stop future updates.
		'
		' ***********************************************

		Try
			MainForm.RemoveFromFormsCollection(Me)
		Catch ex As Exception
		End Try

		Try
			Grid_Comp_Statistics.Visible = False

			If (Me._ParentGridList IsNot Nothing) Then
				SyncLock _ParentGridList
					If _ParentGridList.Contains(Grid_Comp_Statistics) Then
						_ParentGridList.Remove(Grid_Comp_Statistics)
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub frmViewComparisonGrid_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		' ***********************************************
		' Tidy up the parent chart list array to stop future updates.
		'
		' ***********************************************

		Try
			Grid_Comp_Statistics.Visible = False

			If (Me._ParentGridList IsNot Nothing) Then
				SyncLock _ParentGridList
					If _ParentGridList.Contains(Grid_Comp_Statistics) Then
						_ParentGridList.Remove(Grid_Comp_Statistics)
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True

		Try
			KnowledgeDateChanged = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If

		Catch ex As Exception
		Finally
			InPaint = OrgInPaint
		End Try

	End Sub

	Private Sub Menu_MirrorChanges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Menu_MirrorChanges.Click
		Menu_MirrorChanges.Checked = Not Menu_MirrorChanges.Checked
	End Sub

	Private Sub Menu_MirrorChanges_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_MirrorChanges.CheckedChanged
		' ***********************************************
		' Enable / Disable chart updates by adding / Removing 
		' The Chart_Chart control from / to the registered
		' Chart List Array.
		'
		' ***********************************************
		Try
			If Me.Menu_MirrorChanges.Checked Then
				If (Me._ParentGridList IsNot Nothing) Then
					SyncLock _ParentGridList
						If (_ParentGridList.Contains(Grid_Comp_Statistics) = False) Then
							_ParentGridList.Add(Grid_Comp_Statistics)
						End If
					End SyncLock

					' refresh chart.
					If (_UpdateGridSub Is Nothing) Then
						Me.FormGrid = Me._FormGrid
					Else
						_UpdateGridSub(Grid_Comp_Statistics)
					End If

				End If
			Else
				If (Me._ParentGridList IsNot Nothing) Then
					SyncLock _ParentGridList
						If _ParentGridList.Contains(Grid_Comp_Statistics) Then
							_ParentGridList.Remove(Grid_Comp_Statistics)
						End If
					End SyncLock
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub


  Private Sub Grid_Comp_Statistics_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Comp_Statistics.KeyDown
    ' *************************************************************************************
    ' Clipboard 'Copy' functionality
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

End Class
