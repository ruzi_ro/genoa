Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions


Public Class frmGroupReturns

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents Combo_SelectGroupID As System.Windows.Forms.ComboBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents Grid_Returns As C1.Win.C1FlexGrid.C1FlexGrid
	Friend WithEvents StatusStrip_GroupReturns As System.Windows.Forms.StatusStrip
	Friend WithEvents StatusLabel_GroupReturns As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Label_GroupStartDate As System.Windows.Forms.Label
	Friend WithEvents Label_GroupEndDate As System.Windows.Forms.Label
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents GroupBox_Actions As System.Windows.Forms.GroupBox
	Friend WithEvents Btn_SetAllBenchmarkIndex As System.Windows.Forms.Button
	Friend WithEvents Btn_SetSingleBenchmarkIndex As System.Windows.Forms.Button
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Combo_BenckmarkIndex As System.Windows.Forms.ComboBox
	Friend WithEvents Split_GroupReturns As System.Windows.Forms.SplitContainer
	Friend WithEvents Chart_VAMI As Dundas.Charting.WinControl.Chart
	Friend WithEvents Btn_ShowCarts As System.Windows.Forms.Button
	Friend WithEvents Chart_MonthlyReturns As Dundas.Charting.WinControl.Chart
	Friend WithEvents Label_ChartStock As System.Windows.Forms.Label
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim ChartArea3 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
		Dim Legend3 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
		Dim Series5 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim Series6 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim ChartArea4 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
		Dim Legend4 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
		Dim Series7 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim Series8 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectGroupID = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.Grid_Returns = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.StatusStrip_GroupReturns = New System.Windows.Forms.StatusStrip
		Me.StatusLabel_GroupReturns = New System.Windows.Forms.ToolStripStatusLabel
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label_GroupStartDate = New System.Windows.Forms.Label
		Me.Label_GroupEndDate = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.GroupBox_Actions = New System.Windows.Forms.GroupBox
		Me.Btn_SetAllBenchmarkIndex = New System.Windows.Forms.Button
		Me.Btn_SetSingleBenchmarkIndex = New System.Windows.Forms.Button
		Me.Label3 = New System.Windows.Forms.Label
		Me.Combo_BenckmarkIndex = New System.Windows.Forms.ComboBox
		Me.Split_GroupReturns = New System.Windows.Forms.SplitContainer
		Me.Label_ChartStock = New System.Windows.Forms.Label
		Me.Chart_MonthlyReturns = New Dundas.Charting.WinControl.Chart
		Me.Chart_VAMI = New Dundas.Charting.WinControl.Chart
		Me.Btn_ShowCarts = New System.Windows.Forms.Button
		CType(Me.Grid_Returns, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.StatusStrip_GroupReturns.SuspendLayout()
		Me.GroupBox_Actions.SuspendLayout()
		Me.Split_GroupReturns.Panel1.SuspendLayout()
		Me.Split_GroupReturns.Panel2.SuspendLayout()
		Me.Split_GroupReturns.SuspendLayout()
		CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(867, 60)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(50, 20)
		Me.editAuditID.TabIndex = 2
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(452, 31)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 23)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(494, 31)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 23)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(532, 31)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 23)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(568, 31)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 23)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(238, 556)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 10
		Me.btnCancel.Text = "Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(66, 556)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 8
		Me.btnSave.Text = "Save"
		'
		'Combo_SelectGroupID
		'
		Me.Combo_SelectGroupID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroupID.Location = New System.Drawing.Point(121, 60)
		Me.Combo_SelectGroupID.Name = "Combo_SelectGroupID"
		Me.Combo_SelectGroupID.Size = New System.Drawing.Size(740, 21)
		Me.Combo_SelectGroupID.TabIndex = 1
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(6, 63)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(87, 14)
		Me.Label1.TabIndex = 18
		Me.Label1.Text = "Select"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(322, 556)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 11
		Me.btnClose.Text = "Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(930, 24)
		Me.RootMenu.TabIndex = 12
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label6.Location = New System.Drawing.Point(6, 36)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(69, 13)
		Me.Label6.TabIndex = 100
		Me.Label6.Text = "Select Group"
		'
		'Combo_SelectGroup
		'
		Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup.Location = New System.Drawing.Point(121, 33)
		Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
		Me.Combo_SelectGroup.Size = New System.Drawing.Size(318, 21)
		Me.Combo_SelectGroup.TabIndex = 0
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(0, 83)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(930, 10)
		Me.GroupBox1.TabIndex = 101
		Me.GroupBox1.TabStop = False
		'
		'Grid_Returns
		'
		Me.Grid_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Returns.ColumnInfo = "10,0,0,0,0,85,Columns:0{Width:170;}" & Global.Microsoft.VisualBasic.ChrW(9)
		Me.Grid_Returns.Location = New System.Drawing.Point(0, 0)
		Me.Grid_Returns.Name = "Grid_Returns"
		Me.Grid_Returns.Rows.Count = 2
		Me.Grid_Returns.Rows.DefaultSize = 17
		Me.Grid_Returns.Size = New System.Drawing.Size(630, 380)
		Me.Grid_Returns.TabIndex = 102
		'
		'StatusStrip_GroupReturns
		'
		Me.StatusStrip_GroupReturns.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel_GroupReturns})
		Me.StatusStrip_GroupReturns.Location = New System.Drawing.Point(0, 588)
		Me.StatusStrip_GroupReturns.Name = "StatusStrip_GroupReturns"
		Me.StatusStrip_GroupReturns.Size = New System.Drawing.Size(930, 22)
		Me.StatusStrip_GroupReturns.TabIndex = 103
		Me.StatusStrip_GroupReturns.Text = "StatusStrip1"
		'
		'StatusLabel_GroupReturns
		'
		Me.StatusLabel_GroupReturns.Name = "StatusLabel_GroupReturns"
		Me.StatusLabel_GroupReturns.Size = New System.Drawing.Size(111, 17)
		Me.StatusLabel_GroupReturns.Text = "ToolStripStatusLabel1"
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(6, 98)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(101, 17)
		Me.Label2.TabIndex = 104
		Me.Label2.Text = "Group Start Date"
		'
		'Label_GroupStartDate
		'
		Me.Label_GroupStartDate.BackColor = System.Drawing.SystemColors.Window
		Me.Label_GroupStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Label_GroupStartDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_GroupStartDate.Location = New System.Drawing.Point(121, 98)
		Me.Label_GroupStartDate.Name = "Label_GroupStartDate"
		Me.Label_GroupStartDate.Size = New System.Drawing.Size(149, 19)
		Me.Label_GroupStartDate.TabIndex = 105
		Me.Label_GroupStartDate.Text = "MgmtFee"
		'
		'Label_GroupEndDate
		'
		Me.Label_GroupEndDate.BackColor = System.Drawing.SystemColors.Window
		Me.Label_GroupEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Label_GroupEndDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_GroupEndDate.Location = New System.Drawing.Point(121, 124)
		Me.Label_GroupEndDate.Name = "Label_GroupEndDate"
		Me.Label_GroupEndDate.Size = New System.Drawing.Size(149, 19)
		Me.Label_GroupEndDate.TabIndex = 107
		Me.Label_GroupEndDate.Text = "MgmtFee"
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(6, 125)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(101, 16)
		Me.Label4.TabIndex = 106
		Me.Label4.Text = "Group End Date"
		'
		'GroupBox_Actions
		'
		Me.GroupBox_Actions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox_Actions.Controls.Add(Me.Btn_SetAllBenchmarkIndex)
		Me.GroupBox_Actions.Controls.Add(Me.Btn_SetSingleBenchmarkIndex)
		Me.GroupBox_Actions.Controls.Add(Me.Label3)
		Me.GroupBox_Actions.Controls.Add(Me.Combo_BenckmarkIndex)
		Me.GroupBox_Actions.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.GroupBox_Actions.Location = New System.Drawing.Point(287, 92)
		Me.GroupBox_Actions.Name = "GroupBox_Actions"
		Me.GroupBox_Actions.Size = New System.Drawing.Size(638, 68)
		Me.GroupBox_Actions.TabIndex = 108
		Me.GroupBox_Actions.TabStop = False
		Me.GroupBox_Actions.Text = "Set Benchmark Index"
		'
		'Btn_SetAllBenchmarkIndex
		'
		Me.Btn_SetAllBenchmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Btn_SetAllBenchmarkIndex.Location = New System.Drawing.Point(317, 41)
		Me.Btn_SetAllBenchmarkIndex.Name = "Btn_SetAllBenchmarkIndex"
		Me.Btn_SetAllBenchmarkIndex.Size = New System.Drawing.Size(188, 23)
		Me.Btn_SetAllBenchmarkIndex.TabIndex = 104
		Me.Btn_SetAllBenchmarkIndex.Text = "Set As Benckmark - ALL Lines"
		'
		'Btn_SetSingleBenchmarkIndex
		'
		Me.Btn_SetSingleBenchmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Btn_SetSingleBenchmarkIndex.Location = New System.Drawing.Point(123, 41)
		Me.Btn_SetSingleBenchmarkIndex.Name = "Btn_SetSingleBenchmarkIndex"
		Me.Btn_SetSingleBenchmarkIndex.Size = New System.Drawing.Size(188, 23)
		Me.Btn_SetSingleBenchmarkIndex.TabIndex = 103
		Me.Btn_SetSingleBenchmarkIndex.Text = "Set As Benckmark - This Line Only"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Location = New System.Drawing.Point(8, 19)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(89, 13)
		Me.Label3.TabIndex = 102
		Me.Label3.Text = "Select Instrument"
		'
		'Combo_BenckmarkIndex
		'
		Me.Combo_BenckmarkIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_BenckmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_BenckmarkIndex.Location = New System.Drawing.Point(123, 16)
		Me.Combo_BenckmarkIndex.Name = "Combo_BenckmarkIndex"
		Me.Combo_BenckmarkIndex.Size = New System.Drawing.Size(507, 21)
		Me.Combo_BenckmarkIndex.TabIndex = 101
		'
		'Split_GroupReturns
		'
		Me.Split_GroupReturns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Split_GroupReturns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Split_GroupReturns.Location = New System.Drawing.Point(0, 166)
		Me.Split_GroupReturns.Margin = New System.Windows.Forms.Padding(0)
		Me.Split_GroupReturns.Name = "Split_GroupReturns"
		'
		'Split_GroupReturns.Panel1
		'
		Me.Split_GroupReturns.Panel1.Controls.Add(Me.Grid_Returns)
		'
		'Split_GroupReturns.Panel2
		'
		Me.Split_GroupReturns.Panel2.Controls.Add(Me.Label_ChartStock)
		Me.Split_GroupReturns.Panel2.Controls.Add(Me.Chart_MonthlyReturns)
		Me.Split_GroupReturns.Panel2.Controls.Add(Me.Chart_VAMI)
		Me.Split_GroupReturns.Size = New System.Drawing.Size(930, 384)
		Me.Split_GroupReturns.SplitterDistance = 636
		Me.Split_GroupReturns.SplitterWidth = 3
		Me.Split_GroupReturns.TabIndex = 109
		'
		'Label_ChartStock
		'
		Me.Label_ChartStock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_ChartStock.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_ChartStock.Location = New System.Drawing.Point(3, 0)
		Me.Label_ChartStock.Name = "Label_ChartStock"
		Me.Label_ChartStock.Size = New System.Drawing.Size(281, 15)
		Me.Label_ChartStock.TabIndex = 101
		'
		'Chart_MonthlyReturns
		'
		Me.Chart_MonthlyReturns.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_MonthlyReturns.BackColor = System.Drawing.Color.Azure
		Me.Chart_MonthlyReturns.BackGradientEndColor = System.Drawing.Color.SkyBlue
		Me.Chart_MonthlyReturns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
		Me.Chart_MonthlyReturns.BorderLineColor = System.Drawing.Color.LightGray
		Me.Chart_MonthlyReturns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
		Me.Chart_MonthlyReturns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
		ChartArea3.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
		ChartArea3.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea3.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
		ChartArea3.AxisX.LabelStyle.Format = "Y"
		ChartArea3.AxisX.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea3.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisX2.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea3.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
		ChartArea3.AxisY.LabelStyle.Format = "P0"
		ChartArea3.AxisY.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea3.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea3.AxisY.StartFromZero = False
		ChartArea3.AxisY2.LineColor = System.Drawing.Color.DimGray
		ChartArea3.BackColor = System.Drawing.Color.Transparent
		ChartArea3.BorderColor = System.Drawing.Color.DimGray
		ChartArea3.Name = "Default"
		Me.Chart_MonthlyReturns.ChartAreas.Add(ChartArea3)
		Legend3.BackColor = System.Drawing.Color.Transparent
		Legend3.BorderColor = System.Drawing.Color.Transparent
		Legend3.Docking = Dundas.Charting.WinControl.LegendDocking.Left
		Legend3.DockToChartArea = "Default"
		Legend3.Enabled = False
		Legend3.Name = "Default"
		Me.Chart_MonthlyReturns.Legends.Add(Legend3)
		Me.Chart_MonthlyReturns.Location = New System.Drawing.Point(0, 192)
		Me.Chart_MonthlyReturns.Margin = New System.Windows.Forms.Padding(1)
		Me.Chart_MonthlyReturns.MinimumSize = New System.Drawing.Size(150, 150)
		Me.Chart_MonthlyReturns.Name = "Chart_MonthlyReturns"
		Me.Chart_MonthlyReturns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
		Series5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series5.BorderWidth = 2
		Series5.ChartType = "Line"
		Series5.CustomAttributes = "LabelStyle=Top"
		Series5.Name = "Series1"
		Series5.ShadowOffset = 1
		Series5.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series5.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series6.BorderWidth = 2
		Series6.ChartType = "Line"
		Series6.CustomAttributes = "LabelStyle=Top"
		Series6.Name = "Series2"
		Series6.ShadowOffset = 1
		Series6.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series6.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Me.Chart_MonthlyReturns.Series.Add(Series5)
		Me.Chart_MonthlyReturns.Series.Add(Series6)
		Me.Chart_MonthlyReturns.Size = New System.Drawing.Size(285, 188)
		Me.Chart_MonthlyReturns.TabIndex = 2
		Me.Chart_MonthlyReturns.Text = "Chart2"
		'
		'Chart_VAMI
		'
		Me.Chart_VAMI.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_VAMI.BackColor = System.Drawing.Color.Azure
		Me.Chart_VAMI.BackGradientEndColor = System.Drawing.Color.SkyBlue
		Me.Chart_VAMI.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
		Me.Chart_VAMI.BorderLineColor = System.Drawing.Color.LightGray
		Me.Chart_VAMI.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
		Me.Chart_VAMI.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
		ChartArea4.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
		ChartArea4.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea4.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
		ChartArea4.AxisX.LabelStyle.Format = "Y"
		ChartArea4.AxisX.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea4.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisX2.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea4.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
		ChartArea4.AxisY.LabelStyle.Format = "N0"
		ChartArea4.AxisY.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea4.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea4.AxisY.StartFromZero = False
		ChartArea4.AxisY2.LineColor = System.Drawing.Color.DimGray
		ChartArea4.BackColor = System.Drawing.Color.Transparent
		ChartArea4.BorderColor = System.Drawing.Color.DimGray
		ChartArea4.Name = "Default"
		Me.Chart_VAMI.ChartAreas.Add(ChartArea4)
		Legend4.BackColor = System.Drawing.Color.Transparent
		Legend4.BorderColor = System.Drawing.Color.Transparent
		Legend4.Docking = Dundas.Charting.WinControl.LegendDocking.Left
		Legend4.DockToChartArea = "Default"
		Legend4.Enabled = False
		Legend4.Name = "Default"
		Me.Chart_VAMI.Legends.Add(Legend4)
		Me.Chart_VAMI.Location = New System.Drawing.Point(1, 16)
		Me.Chart_VAMI.Margin = New System.Windows.Forms.Padding(1)
		Me.Chart_VAMI.MinimumSize = New System.Drawing.Size(150, 150)
		Me.Chart_VAMI.Name = "Chart_VAMI"
		Me.Chart_VAMI.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
		Series7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series7.BorderWidth = 2
		Series7.ChartType = "Line"
		Series7.CustomAttributes = "LabelStyle=Top"
		Series7.Name = "Series1"
		Series7.ShadowOffset = 1
		Series7.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series7.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series8.BorderWidth = 2
		Series8.ChartType = "Line"
		Series8.CustomAttributes = "LabelStyle=Top"
		Series8.Name = "Series2"
		Series8.ShadowOffset = 1
		Series8.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series8.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Me.Chart_VAMI.Series.Add(Series7)
		Me.Chart_VAMI.Series.Add(Series8)
		Me.Chart_VAMI.Size = New System.Drawing.Size(285, 174)
		Me.Chart_VAMI.TabIndex = 1
		Me.Chart_VAMI.Text = "Chart2"
		'
		'Btn_ShowCarts
		'
		Me.Btn_ShowCarts.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Btn_ShowCarts.Location = New System.Drawing.Point(642, 31)
		Me.Btn_ShowCarts.Name = "Btn_ShowCarts"
		Me.Btn_ShowCarts.Size = New System.Drawing.Size(96, 23)
		Me.Btn_ShowCarts.TabIndex = 110
		Me.Btn_ShowCarts.Text = "Hide Charts"
		'
		'frmGroupReturns
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(930, 610)
		Me.Controls.Add(Me.Btn_ShowCarts)
		Me.Controls.Add(Me.Split_GroupReturns)
		Me.Controls.Add(Me.GroupBox_Actions)
		Me.Controls.Add(Me.Label_GroupEndDate)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Label_GroupStartDate)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.StatusStrip_GroupReturns)
		Me.Controls.Add(Me.btnNavFirst)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.btnNavPrev)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.btnNavNext)
		Me.Controls.Add(Me.Combo_SelectGroup)
		Me.Controls.Add(Me.btnLast)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_SelectGroupID)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(458, 339)
		Me.Name = "frmGroupReturns"
		Me.Text = "View/Edit Group Returns"
		CType(Me.Grid_Returns, System.ComponentModel.ISupportInitialize).EndInit()
		Me.StatusStrip_GroupReturns.ResumeLayout(False)
		Me.StatusStrip_GroupReturns.PerformLayout()
		Me.GroupBox_Actions.ResumeLayout(False)
		Me.GroupBox_Actions.PerformLayout()
		Me.Split_GroupReturns.Panel1.ResumeLayout(False)
		Me.Split_GroupReturns.Panel2.ResumeLayout(False)
		Me.Split_GroupReturns.ResumeLayout(False)
		CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()
	Dim FieldsMenu As ToolStripMenuItem = Nothing
	Dim CustomFieldsMenu As ToolStripMenuItem = Nothing

	Private GetVeniceHoldingsValueDate As Date
	Private GetVeniceHoldingsDatePicker As ToolStripControlHost
	Private GetVeniceHoldingsLookthroughCheckboxChecked As Boolean
	Private GetVeniceHoldingsLookthroughCheckbox As ToolStripControlHost

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  Private GenericUpdateObject As RenaissanceGlobals.RenaissanceTimerUpdateClass

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String

	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	Private THIS_FORM_SelectingCombo As ComboBox
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private myDataset As RenaissanceDataClass.DSGroupList			 ' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
	Private myConnection As SqlConnection
	Private myAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

	Private SortedRows() As DataRow
	Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSGroupList.tblGroupListRow		' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean
  Private InFormUpdate_Tick As Boolean

	Private SelectedGroupID As Integer = 0
	Private DSSelectedGroup As New RenaissanceDataClass.DSGroupsAggregated
	Private LoadedAggregatedGroupListID As Integer = 0

  Private PertracGridCombo As New RadC1GridCombo ' CustomC1Combo
  Private SectorCombo As New CustomC1Combo

	Private GridContextMenuStrip As ContextMenuStrip
	Private GridClickMouseRow As Integer = 0
	Private GridClickMouseCol As Integer = 0

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' Dictionary of Integer keys and boolean values relating to Custom Field IDs. A 'True' value
	' indicates which Custom Fieldsare to appear on the results grid.
	Private Grid_CustomFields_Display_Dictionary As New Dictionary(Of Integer, Boolean)
	Friend CustomFieldDataCache As CustomFieldDataCacheClass	' New

	' Cache object for 'Get positions from Venice' functionality
	Dim tblVeniceHoldings As DataTable = Nothing
	Dim tblVeniceHoldings_FundID As Integer = (-1)

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		If (Me.IsDisposed) Then Exit Sub

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Set pointer to Custom Field Data Cache
		' ******************************************************

		CustomFieldDataCache = MainForm.CustomFieldDataCache

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectGroupID

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		THIS_FORM_ValueMember = "GroupListID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmGroupReturns

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList	 ' This Defines the Form Data !!! 

		PertracGridCombo.MasternameCollection = MainForm.MasternameDictionary

    ' MainForm.SetTblGenericCombo(PertracGridCombo, RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")

    'AddHandler PertracGridCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler PertracGridCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler PertracGridCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler PertracGridCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_BenckmarkIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_BenckmarkIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_BenckmarkIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged

		MainForm.AddCopyMenuToChart(Me.Chart_VAMI)
		MainForm.AddCopyMenuToChart(Me.Chart_MonthlyReturns)

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' NPP 7 Sep 2007
		' DSSelectedGroup = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupsAggregated, False)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		GetVeniceHoldingsValueDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
		GetVeniceHoldingsDatePicker = New ToolStripControlHost(GetNewCmsDateTimePicker)
		GetVeniceHoldingsLookthroughCheckboxChecked = False
		GetVeniceHoldingsLookthroughCheckbox = New ToolStripControlHost(GetNewCmsCheckbox)

		Try
			InPaint = True

			Grid_Returns.Cols.Count = 0
			Grid_Returns.AutoGenerateColumns = True
			Grid_Returns.DataSource = New DSGroupsAggregated.tblGroupsAggregatedDataTable
			Grid_Returns.AutoGenerateColumns = False
			Grid_Returns.DataSource = Nothing

			' 
			Grid_Returns.Cols.Count += 1
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Name = "Col_Updated"
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).DataType = GetType(String)
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Width = 10
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Move(0)
			Grid_Returns.Cols(0).AllowEditing = False
			Grid_Returns.Cols(0).AllowMerging = False
			Grid_Returns.Cols(0).AllowResizing = False
			Grid_Returns.Cols(0).AllowDragging = False
			Grid_Returns.Cols(0).Caption = ""

			Grid_Returns.Cols.Count += 1
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Name = "Col_CustomUpdated"
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).DataType = GetType(String)
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Width = 10
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Move(1)
			Grid_Returns.Cols(1).AllowEditing = False
			Grid_Returns.Cols(1).AllowMerging = False
			Grid_Returns.Cols(1).AllowResizing = False
			Grid_Returns.Cols(1).AllowDragging = False
			Grid_Returns.Cols(1).Caption = ""

			GridContextMenuStrip = New ContextMenuStrip
			AddHandler GridContextMenuStrip.Opening, AddressOf cms_Opening
			GridContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

			Grid_Returns.ContextMenuStrip = GridContextMenuStrip

			Try
				' Set Generic 'Edit' Style. 
				' This is used because the grid appears to limit data entry to the format applied to the Column

				Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

				ThisStyle = Me.Grid_Returns.Styles("Genoa_PlainEdit")
				If (ThisStyle Is Nothing) Then
					ThisStyle = Me.Grid_Returns.Styles.Add("Genoa_PlainEdit")
				End If
				ThisStyle.ForeColor = Color.Black
				ThisStyle.Format = ""
				ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Regular)

			Catch ex As Exception

			End Try

			Call SetGroupCombo()
			Call SetBenchmarkIndexCombo()

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

		' 
		FieldsMenu = SetFieldSelectMenu(RootMenu)
		CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

    GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		If (Combo_SelectGroup.Items.Count > 0) Then
			Combo_SelectGroup.SelectedIndex = 0
		End If

		' Initialise Grid.

		Try
			Dim thisCol As C1.Win.C1FlexGrid.Column
			Dim thisColID As Integer

			Grid_Returns.Rows.Count = 1

			For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update, Col_Custom_Update
				thisCol = Grid_Returns.Cols(thisColID)

				thisCol.Visible = False
				thisCol.AllowEditing = False
				thisCol.AllowMerging = False
				thisCol.Caption = thisCol.Name

				If thisCol.Caption.StartsWith("Group") AndAlso (thisCol.Caption.Length > 5) Then
					thisCol.Caption = thisCol.Caption.Substring(5)
				End If
			Next


			Grid_Returns.Cols("GroupIndexCode").AllowEditing = True
			Grid_Returns.Cols("GroupIndexE").AllowEditing = True
			Grid_Returns.Cols("GroupBetaE").AllowEditing = True
			Grid_Returns.Cols("GroupAlphaE").AllowEditing = True

			Grid_Returns.Cols("GroupSector").AllowEditing = True
			Grid_Returns.Cols("GroupLiquidity").AllowEditing = True
			Grid_Returns.Cols("GroupHolding").AllowEditing = True
			Grid_Returns.Cols("GroupNewHolding").AllowEditing = True
			Grid_Returns.Cols("GroupPercent").AllowEditing = False
			Grid_Returns.Cols("GroupUpperBound").AllowEditing = True
			Grid_Returns.Cols("GroupLowerBound").AllowEditing = True
			Grid_Returns.Cols("GroupFloor").AllowEditing = True
			Grid_Returns.Cols("GroupCap").AllowEditing = True
			Grid_Returns.Cols("GroupTradeSize").AllowEditing = True
			Grid_Returns.Cols("GroupScalingFactor").AllowEditing = True


			Grid_Returns.Cols("PertracName").Width = 200
			Grid_Returns.Cols("IndexName").Width = 200
			Grid_Returns.Cols("GroupIndexCode").Width = 200

			Grid_Returns.Cols("GroupBeta").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupAlpha").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupIndexE").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupBetaE").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupAlphaE").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupExpectedReturn").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupStdErr").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupScalingFactor").Format = "#,##0.00%"

			Grid_Returns.Cols("GroupLiquidity").Format = "#,##0"
			Grid_Returns.Cols("GroupHolding").Format = "#,##0"
			Grid_Returns.Cols("GroupNewHolding").Format = "#,##0"
			Grid_Returns.Cols("GroupPercent").Format = "#,##0.0%"
			Grid_Returns.Cols("GroupUpperBound").Format = "#,##0.00"
			Grid_Returns.Cols("GroupLowerBound").Format = "#,##0.00"
			Grid_Returns.Cols("GroupTradeSize").Format = "#,##0"

			Grid_Returns.Cols("GroupIndexE").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupBetaE").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupAlphaE").Style.BackColor = Color.Honeydew

			Grid_Returns.Cols("GroupSector").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupLiquidity").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupHolding").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupNewHolding").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupUpperBound").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupLowerBound").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupFloor").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupCap").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupTradeSize").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupScalingFactor").Style.BackColor = Color.Honeydew

			' Grid_Returns.Cols("GroupIndexCode").DataMap = MainForm.GetTblLookupCollection(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")
			Grid_Returns.Cols("GroupIndexCode").DataMap = MainForm.PertracLookupCollection
			' Grid_Returns.Cols("GroupSector").DataMap = MainForm.GetTblLookupCollection(RenaissanceStandardDatasets.tblGroupItems, "GroupSector", "GroupSector", "", True, True, True, "", "")

			If (Grid_Returns.Rows(0).Height > 0) Then
				Grid_Returns.Rows.DefaultSize = Grid_Returns.Rows(0).Height
			End If

			Try
				PertracGridCombo.Height = Grid_Returns.Rows.DefaultSize

				Try
					RemoveHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
					'RemoveHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus
				Catch ex As Exception
				End Try
				AddHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
				'AddHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus

				Grid_Returns.Cols("GroupIndexCode").Editor = PertracGridCombo
			Catch ex As Exception
			End Try

			' Sector Combo
			Try
				If Not (TypeOf Grid_Returns.Cols("GroupSector").Editor Is CustomC1Combo) Then
					SectorCombo.DropDownStyle = ComboBoxStyle.DropDown
					SectorCombo.Height = Grid_Returns.Rows.DefaultSize
					MainForm.SetTblGenericCombo(SectorCombo, RenaissanceStandardDatasets.tblGroupItems, "GroupSector", "GroupSector", "", True, True, True, "", "")

					AddHandler SectorCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
					AddHandler SectorCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
					AddHandler SectorCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
					AddHandler SectorCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

					'AddHandler SectorCombo.SelectedValueChanged, AddressOf Me.SectorComboChanged
					AddHandler SectorCombo.LostFocus, AddressOf SectorComboChanged
					' AddHandler SectorCombo.KeyPress, AddressOf Me.SectorComboChanged

					Grid_Returns.Cols("GroupSector").Editor = SectorCombo
				End If

			Catch ex As Exception

			End Try

			SetGrid_StandardReturnColumns()

		Catch ex As Exception
		End Try

		' 

		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
      Call GetFormData(Nothing)
		End If

		If Combo_BenckmarkIndex.Items.Count > 0 Then
			Combo_BenckmarkIndex.SelectedIndex = 0
		End If

		InPaint = False


	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
    MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
			Catch ex As Exception
			End Try

			Try
				RemoveHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
				'RemoveHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus
			Catch ex As Exception
			End Try

			Try
				' RemoveHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.FormControlChanged
			Catch ex As Exception
			End Try


			Try
				' Form Control Changed events

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_BenckmarkIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_BenckmarkIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_BenckmarkIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				'RemoveHandler PertracGridCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler PertracGridCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				'RemoveHandler PertracGridCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				'RemoveHandler PertracGridCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try


		End If

	End Sub

  Private Function FormUpdate_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' Motivation :
    ' Sometimes, when charting parameters may be quickly updated, one wants to defer
    ' the chart update until the update has finished.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InFormUpdate_Tick) OrElse (Not _InUse) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try

    Try
      InFormUpdate_Tick = True

      ' Update Form :

      ' Don't react to changes made in paint routines etc.
      If Not InPaint Then

        If (FormChanged) Then
          Call SetFormData()
        End If

        ' Find the correct data row, then show it...
        thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

        If (thisPosition >= 0) Then
          thisDataRow = Me.SortedRows(thisPosition)
        Else
          thisDataRow = Nothing
        End If

        Try
          Me.Cursor = Cursors.WaitCursor

          Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.Ascending, -1)
          Call GetFormData(thisDataRow)

        Catch ex As Exception
        Finally
          Me.Cursor = Cursors.Default
        End Try

      End If

      RVal = True

    Catch ex As Exception
      RVal = False
    Finally
      InFormUpdate_Tick = False
    End Try

    Return RVal

  End Function

#End Region

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint

		Try
			InPaint = True

			KnowledgeDateChanged = False
			SetButtonStatus_Flag = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
				RefreshForm = True
			End If

			' ****************************************************************
			' Check for changes relevant to this form
			' ****************************************************************


			' SetGroupCombo
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

				' Re-Set combo.
				Call SetGroupCombo()
			End If

			' Changes to the KnowledgeDate :-
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				SetButtonStatus_Flag = True
			End If

			' Changes to the tblUserPermissions table :-
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					Me.Close()
					Exit Sub
				End If

				SetButtonStatus_Flag = True

			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then
				RefreshForm = True
			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = True) Or KnowledgeDateChanged Then
				RefreshForm = True
			End If

			' Venice tblTransaction - Used for the Venice Holdings cache.
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) OrElse (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        If (tblVeniceHoldings IsNot Nothing) Then
          Try
            tblVeniceHoldings.Clear()
          Catch ex As Exception
          Finally
            tblVeniceHoldings = Nothing
          End Try
        End If
      End If

			' Check to see if the local AggregatedGroups table needs to refresh.
			Try
				Dim ThisChangeID As RenaissanceChangeID

				For Each ThisChangeID In e.TablesChanged
					If RenaissanceStandardDatasets.tblGroupsAggregated.ISChangeIDToNote(ThisChangeID) Then
						LoadedAggregatedGroupListID = 0
						Exit For
					ElseIf RenaissanceStandardDatasets.tblGroupsAggregated.IsChangeIDToTriggerUpdate(ThisChangeID) Then
						LoadedAggregatedGroupListID = 0
						Exit For
					End If
				Next
			Catch ex As Exception
			End Try

			' tblPertracCustomFields

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) Or KnowledgeDateChanged Then
				Dim ExistingSelectedFieldIDs() As Integer
				Dim FieldKeys() As Integer

				' Update Grid_CustomFields_Display_Dictionary for new or removed Custom fields

				If (Grid_CustomFields_Display_Dictionary.Count > 0) Then
					Dim FieldCounter As Integer

					ReDim FieldKeys(Grid_CustomFields_Display_Dictionary.Count - 1)
					ReDim ExistingSelectedFieldIDs(Grid_CustomFields_Display_Dictionary.Count - 1)

					Grid_CustomFields_Display_Dictionary.Keys.CopyTo(FieldKeys, 0)

					For FieldCounter = 0 To (FieldKeys.Length - 1)
						ExistingSelectedFieldIDs(FieldCounter) = (-1)

						If Grid_CustomFields_Display_Dictionary(FieldKeys(FieldCounter)) Then
							ExistingSelectedFieldIDs(FieldCounter) = FieldKeys(FieldCounter)
						End If
					Next

					Grid_CustomFields_Display_Dictionary.Clear()
					RootMenu.Items.Remove(CustomFieldsMenu)
					CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)

					For FieldCounter = 0 To (ExistingSelectedFieldIDs.Length - 1)
						If (ExistingSelectedFieldIDs(FieldCounter) >= 0) Then
							SetCustomMenuChecked(ExistingSelectedFieldIDs(FieldCounter).ToString)
						End If
					Next

				Else
					Grid_CustomFields_Display_Dictionary.Clear()
					RootMenu.Items.Remove(CustomFieldsMenu)
					CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)
				End If

			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFieldData) = True) Or KnowledgeDateChanged Then

				If (FormChanged = False) Then
					Dim ColCount As Integer

					For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
						If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then
							PaintCustomFieldColumn(Grid_Returns.Cols(ColCount).Name)

							Application.DoEvents()
						End If
					Next
				End If

			End If

			' MasterName
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername) = True) Then
        PertracGridCombo.MasternameCollection = MainForm.MasternameDictionary

        'MainForm.SetTblGenericCombo(PertracGridCombo, RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")
			End If

			' Performance (Pertrac)
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance) = True) OrElse _
             (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then
        Try
          ' Update charts.

          Dim ThisPertracID As Integer
          Dim ThisScalingFactor As Double
          Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
          Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
          Dim StatsDatePeriod As DealingPeriod

          ThisPertracID = Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode)
          ThisScalingFactor = CDbl(Grid_Returns.Item(Grid_Returns.Row, Col_GroupScalingFactor))

          StatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisPertracID)

          ' ThisSeries = Chart_VAMI.Series.Count - 1
          Set_LineChart(MainForm, StatsDatePeriod, ThisPertracID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), "")

          'ThisSeries = Chart_MonthlyReturns.Series.Count - 1

          Set_RollingReturnChart(MainForm, StatsDatePeriod, ThisPertracID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
          Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)) * 100, "")
        Catch ex As Exception
        End Try
      End If

			' ****************************************************************
			' Changes to the Main FORM table :-
			' ****************************************************************

			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
				RefreshForm = True

				' Re-Set Controls etc.
				Call SetSortedRows()

				' Move again to the correct item
				thisPosition = Get_Position(thisAuditID)

				' Validate current position.
				If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
					thisDataRow = Me.SortedRows(thisPosition)
				Else
					If (Me.SortedRows.GetLength(0) > 0) Then
						thisPosition = 0
						thisDataRow = Me.SortedRows(thisPosition)
						FormChanged = False
					Else
						thisDataRow = Nothing
					End If
				End If

				' Set SelectingCombo Index.

				If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
					Catch ex As Exception
					End Try
				ElseIf (thisPosition < 0) Then
					Try
						MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
					Catch ex As Exception
					End Try
				Else
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
					Catch ex As Exception
					End Try
				End If

			End If

		Catch ex As Exception
		Finally

			InPaint = OrgInPaint

		End Try

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) Then
      GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) Then
			SelectString = "GroupGroup='" & Combo_SelectGroup.SelectedValue.ToString & "'"
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub

#End Region

#Region " Menu Code"

	Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
		' ***************************************************************************************
		' Build the FieldSelect Menu.
		'
		' Add an item for each field in the tblGroupsAggregated table, when selected the item
		' will show or hide the associated field on the grid.
		' ***************************************************************************************

		Dim TransactionTable As DataTable = DSSelectedGroup.tblGroupsAggregated
		Dim ColumnNames(-1) As String
		Dim ColumnCount As Integer

		Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
		FieldsMenu.Name = "Menu_GridField"

		Dim newMenuItem As ToolStripMenuItem

		ReDim ColumnNames(TransactionTable.Columns.Count - 1)
		For ColumnCount = 0 To (TransactionTable.Columns.Count - 1)
			ColumnNames(ColumnCount) = TransactionTable.Columns(ColumnCount).ColumnName
			If (ColumnNames(ColumnCount).StartsWith("Group")) Then
				ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(5) & "."
			End If
		Next
		Array.Sort(ColumnNames)

		For ColumnCount = 0 To (ColumnNames.Length - 1)
			newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

			If (ColumnNames(ColumnCount).EndsWith(".")) Then
				newMenuItem.Name = "Menu_GridField_Group" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
			Else
				newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
			End If
		Next

		RootMenu.Items.Add(FieldsMenu)
		Return FieldsMenu

	End Function

	Private Function SetCustomFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
		' ***************************************************************************************
		' Build the FieldSelect Menu.
		'
		' Add an item for each field in the tblPertracCustomFields table, when selected the item
		' will show or hide the associated field on the grid.
		' ***************************************************************************************

		Dim FieldsTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
		Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
		Dim FieldsRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
		Dim FieldNames(-1) As String
		Dim RowCount As Integer

		FieldsTable = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0)
		SelectedRows = FieldsTable.Select("True", "FieldName")
		Grid_CustomFields_Display_Dictionary.Clear()

		Dim FieldsMenu As New ToolStripMenuItem("Custom &Fields")
		FieldsMenu.Name = "Menu_CustomFields"

		Dim newMenuItem As ToolStripMenuItem

		ReDim FieldNames(SelectedRows.Length - 1)
		For RowCount = 0 To (SelectedRows.Length - 1)
			FieldsRow = SelectedRows(RowCount)

			FieldNames(RowCount) = FieldsRow.FieldName

			newMenuItem = FieldsMenu.DropDownItems.Add(FieldsRow.FieldName, Nothing, AddressOf Me.ShowGridField)
			newMenuItem.Name = "Menu_CustomField_" & FieldsRow.FieldID.ToString

			Grid_CustomFields_Display_Dictionary.Add(FieldsRow.FieldID, False)
		Next

		RootMenu.Items.Add(FieldsMenu)
		Return FieldsMenu

	End Function

	Private Sub SetCustomMenuChecked(ByRef pFieldID As String)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FieldID As Integer

		If IsNumeric(pFieldID) = False Then
			Exit Sub
		End If

		FieldID = CInt(pFieldID)

		Try
			CType(CustomFieldsMenu.DropDownItems("Menu_CustomField_" & FieldID.ToString), ToolStripMenuItem).Checked = True
		Catch ex As Exception
		End Try
		Grid_CustomFields_Display_Dictionary(FieldID) = True

	End Sub

	Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		' Callback function for the Grid Fields Menu items.
		'
		' ***************************************************************************************

		Try
			If TypeOf (sender) Is ToolStripMenuItem Then
				Dim thisMenuItem As ToolStripMenuItem
				Dim FieldName As String

				thisMenuItem = CType(sender, ToolStripMenuItem)
				thisMenuItem.Checked = Not thisMenuItem.Checked
				FieldName = thisMenuItem.Name

				If (FieldName.StartsWith("Menu_CustomField_")) Then
					Dim FieldID As Integer

					FieldID = CInt(FieldName.Substring(17))

					If Grid_CustomFields_Display_Dictionary.ContainsKey(FieldID) Then
						Grid_CustomFields_Display_Dictionary.Item(FieldID) = thisMenuItem.Checked
					Else
						Grid_CustomFields_Display_Dictionary.Add(FieldID, thisMenuItem.Checked)
					End If

					Dim ThisColumnName As String
					ThisColumnName = "Custom_" & FieldID.ToString

					If (thisMenuItem.Checked = False) Then
						If Grid_Returns.Cols.Contains(ThisColumnName) Then
							Grid_Returns.Cols.Remove(ThisColumnName)
						End If
					Else
						CreateCustomFieldColumn(FieldID)
						PaintCustomFieldColumn(FieldID)
					End If

				Else
					If (FieldName.StartsWith("Menu_GridField_")) Then
						FieldName = FieldName.Substring(15)
					End If

					If (Grid_Returns.Cols.Contains(FieldName)) Then
						If (thisMenuItem.Checked) Then
							Grid_Returns.Cols(FieldName).Visible = True
						Else
							Grid_Returns.Cols(FieldName).Visible = False
						End If
					End If

				End If
			End If
		Catch ex As Exception

		End Try
	End Sub

	Sub cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' Acquire references to the owning control and item.
		Dim c As Control = GridContextMenuStrip.SourceControl
		Dim tsi As ToolStripDropDownItem = GridContextMenuStrip.OwnerItem

		' Clear the ContextMenuStrip control's 
		' Items collection.
		GridContextMenuStrip.Items.Clear()

		GridClickMouseCol = Grid_Returns.MouseCol

		' Check the source control first.
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
		Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
		Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
		Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex
    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex

		Dim IsCustomField As Boolean = False

		' Add custom item (Form)

		e.Cancel = False

		GridContextMenuStrip.Items.Add("Show Standard Return Columns", Nothing, AddressOf Menu_SetStandardReturnColumns)
		GridContextMenuStrip.Items.Add("Show Standard Optimiser Columns", Nothing, AddressOf Menu_SetStandardOptimiserColumns)

		GridContextMenuStrip.Items.Add(New ToolStripSeparator)

		If Grid_Returns.Cols(Grid_Returns.MouseCol).Name.StartsWith("Custom_") Then
			IsCustomField = True
		End If

		If (IsCustomField) Then
			If (Grid_Returns.MouseRow > 0) Then
				GridClickMouseRow = Grid_Returns.MouseRow
				GridContextMenuStrip.Items.Add("Delete Group Custom Field Item.", Nothing, AddressOf DeleteGroupCustomFieldData)
				GridContextMenuStrip.Items.Add("Copy Group Custom Field Item from Group Zero.", Nothing, AddressOf GetGroupZeroCustomFieldData)

				' Set Cancel to false. 
				' It is optimized to true based on empty entry.
				e.Cancel = False
			End If
		Else
			Select Case Grid_Returns.MouseCol
				Case Col_GroupSector
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow
						GridContextMenuStrip.Items.Add("Get Sector From Venice", Nothing, AddressOf GetVeniceSector)
						GridContextMenuStrip.Items.Add("Get Selected Sectors From Venice", Nothing, AddressOf GetSelectedVeniceSectors)
						GridContextMenuStrip.Items.Add("Get ALL Sectors From Venice", Nothing, AddressOf GetAllVeniceSectors)
						GridContextMenuStrip.Items.Add("Get Sector From Pertrac", Nothing, AddressOf GetPertracSector)
						GridContextMenuStrip.Items.Add("Get Selected Sectors From Pertrac", Nothing, AddressOf GetSelectedPertracSectors)
						GridContextMenuStrip.Items.Add("Get ALL Sectors From Pertrac", Nothing, AddressOf GetAllPertracSectors)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_GroupLiquidity
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow
						GridContextMenuStrip.Items.Add("Get Liquidity From Venice", Nothing, AddressOf GetVeniceLiquidity)
						GridContextMenuStrip.Items.Add("Get Selected Liquidities From Venice", Nothing, AddressOf GetSelectedVeniceLiquidities)
						GridContextMenuStrip.Items.Add("Get ALL Liquidities From Venice", Nothing, AddressOf GetAllVeniceLiquidities)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_GroupIndexCode
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow

						GridContextMenuStrip.Items.Add("Set Benchmark Index, This Row only.", Nothing, AddressOf Menu_SetBenchmarkSingleRow)
						GridContextMenuStrip.Items.Add("Set Benchmark Index, All Rows.", Nothing, AddressOf Menu_SetBenchmarkAllRows)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_Beta
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow

						GridContextMenuStrip.Items.Add("Set THIS Expected Beta = Index Beta", Nothing, AddressOf SetThisExpectedBeta)
						GridContextMenuStrip.Items.Add("Set ALL Expected Beta = Index Beta", Nothing, AddressOf SetAllExpectedBeta)

						e.Cancel = False
					End If

				Case Col_Alpha
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow

						GridContextMenuStrip.Items.Add("Set THIS Expected Alpha = Index Alpha", Nothing, AddressOf SetThisExpectedAlpha)
						GridContextMenuStrip.Items.Add("Set ALL Expected Alpha = Index Alpha", Nothing, AddressOf SetAllExpectedAlpha)

						e.Cancel = False
					End If

				Case Col_GroupHolding
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow

						GridContextMenuStrip.Items.Add("Set THIS 'Trade' to Zero", Nothing, AddressOf SetThisTradeZero)
						GridContextMenuStrip.Items.Add("Set ALL 'Trade' to Zero", Nothing, AddressOf SetAllTradeZero)

						GridContextMenuStrip.Items.Add(New ToolStripSeparator)
						GridContextMenuStrip.Items.Add("Value Date :")
						GridContextMenuStrip.Items.Add(GetVeniceHoldingsDatePicker)
						GridContextMenuStrip.Items.Add(GetVeniceHoldingsLookthroughCheckbox)

						Dim thisMenu As ToolStripMenuItem
						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get THIS 'Holding' from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.SingleRow, False)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get Selected 'Holding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.MultipleRows, False)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get ALL 'Holding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.AllRows, False)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = Nothing
						e.Cancel = False
					End If

				Case Col_GroupNewHolding
					If (Grid_Returns.MouseRow > 0) Then
						GridClickMouseRow = Grid_Returns.MouseRow

						GridContextMenuStrip.Items.Add("Set THIS 'Trade' to Zero", Nothing, AddressOf SetThisTradeZero)
						GridContextMenuStrip.Items.Add("Set ALL 'Trade' to Zero", Nothing, AddressOf SetAllTradeZero)

						GridContextMenuStrip.Items.Add(New ToolStripSeparator)
						GridContextMenuStrip.Items.Add("Value Date :")
						GridContextMenuStrip.Items.Add(GetVeniceHoldingsDatePicker)
						GridContextMenuStrip.Items.Add(GetVeniceHoldingsLookthroughCheckbox)

						Dim thisMenu As ToolStripMenuItem
						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get THIS 'NewHolding' from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.SingleRow, True)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get Selected 'NewHolding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.MultipleRows, True)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get ALL 'NewHolding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.AllRows, True)
						GridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = Nothing
						e.Cancel = False
					End If

        Case Col_GroupExpectedReturn

          Dim ThisMenuItem As ToolStripMenuItem
          GridClickMouseRow = Grid_Returns.MouseRow

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 1Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "1Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 1Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "1Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 3Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "3Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 3Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "3Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 5Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "5Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 5Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "5Yr"
          GridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to ITD Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "ITD"
          GridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to ITD Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "ITD"
          GridContextMenuStrip.Items.Add(ThisMenuItem)

        Case Else
          '	e.Cancel = True

      End Select
		End If

	End Sub

  Private Sub SetTHISExpectedReturn(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
    Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim ThisPertracID As UInteger
    Dim BackFillPertracID As UInteger
    Dim ThisSimpleStats As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass
    Dim ThisTag As String = ""
    Dim TargetReturn As Double = 0
    Dim ThisIndexExpectedBeta As Double
    Dim ThisIndexExpectedReturn As Double

    If (Me.Grid_Returns.Rows.Count > 1) AndAlso (GridClickMouseRow > 0) Then

      Try

        ThisPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(GridClickMouseRow, Col_GroupPertracCode)), 0))
        BackFillPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(GridClickMouseRow, Col_GroupIndexCode)), 0))

        ThisSimpleStats = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(ThisPertracID, BackFillPertracID), MainForm.StatFunctions.CombinedStatsID(ThisPertracID, BackFillPertracID), False, 0.0#, 1.0#)

        Try
          If CType(sender, ToolStripMenuItem).Tag IsNot Nothing Then
            ThisTag = CType(sender, ToolStripMenuItem).Tag.ToString.ToUpper
          Else
            ThisTag = ""
          End If
        Catch ex As Exception
        End Try

        Select Case ThisTag

          Case "1YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M12

          Case "3YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M36

          Case "5YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M60

          Case "ITD"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

          Case Else

            TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

        End Select

        ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(GridClickMouseRow, Col_GroupIndexE))
        ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(GridClickMouseRow, Col_GroupBetaE))

        Grid_Returns.Item(GridClickMouseRow, Col_GroupAlphaE) = TargetReturn - (ThisIndexExpectedBeta * ThisIndexExpectedReturn)

      Catch ex As Exception
      End Try

    End If

  End Sub

  Private Sub SetALLExpectedReturn(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
    Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim ThisPertracID As UInteger
    Dim BackFillPertracID As UInteger
    Dim ThisSimpleStats As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass
    Dim ThisTag As String = ""
    Dim TargetReturn As Double = 0
    Dim ThisIndexExpectedBeta As Double
    Dim ThisIndexExpectedReturn As Double

    If (Me.Grid_Returns.Rows.Count > 1) Then

      Try

        If (Me.Grid_Returns.Rows.Count > 1) Then
          Dim ThisGridRow As Integer

          For ThisGridRow = 1 To (Me.Grid_Returns.Rows.Count - 1)

            ThisPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(ThisGridRow, Col_GroupPertracCode)), 0))
            BackFillPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(ThisGridRow, Col_GroupIndexCode)), 0))

            ThisSimpleStats = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(ThisPertracID, BackFillPertracID), MainForm.StatFunctions.CombinedStatsID(ThisPertracID, BackFillPertracID), False, 0.0#, 1.0#)

            Try
              If CType(sender, ToolStripMenuItem).Tag IsNot Nothing Then
                ThisTag = CType(sender, ToolStripMenuItem).Tag.ToString.ToUpper
              Else
                ThisTag = ""
              End If
            Catch ex As Exception
            End Try

            Select Case ThisTag

              Case "1YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M12

              Case "3YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M36

              Case "5YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M60

              Case "ITD"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

              Case Else

                TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

            End Select

            ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(ThisGridRow, Col_GroupIndexE))
            ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(ThisGridRow, Col_GroupBetaE))

            Grid_Returns.Item(ThisGridRow, Col_GroupAlphaE) = TargetReturn - (ThisIndexExpectedBeta * ThisIndexExpectedReturn)

          Next

        End If

      Catch ex As Exception
      End Try

    End If
  End Sub

	Private Function GetNewCmsDateTimePicker() As System.Windows.Forms.DateTimePicker
		Dim RVal As New System.Windows.Forms.DateTimePicker

		Try
			RVal.Size = New System.Drawing.Size(200, 20)

			RVal.Value = GetVeniceHoldingsValueDate
			AddHandler RVal.ValueChanged, AddressOf CmsDateTimePicker_ValueChanged

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Private Sub CmsDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		Try
			GetVeniceHoldingsValueDate = CType(sender, System.Windows.Forms.DateTimePicker).Value

			' Clear holdings cache if 'ValueDate' changes

			If (tblVeniceHoldings IsNot Nothing) Then
				tblVeniceHoldings.Clear()
			End If
		Catch ex As Exception
		Finally
			tblVeniceHoldings = Nothing
		End Try

	End Sub

	Private Function GetNewCmsCheckbox() As System.Windows.Forms.CheckBox
		' ************************************************************************
		' Create the 'Lookthrough' Checkbox used on the grid context menu.
		'
		' ************************************************************************

		Dim RVal As New System.Windows.Forms.CheckBox

		Try
			RVal.Text = "Lookthrough holdings"
			RVal.Size = New System.Drawing.Size(200, 20)
			RVal.CheckAlign = ContentAlignment.MiddleLeft
			RVal.AutoCheck = True
			RVal.Checked = GetVeniceHoldingsLookthroughCheckboxChecked
			RVal.BackColor = Color.Transparent
			AddHandler RVal.CheckedChanged, AddressOf CmsCheckbox_CheckedChanged

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Private Sub CmsCheckbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ************************************************************************
		' CheckedChanged callback for the Get Holdings 'Lookthrough' checkbox.
		'
		' ************************************************************************

		Try
			GetVeniceHoldingsLookthroughCheckboxChecked = CType(sender, System.Windows.Forms.CheckBox).Checked

			' Clear holdings cache if 'Lookthrough' changes

			If (tblVeniceHoldings IsNot Nothing) Then
				tblVeniceHoldings.Clear()
			End If
		Catch ex As Exception
		Finally
			tblVeniceHoldings = Nothing
		End Try

	End Sub

	Private Sub SetThisTradeZero(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (GridClickMouseRow > 0) Then
			Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex

			Grid_Returns.Item(GridClickMouseRow, Col_GroupTrade) = 0

		End If
	End Sub

	Private Sub SetAllTradeZero(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Try

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
				Dim RowCount As Integer

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_GroupTrade) = 0
				Next

			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub PopulateGetHoldingMenu(ByVal TargetMenuItem As ToolStripMenuItem, ByVal HoldingSelection As SelectionEnum, ByVal NewHolding As Boolean)
		' *****************************************************************************************
		' Populate this menu with possible Venice Funds.
		'
		' *****************************************************************************************

		Dim thisFundsDS As RenaissanceDataClass.DSFund
		Dim thisFundView As DataView

		Dim FundCounter As Integer
		Dim ThisFund As DSFund.tblFundRow

		If TargetMenuItem.HasDropDownItems Then
			TargetMenuItem.DropDownItems.Clear()
		End If

		thisFundsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund)
		thisFundView = New DataView(thisFundsDS.tblFund, "True", "FundName", DataViewRowState.CurrentRows)

		' Get appropriate callback
		Dim MenuCallback As System.EventHandler

		If (NewHolding) Then

			Select Case HoldingSelection
				Case SelectionEnum.MultipleRows
					MenuCallback = AddressOf GetSelectionNewHoldingFromVenice

				Case SelectionEnum.AllRows
					MenuCallback = AddressOf GetAllNewHoldingFromVenice

				Case Else
					MenuCallback = AddressOf GetSingleNewHoldingFromVenice

			End Select

		Else

			Select Case HoldingSelection
				Case SelectionEnum.MultipleRows
					MenuCallback = AddressOf GetSelectionHoldingFromVenice

				Case SelectionEnum.AllRows
					MenuCallback = AddressOf GetAllHoldingFromVenice

				Case Else
					MenuCallback = AddressOf GetSingleHoldingFromVenice

			End Select

		End If


		For FundCounter = 0 To (thisFundView.Count - 1)
			ThisFund = thisFundView.Item(FundCounter).Row

			TargetMenuItem.DropDownItems.Add(ThisFund.FundName, Nothing, MenuCallback).Tag = ThisFund.FundID

		Next

	End Sub

	Private Sub GetSingleHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' OK the Line to fetch is 'InstrumentsGridClickMouseRow'

		Try
			If (GridClickMouseRow <= 0) OrElse (GridClickMouseRow >= Me.Grid_Returns.Rows.Count) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			Application.DoEvents()

			FundID = CInt(CType(sender, ToolStripItem).Tag)
			PertracID = CInt(Grid_Returns.Item(GridClickMouseRow, Col_GroupPertracCode))

			If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
				tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
				tblVeniceHoldings_FundID = FundID
			End If

			HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Grid_Returns.Item(GridClickMouseRow, Col_GroupHolding) = HoldingValue
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectionHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Not Grid_Returns.Selection.IsValid) Then
			Exit Sub
		End If

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = Grid_Returns.Selection.r1 To Grid_Returns.Selection.r2
				Application.DoEvents()

				If (InstrumentCounter > 0) Then
					Try
						PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

						If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
							tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
							tblVeniceHoldings_FundID = FundID
						End If

						HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

					Catch ex As Exception
						HoldingValue = 0
					End Try

					Grid_Returns.Item(InstrumentCounter, Col_GroupHolding) = HoldingValue
				End If
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

	End Sub

	Private Sub GetAllHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Try
					Application.DoEvents()

					PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

					If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
						tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
						tblVeniceHoldings_FundID = FundID
					End If

					HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

				Catch ex As Exception
					HoldingValue = 0
				End Try

				Grid_Returns.Item(InstrumentCounter, Col_GroupHolding) = HoldingValue
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

	End Sub

	Private Sub GetSingleNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' OK the Line to fetch is 'InstrumentsGridClickMouseRow'

		Try
			If (GridClickMouseRow <= 0) OrElse (GridClickMouseRow >= Me.Grid_Returns.Rows.Count) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			Application.DoEvents()

			FundID = CInt(CType(sender, ToolStripItem).Tag)
			PertracID = CInt(Grid_Returns.Item(GridClickMouseRow, Col_GroupPertracCode))

			If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
				tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
				tblVeniceHoldings_FundID = FundID
			End If

			HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Grid_Returns.Item(GridClickMouseRow, Col_GroupNewHolding) = HoldingValue
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectionNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Not Grid_Returns.Selection.IsValid) Then
			Exit Sub
		End If

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = Grid_Returns.Selection.r1 To Grid_Returns.Selection.r2
				Application.DoEvents()

				If (InstrumentCounter > 0) Then
					Try
						PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

						If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
							tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
							tblVeniceHoldings_FundID = FundID
						End If

						HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

					Catch ex As Exception
						HoldingValue = 0
					End Try

					Grid_Returns.Item(InstrumentCounter, Col_GroupNewHolding) = HoldingValue
				End If
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

	End Sub

	Private Sub GetAllNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Try
					Application.DoEvents()

					PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

					If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
						tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
						tblVeniceHoldings_FundID = FundID
					End If

					HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

				Catch ex As Exception
					HoldingValue = 0
				End Try

				Grid_Returns.Item(InstrumentCounter, Col_GroupNewHolding) = HoldingValue
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, True, "", "All Groups")		' 

	End Sub

	Private Sub SetBenchmarkIndexCombo()

		Call MainForm.SetMasterNameCombo( _
		 Me.Combo_BenckmarkIndex, _
		 "", False)		' 

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSGroupList.tblGroupListRow)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    If (ThisRow Is Nothing) OrElse (FormIsValid = False) OrElse (Me.InUse = False) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
      ' Bad / New Datarow - Clear Form.

      thisAuditID = (-1)

      Me.editAuditID.Text = ""
      Me.Label_GroupStartDate.Text = ""
      Me.Label_GroupEndDate.Text = ""

      Me.Grid_Returns.Rows.Count = 1

      Me.btnCancel.Enabled = False
      Me.THIS_FORM_SelectingCombo.Enabled = True

    Else
      Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
      Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
      Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex

      Dim ThisHolding As Double
      Dim ThisTrade As Double
      Dim ThisNewHolding As Double

      ' Populate Form with given data.
      Try

        Dim GroupListID As Integer
        Dim RowCount As Integer
        Dim ColCount As Integer
        Dim ColOrdinals(Grid_Returns.Cols.Count - 1) As Integer
        Dim SelectedRows() As DSGroupsAggregated.tblGroupsAggregatedRow

        Dim ThisGroupRow As DSGroupsAggregated.tblGroupsAggregatedRow

        ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
        OrgInpaint = InPaint
        InPaint = True

        thisAuditID = thisDataRow.AuditID

        Me.editAuditID.Text = thisDataRow.AuditID.ToString

        GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
        Me.Label_GroupStartDate.Text = ""
        Me.Label_GroupEndDate.Text = ""

        If Not (LoadedAggregatedGroupListID = GroupListID) Then
          LoadSelectedAggregatedGroup(GroupListID, DSSelectedGroup.tblGroupsAggregated)
        End If
        SelectedRows = DSSelectedGroup.tblGroupsAggregated.Select("GroupListID=" & GroupListID.ToString, "PertracName")

        Grid_Returns.Rows.Count = (1 + SelectedRows.Length)

        ' Get Ordinals
        ColOrdinals(0) = (-1)
        ColOrdinals(1) = (-1)
        For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
          ColOrdinals(ColCount) = (-1)
          Try
            If DSSelectedGroup.tblGroupsAggregated.Columns.Contains(Grid_Returns.Cols(ColCount).Name) Then
              ColOrdinals(ColCount) = DSSelectedGroup.tblGroupsAggregated.Columns.IndexOf(Grid_Returns.Cols(ColCount).Name)
            End If
          Catch ex As Exception
          End Try
        Next

        ' Load pertrac Data

        Try
          If (SelectedRows.Length > 0) Then
            Dim SelectedPertracIDs(SelectedRows.Length - 1) As Integer

            MainForm.GenoaStatusLabel.Text = "Loading Pertrac Returns"
            MainForm.GenoaStatusStrip.Refresh()
            Application.DoEvents()

            For RowCount = 1 To (SelectedRows.Length)
              ThisGroupRow = SelectedRows(RowCount - 1)

              SelectedPertracIDs(RowCount - 1) = ThisGroupRow.GroupPertracCode
            Next

            MainForm.PertracData.LoadPertracTables(SelectedPertracIDs)

          End If
        Catch ex As Exception
          Call MainForm.LogError(Me.Name & ", GetFormData", LOG_LEVELS.Error, ex.Message, "Error loading Pertrac returns.", ex.StackTrace, True)
        Finally
          MainForm.GenoaStatusLabel.Text = ""
          MainForm.GenoaStatusStrip.Refresh()
          Application.DoEvents()
        End Try

        ' Show Data

        For RowCount = 1 To (SelectedRows.Length)
          ThisGroupRow = SelectedRows(RowCount - 1)

          If (StatusLabel_GroupReturns.Visible) Then
            StatusLabel_GroupReturns.Text = "Working on " & ThisGroupRow.PertracName
            StatusStrip_GroupReturns.Refresh()
            ' Application.DoEvents()
          Else
            MainForm.GenoaStatusLabel.Text = "Working on " & ThisGroupRow.PertracName
            MainForm.GenoaStatusStrip.Refresh()
            Application.DoEvents()
          End If

          If (RowCount = 1) Then
            If IsDate(ThisGroupRow.GroupDateFrom) Then
              Me.Label_GroupStartDate.Text = ThisGroupRow.GroupDateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
            End If
            If IsDate(ThisGroupRow.GroupDateTo) Then
              Me.Label_GroupEndDate.Text = ThisGroupRow.GroupDateTo.ToString(DISPLAYMEMBER_DATEFORMAT)
            End If
          End If

          Grid_Returns.Item(RowCount, 0) = "" ' Col_Updated
          Grid_Returns.Item(RowCount, 1) = "" ' Col_CustomUpdated
          For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
            If (ColOrdinals(ColCount) >= 0) Then
              Grid_Returns.Item(RowCount, ColCount) = ThisGroupRow(ColOrdinals(ColCount))
            Else
              Grid_Returns.Item(RowCount, ColCount) = ""
            End If
          Next

          ' Update Trade Column to reflect Holding and NewHolding

          ThisHolding = CDbl(Grid_Returns.Item(RowCount, Col_GroupHolding))
          ThisTrade = CDbl(Grid_Returns.Item(RowCount, Col_GroupTrade))
          ThisNewHolding = CDbl(Grid_Returns.Item(RowCount, Col_GroupNewHolding))

          If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
            ' New Holding has changed

            Grid_Returns.Item(RowCount, Col_GroupTrade) = Math.Round(ThisNewHolding - ThisHolding, GENOA_HoldingPrecision)

          End If

          ' Set Alpha And Beta

          GetAlphaBetaAndStdError(ThisGroupRow.GroupPertracCode, ThisGroupRow.GroupIndexCode, RowCount)

        Next

        ' Paint Custom Fields

        For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
          If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then
            PaintCustomFieldColumn(Grid_Returns.Cols(ColCount).Name)
          End If
        Next

        ' Set Buttons

        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception
        Grid_Returns.Rows.Count = 1

        Call MainForm.LogError(Me.Name & ", GetFormData", LOG_LEVELS.Error, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Call GetFormData(Nothing)

      Finally

        ' Restore Grid Sort order ?
        Try
          If (Grid_Returns.SortColumn IsNot Nothing) Then
            Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.UseColSort, Grid_Returns.SortColumn.SafeIndex)
          End If
        Catch ex As Exception
        End Try

        Call Update_Group_Percentages(True, False)

        StatusLabel_GroupReturns.Text = ""
        If (StatusLabel_GroupReturns.Visible = False) Then
          MainForm.GenoaStatusLabel.Text = ""
          MainForm.GenoaStatusStrip.Refresh()
        End If

        ' Allow Field events to trigger before 'InPaint' Is re-set. 
        ' (Should) Prevent Validation errors during Form Draw.
        Application.DoEvents()

        ' Restore 'Paint' flag.
        InPaint = OrgInpaint

      End Try

    End If


    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

	Private Sub LoadSelectedAggregatedGroup(ByVal GroupListID As Integer, ByRef tblGroupsAggregated As DSGroupsAggregated.tblGroupsAggregatedDataTable)
		' *************************************************************
		'
		' *************************************************************

		Try
			' Exit if Table is Null.

			If (tblGroupsAggregated Is Nothing) Then
				Exit Sub
			End If

			' Clear existing Rows

			If (tblGroupsAggregated.Rows.Count > 0) Then
				tblGroupsAggregated.Rows.Clear()
			End If

			' OK, Load data.
			Dim SelectCommand As New SqlCommand

			' Set Select Command
			SelectCommand.CommandText = "[adp_tblGroupsAggregated_SelectCommand]"
			SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

			Try
				SelectCommand.Connection = myConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int)).Value = GroupListID
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

				SyncLock SelectCommand.Connection
					tblGroupsAggregated.Load(SelectCommand.ExecuteReader)
				End SyncLock

				LoadedAggregatedGroupListID = GroupListID

			Catch ex As Exception
				If (tblGroupsAggregated.Rows.Count > 0) Then
					tblGroupsAggregated.Rows.Clear()
				End If

				LoadedAggregatedGroupListID = 0
			Finally
				SelectCommand.Connection = Nothing
			End Try

		Catch ex As Exception
			If (tblGroupsAggregated.Rows.Count > 0) Then
				tblGroupsAggregated.Rows.Clear()
			End If

			LoadedAggregatedGroupListID = 0
		End Try
	End Sub

	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True, Optional ByVal pDiscardChanges As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False
    Dim UpdateDetailsGroupItems As String = ""
    Dim UpdateDetailsDataItems As String = ""

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And (Me.HasInsertPermission = False) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				If (pDiscardChanges) Then
					FormChanged = False
				End If

				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim UpdateRows(0) As DataRow

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' *************************************************************
		' Clear focus from the Grid / Set selected cell to origin
		' *************************************************************

		Grid_Returns.FinishEditing()
		Grid_Returns.Select(0, 0, 0, 0, False)

		' *************************************************************
		' Process Update(s)
		' *************************************************************

		Dim ItemsUpdated As Boolean = False
		Dim ItemsDataUpdated As Boolean = False
		Dim CustomFieldDataUpdated As Boolean = False

		Try
			InPaint = True

			' *************************************************************
			' Static fields.
			'
			' Work through the DataGrid, Checking and updating those records
			' that appear to have changed.
			' *************************************************************

			Dim GroupItemsAdaptor As SqlDataAdapter = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItems.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItems.TableName)
			Dim GroupItemsDataAdaptor As SqlDataAdapter = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItemData.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItemData.TableName)

			Dim ItemsDS As DSGroupItems = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)
			Dim ItemsDataDS As DSGroupItemData = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItemData)
			Dim ItemsTbl As DSGroupItems.tblGroupItemsDataTable = Nothing
			Dim ItemsDataTbl As DSGroupItemData.tblGroupItemDataDataTable = Nothing
			Dim SelectedItem() As DSGroupItems.tblGroupItemsRow
			Dim SelectedItemData() As DSGroupItemData.tblGroupItemDataRow
			Dim GridRow As Integer


			If (ItemsDS IsNot Nothing) Then
				ItemsTbl = ItemsDS.tblGroupItems
			End If
			If (ItemsDataDS IsNot Nothing) Then
				ItemsDataTbl = ItemsDataDS.tblGroupItemData
			End If

			If (ItemsTbl IsNot Nothing) AndAlso (ItemsDataTbl IsNot Nothing) AndAlso (GroupItemsAdaptor IsNot Nothing) AndAlso (GroupItemsDataAdaptor IsNot Nothing) Then
				Dim Col_GroupListID As Integer = Grid_Returns.Cols("GroupListID").SafeIndex
				Dim Col_GroupItemID As Integer = Grid_Returns.Cols("GroupItemID").SafeIndex

				Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
				Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
				Dim Col_GroupAlpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
				Dim Col_GroupBeta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
				Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
				Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
				Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
				Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
				Dim Col_GroupStdErr As Integer = Grid_Returns.Cols("GroupStdErr").SafeIndex
				Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
				Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

				Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex
				Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
				Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
				Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
				Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
				Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
				Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
				Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
				Dim Col_GroupTradeSize As Integer = Grid_Returns.Cols("GroupTradeSize").SafeIndex

				Dim ThisListID As Integer
				Dim ThisItemID As Integer
				Dim ThisPertracID As Integer
				Dim ThisIndexCode As Integer
				Dim ThisSector As String

				Dim ThisItemAlpha As Double
				Dim ThisItemBeta As Double
				Dim ThisItemIndexE As Double
				Dim ThisItemBetaE As Double
				Dim ThisItemAlphaE As Double
				Dim ThisExpectedReturn As Double
				Dim ThisStdErr As Double
				Dim ThisScalingFactor As Double

				SyncLock ItemsTbl
					SyncLock ItemsDataTbl

						For GridRow = 1 To (Grid_Returns.Rows.Count - 1)
							Try
								ThisListID = CInt(Grid_Returns.Item(GridRow, Col_GroupListID))
								ThisItemID = CInt(Grid_Returns.Item(GridRow, Col_GroupItemID))
								ThisPertracID = CInt(Grid_Returns.Item(GridRow, Col_GroupPertracCode))

								' Update tables table

								SelectedItem = ItemsTbl.Select("GroupItemID=" & ThisItemID.ToString)
								If (SelectedItem.Length > 0) Then
									ThisSector = CStr(Grid_Returns.Item(GridRow, Col_GroupSector))
									ThisIndexCode = CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode))

									' 1st, check the GroupItems table

									If (SelectedItem(0).GroupIndexCode <> ThisIndexCode) OrElse (SelectedItem(0).GroupSector <> ThisSector) Then
										SelectedItem(0).GroupIndexCode = ThisIndexCode
										SelectedItem(0).GroupSector = ThisSector
										GroupItemsAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
										GroupItemsAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                    UpdateDetailsGroupItems &= SelectedItem(0).GroupItemID.ToString & ","
                    Call MainForm.AdaptorUpdate(Me.Name, GroupItemsAdaptor, New DataRow() {SelectedItem(0)})
										ItemsUpdated = True
									End If

									' 2nd, check the GroupItemsData table

									ThisItemAlpha = CDbl(Grid_Returns.Item(GridRow, Col_GroupAlpha))
									ThisItemBeta = CDbl(Grid_Returns.Item(GridRow, Col_GroupBeta))
									ThisItemIndexE = CDbl(Grid_Returns.Item(GridRow, Col_GroupIndexE))
									ThisItemBetaE = CDbl(Grid_Returns.Item(GridRow, Col_GroupBetaE))
									ThisItemAlphaE = CDbl(Grid_Returns.Item(GridRow, Col_GroupAlphaE))
									ThisExpectedReturn = CDbl(Grid_Returns.Item(GridRow, Col_GroupExpectedReturn))
									ThisStdErr = CDbl(Grid_Returns.Item(GridRow, Col_GroupStdErr))
									ThisScalingFactor = CDbl(Grid_Returns.Item(GridRow, Col_GroupScalingFactor))

									SelectedItemData = ItemsDataTbl.Select("GroupItemID=" & ThisItemID.ToString)

									If (SelectedItemData.Length > 0) Then

										If Not (SelectedItemData(0).GroupPertracCode = ThisPertracID) Then
											SelectedItemData(0).GroupPertracCode = ThisPertracID
										End If

										If Not (SelectedItemData(0).GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))) Then
											SelectedItemData(0).GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))
										End If

										If Not (SelectedItemData(0).GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))) Then
											SelectedItemData(0).GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))
										End If

										If Not (SelectedItemData(0).GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))) Then
											SelectedItemData(0).GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))
										End If

										If Not (SelectedItemData(0).GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))) Then
											SelectedItemData(0).GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))
										End If

										If Not (SelectedItemData(0).GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))) Then
											SelectedItemData(0).GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))
										End If

										If Not (SelectedItemData(0).GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))) Then
											SelectedItemData(0).GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))
										End If

										If Not (SelectedItemData(0).GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))) Then
											SelectedItemData(0).GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))
										End If

										If Not (SelectedItemData(0).GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))) Then
											SelectedItemData(0).GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))
										End If

										If Not (SelectedItemData(0).GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))) Then
											SelectedItemData(0).GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))
										End If

										If Not (SelectedItemData(0).GroupAlpha = ThisItemAlpha) Then
											SelectedItemData(0).GroupAlpha = ThisItemAlpha
										End If

										If Not (SelectedItemData(0).GroupBeta = ThisItemBeta) Then
											SelectedItemData(0).GroupBeta = ThisItemBeta
										End If

										If Not (SelectedItemData(0).GroupIndexE = ThisItemIndexE) Then
											SelectedItemData(0).GroupIndexE = ThisItemIndexE
										End If

										If Not (SelectedItemData(0).GroupBetaE = ThisItemBetaE) Then
											SelectedItemData(0).GroupBetaE = ThisItemBetaE
										End If

										If Not (SelectedItemData(0).GroupAlphaE = ThisItemAlphaE) Then
											SelectedItemData(0).GroupAlphaE = ThisItemAlphaE
										End If

										If Not (SelectedItemData(0).GroupExpectedReturn = ThisExpectedReturn) Then
											SelectedItemData(0).GroupExpectedReturn = ThisExpectedReturn
										End If

										If Not (SelectedItemData(0).GroupStdErr = ThisStdErr) Then
											SelectedItemData(0).GroupStdErr = ThisStdErr
										End If

										If Not (SelectedItemData(0).GroupScalingFactor = ThisScalingFactor) Then
											SelectedItemData(0).GroupScalingFactor = ThisScalingFactor
										End If

										If (SelectedItemData(0).RowState And DataRowState.Modified) = DataRowState.Modified Then
											GroupItemsDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
											GroupItemsDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

											Call MainForm.AdaptorUpdate(Me.Name, GroupItemsDataAdaptor, New DataRow() {SelectedItemData(0)})
                      UpdateDetailsDataItems &= SelectedItem(0).GroupItemID.ToString & ","

											ItemsDataUpdated = True
										End If

									Else
										' Add new Data Item row.

										Dim NewDataItemRow As DSGroupItemData.tblGroupItemDataRow

										NewDataItemRow = ItemsDataTbl.NewtblGroupItemDataRow

										NewDataItemRow.GroupItemID = ThisItemID
										NewDataItemRow.GroupPertracCode = ThisPertracID

										NewDataItemRow.GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))
										NewDataItemRow.GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))
										NewDataItemRow.GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))
										NewDataItemRow.GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))
										NewDataItemRow.GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))
										NewDataItemRow.GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))
										NewDataItemRow.GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))
										NewDataItemRow.GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))
										NewDataItemRow.GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))

										NewDataItemRow.GroupAlpha = ThisItemAlpha
										NewDataItemRow.GroupBeta = ThisItemBeta
										NewDataItemRow.GroupIndexE = ThisItemIndexE
										NewDataItemRow.GroupBetaE = ThisItemBetaE
										NewDataItemRow.GroupAlphaE = ThisItemAlphaE
										NewDataItemRow.GroupExpectedReturn = ThisExpectedReturn
										NewDataItemRow.GroupStdErr = ThisStdErr
										NewDataItemRow.GroupScalingFactor = ThisScalingFactor

										ItemsDataTbl.Rows.Add(NewDataItemRow)

										GroupItemsDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
										GroupItemsDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

										Call MainForm.AdaptorUpdate(Me.Name, GroupItemsDataAdaptor, New DataRow() {NewDataItemRow})

                    UpdateDetailsDataItems &= ThisItemID.ToString & ","

										ItemsDataUpdated = True

									End If

								Else
									' Item Does not exist...
								End If

							Catch ex As Exception
								ErrMessage = ex.Message
								ErrFlag = True
								ErrStack = ex.StackTrace

								Exit For
							End Try
						Next
					End SyncLock
				End SyncLock

			End If

		Catch ex As Exception
			ErrMessage = ex.Message
			ErrFlag = True
			ErrStack = ex.StackTrace
		Finally
			InPaint = False
		End Try


		' *************************************************************
		' Custom fields.
		'
		' Work through the DataGrid, Checking and updating those records
		' that appear to have changed.
		' *************************************************************
		Dim GroupListID As Integer
		Dim CustomFieldDataUpdateString As String = ""

		If (ErrFlag = False) Then
			Dim ColCount As Integer
			Dim RowCount As Integer
			Dim ColumnName As String
			Dim CustomFieldID As Integer
			Dim Col_Is_Custom As Boolean
			Dim PertracID As Integer

			Dim Changed_Ordinal As Integer = Grid_Returns.Cols.IndexOf("Col_CustomUpdated")
			Dim ID_Ordinal As Integer = Grid_Returns.Cols.IndexOf("GroupPertracCode")

			Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
			Dim ThisConnection As SqlConnection = Nothing
			Dim AdpCustomData As New SqlDataAdapter
			Dim DSCustomData As RenaissanceDataClass.DSPertracCustomFieldData
			Dim tblCustomData As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable
			Dim ThisRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow
			Dim ThisCustomFieldDataUpdated As Boolean = False

			Dim ThisGridItem As Object

			Try
				ThisConnection = MainForm.GetGenoaConnection
				MainForm.MainAdaptorHandler.Set_AdaptorCommands(ThisConnection, AdpCustomData, RenaissanceStandardDatasets.tblPertracCustomFieldData.TableName)
				AdpCustomData.InsertCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
				AdpCustomData.UpdateCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

				If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
					GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
				Else
					GroupListID = (0)
				End If

				For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
					If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then

						ColumnName = Grid_Returns.Cols(ColCount).Name
						CustomFieldID = (-1)
						Col_Is_Custom = False
						thisCustomFieldDefinition = Nothing

						If ColumnName.StartsWith("Custom_") AndAlso (IsNumeric(ColumnName.Substring(7))) Then
							CustomFieldID = CInt(ColumnName.Substring(7))
							thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

							' Only allow the update of 'Custom' fields
							If (thisCustomFieldDefinition IsNot Nothing) Then
								Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
									Case PertracCustomFieldTypes.CustomBoolean, PertracCustomFieldTypes.CustomDate, PertracCustomFieldTypes.CustomNumeric, PertracCustomFieldTypes.CustomString
										Col_Is_Custom = True

									Case Else
										Col_Is_Custom = False

								End Select
							End If
						End If

						If (Col_Is_Custom) Then
							' ********************************************************
							' When Saving Custom field data, rather than get the existing data and 
							' update the datarows as appropriate, I have chosen to simply create
							' a new table and add all changes as new rows. The underlying INSERT command
							' has been written to check for existing records and INSERT / UPDATE as appropriate.
							' 
							' This approach has been taken as I believe it should be faster, especially for 
							' mass updates.
							' ********************************************************

							DSCustomData = New RenaissanceDataClass.DSPertracCustomFieldData
							tblCustomData = DSCustomData.tblPertracCustomFieldData

							For RowCount = 1 To (Grid_Returns.Rows.Count - 1)
								If (CStr(Grid_Returns.Item(RowCount, Changed_Ordinal)) = "x") Then
									Grid_Returns.Item(RowCount, Changed_Ordinal) = ""

									PertracID = CInt(Grid_Returns.Item(RowCount, ID_Ordinal))

									ThisRow = tblCustomData.NewtblPertracCustomFieldDataRow

									ThisRow.FieldDataID = 0
									ThisRow.CustomFieldID = CustomFieldID
									ThisRow.PertracID = PertracID
									ThisRow.GroupID = GroupListID
									ThisRow.FieldNumericData = 0
									ThisRow.FieldTextData = ""
									ThisRow.FieldDateData = Renaissance_BaseDate
									ThisRow.FieldBooleanData = False
									ThisRow.UpdateRequired = False
									ThisRow.LatestReturnDate = Renaissance_BaseDate

									Try
										ThisGridItem = Grid_Returns.Item(RowCount, ColCount)

										If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) Then
											If (ThisGridItem IsNot Nothing) Then
												ThisRow.FieldTextData = ThisGridItem.ToString
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) Then
											If IsNumeric(ThisGridItem) Then
												ThisRow.FieldNumericData = CDbl(ThisGridItem)
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) Then
											If IsDate(ThisGridItem) Then
												ThisRow.FieldDateData = CDate(ThisGridItem)
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) Then
											ThisRow.FieldBooleanData = CBool(ThisGridItem)

										End If
									Catch ex As Exception
									End Try

									If (ThisRow.RowState And DataRowState.Detached) Then
										tblCustomData.Rows.Add(ThisRow)
									End If

									ThisCustomFieldDataUpdated = True

								End If

							Next

							If (ThisCustomFieldDataUpdated) Then
								CustomFieldDataUpdated = True
								MainForm.AdaptorUpdate(Me.Name, AdpCustomData, tblCustomData)

								If (CustomFieldDataUpdateString.Length > 0) Then
									CustomFieldDataUpdateString &= ","
								End If
								CustomFieldDataUpdateString &= (CustomFieldID.ToString & "." & GroupListID.ToString)

								CustomFieldDataCache.ClearDataCacheGroupData(CustomFieldID, GroupListID)
							End If

							tblCustomData = Nothing
							DSCustomData = Nothing

						End If
					End If
				Next

			Catch ex As Exception
				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace
			Finally
				If (ThisConnection IsNot Nothing) Then
					Try
						ThisConnection.Close()
					Catch ex As Exception
					End Try
					ThisConnection = Nothing
				End If
			End Try

		End If


		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes
		Try
			Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

			If ItemsUpdated Then
				UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItems.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblGroupItems.ChangeID) = UpdateDetailsGroupItems
      End If

      If ItemsDataUpdated Then
        UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItemData.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblGroupItemData.ChangeID) = UpdateDetailsDataItems
      End If

      If (CustomFieldDataUpdated) Then
        UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblPertracCustomFieldData.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblPertracCustomFieldData.ChangeID) = CustomFieldDataUpdateString
      End If

			If (ItemsUpdated) OrElse (ItemsDataUpdated) OrElse (CustomFieldDataUpdated) Then
				Call MainForm.Main_RaiseEvent(UpdateMessage)
			End If
		Catch ex As Exception
		End Try

	End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission)) Then


		Else


		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 


		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
    End If

    Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.Ascending, -1)
    Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' ********************************************************************************************
    ' Selection Combo. SelectedItem changed.
    '
    ' ********************************************************************************************

    If (GenericUpdateObject IsNot Nothing) Then

      GenericUpdateObject.FormToUpdate = True

    Else

      FormUpdate_Tick()

    End If


	End Sub


	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
      Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
      Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Form User Control Event Code (inc Grid Events)."

	Private Sub Btn_ShowCarts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ShowCarts.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Split_GroupReturns.Panel2Collapsed = Not Split_GroupReturns.Panel2Collapsed

			If Split_GroupReturns.Panel2Collapsed Then
				Btn_ShowCarts.Text = "Show Charts"
			Else
				Btn_ShowCarts.Text = "Hide Charts"
				Split_GroupReturns_Resize(Split_GroupReturns, New EventArgs)
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Btn_SetSingleBenchmarkIndex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_SetSingleBenchmarkIndex.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (Grid_Returns.Row > 0) Then
      SetSingleBenchmarkIndex(Grid_Returns.Row)
		End If

		Call FormControlChanged(sender, New EventArgs)

	End Sub

  Private Sub SetSingleBenchmarkIndex(ByVal ReturnsRow As Integer)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim SelectedIndex As Integer
    Dim ThisItem As Object
    Dim PertracID As Integer = 0
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex

    Try
      If (Combo_BenckmarkIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_BenckmarkIndex.SelectedValue)) Then
        SelectedIndex = CInt(Combo_BenckmarkIndex.SelectedValue)

        If (Me.Grid_Returns.Rows.Count > 1) AndAlso (ReturnsRow > 0) Then
          Grid_Returns.Item(ReturnsRow, Grid_Returns.Cols("GroupIndexCode").SafeIndex) = SelectedIndex

          ThisItem = Grid_Returns.Item(ReturnsRow, Grid_Returns.Cols.IndexOf("GroupPertracCode"))
          If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
            PertracID = CInt(ThisItem)
          End If

          GetAlphaBetaAndStdError(PertracID, SelectedIndex, ReturnsRow)

          Grid_Returns.Item(ReturnsRow, Col_GroupBetaE) = 0

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

	Private Sub Btn_SetAllBenchmarkIndex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_SetAllBenchmarkIndex.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

    SetAllBenchmarkIndexRows()

		Call FormControlChanged(sender, New EventArgs)

	End Sub

  Private Sub SetAllBenchmarkIndexRows()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim SelectedIndex As Integer
    Dim GridRow As Integer
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim ExistingReturn As Double = 0

    Try

      If (Combo_BenckmarkIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_BenckmarkIndex.SelectedValue)) Then
        SelectedIndex = CInt(Combo_BenckmarkIndex.SelectedValue)

        If (SelectedIndex > 0) Then
          For GridRow = 1 To (Me.Grid_Returns.Rows.Count - 1)
            Try
              If (CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) = SelectedIndex) AndAlso (IsNumeric(Grid_Returns.Item(GridRow, Col_GroupIndexE))) Then
                ExistingReturn = Grid_Returns.Item(GridRow, Col_GroupIndexE)
                Exit For
              End If
            Catch ex As Exception
            End Try
          Next
        End If

        For GridRow = 1 To (Grid_Returns.Rows.Count - 1)
          If (IsNumeric(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) = False) OrElse CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) <> SelectedIndex Then
            Me.SetSingleBenchmarkIndex(GridRow)
            Grid_Returns.Item(GridRow, Col_GroupIndexE) = ExistingReturn
          End If
        Next
      End If

    Catch ex As Exception
    End Try
  End Sub

	Private Sub Menu_SetBenchmarkSingleRow(ByVal sender As Object, ByVal e As System.EventArgs)
		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (GridClickMouseRow > 0) Then
      SetSingleBenchmarkIndex(GridClickMouseRow)
		End If

		Call FormControlChanged(sender, New EventArgs)
	End Sub

	Private Sub Menu_SetStandardReturnColumns(ByVal sender As Object, ByVal e As System.EventArgs)
		Call SetGrid_StandardReturnColumns()
	End Sub

	Private Sub Menu_SetStandardOptimiserColumns(ByVal sender As Object, ByVal e As System.EventArgs)
		Call SetGrid_StandardOptimiserColumns()
	End Sub

	Private Sub Menu_SetBenchmarkAllRows(ByVal sender As Object, ByVal e As System.EventArgs)
    SetAllBenchmarkIndexRows()

		Call FormControlChanged(sender, New EventArgs)
	End Sub

	Private Sub SetThisExpectedBeta(ByVal sender As Object, ByVal e As System.EventArgs)
		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (GridClickMouseRow > 0) Then
			Dim Col_BetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
			Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex

			Grid_Returns.Item(GridClickMouseRow, Col_BetaE) = Grid_Returns.Item(GridClickMouseRow, Col_Beta)

			Call FormControlChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetAllExpectedBeta(ByVal sender As Object, ByVal e As System.EventArgs)
		If (Me.Grid_Returns.Rows.Count > 1) Then
			Dim Col_BetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
			Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
			Dim RowCount As Integer

			For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Grid_Returns.Item(RowCount, Col_BetaE) = Grid_Returns.Item(RowCount, Col_Beta)
			Next

			Call FormControlChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetThisExpectedAlpha(ByVal sender As Object, ByVal e As System.EventArgs)
		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (GridClickMouseRow > 0) Then
			Dim Col_AlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
			Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex

			Grid_Returns.Item(GridClickMouseRow, Col_AlphaE) = Grid_Returns.Item(GridClickMouseRow, Col_Alpha)

			Call FormControlChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetAllExpectedAlpha(ByVal sender As Object, ByVal e As System.EventArgs)
		If (Me.Grid_Returns.Rows.Count > 1) Then
			Dim Col_AlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
			Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
			Dim RowCount As Integer

			For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Grid_Returns.Item(RowCount, Col_AlphaE) = Grid_Returns.Item(RowCount, Col_Alpha)
			Next

			Call FormControlChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub Grid_Returns_BeforeSort(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.SortColEventArgs) Handles Grid_Returns.BeforeSort
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Grid_Returns.Col = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Returns_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Returns.CellChanged
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (e.Row <= 0) OrElse (e.Col <= 1) Then
			Exit Sub
		End If

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
		Dim Col_GroupAlpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
		Dim Col_GroupBeta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
		Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
		Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
		Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
		Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
		Dim Col_GroupStdErr As Integer = Grid_Returns.Cols("GroupStdErr").SafeIndex
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

		Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
		Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex

		Dim ThisIndexCode As Integer
		Dim ThisIndexExpectedBeta As Double
		Dim ThisIndexExpectedAlpha As Double
		Dim ThisIndexExpectedReturn As Double
		Dim RowCounter As Integer
    Dim StatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

    Dim PertracID As Integer = 0

		Try
      If IsNumeric(Grid_Returns.Item(e.Row, Col_GroupPertracCode)) Then
        PertracID = Math.Max(0, CInt(Grid_Returns.Item(e.Row, Col_GroupPertracCode)))
      End If

			Select Case e.Col

				Case Col_GroupIndexE
					' Propagate Index Return 

					If Me.Grid_Returns.Row = e.Row Then	' Prevent recursive loop....
						ThisIndexCode = CInt(Grid_Returns.Item(e.Row, Col_GroupIndexCode))
						ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
						If (ThisIndexCode > 0) Then
							For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
								If (RowCounter <> e.Row) Then
									Try
										If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = ThisIndexCode) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) AndAlso (CDbl(Grid_Returns.Item(RowCounter, Col_GroupIndexE)) <> ThisIndexExpectedReturn) Then
											Grid_Returns.Item(RowCounter, Col_GroupIndexE) = ThisIndexExpectedReturn
										End If
									Catch ex As Exception
									End Try
								End If
							Next
						End If
					End If

					ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
					ThisIndexExpectedAlpha = CDbl(Grid_Returns.Item(e.Row, Col_GroupAlphaE))
					ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(e.Row, Col_GroupBetaE))

					Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = (ThisIndexExpectedBeta * ThisIndexExpectedReturn) + ThisIndexExpectedAlpha

          If (PertracID > 0) Then
            StatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(PertracID)
          End If

          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), "")
					Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)) * 100, "")

				Case Col_GroupBetaE, Col_GroupAlphaE
					' Re-Calculate results Columns

					ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
					ThisIndexExpectedAlpha = CDbl(Grid_Returns.Item(e.Row, Col_GroupAlphaE))
					ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(e.Row, Col_GroupBetaE))

					Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = (ThisIndexExpectedBeta * ThisIndexExpectedReturn) + ThisIndexExpectedAlpha

          If (PertracID > 0) Then
            StatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(PertracID)
          End If

          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), "")
					Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)) * 100, "")

				Case Col_GroupHolding

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' Holding has changed

						Grid_Returns.Item(e.Row, Col_GroupNewHolding) = Math.Round(ThisHolding + ThisTrade, GENOA_HoldingPrecision)

						Call Update_Group_Percentages(True, False)
					End If

				Case Col_GroupTrade

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' Trade has changed

						Grid_Returns.Item(e.Row, Col_GroupNewHolding) = Math.Round(ThisHolding + ThisTrade, GENOA_HoldingPrecision)

						Call Update_Group_Percentages(True, False)

					End If

				Case Col_GroupNewHolding

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' New Holding has changed

						Grid_Returns.Item(e.Row, Col_GroupTrade) = Math.Round(ThisNewHolding - ThisHolding, GENOA_HoldingPrecision)

						Call Update_Group_Percentages(True, False)

					End If

				Case Col_GroupSector

					Grid_Returns.Item(e.Row, e.Col) = SectorCombo.SelectedValue

				Case Col_GroupIndexCode

					If Me.Grid_Returns.Row = e.Row Then	' Prevent recursive loop....
						ThisIndexCode = CInt(Grid_Returns.Item(e.Row, Col_GroupIndexCode))

						If (ThisIndexCode > 0) Then
							For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
								If (RowCounter <> e.Row) Then
									Try
										If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = ThisIndexCode) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) Then
											Grid_Returns.Item(e.Row, Col_GroupIndexE) = Grid_Returns.Item(RowCounter, Col_GroupIndexE)
											Exit For
										End If
									Catch ex As Exception
									End Try
								End If
							Next
						End If
					End If

				Case Col_GroupCap
					Try
						If CBool(Grid_Returns.Item(e.Row, e.Col)) Then
							Grid_Returns.Item(e.Row, Col_GroupUpperBound) = Grid_Returns.Item(e.Row, Col_GroupPercent)
						End If
					Catch ex As Exception
					End Try

				Case Col_GroupFloor
					Try
						If CBool(Grid_Returns.Item(e.Row, e.Col)) Then
							Grid_Returns.Item(e.Row, Col_GroupLowerBound) = Grid_Returns.Item(e.Row, Col_GroupPercent)
						End If
					Catch ex As Exception
					End Try

				Case Else

			End Select

		Catch ex As Exception
			Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = 0
			Grid_Returns.Item(e.Row, Col_GroupStdErr) = 0
		Finally
			If Not (FormChanged) Then
				Call FormControlChanged(Grid_Returns, New EventArgs)
			End If

			If (Grid_Returns.Cols(e.Col).Name.StartsWith("Custom_")) Then
				Grid_Returns.Item(e.Row, 1) = "x"
			Else
				Grid_Returns.Item(e.Row, 0) = "x"
			End If
		End Try

	End Sub

	Private Sub Grid_Returns_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.EnterCell
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Static Dim lastPertracID As Integer

		Dim ThisPertracID As Integer
		Dim ThisScalingFactor As Double

		If (InPaint = False) AndAlso (Grid_Returns.Row >= 0) AndAlso (Grid_Returns.Col >= 0) Then
			Try
				Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
				Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
				Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
				Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
				Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
				Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
				Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
				Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
        Dim StatsDatePeriod As DealingPeriod

				If (IsNumeric(Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode))) Then
					ThisPertracID = Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode)
					ThisScalingFactor = CDbl(Grid_Returns.Item(Grid_Returns.Row, Col_GroupScalingFactor))

					If (ThisPertracID <> lastPertracID) Then
						' Dim ThisSeries As Integer

						lastPertracID = ThisPertracID
						Me.Label_ChartStock.Text = Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("PertracName").SafeIndex).ToString

            StatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisPertracID)

						' ThisSeries = Chart_VAMI.Series.Count - 1
            Set_LineChart(MainForm, StatsDatePeriod, ThisPertracID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
            Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), "")

						'ThisSeries = Chart_MonthlyReturns.Series.Count - 1

            Set_RollingReturnChart(MainForm, StatsDatePeriod, ThisPertracID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
            Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)) * 100, "")

					End If
				End If

				Select Case Grid_Returns.Col
					Case Col_GroupSector
						Me.SectorCombo.Text = Grid_Returns.Item(Grid_Returns.Row, Col_GroupSector)
						Me.SectorCombo.SelectedValue = Grid_Returns.Item(Grid_Returns.Row, Col_GroupSector)

					Case Col_GroupUpperBound
						' Disable Editing of UpperBound Column if 'Cap' is Set
						Try
							If CBool(Grid_Returns.Item(Grid_Returns.Row, Col_GroupCap)) Then
								Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = False
							End If
						Catch ex As Exception
						End Try

					Case Col_GroupLowerBound
						' Disable editing of LowerBound if 'Floor' is Set
						Try
							If CBool(Grid_Returns.Item(Grid_Returns.Row, Col_GroupFloor)) Then
								Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = False
							End If
						Catch ex As Exception
						End Try

				End Select

				If (Grid_Returns.Cols(Grid_Returns.Col).AllowEditing) Then
					Try
						Grid_Returns.SetCellStyle(Grid_Returns.Row, Grid_Returns.Col, Grid_Returns.Styles("Genoa_PlainEdit"))
					Catch ex As Exception
					End Try
				End If

			Catch ex As Exception

			End Try
		End If

	End Sub

	Private Sub Grid_Returns_LeaveCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.LeaveCell
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex

		Try

			Select Case Grid_Returns.Col

				Case Col_GroupSector

					If (SectorCombo.SelectedValue IsNot Nothing) Then
						If (Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) Is Nothing) OrElse (CStr(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col)) <> SectorCombo.SelectedValue.ToString) Then
							Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = SectorCombo.SelectedValue.ToString
						End If
					End If

				Case Col_GroupUpperBound, Col_GroupLowerBound
					Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = True

				Case Else


			End Select

			If Grid_Returns.Cols(Grid_Returns.Col).AllowEditing Then
				Grid_Returns.SetCellStyle(Grid_Returns.Row, Grid_Returns.Col, Grid_Returns.Cols(Grid_Returns.Col).Style)
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Returns_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.LostFocus
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Created) AndAlso (Me.Grid_Returns.Focused = False) Then

			If (Grid_Returns.Cols.Count > 1) AndAlso (Grid_Returns.Col = Grid_Returns.Cols("GroupIndexCode").SafeIndex) Then
				Dim ThisCol As Integer = Grid_Returns.Col

				Grid_Returns.Col = 0
				Grid_Returns.Col = ThisCol
			End If
		End If
	End Sub

	Private Sub Grid_Returns_ChangeEdit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.ChangeEdit
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (InPaint = False) Then
			If (Grid_Returns.Row > 0) AndAlso (Grid_Returns.Col > 1) Then
				Call FormControlChanged(Grid_Returns, New EventArgs)

				If (Grid_Returns.Cols(Grid_Returns.Col).Name.StartsWith("Custom_")) Then
					Grid_Returns.Item(Grid_Returns.Row, 1) = "x"
				Else
					Grid_Returns.Item(Grid_Returns.Row, 0) = "x"
        End If

        If (Grid_Returns.Cols(Grid_Returns.Col).Name = "GroupIndexCode") Then

          Dim PertracID As Integer = 0
          Dim IndexID As Integer = 0

          Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
          Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex

          Dim ThisItem As Object

          Try

            ThisItem = Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols.IndexOf("GroupPertracCode"))
            If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
              PertracID = CInt(ThisItem)
            End If

            ThisItem = Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexCode)

            If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
              IndexID = CInt(ThisItem)
            End If

            ' Set Alpha and Beta

            GetAlphaBetaAndStdError(PertracID, IndexID, Grid_Returns.Row)

            ' Set Index Expected return to match Index return for an existing line with the same Index (if such exists).

            Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexCode) = IndexID

          Catch ex As Exception
            Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
            Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
          End Try

        End If
			End If
		End If
	End Sub

	Private Sub Update_Group_Percentages(ByVal UpdateCapsAndFloors As Boolean, ByVal UpdatePlotPortfolio As Boolean)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
		Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
		Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
		'Dim Col_GroupNewPercent As Integer = Grid_Returns.Cols("GroupNewPercent").SafeIndex


		Try
			Dim SumValue As Double
			Dim FormatString As String

			' 'Holdings' Value

			SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupHolding, Col_GroupPercent, Col_GroupFloor, Col_GroupCap, Col_GroupLowerBound, Col_GroupUpperBound, UpdateCapsAndFloors)

			FormatString = DefaultNumericFormat(SumValue)

			Grid_Returns.Cols("GroupHolding").Format = FormatString
			Grid_Returns.Cols("GroupTrade").Format = FormatString
			Grid_Returns.Cols("GroupNewHolding").Format = FormatString

			'' 'NewHoldings' Value

			'SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupNewHolding, Col_GroupNewPercent, 0, 0, 0, 0, False)

		Catch ex As Exception
		End Try

	End Sub

	Private Sub IndexComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
    ' Event handler for 'PertracGridCombo', event added manually.
		'
		' *****************************************************************************************

		If (sender.Focused) AndAlso (InPaint = False) AndAlso (Me.IsDisposed = False) AndAlso (Grid_Returns.Row > 0) Then
			FormControlChanged(sender, e)
			Grid_Returns.Item(Grid_Returns.Row, 0) = "x"

			Dim PertracID As Integer = 0
			Dim IndexID As Integer = 0

			Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
			Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex

			Dim ThisItem As Object

			Try

				ThisItem = Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols.IndexOf("GroupPertracCode"))
				If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
					PertracID = CInt(ThisItem)
				End If

				If (TypeOf sender Is ComboBox) Then
					ThisItem = CType(sender, ComboBox).SelectedValue
				ElseIf (TypeOf sender Is FCP_TelerikControls.FCP_RadComboBox) Then
					ThisItem = CType(sender, FCP_TelerikControls.FCP_RadComboBox).SelectedValue
				End If

				If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
					IndexID = CInt(ThisItem)
				End If

				' Set Alpha and Beta

        GetAlphaBetaAndStdError(PertracID, IndexID, Grid_Returns.Row)

				' Set Index Expected return to match Index return for an existing line with the same Index (if such exists).

				Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexCode) = IndexID

				'If (IndexID > 0) Then
				'	For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
				'		If (RowCounter <> Grid_Returns.Row) Then
				'			Try
				'				If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = IndexID) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) Then
				'					Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexE) = Grid_Returns.Item(RowCounter, Col_GroupIndexE)
				'					Exit For
				'				End If
				'			Catch ex As Exception
				'			End Try
				'		End If
				'	Next
				'End If

			Catch ex As Exception
				Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
				Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
			End Try

		End If
	End Sub

	Private Sub SectorComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (InPaint = False) AndAlso (Me.IsDisposed = False) AndAlso (Grid_Returns.Row > 0) Then
			Dim SectorCombo As CustomC1Combo

			Try
				If Not (TypeOf sender Is CustomC1Combo) Then
					Exit Sub
				End If

				SectorCombo = CType(sender, CustomC1Combo)

				If (SectorCombo.SelectedIndex < 0) Then
					Dim DTable As DataTable = SectorCombo.DataSource
					Dim NewRow As DataRow = DTable.NewRow

					NewRow("DM") = SectorCombo.Text
					NewRow("VM") = SectorCombo.Text

					DTable.Rows.Add(NewRow)
					SectorCombo.SelectedIndex = SectorCombo.Items.Count - 1
					SectorCombo.Text = SectorCombo.SelectedValue.ToString
				End If

				Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = SectorCombo.SelectedValue.ToString
			Catch ex As Exception
				Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = ""
			End Try

		End If

	End Sub

  Private Sub GetAlphaBetaAndStdError(ByVal PertracID As Integer, ByVal ReferenceID As Integer, ByVal GridRow As Integer)
    ' ********************************************************************************
    '
    ' Return Set Alph & Beta Stats using the lowest Common Native Date Period.
    '
    ' ********************************************************************************
    Dim StartDate As Date
    Dim EndDate As Date

    Dim ThisItem As Object

    Try
      ThisItem = Grid_Returns.Item(GridRow, Grid_Returns.Cols.IndexOf("GroupDateFrom"))
      If (ThisItem IsNot Nothing) AndAlso (IsDate(ThisItem)) Then
        StartDate = CDate(ThisItem)
      Else
        ReferenceID = 0
      End If

      ThisItem = Grid_Returns.Item(GridRow, Grid_Returns.Cols.IndexOf("GroupDateTo"))
      If (ThisItem IsNot Nothing) AndAlso (IsDate(ThisItem)) Then
        EndDate = CDate(ThisItem)
      Else
        ReferenceID = 0
      End If

      If (PertracID > 0) AndAlso (ReferenceID > 0) Then
        Dim thisStatObject As StatFunctions.ComparisonStatsClass

        thisStatObject = MainForm.StatFunctions.GetComparisonStatsItem(MainForm.PertracData.GetPertracDataPeriod(PertracID, ReferenceID), CULng(PertracID), CULng(ReferenceID), False, CULng(PertracID), StartDate, EndDate, 1, 999, -1)

        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = thisStatObject.Alpha(PertracID)
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = thisStatObject.Beta(PertracID)
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = thisStatObject.StandardError(PertracID)
      Else
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = 0
      End If

    Catch ex As Exception
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = 0
    End Try

  End Sub

	Private Sub Split_GroupReturns_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Split_GroupReturns.Resize
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Me.Chart_MonthlyReturns.Top = ((Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2) + Label_ChartStock.Height
			Me.Chart_MonthlyReturns.Height = (Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2
			Me.Chart_VAMI.Height = (Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2

		Catch ex As Exception

		End Try
	End Sub

	Private Sub GetVeniceSector(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceSectors(SelectionEnum.SingleRow)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectedVeniceSectors(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceSectors(SelectionEnum.MultipleRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetAllVeniceSectors(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceSectors(SelectionEnum.AllRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetVeniceSectors(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisPertracID As Integer
		Dim VeniceSector As String
		Dim VeniceFundType As Integer
		Dim GridRow As Integer
		Dim StartRow As Integer = 0
		Dim EndRow As Integer = 0

		Select Case SelectionType

			Case SelectionEnum.MultipleRows
				If (Grid_Returns.Selection.IsValid) Then
					StartRow = Grid_Returns.Selection.r1
					EndRow = Grid_Returns.Selection.r2
				End If

			Case SelectionEnum.AllRows
				StartRow = 1
				EndRow = Grid_Returns.Rows.Count - 1

			Case Else
				StartRow = GridClickMouseRow
				EndRow = GridClickMouseRow

		End Select

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

		If (Me.Created) AndAlso (InPaint = False) Then
			Try
				For GridRow = StartRow To EndRow
					If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then

						Try

							ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
							VeniceFundType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "InstrumentFundType", "InstrumentID", "InstrumentPertracCode=" & ThisPertracID.ToString)

							If (ThisPertracID <= 0) OrElse (VeniceFundType <= 0) Then
								Grid_Returns.Item(GridRow, Col_GroupSector) = ""
							Else

								VeniceSector = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFundType, VeniceFundType, "FundTypeDescription")

								CheckSectorComboContains(VeniceSector)

								If (Grid_Returns.Row = GridRow) Then
									Me.SectorCombo.SelectedValue = VeniceSector
									Me.SectorCombo.Text = VeniceSector
								End If

								Try
									InPaint = True
									Grid_Returns.Item(GridRow, Col_GroupSector) = VeniceSector
								Catch ex As Exception
								Finally
									InPaint = False
								End Try

							End If

						Catch ex As Exception
							Grid_Returns.Item(GridRow, Col_GroupSector) = ""
						Finally
							Grid_Returns.Item(GridRow, 0) = "x"
						End Try

					End If
				Next

			Catch ex As Exception
			End Try
		End If

	End Sub

	Private Sub GetPertracSector(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetPertracSectors(SelectionEnum.SingleRow)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectedPertracSectors(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetPertracSectors(SelectionEnum.MultipleRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetAllPertracSectors(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetPertracSectors(SelectionEnum.AllRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetPertracSectors(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisPertracID As Integer
		Dim PertracSector As String
		Dim TempValue As Object
		Dim GridRow As Integer
		Dim StartRow As Integer = 0
		Dim EndRow As Integer = 0

		Select Case SelectionType

			Case SelectionEnum.MultipleRows
				If (Grid_Returns.Selection.IsValid) Then
					StartRow = Grid_Returns.Selection.r1
					EndRow = Grid_Returns.Selection.r2
				End If

			Case SelectionEnum.AllRows
				StartRow = 1
				EndRow = Grid_Returns.Rows.Count - 1

			Case Else
				StartRow = GridClickMouseRow
				EndRow = GridClickMouseRow

		End Select

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

		If (Me.Created) AndAlso (InPaint = False) Then
			Try
				For GridRow = StartRow To EndRow
					If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then
						Try

							ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
							TempValue = MainForm.PertracData.GetInformationValue(ThisPertracID, PertracInformationFields.Strategy).ToString()

							If (ThisPertracID <= 0) OrElse (TempValue Is Nothing) Then
								Grid_Returns.Item(GridRow, Col_GroupSector) = ""
							Else

								PertracSector = CStr(TempValue)

								CheckSectorComboContains(PertracSector)

								If (Grid_Returns.Row = GridRow) Then
									Me.SectorCombo.SelectedValue = PertracSector
									Me.SectorCombo.Text = PertracSector
								End If

								Try
									InPaint = True
									Grid_Returns.Item(GridRow, Col_GroupSector) = PertracSector
								Catch ex As Exception
								Finally
									InPaint = False
								End Try

							End If

						Catch ex As Exception
							Grid_Returns.Item(GridRow, Col_GroupSector) = ""

						Finally
							Grid_Returns.Item(GridRow, 0) = "x"

						End Try
					End If
				Next

			Catch ex As Exception
			End Try
		End If
	End Sub

	Private Sub GetVeniceLiquidity(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceLiquidities(SelectionEnum.SingleRow)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectedVeniceLiquidities(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceLiquidities(SelectionEnum.MultipleRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetAllVeniceLiquidities(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			GetVeniceLiquidities(SelectionEnum.AllRows)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetVeniceLiquidities(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisPertracID As Integer
		Dim VeniceDealingPeriod As RenaissanceGlobals.DealingPeriod
		Dim VeniceDealingDays As Integer
		Dim GridRow As Integer
		Dim StartRow As Integer = 0
		Dim EndRow As Integer = 0

		Select Case SelectionType

			Case SelectionEnum.MultipleRows
				If (Grid_Returns.Selection.IsValid) Then
					StartRow = Grid_Returns.Selection.r1
					EndRow = Grid_Returns.Selection.r2
				End If

			Case SelectionEnum.AllRows
				StartRow = 1
				EndRow = Grid_Returns.Rows.Count - 1

			Case Else
				StartRow = GridClickMouseRow
				EndRow = GridClickMouseRow

		End Select

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex

		If (Me.Created) AndAlso (InPaint = False) Then
			Try
				For GridRow = StartRow To EndRow
					If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then

						Try

							ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
							VeniceDealingPeriod = CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "InstrumentDealingPeriod", "InstrumentID", "InstrumentPertracCode=" & ThisPertracID.ToString), RenaissanceGlobals.DealingPeriod)

							If (ThisPertracID <= 0) OrElse (VeniceDealingPeriod <= 0) Then
								Grid_Returns.Item(GridRow, Col_GroupLiquidity) = 0
							Else

								VeniceDealingDays = 0
								Select Case VeniceDealingPeriod
									Case DealingPeriod.Daily
										VeniceDealingDays = 1

									Case DealingPeriod.Weekly
										VeniceDealingDays = 7

									Case DealingPeriod.Fortnightly
										VeniceDealingDays = 14

									Case DealingPeriod.Monthly
										VeniceDealingDays = 30

									Case DealingPeriod.Quarterly
										VeniceDealingDays = 90

									Case DealingPeriod.SemiAnnually
										VeniceDealingDays = 180

									Case DealingPeriod.Annually
										VeniceDealingDays = 365

								End Select

								Try
									InPaint = True
									Grid_Returns.Item(GridRow, Col_GroupLiquidity) = VeniceDealingDays
								Catch ex As Exception
								Finally
									InPaint = False
								End Try

							End If

						Catch ex As Exception
							Grid_Returns.Item(GridRow, Col_GroupLiquidity) = 0
						Finally
							Grid_Returns.Item(GridRow, 0) = "x"
						End Try

					End If
				Next

			Catch ex As Exception
			End Try
		End If

	End Sub

	Private Sub DeleteGroupCustomFieldData(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisPertracID As Integer
		Dim ThisGroupListID As Integer
		Dim ThisCustomFieldID As Integer
		Dim ThisColumnName As String

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex

		If (Me.Created) AndAlso (InPaint = False) AndAlso (GridClickMouseRow >= 0) AndAlso (GridClickMouseCol >= 2) Then
			If MessageBox.Show("Are you sure that you want to delete this Custom Field Data Item ?", "Delete ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
				Exit Sub
			End If

			Try
				ThisPertracID = Grid_Returns.Item(GridClickMouseRow, Col_GroupPertracCode)
				ThisColumnName = Grid_Returns.Cols(GridClickMouseCol).Name

				If (ThisColumnName.StartsWith("Custom_") = False) OrElse (IsNumeric(ThisColumnName.Substring(7)) = False) Then
					Exit Sub
				End If
				If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
					Exit Sub
				End If

				ThisCustomFieldID = CInt(ThisColumnName.Substring(7))
				ThisGroupListID = CInt(Combo_SelectGroupID.SelectedValue)

				Dim tmpCommand As New SqlCommand

				Try
					tmpCommand.CommandType = CommandType.StoredProcedure
					tmpCommand.CommandText = "adp_tblPertracCustomFieldData_DeleteCommand"
					tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = ThisGroupListID
					tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = ThisCustomFieldID
					tmpCommand.Parameters.Add("@PertracID", SqlDbType.Int).Value = ThisPertracID

					tmpCommand.Connection = MainForm.GetGenoaConnection
					tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

					tmpCommand.ExecuteNonQuery()
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from Custom Field Data" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
				Finally
					Try
						If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
							tmpCommand.Connection.Close()
							tmpCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try
				End Try

				CustomFieldDataCache.ClearDataCacheGroupData(ThisCustomFieldID, ThisGroupListID)


				Try
					InPaint = True

					' Return Data for Group Zero, hiving just deleted the data for this group!

					Grid_Returns.Item(GridClickMouseRow, GridClickMouseCol) = CustomFieldDataCache.GetDataPoint(ThisCustomFieldID, 0, ThisPertracID)

					Application.DoEvents()
				Catch ex As Exception
				Finally
					InPaint = False
				End Try


			Catch ex As Exception
			End Try

			MainForm.ReloadTable(RenaissanceChangeID.tblPertracCustomFieldData, False)
			Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPertracCustomFieldData))

		End If

	End Sub

	Private Sub GetGroupZeroCustomFieldData(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisPertracID As Integer
		Dim ThisCustomFieldID As Integer
		Dim ThisColumnName As String

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex

		If (Me.Created) AndAlso (InPaint = False) AndAlso (GridClickMouseRow >= 0) AndAlso (GridClickMouseCol >= 2) Then
			Try
				ThisPertracID = Grid_Returns.Item(GridClickMouseRow, Col_GroupPertracCode)
				ThisColumnName = Grid_Returns.Cols(GridClickMouseCol).Name

				If (ThisColumnName.StartsWith("Custom_") = False) OrElse (IsNumeric(ThisColumnName.Substring(7)) = False) Then
					Exit Sub
				End If
				If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
					Exit Sub
				End If

				ThisCustomFieldID = CInt(ThisColumnName.Substring(7))

				Grid_Returns.Item(GridClickMouseRow, GridClickMouseCol) = CustomFieldDataCache.GetDataPoint(ThisCustomFieldID, 0, ThisPertracID)

			Catch ex As Exception
			End Try
		End If

	End Sub

	Private Sub CheckSectorComboContains(ByVal pSectorName As String)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisTable As DataTable
		Dim ThisRow As DataRow
		Dim RowCount As Integer

		ThisTable = SectorCombo.DataSource
		For RowCount = 0 To (ThisTable.Rows.Count - 1)
			ThisRow = ThisTable.Rows(RowCount)

			If ThisRow("VM") = pSectorName Then
				Exit Sub
			End If
		Next

		ThisRow = ThisTable.NewRow
		ThisRow("DM") = pSectorName
		ThisRow("VM") = pSectorName
		ThisTable.Rows.Add(ThisRow)

	End Sub

	Private Sub SetGrid_StandardReturnColumns()

		Dim thisColID As Integer
		Dim thisCol As C1.Win.C1FlexGrid.Column

		Grid_Returns.Cols("Col_Updated").Move(0)
		Grid_Returns.Cols("Col_CustomUpdated").Move(1)
		Grid_Returns.Cols("PertracName").Move(2)
		Grid_Returns.Cols("GroupIndexCode").Move(3)
		Grid_Returns.Cols("GroupBeta").Move(4)
		Grid_Returns.Cols("GroupAlpha").Move(5)
		Grid_Returns.Cols("GroupIndexE").Move(6)
		Grid_Returns.Cols("GroupBetaE").Move(7)
		Grid_Returns.Cols("GroupAlphaE").Move(8)
		Grid_Returns.Cols("GroupExpectedReturn").Move(9)
		Grid_Returns.Cols("GroupStdErr").Move(10)

		For thisColID = 0 To 10
			Grid_Returns.Cols(thisColID).Visible = True
		Next
		For thisColID = 11 To (Grid_Returns.Cols.Count - 1)
			Grid_Returns.Cols(thisColID).Visible = False
		Next

		If (FieldsMenu IsNot Nothing) Then
			For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update and Col_CustomUpdate
				thisCol = Grid_Returns.Cols(thisColID)
				Try
					CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = thisCol.Visible
				Catch ex As Exception
				End Try
			Next
		End If

		Try
			' Clear Custom Field Checks
			Dim ThisDropDownItem As ToolStripDropDownItem

			For Each ThisDropDownItem In CustomFieldsMenu.DropDownItems
				CType(ThisDropDownItem, ToolStripMenuItem).Checked = False
			Next
		Catch ex As Exception
		End Try
	End Sub

	Private Sub SetGrid_StandardOptimiserColumns()

		Dim thisColID As Integer
		Dim thisCol As C1.Win.C1FlexGrid.Column

		Grid_Returns.Cols("Col_Updated").Move(0)
		Grid_Returns.Cols("Col_CustomUpdated").Move(1)
		Grid_Returns.Cols("PertracName").Move(2)
		Grid_Returns.Cols("GroupSector").Move(3)
		Grid_Returns.Cols("GroupHolding").Move(4)
		Grid_Returns.Cols("GroupNewHolding").Move(5)
		Grid_Returns.Cols("GroupTradeSize").Move(6)
		Grid_Returns.Cols("GroupPercent").Move(7)
		Grid_Returns.Cols("GroupFloor").Move(8)
		Grid_Returns.Cols("GroupCap").Move(9)
		Grid_Returns.Cols("GroupLowerBound").Move(10)
		Grid_Returns.Cols("GroupUpperBound").Move(11)
		Grid_Returns.Cols("GroupLiquidity").Move(12)
		Grid_Returns.Cols("GroupExpectedReturn").Move(13)

		For thisColID = 0 To 13
			Grid_Returns.Cols(thisColID).Visible = True
		Next
		For thisColID = 14 To (Grid_Returns.Cols.Count - 1)
			Grid_Returns.Cols(thisColID).Visible = False
		Next

		If (FieldsMenu IsNot Nothing) Then
			For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update, Col_CustomUpdate
				thisCol = Grid_Returns.Cols(thisColID)
				Try
					CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = thisCol.Visible
				Catch ex As Exception
				End Try
			Next
		End If

		Try
			' Clear Custom Field Checks
			Dim ThisDropDownItem As ToolStripDropDownItem

			For Each ThisDropDownItem In CustomFieldsMenu.DropDownItems
				CType(ThisDropDownItem, ToolStripMenuItem).Checked = False
			Next
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' *****************************************************************************************
		' Cancel Changes, redisplay form.
		' *****************************************************************************************

		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' *****************************************************************************************
		' Save Changes, if any, without prompting.
		' *****************************************************************************************

		If (FormChanged = True) Then
			Call SetFormData(False)
		ElseIf btnSave.Enabled Then
			SetButtonStatus()
		End If

	End Sub

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************************
		' Close Form
		' *****************************************************************************************

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region


	Private Sub CreateCustomFieldColumn(ByRef CustomFieldID As Integer)
		' ********************************************************************************************
		' Create a column in the Returns grid for the given Custom Field ID.
		'
		'
		' ********************************************************************************************

		Dim ThisColumnName As String
		Dim ThisColumn As C1.Win.C1FlexGrid.Column
		Dim GroupListID As Integer

		ThisColumnName = "Custom_" & CustomFieldID.ToString

		Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow

		ThisColumn = Nothing

		If (CustomFieldID > 0) Then

			' Check that Custom Data is present for the selected field(s)
			If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
				GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
			Else
				GroupListID = (0)
			End If

			If CustomFieldDataCache.GetCustomFieldDataStatus(CustomFieldID, GroupListID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
				CustomFieldDataCache.LoadCustomFieldData(CustomFieldID, GroupListID)
			End If


			' Add Column if necessary

			If Not Grid_Returns.Cols.Contains(ThisColumnName) Then
				ThisColumn = Grid_Returns.Cols.Add()
				ThisColumn.Name = ThisColumnName
				ThisColumn.Caption = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "FieldName")
				ThisColumn.Visible = True
				ThisColumn.DataType = GetType(String)
				ThisColumn.AllowDragging = True
				ThisColumn.AllowEditing = False
				ThisColumn.AllowResizing = True
				ThisColumn.AllowSorting = True
			End If

			' Format the New column

			If (ThisColumn IsNot Nothing) Then ' A newly added column
				thisCustomFieldDefinition = Nothing
				Try
					thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

					If (thisCustomFieldDefinition IsNot Nothing) Then
						If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
							ThisColumn.DataType = GetType(String)

						ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
							ThisColumn.DataType = GetType(Date)
							ThisColumn.Format = DISPLAYMEMBER_DATEFORMAT

						ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
							ThisColumn.DataType = GetType(Boolean)

						ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.PercentageType) = RenaissanceDataType.PercentageType Then
							ThisColumn.DataType = GetType(Double)
							ThisColumn.Format = "#,##0.00%"

						ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) = RenaissanceDataType.NumericType Then
							ThisColumn.DataType = GetType(Double)
							ThisColumn.Format = "#,##0.00"
						End If

						' Editable Column ?

						If (Me.HasUpdatePermission) Then
							Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
								Case PertracCustomFieldTypes.CustomBoolean
									ThisColumn.AllowEditing = True

								Case PertracCustomFieldTypes.CustomDate
									ThisColumn.AllowEditing = True

								Case PertracCustomFieldTypes.CustomNumeric
									ThisColumn.AllowEditing = True

								Case PertracCustomFieldTypes.CustomString
									ThisColumn.AllowEditing = True

							End Select
						End If

					End If
				Catch ex As Exception
				End Try
			End If
		End If

	End Sub

	Private Sub PaintCustomFieldColumn(ByRef pCustomColumnName As String)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			If (pCustomColumnName.StartsWith("Custom_")) AndAlso (IsNumeric(pCustomColumnName.Substring(7))) Then
				PaintCustomFieldColumn(CInt(pCustomColumnName.Substring(7)))
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub PaintCustomFieldColumn(ByRef pCustomFieldID As Integer)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisColumnName As String
		Dim ThisColumnOrdinal As Integer
		Dim PertracColumnOrdinal As Integer
		Dim ThisCustomValue As Object = Nothing
		Dim OrgInPaint As Boolean
		Dim RowCount As Integer
		Dim PertracID As Integer
		Dim GroupListID As Integer

		' Build Column Name

		ThisColumnName = "Custom_" & pCustomFieldID.ToString

		' Resolve Custom Column.

		If Grid_Returns.Cols.Contains(ThisColumnName) = False Then
			Exit Sub
		Else
			ThisColumnOrdinal = Grid_Returns.Cols.IndexOf(ThisColumnName)
		End If

		' Resolve PertracCode Column

		PertracColumnOrdinal = Grid_Returns.Cols.IndexOf("GroupPertracCode")

		' Resolve GroupListID

		If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
			GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
		Else
			GroupListID = (0)
		End If

		' OK...

		Try
			OrgInPaint = InPaint
			InPaint = True

			If CustomFieldDataCache.GetCustomFieldDataStatus(pCustomFieldID, GroupListID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
				CustomFieldDataCache.LoadCustomFieldData(pCustomFieldID, GroupListID)
			End If

			' Set Grid Custom Field Data

			For RowCount = 1 To (Grid_Returns.Rows.Count - 1)

				' Get Pertrac ID.

				If (IsNumeric(Grid_Returns.Item(RowCount, PertracColumnOrdinal))) Then
					PertracID = CInt(Grid_Returns.Item(RowCount, PertracColumnOrdinal))
				Else
					PertracID = (-1)
				End If

				' If Valid Instrument...

				If (PertracID > 0) Then

					Try
						Try
							ThisCustomValue = Nothing

							ThisCustomValue = CustomFieldDataCache.GetDataPoint(pCustomFieldID, GroupListID, PertracID)

							If (ThisCustomValue Is Nothing) OrElse (ThisCustomValue Is DBNull.Value) Then
								ThisCustomValue = Nz(ThisCustomValue, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
							End If
						Catch ex As Exception
						End Try

						' Set Grid Value

						Grid_Returns.Item(RowCount, ThisColumnOrdinal) = ThisCustomValue

					Catch ex As Exception
						' Set Zero value, appropriate to column type.
						Grid_Returns.Item(RowCount, ThisColumnOrdinal) = Nz(Nothing, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
					End Try

				Else
					Grid_Returns.Item(RowCount, ThisColumnOrdinal) = Nz(Nothing, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
				End If
			Next

		Catch ex As Exception
		Finally
			InPaint = OrgInPaint
		End Try

	End Sub


	Private Sub Grid_Returns_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Returns.KeyDown
		' *************************************************************************************
		'
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), False, False)
			End If
		Catch ex As Exception
		End Try
	End Sub
End Class
