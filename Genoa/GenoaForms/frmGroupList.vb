Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass


Public Class frmGroupList

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
  Friend WithEvents edit_GroupName As System.Windows.Forms.TextBox
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
  Friend WithEvents btnLast As System.Windows.Forms.Button
  Friend WithEvents btnAdd As System.Windows.Forms.Button
  Friend WithEvents btnDelete As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectEntityID As System.Windows.Forms.ComboBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Combo_GroupGroup As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_GroupID As System.Windows.Forms.Label
  Friend WithEvents lbl_EntityName As System.Windows.Forms.Label
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Date_DateFrom As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label_DecisionDate As System.Windows.Forms.Label
  Friend WithEvents Date_DateTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Radio_NoConstraintSet As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_NewConstraintSet As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ExistingConstraintSet As System.Windows.Forms.RadioButton
	Friend WithEvents Combo_ConstraintSets As System.Windows.Forms.ComboBox
	Friend WithEvents Panel_Constraints As System.Windows.Forms.Panel
	Friend WithEvents Panel_Covariance As System.Windows.Forms.Panel
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Combo_CovarianceMatrix As System.Windows.Forms.ComboBox
	Friend WithEvents Radio_LiveCovarianceMatrix As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_SavedCovarianceMatrix As System.Windows.Forms.RadioButton
	Friend WithEvents btnCopy As System.Windows.Forms.Button
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.edit_GroupName = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectEntityID = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.btnCopy = New System.Windows.Forms.Button
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Combo_GroupGroup = New System.Windows.Forms.ComboBox
    Me.lbl_GroupID = New System.Windows.Forms.Label
    Me.lbl_EntityName = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
    Me.Date_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label_DecisionDate = New System.Windows.Forms.Label
    Me.Date_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Radio_NoConstraintSet = New System.Windows.Forms.RadioButton
    Me.Radio_NewConstraintSet = New System.Windows.Forms.RadioButton
    Me.Radio_ExistingConstraintSet = New System.Windows.Forms.RadioButton
    Me.Combo_ConstraintSets = New System.Windows.Forms.ComboBox
    Me.Panel_Constraints = New System.Windows.Forms.Panel
    Me.Panel_Covariance = New System.Windows.Forms.Panel
    Me.Label4 = New System.Windows.Forms.Label
    Me.Combo_CovarianceMatrix = New System.Windows.Forms.ComboBox
    Me.Radio_LiveCovarianceMatrix = New System.Windows.Forms.RadioButton
    Me.Radio_SavedCovarianceMatrix = New System.Windows.Forms.RadioButton
    Me.Panel1.SuspendLayout()
    Me.Panel_Constraints.SuspendLayout()
    Me.Panel_Covariance.SuspendLayout()
    Me.SuspendLayout()
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(389, 60)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(50, 20)
    Me.editAuditID.TabIndex = 2
    '
    'edit_GroupName
    '
    Me.edit_GroupName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_GroupName.Location = New System.Drawing.Point(121, 103)
    Me.edit_GroupName.MaxLength = 100
    Me.edit_GroupName.Name = "edit_GroupName"
    Me.edit_GroupName.Size = New System.Drawing.Size(320, 20)
    Me.edit_GroupName.TabIndex = 3
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(150, 424)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 11
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(234, 424)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 12
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(62, 424)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 10
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectEntityID
    '
    Me.Combo_SelectEntityID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectEntityID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectEntityID.Location = New System.Drawing.Point(121, 60)
    Me.Combo_SelectEntityID.Name = "Combo_SelectEntityID"
    Me.Combo_SelectEntityID.Size = New System.Drawing.Size(262, 21)
    Me.Combo_SelectEntityID.TabIndex = 1
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnCopy)
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(57, 370)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(343, 48)
    Me.Panel1.TabIndex = 9
    '
    'btnCopy
    '
    Me.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCopy.Location = New System.Drawing.Point(260, 8)
    Me.btnCopy.Name = "btnCopy"
    Me.btnCopy.Size = New System.Drawing.Size(75, 28)
    Me.btnCopy.TabIndex = 5
    Me.btnCopy.Text = "&Copy"
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(6, 60)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 18
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(5, 86)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(436, 10)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(318, 424)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 13
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(452, 24)
    Me.RootMenu.TabIndex = 14
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Combo_GroupGroup
    '
    Me.Combo_GroupGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_GroupGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_GroupGroup.FormattingEnabled = True
    Me.Combo_GroupGroup.Location = New System.Drawing.Point(121, 129)
    Me.Combo_GroupGroup.Name = "Combo_GroupGroup"
    Me.Combo_GroupGroup.Size = New System.Drawing.Size(320, 21)
    Me.Combo_GroupGroup.TabIndex = 4
    '
    'lbl_GroupID
    '
    Me.lbl_GroupID.AutoSize = True
    Me.lbl_GroupID.Location = New System.Drawing.Point(6, 132)
    Me.lbl_GroupID.Name = "lbl_GroupID"
    Me.lbl_GroupID.Size = New System.Drawing.Size(36, 13)
    Me.lbl_GroupID.TabIndex = 80
    Me.lbl_GroupID.Text = "Group"
    '
    'lbl_EntityName
    '
    Me.lbl_EntityName.Location = New System.Drawing.Point(6, 106)
    Me.lbl_EntityName.Name = "lbl_EntityName"
    Me.lbl_EntityName.Size = New System.Drawing.Size(111, 17)
    Me.lbl_EntityName.TabIndex = 86
    Me.lbl_EntityName.Text = "Group Name"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(6, 36)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(69, 13)
    Me.Label6.TabIndex = 100
    Me.Label6.Text = "Select Group"
    '
    'Combo_SelectGroup
    '
    Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroup.Location = New System.Drawing.Point(121, 33)
    Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
    Me.Combo_SelectGroup.Size = New System.Drawing.Size(318, 21)
    Me.Combo_SelectGroup.TabIndex = 0
    '
    'Date_DateFrom
    '
    Me.Date_DateFrom.CustomFormat = "dd MMM yyyy"
    Me.Date_DateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_DateFrom.Location = New System.Drawing.Point(121, 156)
    Me.Date_DateFrom.Name = "Date_DateFrom"
    Me.Date_DateFrom.Size = New System.Drawing.Size(174, 20)
    Me.Date_DateFrom.TabIndex = 5
    '
    'Label_DecisionDate
    '
    Me.Label_DecisionDate.Location = New System.Drawing.Point(6, 160)
    Me.Label_DecisionDate.Name = "Label_DecisionDate"
    Me.Label_DecisionDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_DecisionDate.TabIndex = 102
    Me.Label_DecisionDate.Text = "Date From"
    '
    'Date_DateTo
    '
    Me.Date_DateTo.CustomFormat = "dd MMM yyyy"
    Me.Date_DateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_DateTo.Location = New System.Drawing.Point(121, 182)
    Me.Date_DateTo.Name = "Date_DateTo"
    Me.Date_DateTo.Size = New System.Drawing.Size(174, 20)
    Me.Date_DateTo.TabIndex = 6
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(6, 186)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(100, 16)
    Me.Label2.TabIndex = 104
    Me.Label2.Text = "Date To"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(3, 8)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(111, 35)
    Me.Label3.TabIndex = 105
    Me.Label3.Text = "Default Optimisation Constraint Set"
    '
    'Radio_NoConstraintSet
    '
    Me.Radio_NoConstraintSet.AutoSize = True
    Me.Radio_NoConstraintSet.Location = New System.Drawing.Point(118, 6)
    Me.Radio_NoConstraintSet.Name = "Radio_NoConstraintSet"
    Me.Radio_NoConstraintSet.Size = New System.Drawing.Size(54, 17)
    Me.Radio_NoConstraintSet.TabIndex = 0
    Me.Radio_NoConstraintSet.TabStop = True
    Me.Radio_NoConstraintSet.Text = "None."
    Me.Radio_NoConstraintSet.UseVisualStyleBackColor = True
    '
    'Radio_NewConstraintSet
    '
    Me.Radio_NewConstraintSet.AutoSize = True
    Me.Radio_NewConstraintSet.Location = New System.Drawing.Point(118, 29)
    Me.Radio_NewConstraintSet.Name = "Radio_NewConstraintSet"
    Me.Radio_NewConstraintSet.Size = New System.Drawing.Size(116, 17)
    Me.Radio_NewConstraintSet.TabIndex = 1
    Me.Radio_NewConstraintSet.TabStop = True
    Me.Radio_NewConstraintSet.Text = "New Constraint Set"
    Me.Radio_NewConstraintSet.UseVisualStyleBackColor = True
    '
    'Radio_ExistingConstraintSet
    '
    Me.Radio_ExistingConstraintSet.AutoSize = True
    Me.Radio_ExistingConstraintSet.Location = New System.Drawing.Point(118, 52)
    Me.Radio_ExistingConstraintSet.Name = "Radio_ExistingConstraintSet"
    Me.Radio_ExistingConstraintSet.Size = New System.Drawing.Size(14, 13)
    Me.Radio_ExistingConstraintSet.TabIndex = 2
    Me.Radio_ExistingConstraintSet.TabStop = True
    Me.Radio_ExistingConstraintSet.UseVisualStyleBackColor = True
    '
    'Combo_ConstraintSets
    '
    Me.Combo_ConstraintSets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ConstraintSets.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ConstraintSets.Location = New System.Drawing.Point(138, 49)
    Me.Combo_ConstraintSets.Name = "Combo_ConstraintSets"
    Me.Combo_ConstraintSets.Size = New System.Drawing.Size(289, 21)
    Me.Combo_ConstraintSets.TabIndex = 3
    '
    'Panel_Constraints
    '
    Me.Panel_Constraints.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Constraints.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_Constraints.Controls.Add(Me.Label3)
    Me.Panel_Constraints.Controls.Add(Me.Combo_ConstraintSets)
    Me.Panel_Constraints.Controls.Add(Me.Radio_NoConstraintSet)
    Me.Panel_Constraints.Controls.Add(Me.Radio_ExistingConstraintSet)
    Me.Panel_Constraints.Controls.Add(Me.Radio_NewConstraintSet)
    Me.Panel_Constraints.Location = New System.Drawing.Point(5, 208)
    Me.Panel_Constraints.Name = "Panel_Constraints"
    Me.Panel_Constraints.Size = New System.Drawing.Size(440, 80)
    Me.Panel_Constraints.TabIndex = 7
    '
    'Panel_Covariance
    '
    Me.Panel_Covariance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Covariance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_Covariance.Controls.Add(Me.Label4)
    Me.Panel_Covariance.Controls.Add(Me.Combo_CovarianceMatrix)
    Me.Panel_Covariance.Controls.Add(Me.Radio_LiveCovarianceMatrix)
    Me.Panel_Covariance.Controls.Add(Me.Radio_SavedCovarianceMatrix)
    Me.Panel_Covariance.Location = New System.Drawing.Point(5, 294)
    Me.Panel_Covariance.Name = "Panel_Covariance"
    Me.Panel_Covariance.Size = New System.Drawing.Size(440, 61)
    Me.Panel_Covariance.TabIndex = 8
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(3, 8)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(111, 35)
    Me.Label4.TabIndex = 105
    Me.Label4.Text = "Default Covariance Matrix"
    '
    'Combo_CovarianceMatrix
    '
    Me.Combo_CovarianceMatrix.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_CovarianceMatrix.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_CovarianceMatrix.Location = New System.Drawing.Point(138, 29)
    Me.Combo_CovarianceMatrix.Name = "Combo_CovarianceMatrix"
    Me.Combo_CovarianceMatrix.Size = New System.Drawing.Size(289, 21)
    Me.Combo_CovarianceMatrix.TabIndex = 2
    '
    'Radio_LiveCovarianceMatrix
    '
    Me.Radio_LiveCovarianceMatrix.AutoSize = True
    Me.Radio_LiveCovarianceMatrix.Location = New System.Drawing.Point(118, 6)
    Me.Radio_LiveCovarianceMatrix.Name = "Radio_LiveCovarianceMatrix"
    Me.Radio_LiveCovarianceMatrix.Size = New System.Drawing.Size(48, 17)
    Me.Radio_LiveCovarianceMatrix.TabIndex = 0
    Me.Radio_LiveCovarianceMatrix.TabStop = True
    Me.Radio_LiveCovarianceMatrix.Text = "Live."
    Me.Radio_LiveCovarianceMatrix.UseVisualStyleBackColor = True
    '
    'Radio_SavedCovarianceMatrix
    '
    Me.Radio_SavedCovarianceMatrix.AutoSize = True
    Me.Radio_SavedCovarianceMatrix.Location = New System.Drawing.Point(118, 32)
    Me.Radio_SavedCovarianceMatrix.Name = "Radio_SavedCovarianceMatrix"
    Me.Radio_SavedCovarianceMatrix.Size = New System.Drawing.Size(14, 13)
    Me.Radio_SavedCovarianceMatrix.TabIndex = 1
    Me.Radio_SavedCovarianceMatrix.TabStop = True
    Me.Radio_SavedCovarianceMatrix.UseVisualStyleBackColor = True
    '
    'frmGroupList
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(452, 468)
    Me.Controls.Add(Me.Panel_Covariance)
    Me.Controls.Add(Me.Panel_Constraints)
    Me.Controls.Add(Me.Date_DateTo)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Date_DateFrom)
    Me.Controls.Add(Me.Label_DecisionDate)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_SelectGroup)
    Me.Controls.Add(Me.lbl_EntityName)
    Me.Controls.Add(Me.Combo_GroupGroup)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectEntityID)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.edit_GroupName)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.lbl_GroupID)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(458, 496)
    Me.Name = "frmGroupList"
    Me.Text = "Add/Edit Groups"
    Me.Panel1.ResumeLayout(False)
    Me.Panel_Constraints.ResumeLayout(False)
    Me.Panel_Constraints.PerformLayout()
    Me.Panel_Covariance.ResumeLayout(False)
    Me.Panel_Covariance.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  Private THIS_TABLENAME As String
  Private THIS_ADAPTORNAME As String
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblGroupList
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  Private THIS_FORM_SelectingCombo As ComboBox
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
  Private THIS_FORM_SelectBy As String
  Private THIS_FORM_OrderBy As String

  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Data Structures

	Private myDataset As RenaissanceDataClass.DSGroupList				' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
  Private myConnection As SqlConnection
  Private myAdaptor As SqlDataAdapter

  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

  Private SortedRows() As DataRow
  Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSGroupList.tblGroupListRow		 ' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private AddNewRecord As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public ReadOnly Property FormChangedFlag() As Boolean
		Get
			Return FormChanged
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectEntityID
		THIS_FORM_NewMoveToControl = Me.edit_GroupName

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		THIS_FORM_ValueMember = "GroupListID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmGroupList

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler edit_GroupName.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events

		AddHandler edit_GroupName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_GroupGroup.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_GroupGroup.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Date_DateFrom.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_DateTo.ValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_GroupGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_GroupGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_GroupGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_GroupGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_GroupGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_ConstraintSets.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ConstraintSets.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ConstraintSets.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ConstraintSets.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ConstraintSets.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_CovarianceMatrix.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_CovarianceMatrix.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_CovarianceMatrix.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_CovarianceMatrix.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_CovarianceMatrix.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Radio_NoConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_NewConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_ExistingConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ConstraintSets.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Radio_LiveCovarianceMatrix.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_SavedCovarianceMatrix.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_CovarianceMatrix.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		'Me.Controls.Add(MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent))
		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		Try
			InPaint = True

			Call SetGroupCombo()
			Call SetConstraintSetsCombo()
			Call SetCorrelationMatrixCombo()

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		If (Combo_SelectGroup.Items.Count > 0) Then
			Combo_SelectGroup.SelectedIndex = 0
		End If

		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler edit_GroupName.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler edit_GroupName.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_GroupGroup.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_GroupGroup.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_DateFrom.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_DateTo.ValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_GroupGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_GroupGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_GroupGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_GroupGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_GroupGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_ConstraintSets.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ConstraintSets.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ConstraintSets.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ConstraintSets.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ConstraintSets.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_CovarianceMatrix.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_CovarianceMatrix.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_CovarianceMatrix.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_CovarianceMatrix.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_CovarianceMatrix.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Radio_LiveCovarianceMatrix.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_SavedCovarianceMatrix.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_CovarianceMatrix.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Radio_NoConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_NewConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_ExistingConstraintSet.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ConstraintSets.SelectedValueChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' SetGroupCombo
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetGroupCombo()
		End If

		' SetConstraintSetsCombo
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaConstraintList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetConstraintSetsCombo()
		End If

		' SetCorrelationMatrixCombo
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCovarianceList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetCorrelationMatrixCombo()
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If



		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) Then
			SelectString = "GroupGroup='" & Combo_SelectGroup.SelectedValue.ToString & "'"
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
				Me.btnCopy.Enabled = False
			End If
		End If

	End Sub

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetGroupCombo()
		' ***************************************************************
		'
		' ***************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, True, "", "All Groups")		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_GroupGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, False)	 ' 

	End Sub

	Private Sub SetConstraintSetsCombo()
		' ***************************************************************
		'
		' ***************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_ConstraintSets, _
		RenaissanceStandardDatasets.tblGenoaConstraintList, _
		"ConstraintGroupName", _
		"ConstraintGroupID", _
		"", False, True, True, 0, "")		' 

	End Sub

	Private Sub SetCorrelationMatrixCombo()
		' ***************************************************************
		'
		' ***************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_CovarianceMatrix, _
		RenaissanceStandardDatasets.tblCovarianceList, _
		"ListName", _
		"CovListID", _
		"", False, True, True, 0, "")		' 

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSGroupList.tblGroupListRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""
			Me.edit_GroupName.Text = ""

			If Me.Combo_GroupGroup.Items.Count > 0 Then
				Combo_GroupGroup.SelectedIndex = 0
			End If

			Me.Date_DateFrom.Value = #1/1/1900#
			Me.Date_DateTo.Value = #1/1/3000#

			Me.Radio_NoConstraintSet.Checked = True
			Me.Radio_LiveCovarianceMatrix.Checked = True

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
				Me.btnCopy.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
				Me.btnCopy.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.edit_GroupName.Text = thisDataRow.GroupListName

				Combo_GroupGroup.Text = thisDataRow.GroupGroup

				Date_DateFrom.Value = thisDataRow.GroupDateFrom
				Date_DateTo.Value = thisDataRow.GroupDateTo

				If (thisDataRow.DefaultConstraintGroup <= 0) Then
					Me.Radio_NoConstraintSet.Checked = True
				Else
					Me.Radio_ExistingConstraintSet.Checked = True
					Try
						Me.Combo_ConstraintSets.SelectedValue = thisDataRow.DefaultConstraintGroup
					Catch ex As Exception
					End Try
				End If

				' Combo_CovarianceMatrix
				If (thisDataRow.DefaultCovarianceMatrix <= 0) Then
					Me.Radio_LiveCovarianceMatrix.Checked = True
				Else
					Me.Radio_SavedCovarianceMatrix.Checked = True
					Try
						Me.Combo_CovarianceMatrix.SelectedValue = thisDataRow.DefaultCovarianceMatrix
					Catch ex As Exception
					End Try
				End If

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
				Me.btnCopy.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' Note that GroupList Entries are also added from the FundSearch Form. Changes
		' to the GroupList table should be reflected there also.
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add : "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If


		Try
			' Set 'Paint' flag.
			InPaint = True

			' *************************************************************
			' Lock the Data Table, to prevent update conflicts.
			' *************************************************************
			SyncLock myTable

				' Initiate Edit,
				thisDataRow.BeginEdit()

				' Set Data Values

				thisDataRow.GroupListName = edit_GroupName.Text
				LogString &= ", GroupListName = " & edit_GroupName.Text

				If (Combo_GroupGroup.SelectedIndex >= 0) Then
					thisDataRow.GroupGroup = Me.Combo_GroupGroup.SelectedValue.ToString
				Else
					thisDataRow.GroupGroup = Me.Combo_GroupGroup.Text
				End If

				thisDataRow.GroupDateFrom = Me.Date_DateFrom.Value
				thisDataRow.GroupDateTo = Me.Date_DateTo.Value

				Try
					If (Me.Radio_ExistingConstraintSet.Checked) AndAlso (Combo_ConstraintSets.SelectedIndex > 0) AndAlso (IsNumeric(Combo_ConstraintSets.SelectedValue)) Then
						thisDataRow.DefaultConstraintGroup = CInt(Combo_ConstraintSets.SelectedValue)
					ElseIf (Me.Radio_NewConstraintSet.Checked) Then
						' OK, Add a New Constraint Set

						Dim NewConstraintSetID As Integer

						NewConstraintSetID = MainForm.CreateNewConstraintSet(edit_GroupName.Text)

						If (NewConstraintSetID > 0) Then
							thisDataRow.DefaultConstraintGroup = NewConstraintSetID
						Else
							thisDataRow.DefaultConstraintGroup = 0
						End If
					Else
						thisDataRow.DefaultConstraintGroup = 0
					End If
				Catch ex As Exception
					Radio_NoConstraintSet.Checked = True
					thisDataRow.DefaultConstraintGroup = 0
				End Try

				Try
					If (Me.Radio_SavedCovarianceMatrix.Checked) AndAlso (Combo_CovarianceMatrix.SelectedIndex > 0) AndAlso (IsNumeric(Combo_CovarianceMatrix.SelectedValue)) Then
						thisDataRow.DefaultCovarianceMatrix = CInt(Combo_CovarianceMatrix.SelectedValue)
					Else
						thisDataRow.DefaultCovarianceMatrix = 0
					End If
				Catch ex As Exception
					Radio_LiveCovarianceMatrix.Checked = True
					thisDataRow.DefaultCovarianceMatrix = 0
				End Try


				thisDataRow.EndEdit()
				InPaint = False

				' Add and Update DataRow. 

				ErrFlag = False

				If AddNewRecord = True Then
					Try
						myTable.Rows.Add(thisDataRow)
					Catch ex As Exception
						ErrFlag = True
						ErrMessage = ex.Message
						ErrStack = ex.StackTrace
					End Try
				End If

				UpdateRows(0) = thisDataRow

				' Post Additions / Updates to the underlying table :-
				Dim temp As Integer
				Try
					If (ErrFlag = False) Then
						myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
						myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

						temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
					End If

				Catch ex As Exception

					ErrMessage = ex.Message
					ErrFlag = True
					ErrStack = ex.StackTrace

				End Try

				thisAuditID = thisDataRow.AuditID

			End SyncLock

		Catch ex As Exception
			ErrFlag = True
			ErrMessage = ex.Message
			ErrStack = ex.StackTrace
		Finally
			InPaint = False
		End Try


		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes

    Dim UpdateString As String = ""

    Try

      If (thisDataRow IsNot Nothing) AndAlso (Not thisDataRow.IsGroupListIDNull) Then
        UpdateString = thisDataRow.GroupListID.ToString
      End If

    Catch ex As Exception

      UpdateString = ""

    Finally

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, UpdateString))

    End Try

	End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.edit_GroupName.Enabled = True
			Combo_GroupGroup.Enabled = True
			Me.Date_DateFrom.Enabled = True
			Me.Date_DateTo.Enabled = True
			Panel_Constraints.Enabled = True
			Panel_Covariance.Enabled = True
		Else

			Me.edit_GroupName.Enabled = False
			Combo_GroupGroup.Enabled = False
			Me.Date_DateFrom.Enabled = False
			Me.Date_DateTo.Enabled = False
			Panel_Constraints.Enabled = False
			Panel_Covariance.Enabled = False

		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.edit_GroupName.Text.Length <= 0 Then
			pReturnString = "Due Group Name must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *****************************************************************************
		' Selection Combo. SelectedItem changed.
    '
		' *****************************************************************************

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' *****************************************************************************
		' 'Previous' Button.
		' *****************************************************************************

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' *****************************************************************************
		' 'Next' Button.
		' *****************************************************************************

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' *****************************************************************************
		' 'First' Button.
		' *****************************************************************************

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' *****************************************************************************
		' 'Last' Button.
		' *****************************************************************************

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' *****************************************************************************
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.
		' *****************************************************************************

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

  End Sub

  Public Sub AddNewGroup()
    ' *************************************************************
    '
    ' *************************************************************

		Call btnAdd_Click(Me.btnAdd, New System.EventArgs)

  End Sub

  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' *************************************************************
    ' Prepare form to Add a new record.
    ' *************************************************************


    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If (Combo_SelectGroup.Items.Count > 0) Then
      Combo_SelectGroup.SelectedIndex = 0
    End If

    Application.DoEvents()

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
		Me.btnCancel.Enabled = True
		Me.btnCopy.Enabled = False

    Me.THIS_FORM_SelectingCombo.Enabled = False
    If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
      Me.Combo_GroupGroup.SelectedValue = Combo_SelectGroup.SelectedValue
    End If

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

	Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
		' *************************************************************
		' Copy Group
		' *************************************************************

		Try
			Dim NewGroupName As String
			Dim Position As Integer
			Dim NewGroupID As Integer
			Dim DuplicateConstraintSet As Boolean = False

			Position = Get_Position(thisAuditID)

			If Position >= 0 Then
				thisDataRow = Me.SortedRows(Position)

				NewGroupName = InputBox("New Group Name :", "Copy Group", "Copy of " & edit_GroupName.Text)

				If (NewGroupName.Length > 0) Then

					' Reference Existing or Duplicate Copy of Constraint Set.

					If (Radio_ExistingConstraintSet.Checked) Then
						If (MessageBox.Show("Do you wish to Create a new copy of the Constraint set (YES)" & vbCrLf & "or just reference the current set (NO)", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
							DuplicateConstraintSet = True
						End If
					End If

					NewGroupID = CopyToNewGroup(MainForm, thisDataRow.GroupListID, NewGroupName, True, True, DuplicateConstraintSet)
					If (NewGroupID > 0) Then
						thisAuditID = NewGroupID
					End If

					' Finish off

					AddNewRecord = False
					FormChanged = False

					Me.THIS_FORM_SelectingCombo.Enabled = True

					' Propagate changes

					Dim ThisMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

					ThisMessage.TableChanged(THIS_FORM_ChangeID) = True
					ThisMessage.UpdateDetail(THIS_FORM_ChangeID) = NewGroupID.ToString
					ThisMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItems.ChangeID) = True
					ThisMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItemData.ChangeID) = True
					If (DuplicateConstraintSet) Then
						ThisMessage.TableChanged(RenaissanceStandardDatasets.tblGenoaConstraintList.ChangeID) = True
						ThisMessage.TableChanged(RenaissanceStandardDatasets.tblGenoaConstraintItems.ChangeID) = True
					End If

					Call MainForm.Main_RaiseEvent(ThisMessage)

				End If

			Else
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Unable to copy : There does not appear to be a Group Selected. ", "", True)
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Copying Genoa Group.", ex.StackTrace, True)
		End Try

	End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *************************************************************
    ' Close Form
    ' *************************************************************


    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

  Private Sub Radio_NoConstraintSet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_NoConstraintSet.CheckedChanged
    ' *************************************************************
    '
    ' *************************************************************

    If (Me.Created) AndAlso (Not Me.IsDisposed) Then
      If (Radio_NoConstraintSet.Checked) Then
        Radio_NewConstraintSet.Checked = False
        Radio_ExistingConstraintSet.Checked = False
        Combo_ConstraintSets.Enabled = False
        If (Combo_ConstraintSets.Items.Count > 0) Then
          Combo_ConstraintSets.SelectedIndex = 0
        End If
      End If
    End If

  End Sub

  Private Sub Radio_NewConstraintSet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_NewConstraintSet.CheckedChanged
    ' *************************************************************
    '
    ' *************************************************************

    If (Me.Created) AndAlso (Not Me.IsDisposed) Then
      If (Radio_NewConstraintSet.Checked) Then
        Radio_NoConstraintSet.Checked = False
        Radio_ExistingConstraintSet.Checked = False
        Combo_ConstraintSets.Enabled = False
        If (Combo_ConstraintSets.Items.Count > 0) Then
          Combo_ConstraintSets.SelectedIndex = 0
        End If
      End If
    End If

  End Sub

  Private Sub Radio_ExistingConstraintSet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingConstraintSet.CheckedChanged
    ' *************************************************************
    '
    ' *************************************************************

    If (Me.Created) AndAlso (Not Me.IsDisposed) Then
      If (Radio_ExistingConstraintSet.Checked) Then
        Radio_NoConstraintSet.Checked = False
        Radio_NewConstraintSet.Checked = False
        Combo_ConstraintSets.Enabled = True
      End If
    End If

  End Sub

	Private Sub Radio_LiveCovarianceMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_LiveCovarianceMatrix.CheckedChanged
		' *************************************************************
		'
		' *************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Radio_LiveCovarianceMatrix.Checked) Then
				Radio_SavedCovarianceMatrix.Checked = False
				Combo_CovarianceMatrix.Enabled = False

				If (Combo_CovarianceMatrix.Items.Count > 0) Then
					Combo_CovarianceMatrix.SelectedIndex = 0
				End If
			End If

		End If

	End Sub

	Private Sub Radio_SavedCovarianceMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SavedCovarianceMatrix.CheckedChanged
		' *************************************************************
		'
		' *************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Radio_SavedCovarianceMatrix.Checked) Then
				Radio_LiveCovarianceMatrix.Checked = False
				Combo_CovarianceMatrix.Enabled = True
			End If
		End If

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region








End Class
