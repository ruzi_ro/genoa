﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Genoa")> 
<Assembly: AssemblyDescription("Fund Analysis Application")> 
<Assembly: AssemblyCompany("F&&C Partners LLP")> 
<Assembly: AssemblyProduct("Genoa")> 
<Assembly: AssemblyCopyright("Copyright © F&&C Partners LLP 2006")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("daad2d83-b908-403c-bb39-8006863ec05e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.90.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
